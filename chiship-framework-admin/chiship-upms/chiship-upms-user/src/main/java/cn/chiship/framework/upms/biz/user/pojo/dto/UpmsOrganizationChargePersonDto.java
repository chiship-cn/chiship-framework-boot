package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 组织负责人表单
 *
 * @author LiJian
 */
@ApiModel(value = "组织负责人表单")
public class UpmsOrganizationChargePersonDto {

	@ApiModelProperty(value = "组织机构ID")
	@NotNull(message = "组织机构ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 36, message = BaseTipConstants.LENGTH_MIN_MAX)
	private String organizationId;

	@ApiModelProperty(value = "用户ID")
	@NotNull(message = "用户ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 36, message = BaseTipConstants.LENGTH_MIN_MAX)
	private String userId;

	@ApiModelProperty(value = "真实姓名")
	@NotNull(message = "真实姓名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 10, message = BaseTipConstants.LENGTH_MIN_MAX)
	private String realName;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

}
