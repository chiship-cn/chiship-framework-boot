package cn.chiship.framework.upms.biz.user.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "单位机构表单")
public class UpmsUnitDto {

	@ApiModelProperty(value = "单位编号")
	@NotNull
	@Length(min = 1)
	private String organizationCode;

	@ApiModelProperty(value = "单位全称")
	@NotNull
	@Length(min = 1)
	private String organizationName;

	@ApiModelProperty(value = "所属上级")
	@NotNull
	@Length(min = 1)
	private String pid;

	@ApiModelProperty(value = "单位类型")
	@NotNull
	private String organizationCategory;

	@ApiModelProperty(value = "排序")
	@NotNull
	@Min(1)
	private Integer sortOrders;

	@ApiModelProperty(value = "单位Logo")
	private String logo;

	@ApiModelProperty(value = "统一社会信用代码")
	private String socialCreditCode;

	@ApiModelProperty(value = "营业执照")
	private String businessLicense;

	@ApiModelProperty(value = "机构标签")
	private String organizationTag;

	@ApiModelProperty(value = "单位级别")
	private String organizationLevel;

	@ApiModelProperty(value = "省")
	private Long regionLevel1;

	@ApiModelProperty(value = "市")
	private Long regionLevel2;

	@ApiModelProperty(value = "县/区")
	private Long regionLevel3;

	@ApiModelProperty(value = "街道")
	private Long regionLevel4;

	@ApiModelProperty(value = "详细地区")
	private String address;

	@ApiModelProperty(value = "经纬度")
	private String latlng;

	@ApiModelProperty(value = "单位电话")
	private String phone;

	@ApiModelProperty(value = "单位传真")
	private String fax;

	@ApiModelProperty(value = "联系人")
	private String linkPerson;

	@ApiModelProperty(value = "联系电话")
	private String linkMobile;

	@ApiModelProperty(value = "负责人信息")
	private String chargeInfo;

	@ApiModelProperty(value = "单位描述")
	private String description;

	@ApiModelProperty(value = "工作时间")
	private String workTime;

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOrganizationCategory() {
		return organizationCategory;
	}

	public void setOrganizationCategory(String organizationCategory) {
		this.organizationCategory = organizationCategory;
	}

	public Integer getSortOrders() {
		return sortOrders;
	}

	public void setSortOrders(Integer sortOrders) {
		this.sortOrders = sortOrders;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getSocialCreditCode() {
		return socialCreditCode;
	}

	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode;
	}

	public String getBusinessLicense() {
		return businessLicense;
	}

	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	public String getOrganizationTag() {
		return organizationTag;
	}

	public void setOrganizationTag(String organizationTag) {
		this.organizationTag = organizationTag;
	}

	public String getOrganizationLevel() {
		return organizationLevel;
	}

	public void setOrganizationLevel(String organizationLevel) {
		this.organizationLevel = organizationLevel;
	}

	public Long getRegionLevel1() {
		return regionLevel1;
	}

	public void setRegionLevel1(Long regionLevel1) {
		this.regionLevel1 = regionLevel1;
	}

	public Long getRegionLevel2() {
		return regionLevel2;
	}

	public void setRegionLevel2(Long regionLevel2) {
		this.regionLevel2 = regionLevel2;
	}

	public Long getRegionLevel3() {
		return regionLevel3;
	}

	public void setRegionLevel3(Long regionLevel3) {
		this.regionLevel3 = regionLevel3;
	}

	public Long getRegionLevel4() {
		return regionLevel4;
	}

	public void setRegionLevel4(Long regionLevel4) {
		this.regionLevel4 = regionLevel4;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatlng() {
		return latlng;
	}

	public void setLatlng(String latlng) {
		this.latlng = latlng;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getLinkPerson() {
		return linkPerson;
	}

	public void setLinkPerson(String linkPerson) {
		this.linkPerson = linkPerson;
	}

	public String getLinkMobile() {
		return linkMobile;
	}

	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}

	public String getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(String chargeInfo) {
		this.chargeInfo = chargeInfo;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

}
