package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import springfox.documentation.annotations.ApiIgnore;

/**
 * 在线用户
 *
 * @author lijian
 */
public interface UpmsUserOnlineService {

	/**
	 * 分页
	 * @param page
	 * @param limit
	 * @param ip
	 * @param userName
	 * @return
	 */
	BaseResult page(Long page, Long limit, String ip, String userName);

	/**
	 * 设置在线用户信息
	 * @param token
	 * @param userVO
	 * @param ip
	 * @return
	 */
	BaseResult loginUserToUserOnline(String token, @ApiIgnore CacheUserVO userVO, String ip);

	/**
	 * 退出
	 * @param sessionId
	 * @return
	 */
	BaseResult forceLogout(String sessionId);

}
