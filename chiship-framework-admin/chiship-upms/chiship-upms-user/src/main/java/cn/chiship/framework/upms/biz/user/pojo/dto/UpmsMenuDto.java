package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author LiJian
 */
@ApiModel(value = "菜单表单")
public class UpmsMenuDto {

	@ApiModelProperty(value = "所属上级")
	@NotNull
	@Length(min = 1)
	private String pid;

	@ApiModelProperty(value = "菜单名称")
	@NotNull
	@Length(min = 1)
	private String name;

	@ApiModelProperty(value = "菜单编码")
	@NotNull
	@Length(min = 1)
	private String menuCode;

	@ApiModelProperty(value = "菜单图标")
	@NotNull
	@Length(min = 1)
	private String icon;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	private Long orders;

	@ApiModelProperty(value = "是否添加到应用(0:否,1:是)", required = true)
	@NotNull(message = "是否添加到应用" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte application;

	@ApiModelProperty(value = "应用分组")
	private String applicationGroup;

	@ApiModelProperty(value = "分类(0：PC 1：移动)", required = true)
	@NotNull(message = "分类" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte category;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	public Byte getApplication() {
		return application;
	}

	public void setApplication(Byte application) {
		this.application = application;
	}

	public String getApplicationGroup() {
		return applicationGroup;
	}

	public void setApplicationGroup(String applicationGroup) {
		this.applicationGroup = applicationGroup;
	}

	public Byte getCategory() {
		return category;
	}

	public void setCategory(Byte category) {
		this.category = category;
	}

}
