package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-11-29
 */
public class UpmsUser implements Serializable {
    /**
     * 创建时间
     */
    private String id;

    /**
     * 创建时间
     */
    private Long gmtCreated;

    /**
     * 更新时间
     */
    private Long gmtModified;

    /**
     * 逻辑删除（0：否，1：是）
     */
    private Byte isDeleted;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 个性签名
     */
    private String signature;

    /**
     * 帐号
     */
    private String userName;

    /**
     * 密码
     */
    private String password;

    /**
     * 是否初始化密码  0否 1是
     */
    private Byte isInitPassword;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 姓名
     */
    private String realName;

    /**
     * 是否添加到通讯录  0 否 1 是
     */
    private Byte isAddMailList;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 座机
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别(0:女，1：男，2：未知)
     */
    private Byte gender;

    /**
     * 身份证
     */
    private String idNumber;

    /**
     * 是否锁定（0：否，1：是）
     */
    private Byte isLocked;

    /**
     * 工号
     */
    private String userCode;

    /**
     * 钉钉用户ID
     */
    private String dingTalkUserId;

    /**
     * 企业微信用户ID
     */
    private String wxWorkUserId;

    /**
     * 类别
     */
    private String category;

    /**
     * 拼音
     */
    private String userPinyin;

    /**
     * QQ
     */
    private String qq;

    /**
     * 微信
     */
    private String weixin;

    /**
     * 是否内部员工
     */
    private Byte isInner;

    /**
     * 自我介绍
     */
    private String introduction;

    /**
     * 技能
     */
    private String skill;

    /**
     * 标签
     */
    private String tag;

    /**
     * 认证码
     */
    private String authorizationCode;

    /**
     * 籍贯省
     */
    private Long nativePlaceLevel1;

    /**
     * 籍贯市
     */
    private Long nativePlaceLevel2;

    /**
     * 籍贯县
     */
    private Long nativePlaceLevel3;

    /**
     * 文化程度
     */
    private String educationDegree;

    /**
     * 政治面貌
     */
    private String politicalOutlook;

    /**
     * 民族
     */
    private String nation;

    /**
     * 学历
     */
    private String education;

    /**
     * 婚姻状况
     */
    private String maritalStatus;

    /**
     * 毕业院校
     */
    private String graduateSchool;

    /**
     * 最高学位
     */
    private String highestDegree;

    /**
     * 专业
     */
    private String major;

    /**
     * 人员编制
     */
    private String staffing;

    /**
     * 参加工作时间
     */
    private Long startWorkDate;

    /**
     * 现居住地省
     */
    private Long currentResidenceLevel1;

    /**
     * 现居住地市
     */
    private Long currentResidenceLevel2;

    /**
     * 现居住地县
     */
    private Long currentResidenceLevel3;

    /**
     * 现居住地区
     */
    private Long currentResidenceLevel4;

    /**
     * 现住址
     */
    private String currentAddress;

    /**
     * 紧急联系人
     */
    private String emergencyContact;

    /**
     * 紧急联系电话
     */
    private String emergencyContactMobile;

    /**
     * 直属主管
     */
    private String reportsTo;

    /**
     * 上次登录日期
     */
    private Long prevLoginDate;

    /**
     * 上次登录IP
     */
    private String prevLoginIp;

    /**
     * 最后登录日期
     */
    private Long lastLoginDate;

    /**
     * 最后登录IP
     */
    private String lastLoginIp;

    /**
     * 登录次数
     */
    private Integer loginNum;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Long gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Byte getIsInitPassword() {
        return isInitPassword;
    }

    public void setIsInitPassword(Byte isInitPassword) {
        this.isInitPassword = isInitPassword;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public Byte getIsAddMailList() {
        return isAddMailList;
    }

    public void setIsAddMailList(Byte isAddMailList) {
        this.isAddMailList = isAddMailList;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public Byte getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Byte isLocked) {
        this.isLocked = isLocked;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getDingTalkUserId() {
        return dingTalkUserId;
    }

    public void setDingTalkUserId(String dingTalkUserId) {
        this.dingTalkUserId = dingTalkUserId;
    }

    public String getWxWorkUserId() {
        return wxWorkUserId;
    }

    public void setWxWorkUserId(String wxWorkUserId) {
        this.wxWorkUserId = wxWorkUserId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getUserPinyin() {
        return userPinyin;
    }

    public void setUserPinyin(String userPinyin) {
        this.userPinyin = userPinyin;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public Byte getIsInner() {
        return isInner;
    }

    public void setIsInner(Byte isInner) {
        this.isInner = isInner;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Long getNativePlaceLevel1() {
        return nativePlaceLevel1;
    }

    public void setNativePlaceLevel1(Long nativePlaceLevel1) {
        this.nativePlaceLevel1 = nativePlaceLevel1;
    }

    public Long getNativePlaceLevel2() {
        return nativePlaceLevel2;
    }

    public void setNativePlaceLevel2(Long nativePlaceLevel2) {
        this.nativePlaceLevel2 = nativePlaceLevel2;
    }

    public Long getNativePlaceLevel3() {
        return nativePlaceLevel3;
    }

    public void setNativePlaceLevel3(Long nativePlaceLevel3) {
        this.nativePlaceLevel3 = nativePlaceLevel3;
    }

    public String getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(String educationDegree) {
        this.educationDegree = educationDegree;
    }

    public String getPoliticalOutlook() {
        return politicalOutlook;
    }

    public void setPoliticalOutlook(String politicalOutlook) {
        this.politicalOutlook = politicalOutlook;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getGraduateSchool() {
        return graduateSchool;
    }

    public void setGraduateSchool(String graduateSchool) {
        this.graduateSchool = graduateSchool;
    }

    public String getHighestDegree() {
        return highestDegree;
    }

    public void setHighestDegree(String highestDegree) {
        this.highestDegree = highestDegree;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getStaffing() {
        return staffing;
    }

    public void setStaffing(String staffing) {
        this.staffing = staffing;
    }

    public Long getStartWorkDate() {
        return startWorkDate;
    }

    public void setStartWorkDate(Long startWorkDate) {
        this.startWorkDate = startWorkDate;
    }

    public Long getCurrentResidenceLevel1() {
        return currentResidenceLevel1;
    }

    public void setCurrentResidenceLevel1(Long currentResidenceLevel1) {
        this.currentResidenceLevel1 = currentResidenceLevel1;
    }

    public Long getCurrentResidenceLevel2() {
        return currentResidenceLevel2;
    }

    public void setCurrentResidenceLevel2(Long currentResidenceLevel2) {
        this.currentResidenceLevel2 = currentResidenceLevel2;
    }

    public Long getCurrentResidenceLevel3() {
        return currentResidenceLevel3;
    }

    public void setCurrentResidenceLevel3(Long currentResidenceLevel3) {
        this.currentResidenceLevel3 = currentResidenceLevel3;
    }

    public Long getCurrentResidenceLevel4() {
        return currentResidenceLevel4;
    }

    public void setCurrentResidenceLevel4(Long currentResidenceLevel4) {
        this.currentResidenceLevel4 = currentResidenceLevel4;
    }

    public String getCurrentAddress() {
        return currentAddress;
    }

    public void setCurrentAddress(String currentAddress) {
        this.currentAddress = currentAddress;
    }

    public String getEmergencyContact() {
        return emergencyContact;
    }

    public void setEmergencyContact(String emergencyContact) {
        this.emergencyContact = emergencyContact;
    }

    public String getEmergencyContactMobile() {
        return emergencyContactMobile;
    }

    public void setEmergencyContactMobile(String emergencyContactMobile) {
        this.emergencyContactMobile = emergencyContactMobile;
    }

    public String getReportsTo() {
        return reportsTo;
    }

    public void setReportsTo(String reportsTo) {
        this.reportsTo = reportsTo;
    }

    public Long getPrevLoginDate() {
        return prevLoginDate;
    }

    public void setPrevLoginDate(Long prevLoginDate) {
        this.prevLoginDate = prevLoginDate;
    }

    public String getPrevLoginIp() {
        return prevLoginIp;
    }

    public void setPrevLoginIp(String prevLoginIp) {
        this.prevLoginIp = prevLoginIp;
    }

    public Long getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Long lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }

    public Integer getLoginNum() {
        return loginNum;
    }

    public void setLoginNum(Integer loginNum) {
        this.loginNum = loginNum;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreated=").append(gmtCreated);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", nickName=").append(nickName);
        sb.append(", signature=").append(signature);
        sb.append(", userName=").append(userName);
        sb.append(", password=").append(password);
        sb.append(", isInitPassword=").append(isInitPassword);
        sb.append(", salt=").append(salt);
        sb.append(", realName=").append(realName);
        sb.append(", isAddMailList=").append(isAddMailList);
        sb.append(", mobile=").append(mobile);
        sb.append(", avatar=").append(avatar);
        sb.append(", phone=").append(phone);
        sb.append(", email=").append(email);
        sb.append(", gender=").append(gender);
        sb.append(", idNumber=").append(idNumber);
        sb.append(", isLocked=").append(isLocked);
        sb.append(", userCode=").append(userCode);
        sb.append(", dingTalkUserId=").append(dingTalkUserId);
        sb.append(", wxWorkUserId=").append(wxWorkUserId);
        sb.append(", category=").append(category);
        sb.append(", userPinyin=").append(userPinyin);
        sb.append(", qq=").append(qq);
        sb.append(", weixin=").append(weixin);
        sb.append(", isInner=").append(isInner);
        sb.append(", introduction=").append(introduction);
        sb.append(", skill=").append(skill);
        sb.append(", tag=").append(tag);
        sb.append(", authorizationCode=").append(authorizationCode);
        sb.append(", nativePlaceLevel1=").append(nativePlaceLevel1);
        sb.append(", nativePlaceLevel2=").append(nativePlaceLevel2);
        sb.append(", nativePlaceLevel3=").append(nativePlaceLevel3);
        sb.append(", educationDegree=").append(educationDegree);
        sb.append(", politicalOutlook=").append(politicalOutlook);
        sb.append(", nation=").append(nation);
        sb.append(", education=").append(education);
        sb.append(", maritalStatus=").append(maritalStatus);
        sb.append(", graduateSchool=").append(graduateSchool);
        sb.append(", highestDegree=").append(highestDegree);
        sb.append(", major=").append(major);
        sb.append(", staffing=").append(staffing);
        sb.append(", startWorkDate=").append(startWorkDate);
        sb.append(", currentResidenceLevel1=").append(currentResidenceLevel1);
        sb.append(", currentResidenceLevel2=").append(currentResidenceLevel2);
        sb.append(", currentResidenceLevel3=").append(currentResidenceLevel3);
        sb.append(", currentResidenceLevel4=").append(currentResidenceLevel4);
        sb.append(", currentAddress=").append(currentAddress);
        sb.append(", emergencyContact=").append(emergencyContact);
        sb.append(", emergencyContactMobile=").append(emergencyContactMobile);
        sb.append(", reportsTo=").append(reportsTo);
        sb.append(", prevLoginDate=").append(prevLoginDate);
        sb.append(", prevLoginIp=").append(prevLoginIp);
        sb.append(", lastLoginDate=").append(lastLoginDate);
        sb.append(", lastLoginIp=").append(lastLoginIp);
        sb.append(", loginNum=").append(loginNum);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        UpmsUser other = (UpmsUser) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGmtCreated() == null ? other.getGmtCreated() == null : this.getGmtCreated().equals(other.getGmtCreated()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getNickName() == null ? other.getNickName() == null : this.getNickName().equals(other.getNickName()))
            && (this.getSignature() == null ? other.getSignature() == null : this.getSignature().equals(other.getSignature()))
            && (this.getUserName() == null ? other.getUserName() == null : this.getUserName().equals(other.getUserName()))
            && (this.getPassword() == null ? other.getPassword() == null : this.getPassword().equals(other.getPassword()))
            && (this.getIsInitPassword() == null ? other.getIsInitPassword() == null : this.getIsInitPassword().equals(other.getIsInitPassword()))
            && (this.getSalt() == null ? other.getSalt() == null : this.getSalt().equals(other.getSalt()))
            && (this.getRealName() == null ? other.getRealName() == null : this.getRealName().equals(other.getRealName()))
            && (this.getIsAddMailList() == null ? other.getIsAddMailList() == null : this.getIsAddMailList().equals(other.getIsAddMailList()))
            && (this.getMobile() == null ? other.getMobile() == null : this.getMobile().equals(other.getMobile()))
            && (this.getAvatar() == null ? other.getAvatar() == null : this.getAvatar().equals(other.getAvatar()))
            && (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
            && (this.getEmail() == null ? other.getEmail() == null : this.getEmail().equals(other.getEmail()))
            && (this.getGender() == null ? other.getGender() == null : this.getGender().equals(other.getGender()))
            && (this.getIdNumber() == null ? other.getIdNumber() == null : this.getIdNumber().equals(other.getIdNumber()))
            && (this.getIsLocked() == null ? other.getIsLocked() == null : this.getIsLocked().equals(other.getIsLocked()))
            && (this.getUserCode() == null ? other.getUserCode() == null : this.getUserCode().equals(other.getUserCode()))
            && (this.getDingTalkUserId() == null ? other.getDingTalkUserId() == null : this.getDingTalkUserId().equals(other.getDingTalkUserId()))
            && (this.getWxWorkUserId() == null ? other.getWxWorkUserId() == null : this.getWxWorkUserId().equals(other.getWxWorkUserId()))
            && (this.getCategory() == null ? other.getCategory() == null : this.getCategory().equals(other.getCategory()))
            && (this.getUserPinyin() == null ? other.getUserPinyin() == null : this.getUserPinyin().equals(other.getUserPinyin()))
            && (this.getQq() == null ? other.getQq() == null : this.getQq().equals(other.getQq()))
            && (this.getWeixin() == null ? other.getWeixin() == null : this.getWeixin().equals(other.getWeixin()))
            && (this.getIsInner() == null ? other.getIsInner() == null : this.getIsInner().equals(other.getIsInner()))
            && (this.getIntroduction() == null ? other.getIntroduction() == null : this.getIntroduction().equals(other.getIntroduction()))
            && (this.getSkill() == null ? other.getSkill() == null : this.getSkill().equals(other.getSkill()))
            && (this.getTag() == null ? other.getTag() == null : this.getTag().equals(other.getTag()))
            && (this.getAuthorizationCode() == null ? other.getAuthorizationCode() == null : this.getAuthorizationCode().equals(other.getAuthorizationCode()))
            && (this.getNativePlaceLevel1() == null ? other.getNativePlaceLevel1() == null : this.getNativePlaceLevel1().equals(other.getNativePlaceLevel1()))
            && (this.getNativePlaceLevel2() == null ? other.getNativePlaceLevel2() == null : this.getNativePlaceLevel2().equals(other.getNativePlaceLevel2()))
            && (this.getNativePlaceLevel3() == null ? other.getNativePlaceLevel3() == null : this.getNativePlaceLevel3().equals(other.getNativePlaceLevel3()))
            && (this.getEducationDegree() == null ? other.getEducationDegree() == null : this.getEducationDegree().equals(other.getEducationDegree()))
            && (this.getPoliticalOutlook() == null ? other.getPoliticalOutlook() == null : this.getPoliticalOutlook().equals(other.getPoliticalOutlook()))
            && (this.getNation() == null ? other.getNation() == null : this.getNation().equals(other.getNation()))
            && (this.getEducation() == null ? other.getEducation() == null : this.getEducation().equals(other.getEducation()))
            && (this.getMaritalStatus() == null ? other.getMaritalStatus() == null : this.getMaritalStatus().equals(other.getMaritalStatus()))
            && (this.getGraduateSchool() == null ? other.getGraduateSchool() == null : this.getGraduateSchool().equals(other.getGraduateSchool()))
            && (this.getHighestDegree() == null ? other.getHighestDegree() == null : this.getHighestDegree().equals(other.getHighestDegree()))
            && (this.getMajor() == null ? other.getMajor() == null : this.getMajor().equals(other.getMajor()))
            && (this.getStaffing() == null ? other.getStaffing() == null : this.getStaffing().equals(other.getStaffing()))
            && (this.getStartWorkDate() == null ? other.getStartWorkDate() == null : this.getStartWorkDate().equals(other.getStartWorkDate()))
            && (this.getCurrentResidenceLevel1() == null ? other.getCurrentResidenceLevel1() == null : this.getCurrentResidenceLevel1().equals(other.getCurrentResidenceLevel1()))
            && (this.getCurrentResidenceLevel2() == null ? other.getCurrentResidenceLevel2() == null : this.getCurrentResidenceLevel2().equals(other.getCurrentResidenceLevel2()))
            && (this.getCurrentResidenceLevel3() == null ? other.getCurrentResidenceLevel3() == null : this.getCurrentResidenceLevel3().equals(other.getCurrentResidenceLevel3()))
            && (this.getCurrentResidenceLevel4() == null ? other.getCurrentResidenceLevel4() == null : this.getCurrentResidenceLevel4().equals(other.getCurrentResidenceLevel4()))
            && (this.getCurrentAddress() == null ? other.getCurrentAddress() == null : this.getCurrentAddress().equals(other.getCurrentAddress()))
            && (this.getEmergencyContact() == null ? other.getEmergencyContact() == null : this.getEmergencyContact().equals(other.getEmergencyContact()))
            && (this.getEmergencyContactMobile() == null ? other.getEmergencyContactMobile() == null : this.getEmergencyContactMobile().equals(other.getEmergencyContactMobile()))
            && (this.getReportsTo() == null ? other.getReportsTo() == null : this.getReportsTo().equals(other.getReportsTo()))
            && (this.getPrevLoginDate() == null ? other.getPrevLoginDate() == null : this.getPrevLoginDate().equals(other.getPrevLoginDate()))
            && (this.getPrevLoginIp() == null ? other.getPrevLoginIp() == null : this.getPrevLoginIp().equals(other.getPrevLoginIp()))
            && (this.getLastLoginDate() == null ? other.getLastLoginDate() == null : this.getLastLoginDate().equals(other.getLastLoginDate()))
            && (this.getLastLoginIp() == null ? other.getLastLoginIp() == null : this.getLastLoginIp().equals(other.getLastLoginIp()))
            && (this.getLoginNum() == null ? other.getLoginNum() == null : this.getLoginNum().equals(other.getLoginNum()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getNickName() == null) ? 0 : getNickName().hashCode());
        result = prime * result + ((getSignature() == null) ? 0 : getSignature().hashCode());
        result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
        result = prime * result + ((getPassword() == null) ? 0 : getPassword().hashCode());
        result = prime * result + ((getIsInitPassword() == null) ? 0 : getIsInitPassword().hashCode());
        result = prime * result + ((getSalt() == null) ? 0 : getSalt().hashCode());
        result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
        result = prime * result + ((getIsAddMailList() == null) ? 0 : getIsAddMailList().hashCode());
        result = prime * result + ((getMobile() == null) ? 0 : getMobile().hashCode());
        result = prime * result + ((getAvatar() == null) ? 0 : getAvatar().hashCode());
        result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
        result = prime * result + ((getEmail() == null) ? 0 : getEmail().hashCode());
        result = prime * result + ((getGender() == null) ? 0 : getGender().hashCode());
        result = prime * result + ((getIdNumber() == null) ? 0 : getIdNumber().hashCode());
        result = prime * result + ((getIsLocked() == null) ? 0 : getIsLocked().hashCode());
        result = prime * result + ((getUserCode() == null) ? 0 : getUserCode().hashCode());
        result = prime * result + ((getDingTalkUserId() == null) ? 0 : getDingTalkUserId().hashCode());
        result = prime * result + ((getWxWorkUserId() == null) ? 0 : getWxWorkUserId().hashCode());
        result = prime * result + ((getCategory() == null) ? 0 : getCategory().hashCode());
        result = prime * result + ((getUserPinyin() == null) ? 0 : getUserPinyin().hashCode());
        result = prime * result + ((getQq() == null) ? 0 : getQq().hashCode());
        result = prime * result + ((getWeixin() == null) ? 0 : getWeixin().hashCode());
        result = prime * result + ((getIsInner() == null) ? 0 : getIsInner().hashCode());
        result = prime * result + ((getIntroduction() == null) ? 0 : getIntroduction().hashCode());
        result = prime * result + ((getSkill() == null) ? 0 : getSkill().hashCode());
        result = prime * result + ((getTag() == null) ? 0 : getTag().hashCode());
        result = prime * result + ((getAuthorizationCode() == null) ? 0 : getAuthorizationCode().hashCode());
        result = prime * result + ((getNativePlaceLevel1() == null) ? 0 : getNativePlaceLevel1().hashCode());
        result = prime * result + ((getNativePlaceLevel2() == null) ? 0 : getNativePlaceLevel2().hashCode());
        result = prime * result + ((getNativePlaceLevel3() == null) ? 0 : getNativePlaceLevel3().hashCode());
        result = prime * result + ((getEducationDegree() == null) ? 0 : getEducationDegree().hashCode());
        result = prime * result + ((getPoliticalOutlook() == null) ? 0 : getPoliticalOutlook().hashCode());
        result = prime * result + ((getNation() == null) ? 0 : getNation().hashCode());
        result = prime * result + ((getEducation() == null) ? 0 : getEducation().hashCode());
        result = prime * result + ((getMaritalStatus() == null) ? 0 : getMaritalStatus().hashCode());
        result = prime * result + ((getGraduateSchool() == null) ? 0 : getGraduateSchool().hashCode());
        result = prime * result + ((getHighestDegree() == null) ? 0 : getHighestDegree().hashCode());
        result = prime * result + ((getMajor() == null) ? 0 : getMajor().hashCode());
        result = prime * result + ((getStaffing() == null) ? 0 : getStaffing().hashCode());
        result = prime * result + ((getStartWorkDate() == null) ? 0 : getStartWorkDate().hashCode());
        result = prime * result + ((getCurrentResidenceLevel1() == null) ? 0 : getCurrentResidenceLevel1().hashCode());
        result = prime * result + ((getCurrentResidenceLevel2() == null) ? 0 : getCurrentResidenceLevel2().hashCode());
        result = prime * result + ((getCurrentResidenceLevel3() == null) ? 0 : getCurrentResidenceLevel3().hashCode());
        result = prime * result + ((getCurrentResidenceLevel4() == null) ? 0 : getCurrentResidenceLevel4().hashCode());
        result = prime * result + ((getCurrentAddress() == null) ? 0 : getCurrentAddress().hashCode());
        result = prime * result + ((getEmergencyContact() == null) ? 0 : getEmergencyContact().hashCode());
        result = prime * result + ((getEmergencyContactMobile() == null) ? 0 : getEmergencyContactMobile().hashCode());
        result = prime * result + ((getReportsTo() == null) ? 0 : getReportsTo().hashCode());
        result = prime * result + ((getPrevLoginDate() == null) ? 0 : getPrevLoginDate().hashCode());
        result = prime * result + ((getPrevLoginIp() == null) ? 0 : getPrevLoginIp().hashCode());
        result = prime * result + ((getLastLoginDate() == null) ? 0 : getLastLoginDate().hashCode());
        result = prime * result + ((getLastLoginIp() == null) ? 0 : getLastLoginIp().hashCode());
        result = prime * result + ((getLoginNum() == null) ? 0 : getLoginNum().hashCode());
        return result;
    }
}