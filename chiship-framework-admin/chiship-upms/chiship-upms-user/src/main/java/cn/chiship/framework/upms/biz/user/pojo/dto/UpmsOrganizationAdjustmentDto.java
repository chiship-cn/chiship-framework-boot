package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel("组织结构调整表单")
public class UpmsOrganizationAdjustmentDto {

	@ApiModelProperty(value = "组织机构ID")
	@NotNull(message = "组织机构ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 36, message = BaseTipConstants.LENGTH_MIN_MAX)
	private String organizationId;

	@ApiModelProperty(value = "目标组织机构ID")
	@NotNull(message = "目标组织机构ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11, max = 36, message = BaseTipConstants.LENGTH_MIN_MAX)
	private String targetOrganizationId;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getTargetOrganizationId() {
		return targetOrganizationId;
	}

	public void setTargetOrganizationId(String targetOrganizationId) {
		this.targetOrganizationId = targetOrganizationId;
	}

}
