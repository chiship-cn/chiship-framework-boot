package cn.chiship.framework.upms.biz.user.service.impl;

import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsRoleExample;
import cn.chiship.framework.upms.biz.user.mapper.UpmsRoleMapper;
import cn.chiship.framework.upms.biz.user.service.UpmsRoleService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import cn.chiship.sdk.core.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * 角色业务接口实现层 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsRoleServiceImpl extends BaseServiceImpl<UpmsRole, UpmsRoleExample> implements UpmsRoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsRoleServiceImpl.class);

	@Resource
	UpmsRoleMapper upmsRoleMapper;

	@Override
	public BaseResult insertSelective(UpmsRole upmsRole) {
		UpmsRoleExample upmsRoleExample = new UpmsRoleExample();
		upmsRoleExample.createCriteria().andCodeEqualTo(upmsRole.getCode());
		List<UpmsRole> upmsRoleList = super.selectByExample(upmsRoleExample);
		if (!upmsRoleList.isEmpty()) {
			return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "该角色编号已存在，请重新输入");
		}
		return super.insertSelective(upmsRole);
	}

	@Override
	public BaseResult updateByPrimaryKeySelective(UpmsRole upmsRole) {
		UpmsRoleExample upmsRoleExample = new UpmsRoleExample();
		upmsRoleExample.createCriteria().andCodeEqualTo(upmsRole.getCode()).andIdNotEqualTo(upmsRole.getId());
		List<UpmsRole> upmsRoleList = super.selectByExample(upmsRoleExample);
		if (!upmsRoleList.isEmpty()) {
			return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "该角色编号已存在，请重新输入");
		}
		return super.updateByPrimaryKeySelective(upmsRole);
	}

	@Override
	public BaseResult treeTable(String keyword) {
		UpmsRoleExample upmsRoleExample = new UpmsRoleExample();
		UpmsRoleExample.Criteria criteria = upmsRoleExample.createCriteria();
		criteria.andPidEqualTo("0");

		List<UpmsRole> parentRoles = upmsRoleMapper.selectByExample(upmsRoleExample);
		List<String> parentRoleIds = new ArrayList<>();
		JSONArray roleJsonArrays = new JSONArray();
		for (UpmsRole role : parentRoles) {
			parentRoleIds.add(role.getId());
			roleJsonArrays.add(JSON.parseObject(JSON.toJSONString(role)));
		}
		if (!parentRoleIds.isEmpty()) {
			UpmsRoleExample roleExample = new UpmsRoleExample();
			UpmsRoleExample.Criteria roleExampleCriteria1 = roleExample.createCriteria();
			roleExampleCriteria1.andPidIn(parentRoleIds);
			UpmsRoleExample.Criteria roleExampleCriteria2 = roleExample.createCriteria();
			roleExampleCriteria2.andPidIn(parentRoleIds);
			if (!StringUtil.isNullOrEmpty(keyword)) {
				roleExampleCriteria1.andCodeLike(keyword + "%");
				roleExampleCriteria2.andNameLike(keyword + "%");
			}
			roleExample.or(roleExampleCriteria2);

			List<UpmsRole> roles = upmsRoleMapper.selectByExample(roleExample);
			for (int i = 0; i < roleJsonArrays.size(); i++) {
				JSONObject parentRole = roleJsonArrays.getJSONObject(i);
				JSONArray childrenJsonArrays = new JSONArray();
				for (UpmsRole role : roles) {
					if (role.getPid().equals(parentRole.getString("id"))) {
						childrenJsonArrays.add(JSON.parseObject(JSON.toJSONString(role)));
					}
				}
				parentRole.put("childrenSize", childrenJsonArrays.size());
				if (!childrenJsonArrays.isEmpty()) {
					parentRole.put("children", childrenJsonArrays);
				}
			}

		}
		return BaseResult.ok(roleJsonArrays);
	}
}
