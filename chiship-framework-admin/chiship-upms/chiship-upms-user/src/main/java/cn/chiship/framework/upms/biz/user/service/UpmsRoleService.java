package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsRoleExample;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;

/**
 * 角色业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsRoleService extends BaseService<UpmsRole, UpmsRoleExample> {

	/**
	 * 角色树状table
	 * @param keyword
	 * @return
	 */
	BaseResult treeTable(String keyword);

}
