package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsUser;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserRoleExample;

import java.util.List;

/**
 * @author lj 用户与角色关联Mapper
 */
public interface UpmsUserRoleMapper extends BaseMapper<UpmsUserRole, UpmsUserRoleExample> {

	/**
	 * 批量新增记录
	 * @param list
	 * @return
	 */
	int insertSelectiveBatch(List<UpmsUserRole> list);

	/**
	 * 根据角色主键获取用户
	 * @param roleId
	 * @return
	 */
	List<UpmsUser> selectUserByRoleId(String roleId);

}