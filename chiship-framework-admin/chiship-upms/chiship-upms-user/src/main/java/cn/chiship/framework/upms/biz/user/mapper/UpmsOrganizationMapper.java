package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsOrganizationExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsOrganization;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * 组织机构mapper
 *
 * @author lj
 */
public interface UpmsOrganizationMapper extends BaseMapper<UpmsOrganization, UpmsOrganizationExample> {

    /**
     * 根据父级获取最大编码
     *
     * @param pid
     * @return
     */
    Map<String, String> getTreeNumberByPid(String pid);

    /**
     * 更新顶级机构及其子机构的 treeNumber
     *
     * @return
     */
    int updateTreeNumber(@Param("oldPrefix") String oldPrefix, @Param("newPrefix") String newPrefix);

}
