package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.common.enums.LoginTypeEnum;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsUserDto;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsUserPersonalInformationModifyDto;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsUserSmsInviteDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.user.entity.UpmsUser;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserExample;
import cn.chiship.sdk.framework.pojo.dto.UserForgotPasswordDto;
import cn.chiship.sdk.framework.pojo.dto.UserModifyPasswordDto;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.sdk.third.core.common.ThirdOauthLoginDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsUserService extends BaseService<UpmsUser, UpmsUserExample> {

    /**
     * 分页
     *
     * @param pageVo
     * @return
     */
    BaseResult page(PageVo pageVo);

    /**
     * 分页
     *
     * @param pageVo
     * @param paramsMap
     * @return
     */
    BaseResult page(PageVo pageVo, Map<String, Object> paramsMap);

    /**
     * 获取人员角色树状结构
     *
     * @param paramsMap
     * @return
     */
    BaseResult getUserAndRoleTree(Map<String, Object> paramsMap);

    /**
     * 获取人员岗位树状结构
     *
     * @param paramsMap
     * @return
     */
    BaseResult getUserAndPostTree(Map<String, Object> paramsMap);

    /**
     * 获取人员组织树状结构
     *
     * @param paramsMap
     * @return
     */
    BaseResult getUserAndOrgTree(Map<String, Object> paramsMap);

    /**
     * 根据主键集合获取基本用户信息
     *
     * @param ids
     * @return
     */
    BaseResult getUserBaseInfoByIds(String ids);

    /**
     * 获取用户基本信息
     *
     * @param upmsUserExample
     * @return
     */
    BaseResult selectBaseInfo(UpmsUserExample upmsUserExample);

    /**
     * 保存用户
     *
     * @param upmsUserDto
     * @return
     */
    BaseResult saveUser(UpmsUserDto upmsUserDto);

    /**
     * 更新用户
     *
     * @param userId
     * @param upmsUserDto
     * @return
     */
    BaseResult updateUser(String userId, UpmsUserDto upmsUserDto);

    /**
     * 删除用户
     *
     * @param userIds
     * @return
     */
    BaseResult removeUser(List<String> userIds);

    /**
     * 根据手机号获取用户
     *
     * @param mobile
     * @return
     */
    BaseResult getUserByMobile(String mobile);

    /**
     * 登录
     *
     * @param sessionId
     * @param username
     * @param password
     * @param loginIp
     * @param loginTypeEnum
     * @return
     */
    BaseResult login(String sessionId, String username, String password, String loginIp, LoginTypeEnum loginTypeEnum);

    /**
     * 手机号登录
     *
     * @param sessionId
     * @param mobile
     * @param loginIp
     * @param loginTypeEnum
     * @return
     */
    BaseResult mobileLogin(String sessionId, String mobile, String loginIp, LoginTypeEnum loginTypeEnum);

    /**
     * 钉钉授权登录
     *
     * @param sessionId
     * @param authCode
     * @param ip
     * @param loginTypeEnum
     * @return
     */
    BaseResult dingTalkAuthCodeLogin(String sessionId, String authCode, String ip, LoginTypeEnum loginTypeEnum);

    /**
     * 获取三方二维码授权链接
     *
     * @param thirdOauthLoginDto
     * @return
     */
    BaseResult thirdOauth2Auth(ThirdOauthLoginDto thirdOauthLoginDto);

    /**
     * 钉钉OAuth登录授权
     *
     * @param sessionId
     * @param authCode
     * @param ip
     * @return
     */
    BaseResult dingTalkOauth2AuthLogin(String sessionId, String authCode, String ip, LoginTypeEnum loginTypeEnum);


    /**
     * 企业微信OAuth登录授权
     *
     * @param sessionId
     * @param authCode
     * @param ip
     * @return
     */
    BaseResult wxWorkOauth2AuthLogin(String sessionId, String authCode, String ip, LoginTypeEnum loginTypeEnum);

    /**
     * 扫码登录
     *
     * @param sessionId
     * @param username
     * @param loginIp
     * @param loginTypeEnum
     * @return
     */
    BaseResult scanLogin(String sessionId, String username, String loginIp, LoginTypeEnum loginTypeEnum);

    /**
     * 注册
     *
     * @param mobile
     * @param realName
     * @param password
     * @param passwordAgain
     * @return
     */
    BaseResult register(String mobile, String realName, String password, String passwordAgain);

    /**
     * 加入组织
     *
     * @param cacheUserVO
     * @param invitationCode
     * @param realName
     * @return
     */
    BaseResult joinOrg(CacheUserVO cacheUserVO, String invitationCode, String realName);

    /**
     * 退出组织
     *
     * @param cacheUserVO
     * @param organizationId
     * @return
     */
    BaseResult leaveOrg(CacheUserVO cacheUserVO, String organizationId);

    /**
     * 进入企业
     *
     * @param cacheUserVO
     * @param organizationId
     * @return
     */
    BaseResult enterOrg(CacheUserVO cacheUserVO, String organizationId);

    /**
     * 解散企业
     *
     * @param cacheUserVO
     * @param organizationId
     * @return
     */
    BaseResult dissolutionOrg(CacheUserVO cacheUserVO, String organizationId);

    /**
     * 切换部门
     *
     * @param cacheUserVO
     * @param deptId
     * @return
     */
    BaseResult changeDept(CacheUserVO cacheUserVO, String deptId);

    /**
     * 校验用户是否加入组织
     *
     * @param userId
     * @param invitationCode
     * @return
     */
    BaseResult checkJoinOrg(String userId, String invitationCode);

    /**
     * 修改密码
     *
     * @param userId
     * @param modifyPasswordDto
     * @return
     */
    BaseResult modifyPassword(String userId, UserModifyPasswordDto modifyPasswordDto);

    /**
     * 忘记密码
     *
     * @param userForgotPasswordDto
     * @return
     */
    BaseResult forgotPassword(UserForgotPasswordDto userForgotPasswordDto);

    /**
     * 修改头像
     *
     * @param avatar
     * @param cacheUserVO
     * @return
     */
    BaseResult modifyAvatar(String avatar, CacheUserVO cacheUserVO);

    /**
     * 设置用户名
     *
     * @param userName
     * @param id
     * @return
     */
    BaseResult modifyUserName(String userName, String id);

    /**
     * 修改手机号
     *
     * @param newMobile
     * @param cacheUserVO
     * @return
     */
    BaseResult modifyMobile(String newMobile, CacheUserVO cacheUserVO);

    /**
     * 修改邮箱
     *
     * @param newEmail
     * @param cacheUserVO
     * @return
     */
    BaseResult modifyEmail(String newEmail, CacheUserVO cacheUserVO);

    /**
     * 修改个人信息
     *
     * @param personalInformationModifyDto
     * @param userId
     * @return
     */
    BaseResult modifyPersonInfo(String userId, UpmsUserPersonalInformationModifyDto personalInformationModifyDto);

    /**
     * 解锁
     *
     * @param userId
     * @param password
     * @param ip
     * @return
     */
    BaseResult unlock(String userId, String password, String ip);

    /**
     * 初始化密码
     *
     * @param ids
     * @return
     */
    BaseResult initialPassword(List<String> ids);

    /**
     * 锁定与启动
     *
     * @param id
     * @param isLocked
     * @return
     */
    BaseResult changeIsLocked(String id, Boolean isLocked);


    /**
     * 根据用户ID获取权限
     *
     * @param userId
     * @return
     */
    BaseResult getPermission(String userId);


    /**
     * 发布通知公告
     *
     * @param noticeId
     * @param scope
     * @return
     */
    BaseResult publishNotice(String noticeId, String scope);

    /**
     * 短信邀请
     * @param upmsUserSmsInviteDto
     * @return
     */
    BaseResult smsInvite(UpmsUserSmsInviteDto upmsUserSmsInviteDto,CacheUserVO cacheUserVO);

}
