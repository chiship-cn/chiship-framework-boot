package cn.chiship.framework.upms.biz.user.service.impl;

import cn.chiship.framework.common.enums.LoginTypeEnum;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistory;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistoryExample;
import cn.chiship.framework.upms.biz.user.mapper.UpmsUserLoginHistoryMapper;
import cn.chiship.framework.upms.biz.user.service.UpmsUserLoginHistoryService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.useragent.UserAgentUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.ip.AddressUtils;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import cn.chiship.sdk.framework.util.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 用户登录历史记录业务接口实现层 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsUserLoginHistoryServiceImpl extends BaseServiceImpl<UpmsUserLoginHistory, UpmsUserLoginHistoryExample>
		implements UpmsUserLoginHistoryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserLoginHistoryServiceImpl.class);

	@Resource
	UpmsUserLoginHistoryMapper upmsUserLoginHistoryMapper;

	@Override
	public void loginHistory(String userId, String userName, String realName, String remark, byte isSuccess,
			String loginIp, LoginTypeEnum loginTypeEnum) {
		UserAgentUtil userAgent = UserAgentUtil.parseUserAgentString(ServletUtil.getRequest().getHeader("User-Agent"));
		UpmsUserLoginHistory upmsUserLoginHistory = new UpmsUserLoginHistory();
		upmsUserLoginHistory.setId(SnowflakeIdUtil.generateStrId());
		upmsUserLoginHistory.setGmtCreated(System.currentTimeMillis());
		upmsUserLoginHistory.setGmtModified(System.currentTimeMillis());
		upmsUserLoginHistory.setIsDeleted(BaseConstants.NO);
		upmsUserLoginHistory.setUserId(userId);
		upmsUserLoginHistory.setUserName(userName);
		upmsUserLoginHistory.setRealName(realName);
		upmsUserLoginHistory.setRemark(remark);
		upmsUserLoginHistory.setLoginIp(loginIp);
		upmsUserLoginHistory.setIsSuccess(isSuccess);
		upmsUserLoginHistory.setBrowser(userAgent.getBrowser().getName());
		if (!StringUtil.isNull(userAgent.getBrowserVersion())) {
			upmsUserLoginHistory.setBrowserVersion(userAgent.getBrowserVersion().getVersion());
		}
		upmsUserLoginHistory.setOs(userAgent.getOperatingSystem().getName());
		upmsUserLoginHistory.setLoginAddress(AddressUtils.getRealAddressByIp(loginIp));
		upmsUserLoginHistory.setLoginType(loginTypeEnum.getCode());

		upmsUserLoginHistoryMapper.insertSelective(upmsUserLoginHistory);

	}

}
