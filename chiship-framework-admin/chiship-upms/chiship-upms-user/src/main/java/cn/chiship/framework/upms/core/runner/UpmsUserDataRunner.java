package cn.chiship.framework.upms.core.runner;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.upms.biz.base.service.UpmsUserCacheService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化数据
 *
 * @author lijian
 */
@Component
@Order(1)
public class UpmsUserDataRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserDataRunner.class);

    @Resource
    RedisService redisService;
    @Resource
    UpmsUserCacheService upmsCacheService;

    @Override
    public void run(String... args) {
        LOGGER.info("----------------UpmsRunner---------------------");

        /**
         * 在线用户下线
         */
        redisService.removeAll(CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_ONLINE_USER_PREFIX));
        redisService.removeAll(CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_QRCODE_LOGIN_PREFIX));
        upmsCacheService.cacheOrganizations();
        LOGGER.info("缓存组织数据结束！");

        upmsCacheService.cachePosts();
        LOGGER.info("缓存岗位数据结束！");

    }

}
