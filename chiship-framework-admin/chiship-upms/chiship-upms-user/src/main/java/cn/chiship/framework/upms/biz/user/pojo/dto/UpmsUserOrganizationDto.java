package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 用户组织关联表单
 *
 * @author LiJian
 */
@ApiModel(value = "用户组织关联表单")
public class UpmsUserOrganizationDto {

	@ApiModelProperty(value = "组织机构ID")
	@NotNull(message = "组织机构ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 11)
	private String organizationId;

	@ApiModelProperty(value = "用户主键")
	private String userId;

	@ApiModelProperty(value = "电话")
	private String phone;

	@ApiModelProperty(value = "工作")
	private String job;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	private Integer orders;

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getJob() {
		return job;
	}

	public void setJob(String job) {
		this.job = job;
	}

	public Integer getOrders() {
		return orders;
	}

	public void setOrders(Integer orders) {
		this.orders = orders;
	}

}
