package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.user.entity.UpmsPost;
import cn.chiship.framework.upms.biz.user.entity.UpmsPostExample;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 工作岗位业务接口层
 * 2025/2/10
 *
 * @author lijian
 */
public interface UpmsPostService extends BaseService<UpmsPost, UpmsPostExample> {

    /**
     * 组装树结构
     * @param upmsPosts
     * @return
     */
    BaseResult tree(List<UpmsPost>upmsPosts);

    /**
     * 缓存岗位
     */
    void cachePosts();

    /**
     * 根据主键获取工作岗位
     *
     * @param id
     * @param isType 是否加载类型
     * @return
     */
    String getCacheNameById(String id,Boolean isType);

    /**
     * 根据主键获取工作岗位
     *
     * @param id
     * @return
     */
    JSONObject getCacheById(String id);

    /**
     * 改变状态
     *
     * @param id
     * @param status
     * @return
     */
    BaseResult changeStatus(String id, Byte status);
}
