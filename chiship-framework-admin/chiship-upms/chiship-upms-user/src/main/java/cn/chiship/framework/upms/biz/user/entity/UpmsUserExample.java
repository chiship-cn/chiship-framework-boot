package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-11-29
 */
public class UpmsUserExample implements Serializable {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private static final long serialVersionUID = 1L;

    public UpmsUserExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria implements Serializable {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedIsNull() {
            addCriterion("gmt_created is null");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedIsNotNull() {
            addCriterion("gmt_created is not null");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedEqualTo(Long value) {
            addCriterion("gmt_created =", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedNotEqualTo(Long value) {
            addCriterion("gmt_created <>", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedGreaterThan(Long value) {
            addCriterion("gmt_created >", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
            addCriterion("gmt_created >=", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedLessThan(Long value) {
            addCriterion("gmt_created <", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
            addCriterion("gmt_created <=", value, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedIn(List<Long> values) {
            addCriterion("gmt_created in", values, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedNotIn(List<Long> values) {
            addCriterion("gmt_created not in", values, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedBetween(Long value1, Long value2) {
            addCriterion("gmt_created between", value1, value2, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
            addCriterion("gmt_created not between", value1, value2, "gmtCreated");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNull() {
            addCriterion("gmt_modified is null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIsNotNull() {
            addCriterion("gmt_modified is not null");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedEqualTo(Long value) {
            addCriterion("gmt_modified =", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotEqualTo(Long value) {
            addCriterion("gmt_modified <>", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThan(Long value) {
            addCriterion("gmt_modified >", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
            addCriterion("gmt_modified >=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThan(Long value) {
            addCriterion("gmt_modified <", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
            addCriterion("gmt_modified <=", value, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedIn(List<Long> values) {
            addCriterion("gmt_modified in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotIn(List<Long> values) {
            addCriterion("gmt_modified not in", values, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedBetween(Long value1, Long value2) {
            addCriterion("gmt_modified between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
            addCriterion("gmt_modified not between", value1, value2, "gmtModified");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNull() {
            addCriterion("is_deleted is null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIsNotNull() {
            addCriterion("is_deleted is not null");
            return (Criteria) this;
        }

        public Criteria andIsDeletedEqualTo(Byte value) {
            addCriterion("is_deleted =", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotEqualTo(Byte value) {
            addCriterion("is_deleted <>", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThan(Byte value) {
            addCriterion("is_deleted >", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_deleted >=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThan(Byte value) {
            addCriterion("is_deleted <", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
            addCriterion("is_deleted <=", value, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedIn(List<Byte> values) {
            addCriterion("is_deleted in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotIn(List<Byte> values) {
            addCriterion("is_deleted not in", values, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
            addCriterion("is_deleted between", value1, value2, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
            addCriterion("is_deleted not between", value1, value2, "isDeleted");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNull() {
            addCriterion("nick_name is null");
            return (Criteria) this;
        }

        public Criteria andNickNameIsNotNull() {
            addCriterion("nick_name is not null");
            return (Criteria) this;
        }

        public Criteria andNickNameEqualTo(String value) {
            addCriterion("nick_name =", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotEqualTo(String value) {
            addCriterion("nick_name <>", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThan(String value) {
            addCriterion("nick_name >", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameGreaterThanOrEqualTo(String value) {
            addCriterion("nick_name >=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThan(String value) {
            addCriterion("nick_name <", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLessThanOrEqualTo(String value) {
            addCriterion("nick_name <=", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameLike(String value) {
            addCriterion("nick_name like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotLike(String value) {
            addCriterion("nick_name not like", value, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameIn(List<String> values) {
            addCriterion("nick_name in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotIn(List<String> values) {
            addCriterion("nick_name not in", values, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameBetween(String value1, String value2) {
            addCriterion("nick_name between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andNickNameNotBetween(String value1, String value2) {
            addCriterion("nick_name not between", value1, value2, "nickName");
            return (Criteria) this;
        }

        public Criteria andSignatureIsNull() {
            addCriterion("signature is null");
            return (Criteria) this;
        }

        public Criteria andSignatureIsNotNull() {
            addCriterion("signature is not null");
            return (Criteria) this;
        }

        public Criteria andSignatureEqualTo(String value) {
            addCriterion("signature =", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureNotEqualTo(String value) {
            addCriterion("signature <>", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureGreaterThan(String value) {
            addCriterion("signature >", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureGreaterThanOrEqualTo(String value) {
            addCriterion("signature >=", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureLessThan(String value) {
            addCriterion("signature <", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureLessThanOrEqualTo(String value) {
            addCriterion("signature <=", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureLike(String value) {
            addCriterion("signature like", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureNotLike(String value) {
            addCriterion("signature not like", value, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureIn(List<String> values) {
            addCriterion("signature in", values, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureNotIn(List<String> values) {
            addCriterion("signature not in", values, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureBetween(String value1, String value2) {
            addCriterion("signature between", value1, value2, "signature");
            return (Criteria) this;
        }

        public Criteria andSignatureNotBetween(String value1, String value2) {
            addCriterion("signature not between", value1, value2, "signature");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNull() {
            addCriterion("user_name is null");
            return (Criteria) this;
        }

        public Criteria andUserNameIsNotNull() {
            addCriterion("user_name is not null");
            return (Criteria) this;
        }

        public Criteria andUserNameEqualTo(String value) {
            addCriterion("user_name =", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotEqualTo(String value) {
            addCriterion("user_name <>", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThan(String value) {
            addCriterion("user_name >", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameGreaterThanOrEqualTo(String value) {
            addCriterion("user_name >=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThan(String value) {
            addCriterion("user_name <", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLessThanOrEqualTo(String value) {
            addCriterion("user_name <=", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameLike(String value) {
            addCriterion("user_name like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotLike(String value) {
            addCriterion("user_name not like", value, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameIn(List<String> values) {
            addCriterion("user_name in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotIn(List<String> values) {
            addCriterion("user_name not in", values, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameBetween(String value1, String value2) {
            addCriterion("user_name between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andUserNameNotBetween(String value1, String value2) {
            addCriterion("user_name not between", value1, value2, "userName");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNull() {
            addCriterion("password is null");
            return (Criteria) this;
        }

        public Criteria andPasswordIsNotNull() {
            addCriterion("password is not null");
            return (Criteria) this;
        }

        public Criteria andPasswordEqualTo(String value) {
            addCriterion("password =", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotEqualTo(String value) {
            addCriterion("password <>", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThan(String value) {
            addCriterion("password >", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordGreaterThanOrEqualTo(String value) {
            addCriterion("password >=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThan(String value) {
            addCriterion("password <", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLessThanOrEqualTo(String value) {
            addCriterion("password <=", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordLike(String value) {
            addCriterion("password like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotLike(String value) {
            addCriterion("password not like", value, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordIn(List<String> values) {
            addCriterion("password in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotIn(List<String> values) {
            addCriterion("password not in", values, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordBetween(String value1, String value2) {
            addCriterion("password between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andPasswordNotBetween(String value1, String value2) {
            addCriterion("password not between", value1, value2, "password");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordIsNull() {
            addCriterion("is_init_password is null");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordIsNotNull() {
            addCriterion("is_init_password is not null");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordEqualTo(Byte value) {
            addCriterion("is_init_password =", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordNotEqualTo(Byte value) {
            addCriterion("is_init_password <>", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordGreaterThan(Byte value) {
            addCriterion("is_init_password >", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_init_password >=", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordLessThan(Byte value) {
            addCriterion("is_init_password <", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordLessThanOrEqualTo(Byte value) {
            addCriterion("is_init_password <=", value, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordIn(List<Byte> values) {
            addCriterion("is_init_password in", values, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordNotIn(List<Byte> values) {
            addCriterion("is_init_password not in", values, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordBetween(Byte value1, Byte value2) {
            addCriterion("is_init_password between", value1, value2, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andIsInitPasswordNotBetween(Byte value1, Byte value2) {
            addCriterion("is_init_password not between", value1, value2, "isInitPassword");
            return (Criteria) this;
        }

        public Criteria andSaltIsNull() {
            addCriterion("salt is null");
            return (Criteria) this;
        }

        public Criteria andSaltIsNotNull() {
            addCriterion("salt is not null");
            return (Criteria) this;
        }

        public Criteria andSaltEqualTo(String value) {
            addCriterion("salt =", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotEqualTo(String value) {
            addCriterion("salt <>", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThan(String value) {
            addCriterion("salt >", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltGreaterThanOrEqualTo(String value) {
            addCriterion("salt >=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThan(String value) {
            addCriterion("salt <", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLessThanOrEqualTo(String value) {
            addCriterion("salt <=", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltLike(String value) {
            addCriterion("salt like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotLike(String value) {
            addCriterion("salt not like", value, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltIn(List<String> values) {
            addCriterion("salt in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotIn(List<String> values) {
            addCriterion("salt not in", values, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltBetween(String value1, String value2) {
            addCriterion("salt between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andSaltNotBetween(String value1, String value2) {
            addCriterion("salt not between", value1, value2, "salt");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNull() {
            addCriterion("real_name is null");
            return (Criteria) this;
        }

        public Criteria andRealNameIsNotNull() {
            addCriterion("real_name is not null");
            return (Criteria) this;
        }

        public Criteria andRealNameEqualTo(String value) {
            addCriterion("real_name =", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotEqualTo(String value) {
            addCriterion("real_name <>", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThan(String value) {
            addCriterion("real_name >", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameGreaterThanOrEqualTo(String value) {
            addCriterion("real_name >=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThan(String value) {
            addCriterion("real_name <", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLessThanOrEqualTo(String value) {
            addCriterion("real_name <=", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameLike(String value) {
            addCriterion("real_name like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotLike(String value) {
            addCriterion("real_name not like", value, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameIn(List<String> values) {
            addCriterion("real_name in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotIn(List<String> values) {
            addCriterion("real_name not in", values, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameBetween(String value1, String value2) {
            addCriterion("real_name between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andRealNameNotBetween(String value1, String value2) {
            addCriterion("real_name not between", value1, value2, "realName");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListIsNull() {
            addCriterion("is_add_mail_list is null");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListIsNotNull() {
            addCriterion("is_add_mail_list is not null");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListEqualTo(Byte value) {
            addCriterion("is_add_mail_list =", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListNotEqualTo(Byte value) {
            addCriterion("is_add_mail_list <>", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListGreaterThan(Byte value) {
            addCriterion("is_add_mail_list >", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_add_mail_list >=", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListLessThan(Byte value) {
            addCriterion("is_add_mail_list <", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListLessThanOrEqualTo(Byte value) {
            addCriterion("is_add_mail_list <=", value, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListIn(List<Byte> values) {
            addCriterion("is_add_mail_list in", values, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListNotIn(List<Byte> values) {
            addCriterion("is_add_mail_list not in", values, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListBetween(Byte value1, Byte value2) {
            addCriterion("is_add_mail_list between", value1, value2, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andIsAddMailListNotBetween(Byte value1, Byte value2) {
            addCriterion("is_add_mail_list not between", value1, value2, "isAddMailList");
            return (Criteria) this;
        }

        public Criteria andMobileIsNull() {
            addCriterion("mobile is null");
            return (Criteria) this;
        }

        public Criteria andMobileIsNotNull() {
            addCriterion("mobile is not null");
            return (Criteria) this;
        }

        public Criteria andMobileEqualTo(String value) {
            addCriterion("mobile =", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotEqualTo(String value) {
            addCriterion("mobile <>", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThan(String value) {
            addCriterion("mobile >", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileGreaterThanOrEqualTo(String value) {
            addCriterion("mobile >=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThan(String value) {
            addCriterion("mobile <", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLessThanOrEqualTo(String value) {
            addCriterion("mobile <=", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileLike(String value) {
            addCriterion("mobile like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotLike(String value) {
            addCriterion("mobile not like", value, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileIn(List<String> values) {
            addCriterion("mobile in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotIn(List<String> values) {
            addCriterion("mobile not in", values, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileBetween(String value1, String value2) {
            addCriterion("mobile between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andMobileNotBetween(String value1, String value2) {
            addCriterion("mobile not between", value1, value2, "mobile");
            return (Criteria) this;
        }

        public Criteria andAvatarIsNull() {
            addCriterion("avatar is null");
            return (Criteria) this;
        }

        public Criteria andAvatarIsNotNull() {
            addCriterion("avatar is not null");
            return (Criteria) this;
        }

        public Criteria andAvatarEqualTo(String value) {
            addCriterion("avatar =", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarNotEqualTo(String value) {
            addCriterion("avatar <>", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarGreaterThan(String value) {
            addCriterion("avatar >", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarGreaterThanOrEqualTo(String value) {
            addCriterion("avatar >=", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarLessThan(String value) {
            addCriterion("avatar <", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarLessThanOrEqualTo(String value) {
            addCriterion("avatar <=", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarLike(String value) {
            addCriterion("avatar like", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarNotLike(String value) {
            addCriterion("avatar not like", value, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarIn(List<String> values) {
            addCriterion("avatar in", values, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarNotIn(List<String> values) {
            addCriterion("avatar not in", values, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarBetween(String value1, String value2) {
            addCriterion("avatar between", value1, value2, "avatar");
            return (Criteria) this;
        }

        public Criteria andAvatarNotBetween(String value1, String value2) {
            addCriterion("avatar not between", value1, value2, "avatar");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNull() {
            addCriterion("phone is null");
            return (Criteria) this;
        }

        public Criteria andPhoneIsNotNull() {
            addCriterion("phone is not null");
            return (Criteria) this;
        }

        public Criteria andPhoneEqualTo(String value) {
            addCriterion("phone =", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotEqualTo(String value) {
            addCriterion("phone <>", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThan(String value) {
            addCriterion("phone >", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneGreaterThanOrEqualTo(String value) {
            addCriterion("phone >=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThan(String value) {
            addCriterion("phone <", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLessThanOrEqualTo(String value) {
            addCriterion("phone <=", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneLike(String value) {
            addCriterion("phone like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotLike(String value) {
            addCriterion("phone not like", value, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneIn(List<String> values) {
            addCriterion("phone in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotIn(List<String> values) {
            addCriterion("phone not in", values, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneBetween(String value1, String value2) {
            addCriterion("phone between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andPhoneNotBetween(String value1, String value2) {
            addCriterion("phone not between", value1, value2, "phone");
            return (Criteria) this;
        }

        public Criteria andEmailIsNull() {
            addCriterion("email is null");
            return (Criteria) this;
        }

        public Criteria andEmailIsNotNull() {
            addCriterion("email is not null");
            return (Criteria) this;
        }

        public Criteria andEmailEqualTo(String value) {
            addCriterion("email =", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotEqualTo(String value) {
            addCriterion("email <>", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThan(String value) {
            addCriterion("email >", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailGreaterThanOrEqualTo(String value) {
            addCriterion("email >=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThan(String value) {
            addCriterion("email <", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLessThanOrEqualTo(String value) {
            addCriterion("email <=", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailLike(String value) {
            addCriterion("email like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotLike(String value) {
            addCriterion("email not like", value, "email");
            return (Criteria) this;
        }

        public Criteria andEmailIn(List<String> values) {
            addCriterion("email in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotIn(List<String> values) {
            addCriterion("email not in", values, "email");
            return (Criteria) this;
        }

        public Criteria andEmailBetween(String value1, String value2) {
            addCriterion("email between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andEmailNotBetween(String value1, String value2) {
            addCriterion("email not between", value1, value2, "email");
            return (Criteria) this;
        }

        public Criteria andGenderIsNull() {
            addCriterion("gender is null");
            return (Criteria) this;
        }

        public Criteria andGenderIsNotNull() {
            addCriterion("gender is not null");
            return (Criteria) this;
        }

        public Criteria andGenderEqualTo(Byte value) {
            addCriterion("gender =", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotEqualTo(Byte value) {
            addCriterion("gender <>", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThan(Byte value) {
            addCriterion("gender >", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderGreaterThanOrEqualTo(Byte value) {
            addCriterion("gender >=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThan(Byte value) {
            addCriterion("gender <", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderLessThanOrEqualTo(Byte value) {
            addCriterion("gender <=", value, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderIn(List<Byte> values) {
            addCriterion("gender in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotIn(List<Byte> values) {
            addCriterion("gender not in", values, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderBetween(Byte value1, Byte value2) {
            addCriterion("gender between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andGenderNotBetween(Byte value1, Byte value2) {
            addCriterion("gender not between", value1, value2, "gender");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNull() {
            addCriterion("id_number is null");
            return (Criteria) this;
        }

        public Criteria andIdNumberIsNotNull() {
            addCriterion("id_number is not null");
            return (Criteria) this;
        }

        public Criteria andIdNumberEqualTo(String value) {
            addCriterion("id_number =", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotEqualTo(String value) {
            addCriterion("id_number <>", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThan(String value) {
            addCriterion("id_number >", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberGreaterThanOrEqualTo(String value) {
            addCriterion("id_number >=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThan(String value) {
            addCriterion("id_number <", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLessThanOrEqualTo(String value) {
            addCriterion("id_number <=", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberLike(String value) {
            addCriterion("id_number like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotLike(String value) {
            addCriterion("id_number not like", value, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberIn(List<String> values) {
            addCriterion("id_number in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotIn(List<String> values) {
            addCriterion("id_number not in", values, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberBetween(String value1, String value2) {
            addCriterion("id_number between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIdNumberNotBetween(String value1, String value2) {
            addCriterion("id_number not between", value1, value2, "idNumber");
            return (Criteria) this;
        }

        public Criteria andIsLockedIsNull() {
            addCriterion("is_locked is null");
            return (Criteria) this;
        }

        public Criteria andIsLockedIsNotNull() {
            addCriterion("is_locked is not null");
            return (Criteria) this;
        }

        public Criteria andIsLockedEqualTo(Byte value) {
            addCriterion("is_locked =", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedNotEqualTo(Byte value) {
            addCriterion("is_locked <>", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedGreaterThan(Byte value) {
            addCriterion("is_locked >", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_locked >=", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedLessThan(Byte value) {
            addCriterion("is_locked <", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedLessThanOrEqualTo(Byte value) {
            addCriterion("is_locked <=", value, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedIn(List<Byte> values) {
            addCriterion("is_locked in", values, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedNotIn(List<Byte> values) {
            addCriterion("is_locked not in", values, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedBetween(Byte value1, Byte value2) {
            addCriterion("is_locked between", value1, value2, "isLocked");
            return (Criteria) this;
        }

        public Criteria andIsLockedNotBetween(Byte value1, Byte value2) {
            addCriterion("is_locked not between", value1, value2, "isLocked");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNull() {
            addCriterion("user_code is null");
            return (Criteria) this;
        }

        public Criteria andUserCodeIsNotNull() {
            addCriterion("user_code is not null");
            return (Criteria) this;
        }

        public Criteria andUserCodeEqualTo(String value) {
            addCriterion("user_code =", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotEqualTo(String value) {
            addCriterion("user_code <>", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThan(String value) {
            addCriterion("user_code >", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeGreaterThanOrEqualTo(String value) {
            addCriterion("user_code >=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThan(String value) {
            addCriterion("user_code <", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLessThanOrEqualTo(String value) {
            addCriterion("user_code <=", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeLike(String value) {
            addCriterion("user_code like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotLike(String value) {
            addCriterion("user_code not like", value, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeIn(List<String> values) {
            addCriterion("user_code in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotIn(List<String> values) {
            addCriterion("user_code not in", values, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeBetween(String value1, String value2) {
            addCriterion("user_code between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andUserCodeNotBetween(String value1, String value2) {
            addCriterion("user_code not between", value1, value2, "userCode");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdIsNull() {
            addCriterion("ding_talk_user_id is null");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdIsNotNull() {
            addCriterion("ding_talk_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdEqualTo(String value) {
            addCriterion("ding_talk_user_id =", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdNotEqualTo(String value) {
            addCriterion("ding_talk_user_id <>", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdGreaterThan(String value) {
            addCriterion("ding_talk_user_id >", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("ding_talk_user_id >=", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdLessThan(String value) {
            addCriterion("ding_talk_user_id <", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdLessThanOrEqualTo(String value) {
            addCriterion("ding_talk_user_id <=", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdLike(String value) {
            addCriterion("ding_talk_user_id like", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdNotLike(String value) {
            addCriterion("ding_talk_user_id not like", value, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdIn(List<String> values) {
            addCriterion("ding_talk_user_id in", values, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdNotIn(List<String> values) {
            addCriterion("ding_talk_user_id not in", values, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdBetween(String value1, String value2) {
            addCriterion("ding_talk_user_id between", value1, value2, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andDingTalkUserIdNotBetween(String value1, String value2) {
            addCriterion("ding_talk_user_id not between", value1, value2, "dingTalkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdIsNull() {
            addCriterion("wx_work_user_id is null");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdIsNotNull() {
            addCriterion("wx_work_user_id is not null");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdEqualTo(String value) {
            addCriterion("wx_work_user_id =", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdNotEqualTo(String value) {
            addCriterion("wx_work_user_id <>", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdGreaterThan(String value) {
            addCriterion("wx_work_user_id >", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdGreaterThanOrEqualTo(String value) {
            addCriterion("wx_work_user_id >=", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdLessThan(String value) {
            addCriterion("wx_work_user_id <", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdLessThanOrEqualTo(String value) {
            addCriterion("wx_work_user_id <=", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdLike(String value) {
            addCriterion("wx_work_user_id like", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdNotLike(String value) {
            addCriterion("wx_work_user_id not like", value, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdIn(List<String> values) {
            addCriterion("wx_work_user_id in", values, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdNotIn(List<String> values) {
            addCriterion("wx_work_user_id not in", values, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdBetween(String value1, String value2) {
            addCriterion("wx_work_user_id between", value1, value2, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andWxWorkUserIdNotBetween(String value1, String value2) {
            addCriterion("wx_work_user_id not between", value1, value2, "wxWorkUserId");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNull() {
            addCriterion("category is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNotNull() {
            addCriterion("category is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryEqualTo(String value) {
            addCriterion("category =", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotEqualTo(String value) {
            addCriterion("category <>", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThan(String value) {
            addCriterion("category >", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("category >=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThan(String value) {
            addCriterion("category <", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThanOrEqualTo(String value) {
            addCriterion("category <=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLike(String value) {
            addCriterion("category like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotLike(String value) {
            addCriterion("category not like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryIn(List<String> values) {
            addCriterion("category in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotIn(List<String> values) {
            addCriterion("category not in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryBetween(String value1, String value2) {
            addCriterion("category between", value1, value2, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotBetween(String value1, String value2) {
            addCriterion("category not between", value1, value2, "category");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIsNull() {
            addCriterion("user_pinyin is null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIsNotNull() {
            addCriterion("user_pinyin is not null");
            return (Criteria) this;
        }

        public Criteria andUserPinyinEqualTo(String value) {
            addCriterion("user_pinyin =", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotEqualTo(String value) {
            addCriterion("user_pinyin <>", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinGreaterThan(String value) {
            addCriterion("user_pinyin >", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinGreaterThanOrEqualTo(String value) {
            addCriterion("user_pinyin >=", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLessThan(String value) {
            addCriterion("user_pinyin <", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLessThanOrEqualTo(String value) {
            addCriterion("user_pinyin <=", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinLike(String value) {
            addCriterion("user_pinyin like", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotLike(String value) {
            addCriterion("user_pinyin not like", value, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinIn(List<String> values) {
            addCriterion("user_pinyin in", values, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotIn(List<String> values) {
            addCriterion("user_pinyin not in", values, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinBetween(String value1, String value2) {
            addCriterion("user_pinyin between", value1, value2, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andUserPinyinNotBetween(String value1, String value2) {
            addCriterion("user_pinyin not between", value1, value2, "userPinyin");
            return (Criteria) this;
        }

        public Criteria andQqIsNull() {
            addCriterion("qq is null");
            return (Criteria) this;
        }

        public Criteria andQqIsNotNull() {
            addCriterion("qq is not null");
            return (Criteria) this;
        }

        public Criteria andQqEqualTo(String value) {
            addCriterion("qq =", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotEqualTo(String value) {
            addCriterion("qq <>", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThan(String value) {
            addCriterion("qq >", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqGreaterThanOrEqualTo(String value) {
            addCriterion("qq >=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThan(String value) {
            addCriterion("qq <", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLessThanOrEqualTo(String value) {
            addCriterion("qq <=", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqLike(String value) {
            addCriterion("qq like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotLike(String value) {
            addCriterion("qq not like", value, "qq");
            return (Criteria) this;
        }

        public Criteria andQqIn(List<String> values) {
            addCriterion("qq in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotIn(List<String> values) {
            addCriterion("qq not in", values, "qq");
            return (Criteria) this;
        }

        public Criteria andQqBetween(String value1, String value2) {
            addCriterion("qq between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andQqNotBetween(String value1, String value2) {
            addCriterion("qq not between", value1, value2, "qq");
            return (Criteria) this;
        }

        public Criteria andWeixinIsNull() {
            addCriterion("weixin is null");
            return (Criteria) this;
        }

        public Criteria andWeixinIsNotNull() {
            addCriterion("weixin is not null");
            return (Criteria) this;
        }

        public Criteria andWeixinEqualTo(String value) {
            addCriterion("weixin =", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinNotEqualTo(String value) {
            addCriterion("weixin <>", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinGreaterThan(String value) {
            addCriterion("weixin >", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinGreaterThanOrEqualTo(String value) {
            addCriterion("weixin >=", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinLessThan(String value) {
            addCriterion("weixin <", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinLessThanOrEqualTo(String value) {
            addCriterion("weixin <=", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinLike(String value) {
            addCriterion("weixin like", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinNotLike(String value) {
            addCriterion("weixin not like", value, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinIn(List<String> values) {
            addCriterion("weixin in", values, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinNotIn(List<String> values) {
            addCriterion("weixin not in", values, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinBetween(String value1, String value2) {
            addCriterion("weixin between", value1, value2, "weixin");
            return (Criteria) this;
        }

        public Criteria andWeixinNotBetween(String value1, String value2) {
            addCriterion("weixin not between", value1, value2, "weixin");
            return (Criteria) this;
        }

        public Criteria andIsInnerIsNull() {
            addCriterion("is_inner is null");
            return (Criteria) this;
        }

        public Criteria andIsInnerIsNotNull() {
            addCriterion("is_inner is not null");
            return (Criteria) this;
        }

        public Criteria andIsInnerEqualTo(Byte value) {
            addCriterion("is_inner =", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerNotEqualTo(Byte value) {
            addCriterion("is_inner <>", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerGreaterThan(Byte value) {
            addCriterion("is_inner >", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerGreaterThanOrEqualTo(Byte value) {
            addCriterion("is_inner >=", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerLessThan(Byte value) {
            addCriterion("is_inner <", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerLessThanOrEqualTo(Byte value) {
            addCriterion("is_inner <=", value, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerIn(List<Byte> values) {
            addCriterion("is_inner in", values, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerNotIn(List<Byte> values) {
            addCriterion("is_inner not in", values, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerBetween(Byte value1, Byte value2) {
            addCriterion("is_inner between", value1, value2, "isInner");
            return (Criteria) this;
        }

        public Criteria andIsInnerNotBetween(Byte value1, Byte value2) {
            addCriterion("is_inner not between", value1, value2, "isInner");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNull() {
            addCriterion("introduction is null");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNotNull() {
            addCriterion("introduction is not null");
            return (Criteria) this;
        }

        public Criteria andIntroductionEqualTo(String value) {
            addCriterion("introduction =", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotEqualTo(String value) {
            addCriterion("introduction <>", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThan(String value) {
            addCriterion("introduction >", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThanOrEqualTo(String value) {
            addCriterion("introduction >=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThan(String value) {
            addCriterion("introduction <", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThanOrEqualTo(String value) {
            addCriterion("introduction <=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLike(String value) {
            addCriterion("introduction like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotLike(String value) {
            addCriterion("introduction not like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionIn(List<String> values) {
            addCriterion("introduction in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotIn(List<String> values) {
            addCriterion("introduction not in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionBetween(String value1, String value2) {
            addCriterion("introduction between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotBetween(String value1, String value2) {
            addCriterion("introduction not between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andSkillIsNull() {
            addCriterion("skill is null");
            return (Criteria) this;
        }

        public Criteria andSkillIsNotNull() {
            addCriterion("skill is not null");
            return (Criteria) this;
        }

        public Criteria andSkillEqualTo(String value) {
            addCriterion("skill =", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillNotEqualTo(String value) {
            addCriterion("skill <>", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillGreaterThan(String value) {
            addCriterion("skill >", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillGreaterThanOrEqualTo(String value) {
            addCriterion("skill >=", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillLessThan(String value) {
            addCriterion("skill <", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillLessThanOrEqualTo(String value) {
            addCriterion("skill <=", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillLike(String value) {
            addCriterion("skill like", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillNotLike(String value) {
            addCriterion("skill not like", value, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillIn(List<String> values) {
            addCriterion("skill in", values, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillNotIn(List<String> values) {
            addCriterion("skill not in", values, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillBetween(String value1, String value2) {
            addCriterion("skill between", value1, value2, "skill");
            return (Criteria) this;
        }

        public Criteria andSkillNotBetween(String value1, String value2) {
            addCriterion("skill not between", value1, value2, "skill");
            return (Criteria) this;
        }

        public Criteria andTagIsNull() {
            addCriterion("tag is null");
            return (Criteria) this;
        }

        public Criteria andTagIsNotNull() {
            addCriterion("tag is not null");
            return (Criteria) this;
        }

        public Criteria andTagEqualTo(String value) {
            addCriterion("tag =", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagNotEqualTo(String value) {
            addCriterion("tag <>", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagGreaterThan(String value) {
            addCriterion("tag >", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagGreaterThanOrEqualTo(String value) {
            addCriterion("tag >=", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagLessThan(String value) {
            addCriterion("tag <", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagLessThanOrEqualTo(String value) {
            addCriterion("tag <=", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagLike(String value) {
            addCriterion("tag like", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagNotLike(String value) {
            addCriterion("tag not like", value, "tag");
            return (Criteria) this;
        }

        public Criteria andTagIn(List<String> values) {
            addCriterion("tag in", values, "tag");
            return (Criteria) this;
        }

        public Criteria andTagNotIn(List<String> values) {
            addCriterion("tag not in", values, "tag");
            return (Criteria) this;
        }

        public Criteria andTagBetween(String value1, String value2) {
            addCriterion("tag between", value1, value2, "tag");
            return (Criteria) this;
        }

        public Criteria andTagNotBetween(String value1, String value2) {
            addCriterion("tag not between", value1, value2, "tag");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeIsNull() {
            addCriterion("authorization_code is null");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeIsNotNull() {
            addCriterion("authorization_code is not null");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeEqualTo(String value) {
            addCriterion("authorization_code =", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeNotEqualTo(String value) {
            addCriterion("authorization_code <>", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeGreaterThan(String value) {
            addCriterion("authorization_code >", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeGreaterThanOrEqualTo(String value) {
            addCriterion("authorization_code >=", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeLessThan(String value) {
            addCriterion("authorization_code <", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeLessThanOrEqualTo(String value) {
            addCriterion("authorization_code <=", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeLike(String value) {
            addCriterion("authorization_code like", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeNotLike(String value) {
            addCriterion("authorization_code not like", value, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeIn(List<String> values) {
            addCriterion("authorization_code in", values, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeNotIn(List<String> values) {
            addCriterion("authorization_code not in", values, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeBetween(String value1, String value2) {
            addCriterion("authorization_code between", value1, value2, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andAuthorizationCodeNotBetween(String value1, String value2) {
            addCriterion("authorization_code not between", value1, value2, "authorizationCode");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1IsNull() {
            addCriterion("native_place_level1 is null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1IsNotNull() {
            addCriterion("native_place_level1 is not null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1EqualTo(Long value) {
            addCriterion("native_place_level1 =", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1NotEqualTo(Long value) {
            addCriterion("native_place_level1 <>", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1GreaterThan(Long value) {
            addCriterion("native_place_level1 >", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1GreaterThanOrEqualTo(Long value) {
            addCriterion("native_place_level1 >=", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1LessThan(Long value) {
            addCriterion("native_place_level1 <", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1LessThanOrEqualTo(Long value) {
            addCriterion("native_place_level1 <=", value, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1In(List<Long> values) {
            addCriterion("native_place_level1 in", values, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1NotIn(List<Long> values) {
            addCriterion("native_place_level1 not in", values, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1Between(Long value1, Long value2) {
            addCriterion("native_place_level1 between", value1, value2, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel1NotBetween(Long value1, Long value2) {
            addCriterion("native_place_level1 not between", value1, value2, "nativePlaceLevel1");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2IsNull() {
            addCriterion("native_place_level2 is null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2IsNotNull() {
            addCriterion("native_place_level2 is not null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2EqualTo(Long value) {
            addCriterion("native_place_level2 =", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2NotEqualTo(Long value) {
            addCriterion("native_place_level2 <>", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2GreaterThan(Long value) {
            addCriterion("native_place_level2 >", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2GreaterThanOrEqualTo(Long value) {
            addCriterion("native_place_level2 >=", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2LessThan(Long value) {
            addCriterion("native_place_level2 <", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2LessThanOrEqualTo(Long value) {
            addCriterion("native_place_level2 <=", value, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2In(List<Long> values) {
            addCriterion("native_place_level2 in", values, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2NotIn(List<Long> values) {
            addCriterion("native_place_level2 not in", values, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2Between(Long value1, Long value2) {
            addCriterion("native_place_level2 between", value1, value2, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel2NotBetween(Long value1, Long value2) {
            addCriterion("native_place_level2 not between", value1, value2, "nativePlaceLevel2");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3IsNull() {
            addCriterion("native_place_level3 is null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3IsNotNull() {
            addCriterion("native_place_level3 is not null");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3EqualTo(Long value) {
            addCriterion("native_place_level3 =", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3NotEqualTo(Long value) {
            addCriterion("native_place_level3 <>", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3GreaterThan(Long value) {
            addCriterion("native_place_level3 >", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3GreaterThanOrEqualTo(Long value) {
            addCriterion("native_place_level3 >=", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3LessThan(Long value) {
            addCriterion("native_place_level3 <", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3LessThanOrEqualTo(Long value) {
            addCriterion("native_place_level3 <=", value, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3In(List<Long> values) {
            addCriterion("native_place_level3 in", values, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3NotIn(List<Long> values) {
            addCriterion("native_place_level3 not in", values, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3Between(Long value1, Long value2) {
            addCriterion("native_place_level3 between", value1, value2, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andNativePlaceLevel3NotBetween(Long value1, Long value2) {
            addCriterion("native_place_level3 not between", value1, value2, "nativePlaceLevel3");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeIsNull() {
            addCriterion("education_degree is null");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeIsNotNull() {
            addCriterion("education_degree is not null");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeEqualTo(String value) {
            addCriterion("education_degree =", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeNotEqualTo(String value) {
            addCriterion("education_degree <>", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeGreaterThan(String value) {
            addCriterion("education_degree >", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeGreaterThanOrEqualTo(String value) {
            addCriterion("education_degree >=", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeLessThan(String value) {
            addCriterion("education_degree <", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeLessThanOrEqualTo(String value) {
            addCriterion("education_degree <=", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeLike(String value) {
            addCriterion("education_degree like", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeNotLike(String value) {
            addCriterion("education_degree not like", value, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeIn(List<String> values) {
            addCriterion("education_degree in", values, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeNotIn(List<String> values) {
            addCriterion("education_degree not in", values, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeBetween(String value1, String value2) {
            addCriterion("education_degree between", value1, value2, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andEducationDegreeNotBetween(String value1, String value2) {
            addCriterion("education_degree not between", value1, value2, "educationDegree");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookIsNull() {
            addCriterion("political_outlook is null");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookIsNotNull() {
            addCriterion("political_outlook is not null");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookEqualTo(String value) {
            addCriterion("political_outlook =", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookNotEqualTo(String value) {
            addCriterion("political_outlook <>", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookGreaterThan(String value) {
            addCriterion("political_outlook >", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookGreaterThanOrEqualTo(String value) {
            addCriterion("political_outlook >=", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookLessThan(String value) {
            addCriterion("political_outlook <", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookLessThanOrEqualTo(String value) {
            addCriterion("political_outlook <=", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookLike(String value) {
            addCriterion("political_outlook like", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookNotLike(String value) {
            addCriterion("political_outlook not like", value, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookIn(List<String> values) {
            addCriterion("political_outlook in", values, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookNotIn(List<String> values) {
            addCriterion("political_outlook not in", values, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookBetween(String value1, String value2) {
            addCriterion("political_outlook between", value1, value2, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andPoliticalOutlookNotBetween(String value1, String value2) {
            addCriterion("political_outlook not between", value1, value2, "politicalOutlook");
            return (Criteria) this;
        }

        public Criteria andNationIsNull() {
            addCriterion("nation is null");
            return (Criteria) this;
        }

        public Criteria andNationIsNotNull() {
            addCriterion("nation is not null");
            return (Criteria) this;
        }

        public Criteria andNationEqualTo(String value) {
            addCriterion("nation =", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotEqualTo(String value) {
            addCriterion("nation <>", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationGreaterThan(String value) {
            addCriterion("nation >", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationGreaterThanOrEqualTo(String value) {
            addCriterion("nation >=", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLessThan(String value) {
            addCriterion("nation <", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLessThanOrEqualTo(String value) {
            addCriterion("nation <=", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationLike(String value) {
            addCriterion("nation like", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotLike(String value) {
            addCriterion("nation not like", value, "nation");
            return (Criteria) this;
        }

        public Criteria andNationIn(List<String> values) {
            addCriterion("nation in", values, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotIn(List<String> values) {
            addCriterion("nation not in", values, "nation");
            return (Criteria) this;
        }

        public Criteria andNationBetween(String value1, String value2) {
            addCriterion("nation between", value1, value2, "nation");
            return (Criteria) this;
        }

        public Criteria andNationNotBetween(String value1, String value2) {
            addCriterion("nation not between", value1, value2, "nation");
            return (Criteria) this;
        }

        public Criteria andEducationIsNull() {
            addCriterion("education is null");
            return (Criteria) this;
        }

        public Criteria andEducationIsNotNull() {
            addCriterion("education is not null");
            return (Criteria) this;
        }

        public Criteria andEducationEqualTo(String value) {
            addCriterion("education =", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotEqualTo(String value) {
            addCriterion("education <>", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThan(String value) {
            addCriterion("education >", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationGreaterThanOrEqualTo(String value) {
            addCriterion("education >=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThan(String value) {
            addCriterion("education <", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLessThanOrEqualTo(String value) {
            addCriterion("education <=", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationLike(String value) {
            addCriterion("education like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotLike(String value) {
            addCriterion("education not like", value, "education");
            return (Criteria) this;
        }

        public Criteria andEducationIn(List<String> values) {
            addCriterion("education in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotIn(List<String> values) {
            addCriterion("education not in", values, "education");
            return (Criteria) this;
        }

        public Criteria andEducationBetween(String value1, String value2) {
            addCriterion("education between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andEducationNotBetween(String value1, String value2) {
            addCriterion("education not between", value1, value2, "education");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIsNull() {
            addCriterion("marital_status is null");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIsNotNull() {
            addCriterion("marital_status is not null");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusEqualTo(String value) {
            addCriterion("marital_status =", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotEqualTo(String value) {
            addCriterion("marital_status <>", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusGreaterThan(String value) {
            addCriterion("marital_status >", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusGreaterThanOrEqualTo(String value) {
            addCriterion("marital_status >=", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLessThan(String value) {
            addCriterion("marital_status <", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLessThanOrEqualTo(String value) {
            addCriterion("marital_status <=", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusLike(String value) {
            addCriterion("marital_status like", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotLike(String value) {
            addCriterion("marital_status not like", value, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusIn(List<String> values) {
            addCriterion("marital_status in", values, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotIn(List<String> values) {
            addCriterion("marital_status not in", values, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusBetween(String value1, String value2) {
            addCriterion("marital_status between", value1, value2, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andMaritalStatusNotBetween(String value1, String value2) {
            addCriterion("marital_status not between", value1, value2, "maritalStatus");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolIsNull() {
            addCriterion("graduate_school is null");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolIsNotNull() {
            addCriterion("graduate_school is not null");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolEqualTo(String value) {
            addCriterion("graduate_school =", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolNotEqualTo(String value) {
            addCriterion("graduate_school <>", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolGreaterThan(String value) {
            addCriterion("graduate_school >", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolGreaterThanOrEqualTo(String value) {
            addCriterion("graduate_school >=", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolLessThan(String value) {
            addCriterion("graduate_school <", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolLessThanOrEqualTo(String value) {
            addCriterion("graduate_school <=", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolLike(String value) {
            addCriterion("graduate_school like", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolNotLike(String value) {
            addCriterion("graduate_school not like", value, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolIn(List<String> values) {
            addCriterion("graduate_school in", values, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolNotIn(List<String> values) {
            addCriterion("graduate_school not in", values, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolBetween(String value1, String value2) {
            addCriterion("graduate_school between", value1, value2, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andGraduateSchoolNotBetween(String value1, String value2) {
            addCriterion("graduate_school not between", value1, value2, "graduateSchool");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeIsNull() {
            addCriterion("highest_degree is null");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeIsNotNull() {
            addCriterion("highest_degree is not null");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeEqualTo(String value) {
            addCriterion("highest_degree =", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeNotEqualTo(String value) {
            addCriterion("highest_degree <>", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeGreaterThan(String value) {
            addCriterion("highest_degree >", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeGreaterThanOrEqualTo(String value) {
            addCriterion("highest_degree >=", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeLessThan(String value) {
            addCriterion("highest_degree <", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeLessThanOrEqualTo(String value) {
            addCriterion("highest_degree <=", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeLike(String value) {
            addCriterion("highest_degree like", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeNotLike(String value) {
            addCriterion("highest_degree not like", value, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeIn(List<String> values) {
            addCriterion("highest_degree in", values, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeNotIn(List<String> values) {
            addCriterion("highest_degree not in", values, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeBetween(String value1, String value2) {
            addCriterion("highest_degree between", value1, value2, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andHighestDegreeNotBetween(String value1, String value2) {
            addCriterion("highest_degree not between", value1, value2, "highestDegree");
            return (Criteria) this;
        }

        public Criteria andMajorIsNull() {
            addCriterion("major is null");
            return (Criteria) this;
        }

        public Criteria andMajorIsNotNull() {
            addCriterion("major is not null");
            return (Criteria) this;
        }

        public Criteria andMajorEqualTo(String value) {
            addCriterion("major =", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotEqualTo(String value) {
            addCriterion("major <>", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThan(String value) {
            addCriterion("major >", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorGreaterThanOrEqualTo(String value) {
            addCriterion("major >=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThan(String value) {
            addCriterion("major <", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLessThanOrEqualTo(String value) {
            addCriterion("major <=", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorLike(String value) {
            addCriterion("major like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotLike(String value) {
            addCriterion("major not like", value, "major");
            return (Criteria) this;
        }

        public Criteria andMajorIn(List<String> values) {
            addCriterion("major in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotIn(List<String> values) {
            addCriterion("major not in", values, "major");
            return (Criteria) this;
        }

        public Criteria andMajorBetween(String value1, String value2) {
            addCriterion("major between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andMajorNotBetween(String value1, String value2) {
            addCriterion("major not between", value1, value2, "major");
            return (Criteria) this;
        }

        public Criteria andStaffingIsNull() {
            addCriterion("staffing is null");
            return (Criteria) this;
        }

        public Criteria andStaffingIsNotNull() {
            addCriterion("staffing is not null");
            return (Criteria) this;
        }

        public Criteria andStaffingEqualTo(String value) {
            addCriterion("staffing =", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingNotEqualTo(String value) {
            addCriterion("staffing <>", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingGreaterThan(String value) {
            addCriterion("staffing >", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingGreaterThanOrEqualTo(String value) {
            addCriterion("staffing >=", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingLessThan(String value) {
            addCriterion("staffing <", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingLessThanOrEqualTo(String value) {
            addCriterion("staffing <=", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingLike(String value) {
            addCriterion("staffing like", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingNotLike(String value) {
            addCriterion("staffing not like", value, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingIn(List<String> values) {
            addCriterion("staffing in", values, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingNotIn(List<String> values) {
            addCriterion("staffing not in", values, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingBetween(String value1, String value2) {
            addCriterion("staffing between", value1, value2, "staffing");
            return (Criteria) this;
        }

        public Criteria andStaffingNotBetween(String value1, String value2) {
            addCriterion("staffing not between", value1, value2, "staffing");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateIsNull() {
            addCriterion("start_work_date is null");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateIsNotNull() {
            addCriterion("start_work_date is not null");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateEqualTo(Long value) {
            addCriterion("start_work_date =", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateNotEqualTo(Long value) {
            addCriterion("start_work_date <>", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateGreaterThan(Long value) {
            addCriterion("start_work_date >", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateGreaterThanOrEqualTo(Long value) {
            addCriterion("start_work_date >=", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateLessThan(Long value) {
            addCriterion("start_work_date <", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateLessThanOrEqualTo(Long value) {
            addCriterion("start_work_date <=", value, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateIn(List<Long> values) {
            addCriterion("start_work_date in", values, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateNotIn(List<Long> values) {
            addCriterion("start_work_date not in", values, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateBetween(Long value1, Long value2) {
            addCriterion("start_work_date between", value1, value2, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andStartWorkDateNotBetween(Long value1, Long value2) {
            addCriterion("start_work_date not between", value1, value2, "startWorkDate");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1IsNull() {
            addCriterion("current_residence_level1 is null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1IsNotNull() {
            addCriterion("current_residence_level1 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1EqualTo(Long value) {
            addCriterion("current_residence_level1 =", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1NotEqualTo(Long value) {
            addCriterion("current_residence_level1 <>", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1GreaterThan(Long value) {
            addCriterion("current_residence_level1 >", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1GreaterThanOrEqualTo(Long value) {
            addCriterion("current_residence_level1 >=", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1LessThan(Long value) {
            addCriterion("current_residence_level1 <", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1LessThanOrEqualTo(Long value) {
            addCriterion("current_residence_level1 <=", value, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1In(List<Long> values) {
            addCriterion("current_residence_level1 in", values, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1NotIn(List<Long> values) {
            addCriterion("current_residence_level1 not in", values, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1Between(Long value1, Long value2) {
            addCriterion("current_residence_level1 between", value1, value2, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel1NotBetween(Long value1, Long value2) {
            addCriterion("current_residence_level1 not between", value1, value2, "currentResidenceLevel1");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2IsNull() {
            addCriterion("current_residence_level2 is null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2IsNotNull() {
            addCriterion("current_residence_level2 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2EqualTo(Long value) {
            addCriterion("current_residence_level2 =", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2NotEqualTo(Long value) {
            addCriterion("current_residence_level2 <>", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2GreaterThan(Long value) {
            addCriterion("current_residence_level2 >", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2GreaterThanOrEqualTo(Long value) {
            addCriterion("current_residence_level2 >=", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2LessThan(Long value) {
            addCriterion("current_residence_level2 <", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2LessThanOrEqualTo(Long value) {
            addCriterion("current_residence_level2 <=", value, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2In(List<Long> values) {
            addCriterion("current_residence_level2 in", values, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2NotIn(List<Long> values) {
            addCriterion("current_residence_level2 not in", values, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2Between(Long value1, Long value2) {
            addCriterion("current_residence_level2 between", value1, value2, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel2NotBetween(Long value1, Long value2) {
            addCriterion("current_residence_level2 not between", value1, value2, "currentResidenceLevel2");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3IsNull() {
            addCriterion("current_residence_level3 is null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3IsNotNull() {
            addCriterion("current_residence_level3 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3EqualTo(Long value) {
            addCriterion("current_residence_level3 =", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3NotEqualTo(Long value) {
            addCriterion("current_residence_level3 <>", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3GreaterThan(Long value) {
            addCriterion("current_residence_level3 >", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3GreaterThanOrEqualTo(Long value) {
            addCriterion("current_residence_level3 >=", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3LessThan(Long value) {
            addCriterion("current_residence_level3 <", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3LessThanOrEqualTo(Long value) {
            addCriterion("current_residence_level3 <=", value, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3In(List<Long> values) {
            addCriterion("current_residence_level3 in", values, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3NotIn(List<Long> values) {
            addCriterion("current_residence_level3 not in", values, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3Between(Long value1, Long value2) {
            addCriterion("current_residence_level3 between", value1, value2, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel3NotBetween(Long value1, Long value2) {
            addCriterion("current_residence_level3 not between", value1, value2, "currentResidenceLevel3");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4IsNull() {
            addCriterion("current_residence_level4 is null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4IsNotNull() {
            addCriterion("current_residence_level4 is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4EqualTo(Long value) {
            addCriterion("current_residence_level4 =", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4NotEqualTo(Long value) {
            addCriterion("current_residence_level4 <>", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4GreaterThan(Long value) {
            addCriterion("current_residence_level4 >", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4GreaterThanOrEqualTo(Long value) {
            addCriterion("current_residence_level4 >=", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4LessThan(Long value) {
            addCriterion("current_residence_level4 <", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4LessThanOrEqualTo(Long value) {
            addCriterion("current_residence_level4 <=", value, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4In(List<Long> values) {
            addCriterion("current_residence_level4 in", values, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4NotIn(List<Long> values) {
            addCriterion("current_residence_level4 not in", values, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4Between(Long value1, Long value2) {
            addCriterion("current_residence_level4 between", value1, value2, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentResidenceLevel4NotBetween(Long value1, Long value2) {
            addCriterion("current_residence_level4 not between", value1, value2, "currentResidenceLevel4");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressIsNull() {
            addCriterion("current_address is null");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressIsNotNull() {
            addCriterion("current_address is not null");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressEqualTo(String value) {
            addCriterion("current_address =", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressNotEqualTo(String value) {
            addCriterion("current_address <>", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressGreaterThan(String value) {
            addCriterion("current_address >", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressGreaterThanOrEqualTo(String value) {
            addCriterion("current_address >=", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressLessThan(String value) {
            addCriterion("current_address <", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressLessThanOrEqualTo(String value) {
            addCriterion("current_address <=", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressLike(String value) {
            addCriterion("current_address like", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressNotLike(String value) {
            addCriterion("current_address not like", value, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressIn(List<String> values) {
            addCriterion("current_address in", values, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressNotIn(List<String> values) {
            addCriterion("current_address not in", values, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressBetween(String value1, String value2) {
            addCriterion("current_address between", value1, value2, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andCurrentAddressNotBetween(String value1, String value2) {
            addCriterion("current_address not between", value1, value2, "currentAddress");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactIsNull() {
            addCriterion("emergency_contact is null");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactIsNotNull() {
            addCriterion("emergency_contact is not null");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactEqualTo(String value) {
            addCriterion("emergency_contact =", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactNotEqualTo(String value) {
            addCriterion("emergency_contact <>", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactGreaterThan(String value) {
            addCriterion("emergency_contact >", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactGreaterThanOrEqualTo(String value) {
            addCriterion("emergency_contact >=", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactLessThan(String value) {
            addCriterion("emergency_contact <", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactLessThanOrEqualTo(String value) {
            addCriterion("emergency_contact <=", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactLike(String value) {
            addCriterion("emergency_contact like", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactNotLike(String value) {
            addCriterion("emergency_contact not like", value, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactIn(List<String> values) {
            addCriterion("emergency_contact in", values, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactNotIn(List<String> values) {
            addCriterion("emergency_contact not in", values, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactBetween(String value1, String value2) {
            addCriterion("emergency_contact between", value1, value2, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactNotBetween(String value1, String value2) {
            addCriterion("emergency_contact not between", value1, value2, "emergencyContact");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileIsNull() {
            addCriterion("emergency_contact_mobile is null");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileIsNotNull() {
            addCriterion("emergency_contact_mobile is not null");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileEqualTo(String value) {
            addCriterion("emergency_contact_mobile =", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileNotEqualTo(String value) {
            addCriterion("emergency_contact_mobile <>", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileGreaterThan(String value) {
            addCriterion("emergency_contact_mobile >", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileGreaterThanOrEqualTo(String value) {
            addCriterion("emergency_contact_mobile >=", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileLessThan(String value) {
            addCriterion("emergency_contact_mobile <", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileLessThanOrEqualTo(String value) {
            addCriterion("emergency_contact_mobile <=", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileLike(String value) {
            addCriterion("emergency_contact_mobile like", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileNotLike(String value) {
            addCriterion("emergency_contact_mobile not like", value, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileIn(List<String> values) {
            addCriterion("emergency_contact_mobile in", values, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileNotIn(List<String> values) {
            addCriterion("emergency_contact_mobile not in", values, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileBetween(String value1, String value2) {
            addCriterion("emergency_contact_mobile between", value1, value2, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andEmergencyContactMobileNotBetween(String value1, String value2) {
            addCriterion("emergency_contact_mobile not between", value1, value2, "emergencyContactMobile");
            return (Criteria) this;
        }

        public Criteria andReportsToIsNull() {
            addCriterion("reports_to is null");
            return (Criteria) this;
        }

        public Criteria andReportsToIsNotNull() {
            addCriterion("reports_to is not null");
            return (Criteria) this;
        }

        public Criteria andReportsToEqualTo(String value) {
            addCriterion("reports_to =", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToNotEqualTo(String value) {
            addCriterion("reports_to <>", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToGreaterThan(String value) {
            addCriterion("reports_to >", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToGreaterThanOrEqualTo(String value) {
            addCriterion("reports_to >=", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToLessThan(String value) {
            addCriterion("reports_to <", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToLessThanOrEqualTo(String value) {
            addCriterion("reports_to <=", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToLike(String value) {
            addCriterion("reports_to like", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToNotLike(String value) {
            addCriterion("reports_to not like", value, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToIn(List<String> values) {
            addCriterion("reports_to in", values, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToNotIn(List<String> values) {
            addCriterion("reports_to not in", values, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToBetween(String value1, String value2) {
            addCriterion("reports_to between", value1, value2, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andReportsToNotBetween(String value1, String value2) {
            addCriterion("reports_to not between", value1, value2, "reportsTo");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateIsNull() {
            addCriterion("prev_login_date is null");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateIsNotNull() {
            addCriterion("prev_login_date is not null");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateEqualTo(Long value) {
            addCriterion("prev_login_date =", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateNotEqualTo(Long value) {
            addCriterion("prev_login_date <>", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateGreaterThan(Long value) {
            addCriterion("prev_login_date >", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateGreaterThanOrEqualTo(Long value) {
            addCriterion("prev_login_date >=", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateLessThan(Long value) {
            addCriterion("prev_login_date <", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateLessThanOrEqualTo(Long value) {
            addCriterion("prev_login_date <=", value, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateIn(List<Long> values) {
            addCriterion("prev_login_date in", values, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateNotIn(List<Long> values) {
            addCriterion("prev_login_date not in", values, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateBetween(Long value1, Long value2) {
            addCriterion("prev_login_date between", value1, value2, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginDateNotBetween(Long value1, Long value2) {
            addCriterion("prev_login_date not between", value1, value2, "prevLoginDate");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpIsNull() {
            addCriterion("prev_login_ip is null");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpIsNotNull() {
            addCriterion("prev_login_ip is not null");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpEqualTo(String value) {
            addCriterion("prev_login_ip =", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpNotEqualTo(String value) {
            addCriterion("prev_login_ip <>", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpGreaterThan(String value) {
            addCriterion("prev_login_ip >", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpGreaterThanOrEqualTo(String value) {
            addCriterion("prev_login_ip >=", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpLessThan(String value) {
            addCriterion("prev_login_ip <", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpLessThanOrEqualTo(String value) {
            addCriterion("prev_login_ip <=", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpLike(String value) {
            addCriterion("prev_login_ip like", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpNotLike(String value) {
            addCriterion("prev_login_ip not like", value, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpIn(List<String> values) {
            addCriterion("prev_login_ip in", values, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpNotIn(List<String> values) {
            addCriterion("prev_login_ip not in", values, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpBetween(String value1, String value2) {
            addCriterion("prev_login_ip between", value1, value2, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andPrevLoginIpNotBetween(String value1, String value2) {
            addCriterion("prev_login_ip not between", value1, value2, "prevLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIsNull() {
            addCriterion("last_login_date is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIsNotNull() {
            addCriterion("last_login_date is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateEqualTo(Long value) {
            addCriterion("last_login_date =", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotEqualTo(Long value) {
            addCriterion("last_login_date <>", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateGreaterThan(Long value) {
            addCriterion("last_login_date >", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateGreaterThanOrEqualTo(Long value) {
            addCriterion("last_login_date >=", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateLessThan(Long value) {
            addCriterion("last_login_date <", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateLessThanOrEqualTo(Long value) {
            addCriterion("last_login_date <=", value, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateIn(List<Long> values) {
            addCriterion("last_login_date in", values, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotIn(List<Long> values) {
            addCriterion("last_login_date not in", values, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateBetween(Long value1, Long value2) {
            addCriterion("last_login_date between", value1, value2, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginDateNotBetween(Long value1, Long value2) {
            addCriterion("last_login_date not between", value1, value2, "lastLoginDate");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIsNull() {
            addCriterion("last_login_ip is null");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIsNotNull() {
            addCriterion("last_login_ip is not null");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpEqualTo(String value) {
            addCriterion("last_login_ip =", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotEqualTo(String value) {
            addCriterion("last_login_ip <>", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpGreaterThan(String value) {
            addCriterion("last_login_ip >", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpGreaterThanOrEqualTo(String value) {
            addCriterion("last_login_ip >=", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLessThan(String value) {
            addCriterion("last_login_ip <", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLessThanOrEqualTo(String value) {
            addCriterion("last_login_ip <=", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpLike(String value) {
            addCriterion("last_login_ip like", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotLike(String value) {
            addCriterion("last_login_ip not like", value, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpIn(List<String> values) {
            addCriterion("last_login_ip in", values, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotIn(List<String> values) {
            addCriterion("last_login_ip not in", values, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpBetween(String value1, String value2) {
            addCriterion("last_login_ip between", value1, value2, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLastLoginIpNotBetween(String value1, String value2) {
            addCriterion("last_login_ip not between", value1, value2, "lastLoginIp");
            return (Criteria) this;
        }

        public Criteria andLoginNumIsNull() {
            addCriterion("login_num is null");
            return (Criteria) this;
        }

        public Criteria andLoginNumIsNotNull() {
            addCriterion("login_num is not null");
            return (Criteria) this;
        }

        public Criteria andLoginNumEqualTo(Integer value) {
            addCriterion("login_num =", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumNotEqualTo(Integer value) {
            addCriterion("login_num <>", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumGreaterThan(Integer value) {
            addCriterion("login_num >", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumGreaterThanOrEqualTo(Integer value) {
            addCriterion("login_num >=", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumLessThan(Integer value) {
            addCriterion("login_num <", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumLessThanOrEqualTo(Integer value) {
            addCriterion("login_num <=", value, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumIn(List<Integer> values) {
            addCriterion("login_num in", values, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumNotIn(List<Integer> values) {
            addCriterion("login_num not in", values, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumBetween(Integer value1, Integer value2) {
            addCriterion("login_num between", value1, value2, "loginNum");
            return (Criteria) this;
        }

        public Criteria andLoginNumNotBetween(Integer value1, Integer value2) {
            addCriterion("login_num not between", value1, value2, "loginNum");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria implements Serializable {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion implements Serializable {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}