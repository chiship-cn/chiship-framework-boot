package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

/**
 * @author LiJian
 */
@ApiModel(value = "用户表单")
public class UpmsUserDto {

    @ApiModelProperty(value = "是否添加到通讯录(0 否 1 是)", required = true)
    @NotNull(message = "是否添加到通讯录" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isAddMailList;

    @ApiModelProperty(value = "是否内部员工", required = true)
    @NotNull(message = "是否内部员工" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isInner;

    @ApiModelProperty(value = "用户名", required = true)
    @NotEmpty(message = "用户名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 11)
    private String userName;

    @ApiModelProperty(value = "真实姓名", required = true)
    @NotEmpty(message = "真实姓名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 2, max = 5)
    private String realName;

    @ApiModelProperty(value = "手机号", required = true)
    @NotEmpty(message = "手机号" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 11, max = 11)
    @Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
    private String mobile;

    @ApiModelProperty(value = "工号")
    private String userCode;

    @ApiModelProperty(value = "员工分类", required = true)
    @NotEmpty(message = "员工分类" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 10)
    private String category;

    @ApiModelProperty(value = "性别(0：保密  1：男   2：女)")
    @Min(0)
    private Byte gender;

    @ApiModelProperty(value = "座机")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "身份证")
    private String idNumber;

    @ApiModelProperty(value = "微信号")
    private String weixin;

    @ApiModelProperty(value = "企业微信用户ID")
    private String wxWorkUserId;

    @ApiModelProperty(value = "角色集合")
    private List<String> roles;

    @ApiModelProperty(value = "组织")
    @Valid
    private UpmsUserOrganizationDto userOrganization;

    public Byte getIsAddMailList() {
        return isAddMailList;
    }

    public void setIsAddMailList(Byte isAddMailList) {
        this.isAddMailList = isAddMailList;
    }

    public Byte getIsInner() {
        return isInner;
    }

    public void setIsInner(Byte isInner) {
        this.isInner = isInner;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getWeixin() {
        return weixin;
    }

    public void setWeixin(String weixin) {
        this.weixin = weixin;
    }

    public String getWxWorkUserId() {
        return wxWorkUserId;
    }

    public void setWxWorkUserId(String wxWorkUserId) {
        this.wxWorkUserId = wxWorkUserId;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public UpmsUserOrganizationDto getUserOrganization() {
        return userOrganization;
    }

    public void setUserOrganization(UpmsUserOrganizationDto userOrganization) {
        this.userOrganization = userOrganization;
    }

}
