package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * @author LiJian
 */
@ApiModel(value = "用户个人信息修改表单")
public class UpmsUserPersonalInformationModifyDto {

	@ApiModelProperty(value = "昵称")
	@Length(max = 20)
	private String nickName;

	@ApiModelProperty(value = "真实姓名", required = true)
	@NotEmpty(message = "真实姓名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 5)
	private String realName;

	@ApiModelProperty(value = "性别(0：保密  1：男   2：女)")
	@Min(0)
	private Byte gender;

	@ApiModelProperty(value = "个性签名")
	private String signature;

	@ApiModelProperty(value = "简介")
	private String introduction;

	@ApiModelProperty(value = "技能")
	private String skill;

	@ApiModelProperty(value = "标签")
	private String tag;

	@ApiModelProperty(value = "座机")
	private String phone;

	@ApiModelProperty(value = "身份证")
	private String idNumber;

	@ApiModelProperty(value = "QQ")
	private String qq;

	@ApiModelProperty(value = "微信号")
	private String weixin;

	@ApiModelProperty(value = "直属主管")
	private String reportsTo;

	@ApiModelProperty(value = "籍贯省")
	private Long nativePlaceLevel1;

	@ApiModelProperty(value = "籍贯市")
	private Long nativePlaceLevel2;

	@ApiModelProperty(value = "籍贯县")
	private Long nativePlaceLevel3;

	@ApiModelProperty(value = "文化程度")
	private String educationDegree;

	@ApiModelProperty(value = "政治面貌")
	private String politicalOutlook;

	@ApiModelProperty(value = "民族")
	private String nation;

	@ApiModelProperty(value = "学历")
	private String education;

	@ApiModelProperty(value = "婚姻状态")
	private String maritalStatus;

	@ApiModelProperty(value = "毕业学校")
	private String graduateSchool;

	@ApiModelProperty(value = "最高学位")
	private String highestDegree;

	@ApiModelProperty(value = "专业")
	private String major;

	@ApiModelProperty(value = "人员编制")
	private String staffing;

	@ApiModelProperty(value = "入职时间")
	private Long startWorkDate;

	@ApiModelProperty(value = "现居住地省")
	private Long currentResidenceLevel1;

	@ApiModelProperty(value = "现居住地市")
	private Long currentResidenceLevel2;

	@ApiModelProperty(value = "现居住地县")
	private Long currentResidenceLevel3;

	@ApiModelProperty(value = "现居住地区")
	private Long currentResidenceLevel4;

	@ApiModelProperty(value = "现居住地详细地址")
	private String currentAddress;

	@ApiModelProperty(value = "紧急联系人")
	private String emergencyContact;

	@ApiModelProperty(value = "紧急电话")
	private String emergencyContactMobile;

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	public String getQq() {
		return qq;
	}

	public void setQq(String qq) {
		this.qq = qq;
	}

	public String getWeixin() {
		return weixin;
	}

	public void setWeixin(String weixin) {
		this.weixin = weixin;
	}

	public String getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(String reportsTo) {
		this.reportsTo = reportsTo;
	}

	public Long getNativePlaceLevel1() {
		return nativePlaceLevel1;
	}

	public void setNativePlaceLevel1(Long nativePlaceLevel1) {
		this.nativePlaceLevel1 = nativePlaceLevel1;
	}

	public Long getNativePlaceLevel2() {
		return nativePlaceLevel2;
	}

	public void setNativePlaceLevel2(Long nativePlaceLevel2) {
		this.nativePlaceLevel2 = nativePlaceLevel2;
	}

	public Long getNativePlaceLevel3() {
		return nativePlaceLevel3;
	}

	public void setNativePlaceLevel3(Long nativePlaceLevel3) {
		this.nativePlaceLevel3 = nativePlaceLevel3;
	}

	public String getEducationDegree() {
		return educationDegree;
	}

	public void setEducationDegree(String educationDegree) {
		this.educationDegree = educationDegree;
	}

	public String getPoliticalOutlook() {
		return politicalOutlook;
	}

	public void setPoliticalOutlook(String politicalOutlook) {
		this.politicalOutlook = politicalOutlook;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(String maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public String getGraduateSchool() {
		return graduateSchool;
	}

	public void setGraduateSchool(String graduateSchool) {
		this.graduateSchool = graduateSchool;
	}

	public String getHighestDegree() {
		return highestDegree;
	}

	public void setHighestDegree(String highestDegree) {
		this.highestDegree = highestDegree;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStaffing() {
		return staffing;
	}

	public void setStaffing(String staffing) {
		this.staffing = staffing;
	}

	public Long getStartWorkDate() {
		return startWorkDate;
	}

	public void setStartWorkDate(Long startWorkDate) {
		this.startWorkDate = startWorkDate;
	}

	public Long getCurrentResidenceLevel1() {
		return currentResidenceLevel1;
	}

	public void setCurrentResidenceLevel1(Long currentResidenceLevel1) {
		this.currentResidenceLevel1 = currentResidenceLevel1;
	}

	public Long getCurrentResidenceLevel2() {
		return currentResidenceLevel2;
	}

	public void setCurrentResidenceLevel2(Long currentResidenceLevel2) {
		this.currentResidenceLevel2 = currentResidenceLevel2;
	}

	public Long getCurrentResidenceLevel3() {
		return currentResidenceLevel3;
	}

	public void setCurrentResidenceLevel3(Long currentResidenceLevel3) {
		this.currentResidenceLevel3 = currentResidenceLevel3;
	}

	public Long getCurrentResidenceLevel4() {
		return currentResidenceLevel4;
	}

	public void setCurrentResidenceLevel4(Long currentResidenceLevel4) {
		this.currentResidenceLevel4 = currentResidenceLevel4;
	}

	public String getCurrentAddress() {
		return currentAddress;
	}

	public void setCurrentAddress(String currentAddress) {
		this.currentAddress = currentAddress;
	}

	public String getEmergencyContact() {
		return emergencyContact;
	}

	public void setEmergencyContact(String emergencyContact) {
		this.emergencyContact = emergencyContact;
	}

	public String getEmergencyContactMobile() {
		return emergencyContactMobile;
	}

	public void setEmergencyContactMobile(String emergencyContactMobile) {
		this.emergencyContactMobile = emergencyContactMobile;
	}

}
