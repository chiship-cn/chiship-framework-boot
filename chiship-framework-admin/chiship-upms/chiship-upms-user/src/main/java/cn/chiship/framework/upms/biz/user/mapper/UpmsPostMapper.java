package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsPost;
import cn.chiship.framework.upms.biz.user.entity.UpmsPostExample;
import java.util.List;

import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lijian
 * @date 2025-02-10
 */
public interface UpmsPostMapper extends BaseMapper<UpmsPost,UpmsPostExample> {

}