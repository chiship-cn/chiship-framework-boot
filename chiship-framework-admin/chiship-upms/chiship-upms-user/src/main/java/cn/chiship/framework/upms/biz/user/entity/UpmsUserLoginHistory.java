package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UpmsUserLoginHistory implements Serializable {

	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 用户ID
	 *
	 * @mbg.generated
	 */
	private String userId;

	private String userName;

	private String realName;

	/**
	 * 登录IP
	 *
	 * @mbg.generated
	 */
	private String loginIp;

	/**
	 * 浏览器
	 *
	 * @mbg.generated
	 */
	private String browser;

	/**
	 * 浏览器版本
	 *
	 * @mbg.generated
	 */
	private String browserVersion;

	/**
	 * 操作系统
	 *
	 * @mbg.generated
	 */
	private String os;

	/**
	 * 经度
	 *
	 * @mbg.generated
	 */
	private String lng;

	/**
	 * 维度
	 *
	 * @mbg.generated
	 */
	private String lat;

	/**
	 * 是否登录成功（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isSuccess;

	private String remark;

	/**
	 * 登录地
	 *
	 * @mbg.generated
	 */
	private String loginAddress;

	/**
	 * 登录方式 pc&scan&app
	 *
	 * @mbg.generated
	 */
	private String loginType;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public String getBrowser() {
		return browser;
	}

	public void setBrowser(String browser) {
		this.browser = browser;
	}

	public String getBrowserVersion() {
		return browserVersion;
	}

	public void setBrowserVersion(String browserVersion) {
		this.browserVersion = browserVersion;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public Byte getIsSuccess() {
		return isSuccess;
	}

	public void setIsSuccess(Byte isSuccess) {
		this.isSuccess = isSuccess;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getLoginAddress() {
		return loginAddress;
	}

	public void setLoginAddress(String loginAddress) {
		this.loginAddress = loginAddress;
	}

	public String getLoginType() {
		return loginType;
	}

	public void setLoginType(String loginType) {
		this.loginType = loginType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", userId=").append(userId);
		sb.append(", userName=").append(userName);
		sb.append(", realName=").append(realName);
		sb.append(", loginIp=").append(loginIp);
		sb.append(", browser=").append(browser);
		sb.append(", browserVersion=").append(browserVersion);
		sb.append(", os=").append(os);
		sb.append(", lng=").append(lng);
		sb.append(", lat=").append(lat);
		sb.append(", isSuccess=").append(isSuccess);
		sb.append(", remark=").append(remark);
		sb.append(", loginAddress=").append(loginAddress);
		sb.append(", loginType=").append(loginType);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsUserLoginHistory other = (UpmsUserLoginHistory) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserName() == null ? other.getUserName() == null
						: this.getUserName().equals(other.getUserName()))
				&& (this.getRealName() == null ? other.getRealName() == null
						: this.getRealName().equals(other.getRealName()))
				&& (this.getLoginIp() == null ? other.getLoginIp() == null
						: this.getLoginIp().equals(other.getLoginIp()))
				&& (this.getBrowser() == null ? other.getBrowser() == null
						: this.getBrowser().equals(other.getBrowser()))
				&& (this.getBrowserVersion() == null ? other.getBrowserVersion() == null
						: this.getBrowserVersion().equals(other.getBrowserVersion()))
				&& (this.getOs() == null ? other.getOs() == null : this.getOs().equals(other.getOs()))
				&& (this.getLng() == null ? other.getLng() == null : this.getLng().equals(other.getLng()))
				&& (this.getLat() == null ? other.getLat() == null : this.getLat().equals(other.getLat()))
				&& (this.getIsSuccess() == null ? other.getIsSuccess() == null
						: this.getIsSuccess().equals(other.getIsSuccess()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
				&& (this.getLoginAddress() == null ? other.getLoginAddress() == null
						: this.getLoginAddress().equals(other.getLoginAddress()))
				&& (this.getLoginType() == null ? other.getLoginType() == null
						: this.getLoginType().equals(other.getLoginType()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
		result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
		result = prime * result + ((getLoginIp() == null) ? 0 : getLoginIp().hashCode());
		result = prime * result + ((getBrowser() == null) ? 0 : getBrowser().hashCode());
		result = prime * result + ((getBrowserVersion() == null) ? 0 : getBrowserVersion().hashCode());
		result = prime * result + ((getOs() == null) ? 0 : getOs().hashCode());
		result = prime * result + ((getLng() == null) ? 0 : getLng().hashCode());
		result = prime * result + ((getLat() == null) ? 0 : getLat().hashCode());
		result = prime * result + ((getIsSuccess() == null) ? 0 : getIsSuccess().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		result = prime * result + ((getLoginAddress() == null) ? 0 : getLoginAddress().hashCode());
		result = prime * result + ((getLoginType() == null) ? 0 : getLoginType().hashCode());
		return result;
	}

}