package cn.chiship.framework.upms.biz.user.controller;

import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.user.service.UpmsUserLoginHistoryService;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistory;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistoryExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户登录历史记录控制层 2021/9/27
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/userLoginHistory")
@Api(tags = "用户登录历史")
public class UpmsUserLoginHistoryController extends BaseController<UpmsUserLoginHistory, UpmsUserLoginHistoryExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserLoginHistoryController.class);

    @Resource
    private UpmsUserLoginHistoryService upmsUserLoginHistoryService;

    @Override
    public BaseService getService() {
        return upmsUserLoginHistoryService;
    }

    @ApiOperation(value = "用户登录历史分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "+id", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "loginIp", value = "登录IP地址", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "userName", value = "用户名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "isSuccess", value = "是否登录成功(1:成功   0：失败)", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"),})
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "loginIp") String loginIp,
                                           @RequestParam(required = false, value = "userName") String userName,
                                           @RequestParam(required = false, value = "realName") String realName,
                                           @RequestParam(required = false, value = "isSuccess") Byte isSuccess,
                                           @RequestParam(required = false, value = "startTime") Long startTime,
                                           @RequestParam(required = false, value = "endTime") Long endTime) {

        UpmsUserLoginHistoryExample upmsUserLoginHistoryExample = new UpmsUserLoginHistoryExample();
        // 创造条件
        UpmsUserLoginHistoryExample.Criteria criteria = upmsUserLoginHistoryExample.createCriteria();
        criteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(loginIp)) {
            criteria.andLoginIpLike(loginIp + "%");
        }
        if (!StringUtil.isNullOrEmpty(userName)) {
            criteria.andUserNameLike(userName + "%");
        }
        if (!StringUtil.isNullOrEmpty(realName)) {
            criteria.andRealNameLike(realName + "%");
        }
        if (!StringUtil.isNull(isSuccess)) {
            criteria.andIsSuccessEqualTo(isSuccess);
        }
        if (startTime != null) {
            criteria.andGmtCreatedGreaterThanOrEqualTo(startTime);
        }
        if (endTime != null) {
            criteria.andGmtCreatedLessThanOrEqualTo(endTime);
        }
        return super.responseEntity(BaseResult.ok(super.page(upmsUserLoginHistoryExample)));
    }

    @ApiOperation(value = "当前用户登录历史分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "+id", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "isSuccess", value = "是否登录成功(1:成功   0：失败)", dataTypeClass = Byte.class, paramType = "query"),
    })
    @GetMapping(value = "/current/page")
    public ResponseEntity<BaseResult> currentUserLoginPage(
            @RequestParam(required = false, value = "isSuccess") Byte isSuccess,
            @RequestParam(required = false, value = "startTime") Long startTime,
            @RequestParam(required = false, value = "endTime") Long endTime) {

        UpmsUserLoginHistoryExample upmsUserLoginHistoryExample = new UpmsUserLoginHistoryExample();
        // 创造条件
        UpmsUserLoginHistoryExample.Criteria criteria = upmsUserLoginHistoryExample.createCriteria();
        criteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(getUserId());
        if (!StringUtil.isNull(isSuccess)) {
            criteria.andIsSuccessEqualTo(isSuccess);
        }
        if (startTime != null) {
            criteria.andGmtCreatedGreaterThanOrEqualTo(startTime);
        }
        if (endTime != null) {
            criteria.andGmtCreatedLessThanOrEqualTo(endTime);
        }
        return super.responseEntity(BaseResult.ok(super.page(upmsUserLoginHistoryExample)));
    }

    @SystemOptionAnnotation(describe = "用户登录历史分页", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
    @ApiOperation(value = "删除用户登录历史")
    @PostMapping(value = "/remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsUserLoginHistoryExample upmsUserLoginHistoryExample = new UpmsUserLoginHistoryExample();
        upmsUserLoginHistoryExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsUserLoginHistoryExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空登录历史")
    @ApiOperation(value = "清空登录历史")
    @PostMapping(value = "/clear")
    public ResponseEntity<BaseResult> clear() {
        UpmsUserLoginHistoryExample upmsUserLoginHistoryExample = new UpmsUserLoginHistoryExample();
        return super.responseEntity(super.remove(upmsUserLoginHistoryExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空当前用户登录历史")
    @ApiOperation(value = "清空当前用户登录历史")
    @PostMapping(value = "/current/clear")
    public ResponseEntity<BaseResult> clearCurrent() {
        UpmsUserLoginHistoryExample upmsUserLoginHistoryExample = new UpmsUserLoginHistoryExample();
        upmsUserLoginHistoryExample.createCriteria().andUserIdEqualTo(getUserId());
        return super.responseEntity(super.remove(upmsUserLoginHistoryExample));
    }


}
