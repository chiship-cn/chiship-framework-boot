package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author LiJian
 */
@ApiModel(value = "权限表单")
public class UpmsPermissionDto {

    @ApiModelProperty(value = "所属菜单")
    @NotNull
    @Length(min = 1)
    private String pid;

    @ApiModelProperty(value = "权限名称")
    @NotNull
    @Length(min = 1)
    private String name;

    @ApiModelProperty(value = "权限值")
    @NotNull
    @Length(min = 1)
    private String permissionValue;

    @ApiModelProperty(value = "排序", required = true)
    @NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    private Long orders;

    @ApiModelProperty(value = "分类(0：PC 1：移动)", required = true)
    @NotNull(message = "分类" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte category;

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPermissionValue() {
        return permissionValue;
    }

    public void setPermissionValue(String permissionValue) {
        this.permissionValue = permissionValue;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    public Byte getCategory() {
        return category;
    }

    public void setCategory(Byte category) {
        this.category = category;
    }
}
