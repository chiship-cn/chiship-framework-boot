package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.entity.UpmsUserRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserRoleExample;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;

import java.util.List;

/**
 * 用户角色关联表业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsUserRoleService extends BaseService<UpmsUserRole, UpmsUserRoleExample> {

	/**
	 * 根据角色查询用户分页
	 * @param page
	 * @param limit
	 * @param roleId
	 * @return
	 */
	BaseResult pageUserByRoleId(Long page, Long limit, String roleId);

	/**
	 * 添加
	 * @param roleId
	 * @param userIds
	 * @return
	 */
	BaseResult userRoleSave(String roleId, List<String> userIds);

	/**
	 * 删除
	 * @param roleId
	 * @param userIds
	 * @return
	 */
	BaseResult userRoleRemove(String roleId, List<String> userIds);

}
