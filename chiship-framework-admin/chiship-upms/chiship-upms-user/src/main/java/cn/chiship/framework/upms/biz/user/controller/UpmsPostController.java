package cn.chiship.framework.upms.biz.user.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.user.service.UpmsPostService;
import cn.chiship.framework.upms.biz.user.entity.UpmsPost;
import cn.chiship.framework.upms.biz.user.entity.UpmsPostExample;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsPostDto;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工作岗位控制层
 * 2025/2/10
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/upmsPost")
@Api(tags = "工作岗位")
public class UpmsPostController extends BaseController<UpmsPost, UpmsPostExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsPostController.class);

    @Resource
    private UpmsPostService upmsPostService;

    @Override
    public BaseService getService() {
        return upmsPostService;
    }

    @SystemOptionAnnotation(describe = "工作岗位分页")
    @ApiOperation(value = "工作岗位分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "岗位类型", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "", value = "type") String type) {
        UpmsPostExample postExample = new UpmsPostExample();
        UpmsPostExample.Criteria criteria1 = postExample.createCriteria();
        UpmsPostExample.Criteria criteria2 = postExample.createCriteria();
        if (!StringUtil.isNullOrEmpty(keyword)) {
            criteria1.andCodeLike(keyword + "%");
            criteria2.andNameLike(keyword + "%");
        }
        if (!StringUtil.isNullOrEmpty(type)) {
            criteria1.andTypeEqualTo(type);
            criteria2.andTypeEqualTo(type);
        }
        postExample.or(criteria2);
        PageVo pageVo = super.page(postExample);
        pageVo.setRecords(upmsPostService.assembleData(pageVo.getRecords()));
        return super.responseEntity(BaseResult.ok(pageVo));
    }

    @SystemOptionAnnotation(describe = "工作岗位列表数据")
    @ApiOperation(value = "工作岗位列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "岗位类型", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "", value = "type") String type,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        UpmsPostExample postExample = new UpmsPostExample();
        UpmsPostExample.Criteria criteria1 = postExample.createCriteria();
        UpmsPostExample.Criteria criteria2 = postExample.createCriteria();
        if (!StringUtil.isNullOrEmpty(keyword)) {
            criteria1.andCodeLike(keyword + "%");
            criteria2.andNameLike(keyword + "%");
        }
        if (!StringUtil.isNullOrEmpty(type)) {
            criteria1.andTypeEqualTo(type);
            criteria2.andTypeEqualTo(type);
        }
        postExample.or(criteria2);
        postExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(postExample)));
    }

    @ApiOperation(value = "工作岗位树状数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "岗位类型", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/tree")
    public ResponseEntity<BaseResult> tree(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "", value = "type") String type) {
        UpmsPostExample postExample = new UpmsPostExample();
        UpmsPostExample.Criteria criteria1 = postExample.createCriteria();
        UpmsPostExample.Criteria criteria2 = postExample.createCriteria();
        if (!StringUtil.isNullOrEmpty(keyword)) {
            criteria1.andCodeLike(keyword + "%");
            criteria2.andNameLike(keyword + "%");
        }
        if (!StringUtil.isNullOrEmpty(type)) {
            criteria1.andTypeEqualTo(type);
            criteria2.andTypeEqualTo(type);
        }
        postExample.or(criteria2);
        return super.responseEntity(upmsPostService.tree(super.list(postExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存工作岗位")
    @ApiOperation(value = "保存工作岗位")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsPostDto upmsPostDto) {
        UpmsPost upmsPost = new UpmsPost();
        BeanUtils.copyProperties(upmsPostDto, upmsPost);
        return super.responseEntity(super.save(upmsPost));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新工作岗位")
    @ApiOperation(value = "更新工作岗位")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid UpmsPostDto upmsPostDto) {
        UpmsPost upmsPost = new UpmsPost();
        BeanUtils.copyProperties(upmsPostDto, upmsPost);
        return super.responseEntity(super.update(id, upmsPost));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除工作岗位")
    @ApiOperation(value = "删除工作岗位")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsPostExample upmsPostExample = new UpmsPostExample();
        upmsPostExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsPostExample));
    }

    @SystemOptionAnnotation(describe = "工作岗位启用&锁定", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
    @ApiOperation(value = "工作岗位启用&锁定")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "工作岗位id", required = true, dataTypeClass = String.class, paramType = "path"),
            @ApiImplicitParam(name = "status", value = "启用(0)&禁用(1)", required = true, dataTypeClass = Byte.class, paramType = "body")
    })
    @PostMapping(value = "changeStatus/{id}")
    public ResponseEntity<BaseResult> changeStatus(@PathVariable("id") String id, @RequestBody Byte status) {

        return super.responseEntity(upmsPostService.changeStatus(id, status));
    }
}