package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.user.entity.UpmsPermissionExample;

import javax.servlet.http.HttpServletResponse;

/**
 * 权限业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsPermissionService extends BaseService<UpmsPermission, UpmsPermissionExample> {

    /**
     * 启用&禁用
     *
     * @param id
     * @param isDisabled
     * @return
     */
    BaseResult changeIsState(String id, Boolean isDisabled);

    /**
     * 下载权限编码
     *
     * @param response
     * @param category
     */
    void downloadPermissionCode(HttpServletResponse response, Byte category);

}
