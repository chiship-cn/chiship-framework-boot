package cn.chiship.framework.upms.biz.user.pojo.vo;

import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsUser;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "用户视图")
public class UpmsUserVo extends UpmsUser {

	/**
	 * 人员组织主键
	 */
	private String userOrgId;

	/**
	 * 人员在组织电话
	 */
	private String userOrgPhone;

	/**
	 * 人员在组织任职
	 */
	private String userOrgJob;

	/**
	 * 人员在组织排序
	 */
	private String userOrgOrder;

	/**
	 * 角色名称
	 */
	private String roleName;

	/**
	 * 角色编号
	 */
	private String roleCode;

	/**
	 * 加入组织时间
	 */
	private String addTime;

	/**
	 * 组织主键
	 */
	private String organizationId;

	/**
	 * 组织名称
	 */
	private String organizationName;

	/**
	 * 组织编号
	 */
	private String organizationCode;

	/**
	 * 组织类型
	 */
	private String organizationType;

	/**
	 * 组织层级
	 */
	private String treeNumber;

	private List<UpmsRole> roles;

	private List<JSONObject> organizationUsers;

	public String getUserOrgId() {
		return userOrgId;
	}

	public void setUserOrgId(String userOrgId) {
		this.userOrgId = userOrgId;
	}

	public String getUserOrgPhone() {
		return userOrgPhone;
	}

	public void setUserOrgPhone(String userOrgPhone) {
		this.userOrgPhone = userOrgPhone;
	}

	public String getUserOrgJob() {
		return userOrgJob;
	}

	public void setUserOrgJob(String userOrgJob) {
		this.userOrgJob = userOrgJob;
	}

	public String getUserOrgOrder() {
		return userOrgOrder;
	}

	public void setUserOrgOrder(String userOrgOrder) {
		this.userOrgOrder = userOrgOrder;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	public String getAddTime() {
		return addTime;
	}

	public void setAddTime(String addTime) {
		this.addTime = addTime;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationType() {
		return organizationType;
	}

	public void setOrganizationType(String organizationType) {
		this.organizationType = organizationType;
	}

	public String getTreeNumber() {
		return treeNumber;
	}

	public void setTreeNumber(String treeNumber) {
		this.treeNumber = treeNumber;
	}

	public List<UpmsRole> getRoles() {
		return roles;
	}

	public void setRoles(List<UpmsRole> roles) {
		this.roles = roles;
	}

	public List<JSONObject> getOrganizationUsers() {
		return organizationUsers;
	}

	public void setOrganizationUsers(List<JSONObject> organizationUsers) {
		this.organizationUsers = organizationUsers;
	}

}
