package cn.chiship.framework.upms.biz.user.service.impl;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermission;
import cn.chiship.framework.upms.biz.user.mapper.UpmsPermissionMapper;
import cn.chiship.framework.upms.biz.user.mapper.UpmsRolePermissionMapper;
import cn.chiship.framework.upms.biz.user.service.UpmsRolePermissionService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermissionExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * 角色权限关联表业务接口实现层 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsRolePermissionServiceImpl extends BaseServiceImpl<UpmsRolePermission, UpmsRolePermissionExample>
		implements UpmsRolePermissionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsRolePermissionServiceImpl.class);

	@Resource
	private UpmsRolePermissionMapper upmsRolePermissionMapper;

	@Resource
	private UpmsPermissionMapper upmsPermissionMapper;

	@Override
	public BaseResult getPermissionByRoleId(String roleId) {
		List<UpmsPermission> permissions = upmsRolePermissionMapper.getPermissionsByRoleId(roleId);
		return BaseResult.ok(permissions);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResult rolePermissionSave(String roleId, List<String> permissionIds) {
		if (permissionIds.isEmpty()) {
			return BaseResult.error("请至少选择一条授权权限！");
		}
		UpmsRolePermissionExample upmsRolePermissionExample = new UpmsRolePermissionExample();
		UpmsRolePermissionExample.Criteria criteria = upmsRolePermissionExample.createCriteria();
		criteria.andRoleIdEqualTo(roleId);
		List<UpmsRolePermission> currentRolePermissions = upmsRolePermissionMapper
				.selectByExample(upmsRolePermissionExample);
		List<String> listPermissionId = new ArrayList<>();
		for (UpmsRolePermission rolePermission : currentRolePermissions) {
			listPermissionId.add(rolePermission.getPermissionId());
		}

		/**
		 * 新增
		 */
		List<String> notExists = new ArrayList<>(permissionIds);
		notExists.removeAll(listPermissionId);
		if (!notExists.isEmpty()) {
			List<UpmsRolePermission> rolePermissions = new ArrayList<>();
			for (String permissionId : notExists) {
				UpmsRolePermission rolePermission = new UpmsRolePermission();
				rolePermission.setId(SnowflakeIdUtil.generateStrId());
				rolePermission.setIsDeleted(BaseConstants.NO);
				rolePermission.setGmtCreated(System.currentTimeMillis());
				rolePermission.setGmtModified(System.currentTimeMillis());
				rolePermission.setRoleId(roleId);
				rolePermission.setPermissionId(permissionId);
				rolePermissions.add(rolePermission);
			}
			upmsRolePermissionMapper.insertSelectiveBatch(rolePermissions);
		}
		LOGGER.info("权限分配成功");
		return BaseResult.ok("权限分配成功");
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResult rolePermissionCancel(String roleId, List<String> permissionIds) {
		if (permissionIds.isEmpty()) {
			return BaseResult.error("请至少选择一条取消授权权限！");
		}
		UpmsRolePermissionExample upmsRolePermissionExample = new UpmsRolePermissionExample();
		UpmsRolePermissionExample.Criteria criteria = upmsRolePermissionExample.createCriteria();
		criteria.andRoleIdEqualTo(roleId);
		/**
		 * 删除
		 */
		List<String> listId = upmsPermissionMapper.getChildrenIdById(permissionIds);
		listId.addAll(permissionIds);
		criteria.andPermissionIdIn(listId);
		upmsRolePermissionMapper.deleteByExample(upmsRolePermissionExample);
		LOGGER.info("权限取消授权成功");
		return BaseResult.ok("权限取消授权成功");
	}

}
