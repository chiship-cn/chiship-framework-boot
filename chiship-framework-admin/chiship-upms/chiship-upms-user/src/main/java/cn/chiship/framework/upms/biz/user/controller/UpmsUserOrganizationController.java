package cn.chiship.framework.upms.biz.user.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.user.service.UpmsUserOrganizationService;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganization;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganizationExample;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsUserOrganizationDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 用户组织关联表控制层 2023/2/21
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/userOrganization")
@Api(tags = "用户组织关联控制器")
public class UpmsUserOrganizationController extends BaseController<UpmsUserOrganization, UpmsUserOrganizationExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserOrganizationController.class);

    @Resource
    private UpmsUserOrganizationService upmsUserOrganizationService;

    @Override
    public BaseService getService() {
        return upmsUserOrganizationService;
    }

    @ApiOperation(value = "用户组织列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户主键", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping(value = "/listByUserId")
    public ResponseEntity<BaseResult> listByUserId(@RequestParam(value = "userId") String userId) {
        return super.responseEntity(BaseResult.ok(upmsUserOrganizationService.selectUserOrg(userId)));
    }

    @ApiOperation(value = "校验用户是否在组织下存在")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户", required = true, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "orgId", value = "组织", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping(value = "validateExist")
    public ResponseEntity<BaseResult> validateExist(@RequestParam(value = "userId") String userId,
                                                    @RequestParam(value = "orgId") String orgId) {
        return new ResponseEntity<>(BaseResult.ok(upmsUserOrganizationService.validateExist(userId, orgId)), HttpStatus.OK);
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存机构用户")
    @ApiOperation(value = "保存机构用户")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsUserOrganizationDto upmsUserOrganizationDto) {
        upmsUserOrganizationService.saveUserOrganization(upmsUserOrganizationDto);
        return super.responseEntity(BaseResult.ok());
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "修改机构用户")
    @ApiOperation(value = "修改机构用户")
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid UpmsUserOrganizationDto upmsUserOrganizationDto) {
        UpmsUserOrganization upmsUserOrganization = new UpmsUserOrganization();
        BeanUtils.copyProperties(upmsUserOrganizationDto, upmsUserOrganization);
        return super.responseEntity(super.update(id, upmsUserOrganization));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "移除组织用户")
    @ApiOperation(value = "移除组织用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids", value = "主键ID集合", required = true, dataTypeClass = List.class, paramType = "body")
    })
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody List<String> ids) {
        UpmsUserOrganizationExample upmsUserOrganizationExample = new UpmsUserOrganizationExample();
        upmsUserOrganizationExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsUserOrganizationExample));
    }

}
