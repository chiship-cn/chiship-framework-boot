package cn.chiship.framework.upms.biz.user.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author LiJian
 */
@ApiModel(value = "角色表单")
public class UpmsRoleDto {

	@ApiModelProperty(value = "角色名称")
	@NotNull
	@Length(min = 1, max = 20)
	private String name;

	@ApiModelProperty(value = "角色编号")
	@NotNull
	@Length(min = 1, max = 20)
	private String code;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "上级")
	private String pid;

	@ApiModelProperty(value = "数据范围(1：全部数据权限 2：自定数据权限 3：本机构数据权限 4：本机构及以下数据权限 5：本部门数据权限 6：本部门及以下数据权限  7：仅本人数据权限)")
	@NotNull
	@Min(value = 1)
	@Max(value = 7)
	private Byte dataScope;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Byte getDataScope() {
		return dataScope;
	}

	public void setDataScope(Byte dataScope) {
		this.dataScope = dataScope;
	}

}
