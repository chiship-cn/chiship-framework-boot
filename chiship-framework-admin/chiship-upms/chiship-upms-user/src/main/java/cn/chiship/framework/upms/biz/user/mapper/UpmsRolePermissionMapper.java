package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermission;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermissionExample;

import java.util.List;

/**
 * @author lj
 */
public interface UpmsRolePermissionMapper extends BaseMapper<UpmsRolePermission, UpmsRolePermissionExample> {

	/**
	 * 批量添加
	 * @param list
	 * @return
	 */
	int insertSelectiveBatch(List<UpmsRolePermission> list);

	/**
	 * 根据角色查找授权的权限
	 * @param roleId
	 * @return
	 */
	List<UpmsPermission> getPermissionsByRoleId(String roleId);

}
