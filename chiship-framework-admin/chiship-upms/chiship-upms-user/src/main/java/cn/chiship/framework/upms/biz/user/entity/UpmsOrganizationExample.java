package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-06-29
 */
public class UpmsOrganizationExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsOrganizationExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andLogoIsNull() {
			addCriterion("logo is null");
			return (Criteria) this;
		}

		public Criteria andLogoIsNotNull() {
			addCriterion("logo is not null");
			return (Criteria) this;
		}

		public Criteria andLogoEqualTo(String value) {
			addCriterion("logo =", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoNotEqualTo(String value) {
			addCriterion("logo <>", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoGreaterThan(String value) {
			addCriterion("logo >", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoGreaterThanOrEqualTo(String value) {
			addCriterion("logo >=", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoLessThan(String value) {
			addCriterion("logo <", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoLessThanOrEqualTo(String value) {
			addCriterion("logo <=", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoLike(String value) {
			addCriterion("logo like", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoNotLike(String value) {
			addCriterion("logo not like", value, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoIn(List<String> values) {
			addCriterion("logo in", values, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoNotIn(List<String> values) {
			addCriterion("logo not in", values, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoBetween(String value1, String value2) {
			addCriterion("logo between", value1, value2, "logo");
			return (Criteria) this;
		}

		public Criteria andLogoNotBetween(String value1, String value2) {
			addCriterion("logo not between", value1, value2, "logo");
			return (Criteria) this;
		}

		public Criteria andPidIsNull() {
			addCriterion("pid is null");
			return (Criteria) this;
		}

		public Criteria andPidIsNotNull() {
			addCriterion("pid is not null");
			return (Criteria) this;
		}

		public Criteria andPidEqualTo(String value) {
			addCriterion("pid =", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotEqualTo(String value) {
			addCriterion("pid <>", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThan(String value) {
			addCriterion("pid >", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThanOrEqualTo(String value) {
			addCriterion("pid >=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThan(String value) {
			addCriterion("pid <", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThanOrEqualTo(String value) {
			addCriterion("pid <=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLike(String value) {
			addCriterion("pid like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotLike(String value) {
			addCriterion("pid not like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidIn(List<String> values) {
			addCriterion("pid in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotIn(List<String> values) {
			addCriterion("pid not in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidBetween(String value1, String value2) {
			addCriterion("pid between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotBetween(String value1, String value2) {
			addCriterion("pid not between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIsNull() {
			addCriterion("organization_code is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIsNotNull() {
			addCriterion("organization_code is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeEqualTo(String value) {
			addCriterion("organization_code =", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotEqualTo(String value) {
			addCriterion("organization_code <>", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeGreaterThan(String value) {
			addCriterion("organization_code >", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
			addCriterion("organization_code >=", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLessThan(String value) {
			addCriterion("organization_code <", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
			addCriterion("organization_code <=", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLike(String value) {
			addCriterion("organization_code like", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotLike(String value) {
			addCriterion("organization_code not like", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIn(List<String> values) {
			addCriterion("organization_code in", values, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotIn(List<String> values) {
			addCriterion("organization_code not in", values, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeBetween(String value1, String value2) {
			addCriterion("organization_code between", value1, value2, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
			addCriterion("organization_code not between", value1, value2, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIsNull() {
			addCriterion("organization_name is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIsNotNull() {
			addCriterion("organization_name is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameEqualTo(String value) {
			addCriterion("organization_name =", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotEqualTo(String value) {
			addCriterion("organization_name <>", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameGreaterThan(String value) {
			addCriterion("organization_name >", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameGreaterThanOrEqualTo(String value) {
			addCriterion("organization_name >=", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLessThan(String value) {
			addCriterion("organization_name <", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLessThanOrEqualTo(String value) {
			addCriterion("organization_name <=", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLike(String value) {
			addCriterion("organization_name like", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotLike(String value) {
			addCriterion("organization_name not like", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIn(List<String> values) {
			addCriterion("organization_name in", values, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotIn(List<String> values) {
			addCriterion("organization_name not in", values, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameBetween(String value1, String value2) {
			addCriterion("organization_name between", value1, value2, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotBetween(String value1, String value2) {
			addCriterion("organization_name not between", value1, value2, "organizationName");
			return (Criteria) this;
		}

		public Criteria andSortOrdersIsNull() {
			addCriterion("sort_orders is null");
			return (Criteria) this;
		}

		public Criteria andSortOrdersIsNotNull() {
			addCriterion("sort_orders is not null");
			return (Criteria) this;
		}

		public Criteria andSortOrdersEqualTo(Integer value) {
			addCriterion("sort_orders =", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersNotEqualTo(Integer value) {
			addCriterion("sort_orders <>", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersGreaterThan(Integer value) {
			addCriterion("sort_orders >", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersGreaterThanOrEqualTo(Integer value) {
			addCriterion("sort_orders >=", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersLessThan(Integer value) {
			addCriterion("sort_orders <", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersLessThanOrEqualTo(Integer value) {
			addCriterion("sort_orders <=", value, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersIn(List<Integer> values) {
			addCriterion("sort_orders in", values, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersNotIn(List<Integer> values) {
			addCriterion("sort_orders not in", values, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersBetween(Integer value1, Integer value2) {
			addCriterion("sort_orders between", value1, value2, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andSortOrdersNotBetween(Integer value1, Integer value2) {
			addCriterion("sort_orders not between", value1, value2, "sortOrders");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelIsNull() {
			addCriterion("organization_level is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelIsNotNull() {
			addCriterion("organization_level is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelEqualTo(String value) {
			addCriterion("organization_level =", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelNotEqualTo(String value) {
			addCriterion("organization_level <>", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelGreaterThan(String value) {
			addCriterion("organization_level >", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelGreaterThanOrEqualTo(String value) {
			addCriterion("organization_level >=", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelLessThan(String value) {
			addCriterion("organization_level <", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelLessThanOrEqualTo(String value) {
			addCriterion("organization_level <=", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelLike(String value) {
			addCriterion("organization_level like", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelNotLike(String value) {
			addCriterion("organization_level not like", value, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelIn(List<String> values) {
			addCriterion("organization_level in", values, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelNotIn(List<String> values) {
			addCriterion("organization_level not in", values, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelBetween(String value1, String value2) {
			addCriterion("organization_level between", value1, value2, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationLevelNotBetween(String value1, String value2) {
			addCriterion("organization_level not between", value1, value2, "organizationLevel");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryIsNull() {
			addCriterion("organization_category is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryIsNotNull() {
			addCriterion("organization_category is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryEqualTo(String value) {
			addCriterion("organization_category =", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryNotEqualTo(String value) {
			addCriterion("organization_category <>", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryGreaterThan(String value) {
			addCriterion("organization_category >", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryGreaterThanOrEqualTo(String value) {
			addCriterion("organization_category >=", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryLessThan(String value) {
			addCriterion("organization_category <", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryLessThanOrEqualTo(String value) {
			addCriterion("organization_category <=", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryLike(String value) {
			addCriterion("organization_category like", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryNotLike(String value) {
			addCriterion("organization_category not like", value, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryIn(List<String> values) {
			addCriterion("organization_category in", values, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryNotIn(List<String> values) {
			addCriterion("organization_category not in", values, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryBetween(String value1, String value2) {
			addCriterion("organization_category between", value1, value2, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationCategoryNotBetween(String value1, String value2) {
			addCriterion("organization_category not between", value1, value2, "organizationCategory");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagIsNull() {
			addCriterion("organization_tag is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagIsNotNull() {
			addCriterion("organization_tag is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagEqualTo(String value) {
			addCriterion("organization_tag =", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagNotEqualTo(String value) {
			addCriterion("organization_tag <>", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagGreaterThan(String value) {
			addCriterion("organization_tag >", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagGreaterThanOrEqualTo(String value) {
			addCriterion("organization_tag >=", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagLessThan(String value) {
			addCriterion("organization_tag <", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagLessThanOrEqualTo(String value) {
			addCriterion("organization_tag <=", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagLike(String value) {
			addCriterion("organization_tag like", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagNotLike(String value) {
			addCriterion("organization_tag not like", value, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagIn(List<String> values) {
			addCriterion("organization_tag in", values, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagNotIn(List<String> values) {
			addCriterion("organization_tag not in", values, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagBetween(String value1, String value2) {
			addCriterion("organization_tag between", value1, value2, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andOrganizationTagNotBetween(String value1, String value2) {
			addCriterion("organization_tag not between", value1, value2, "organizationTag");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseIsNull() {
			addCriterion("business_license is null");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseIsNotNull() {
			addCriterion("business_license is not null");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseEqualTo(String value) {
			addCriterion("business_license =", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseNotEqualTo(String value) {
			addCriterion("business_license <>", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseGreaterThan(String value) {
			addCriterion("business_license >", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseGreaterThanOrEqualTo(String value) {
			addCriterion("business_license >=", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseLessThan(String value) {
			addCriterion("business_license <", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseLessThanOrEqualTo(String value) {
			addCriterion("business_license <=", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseLike(String value) {
			addCriterion("business_license like", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseNotLike(String value) {
			addCriterion("business_license not like", value, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseIn(List<String> values) {
			addCriterion("business_license in", values, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseNotIn(List<String> values) {
			addCriterion("business_license not in", values, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseBetween(String value1, String value2) {
			addCriterion("business_license between", value1, value2, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andBusinessLicenseNotBetween(String value1, String value2) {
			addCriterion("business_license not between", value1, value2, "businessLicense");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeIsNull() {
			addCriterion("social_credit_code is null");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeIsNotNull() {
			addCriterion("social_credit_code is not null");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeEqualTo(String value) {
			addCriterion("social_credit_code =", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeNotEqualTo(String value) {
			addCriterion("social_credit_code <>", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeGreaterThan(String value) {
			addCriterion("social_credit_code >", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeGreaterThanOrEqualTo(String value) {
			addCriterion("social_credit_code >=", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeLessThan(String value) {
			addCriterion("social_credit_code <", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeLessThanOrEqualTo(String value) {
			addCriterion("social_credit_code <=", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeLike(String value) {
			addCriterion("social_credit_code like", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeNotLike(String value) {
			addCriterion("social_credit_code not like", value, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeIn(List<String> values) {
			addCriterion("social_credit_code in", values, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeNotIn(List<String> values) {
			addCriterion("social_credit_code not in", values, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeBetween(String value1, String value2) {
			addCriterion("social_credit_code between", value1, value2, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andSocialCreditCodeNotBetween(String value1, String value2) {
			addCriterion("social_credit_code not between", value1, value2, "socialCreditCode");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIsNull() {
			addCriterion("tree_number is null");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIsNotNull() {
			addCriterion("tree_number is not null");
			return (Criteria) this;
		}

		public Criteria andTreeNumberEqualTo(String value) {
			addCriterion("tree_number =", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotEqualTo(String value) {
			addCriterion("tree_number <>", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberGreaterThan(String value) {
			addCriterion("tree_number >", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberGreaterThanOrEqualTo(String value) {
			addCriterion("tree_number >=", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLessThan(String value) {
			addCriterion("tree_number <", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLessThanOrEqualTo(String value) {
			addCriterion("tree_number <=", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLike(String value) {
			addCriterion("tree_number like", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotLike(String value) {
			addCriterion("tree_number not like", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIn(List<String> values) {
			addCriterion("tree_number in", values, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotIn(List<String> values) {
			addCriterion("tree_number not in", values, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberBetween(String value1, String value2) {
			addCriterion("tree_number between", value1, value2, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotBetween(String value1, String value2) {
			addCriterion("tree_number not between", value1, value2, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeIsNull() {
			addCriterion("invitation_code is null");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeIsNotNull() {
			addCriterion("invitation_code is not null");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeEqualTo(String value) {
			addCriterion("invitation_code =", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeNotEqualTo(String value) {
			addCriterion("invitation_code <>", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeGreaterThan(String value) {
			addCriterion("invitation_code >", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeGreaterThanOrEqualTo(String value) {
			addCriterion("invitation_code >=", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeLessThan(String value) {
			addCriterion("invitation_code <", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeLessThanOrEqualTo(String value) {
			addCriterion("invitation_code <=", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeLike(String value) {
			addCriterion("invitation_code like", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeNotLike(String value) {
			addCriterion("invitation_code not like", value, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeIn(List<String> values) {
			addCriterion("invitation_code in", values, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeNotIn(List<String> values) {
			addCriterion("invitation_code not in", values, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeBetween(String value1, String value2) {
			addCriterion("invitation_code between", value1, value2, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andInvitationCodeNotBetween(String value1, String value2) {
			addCriterion("invitation_code not between", value1, value2, "invitationCode");
			return (Criteria) this;
		}

		public Criteria andDescriptionIsNull() {
			addCriterion("description is null");
			return (Criteria) this;
		}

		public Criteria andDescriptionIsNotNull() {
			addCriterion("description is not null");
			return (Criteria) this;
		}

		public Criteria andDescriptionEqualTo(String value) {
			addCriterion("description =", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotEqualTo(String value) {
			addCriterion("description <>", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionGreaterThan(String value) {
			addCriterion("description >", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
			addCriterion("description >=", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLessThan(String value) {
			addCriterion("description <", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLessThanOrEqualTo(String value) {
			addCriterion("description <=", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLike(String value) {
			addCriterion("description like", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotLike(String value) {
			addCriterion("description not like", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionIn(List<String> values) {
			addCriterion("description in", values, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotIn(List<String> values) {
			addCriterion("description not in", values, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionBetween(String value1, String value2) {
			addCriterion("description between", value1, value2, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotBetween(String value1, String value2) {
			addCriterion("description not between", value1, value2, "description");
			return (Criteria) this;
		}

		public Criteria andPhoneIsNull() {
			addCriterion("phone is null");
			return (Criteria) this;
		}

		public Criteria andPhoneIsNotNull() {
			addCriterion("phone is not null");
			return (Criteria) this;
		}

		public Criteria andPhoneEqualTo(String value) {
			addCriterion("phone =", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotEqualTo(String value) {
			addCriterion("phone <>", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneGreaterThan(String value) {
			addCriterion("phone >", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneGreaterThanOrEqualTo(String value) {
			addCriterion("phone >=", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLessThan(String value) {
			addCriterion("phone <", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLessThanOrEqualTo(String value) {
			addCriterion("phone <=", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneLike(String value) {
			addCriterion("phone like", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotLike(String value) {
			addCriterion("phone not like", value, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneIn(List<String> values) {
			addCriterion("phone in", values, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotIn(List<String> values) {
			addCriterion("phone not in", values, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneBetween(String value1, String value2) {
			addCriterion("phone between", value1, value2, "phone");
			return (Criteria) this;
		}

		public Criteria andPhoneNotBetween(String value1, String value2) {
			addCriterion("phone not between", value1, value2, "phone");
			return (Criteria) this;
		}

		public Criteria andFaxIsNull() {
			addCriterion("fax is null");
			return (Criteria) this;
		}

		public Criteria andFaxIsNotNull() {
			addCriterion("fax is not null");
			return (Criteria) this;
		}

		public Criteria andFaxEqualTo(String value) {
			addCriterion("fax =", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxNotEqualTo(String value) {
			addCriterion("fax <>", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxGreaterThan(String value) {
			addCriterion("fax >", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxGreaterThanOrEqualTo(String value) {
			addCriterion("fax >=", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxLessThan(String value) {
			addCriterion("fax <", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxLessThanOrEqualTo(String value) {
			addCriterion("fax <=", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxLike(String value) {
			addCriterion("fax like", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxNotLike(String value) {
			addCriterion("fax not like", value, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxIn(List<String> values) {
			addCriterion("fax in", values, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxNotIn(List<String> values) {
			addCriterion("fax not in", values, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxBetween(String value1, String value2) {
			addCriterion("fax between", value1, value2, "fax");
			return (Criteria) this;
		}

		public Criteria andFaxNotBetween(String value1, String value2) {
			addCriterion("fax not between", value1, value2, "fax");
			return (Criteria) this;
		}

		public Criteria andChargeInfoIsNull() {
			addCriterion("charge_info is null");
			return (Criteria) this;
		}

		public Criteria andChargeInfoIsNotNull() {
			addCriterion("charge_info is not null");
			return (Criteria) this;
		}

		public Criteria andChargeInfoEqualTo(String value) {
			addCriterion("charge_info =", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoNotEqualTo(String value) {
			addCriterion("charge_info <>", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoGreaterThan(String value) {
			addCriterion("charge_info >", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoGreaterThanOrEqualTo(String value) {
			addCriterion("charge_info >=", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoLessThan(String value) {
			addCriterion("charge_info <", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoLessThanOrEqualTo(String value) {
			addCriterion("charge_info <=", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoLike(String value) {
			addCriterion("charge_info like", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoNotLike(String value) {
			addCriterion("charge_info not like", value, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoIn(List<String> values) {
			addCriterion("charge_info in", values, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoNotIn(List<String> values) {
			addCriterion("charge_info not in", values, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoBetween(String value1, String value2) {
			addCriterion("charge_info between", value1, value2, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andChargeInfoNotBetween(String value1, String value2) {
			addCriterion("charge_info not between", value1, value2, "chargeInfo");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1IsNull() {
			addCriterion("region_level1 is null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1IsNotNull() {
			addCriterion("region_level1 is not null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1EqualTo(Long value) {
			addCriterion("region_level1 =", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1NotEqualTo(Long value) {
			addCriterion("region_level1 <>", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1GreaterThan(Long value) {
			addCriterion("region_level1 >", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1GreaterThanOrEqualTo(Long value) {
			addCriterion("region_level1 >=", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1LessThan(Long value) {
			addCriterion("region_level1 <", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1LessThanOrEqualTo(Long value) {
			addCriterion("region_level1 <=", value, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1In(List<Long> values) {
			addCriterion("region_level1 in", values, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1NotIn(List<Long> values) {
			addCriterion("region_level1 not in", values, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1Between(Long value1, Long value2) {
			addCriterion("region_level1 between", value1, value2, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel1NotBetween(Long value1, Long value2) {
			addCriterion("region_level1 not between", value1, value2, "regionLevel1");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2IsNull() {
			addCriterion("region_level2 is null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2IsNotNull() {
			addCriterion("region_level2 is not null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2EqualTo(Long value) {
			addCriterion("region_level2 =", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2NotEqualTo(Long value) {
			addCriterion("region_level2 <>", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2GreaterThan(Long value) {
			addCriterion("region_level2 >", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2GreaterThanOrEqualTo(Long value) {
			addCriterion("region_level2 >=", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2LessThan(Long value) {
			addCriterion("region_level2 <", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2LessThanOrEqualTo(Long value) {
			addCriterion("region_level2 <=", value, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2In(List<Long> values) {
			addCriterion("region_level2 in", values, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2NotIn(List<Long> values) {
			addCriterion("region_level2 not in", values, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2Between(Long value1, Long value2) {
			addCriterion("region_level2 between", value1, value2, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel2NotBetween(Long value1, Long value2) {
			addCriterion("region_level2 not between", value1, value2, "regionLevel2");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3IsNull() {
			addCriterion("region_level3 is null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3IsNotNull() {
			addCriterion("region_level3 is not null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3EqualTo(Long value) {
			addCriterion("region_level3 =", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3NotEqualTo(Long value) {
			addCriterion("region_level3 <>", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3GreaterThan(Long value) {
			addCriterion("region_level3 >", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3GreaterThanOrEqualTo(Long value) {
			addCriterion("region_level3 >=", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3LessThan(Long value) {
			addCriterion("region_level3 <", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3LessThanOrEqualTo(Long value) {
			addCriterion("region_level3 <=", value, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3In(List<Long> values) {
			addCriterion("region_level3 in", values, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3NotIn(List<Long> values) {
			addCriterion("region_level3 not in", values, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3Between(Long value1, Long value2) {
			addCriterion("region_level3 between", value1, value2, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel3NotBetween(Long value1, Long value2) {
			addCriterion("region_level3 not between", value1, value2, "regionLevel3");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4IsNull() {
			addCriterion("region_level4 is null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4IsNotNull() {
			addCriterion("region_level4 is not null");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4EqualTo(Long value) {
			addCriterion("region_level4 =", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4NotEqualTo(Long value) {
			addCriterion("region_level4 <>", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4GreaterThan(Long value) {
			addCriterion("region_level4 >", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4GreaterThanOrEqualTo(Long value) {
			addCriterion("region_level4 >=", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4LessThan(Long value) {
			addCriterion("region_level4 <", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4LessThanOrEqualTo(Long value) {
			addCriterion("region_level4 <=", value, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4In(List<Long> values) {
			addCriterion("region_level4 in", values, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4NotIn(List<Long> values) {
			addCriterion("region_level4 not in", values, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4Between(Long value1, Long value2) {
			addCriterion("region_level4 between", value1, value2, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andRegionLevel4NotBetween(Long value1, Long value2) {
			addCriterion("region_level4 not between", value1, value2, "regionLevel4");
			return (Criteria) this;
		}

		public Criteria andAddressIsNull() {
			addCriterion("address is null");
			return (Criteria) this;
		}

		public Criteria andAddressIsNotNull() {
			addCriterion("address is not null");
			return (Criteria) this;
		}

		public Criteria andAddressEqualTo(String value) {
			addCriterion("address =", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotEqualTo(String value) {
			addCriterion("address <>", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThan(String value) {
			addCriterion("address >", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressGreaterThanOrEqualTo(String value) {
			addCriterion("address >=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThan(String value) {
			addCriterion("address <", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLessThanOrEqualTo(String value) {
			addCriterion("address <=", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressLike(String value) {
			addCriterion("address like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotLike(String value) {
			addCriterion("address not like", value, "address");
			return (Criteria) this;
		}

		public Criteria andAddressIn(List<String> values) {
			addCriterion("address in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotIn(List<String> values) {
			addCriterion("address not in", values, "address");
			return (Criteria) this;
		}

		public Criteria andAddressBetween(String value1, String value2) {
			addCriterion("address between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andAddressNotBetween(String value1, String value2) {
			addCriterion("address not between", value1, value2, "address");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNull() {
			addCriterion("is_built_in is null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNotNull() {
			addCriterion("is_built_in is not null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInEqualTo(Byte value) {
			addCriterion("is_built_in =", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotEqualTo(Byte value) {
			addCriterion("is_built_in <>", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThan(Byte value) {
			addCriterion("is_built_in >", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_built_in >=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThan(Byte value) {
			addCriterion("is_built_in <", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThanOrEqualTo(Byte value) {
			addCriterion("is_built_in <=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIn(List<Byte> values) {
			addCriterion("is_built_in in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotIn(List<Byte> values) {
			addCriterion("is_built_in not in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in not between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsDisableIsNull() {
			addCriterion("is_disable is null");
			return (Criteria) this;
		}

		public Criteria andIsDisableIsNotNull() {
			addCriterion("is_disable is not null");
			return (Criteria) this;
		}

		public Criteria andIsDisableEqualTo(Byte value) {
			addCriterion("is_disable =", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableNotEqualTo(Byte value) {
			addCriterion("is_disable <>", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableGreaterThan(Byte value) {
			addCriterion("is_disable >", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_disable >=", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableLessThan(Byte value) {
			addCriterion("is_disable <", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableLessThanOrEqualTo(Byte value) {
			addCriterion("is_disable <=", value, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableIn(List<Byte> values) {
			addCriterion("is_disable in", values, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableNotIn(List<Byte> values) {
			addCriterion("is_disable not in", values, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableBetween(Byte value1, Byte value2) {
			addCriterion("is_disable between", value1, value2, "isDisable");
			return (Criteria) this;
		}

		public Criteria andIsDisableNotBetween(Byte value1, Byte value2) {
			addCriterion("is_disable not between", value1, value2, "isDisable");
			return (Criteria) this;
		}

		public Criteria andLatlngIsNull() {
			addCriterion("latLng is null");
			return (Criteria) this;
		}

		public Criteria andLatlngIsNotNull() {
			addCriterion("latLng is not null");
			return (Criteria) this;
		}

		public Criteria andLatlngEqualTo(String value) {
			addCriterion("latLng =", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngNotEqualTo(String value) {
			addCriterion("latLng <>", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngGreaterThan(String value) {
			addCriterion("latLng >", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngGreaterThanOrEqualTo(String value) {
			addCriterion("latLng >=", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngLessThan(String value) {
			addCriterion("latLng <", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngLessThanOrEqualTo(String value) {
			addCriterion("latLng <=", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngLike(String value) {
			addCriterion("latLng like", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngNotLike(String value) {
			addCriterion("latLng not like", value, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngIn(List<String> values) {
			addCriterion("latLng in", values, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngNotIn(List<String> values) {
			addCriterion("latLng not in", values, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngBetween(String value1, String value2) {
			addCriterion("latLng between", value1, value2, "latlng");
			return (Criteria) this;
		}

		public Criteria andLatlngNotBetween(String value1, String value2) {
			addCriterion("latLng not between", value1, value2, "latlng");
			return (Criteria) this;
		}

		public Criteria andLinkPersonIsNull() {
			addCriterion("link_person is null");
			return (Criteria) this;
		}

		public Criteria andLinkPersonIsNotNull() {
			addCriterion("link_person is not null");
			return (Criteria) this;
		}

		public Criteria andLinkPersonEqualTo(String value) {
			addCriterion("link_person =", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonNotEqualTo(String value) {
			addCriterion("link_person <>", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonGreaterThan(String value) {
			addCriterion("link_person >", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonGreaterThanOrEqualTo(String value) {
			addCriterion("link_person >=", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonLessThan(String value) {
			addCriterion("link_person <", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonLessThanOrEqualTo(String value) {
			addCriterion("link_person <=", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonLike(String value) {
			addCriterion("link_person like", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonNotLike(String value) {
			addCriterion("link_person not like", value, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonIn(List<String> values) {
			addCriterion("link_person in", values, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonNotIn(List<String> values) {
			addCriterion("link_person not in", values, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonBetween(String value1, String value2) {
			addCriterion("link_person between", value1, value2, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkPersonNotBetween(String value1, String value2) {
			addCriterion("link_person not between", value1, value2, "linkPerson");
			return (Criteria) this;
		}

		public Criteria andLinkMobileIsNull() {
			addCriterion("link_mobile is null");
			return (Criteria) this;
		}

		public Criteria andLinkMobileIsNotNull() {
			addCriterion("link_mobile is not null");
			return (Criteria) this;
		}

		public Criteria andLinkMobileEqualTo(String value) {
			addCriterion("link_mobile =", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileNotEqualTo(String value) {
			addCriterion("link_mobile <>", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileGreaterThan(String value) {
			addCriterion("link_mobile >", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileGreaterThanOrEqualTo(String value) {
			addCriterion("link_mobile >=", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileLessThan(String value) {
			addCriterion("link_mobile <", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileLessThanOrEqualTo(String value) {
			addCriterion("link_mobile <=", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileLike(String value) {
			addCriterion("link_mobile like", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileNotLike(String value) {
			addCriterion("link_mobile not like", value, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileIn(List<String> values) {
			addCriterion("link_mobile in", values, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileNotIn(List<String> values) {
			addCriterion("link_mobile not in", values, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileBetween(String value1, String value2) {
			addCriterion("link_mobile between", value1, value2, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andLinkMobileNotBetween(String value1, String value2) {
			addCriterion("link_mobile not between", value1, value2, "linkMobile");
			return (Criteria) this;
		}

		public Criteria andWorkTimeIsNull() {
			addCriterion("work_time is null");
			return (Criteria) this;
		}

		public Criteria andWorkTimeIsNotNull() {
			addCriterion("work_time is not null");
			return (Criteria) this;
		}

		public Criteria andWorkTimeEqualTo(String value) {
			addCriterion("work_time =", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeNotEqualTo(String value) {
			addCriterion("work_time <>", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeGreaterThan(String value) {
			addCriterion("work_time >", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeGreaterThanOrEqualTo(String value) {
			addCriterion("work_time >=", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeLessThan(String value) {
			addCriterion("work_time <", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeLessThanOrEqualTo(String value) {
			addCriterion("work_time <=", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeLike(String value) {
			addCriterion("work_time like", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeNotLike(String value) {
			addCriterion("work_time not like", value, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeIn(List<String> values) {
			addCriterion("work_time in", values, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeNotIn(List<String> values) {
			addCriterion("work_time not in", values, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeBetween(String value1, String value2) {
			addCriterion("work_time between", value1, value2, "workTime");
			return (Criteria) this;
		}

		public Criteria andWorkTimeNotBetween(String value1, String value2) {
			addCriterion("work_time not between", value1, value2, "workTime");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}