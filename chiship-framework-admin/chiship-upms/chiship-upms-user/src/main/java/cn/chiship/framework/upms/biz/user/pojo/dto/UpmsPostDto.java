package cn.chiship.framework.upms.biz.user.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import cn.chiship.framework.upms.biz.user.entity.UpmsPost;

import javax.validation.constraints.*;

/**
 * 工作岗位表单
 * 2025/2/10
 *
 * @author LiJian
 */
@ApiModel(value = "工作岗位表单")
public class UpmsPostDto extends UpmsPost {
    @ApiModelProperty(value = "工作岗位编号")
    @NotNull
    @Length(min = 1, max = 10)
    private String code;

    @ApiModelProperty(value = "工作岗位名称")
    @NotNull
    @Length(min = 1, max = 20)
    private String name;

    @ApiModelProperty(value = "工作岗位类型")
    @NotNull
    @Length(min = 1, max = 50)
    private String type;

    @ApiModelProperty(value = "排序")
    @NotNull
    private Long orders;

    @ApiModelProperty(value = "工作岗位描述")
    private String description;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}