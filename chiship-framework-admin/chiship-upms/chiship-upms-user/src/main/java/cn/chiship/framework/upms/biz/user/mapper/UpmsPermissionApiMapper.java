package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.pojo.vo.UpmsOrganizationUserVo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 用户VOMapper
 *
 * @author jianhun
 * @date 2018/5/22
 */
@Mapper
public interface UpmsPermissionApiMapper {

	/**
	 * 根据用户ID查询组织
	 * @param userId
	 * @return
	 */
	List<UpmsOrganizationUserVo> selectOrganizationByUserId(String userId);

	/**
	 * 根据用户id获取所属的角色
	 * @param userId
	 * @return
	 */
	List<UpmsRole> selectRoleByUserId(String userId);

	/**
	 * 根据用户id获取所拥有的权限
	 * @param userId
	 * @return
	 */
	List<UpmsPermission> selectPermissionByUserId(String userId);

}
