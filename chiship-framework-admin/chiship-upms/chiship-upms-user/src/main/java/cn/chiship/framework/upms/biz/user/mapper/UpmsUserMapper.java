package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.pojo.vo.UpmsUserBaseVo;
import cn.chiship.framework.upms.biz.user.pojo.vo.UpmsUserRelationDataVo;
import cn.chiship.framework.upms.biz.user.pojo.vo.UpmsUserVo;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsUser;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserExample;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户Mapper
 *
 * @author lj
 */
public interface UpmsUserMapper extends BaseMapper<UpmsUser, UpmsUserExample> {

    /**
     * 查询用户、组织、角色
     *
     * @param params
     * @return
     */
    List<UpmsUserVo> selectUserAndOrgAndRole(Map<String, Object> params);

    /**
     * 查询用户与角色组装好的层级数据
     *
     * @param params
     * @return
     */
    List<UpmsUserRelationDataVo> selectUserRoleHierarchical(Map<String, Object> params);

    /**
     * 查询用户与岗位组装好的层级数据
     *
     * @param params
     * @return
     */
    List<UpmsUserRelationDataVo> selectUserPostHierarchical(Map<String, Object> params);


    /**
     * 查询用户与组织组装好的层级数据
     *
     * @param params
     * @return
     */
    List<UpmsUserRelationDataVo> selectUserOrgHierarchical(Map<String, Object> params);


    /**
     * 获取用户基本信息
     *
     * @param upmsUserExample
     * @return
     */
    List<UpmsUserBaseVo> selectBaseByExample(UpmsUserExample upmsUserExample);
}
