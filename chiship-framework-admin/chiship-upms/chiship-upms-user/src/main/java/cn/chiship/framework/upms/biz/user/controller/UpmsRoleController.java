package cn.chiship.framework.upms.biz.user.controller;

import cn.chiship.framework.common.annotation.DataScope;
import cn.chiship.framework.common.pojo.dto.BaseQueryEntityDto;
import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsRoleExample;
import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsRoleDto;
import cn.chiship.framework.upms.biz.user.service.UpmsRolePermissionService;
import cn.chiship.framework.upms.biz.user.service.UpmsRoleService;
import cn.chiship.framework.upms.biz.user.service.UpmsUserRoleService;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色控制层 2021/9/27
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/role")
@Api(tags = "用户角色")
public class UpmsRoleController extends BaseController<UpmsRole, UpmsRoleExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsRoleController.class);

	@Resource
	private UpmsRoleService upmsRoleService;

	@Resource
	private UpmsUserRoleService upmsUserRoleService;

	@Resource
	private UpmsRolePermissionService upmsRolePermissionService;

	@Override
	public BaseService getService() {
		return upmsRoleService;
	}

	@SystemOptionAnnotation(describe = "角色树形表格数据")
	@ApiOperation(value = "角色树形表格数据", notes = "角色树形表格数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键词", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/treeTable")
	@DataScope
	public ResponseEntity<BaseResult> treeTable(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			BaseQueryEntityDto baseQueryEntityDto) {
		PrintUtil.console(baseQueryEntityDto);
		return super.responseEntity(upmsRoleService.treeTable(keyword));
	}

	@SystemOptionAnnotation(describe = "保存角色数据", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
	@ApiOperation(value = "保存角色", notes = "保存角色数据")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsRoleDto upmsRoleDto) {
		UpmsRole upmsRole = new UpmsRole();
		BeanUtils.copyProperties(upmsRoleDto, upmsRole);
		return super.responseEntity(super.save(upmsRole));
	}

	/**
	 * 更新角色
	 */
	@SystemOptionAnnotation(describe = "根据主键更新角色数据", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
	@ApiOperation(value = "更新角色", notes = "根据主键更新角色数据")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "角色id", required = true, dataTypeClass = String.class,
			paramType = "path"), })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid UpmsRoleDto upmsRoleDto) {
		UpmsRole upmsRole = new UpmsRole();
		BeanUtils.copyProperties(upmsRoleDto, upmsRole);
		return super.responseEntity(super.update(id, upmsRole));
	}

	@SystemOptionAnnotation(describe = "根据角色主键集合删除系统的信息", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
	@ApiOperation(value = "删除角色数据", notes = "根据角色主键集合删除系统的信息")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {

		UpmsRoleExample upmsRoleExample = new UpmsRoleExample();
		upmsRoleExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(upmsRoleExample));

	}

	@SystemOptionAnnotation(describe = "获取指定角色下的用户")
	@ApiOperation(value = "获取指定角色下的用户", notes = "获取指定角色下的用户")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "roleId", value = "角色id", required = true, dataTypeClass = String.class,
					paramType = "query"), })
	@GetMapping(value = "pageUserByRoleId")
	public ResponseEntity<BaseResult> pageUserByRoleId(
			@RequestParam(required = false, defaultValue = "1", value = "page") Long page,
			@RequestParam(required = false, defaultValue = "10", value = "limit") Long limit,
			@RequestParam(value = "roleId") String roleId) {
		return super.responseEntity(upmsUserRoleService.pageUserByRoleId(page, limit, roleId));
	}

	@SystemOptionAnnotation(describe = "保存分配的用户", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
	@ApiOperation(value = "保存分配的用户", notes = "保存分配的用户")
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleIdList", value = "用户id集合", required = true,
			dataTypeClass = List.class, paramType = "body") })
	@PostMapping(value = "userRoleSave/{roleId}")
	public ResponseEntity<BaseResult> userPermissionSave(@PathVariable("roleId") String roleId,
			@RequestBody @Valid List<String> userIds) {
		return super.responseEntity(upmsUserRoleService.userRoleSave(roleId, userIds));
	}

	@SystemOptionAnnotation(describe = "删除分配的用户", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
	@ApiOperation(value = "删除分配的用户", notes = "删除分配的用户")
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleIdList", value = "用户id集合", required = true,
			dataTypeClass = List.class, paramType = "body") })
	@PostMapping(value = "userRoleRemove/{roleId}")
	public ResponseEntity<BaseResult> userPermissionRemove(@PathVariable("roleId") String roleId,
			@RequestBody @Valid List<String> userIds) {
		return super.responseEntity(upmsUserRoleService.userRoleRemove(roleId, userIds));
	}

	@SystemOptionAnnotation(describe = "获取指定角色下的权限")
	@ApiOperation(value = "获取指定角色下的权限")
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleId", value = "角色id", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "getPermissionByRoleId")
	public ResponseEntity<BaseResult> getPermissionByRoleId(@RequestParam("roleId") String roleId) {
		return super.responseEntity(upmsRolePermissionService.getPermissionByRoleId(roleId));
	}

	@SystemOptionAnnotation(describe = "保存分配的权限", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
	@ApiOperation(value = "保存分配的权限")
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleId", value = "角色id", required = true,
			dataTypeClass = String.class, paramType = "path"), })
	@PostMapping(value = "rolePermissionSave/{roleId}")
	public ResponseEntity<BaseResult> rolePermissionSave(@PathVariable("roleId") String roleId,
			@RequestBody @Valid List<String> permissionIds) {
		return super.responseEntity(upmsRolePermissionService.rolePermissionSave(roleId, permissionIds));
	}

	@SystemOptionAnnotation(describe = "取消分配的权限", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
	@ApiOperation(value = "取消分配的权限")
	@ApiImplicitParams({ @ApiImplicitParam(name = "roleId", value = "角色id", required = true,
			dataTypeClass = String.class, paramType = "path"), })
	@PostMapping(value = "rolePermissionCancel/{roleId}")
	public ResponseEntity<BaseResult> rolePermissionCancel(@PathVariable("roleId") String roleId,
			@RequestBody @Valid List<String> permissionIds) {
		return super.responseEntity(upmsRolePermissionService.rolePermissionCancel(roleId, permissionIds));
	}

}
