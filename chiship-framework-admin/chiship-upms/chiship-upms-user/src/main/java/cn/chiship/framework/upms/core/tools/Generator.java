package cn.chiship.framework.upms.core.tools;

import cn.chiship.sdk.core.util.PropertiesFileUtil;
import cn.chiship.sdk.framework.velocity.MybatisGeneratorTools;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * 代码生成类
 *
 * @author lijian
 */
public class Generator {

	private static String JDBC_DATABASE = PropertiesFileUtil.getInstance("jdbc").get("jdbc.database");

	private static String JDBC_DRIVER = PropertiesFileUtil.getInstance("jdbc").get("jdbc.driver");

	private static String JDBC_URL = PropertiesFileUtil.getInstance("jdbc").get("jdbc.url");

	private static String JDBC_USERNAME = PropertiesFileUtil.getInstance("jdbc").get("jdbc.username");

	private static String JDBC_PASSWORD = PropertiesFileUtil.getInstance("jdbc").get("jdbc.password");

	/**
	 * 模块
	 */
	private static String PACKAGE_NAME = "cn.chiship.framework.upms.biz.user";

	/**
	 * 表名前缀
	 */
	private static String TABLE_PREFIX = "upms_post";

	/**
	 * 需要insert后返回主键的表配置，key:表名,value:主键名
	 */
	private static Map<String, String> LAST_INSERT_ID_TABLES = Maps.newHashMapWithExpectedSize(7);

	/**
	 * 自动代码生成
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		MybatisGeneratorTools.generator(JDBC_DRIVER, JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD, JDBC_DATABASE,
				TABLE_PREFIX, PACKAGE_NAME, LAST_INSERT_ID_TABLES);

	}

}
