package cn.chiship.framework.upms.biz.user.pojo.vo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author lijian
 */
@ApiModel(value = "用户基本信息视图")
public class UpmsUserBaseVo implements Serializable {

	/**
	 * 创建时间
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 昵称
	 */
	private String nickName;

	/**
	 * 个性签名
	 */
	private String signature;

	/**
	 * 帐号
	 */
	private String userName;


	/**
	 * 姓名
	 */
	private String realName;

	/**
	 * 手机
	 */
	private String mobile;

	/**
	 * 头像
	 */
	private String avatar;

	/**
	 * 座机
	 */
	private String phone;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 性别(0:女，1：男，2：未知)
	 */
	private Byte gender;

	/**
	 * 工号
	 */
	private String userCode;


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
}
