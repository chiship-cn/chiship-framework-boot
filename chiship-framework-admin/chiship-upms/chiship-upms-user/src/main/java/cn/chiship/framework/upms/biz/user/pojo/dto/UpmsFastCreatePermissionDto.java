package cn.chiship.framework.upms.biz.user.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "快速创建权限表单")
public class UpmsFastCreatePermissionDto {

	@ApiModelProperty(value = "所属菜单")
	@NotNull
	@Length(min = 1)
	private String pid;

	@ApiModelProperty(value = "权限值")
	@NotNull
	@Length(min = 1)
	private String permissionValue;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getPermissionValue() {
		return permissionValue;
	}

	public void setPermissionValue(String permissionValue) {
		this.permissionValue = permissionValue;
	}

}
