package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.pojo.dto.UpmsUserOrganizationDto;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganization;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganizationExample;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 用户组织关联表业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsUserOrganizationService extends BaseService<UpmsUserOrganization, UpmsUserOrganizationExample> {

	/**
	 * 根据用户ID查询组织与用户关联详情
	 * @param userId
	 * @return
	 */
	List<JSONObject> selectUserOrg(String userId);

	/**
	 * 校验用户是否在组织下添加
	 * @param userId
	 * @param orgId
	 * @return
	 */
	Boolean validateExist(String userId, String orgId);

	/**
	 * 保存用户机构关联关系
	 * @param upmsUserOrganizationDto
	 */
	void saveUserOrganization(UpmsUserOrganizationDto upmsUserOrganizationDto);

	/**
	 * 根据机构主键获取所有层级
	 * @param treeNumber
	 * @param json
	 * @return
	 */
	JSONObject getParentInfoByTreeNumber(String treeNumber, JSONObject json);

	/**
	 * 获取完整父级信息
	 * @param treeNumber
	 * @return
	 */
	String getParentOrgIdByTreeNumber(String treeNumber);

	/**
	 * 获取完整父级信息
	 * @param treeNumber
	 * @return
	 */
	String getParentOrgNameByTreeNumber(String treeNumber);

	/**
	 * 根据树状层级获取所有孩子元素主键
	 * @param treeNumber
	 * @return
	 */
	List<String> listOrgIdByTreeNumber(String treeNumber);

}
