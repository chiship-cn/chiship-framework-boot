package cn.chiship.framework.upms.biz.user.controller;

import cn.chiship.sdk.core.annotation.Authorization;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.user.service.UpmsUserOnlineService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 在线用户控制层 2021/10/5
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/userOnline")
@Api(tags = "在线用户")
public class UpmsUserOnlineController {

	@Resource
	private UpmsUserOnlineService upmsUserOnlineService;

	@ApiOperation(value = "在线用户分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "userName", value = "用户名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "ip", value = "操作IP地址", dataTypeClass = String.class, paramType = "query") })
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "1", value = "page") Long page,
			@RequestParam(required = false, defaultValue = "10", value = "limit") Long limit,
			@RequestParam(required = false, value = "ip") String ip,
			@RequestParam(required = false, value = "userName") String userName) {
		return new ResponseEntity<>(upmsUserOnlineService.page(page, limit, userName, ip), HttpStatus.OK);
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "强制下线")
	@ApiOperation(value = "强制下线")
	@PostMapping(value = "/forceLogout")
	public ResponseEntity<BaseResult> remove(@RequestBody String sessionId) {
		return new ResponseEntity<>(upmsUserOnlineService.forceLogout(sessionId), HttpStatus.OK);
	}

}
