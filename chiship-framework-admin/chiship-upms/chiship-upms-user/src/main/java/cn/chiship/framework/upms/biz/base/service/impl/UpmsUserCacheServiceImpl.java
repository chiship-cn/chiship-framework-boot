package cn.chiship.framework.upms.biz.base.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.upms.biz.base.service.UpmsUserCacheService;
import cn.chiship.framework.upms.biz.user.entity.UpmsOrganization;
import cn.chiship.framework.upms.biz.user.entity.UpmsOrganizationExample;
import cn.chiship.framework.upms.biz.user.mapper.UpmsOrganizationMapper;
import cn.chiship.framework.upms.biz.user.service.UpmsPostService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lijian
 */
@Service
public class UpmsUserCacheServiceImpl implements UpmsUserCacheService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserCacheServiceImpl.class);

    @Resource
    RedisService redisService;

    @Resource
    UpmsOrganizationMapper upmsOrganizationMapper;

    @Resource
    UpmsPostService upmsPostService;

    @Override
    public void cacheOrganizations() {
        String key = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_ORG_PREFIX);
        redisService.del(key);
        UpmsOrganizationExample upmsOrganizationExample = new UpmsOrganizationExample();
        upmsOrganizationExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO);
        List<UpmsOrganization> upmsOrganizations = upmsOrganizationMapper.selectByExample(upmsOrganizationExample);
        upmsOrganizations.forEach(organization -> {
            redisService.hset(key, organization.getId(), organization);
            redisService.hset(key, organization.getTreeNumber(), organization);

        });
    }

    @Override
    public void cachePosts() {
        upmsPostService.cachePosts();
    }
}
