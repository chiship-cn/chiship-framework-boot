package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganizationExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserOrganization;

/**
 * @author lj 用户与组织关联表Mapper
 */
public interface UpmsUserOrganizationMapper extends BaseMapper<UpmsUserOrganization, UpmsUserOrganizationExample> {

}
