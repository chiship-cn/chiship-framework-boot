package cn.chiship.framework.upms.biz.user.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.upms.biz.system.service.UpmsCategoryDictService;
import cn.chiship.framework.upms.biz.system.service.UpmsDataDictService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.user.mapper.UpmsPostMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsPost;
import cn.chiship.framework.upms.biz.user.entity.UpmsPostExample;
import cn.chiship.framework.upms.biz.user.service.UpmsPostService;
import cn.chiship.sdk.framework.pojo.vo.DataDictItemVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 工作岗位业务接口实现层
 * 2025/2/10
 *
 * @author lijian
 */
@Service
public class UpmsPostServiceImpl extends BaseServiceImpl<UpmsPost, UpmsPostExample> implements UpmsPostService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsPostServiceImpl.class);

    @Resource
    UpmsPostMapper upmsPostMapper;
    @Resource
    RedisService redisService;
    @Resource
    UpmsDataDictService upmsDataDictService;

    @Override
    public JSONObject assembleData(UpmsPost upmsPost) {
        if (upmsPost == null) {
            return new JSONObject();
        }
        JSONObject json = JSON.parseObject(JSON.toJSONString(upmsPost));
        if (!StringUtil.isNullOrEmpty(upmsPost.getType())) {
            json.put("_type", upmsDataDictService.getDictItemNameFromCache("postType", upmsPost.getType()));
        }
        return json;
    }

    @Override
    public BaseResult tree(List<UpmsPost> upmsPosts) {
        List<DataDictItemVo> dataDictItemVos = upmsDataDictService.findByCodeFromCache("postType");
        List<JSONObject> dataJsons = new ArrayList<>();
        for (DataDictItemVo dataDictItemVo : dataDictItemVos) {
            JSONObject json = new JSONObject();
            json.put("id", dataDictItemVo.getDataDictItemCode());
            json.put("name", dataDictItemVo.getDataDictItemName());
            json.put("type", 1);
            List<UpmsPost> children = upmsPosts.stream().filter(upmsPost -> upmsPost.getType().equals(dataDictItemVo.getDataDictItemCode())).collect(Collectors.toList());
            List<JSONObject> childrenJsons = new ArrayList<>();
            for (UpmsPost post : children) {
                JSONObject childrenJson = new JSONObject();
                childrenJson.put("id", post.getId());
                childrenJson.put("name", post.getName());
                childrenJson.put("type", 2);
                childrenJsons.add(childrenJson);
            }
            if (!childrenJsons.isEmpty()) {
                json.put("children", childrenJsons);
                dataJsons.add(json);
            }
        }
        return BaseResult.ok(dataJsons);
    }

    @Override
    public void cachePosts() {
        String key = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_POST_PREFIX);
        redisService.del(key);
        UpmsPostExample upmsPostExample = new UpmsPostExample();
        upmsPostExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO);
        List<UpmsPost> upmsPosts = upmsPostMapper.selectByExample(upmsPostExample);
        upmsPosts.forEach(upmsPost -> {
            redisService.hset(key, upmsPost.getId(), upmsPost);
        });
    }

    @Override
    public JSONObject getCacheById(String id) {
        if (StringUtil.isNull(id)) {
            return null;
        }
        String itemKey = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_POST_PREFIX);
        if (!redisService.hHasKey(itemKey, id)) {
            return null;
        }
        UpmsPost upmsPost = (UpmsPost) redisService.hget(itemKey, id);
        return assembleData(upmsPost);
    }

    @Override
    public String getCacheNameById(String id, Boolean isType) {
        JSONObject json = getCacheById(id);
        if (ObjectUtils.isEmpty(json)) {
            return "";
        }
        if (Boolean.TRUE.equals(isType)) {
            return String.format("[%s]%s", json.getString("_type"), json.getString("name"));
        }
        return json.getString("name");
    }

    @Override
    public BaseResult insertSelective(UpmsPost upmsPost) {
        UpmsPostExample postExample = new UpmsPostExample();
        postExample.createCriteria().andCodeEqualTo(upmsPost.getCode());
        if (countByExample(postExample) > 0) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "该岗位编号已存在，请重新输入");
        }
        BaseResult baseResult = super.insertSelective(upmsPost);
        if (baseResult.isSuccess()) {
            cachePosts();
        }
        return baseResult;
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(UpmsPost upmsPost) {
        UpmsPostExample postExample = new UpmsPostExample();
        postExample.createCriteria().andCodeEqualTo(upmsPost.getCode()).andIdNotEqualTo(upmsPost.getId());
        if (countByExample(postExample) > 0) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "该岗位编号已存在，请重新输入");
        }
        BaseResult baseResult = super.updateByPrimaryKeySelective(upmsPost);
        if (baseResult.isSuccess()) {
            cachePosts();
        }
        return baseResult;
    }

    @Override
    public BaseResult deleteByExample(UpmsPostExample upmsPostExample) {
        BaseResult baseResult = super.deleteByExample(upmsPostExample);
        if (baseResult.isSuccess()) {
            cachePosts();
        }
        return baseResult;
    }

    @Override
    public BaseResult changeStatus(String id, Byte status) {
        UpmsPost upmsPost = upmsPostMapper.selectByPrimaryKey(id);
        if (ObjectUtils.isEmpty(upmsPost)) {
            return BaseResult.error("无效标识");
        }
        upmsPost.setStatus(status);
        upmsPostMapper.updateByPrimaryKeySelective(upmsPost);
        return BaseResult.ok();
    }
}
