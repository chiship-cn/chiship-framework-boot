package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-06-29
 */
public class UpmsOrganization implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 组织才有
	 */
	private String logo;

	/**
	 * 所属上级
	 */
	private String pid;

	/**
	 * 编号 组织才有
	 */
	private String organizationCode;

	/**
	 * 类型 1 组织 2 部门
	 */
	private Byte type;

	/**
	 * 组织全称
	 */
	private String organizationName;

	/**
	 * 排序
	 */
	private Integer sortOrders;

	/**
	 * 组织级别 只有组织才有 保留字段
	 */
	private String organizationLevel;

	/**
	 * 机构类型
	 */
	private String organizationCategory;

	/**
	 * 机构标签
	 */
	private String organizationTag;

	/**
	 * 营业执照
	 */
	private String businessLicense;

	/**
	 * 统一社会信用代码
	 */
	private String socialCreditCode;

	/**
	 * 机构层级树
	 */
	private String treeNumber;

	/**
	 * 邀请码
	 */
	private String invitationCode;

	/**
	 * 组织描述
	 */
	private String description;

	/**
	 * 组织才有
	 */
	private String phone;

	/**
	 * 传真 组织才有
	 */
	private String fax;

	/**
	 * 负责人信息：JSON格式{id:'',username:',realname:'',mobile:''}
	 */
	private String chargeInfo;

	/**
	 * 省
	 */
	private Long regionLevel1;

	/**
	 * 市
	 */
	private Long regionLevel2;

	/**
	 * 县/区
	 */
	private Long regionLevel3;

	/**
	 * 街道
	 */
	private Long regionLevel4;

	/**
	 * 详细地区
	 */
	private String address;

	/**
	 * 是否内置 0：否 1：是
	 */
	private Byte isBuiltIn;

	/**
	 * 是否禁用 0 否 1是
	 */
	private Byte isDisable;

	/**
	 * 经纬度
	 */
	private String latlng;

	/**
	 * 联系人姓名
	 */
	private String linkPerson;

	/**
	 * 联系人电话
	 */
	private String linkMobile;

	/**
	 * 工作时间
	 */
	private String workTime;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Integer getSortOrders() {
		return sortOrders;
	}

	public void setSortOrders(Integer sortOrders) {
		this.sortOrders = sortOrders;
	}

	public String getOrganizationLevel() {
		return organizationLevel;
	}

	public void setOrganizationLevel(String organizationLevel) {
		this.organizationLevel = organizationLevel;
	}

	public String getOrganizationCategory() {
		return organizationCategory;
	}

	public void setOrganizationCategory(String organizationCategory) {
		this.organizationCategory = organizationCategory;
	}

	public String getOrganizationTag() {
		return organizationTag;
	}

	public void setOrganizationTag(String organizationTag) {
		this.organizationTag = organizationTag;
	}

	public String getBusinessLicense() {
		return businessLicense;
	}

	public void setBusinessLicense(String businessLicense) {
		this.businessLicense = businessLicense;
	}

	public String getSocialCreditCode() {
		return socialCreditCode;
	}

	public void setSocialCreditCode(String socialCreditCode) {
		this.socialCreditCode = socialCreditCode;
	}

	public String getTreeNumber() {
		return treeNumber;
	}

	public void setTreeNumber(String treeNumber) {
		this.treeNumber = treeNumber;
	}

	public String getInvitationCode() {
		return invitationCode;
	}

	public void setInvitationCode(String invitationCode) {
		this.invitationCode = invitationCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(String chargeInfo) {
		this.chargeInfo = chargeInfo;
	}

	public Long getRegionLevel1() {
		return regionLevel1;
	}

	public void setRegionLevel1(Long regionLevel1) {
		this.regionLevel1 = regionLevel1;
	}

	public Long getRegionLevel2() {
		return regionLevel2;
	}

	public void setRegionLevel2(Long regionLevel2) {
		this.regionLevel2 = regionLevel2;
	}

	public Long getRegionLevel3() {
		return regionLevel3;
	}

	public void setRegionLevel3(Long regionLevel3) {
		this.regionLevel3 = regionLevel3;
	}

	public Long getRegionLevel4() {
		return regionLevel4;
	}

	public void setRegionLevel4(Long regionLevel4) {
		this.regionLevel4 = regionLevel4;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Byte getIsBuiltIn() {
		return isBuiltIn;
	}

	public void setIsBuiltIn(Byte isBuiltIn) {
		this.isBuiltIn = isBuiltIn;
	}

	public Byte getIsDisable() {
		return isDisable;
	}

	public void setIsDisable(Byte isDisable) {
		this.isDisable = isDisable;
	}

	public String getLatlng() {
		return latlng;
	}

	public void setLatlng(String latlng) {
		this.latlng = latlng;
	}

	public String getLinkPerson() {
		return linkPerson;
	}

	public void setLinkPerson(String linkPerson) {
		this.linkPerson = linkPerson;
	}

	public String getLinkMobile() {
		return linkMobile;
	}

	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}

	public String getWorkTime() {
		return workTime;
	}

	public void setWorkTime(String workTime) {
		this.workTime = workTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", logo=").append(logo);
		sb.append(", pid=").append(pid);
		sb.append(", organizationCode=").append(organizationCode);
		sb.append(", type=").append(type);
		sb.append(", organizationName=").append(organizationName);
		sb.append(", sortOrders=").append(sortOrders);
		sb.append(", organizationLevel=").append(organizationLevel);
		sb.append(", organizationCategory=").append(organizationCategory);
		sb.append(", organizationTag=").append(organizationTag);
		sb.append(", businessLicense=").append(businessLicense);
		sb.append(", socialCreditCode=").append(socialCreditCode);
		sb.append(", treeNumber=").append(treeNumber);
		sb.append(", invitationCode=").append(invitationCode);
		sb.append(", description=").append(description);
		sb.append(", phone=").append(phone);
		sb.append(", fax=").append(fax);
		sb.append(", chargeInfo=").append(chargeInfo);
		sb.append(", regionLevel1=").append(regionLevel1);
		sb.append(", regionLevel2=").append(regionLevel2);
		sb.append(", regionLevel3=").append(regionLevel3);
		sb.append(", regionLevel4=").append(regionLevel4);
		sb.append(", address=").append(address);
		sb.append(", isBuiltIn=").append(isBuiltIn);
		sb.append(", isDisable=").append(isDisable);
		sb.append(", latlng=").append(latlng);
		sb.append(", linkPerson=").append(linkPerson);
		sb.append(", linkMobile=").append(linkMobile);
		sb.append(", workTime=").append(workTime);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsOrganization other = (UpmsOrganization) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getLogo() == null ? other.getLogo() == null : this.getLogo().equals(other.getLogo()))
				&& (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
				&& (this.getOrganizationCode() == null ? other.getOrganizationCode() == null
						: this.getOrganizationCode().equals(other.getOrganizationCode()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getOrganizationName() == null ? other.getOrganizationName() == null
						: this.getOrganizationName().equals(other.getOrganizationName()))
				&& (this.getSortOrders() == null ? other.getSortOrders() == null
						: this.getSortOrders().equals(other.getSortOrders()))
				&& (this.getOrganizationLevel() == null ? other.getOrganizationLevel() == null
						: this.getOrganizationLevel().equals(other.getOrganizationLevel()))
				&& (this.getOrganizationCategory() == null ? other.getOrganizationCategory() == null
						: this.getOrganizationCategory().equals(other.getOrganizationCategory()))
				&& (this.getOrganizationTag() == null ? other.getOrganizationTag() == null
						: this.getOrganizationTag().equals(other.getOrganizationTag()))
				&& (this.getBusinessLicense() == null ? other.getBusinessLicense() == null
						: this.getBusinessLicense().equals(other.getBusinessLicense()))
				&& (this.getSocialCreditCode() == null ? other.getSocialCreditCode() == null
						: this.getSocialCreditCode().equals(other.getSocialCreditCode()))
				&& (this.getTreeNumber() == null ? other.getTreeNumber() == null
						: this.getTreeNumber().equals(other.getTreeNumber()))
				&& (this.getInvitationCode() == null ? other.getInvitationCode() == null
						: this.getInvitationCode().equals(other.getInvitationCode()))
				&& (this.getDescription() == null ? other.getDescription() == null
						: this.getDescription().equals(other.getDescription()))
				&& (this.getPhone() == null ? other.getPhone() == null : this.getPhone().equals(other.getPhone()))
				&& (this.getFax() == null ? other.getFax() == null : this.getFax().equals(other.getFax()))
				&& (this.getChargeInfo() == null ? other.getChargeInfo() == null
						: this.getChargeInfo().equals(other.getChargeInfo()))
				&& (this.getRegionLevel1() == null ? other.getRegionLevel1() == null
						: this.getRegionLevel1().equals(other.getRegionLevel1()))
				&& (this.getRegionLevel2() == null ? other.getRegionLevel2() == null
						: this.getRegionLevel2().equals(other.getRegionLevel2()))
				&& (this.getRegionLevel3() == null ? other.getRegionLevel3() == null
						: this.getRegionLevel3().equals(other.getRegionLevel3()))
				&& (this.getRegionLevel4() == null ? other.getRegionLevel4() == null
						: this.getRegionLevel4().equals(other.getRegionLevel4()))
				&& (this.getAddress() == null ? other.getAddress() == null
						: this.getAddress().equals(other.getAddress()))
				&& (this.getIsBuiltIn() == null ? other.getIsBuiltIn() == null
						: this.getIsBuiltIn().equals(other.getIsBuiltIn()))
				&& (this.getIsDisable() == null ? other.getIsDisable() == null
						: this.getIsDisable().equals(other.getIsDisable()))
				&& (this.getLatlng() == null ? other.getLatlng() == null : this.getLatlng().equals(other.getLatlng()))
				&& (this.getLinkPerson() == null ? other.getLinkPerson() == null
						: this.getLinkPerson().equals(other.getLinkPerson()))
				&& (this.getLinkMobile() == null ? other.getLinkMobile() == null
						: this.getLinkMobile().equals(other.getLinkMobile()))
				&& (this.getWorkTime() == null ? other.getWorkTime() == null
						: this.getWorkTime().equals(other.getWorkTime()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getLogo() == null) ? 0 : getLogo().hashCode());
		result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
		result = prime * result + ((getOrganizationCode() == null) ? 0 : getOrganizationCode().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getOrganizationName() == null) ? 0 : getOrganizationName().hashCode());
		result = prime * result + ((getSortOrders() == null) ? 0 : getSortOrders().hashCode());
		result = prime * result + ((getOrganizationLevel() == null) ? 0 : getOrganizationLevel().hashCode());
		result = prime * result + ((getOrganizationCategory() == null) ? 0 : getOrganizationCategory().hashCode());
		result = prime * result + ((getOrganizationTag() == null) ? 0 : getOrganizationTag().hashCode());
		result = prime * result + ((getBusinessLicense() == null) ? 0 : getBusinessLicense().hashCode());
		result = prime * result + ((getSocialCreditCode() == null) ? 0 : getSocialCreditCode().hashCode());
		result = prime * result + ((getTreeNumber() == null) ? 0 : getTreeNumber().hashCode());
		result = prime * result + ((getInvitationCode() == null) ? 0 : getInvitationCode().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getPhone() == null) ? 0 : getPhone().hashCode());
		result = prime * result + ((getFax() == null) ? 0 : getFax().hashCode());
		result = prime * result + ((getChargeInfo() == null) ? 0 : getChargeInfo().hashCode());
		result = prime * result + ((getRegionLevel1() == null) ? 0 : getRegionLevel1().hashCode());
		result = prime * result + ((getRegionLevel2() == null) ? 0 : getRegionLevel2().hashCode());
		result = prime * result + ((getRegionLevel3() == null) ? 0 : getRegionLevel3().hashCode());
		result = prime * result + ((getRegionLevel4() == null) ? 0 : getRegionLevel4().hashCode());
		result = prime * result + ((getAddress() == null) ? 0 : getAddress().hashCode());
		result = prime * result + ((getIsBuiltIn() == null) ? 0 : getIsBuiltIn().hashCode());
		result = prime * result + ((getIsDisable() == null) ? 0 : getIsDisable().hashCode());
		result = prime * result + ((getLatlng() == null) ? 0 : getLatlng().hashCode());
		result = prime * result + ((getLinkPerson() == null) ? 0 : getLinkPerson().hashCode());
		result = prime * result + ((getLinkMobile() == null) ? 0 : getLinkMobile().hashCode());
		result = prime * result + ((getWorkTime() == null) ? 0 : getWorkTime().hashCode());
		return result;
	}

}