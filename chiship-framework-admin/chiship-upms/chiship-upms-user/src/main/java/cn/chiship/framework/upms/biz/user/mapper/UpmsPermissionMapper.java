package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsPermissionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 权限Mapper
 *
 * @author lj
 */
public interface UpmsPermissionMapper extends BaseMapper<UpmsPermission, UpmsPermissionExample> {

    /**
     * 权限分配
     * @param ids
     * @return
     */
    List<String> getPidById(@Param("ids") List<String> ids);

    /**
     * 取消授权
     * @param ids
     * @return
     */
    List<String> getChildrenIdById(@Param("ids") List<String> ids);

}