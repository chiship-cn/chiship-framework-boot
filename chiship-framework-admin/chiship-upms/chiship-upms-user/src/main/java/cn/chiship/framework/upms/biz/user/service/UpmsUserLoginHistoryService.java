package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.common.enums.LoginTypeEnum;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistoryExample;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistory;

/**
 * 用户登录历史记录业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsUserLoginHistoryService extends BaseService<UpmsUserLoginHistory, UpmsUserLoginHistoryExample> {

	/**
	 * 记录用户登录离职
	 * @param userId
	 * @param userName
	 * @param realName
	 * @param remark
	 * @param isSuccess
	 * @param loginIp
	 * @param loginTypeEnum
	 */
	void loginHistory(String userId, String userName, String realName, String remark, byte isSuccess, String loginIp,
			LoginTypeEnum loginTypeEnum);

}
