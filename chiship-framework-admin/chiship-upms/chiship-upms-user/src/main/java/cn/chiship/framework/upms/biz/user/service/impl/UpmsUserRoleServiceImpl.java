package cn.chiship.framework.upms.biz.user.service.impl;

import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsUser;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserRoleExample;
import cn.chiship.framework.upms.biz.user.mapper.UpmsPermissionApiMapper;
import cn.chiship.framework.upms.biz.user.mapper.UpmsUserRoleMapper;
import cn.chiship.framework.upms.biz.user.pojo.vo.UpmsUserVo;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.framework.upms.biz.user.mapper.UpmsRoleMapper;
import cn.chiship.framework.upms.biz.user.service.UpmsUserRoleService;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户角色关联表业务接口实现层 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsUserRoleServiceImpl extends BaseServiceImpl<UpmsUserRole, UpmsUserRoleExample>
		implements UpmsUserRoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsUserRoleServiceImpl.class);

	@Resource
	UpmsUserRoleMapper upmsUserRoleMapper;

	@Resource
	UpmsRoleMapper upmsRoleMapper;

	@Resource
	UpmsPermissionApiMapper upmsPermissionApiMapper;

	@Override
	public BaseResult pageUserByRoleId(Long page, Long limit, String roleId) {

		Page pageResult = PageMethod.startPage(page.intValue(), limit.intValue());

		List<UpmsUser> users = upmsUserRoleMapper.selectUserByRoleId(roleId);
		List<UpmsUserVo> userVos = new ArrayList<>();
		for (UpmsUser user : users) {
			UpmsUserVo userVo = new UpmsUserVo();
			BeanUtils.copyProperties(user, userVo);
			userVo.setRoles(upmsPermissionApiMapper.selectRoleByUserId(user.getId()));
			userVos.add(userVo);
		}
		PageVo pageVo = new PageVo();
		pageVo.setRecords(userVos);
		pageVo.setTotal(pageResult.getTotal());
		pageVo.setPages(Long.valueOf(pageResult.getPages()));
		pageVo.setCurrent(page);
		pageVo.setSize(limit);
		return BaseResult.ok(pageVo);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResult userRoleSave(String roleId, List<String> userIds) {
		if (userIds.isEmpty()) {
			return BaseResult.error("请至少选择一条记录！");
		}
		UpmsRole upmsRole = upmsRoleMapper.selectByPrimaryKey(roleId);
		if (StringUtil.isNull(upmsRole)) {
			return BaseResult.error("角色不存在！");
		}
		UpmsUserRoleExample upmsUserRoleExample = new UpmsUserRoleExample();
		UpmsUserRoleExample.Criteria criteria = upmsUserRoleExample.createCriteria();
		criteria.andRoleIdEqualTo(roleId);
		List<UpmsUserRole> currentUserRoles = upmsUserRoleMapper.selectByExample(upmsUserRoleExample);
		List<String> listUserId = new ArrayList<>();
		for (UpmsUserRole userRole : currentUserRoles) {
			listUserId.add(userRole.getUserId());
		}
		/**
		 * 新增
		 */
		List<String> notExists = new ArrayList<>(userIds);
		notExists.removeAll(listUserId);
		if (!notExists.isEmpty()) {
			List<UpmsUserRole> userRoles = new ArrayList<>();
			for (String userId : notExists) {
				UpmsUserRole userRole = new UpmsUserRole();
				userRole.setId(SnowflakeIdUtil.generateStrId());
				userRole.setIsDeleted(BaseConstants.NO);
				userRole.setGmtCreated(System.currentTimeMillis());
				userRole.setGmtModified(System.currentTimeMillis());
				userRole.setRoleId(roleId);
				userRole.setUserId(userId);
				userRoles.add(userRole);
			}
			upmsUserRoleMapper.insertSelectiveBatch(userRoles);
		}
		LOGGER.info("人员分配成功");
		return BaseResult.ok("人员分配成功");

	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResult userRoleRemove(String roleId, List<String> userIds) {
		if (userIds.isEmpty()) {
			return BaseResult.error("请至少选择一条记录！");
		}
		UpmsUserRoleExample upmsUserRoleExample = new UpmsUserRoleExample();
		UpmsUserRoleExample.Criteria criteria = upmsUserRoleExample.createCriteria();
		criteria.andRoleIdEqualTo(roleId).andUserIdIn(userIds);

		upmsUserRoleMapper.deleteByExample(upmsUserRoleExample);
		LOGGER.info("人员取消授权成功");
		return BaseResult.ok("人员取消授权成功");

	}

}
