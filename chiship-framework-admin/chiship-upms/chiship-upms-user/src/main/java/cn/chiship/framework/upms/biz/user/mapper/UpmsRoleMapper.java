package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.framework.upms.biz.user.entity.UpmsRole;
import cn.chiship.framework.upms.biz.user.entity.UpmsRoleExample;
import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * 角色Mapper
 *
 * @author lj
 */
public interface UpmsRoleMapper extends BaseMapper<UpmsRole, UpmsRoleExample> {

}