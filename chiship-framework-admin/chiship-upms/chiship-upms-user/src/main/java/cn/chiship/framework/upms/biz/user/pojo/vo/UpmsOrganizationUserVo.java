package cn.chiship.framework.upms.biz.user.pojo.vo;

import cn.chiship.framework.upms.biz.user.entity.UpmsOrganization;
import io.swagger.annotations.ApiModel;

/**
 * @author lijian
 */
@ApiModel(value = "组织用户视图")
public class UpmsOrganizationUserVo extends UpmsOrganization {

	/**
	 * 组织与用户关联主键
	 */
	private String userOrgId;

	/**
	 * 加入时间
	 */
	private Long addTime;

	/**
	 * 工作
	 */
	private String orgJob;

	/**
	 * 电话
	 */
	private String orgPhone;

	/**
	 * 排序
	 */
	private Integer orders;

	/**
	 * 用户主键
	 */
	private String userId;

	/**
	 * 用户账号
	 */
	private String userName;

	/**
	 * 用户电话
	 */
	private String userMobile;

	/**
	 * 真实姓名
	 */
	private String userRealName;

	public String getUserOrgId() {
		return userOrgId;
	}

	public void setUserOrgId(String userOrgId) {
		this.userOrgId = userOrgId;
	}

	public Long getAddTime() {
		return addTime;
	}

	public void setAddTime(Long addTime) {
		this.addTime = addTime;
	}

	public String getOrgJob() {
		return orgJob;
	}

	public void setOrgJob(String orgJob) {
		this.orgJob = orgJob;
	}

	public String getOrgPhone() {
		return orgPhone;
	}

	public void setOrgPhone(String orgPhone) {
		this.orgPhone = orgPhone;
	}

	public Integer getOrders() {
		return orders;
	}

	public void setOrders(Integer orders) {
		this.orders = orders;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

}
