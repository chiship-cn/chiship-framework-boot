package cn.chiship.framework.upms.biz.user.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "单位部门表单")
public class UpmsDepartmentDto {

	@ApiModelProperty(value = "上级单位id")
	@NotNull
	private String pid;

	@ApiModelProperty(value = "部门编号")
	@NotNull
	@Length(min = 1)
	private String organizationCode;

	@ApiModelProperty(value = "部门全称")
	@NotNull
	@Length(min = 1)
	private String organizationName;

	@ApiModelProperty(value = "排序")
	@NotNull
	@Min(1)
	private Integer sortOrders;

	@ApiModelProperty(value = "单位类型")
	@NotNull
	private String organizationCategory;

	@ApiModelProperty(value = "部门描述")
	private String description;

	@ApiModelProperty(value = "部门电话")
	private String phone;

	@ApiModelProperty(value = "部门传真")
	private String fax;

	@ApiModelProperty(value = "负责人信息")
	private String chargeInfo;

	@ApiModelProperty(value = "联系人")
	private String linkPerson;

	@ApiModelProperty(value = "联系电话")
	private String linkMobile;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public Integer getSortOrders() {
		return sortOrders;
	}

	public void setSortOrders(Integer sortOrders) {
		this.sortOrders = sortOrders;
	}

	public String getOrganizationCategory() {
		return organizationCategory;
	}

	public void setOrganizationCategory(String organizationCategory) {
		this.organizationCategory = organizationCategory;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getChargeInfo() {
		return chargeInfo;
	}

	public void setChargeInfo(String chargeInfo) {
		this.chargeInfo = chargeInfo;
	}

	public String getLinkPerson() {
		return linkPerson;
	}

	public void setLinkPerson(String linkPerson) {
		this.linkPerson = linkPerson;
	}

	public String getLinkMobile() {
		return linkMobile;
	}

	public void setLinkMobile(String linkMobile) {
		this.linkMobile = linkMobile;
	}

}
