package cn.chiship.framework.upms.biz.base.service;

/**
 * 缓存数据
 *
 * @author lijian
 */
public interface UpmsUserCacheService {

	/**
	 * 缓存组织
	 */
	void cacheOrganizations();

	/**
	 * 缓存工作岗位
	 */

	void cachePosts();

}
