package cn.chiship.framework.upms.biz.user.mapper;

import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistory;
import cn.chiship.framework.upms.biz.user.entity.UpmsUserLoginHistoryExample;

/**
 * @author lijian
 */
public interface UpmsUserLoginHistoryMapper extends BaseMapper<UpmsUserLoginHistory, UpmsUserLoginHistoryExample> {

}