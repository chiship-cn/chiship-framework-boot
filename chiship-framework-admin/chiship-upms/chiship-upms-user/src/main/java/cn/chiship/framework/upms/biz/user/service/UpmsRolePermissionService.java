package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermission;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.user.entity.UpmsRolePermissionExample;

import java.util.List;

/**
 * 角色权限关联表业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsRolePermissionService extends BaseService<UpmsRolePermission, UpmsRolePermissionExample> {

	/**
	 * 根据角色ID获取权限
	 * @param roleId
	 * @return
	 */
	BaseResult getPermissionByRoleId(String roleId);

	/**
	 * 保存
	 * @param roleId
	 * @param permissionIds
	 * @return
	 */
	BaseResult rolePermissionSave(String roleId, List<String> permissionIds);

	/**
	 * 删除
	 * @param roleId
	 * @param permissionIds
	 * @return
	 */
	BaseResult rolePermissionCancel(String roleId, List<String> permissionIds);

}
