package cn.chiship.framework.upms.biz.user.service;

import cn.chiship.framework.upms.biz.user.entity.UpmsOrganizationExample;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.user.entity.UpmsOrganization;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 组织业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsOrganizationService extends BaseService<UpmsOrganization, UpmsOrganizationExample> {

    /**
     * 机构编码生成
     *
     * @param pid
     * @param type
     * @return
     */
    BaseResult generateOrgCode(String pid, Byte type);

    /**
     * 生成层级
     *
     * @param pid
     * @return
     */
    BaseResult generateTreeNumber(String pid);

    /**
     * 生成邀请码
     *
     * @param id
     * @return
     */
    BaseResult invitationCode(String id);

    /**
     * 根据邀请码获取机构信息
     *
     * @param code
     * @return
     */
    BaseResult getByInvitation(String code);

    /**
     * 设置负责人
     *
     * @param organizationId
     * @param userId
     * @param realName
     * @return
     */
    BaseResult setChargePerson(String organizationId, String userId, String realName);

    /**
     * 组织机构调整
     *
     * @param organizationId
     * @param targetOrg
     * @return
     */
    BaseResult adjustment(String organizationId, String targetOrg);

    /**
     * 删除组织机构
     *
     * @param orgIds
     * @return
     */
    BaseResult removeOrg(List<String> orgIds);

    /**
     * 缓存获取TreeNumber
     *
     * @param orgId
     * @return
     */
    String cacheGetTreeNumberById(String orgId);

    /**
     * 根据主键获取完整信息
     *
     * @param orgId 主键
     * @return
     */
    void getParentInfoById(String orgId, JSONObject json);

    /**
     * 根据层级编码获取完整信息
     *
     * @param treeNumber
     * @return
     */
    void getParentInfoByTreeNumber(String treeNumber, JSONObject json);

    /**
     * 根据主键获取机构名
     *
     * @param orgId  主键
     * @param isFull 是否完整
     * @return
     */
    String getOrgNameById(String orgId, Boolean isFull);

    /**
     * 解散企业
     *
     * @param orgId
     * @return
     */
    BaseResult dissolution(String orgId);

    /**
     * 根据TreeNumber加载树
     *
     * @param treeNumber
     * @return
     */
    BaseResult loadTreeByTreeNumber(String treeNumber);

    /**
     * 根据集合获取
     *
     * @param ids
     * @return
     */
    BaseResult listByIds(String ids);

}
