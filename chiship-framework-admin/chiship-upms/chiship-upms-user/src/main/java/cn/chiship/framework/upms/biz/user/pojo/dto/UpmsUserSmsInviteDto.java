package cn.chiship.framework.upms.biz.user.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;
import java.util.List;

/**
 * @author LiJian
 */
@ApiModel(value = "短信邀请用户")
public class UpmsUserSmsInviteDto {

    @ApiModelProperty(value = "机构", required = true)
    @NotNull(message = "机构" + BaseTipConstants.NOT_EMPTY)
    private String orgId;

    @ApiModelProperty(value = "手机号集合", required = true)
    @NotNull(message = "手机号集合" + BaseTipConstants.NOT_EMPTY)
    private List<String> mobiles;

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public List<String> getMobiles() {
        return mobiles;
    }

    public void setMobiles(List<String> mobiles) {
        this.mobiles = mobiles;
    }
}
