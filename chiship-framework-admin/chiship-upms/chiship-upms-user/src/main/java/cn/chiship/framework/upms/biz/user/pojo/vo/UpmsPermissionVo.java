package cn.chiship.framework.upms.biz.user.pojo.vo;

import cn.chiship.framework.upms.biz.user.entity.UpmsPermission;
import io.swagger.annotations.ApiModel;

import java.util.List;

/**
 * @author lijian
 */
@ApiModel(value = "菜单权限视图")
public class UpmsPermissionVo extends UpmsPermission {

	private List<UpmsPermissionVo> permissionVos;

	public List<UpmsPermissionVo> getPermissionVos() {
		return permissionVos;
	}

	public void setPermissionVos(List<UpmsPermissionVo> permissionVos) {
		this.permissionVos = permissionVos;
	}

}
