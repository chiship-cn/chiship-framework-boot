package cn.chiship.framework.upms.biz.user.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class UpmsUserLoginHistoryExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsUserLoginHistoryExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNull() {
			addCriterion("user_name is null");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNotNull() {
			addCriterion("user_name is not null");
			return (Criteria) this;
		}

		public Criteria andUserNameEqualTo(String value) {
			addCriterion("user_name =", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotEqualTo(String value) {
			addCriterion("user_name <>", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThan(String value) {
			addCriterion("user_name >", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("user_name >=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThan(String value) {
			addCriterion("user_name <", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThanOrEqualTo(String value) {
			addCriterion("user_name <=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLike(String value) {
			addCriterion("user_name like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotLike(String value) {
			addCriterion("user_name not like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameIn(List<String> values) {
			addCriterion("user_name in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotIn(List<String> values) {
			addCriterion("user_name not in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameBetween(String value1, String value2) {
			addCriterion("user_name between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotBetween(String value1, String value2) {
			addCriterion("user_name not between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNull() {
			addCriterion("real_name is null");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNotNull() {
			addCriterion("real_name is not null");
			return (Criteria) this;
		}

		public Criteria andRealNameEqualTo(String value) {
			addCriterion("real_name =", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotEqualTo(String value) {
			addCriterion("real_name <>", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThan(String value) {
			addCriterion("real_name >", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("real_name >=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThan(String value) {
			addCriterion("real_name <", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThanOrEqualTo(String value) {
			addCriterion("real_name <=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLike(String value) {
			addCriterion("real_name like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotLike(String value) {
			addCriterion("real_name not like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameIn(List<String> values) {
			addCriterion("real_name in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotIn(List<String> values) {
			addCriterion("real_name not in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameBetween(String value1, String value2) {
			addCriterion("real_name between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotBetween(String value1, String value2) {
			addCriterion("real_name not between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andLoginIpIsNull() {
			addCriterion("login_ip is null");
			return (Criteria) this;
		}

		public Criteria andLoginIpIsNotNull() {
			addCriterion("login_ip is not null");
			return (Criteria) this;
		}

		public Criteria andLoginIpEqualTo(String value) {
			addCriterion("login_ip =", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpNotEqualTo(String value) {
			addCriterion("login_ip <>", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpGreaterThan(String value) {
			addCriterion("login_ip >", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpGreaterThanOrEqualTo(String value) {
			addCriterion("login_ip >=", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpLessThan(String value) {
			addCriterion("login_ip <", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpLessThanOrEqualTo(String value) {
			addCriterion("login_ip <=", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpLike(String value) {
			addCriterion("login_ip like", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpNotLike(String value) {
			addCriterion("login_ip not like", value, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpIn(List<String> values) {
			addCriterion("login_ip in", values, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpNotIn(List<String> values) {
			addCriterion("login_ip not in", values, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpBetween(String value1, String value2) {
			addCriterion("login_ip between", value1, value2, "loginIp");
			return (Criteria) this;
		}

		public Criteria andLoginIpNotBetween(String value1, String value2) {
			addCriterion("login_ip not between", value1, value2, "loginIp");
			return (Criteria) this;
		}

		public Criteria andBrowserIsNull() {
			addCriterion("browser is null");
			return (Criteria) this;
		}

		public Criteria andBrowserIsNotNull() {
			addCriterion("browser is not null");
			return (Criteria) this;
		}

		public Criteria andBrowserEqualTo(String value) {
			addCriterion("browser =", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserNotEqualTo(String value) {
			addCriterion("browser <>", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserGreaterThan(String value) {
			addCriterion("browser >", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserGreaterThanOrEqualTo(String value) {
			addCriterion("browser >=", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserLessThan(String value) {
			addCriterion("browser <", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserLessThanOrEqualTo(String value) {
			addCriterion("browser <=", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserLike(String value) {
			addCriterion("browser like", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserNotLike(String value) {
			addCriterion("browser not like", value, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserIn(List<String> values) {
			addCriterion("browser in", values, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserNotIn(List<String> values) {
			addCriterion("browser not in", values, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserBetween(String value1, String value2) {
			addCriterion("browser between", value1, value2, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserNotBetween(String value1, String value2) {
			addCriterion("browser not between", value1, value2, "browser");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionIsNull() {
			addCriterion("browser_version is null");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionIsNotNull() {
			addCriterion("browser_version is not null");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionEqualTo(String value) {
			addCriterion("browser_version =", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionNotEqualTo(String value) {
			addCriterion("browser_version <>", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionGreaterThan(String value) {
			addCriterion("browser_version >", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionGreaterThanOrEqualTo(String value) {
			addCriterion("browser_version >=", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionLessThan(String value) {
			addCriterion("browser_version <", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionLessThanOrEqualTo(String value) {
			addCriterion("browser_version <=", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionLike(String value) {
			addCriterion("browser_version like", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionNotLike(String value) {
			addCriterion("browser_version not like", value, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionIn(List<String> values) {
			addCriterion("browser_version in", values, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionNotIn(List<String> values) {
			addCriterion("browser_version not in", values, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionBetween(String value1, String value2) {
			addCriterion("browser_version between", value1, value2, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andBrowserVersionNotBetween(String value1, String value2) {
			addCriterion("browser_version not between", value1, value2, "browserVersion");
			return (Criteria) this;
		}

		public Criteria andOsIsNull() {
			addCriterion("os is null");
			return (Criteria) this;
		}

		public Criteria andOsIsNotNull() {
			addCriterion("os is not null");
			return (Criteria) this;
		}

		public Criteria andOsEqualTo(String value) {
			addCriterion("os =", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsNotEqualTo(String value) {
			addCriterion("os <>", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsGreaterThan(String value) {
			addCriterion("os >", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsGreaterThanOrEqualTo(String value) {
			addCriterion("os >=", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsLessThan(String value) {
			addCriterion("os <", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsLessThanOrEqualTo(String value) {
			addCriterion("os <=", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsLike(String value) {
			addCriterion("os like", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsNotLike(String value) {
			addCriterion("os not like", value, "os");
			return (Criteria) this;
		}

		public Criteria andOsIn(List<String> values) {
			addCriterion("os in", values, "os");
			return (Criteria) this;
		}

		public Criteria andOsNotIn(List<String> values) {
			addCriterion("os not in", values, "os");
			return (Criteria) this;
		}

		public Criteria andOsBetween(String value1, String value2) {
			addCriterion("os between", value1, value2, "os");
			return (Criteria) this;
		}

		public Criteria andOsNotBetween(String value1, String value2) {
			addCriterion("os not between", value1, value2, "os");
			return (Criteria) this;
		}

		public Criteria andLngIsNull() {
			addCriterion("lng is null");
			return (Criteria) this;
		}

		public Criteria andLngIsNotNull() {
			addCriterion("lng is not null");
			return (Criteria) this;
		}

		public Criteria andLngEqualTo(String value) {
			addCriterion("lng =", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotEqualTo(String value) {
			addCriterion("lng <>", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngGreaterThan(String value) {
			addCriterion("lng >", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngGreaterThanOrEqualTo(String value) {
			addCriterion("lng >=", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLessThan(String value) {
			addCriterion("lng <", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLessThanOrEqualTo(String value) {
			addCriterion("lng <=", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngLike(String value) {
			addCriterion("lng like", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotLike(String value) {
			addCriterion("lng not like", value, "lng");
			return (Criteria) this;
		}

		public Criteria andLngIn(List<String> values) {
			addCriterion("lng in", values, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotIn(List<String> values) {
			addCriterion("lng not in", values, "lng");
			return (Criteria) this;
		}

		public Criteria andLngBetween(String value1, String value2) {
			addCriterion("lng between", value1, value2, "lng");
			return (Criteria) this;
		}

		public Criteria andLngNotBetween(String value1, String value2) {
			addCriterion("lng not between", value1, value2, "lng");
			return (Criteria) this;
		}

		public Criteria andLatIsNull() {
			addCriterion("lat is null");
			return (Criteria) this;
		}

		public Criteria andLatIsNotNull() {
			addCriterion("lat is not null");
			return (Criteria) this;
		}

		public Criteria andLatEqualTo(String value) {
			addCriterion("lat =", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotEqualTo(String value) {
			addCriterion("lat <>", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatGreaterThan(String value) {
			addCriterion("lat >", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatGreaterThanOrEqualTo(String value) {
			addCriterion("lat >=", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLessThan(String value) {
			addCriterion("lat <", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLessThanOrEqualTo(String value) {
			addCriterion("lat <=", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatLike(String value) {
			addCriterion("lat like", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotLike(String value) {
			addCriterion("lat not like", value, "lat");
			return (Criteria) this;
		}

		public Criteria andLatIn(List<String> values) {
			addCriterion("lat in", values, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotIn(List<String> values) {
			addCriterion("lat not in", values, "lat");
			return (Criteria) this;
		}

		public Criteria andLatBetween(String value1, String value2) {
			addCriterion("lat between", value1, value2, "lat");
			return (Criteria) this;
		}

		public Criteria andLatNotBetween(String value1, String value2) {
			addCriterion("lat not between", value1, value2, "lat");
			return (Criteria) this;
		}

		public Criteria andIsSuccessIsNull() {
			addCriterion("is_success is null");
			return (Criteria) this;
		}

		public Criteria andIsSuccessIsNotNull() {
			addCriterion("is_success is not null");
			return (Criteria) this;
		}

		public Criteria andIsSuccessEqualTo(Byte value) {
			addCriterion("is_success =", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessNotEqualTo(Byte value) {
			addCriterion("is_success <>", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessGreaterThan(Byte value) {
			addCriterion("is_success >", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_success >=", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessLessThan(Byte value) {
			addCriterion("is_success <", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessLessThanOrEqualTo(Byte value) {
			addCriterion("is_success <=", value, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessIn(List<Byte> values) {
			addCriterion("is_success in", values, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessNotIn(List<Byte> values) {
			addCriterion("is_success not in", values, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessBetween(Byte value1, Byte value2) {
			addCriterion("is_success between", value1, value2, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andIsSuccessNotBetween(Byte value1, Byte value2) {
			addCriterion("is_success not between", value1, value2, "isSuccess");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andLoginAddressIsNull() {
			addCriterion("login_address is null");
			return (Criteria) this;
		}

		public Criteria andLoginAddressIsNotNull() {
			addCriterion("login_address is not null");
			return (Criteria) this;
		}

		public Criteria andLoginAddressEqualTo(String value) {
			addCriterion("login_address =", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressNotEqualTo(String value) {
			addCriterion("login_address <>", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressGreaterThan(String value) {
			addCriterion("login_address >", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressGreaterThanOrEqualTo(String value) {
			addCriterion("login_address >=", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressLessThan(String value) {
			addCriterion("login_address <", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressLessThanOrEqualTo(String value) {
			addCriterion("login_address <=", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressLike(String value) {
			addCriterion("login_address like", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressNotLike(String value) {
			addCriterion("login_address not like", value, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressIn(List<String> values) {
			addCriterion("login_address in", values, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressNotIn(List<String> values) {
			addCriterion("login_address not in", values, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressBetween(String value1, String value2) {
			addCriterion("login_address between", value1, value2, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginAddressNotBetween(String value1, String value2) {
			addCriterion("login_address not between", value1, value2, "loginAddress");
			return (Criteria) this;
		}

		public Criteria andLoginTypeIsNull() {
			addCriterion("login_type is null");
			return (Criteria) this;
		}

		public Criteria andLoginTypeIsNotNull() {
			addCriterion("login_type is not null");
			return (Criteria) this;
		}

		public Criteria andLoginTypeEqualTo(String value) {
			addCriterion("login_type =", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeNotEqualTo(String value) {
			addCriterion("login_type <>", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeGreaterThan(String value) {
			addCriterion("login_type >", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeGreaterThanOrEqualTo(String value) {
			addCriterion("login_type >=", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeLessThan(String value) {
			addCriterion("login_type <", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeLessThanOrEqualTo(String value) {
			addCriterion("login_type <=", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeLike(String value) {
			addCriterion("login_type like", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeNotLike(String value) {
			addCriterion("login_type not like", value, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeIn(List<String> values) {
			addCriterion("login_type in", values, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeNotIn(List<String> values) {
			addCriterion("login_type not in", values, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeBetween(String value1, String value2) {
			addCriterion("login_type between", value1, value2, "loginType");
			return (Criteria) this;
		}

		public Criteria andLoginTypeNotBetween(String value1, String value2) {
			addCriterion("login_type not between", value1, value2, "loginType");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}