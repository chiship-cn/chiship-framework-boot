package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "参数配置表单")
public class UpmsConfigSettingDto {

	@ApiModelProperty(value = "配置主键", required = true)
	@NotNull(message = "配置主键" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	private String id;

	@ApiModelProperty(value = "配置数值")
	@NotNull(message = "配置数值" + BaseTipConstants.NOT_EMPTY)
	private String paramValue;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

}
