package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * App版本Dto 2020-05-07 09:30
 *
 * @author: gengce
 **/
@ApiModel(value = "App版本信息表单")
public class UpmsAppVersionDto {

	@ApiModelProperty(value = "App唯一识别", required = true)
	@NotEmpty(message = "App唯一识别" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String appCode;

	@ApiModelProperty(value = "App名称", required = true)
	@NotEmpty(message = "App名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String name;

	@ApiModelProperty(value = "版本号", required = true)
	@NotNull(message = "版本号" + BaseTipConstants.NOT_EMPTY)
	@Min(value = 1)
	private Integer appVersion;

	@ApiModelProperty(value = "版本名称", required = true)
	@NotEmpty(message = "版本名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 18)
	private String appVersionName;

	@ApiModelProperty(value = "更新内容", required = true)
	@Length(min = 1, max = 1000)
	private String appRemark;

	@ApiModelProperty(value = "下载地址", required = true)
	@Length(min = 1, max = 200)
	private String appUrl;

	@ApiModelProperty(value = "app类型(0 android  1 IOS)", required = true)
	@NotNull(message = "App唯一识别" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte appType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(Integer appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppVersionName() {
		return appVersionName;
	}

	public void setAppVersionName(String appVersionName) {
		this.appVersionName = appVersionName;
	}

	public String getAppRemark() {
		return appRemark;
	}

	public void setAppRemark(String appRemark) {
		this.appRemark = appRemark;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public Byte getAppType() {
		return appType;
	}

	public void setAppType(Byte appType) {
		this.appType = appType;
	}

}
