package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UpmsSmsCode implements Serializable {

	/**
	 * 主键
	 *
	 * @mbg.generated
	 */
	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 操作描述
	 *
	 * @mbg.generated
	 */
	private String codeDevice;

	private String code;

	/**
	 * 0 禁用 1 启用
	 *
	 * @mbg.generated
	 */
	private Byte isUsed;

	private Long expiryTime;

	private Long referenceCode;

	/**
	 * 类型 0 短信验证码 1 邮箱验证码
	 *
	 * @mbg.generated
	 */
	private Byte type;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCodeDevice() {
		return codeDevice;
	}

	public void setCodeDevice(String codeDevice) {
		this.codeDevice = codeDevice;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Byte getIsUsed() {
		return isUsed;
	}

	public void setIsUsed(Byte isUsed) {
		this.isUsed = isUsed;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public Long getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(Long referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", codeDevice=").append(codeDevice);
		sb.append(", code=").append(code);
		sb.append(", isUsed=").append(isUsed);
		sb.append(", expiryTime=").append(expiryTime);
		sb.append(", referenceCode=").append(referenceCode);
		sb.append(", type=").append(type);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsSmsCode other = (UpmsSmsCode) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCodeDevice() == null ? other.getCodeDevice() == null
						: this.getCodeDevice().equals(other.getCodeDevice()))
				&& (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
				&& (this.getIsUsed() == null ? other.getIsUsed() == null : this.getIsUsed().equals(other.getIsUsed()))
				&& (this.getExpiryTime() == null ? other.getExpiryTime() == null
						: this.getExpiryTime().equals(other.getExpiryTime()))
				&& (this.getReferenceCode() == null ? other.getReferenceCode() == null
						: this.getReferenceCode().equals(other.getReferenceCode()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCodeDevice() == null) ? 0 : getCodeDevice().hashCode());
		result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
		result = prime * result + ((getIsUsed() == null) ? 0 : getIsUsed().hashCode());
		result = prime * result + ((getExpiryTime() == null) ? 0 : getExpiryTime().hashCode());
		result = prime * result + ((getReferenceCode() == null) ? 0 : getReferenceCode().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		return result;
	}

}