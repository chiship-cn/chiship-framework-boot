package cn.chiship.framework.upms.core.enums;

/**
 * 通知公告模块类型
 */
public enum NoticeModuleTypeEnums {

	NOTICE_MODULE_TYPE_DEFAULT("default", "默认"),;

	/**
	 * 类型
	 */
	private String type;

	/**
	 * 描述
	 */
	private String message;

	NoticeModuleTypeEnums(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
