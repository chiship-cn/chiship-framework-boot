package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "字典数据项表单")
public class UpmsDataDictItemDto {

	@ApiModelProperty(value = "所属字典组")
	@NotNull
	@Length(min = 12, max = 36)
	private String pid;

	@ApiModelProperty(value = "字典代码")
	@NotNull
	@Length(min = 1, max = 45)
	private String dataDictCode;

	@ApiModelProperty(value = "字典名称")
	@NotNull
	@Length(min = 1, max = 45)
	private String dataDictName;

	@ApiModelProperty(value = "颜色值")
	@Length(min = 6, max = 20, message = "请输入16进制或RGB颜色值")
	private String colorValue;

	@ApiModelProperty(value = "是否标准数据", required = true)
	@NotNull(message = "是否标准数据" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isNationalStandard;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	@Max(100)
	private Long sortValue;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getDataDictCode() {
		return dataDictCode;
	}

	public void setDataDictCode(String dataDictCode) {
		this.dataDictCode = dataDictCode;
	}

	public String getDataDictName() {
		return dataDictName;
	}

	public void setDataDictName(String dataDictName) {
		this.dataDictName = dataDictName;
	}

	public String getColorValue() {
		return colorValue;
	}

	public void setColorValue(String colorValue) {
		this.colorValue = colorValue;
	}

	public Byte getIsNationalStandard() {
		return isNationalStandard;
	}

	public void setIsNationalStandard(Byte isNationalStandard) {
		this.isNationalStandard = isNationalStandard;
	}

	public Long getSortValue() {
		return sortValue;
	}

	public void setSortValue(Long sortValue) {
		this.sortValue = sortValue;
	}

}
