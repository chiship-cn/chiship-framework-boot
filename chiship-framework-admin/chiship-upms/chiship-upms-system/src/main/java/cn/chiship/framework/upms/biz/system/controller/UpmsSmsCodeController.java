package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.upms.biz.system.service.UpmsSmsService;
import cn.chiship.sdk.cache.service.UserCacheService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsCodeService;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCode;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCodeExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 验证码控制层 2021/9/30
 *
 * @author lijian
 */
@RestController
@RequestMapping("/smsCode")
@Api(tags = "三方验证码")
public class UpmsSmsCodeController extends BaseController<UpmsSmsCode, UpmsSmsCodeExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSmsCodeController.class);

    @Resource
    private UpmsSmsCodeService upmsSmsCodeService;
    @Resource
    private UpmsSmsService upmsSmsService;
    @Resource
    private UserCacheService userCacheService;

    @Override
    public BaseService getService() {
        return upmsSmsCodeService;
    }

    @SystemOptionAnnotation(describe = "短信验证码分页")
    @ApiOperation(value = "短信验证码信息分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页数", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "codeDevice", value = "设备", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "短信类型（0 短信验证码  1 邮箱验证码）", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "isUsed", value = "是否使用", dataTypeClass = Byte.class, paramType = "query")

    })
    @GetMapping(value = "page")
    @Authorization
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "codeDevice") String codeDevice,
                                           @RequestParam(required = false, defaultValue = "", value = "isUsed") Byte isUsed,
                                           @RequestParam(required = false, defaultValue = "", value = "type") Byte type) {

        UpmsSmsCodeExample upmsSmsCodeExample = new UpmsSmsCodeExample();
        UpmsSmsCodeExample.Criteria criteria = upmsSmsCodeExample.createCriteria()
                .andIsDeletedEqualTo(BaseConstants.NO);

        if (!StringUtil.isNullOrEmpty(codeDevice)) {
            criteria.andCodeDeviceLike("%" + codeDevice + "%");
        }
        if (!StringUtil.isNull(isUsed)) {
            criteria.andIsUsedEqualTo(isUsed);
        }
        if (!StringUtil.isNull(type)) {
            criteria.andTypeEqualTo(type);
        }

        PageVo pageVo = super.page(upmsSmsCodeExample);

        return super.responseEntity(BaseResult.ok(pageVo));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除短信验证码数据")
    @ApiOperation(value = "删除短信验证码数据")
    @PostMapping(value = "remove")
    @Authorization
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsSmsCodeExample upmsSmsCodeExample = new UpmsSmsCodeExample();
        upmsSmsCodeExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsSmsCodeExample));

    }

    @SystemOptionAnnotation(describe = "获取手机验证码", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "获取手机验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "mobile", value = "手机号", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("mobile")
    public ResponseEntity<BaseResult> mobile(@RequestParam(value = "mobile") String mobile) {
        CacheUserVO userVO = null;
        try {
            userVO = userCacheService.getUser();
        } catch (Exception e) {

        }
        return super.responseEntity(upmsSmsService.sendSmsCode(mobile, userVO), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "获取邮箱验证码", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "获取邮箱验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "email", value = "电子邮箱", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("email")
    public ResponseEntity<BaseResult> email(@RequestParam(value = "email") String email) {
        return super.responseEntity(upmsSmsCodeService.get(email, Byte.valueOf("1")), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "校验验证码并设为已使用", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "校验验证码并设为已使用")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "device", value = "获取验证码的设备号", required = true, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "code", value = "验证码", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("verification")
    public ResponseEntity<BaseResult> verification(@RequestParam(value = "device") String device,
                                                   @RequestParam(value = "code") String code) {
        return super.responseEntity(upmsSmsCodeService.verification(device, code));
    }

    @SystemOptionAnnotation(describe = "核查验证码与设备号是否匹配", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "核查验证码与设备号是否匹配")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "device", value = "获取验证码的设备号", required = true, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "code", value = "验证码", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("check")
    public ResponseEntity<BaseResult> check(@RequestParam(value = "device") String device,
                                            @RequestParam(value = "code") String code) {
        return super.responseEntity(upmsSmsCodeService.check(device, code));
    }

}
