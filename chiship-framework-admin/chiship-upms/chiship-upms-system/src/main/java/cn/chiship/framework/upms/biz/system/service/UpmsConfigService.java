package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.entity.UpmsConfig;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigGroupDto;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigSettingDto;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfigExample;

import java.util.List;

/**
 * 系统配置业务接口层 2021/9/27
 *
 * @author lijian
 */
public interface UpmsConfigService extends BaseService<UpmsConfig, UpmsConfigExample> {

	/**
	 * 新增分组
	 * @param configGroupDto
	 * @return
	 */
	BaseResult saveGroup(UpmsConfigGroupDto configGroupDto);

	/**
	 * 编辑分组
	 * @param id
	 * @param configGroupDto
	 * @return
	 */
	BaseResult updateGroup(String id, UpmsConfigGroupDto configGroupDto);

	/**
	 * 批量修改配置
	 * @param configSettingDtos
	 * @return
	 */
	BaseResult updateConfig(List<UpmsConfigSettingDto> configSettingDtos);

	/**
	 * 缓存数据
	 */
	void cacheConfigs();

	/**
	 * 根据分组主键获取参数项目
	 * @param groupId
	 * @return
	 */
	BaseResult listByGroupId(String groupId);

	/**
	 * 根据分组编号获取参数项
	 * @param groupCode
	 * @return
	 */
	BaseResult listByGroupCode(String groupCode);

	/**
	 * 根据分组编号获取记录及孩子元素
	 * @param groupCode
	 * @return
	 */
	BaseResult getByGroupCode(String groupCode);

}
