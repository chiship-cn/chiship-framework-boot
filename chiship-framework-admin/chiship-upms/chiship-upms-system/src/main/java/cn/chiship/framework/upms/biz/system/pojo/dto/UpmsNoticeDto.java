package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * @author lijian
 */
@ApiModel(value = "通知公告表单")
public class UpmsNoticeDto {

	@ApiModelProperty(value = "公告标题", required = true)
	@NotEmpty(message = "公告标题" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String noticeTitle;

	@ApiModelProperty(value = "消息类型1:通知公告2:系统消息", required = true)
	@NotNull(message = "消息类型" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	@Max(2)
	private Byte noticeType;

	@ApiModelProperty(value = "通告用户类型（admin:管理人员，member:会员）", required = true)
	@NotEmpty(message = "通告用户类型" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = "admin|member", message = "用户类型必须为admin、member")
	private String noticeUserType;

	@ApiModelProperty(value = "通告对象类型（USER:指定用户，ALL:全体用户）", required = true)
	@NotEmpty(message = "通告对象类型" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = "USER|ALL", message = "用户类型必须为USER、ALL")
	private String noticeCategory;

	@ApiModelProperty(value = "接收者(通知时必传)", required = false)
	@Length(min = 1, max = 2000)
	private String receiver;

	@ApiModelProperty(value = "公告内容", required = true)
	private String noticeContent;

	@ApiModelProperty(value = "摘要")
	@Length(min = 0, max = 500)
	private String noticeAbstract;

	@ApiModelProperty(value = "附件")
	private String noticeFile;

	@ApiModelProperty(value = "结束时间")
	private Long endTime;

	@ApiModelProperty(value = "优先级（0低，1中，2高）", required = true)
	@NotNull(message = "优先级" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	@Max(2)
	private Byte priority;

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public Byte getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(Byte noticeType) {
		this.noticeType = noticeType;
	}

	public String getNoticeUserType() {
		return noticeUserType;
	}

	public void setNoticeUserType(String noticeUserType) {
		this.noticeUserType = noticeUserType;
	}

	public String getNoticeCategory() {
		return noticeCategory;
	}

	public void setNoticeCategory(String noticeCategory) {
		this.noticeCategory = noticeCategory;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	public String getNoticeAbstract() {
		return noticeAbstract;
	}

	public void setNoticeAbstract(String noticeAbstract) {
		this.noticeAbstract = noticeAbstract;
	}

	public String getNoticeFile() {
		return noticeFile;
	}

	public void setNoticeFile(String noticeFile) {
		this.noticeFile = noticeFile;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Byte getPriority() {
		return priority;
	}

	public void setPriority(Byte priority) {
		this.priority = priority;
	}

}
