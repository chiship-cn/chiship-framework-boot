package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lj
 * @date 2023-06-15
 */
public class UpmsDataDictExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsDataDictExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andPidIsNull() {
			addCriterion("pid is null");
			return (Criteria) this;
		}

		public Criteria andPidIsNotNull() {
			addCriterion("pid is not null");
			return (Criteria) this;
		}

		public Criteria andPidEqualTo(String value) {
			addCriterion("pid =", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotEqualTo(String value) {
			addCriterion("pid <>", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThan(String value) {
			addCriterion("pid >", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThanOrEqualTo(String value) {
			addCriterion("pid >=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThan(String value) {
			addCriterion("pid <", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThanOrEqualTo(String value) {
			addCriterion("pid <=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLike(String value) {
			addCriterion("pid like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotLike(String value) {
			addCriterion("pid not like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidIn(List<String> values) {
			addCriterion("pid in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotIn(List<String> values) {
			addCriterion("pid not in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidBetween(String value1, String value2) {
			addCriterion("pid between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotBetween(String value1, String value2) {
			addCriterion("pid not between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeIsNull() {
			addCriterion("data_dict_code is null");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeIsNotNull() {
			addCriterion("data_dict_code is not null");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeEqualTo(String value) {
			addCriterion("data_dict_code =", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeNotEqualTo(String value) {
			addCriterion("data_dict_code <>", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeGreaterThan(String value) {
			addCriterion("data_dict_code >", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeGreaterThanOrEqualTo(String value) {
			addCriterion("data_dict_code >=", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeLessThan(String value) {
			addCriterion("data_dict_code <", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeLessThanOrEqualTo(String value) {
			addCriterion("data_dict_code <=", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeLike(String value) {
			addCriterion("data_dict_code like", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeNotLike(String value) {
			addCriterion("data_dict_code not like", value, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeIn(List<String> values) {
			addCriterion("data_dict_code in", values, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeNotIn(List<String> values) {
			addCriterion("data_dict_code not in", values, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeBetween(String value1, String value2) {
			addCriterion("data_dict_code between", value1, value2, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictCodeNotBetween(String value1, String value2) {
			addCriterion("data_dict_code not between", value1, value2, "dataDictCode");
			return (Criteria) this;
		}

		public Criteria andDataDictNameIsNull() {
			addCriterion("data_dict_name is null");
			return (Criteria) this;
		}

		public Criteria andDataDictNameIsNotNull() {
			addCriterion("data_dict_name is not null");
			return (Criteria) this;
		}

		public Criteria andDataDictNameEqualTo(String value) {
			addCriterion("data_dict_name =", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameNotEqualTo(String value) {
			addCriterion("data_dict_name <>", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameGreaterThan(String value) {
			addCriterion("data_dict_name >", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameGreaterThanOrEqualTo(String value) {
			addCriterion("data_dict_name >=", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameLessThan(String value) {
			addCriterion("data_dict_name <", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameLessThanOrEqualTo(String value) {
			addCriterion("data_dict_name <=", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameLike(String value) {
			addCriterion("data_dict_name like", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameNotLike(String value) {
			addCriterion("data_dict_name not like", value, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameIn(List<String> values) {
			addCriterion("data_dict_name in", values, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameNotIn(List<String> values) {
			addCriterion("data_dict_name not in", values, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameBetween(String value1, String value2) {
			addCriterion("data_dict_name between", value1, value2, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andDataDictNameNotBetween(String value1, String value2) {
			addCriterion("data_dict_name not between", value1, value2, "dataDictName");
			return (Criteria) this;
		}

		public Criteria andColorValueIsNull() {
			addCriterion("color_value is null");
			return (Criteria) this;
		}

		public Criteria andColorValueIsNotNull() {
			addCriterion("color_value is not null");
			return (Criteria) this;
		}

		public Criteria andColorValueEqualTo(String value) {
			addCriterion("color_value =", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueNotEqualTo(String value) {
			addCriterion("color_value <>", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueGreaterThan(String value) {
			addCriterion("color_value >", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueGreaterThanOrEqualTo(String value) {
			addCriterion("color_value >=", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueLessThan(String value) {
			addCriterion("color_value <", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueLessThanOrEqualTo(String value) {
			addCriterion("color_value <=", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueLike(String value) {
			addCriterion("color_value like", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueNotLike(String value) {
			addCriterion("color_value not like", value, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueIn(List<String> values) {
			addCriterion("color_value in", values, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueNotIn(List<String> values) {
			addCriterion("color_value not in", values, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueBetween(String value1, String value2) {
			addCriterion("color_value between", value1, value2, "colorValue");
			return (Criteria) this;
		}

		public Criteria andColorValueNotBetween(String value1, String value2) {
			addCriterion("color_value not between", value1, value2, "colorValue");
			return (Criteria) this;
		}

		public Criteria andSortValueIsNull() {
			addCriterion("sort_value is null");
			return (Criteria) this;
		}

		public Criteria andSortValueIsNotNull() {
			addCriterion("sort_value is not null");
			return (Criteria) this;
		}

		public Criteria andSortValueEqualTo(Long value) {
			addCriterion("sort_value =", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueNotEqualTo(Long value) {
			addCriterion("sort_value <>", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueGreaterThan(Long value) {
			addCriterion("sort_value >", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueGreaterThanOrEqualTo(Long value) {
			addCriterion("sort_value >=", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueLessThan(Long value) {
			addCriterion("sort_value <", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueLessThanOrEqualTo(Long value) {
			addCriterion("sort_value <=", value, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueIn(List<Long> values) {
			addCriterion("sort_value in", values, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueNotIn(List<Long> values) {
			addCriterion("sort_value not in", values, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueBetween(Long value1, Long value2) {
			addCriterion("sort_value between", value1, value2, "sortValue");
			return (Criteria) this;
		}

		public Criteria andSortValueNotBetween(Long value1, Long value2) {
			addCriterion("sort_value not between", value1, value2, "sortValue");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNull() {
			addCriterion("is_built_in is null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNotNull() {
			addCriterion("is_built_in is not null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInEqualTo(Byte value) {
			addCriterion("is_built_in =", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotEqualTo(Byte value) {
			addCriterion("is_built_in <>", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThan(Byte value) {
			addCriterion("is_built_in >", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_built_in >=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThan(Byte value) {
			addCriterion("is_built_in <", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThanOrEqualTo(Byte value) {
			addCriterion("is_built_in <=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIn(List<Byte> values) {
			addCriterion("is_built_in in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotIn(List<Byte> values) {
			addCriterion("is_built_in not in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in not between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardIsNull() {
			addCriterion("is_national_standard is null");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardIsNotNull() {
			addCriterion("is_national_standard is not null");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardEqualTo(Byte value) {
			addCriterion("is_national_standard =", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardNotEqualTo(Byte value) {
			addCriterion("is_national_standard <>", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardGreaterThan(Byte value) {
			addCriterion("is_national_standard >", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_national_standard >=", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardLessThan(Byte value) {
			addCriterion("is_national_standard <", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardLessThanOrEqualTo(Byte value) {
			addCriterion("is_national_standard <=", value, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardIn(List<Byte> values) {
			addCriterion("is_national_standard in", values, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardNotIn(List<Byte> values) {
			addCriterion("is_national_standard not in", values, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardBetween(Byte value1, Byte value2) {
			addCriterion("is_national_standard between", value1, value2, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andIsNationalStandardNotBetween(Byte value1, Byte value2) {
			addCriterion("is_national_standard not between", value1, value2, "isNationalStandard");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}