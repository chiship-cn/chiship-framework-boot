package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;

import java.util.Map;

/**
 * 短信业务
 *
 * @author lijian
 */
public interface UpmsSmsService {

    /**
     * 发送验证码
     *
     * @param codeDevice 设备号码
     * @return
     */
    BaseResult sendSmsCode(String mobile, CacheUserVO cacheUserVO);

    /**
     * 发送短信
     *
     * @param template    模板编号，数据库缓存的key值，如：smsCodeTemplate
     * @param mobile
     * @param paramsMap
     * @param cacheUserVO
     * @return
     */
    BaseResult sendSms(String template, String mobile, Map<String, String> paramsMap, CacheUserVO cacheUserVO);
}
