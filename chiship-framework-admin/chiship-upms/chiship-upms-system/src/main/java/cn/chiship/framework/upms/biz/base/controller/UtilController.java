package cn.chiship.framework.upms.biz.base.controller;

import cn.chiship.framework.common.pojo.dto.IdCardOcrDto;
import cn.chiship.framework.common.pojo.dto.OcrDto;
import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.third.baidu.BaiDuOcrUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lijian
 */
@RestController
@RequestMapping("/util")
@Api(tags = "工具类控制器")
public class UtilController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UtilController.class);

	@Resource
	private GlobalCacheService globalCacheService;

	@ApiOperation(value = "获取系统配置(缓存)")
	@ApiImplicitParams({ @ApiImplicitParam(name = "keys", value = "系统配置Key集合,若多个以‘,’隔开", defaultValue = "systemDesc",
			required = true, dataTypeClass = List.class, paramType = "query") })
	@GetMapping(value = "/systemConfig")
	public ResponseEntity<BaseResult> systemConfig(@RequestParam(value = "keys") List<String> keys) {
		return new ResponseEntity<>(BaseResult.ok(globalCacheService.getSystemConfig(keys)), HttpStatus.OK);
	}

	@ApiOperation(value = "证件照OCR识别")
	@PostMapping(value = "/idCardOcr")
	public ResponseEntity<BaseResult> idCardOcr(@RequestBody IdCardOcrDto idCardOcrDto) {
		BaiDuOcrUtils baiDuOcrUtils = BaiDuOcrUtils.getInstance().config().token();
		BaseResult baseResult = baiDuOcrUtils.idCardOcr(idCardOcrDto.getBase64Image(), idCardOcrDto.getIsFront(), false);
		return new ResponseEntity<>(baseResult, HttpStatus.OK);

	}

	@ApiOperation(value = "OCR识别")
	@PostMapping(value = "/ocr")
	public ResponseEntity<BaseResult> ocr(@RequestBody OcrDto ocrDto) {
		BaiDuOcrUtils baiDuOcrUtils = BaiDuOcrUtils.getInstance().config().token();
		BaseResult baseResult;
		switch (ocrDto.getType()) {
		case 1:
			baseResult = baiDuOcrUtils.idCardOcr(ocrDto.getBase64Image(), true, false);
			break;
		case 2:
			baseResult = baiDuOcrUtils.bankCard(ocrDto.getBase64Image(), false);
			break;
		case 3:
			baseResult = baiDuOcrUtils.businessLicense(ocrDto.getBase64Image(), false);
			break;
		case 4:
			baseResult = baiDuOcrUtils.passport(ocrDto.getBase64Image(), false);
			break;
		case 5:
			baseResult = baiDuOcrUtils.socialSecurityCard(ocrDto.getBase64Image(), false);
			break;
		default:
			baseResult = BaseResult.error("暂不支持该类型卡片OCR识别!");
		}
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

}
