package cn.chiship.framework.upms.core.enums;

/**
 * 通知公告优先级
 */
public enum NoticePriorityEnums {

	NOTICE_PRIORITY_LOW(Byte.valueOf("0"), "低"),

	NOTICE_PRIORITY_MIDDLE(Byte.valueOf("1"), "中"),

	NOTICE_PRIORITY_HIGH(Byte.valueOf("2"), "高"),;

	/**
	 * 类型
	 */
	private Byte type;

	/**
	 * 描述
	 */
	private String message;

	NoticePriorityEnums(Byte type, String message) {
		this.type = type;
		this.message = message;
	}

	public Byte getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
