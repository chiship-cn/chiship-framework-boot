package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UpmsQuartzJob implements Serializable {

	/**
	 * 公告ID
	 *
	 * @mbg.generated
	 */
	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 创建者
	 *
	 * @mbg.generated
	 */
	private String createdBy;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 任务名称
	 *
	 * @mbg.generated
	 */
	private String jobTitle;

	/**
	 * 任务类名
	 *
	 * @mbg.generated
	 */
	private String jobClass;

	/**
	 * 任务方法
	 *
	 * @mbg.generated
	 */
	private String jobMethod;

	/**
	 * 任务表达式
	 *
	 * @mbg.generated
	 */
	private String jobCron;

	/**
	 * 任务参数
	 *
	 * @mbg.generated
	 */
	private String jobParam;

	/**
	 * 更新者
	 *
	 * @mbg.generated
	 */
	private String modifyBy;

	/**
	 * 状态（0正常 1关闭）
	 *
	 * @mbg.generated
	 */
	private Byte status;

	/**
	 * 备注
	 *
	 * @mbg.generated
	 */
	private String remark;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getJobMethod() {
		return jobMethod;
	}

	public void setJobMethod(String jobMethod) {
		this.jobMethod = jobMethod;
	}

	public String getJobCron() {
		return jobCron;
	}

	public void setJobCron(String jobCron) {
		this.jobCron = jobCron;
	}

	public String getJobParam() {
		return jobParam;
	}

	public void setJobParam(String jobParam) {
		this.jobParam = jobParam;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", jobTitle=").append(jobTitle);
		sb.append(", jobClass=").append(jobClass);
		sb.append(", jobMethod=").append(jobMethod);
		sb.append(", jobCron=").append(jobCron);
		sb.append(", jobParam=").append(jobParam);
		sb.append(", modifyBy=").append(modifyBy);
		sb.append(", status=").append(status);
		sb.append(", remark=").append(remark);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsQuartzJob other = (UpmsQuartzJob) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getJobTitle() == null ? other.getJobTitle() == null
						: this.getJobTitle().equals(other.getJobTitle()))
				&& (this.getJobClass() == null ? other.getJobClass() == null
						: this.getJobClass().equals(other.getJobClass()))
				&& (this.getJobMethod() == null ? other.getJobMethod() == null
						: this.getJobMethod().equals(other.getJobMethod()))
				&& (this.getJobCron() == null ? other.getJobCron() == null
						: this.getJobCron().equals(other.getJobCron()))
				&& (this.getJobParam() == null ? other.getJobParam() == null
						: this.getJobParam().equals(other.getJobParam()))
				&& (this.getModifyBy() == null ? other.getModifyBy() == null
						: this.getModifyBy().equals(other.getModifyBy()))
				&& (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getJobTitle() == null) ? 0 : getJobTitle().hashCode());
		result = prime * result + ((getJobClass() == null) ? 0 : getJobClass().hashCode());
		result = prime * result + ((getJobMethod() == null) ? 0 : getJobMethod().hashCode());
		result = prime * result + ((getJobCron() == null) ? 0 : getJobCron().hashCode());
		result = prime * result + ((getJobParam() == null) ? 0 : getJobParam().hashCode());
		result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
		result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		return result;
	}

}