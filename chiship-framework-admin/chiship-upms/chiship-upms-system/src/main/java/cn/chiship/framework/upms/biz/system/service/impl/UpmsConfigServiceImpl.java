package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.common.pojo.vo.SystemConfigVo;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfig;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfigExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsConfigMapper;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigGroupDto;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigSettingDto;
import cn.chiship.framework.upms.biz.system.service.UpmsConfigService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import javax.annotation.Resource;

import cn.chiship.sdk.framework.util.FrameworkUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * 系统配置业务接口实现层 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsConfigServiceImpl extends BaseServiceImpl<UpmsConfig, UpmsConfigExample> implements UpmsConfigService {

    @Resource
    UpmsConfigMapper upmsConfigMapper;

    @Resource
    RedisService redisService;

    @Override
    public BaseResult saveGroup(UpmsConfigGroupDto configGroupDto) {
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        upmsConfigExample.createCriteria().andParamCodeEqualTo(configGroupDto.getGroupCode());
        List<UpmsConfig> configs = upmsConfigMapper.selectByExample(upmsConfigExample);
        if (!configs.isEmpty()) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "分组编号已存在，请重新输入");
        }
        UpmsConfig upmsConfig = new UpmsConfig();
        upmsConfig.setParamCode(configGroupDto.getGroupCode());
        upmsConfig.setParamName(configGroupDto.getGroupName());
        upmsConfig.setOrders(configGroupDto.getOrders());
        upmsConfig.setPid(configGroupDto.getPid());
        upmsConfig.setType(Byte.valueOf("0"));
        return super.insertSelective(upmsConfig);
    }

    @Override
    public BaseResult updateGroup(String id, UpmsConfigGroupDto configGroupDto) {
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        upmsConfigExample.createCriteria().andParamCodeEqualTo(configGroupDto.getGroupCode()).andIdNotEqualTo(id);
        List<UpmsConfig> configs = upmsConfigMapper.selectByExample(upmsConfigExample);
        if (!configs.isEmpty()) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "分组编号已存在，请重新输入");
        }
        UpmsConfig upmsConfig = new UpmsConfig();
        upmsConfig.setId(id);
        upmsConfig.setParamCode(configGroupDto.getGroupCode());
        upmsConfig.setParamName(configGroupDto.getGroupName());
        upmsConfig.setOrders(configGroupDto.getOrders());
        upmsConfig.setPid(configGroupDto.getPid());
        return super.updateByPrimaryKeySelective(upmsConfig);
    }

    @Override
    public BaseResult insertSelective(UpmsConfig upmsConfig) {
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        upmsConfigExample.createCriteria().andParamCodeEqualTo(upmsConfig.getParamCode());
        List<UpmsConfig> configs = upmsConfigMapper.selectByExample(upmsConfigExample);
        if (!configs.isEmpty()) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "参数项编号已存在，请重新输入");
        }
        return super.insertSelective(upmsConfig);
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(UpmsConfig upmsConfig) {
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        upmsConfigExample.createCriteria().andParamCodeEqualTo(upmsConfig.getParamCode())
                .andIdNotEqualTo(upmsConfig.getId());
        List<UpmsConfig> configs = upmsConfigMapper.selectByExample(upmsConfigExample);
        if (!configs.isEmpty()) {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "参数项编号已存在，请重新输入");
        }
        return super.updateByPrimaryKeySelective(upmsConfig);
    }

    @Override
    public BaseResult updateConfig(List<UpmsConfigSettingDto> configSettingDtos) {
        for (UpmsConfigSettingDto configSettingDto : configSettingDtos) {
            UpmsConfig upmsConfig = new UpmsConfig();
            upmsConfig.setId(configSettingDto.getId());
            upmsConfig.setParamValue(configSettingDto.getParamValue());
            upmsConfigMapper.updateByPrimaryKeySelective(upmsConfig);
        }
        return BaseResult.ok();
    }

    @Override
    public void cacheConfigs() {
        String key = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_SYSTEM_CONFIG_PREFIX);
        redisService.del(key);
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        upmsConfigExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO);
        List<UpmsConfig> upmsConfigs = upmsConfigMapper.selectByExample(upmsConfigExample);
        upmsConfigs.forEach(upmsConfig -> {
            SystemConfigVo systemConfigVo = new SystemConfigVo();
            BeanUtils.copyProperties(upmsConfig, systemConfigVo);
            systemConfigVo.setRemark(upmsConfig.getParamDesc());
            redisService.hset(key, systemConfigVo.getParamCode(), systemConfigVo);
        });
    }

    @Override
    public BaseResult listByGroupId(String groupId) {
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        UpmsConfigExample.Criteria criteria = upmsConfigExample.createCriteria();
        criteria.andIsDeletedEqualTo(BaseConstants.NO).andPidEqualTo(groupId);
        upmsConfigExample.setOrderByClause(FrameworkUtil.formatSort("+orders"));
        List<UpmsConfig> upmsConfigs = upmsConfigMapper.selectByExample(upmsConfigExample);
        return BaseResult.ok(upmsConfigs);
    }

    @Override
    public BaseResult listByGroupCode(String groupCode) {
        BaseResult baseResult = getByGroupCode(groupCode);
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        Map<String, Object> map = (Map<String, Object>) baseResult.getData();
        return BaseResult.ok(map.get("children"));
    }

    @Override
    public BaseResult getByGroupCode(String groupCode) {
        SystemConfigVo systemConfigVo = (SystemConfigVo) redisService
                .hget(CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_SYSTEM_CONFIG_PREFIX), groupCode);
        if (StringUtil.isNull(systemConfigVo)) {
            return BaseResult.error("无效的组编号");
        }
        UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
        UpmsConfigExample.Criteria criteria = upmsConfigExample.createCriteria();
        criteria.andIsDeletedEqualTo(BaseConstants.NO).andPidEqualTo(systemConfigVo.getId());
        upmsConfigExample.setOrderByClause(FrameworkUtil.formatSort("+orders"));
        List<UpmsConfig> upmsConfigs = upmsConfigMapper.selectByExample(upmsConfigExample);
        Map<String, Object> map = new HashMap<>();
        map.put("record", systemConfigVo);
        map.put("children", upmsConfigs);
        return BaseResult.ok(map);
    }

}
