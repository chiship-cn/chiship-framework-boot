package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDict;
import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDictExample;

import java.util.Map;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-10-13
 */
public interface UpmsCategoryDictMapper extends BaseMapper<UpmsCategoryDict, UpmsCategoryDictExample> {

    /**
     * 根据父级获取最大编码
     *
     * @param pid
     * @return
     */
    Map<String, String> getTreeNumberByPid(String pid);

}