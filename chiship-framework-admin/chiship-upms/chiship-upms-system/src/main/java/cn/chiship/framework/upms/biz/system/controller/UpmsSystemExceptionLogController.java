package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLogExample;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsExceptionDealDto;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemExceptionLogService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.common.pojo.dto.SystemExceptionLogDto;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLog;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统异常日志控制层 2021/10/23
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/systemExceptionLog")
@Api(tags = "系统异常日志")
public class UpmsSystemExceptionLogController
		extends BaseController<UpmsSystemExceptionLog, UpmsSystemExceptionLogExample> {

	@Resource
	private UpmsSystemExceptionLogService upmsSystemExceptionLogService;

	@Override
	public BaseService getService() {
		return upmsSystemExceptionLogService;
	}

	@ApiOperation(value = "系统错误日志分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "+id", dataTypeClass = String.class,
					paramType = "query"),
			@ApiImplicitParam(name = "userName", value = "用户名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "systemName", value = "系统名称", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "requestType", value = "请求方式", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "requestId", value = "请求码", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "ip", value = "操作IP地址", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"), })
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "systemName") String systemName,
			@RequestParam(required = false, value = "requestId") String requestId,
			@RequestParam(required = false, value = "ip") String ip,
			@RequestParam(required = false, value = "requestType") String requestType,
			@RequestParam(required = false, value = "userName") String userName,
			@RequestParam(required = false, value = "realName") String realName,
			@RequestParam(required = false, value = "startTime") Long startTime,
			@RequestParam(required = false, value = "endTime") Long endTime) {

		UpmsSystemExceptionLogExample systemExceptionLogExample = new UpmsSystemExceptionLogExample();
		// 创造条件
		UpmsSystemExceptionLogExample.Criteria criteria = systemExceptionLogExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(ip)) {
			criteria.andIpLike("%" + ip + "%");
		}
		if (!StringUtil.isNullOrEmpty(requestType)) {
			criteria.andRequestTypeEqualTo(requestType);
		}
		if (!StringUtil.isNullOrEmpty(userName)) {
			criteria.andUserNameLike("%" + userName + "%");
		}
		if (!StringUtil.isNullOrEmpty(realName)) {
			criteria.andRealNameLike("%" + realName + "%");
		}
		if (!StringUtil.isNullOrEmpty(systemName)) {
			criteria.andSystemNameLike("%" + systemName + "%");
		}
		if (startTime != null) {
			criteria.andGmtCreatedGreaterThanOrEqualTo(startTime);
		}
		if (endTime != null) {
			criteria.andGmtCreatedLessThanOrEqualTo(endTime);
		}
		if (!StringUtil.isNullOrEmpty(requestId)) {
			criteria.andRequestIdLike("%" + requestId + "%");
		}

		return super.responseEntity(BaseResult.ok(super.page(systemExceptionLogExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除系统异常日志")
	@ApiOperation(value = "删除系统异常日志")
	@PostMapping(value = "/remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		UpmsSystemExceptionLogExample systemExceptionLogExample = new UpmsSystemExceptionLogExample();
		systemExceptionLogExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(systemExceptionLogExample));
	}

	@ApiOperation(value = "处理异常")
	@PostMapping(value = "/deal")
	public ResponseEntity<BaseResult> deal(@RequestBody @Valid UpmsExceptionDealDto upmsExceptionDealDto) {
		UpmsSystemExceptionLog systemExceptionLog = upmsSystemExceptionLogService
				.selectByPrimaryKey(upmsExceptionDealDto.getId());
		systemExceptionLog.setDealDesc(upmsExceptionDealDto.getDealDesc());
		systemExceptionLog.setDealUserInfo(upmsExceptionDealDto.getDealUserInfo());
		systemExceptionLog.setDealStatus(Byte.valueOf("1"));
		return super.responseEntity(super.update(upmsExceptionDealDto.getId(), systemExceptionLog));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空系统异常日志")
	@ApiOperation(value = "清空系统异常日志")
	@PostMapping(value = "/clear")
	public ResponseEntity<BaseResult> clear() {
		UpmsSystemExceptionLogExample systemExceptionLogExample = new UpmsSystemExceptionLogExample();
		return super.responseEntity(super.remove(systemExceptionLogExample));
	}

	/**
	 * 此方法禁止修改
	 * @param systemExceptionLogDto
	 * @return
	 */
	@ApiOperation(value = "新增系统异常日志")
	@PostMapping(value = "/insertSelective")
	public BaseResult insertSelective(@RequestBody @Valid SystemExceptionLogDto systemExceptionLogDto) {
		UpmsSystemExceptionLog systemExceptionLog = new UpmsSystemExceptionLog();
		BeanUtils.copyProperties(systemExceptionLog, systemExceptionLogDto);
		return BaseResult.ok(super.save(systemExceptionLog));
	}

}
