package cn.chiship.framework.upms.biz.base.service;

import cn.chiship.sdk.core.base.BaseResult;

import javax.servlet.http.HttpServletResponse;

public interface CaptchaService {

	/**
	 * 获取验证码图片形式
	 * @param response
	 */
	void getCaptchaCodeImage(HttpServletResponse response);

	/**
	 * 获取验证码文本形式
	 * @return
	 */
	BaseResult getCaptchaCodeText();

	/**
	 * 验证
	 * @param code
	 * @return
	 */
	BaseResult verification(String code);

}
