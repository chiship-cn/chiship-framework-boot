package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsNotice;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeExample;
import cn.chiship.sdk.framework.base.BaseMapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @author lijian
 */
public interface UpmsNoticeMapper extends BaseMapper<UpmsNotice, UpmsNoticeExample> {

	/**
	 * 查询大文本
	 * @param example
	 * @return
	 */
	List<UpmsNotice> selectByExampleWithBLOBs(UpmsNoticeExample example);

	/**
	 * 根据条件更新大文本
	 * @param record
	 * @param example
	 * @return
	 */
	int updateByExampleWithBLOBs(@Param("record") UpmsNotice record, @Param("example") UpmsNoticeExample example);

	/**
	 * 更新大文本
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeyWithBLOBs(UpmsNotice record);

}
