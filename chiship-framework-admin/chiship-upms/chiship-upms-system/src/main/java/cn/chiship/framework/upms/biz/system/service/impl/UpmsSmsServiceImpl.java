package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.common.constants.SystemConfigConstants;
import cn.chiship.framework.common.pojo.vo.ConfigJson;
import cn.chiship.framework.common.pojo.vo.SystemConfigVo;
import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCode;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsSmsLogDto;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsCodeService;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsLogService;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.RandomUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.third.ali.AliDySmsUtil;
import cn.chiship.sdk.third.core.model.BaseConfigModel;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 * 短信业务实现
 */
@Service
public class UpmsSmsServiceImpl implements UpmsSmsService {

    @Resource
    UpmsSmsCodeService upmsSmsCodeService;
    @Resource
    UpmsSmsLogService upmsSmsLogService;
    @Resource
    GlobalCacheService globalCacheService;

    @Override
    public BaseResult sendSmsCode(String mobile, CacheUserVO cacheUserVO) {
        if (!mobile.matches(RegexConstants.MOBILE)) {
            return BaseResult.error("请输入正确的手机号码!");
        }
        try {
            /*
             * 短信厂家 smsType 是否启用 isUseSms 有效时间(分钟) expiryTime 模板编号 smsTemplate 签名内容
             * signContent 用户密钥 smsSecretKey 用户编号 smsAccessKey
             */
            ConfigJson configJson = globalCacheService.getSystemConfigJson(Arrays.asList(
                    SystemConfigConstants.SMS_TYPE,
                    SystemConfigConstants.IS_USE_SMS,
                    SystemConfigConstants.EXPIRY_TIME,
                    SystemConfigConstants.SMS_CODE_TEMPLATE,
                    SystemConfigConstants.SIGN_CONTENT,
                    SystemConfigConstants.SMS_ACCESS_KEY,
                    SystemConfigConstants.SMS_SECRET_KEY)
            );
            int expiryTime = Integer.valueOf(configJson.getString(SystemConfigConstants.EXPIRY_TIME));
            BaseResult baseResult = upmsSmsCodeService.checkCodeDeviceIsSendCode(mobile);
            if (!baseResult.isSuccess()) {
                Map<String, Object> map = Maps.newHashMapWithExpectedSize(7);
                baseResult = BaseResult.ok(null);
                String code = RandomUtil.number(6);
                if (String.valueOf(BaseConstants.YES)
                        .equals(configJson.getString(SystemConfigConstants.IS_USE_SMS))) {
                    BaseConfigModel baseConfigModel = new BaseConfigModel(
                            configJson.getString(SystemConfigConstants.SMS_ACCESS_KEY),
                            configJson.getString(SystemConfigConstants.SMS_SECRET_KEY));
                    AliDySmsUtil aliDySmsUtil = AliDySmsUtil.getInstance().config(baseConfigModel);
                    Map<String, String> paramsMap = new HashMap<>(2);
                    paramsMap.put("code", code);
                    aliDySmsUtil.load(configJson.getString(SystemConfigConstants.SIGN_CONTENT),
                            configJson.getString(SystemConfigConstants.SMS_CODE_TEMPLATE));
                    baseResult = aliDySmsUtil.sendSms(mobile, paramsMap);
                    SystemConfigVo systemConfigVo = globalCacheService.getSystemConfig(SystemConfigConstants.SMS_CODE_TEMPLATE);
                    UpmsSmsLogDto upmsSmsLogDto = new UpmsSmsLogDto(
                            configJson.getString(SystemConfigConstants.SMS_CODE_TEMPLATE),
                            systemConfigVo.getRemark(),
                            mobile,
                            paramsMap,
                            baseResult.isSuccess() ? BaseConstants.YES : BaseConstants.NO,
                            cacheUserVO
                    );
                    if(!baseResult.isSuccess()){
                        upmsSmsLogDto.setErrorMessage(baseResult.getData().toString());
                    }
                    upmsSmsLogService.save(upmsSmsLogDto);

                } else {
                    map.put("code", code);
                }
                map.put("msg", "验证码已发送，有效期为" + expiryTime + "分钟，注意查收");
                map.put("expiryTime", expiryTime);

                if (baseResult.isSuccess()) {
                    UpmsSmsCode upmsSmsCode = new UpmsSmsCode();
                    upmsSmsCode.setId(RandomUtil.uuidLowerCase());
                    upmsSmsCode.setCode(code);
                    upmsSmsCode.setReferenceCode(System.currentTimeMillis());
                    upmsSmsCode.setExpiryTime(System.currentTimeMillis() + expiryTime * 60 * 1000);
                    upmsSmsCode.setIsUsed(BaseConstants.NO);
                    upmsSmsCode.setCodeDevice(mobile);
                    upmsSmsCode.setIsDeleted(BaseConstants.NO);
                    upmsSmsCode.setGmtCreated(System.currentTimeMillis());
                    upmsSmsCode.setGmtModified(System.currentTimeMillis());
                    upmsSmsCode.setType(Byte.valueOf("0"));
                    upmsSmsCodeService.insertSelective(upmsSmsCode);
                    upmsSmsCodeService.cacheSmsCode(mobile, code, upmsSmsCode.getExpiryTime(), expiryTime * 60);
                    return BaseResult.ok(map);
                } else {
                    return baseResult;
                }
            } else {
                return BaseResult.error("您已获得验证码，有效期为" + expiryTime + "分钟");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getLocalizedMessage());
        }
    }


    @Override
    public BaseResult sendSms(String template, String mobile, Map<String, String> paramsMap, CacheUserVO cacheUserVO) {
        if (!mobile.matches(RegexConstants.MOBILE)) {
            return BaseResult.error("请输入正确的手机号码!");
        }
        try {
            ConfigJson configJson = globalCacheService.getSystemConfigJson(Arrays.asList(
                    SystemConfigConstants.SMS_TYPE,
                    SystemConfigConstants.IS_USE_SMS,
                    template,
                    SystemConfigConstants.SIGN_CONTENT,
                    SystemConfigConstants.SMS_ACCESS_KEY,
                    SystemConfigConstants.SMS_SECRET_KEY)
            );
            String smsTemplate = configJson.getString(template);
            if (StringUtil.isNullOrEmpty(smsTemplate)) {
                return BaseResult.error("模板编号[" + template + "]不存在");
            }
            if (String.valueOf(BaseConstants.YES).equals(configJson.getString(SystemConfigConstants.IS_USE_SMS))) {
                BaseConfigModel baseConfigModel = new BaseConfigModel(
                        configJson.getString(SystemConfigConstants.SMS_ACCESS_KEY),
                        configJson.getString(SystemConfigConstants.SMS_SECRET_KEY)
                );
                AliDySmsUtil aliDySmsUtil = AliDySmsUtil.getInstance().config(baseConfigModel);
                aliDySmsUtil.load(
                        configJson.getString(SystemConfigConstants.SIGN_CONTENT),
                        smsTemplate
                );
                BaseResult baseResult = aliDySmsUtil.sendSms(mobile, paramsMap);
                SystemConfigVo systemConfigVo = globalCacheService.getSystemConfig(template);
                UpmsSmsLogDto upmsSmsLogDto = new UpmsSmsLogDto(
                        smsTemplate,
                        systemConfigVo.getRemark(),
                        mobile,
                        paramsMap,
                        baseResult.isSuccess() ? BaseConstants.YES : BaseConstants.NO,
                        cacheUserVO
                );
                if(!baseResult.isSuccess()){
                    upmsSmsLogDto.setErrorMessage(baseResult.getData().toString());
                }
                upmsSmsLogService.save(upmsSmsLogDto);
                return baseResult;

            } else {
                return BaseResult.ok("短信模拟发送成功!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessException(e.getLocalizedMessage());
        }
    }
}
