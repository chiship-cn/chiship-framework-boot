package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class UpmsSmsCodeExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsSmsCodeExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceIsNull() {
			addCriterion("code_device is null");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceIsNotNull() {
			addCriterion("code_device is not null");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceEqualTo(String value) {
			addCriterion("code_device =", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceNotEqualTo(String value) {
			addCriterion("code_device <>", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceGreaterThan(String value) {
			addCriterion("code_device >", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceGreaterThanOrEqualTo(String value) {
			addCriterion("code_device >=", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceLessThan(String value) {
			addCriterion("code_device <", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceLessThanOrEqualTo(String value) {
			addCriterion("code_device <=", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceLike(String value) {
			addCriterion("code_device like", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceNotLike(String value) {
			addCriterion("code_device not like", value, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceIn(List<String> values) {
			addCriterion("code_device in", values, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceNotIn(List<String> values) {
			addCriterion("code_device not in", values, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceBetween(String value1, String value2) {
			addCriterion("code_device between", value1, value2, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeDeviceNotBetween(String value1, String value2) {
			addCriterion("code_device not between", value1, value2, "codeDevice");
			return (Criteria) this;
		}

		public Criteria andCodeIsNull() {
			addCriterion("code is null");
			return (Criteria) this;
		}

		public Criteria andCodeIsNotNull() {
			addCriterion("code is not null");
			return (Criteria) this;
		}

		public Criteria andCodeEqualTo(String value) {
			addCriterion("code =", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotEqualTo(String value) {
			addCriterion("code <>", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeGreaterThan(String value) {
			addCriterion("code >", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeGreaterThanOrEqualTo(String value) {
			addCriterion("code >=", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLessThan(String value) {
			addCriterion("code <", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLessThanOrEqualTo(String value) {
			addCriterion("code <=", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeLike(String value) {
			addCriterion("code like", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotLike(String value) {
			addCriterion("code not like", value, "code");
			return (Criteria) this;
		}

		public Criteria andCodeIn(List<String> values) {
			addCriterion("code in", values, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotIn(List<String> values) {
			addCriterion("code not in", values, "code");
			return (Criteria) this;
		}

		public Criteria andCodeBetween(String value1, String value2) {
			addCriterion("code between", value1, value2, "code");
			return (Criteria) this;
		}

		public Criteria andCodeNotBetween(String value1, String value2) {
			addCriterion("code not between", value1, value2, "code");
			return (Criteria) this;
		}

		public Criteria andIsUsedIsNull() {
			addCriterion("is_used is null");
			return (Criteria) this;
		}

		public Criteria andIsUsedIsNotNull() {
			addCriterion("is_used is not null");
			return (Criteria) this;
		}

		public Criteria andIsUsedEqualTo(Byte value) {
			addCriterion("is_used =", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotEqualTo(Byte value) {
			addCriterion("is_used <>", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedGreaterThan(Byte value) {
			addCriterion("is_used >", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_used >=", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedLessThan(Byte value) {
			addCriterion("is_used <", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedLessThanOrEqualTo(Byte value) {
			addCriterion("is_used <=", value, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedIn(List<Byte> values) {
			addCriterion("is_used in", values, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotIn(List<Byte> values) {
			addCriterion("is_used not in", values, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedBetween(Byte value1, Byte value2) {
			addCriterion("is_used between", value1, value2, "isUsed");
			return (Criteria) this;
		}

		public Criteria andIsUsedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_used not between", value1, value2, "isUsed");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeIsNull() {
			addCriterion("expiry_time is null");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeIsNotNull() {
			addCriterion("expiry_time is not null");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeEqualTo(Long value) {
			addCriterion("expiry_time =", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeNotEqualTo(Long value) {
			addCriterion("expiry_time <>", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeGreaterThan(Long value) {
			addCriterion("expiry_time >", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("expiry_time >=", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeLessThan(Long value) {
			addCriterion("expiry_time <", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeLessThanOrEqualTo(Long value) {
			addCriterion("expiry_time <=", value, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeIn(List<Long> values) {
			addCriterion("expiry_time in", values, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeNotIn(List<Long> values) {
			addCriterion("expiry_time not in", values, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeBetween(Long value1, Long value2) {
			addCriterion("expiry_time between", value1, value2, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andExpiryTimeNotBetween(Long value1, Long value2) {
			addCriterion("expiry_time not between", value1, value2, "expiryTime");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeIsNull() {
			addCriterion("reference_code is null");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeIsNotNull() {
			addCriterion("reference_code is not null");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeEqualTo(Long value) {
			addCriterion("reference_code =", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeNotEqualTo(Long value) {
			addCriterion("reference_code <>", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeGreaterThan(Long value) {
			addCriterion("reference_code >", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeGreaterThanOrEqualTo(Long value) {
			addCriterion("reference_code >=", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeLessThan(Long value) {
			addCriterion("reference_code <", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeLessThanOrEqualTo(Long value) {
			addCriterion("reference_code <=", value, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeIn(List<Long> values) {
			addCriterion("reference_code in", values, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeNotIn(List<Long> values) {
			addCriterion("reference_code not in", values, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeBetween(Long value1, Long value2) {
			addCriterion("reference_code between", value1, value2, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andReferenceCodeNotBetween(Long value1, Long value2) {
			addCriterion("reference_code not between", value1, value2, "referenceCode");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}