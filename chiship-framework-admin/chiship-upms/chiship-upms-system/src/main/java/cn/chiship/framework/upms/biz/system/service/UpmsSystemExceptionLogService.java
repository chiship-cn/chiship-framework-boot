package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLogExample;

/**
 * 系统异常日志业务接口层 2021/10/23
 *
 * @author lijian
 */
public interface UpmsSystemExceptionLogService
		extends BaseService<UpmsSystemExceptionLog, UpmsSystemExceptionLogExample> {

	/**
	 * 主键获取详情
	 * @param id
	 * @return
	 */
	BaseResult getById(String id);

}
