package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLogExample;

import java.util.List;

import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lj
 * @date 2023-06-15
 */
public interface UpmsSystemOptionLogMapper extends BaseMapper<UpmsSystemOptionLog, UpmsSystemOptionLogExample> {

	/**
	 * 查询大文本
	 * @param example
	 * @return
	 */
	List<UpmsSystemOptionLog> selectByExampleWithBLOBs(UpmsSystemOptionLogExample example);

	/**
	 * 根据条件更新大文本
	 * @param record
	 * @param example
	 * @return
	 */
	int updateByExampleWithBLOBs(@Param("record") UpmsSystemOptionLog record,
			@Param("example") UpmsSystemOptionLogExample example);

	/**
	 * 更新大文本
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeyWithBLOBs(UpmsSystemOptionLog record);

}
