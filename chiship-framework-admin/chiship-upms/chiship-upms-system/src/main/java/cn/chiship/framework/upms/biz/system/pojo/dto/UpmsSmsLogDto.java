package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.Map;

/**
 * 短信发送日志表单
 * 2025/2/27
 *
 * @author LiJian
 */
@ApiModel(value = "短信发送日志表单")
public class UpmsSmsLogDto {

    @ApiModelProperty(value = "模板", required = true)
    @NotEmpty(message = "模板" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 20)
    private String template;

    @ApiModelProperty(value = "模板内容", required = true)
    @NotEmpty(message = "模板" + BaseTipConstants.NOT_EMPTY)
    private String templateContent;

    @ApiModelProperty(value = "接收者", required = true)
    @NotEmpty(message = "接收者" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 10)
    private String recipient;

    @ApiModelProperty(value = "短信参数")
    private Map<String, String> paramsMap;

    @ApiModelProperty(value = "状态", required = true)
    @NotNull(message = "状态" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte status;

    @ApiModelProperty(value = "操作员")
    private CacheUserVO cacheUserVO;

    @ApiModelProperty(value = "失败错误代码")
    private String errorCode;

    @ApiModelProperty(value = "消息ID 运营商提供")
    private String messageId;

    @ApiModelProperty(value = "错误信息")
    private String errorMessage;

    public UpmsSmsLogDto(String template, String templateContent, String recipient, Map<String, String> paramsMap, Byte status, CacheUserVO cacheUserVO) {
        this.template = template;
        this.templateContent = templateContent;
        this.recipient = recipient;
        this.paramsMap = paramsMap;
        this.status = status;
        this.cacheUserVO = cacheUserVO;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateContent(String templateContent) {
        this.templateContent = templateContent;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public Map<String, String> getParamsMap() {
        return paramsMap;
    }

    public void setParamsMap(Map<String, String> paramsMap) {
        this.paramsMap = paramsMap;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public CacheUserVO getCacheUserVO() {
        return cacheUserVO;
    }

    public void setCacheUserVO(CacheUserVO cacheUserVO) {
        this.cacheUserVO = cacheUserVO;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}