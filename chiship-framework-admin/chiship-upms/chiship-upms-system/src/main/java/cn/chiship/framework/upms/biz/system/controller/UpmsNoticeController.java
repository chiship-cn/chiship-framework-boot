package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeExample;
import cn.chiship.framework.upms.biz.system.service.UpmsNoticeService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.system.entity.UpmsNotice;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsNoticeDto;
import cn.chiship.sdk.framework.util.OptionUserUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 通知公告表控制层 2021/10/22
 *
 * @author lijian
 */
@RestController
@RequestMapping("/notice")
@Api(tags = "通知公告")
public class UpmsNoticeController extends BaseController<UpmsNotice, UpmsNoticeExample> {

	@Resource
	private UpmsNoticeService upmsNoticeService;

	@Override
	public BaseService getService() {
		return upmsNoticeService;
	}

	@SystemOptionAnnotation(describe = "通知公告分页查询")
	@ApiOperation(value = "通知公告分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeTitle", value = "标题", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeType", value = "1 通知公告 2 系统消息 ", defaultValue = "1",
					dataTypeClass = Byte.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeUserType", value = "admin 后台 member 会员 ", dataTypeClass = String.class,
					paramType = "query") })
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(@RequestParam(value = "noticeType") Byte noticeType,
			@RequestParam(required = false, defaultValue = "", value = "noticeUserType") String noticeUserType,
			@RequestParam(required = false, defaultValue = "", value = "noticeTitle") String noticeTitle) {
		UpmsNoticeExample upmsNoticeExample = new UpmsNoticeExample();
		// 创造条件
		UpmsNoticeExample.Criteria criteria = upmsNoticeExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO).andNoticeTypeEqualTo(noticeType);
		if (!StringUtil.isNullOrEmpty(noticeTitle)) {
			criteria.andNoticeTitleLike(noticeTitle + "%");
		}
		if (!StringUtil.isNullOrEmpty(noticeUserType)) {
			criteria.andNoticeUserTypeEqualTo(noticeUserType);
		}
		PageVo pageVo = page(upmsNoticeExample);
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@SystemOptionAnnotation(describe = "通知公告分页查询（面向用户）")
	@ApiOperation(value = "通知公告分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeTitle", value = "标题", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeType", value = "1 通知公告 2 系统消息 ", required = true,
					dataTypeClass = Byte.class, paramType = "query"),
			@ApiImplicitParam(name = "noticeUserType", value = "admin 后台 member 会员 ", required = true,
					dataTypeClass = String.class, paramType = "query") })
	@GetMapping(value = "/pageV1")
	public ResponseEntity<BaseResult> pageV1(@RequestParam(value = "noticeType") Byte noticeType,
			@RequestParam(value = "noticeUserType") String noticeUserType,
			@RequestParam(required = false, defaultValue = "", value = "noticeTitle") String noticeTitle) {
		UpmsNoticeExample upmsNoticeExample = new UpmsNoticeExample();
		// 创造条件
		UpmsNoticeExample.Criteria criteria = upmsNoticeExample.createCriteria();
		UpmsNoticeExample.Criteria criteria2 = upmsNoticeExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO).andNoticeTypeEqualTo(noticeType)
				.andSendStatusEqualTo(Byte.valueOf("1")).andNoticeUserTypeEqualTo(noticeUserType)
				.andStartTimeLessThanOrEqualTo(System.currentTimeMillis())
				.andEndTimeGreaterThanOrEqualTo(System.currentTimeMillis());
		criteria2.andIsDeletedEqualTo(BaseConstants.NO).andNoticeTypeEqualTo(noticeType)
				.andSendStatusEqualTo(Byte.valueOf("1")).andNoticeUserTypeEqualTo(noticeUserType)
				.andStartTimeLessThanOrEqualTo(System.currentTimeMillis()).andEndTimeIsNull();

		if (!StringUtil.isNullOrEmpty(noticeTitle)) {
			criteria.andNoticeTitleLike(noticeTitle + "%");
			criteria2.andNoticeTitleLike(noticeTitle + "%");
		}
		upmsNoticeExample.or(criteria2);
		PageVo pageVo = page(upmsNoticeExample);
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@SystemOptionAnnotation(describe = "保存通知公告", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
	@ApiOperation(value = "保存通知公告")
	@PostMapping(value = "save")
	@Authorization
	public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsNoticeDto upmsNoticeDto) {
		UpmsNotice upmsNotice = new UpmsNotice();
		BeanUtils.copyProperties(upmsNoticeDto, upmsNotice);
		upmsNotice.setSender(OptionUserUtil.getCurrentOptionUser(getLoginUser()));
		upmsNotice.setCreatedUserId(getUserId());
		upmsNotice.setCreatedOrgId(getLoginUserOrgId());
		return super.responseEntity(super.save(upmsNotice));

	}

	@SystemOptionAnnotation(describe = "更新通知公告", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
	@ApiOperation(value = "更新通知公告")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path"), })
	@PostMapping(value = "update/{id}")
	@Authorization
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid UpmsNoticeDto upmsNoticeDto) {
		UpmsNotice upmsNotice = new UpmsNotice();
		BeanUtils.copyProperties(upmsNoticeDto, upmsNotice);
		return super.responseEntity(super.update(id, upmsNotice));
	}

	@SystemOptionAnnotation(describe = "删除通知公告", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
	@ApiOperation(value = "删除通知公告")
	@PostMapping(value = "remove")
	@Authorization
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		UpmsNoticeExample upmsNoticeExample = new UpmsNoticeExample();
		upmsNoticeExample.createCriteria().andIdIn(ids);
		return super.responseEntity(BaseResult.ok(super.remove(upmsNoticeExample)));
	}

	@SystemOptionAnnotation(describe = "通知公告撤销", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
	@ApiOperation(value = "通知公告撤销")
	@ApiImplicitParams({})
	@PostMapping(value = "revoke/{id}")
	@Authorization
	public ResponseEntity<BaseResult> revoke(@PathVariable("id") String id) {
		return super.responseEntity(upmsNoticeService.revoke(id));
	}

}
