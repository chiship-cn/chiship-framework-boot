package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLogExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLog;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @author lijian
 */
public interface UpmsSystemExceptionLogMapper
		extends BaseMapper<UpmsSystemExceptionLog, UpmsSystemExceptionLogExample> {

	/**
	 * 查询大文本
	 * @param example
	 * @return
	 */
	List<UpmsSystemExceptionLog> selectByExampleWithBLOBs(UpmsSystemExceptionLogExample example);

	/**
	 * 根据条件更新大文本
	 * @param record
	 * @param example
	 * @return
	 */
	int updateByExampleWithBLOBs(@Param("record") UpmsSystemExceptionLog record,
			@Param("example") UpmsSystemExceptionLogExample example);

	/**
	 * 更新大文本
	 * @param record
	 * @return
	 */
	int updateByPrimaryKeyWithBLOBs(UpmsSystemExceptionLog record);

}
