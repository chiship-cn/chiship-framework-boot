package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersion;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersionExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsAppVersionMapper;
import cn.chiship.framework.upms.biz.system.service.UpmsAppVersionService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import javax.annotation.Resource;

import cn.chiship.sdk.framework.pojo.vo.PageVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * App版本业务接口实现层 2021/9/30
 *
 * @author lijian
 */
@Service
public class UpmsAppVersionServiceImpl extends BaseServiceImpl<UpmsAppVersion, UpmsAppVersionExample>
		implements UpmsAppVersionService {

	@Resource
	UpmsAppVersionMapper upmsAppVersionMapper;

	@Override
	public BaseResult insertSelective(UpmsAppVersion upmsAppVersion) {
		UpmsAppVersionExample upmsAppVersionExample = new UpmsAppVersionExample();
		upmsAppVersionExample.createCriteria().andAppCodeEqualTo(upmsAppVersion.getAppCode())
				.andAppVersionEqualTo(upmsAppVersion.getAppVersion()).andAppTypeEqualTo(upmsAppVersion.getAppType());
		List<UpmsAppVersion> upmsAppVersions = upmsAppVersionMapper.selectByExample(upmsAppVersionExample);
		if (upmsAppVersions.isEmpty()) {
			upmsAppVersion.setId(SnowflakeIdUtil.generateStrId());
			return super.insertSelective(upmsAppVersion);
		}
		else {
			return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "同一种类型同一版本同一识别码的APP已存在，请重新输入");
		}

	}

	@Override
	public BaseResult updateByPrimaryKeySelective(UpmsAppVersion upmsAppVersion) {
		UpmsAppVersionExample upmsAppVersionExample = new UpmsAppVersionExample();
		upmsAppVersionExample.createCriteria().andAppCodeEqualTo(upmsAppVersion.getAppCode())
				.andAppVersionEqualTo(upmsAppVersion.getAppVersion()).andAppTypeEqualTo(upmsAppVersion.getAppType())
				.andIdNotEqualTo(upmsAppVersion.getId());

		List<UpmsAppVersion> upmsAppVersions = upmsAppVersionMapper.selectByExample(upmsAppVersionExample);
		if (upmsAppVersions.isEmpty()) {
			return super.updateByPrimaryKeySelective(upmsAppVersion);
		}
		else {
			return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "同一种类型同一版本同一识别码的APP已存在，请重新输入");
		}
	}

	@Override
	public List<UpmsAppVersion> listNewsApp() {
		List<UpmsAppVersion> upmsAppVersions = new ArrayList<>();
		List<String> appCodes = listAppCode();
		PageVo pageVo = new PageVo();
		pageVo.setCurrent(1L);
		pageVo.setSize(1L);
		UpmsAppVersionExample upmsAppVersionExample;
		for (String appCode : appCodes) {
			upmsAppVersionExample = new UpmsAppVersionExample();
			upmsAppVersionExample.createCriteria().andAppCodeEqualTo(appCode);
			pageVo.setSort("-appVersion");
			pageVo = super.selectPageByExample(pageVo, upmsAppVersionExample);
			upmsAppVersions.addAll(pageVo.getRecords());
		}

		return upmsAppVersions;

	}

	@Override
	public List<String> listAppCode() {
		return upmsAppVersionMapper.listAppCode();
	}

}
