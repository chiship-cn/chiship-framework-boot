package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsConfig;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfigExample;
import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * 系统配置Mapper
 *
 * @author lj
 */
public interface UpmsConfigMapper extends BaseMapper<UpmsConfig, UpmsConfigExample> {

}