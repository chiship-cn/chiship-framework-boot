package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersion;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersionExample;

import java.util.List;

/**
 * App版本业务接口层 2021/9/30
 *
 * @author lijian
 */
public interface UpmsAppVersionService extends BaseService<UpmsAppVersion, UpmsAppVersionExample> {

	/**
	 * 获得所有AppCode
	 * @return
	 */
	List<String> listAppCode();

	/**
	 * 获得最新APP
	 * @return
	 */
	List<UpmsAppVersion> listNewsApp();

}
