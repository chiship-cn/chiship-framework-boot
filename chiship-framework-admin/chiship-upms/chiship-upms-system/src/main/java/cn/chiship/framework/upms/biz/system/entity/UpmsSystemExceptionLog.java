package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UpmsSystemExceptionLog implements Serializable {

	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 用户ID
	 *
	 * @mbg.generated
	 */
	private String userId;

	private String userName;

	private String realName;

	/**
	 * 请求ID
	 *
	 * @mbg.generated
	 */
	private String requestId;

	/**
	 * 操作IP地址
	 *
	 * @mbg.generated
	 */
	private String ip;

	/**
	 * 请求方法
	 *
	 * @mbg.generated
	 */
	private String method;

	/**
	 * 根路径
	 *
	 * @mbg.generated
	 */
	private String basePath;

	/**
	 * 统一资源标识符
	 *
	 * @mbg.generated
	 */
	private String uri;

	/**
	 * 异常类
	 *
	 * @mbg.generated
	 */
	private String errorClass;

	/**
	 * 描述
	 *
	 * @mbg.generated
	 */
	private String description;

	private String errorLocalizedMessage;

	/**
	 * 请求类型
	 *
	 * @mbg.generated
	 */
	private String requestType;

	/**
	 * 浏览器用户代理
	 *
	 * @mbg.generated
	 */
	private String userAgent;

	private String systemName;

	/**
	 * 处理状态 0 未处理 1已处理
	 *
	 * @mbg.generated
	 */
	private Byte dealStatus;

	/**
	 * 处理人员信息
	 *
	 * @mbg.generated
	 */
	private String dealUserInfo;

	/**
	 * 处理简述
	 *
	 * @mbg.generated
	 */
	private String dealDesc;

	/**
	 * 请求参数
	 *
	 * @mbg.generated
	 */
	private String parameter;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getErrorLocalizedMessage() {
		return errorLocalizedMessage;
	}

	public void setErrorLocalizedMessage(String errorLocalizedMessage) {
		this.errorLocalizedMessage = errorLocalizedMessage;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public Byte getDealStatus() {
		return dealStatus;
	}

	public void setDealStatus(Byte dealStatus) {
		this.dealStatus = dealStatus;
	}

	public String getDealUserInfo() {
		return dealUserInfo;
	}

	public void setDealUserInfo(String dealUserInfo) {
		this.dealUserInfo = dealUserInfo;
	}

	public String getDealDesc() {
		return dealDesc;
	}

	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", userId=").append(userId);
		sb.append(", userName=").append(userName);
		sb.append(", realName=").append(realName);
		sb.append(", requestId=").append(requestId);
		sb.append(", ip=").append(ip);
		sb.append(", method=").append(method);
		sb.append(", basePath=").append(basePath);
		sb.append(", uri=").append(uri);
		sb.append(", errorClass=").append(errorClass);
		sb.append(", description=").append(description);
		sb.append(", errorLocalizedMessage=").append(errorLocalizedMessage);
		sb.append(", requestType=").append(requestType);
		sb.append(", userAgent=").append(userAgent);
		sb.append(", systemName=").append(systemName);
		sb.append(", dealStatus=").append(dealStatus);
		sb.append(", dealUserInfo=").append(dealUserInfo);
		sb.append(", dealDesc=").append(dealDesc);
		sb.append(", parameter=").append(parameter);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsSystemExceptionLog other = (UpmsSystemExceptionLog) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserName() == null ? other.getUserName() == null
						: this.getUserName().equals(other.getUserName()))
				&& (this.getRealName() == null ? other.getRealName() == null
						: this.getRealName().equals(other.getRealName()))
				&& (this.getRequestId() == null ? other.getRequestId() == null
						: this.getRequestId().equals(other.getRequestId()))
				&& (this.getIp() == null ? other.getIp() == null : this.getIp().equals(other.getIp()))
				&& (this.getMethod() == null ? other.getMethod() == null : this.getMethod().equals(other.getMethod()))
				&& (this.getBasePath() == null ? other.getBasePath() == null
						: this.getBasePath().equals(other.getBasePath()))
				&& (this.getUri() == null ? other.getUri() == null : this.getUri().equals(other.getUri()))
				&& (this.getErrorClass() == null ? other.getErrorClass() == null
						: this.getErrorClass().equals(other.getErrorClass()))
				&& (this.getDescription() == null ? other.getDescription() == null
						: this.getDescription().equals(other.getDescription()))
				&& (this.getErrorLocalizedMessage() == null ? other.getErrorLocalizedMessage() == null
						: this.getErrorLocalizedMessage().equals(other.getErrorLocalizedMessage()))
				&& (this.getRequestType() == null ? other.getRequestType() == null
						: this.getRequestType().equals(other.getRequestType()))
				&& (this.getUserAgent() == null ? other.getUserAgent() == null
						: this.getUserAgent().equals(other.getUserAgent()))
				&& (this.getSystemName() == null ? other.getSystemName() == null
						: this.getSystemName().equals(other.getSystemName()))
				&& (this.getDealStatus() == null ? other.getDealStatus() == null
						: this.getDealStatus().equals(other.getDealStatus()))
				&& (this.getDealUserInfo() == null ? other.getDealUserInfo() == null
						: this.getDealUserInfo().equals(other.getDealUserInfo()))
				&& (this.getDealDesc() == null ? other.getDealDesc() == null
						: this.getDealDesc().equals(other.getDealDesc()))
				&& (this.getParameter() == null ? other.getParameter() == null
						: this.getParameter().equals(other.getParameter()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
		result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
		result = prime * result + ((getRequestId() == null) ? 0 : getRequestId().hashCode());
		result = prime * result + ((getIp() == null) ? 0 : getIp().hashCode());
		result = prime * result + ((getMethod() == null) ? 0 : getMethod().hashCode());
		result = prime * result + ((getBasePath() == null) ? 0 : getBasePath().hashCode());
		result = prime * result + ((getUri() == null) ? 0 : getUri().hashCode());
		result = prime * result + ((getErrorClass() == null) ? 0 : getErrorClass().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getErrorLocalizedMessage() == null) ? 0 : getErrorLocalizedMessage().hashCode());
		result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
		result = prime * result + ((getUserAgent() == null) ? 0 : getUserAgent().hashCode());
		result = prime * result + ((getSystemName() == null) ? 0 : getSystemName().hashCode());
		result = prime * result + ((getDealStatus() == null) ? 0 : getDealStatus().hashCode());
		result = prime * result + ((getDealUserInfo() == null) ? 0 : getDealUserInfo().hashCode());
		result = prime * result + ((getDealDesc() == null) ? 0 : getDealDesc().hashCode());
		result = prime * result + ((getParameter() == null) ? 0 : getParameter().hashCode());
		return result;
	}

}