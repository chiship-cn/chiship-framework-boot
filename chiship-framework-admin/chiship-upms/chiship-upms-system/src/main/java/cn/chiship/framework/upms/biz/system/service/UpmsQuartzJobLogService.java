package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLogExample;
/**
 * 定时任务日志业务接口层
 * 2025/2/10
 * @author lijian
 */
public interface UpmsQuartzJobLogService extends BaseService<UpmsQuartzJobLog,UpmsQuartzJobLogExample> {

}
