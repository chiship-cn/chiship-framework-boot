package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.mapper.UpmsQuartzJobLogMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLogExample;
import cn.chiship.framework.upms.biz.system.service.UpmsQuartzJobLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 定时任务日志业务接口实现层
 * 2025/2/10
 * @author lijian
 */
@Service
public class UpmsQuartzJobLogServiceImpl extends BaseServiceImpl<UpmsQuartzJobLog,UpmsQuartzJobLogExample> implements UpmsQuartzJobLogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsQuartzJobLogServiceImpl.class);

    @Resource
    UpmsQuartzJobLogMapper upmsQuartzJobLogMapper;

}
