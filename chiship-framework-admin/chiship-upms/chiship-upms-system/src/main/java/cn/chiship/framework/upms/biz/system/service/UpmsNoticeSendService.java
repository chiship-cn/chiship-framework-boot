package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSend;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSendExample;

import java.util.HashMap;

/**
 * 用户通告阅读标记表业务接口层 2022/2/4
 *
 * @author lijian
 */
public interface UpmsNoticeSendService extends BaseService<UpmsNoticeSend, UpmsNoticeSendExample> {

	/**
	 * 详情
	 * @param map
	 * @return
	 */
	BaseResult pageProfitDetails(HashMap<String, Object> map);

	/**
	 * 设置当前登录用户消息全部已读
	 * @param noticeType
	 * @param userId
	 * @return
	 */
	BaseResult currentUserSetRead(Byte noticeType, String userId);

	/**
	 * 设为已读状态
	 * @param id
	 * @param userId
	 * @return
	 */
	BaseResult setAsRead(String id, String userId);

}
