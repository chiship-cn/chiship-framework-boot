package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.common.annotation.JobAnnotation;
import cn.chiship.framework.upms.biz.system.mapper.UpmsQuartzJobMapper;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJob;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobExample;
import cn.chiship.framework.upms.biz.system.service.UpmsQuartzJobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.aop.support.AopUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 定时任务业务接口实现层 2021/12/2
 *
 * @author lijian
 */
@Service
public class UpmsQuartzJobServiceImpl extends BaseServiceImpl<UpmsQuartzJob, UpmsQuartzJobExample>
        implements UpmsQuartzJobService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsQuartzJobServiceImpl.class);

    @Resource
    UpmsQuartzJobMapper upmsQuartzJobMapper;

    @Resource
    ApplicationContext applicationContext;

    @Override
    public BaseResult insertSelective(UpmsQuartzJob upmsQuartzJob) {
        UpmsQuartzJobExample quartzJobExample = new UpmsQuartzJobExample();
        quartzJobExample.createCriteria().andJobClassEqualTo(upmsQuartzJob.getJobClass())
                .andJobMethodEqualTo(upmsQuartzJob.getJobMethod());
        List<UpmsQuartzJob> upmsQuartzJobs = upmsQuartzJobMapper.selectByExample(quartzJobExample);
        if (upmsQuartzJobs.isEmpty()) {
            return super.insertSelective(upmsQuartzJob);
        } else {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "定时任务已存在，请重新输入");
        }
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(UpmsQuartzJob upmsQuartzJob) {
        UpmsQuartzJobExample quartzJobExample = new UpmsQuartzJobExample();
        quartzJobExample.createCriteria().andJobClassEqualTo(upmsQuartzJob.getJobClass())
                .andJobMethodEqualTo(upmsQuartzJob.getJobMethod()).andIdNotEqualTo(upmsQuartzJob.getId());
        List<UpmsQuartzJob> upmsQuartzJobs = upmsQuartzJobMapper.selectByExample(quartzJobExample);
        if (upmsQuartzJobs.isEmpty()) {
            return super.updateByPrimaryKeySelective(upmsQuartzJob);
        } else {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, "定时任务已存在，请重新输入");
        }
    }

    @Override
    public BaseResult listTasks() {
        List<Map<String, Object>> maps = new ArrayList<>();
        Map<String, Object> beansWithAnnotation = applicationContext.getBeansWithAnnotation(JobAnnotation.class);
        for (Object bean : beansWithAnnotation.values()) {
            Map<String, Object> classMap = new HashMap<>(7);
            Class<?> beanClass = AopUtils.getTargetClass(bean);
            JobAnnotation classAnnotation = beanClass.getAnnotation(JobAnnotation.class);
            if (classAnnotation != null) {
                classMap.put("name", StringUtil.toLowerCaseFirstOne(beanClass.getSimpleName()));
                classMap.put("desc", classAnnotation.value());
                classMap.put("children", listMethods(beanClass));
                maps.add(classMap);
            }
        }
        return BaseResult.ok(maps);
    }

    public List<Map<String, Object>> listMethods(Class<?> beanClass) {
        List<Map<String, Object>> maps = new ArrayList<>();
        Method[] methods = beanClass.getDeclaredMethods();
        for (Method method : methods) {
            Map<String, Object> methodMap = new HashMap<>(7);
            JobAnnotation methodAnnotation = method.getAnnotation(JobAnnotation.class);
            if (methodAnnotation != null) {
                methodMap.put("name", method.getName());
                methodMap.put("desc", methodAnnotation.value());
                maps.add(methodMap);
            }
        }
        return maps;
    }
}
