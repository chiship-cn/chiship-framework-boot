package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-11
 */
public class UpmsFeedbackExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsFeedbackExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNull() {
			addCriterion("created_user_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNotNull() {
			addCriterion("created_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdEqualTo(String value) {
			addCriterion("created_user_id =", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotEqualTo(String value) {
			addCriterion("created_user_id <>", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThan(String value) {
			addCriterion("created_user_id >", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_id >=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThan(String value) {
			addCriterion("created_user_id <", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThanOrEqualTo(String value) {
			addCriterion("created_user_id <=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLike(String value) {
			addCriterion("created_user_id like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotLike(String value) {
			addCriterion("created_user_id not like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIn(List<String> values) {
			addCriterion("created_user_id in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotIn(List<String> values) {
			addCriterion("created_user_id not in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdBetween(String value1, String value2) {
			addCriterion("created_user_id between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotBetween(String value1, String value2) {
			addCriterion("created_user_id not between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andUserTypeIsNull() {
			addCriterion("user_type is null");
			return (Criteria) this;
		}

		public Criteria andUserTypeIsNotNull() {
			addCriterion("user_type is not null");
			return (Criteria) this;
		}

		public Criteria andUserTypeEqualTo(String value) {
			addCriterion("user_type =", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeNotEqualTo(String value) {
			addCriterion("user_type <>", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeGreaterThan(String value) {
			addCriterion("user_type >", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeGreaterThanOrEqualTo(String value) {
			addCriterion("user_type >=", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeLessThan(String value) {
			addCriterion("user_type <", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeLessThanOrEqualTo(String value) {
			addCriterion("user_type <=", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeLike(String value) {
			addCriterion("user_type like", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeNotLike(String value) {
			addCriterion("user_type not like", value, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeIn(List<String> values) {
			addCriterion("user_type in", values, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeNotIn(List<String> values) {
			addCriterion("user_type not in", values, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeBetween(String value1, String value2) {
			addCriterion("user_type between", value1, value2, "userType");
			return (Criteria) this;
		}

		public Criteria andUserTypeNotBetween(String value1, String value2) {
			addCriterion("user_type not between", value1, value2, "userType");
			return (Criteria) this;
		}

		public Criteria andTitleIsNull() {
			addCriterion("title is null");
			return (Criteria) this;
		}

		public Criteria andTitleIsNotNull() {
			addCriterion("title is not null");
			return (Criteria) this;
		}

		public Criteria andTitleEqualTo(String value) {
			addCriterion("title =", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotEqualTo(String value) {
			addCriterion("title <>", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThan(String value) {
			addCriterion("title >", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThanOrEqualTo(String value) {
			addCriterion("title >=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThan(String value) {
			addCriterion("title <", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThanOrEqualTo(String value) {
			addCriterion("title <=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLike(String value) {
			addCriterion("title like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotLike(String value) {
			addCriterion("title not like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleIn(List<String> values) {
			addCriterion("title in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotIn(List<String> values) {
			addCriterion("title not in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleBetween(String value1, String value2) {
			addCriterion("title between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotBetween(String value1, String value2) {
			addCriterion("title not between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andCategoryIsNull() {
			addCriterion("category is null");
			return (Criteria) this;
		}

		public Criteria andCategoryIsNotNull() {
			addCriterion("category is not null");
			return (Criteria) this;
		}

		public Criteria andCategoryEqualTo(String value) {
			addCriterion("category =", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryNotEqualTo(String value) {
			addCriterion("category <>", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryGreaterThan(String value) {
			addCriterion("category >", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryGreaterThanOrEqualTo(String value) {
			addCriterion("category >=", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryLessThan(String value) {
			addCriterion("category <", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryLessThanOrEqualTo(String value) {
			addCriterion("category <=", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryLike(String value) {
			addCriterion("category like", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryNotLike(String value) {
			addCriterion("category not like", value, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryIn(List<String> values) {
			addCriterion("category in", values, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryNotIn(List<String> values) {
			addCriterion("category not in", values, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryBetween(String value1, String value2) {
			addCriterion("category between", value1, value2, "category");
			return (Criteria) this;
		}

		public Criteria andCategoryNotBetween(String value1, String value2) {
			addCriterion("category not between", value1, value2, "category");
			return (Criteria) this;
		}

		public Criteria andContentIsNull() {
			addCriterion("content is null");
			return (Criteria) this;
		}

		public Criteria andContentIsNotNull() {
			addCriterion("content is not null");
			return (Criteria) this;
		}

		public Criteria andContentEqualTo(String value) {
			addCriterion("content =", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentNotEqualTo(String value) {
			addCriterion("content <>", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentGreaterThan(String value) {
			addCriterion("content >", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentGreaterThanOrEqualTo(String value) {
			addCriterion("content >=", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentLessThan(String value) {
			addCriterion("content <", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentLessThanOrEqualTo(String value) {
			addCriterion("content <=", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentLike(String value) {
			addCriterion("content like", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentNotLike(String value) {
			addCriterion("content not like", value, "content");
			return (Criteria) this;
		}

		public Criteria andContentIn(List<String> values) {
			addCriterion("content in", values, "content");
			return (Criteria) this;
		}

		public Criteria andContentNotIn(List<String> values) {
			addCriterion("content not in", values, "content");
			return (Criteria) this;
		}

		public Criteria andContentBetween(String value1, String value2) {
			addCriterion("content between", value1, value2, "content");
			return (Criteria) this;
		}

		public Criteria andContentNotBetween(String value1, String value2) {
			addCriterion("content not between", value1, value2, "content");
			return (Criteria) this;
		}

		public Criteria andImagesIsNull() {
			addCriterion("images is null");
			return (Criteria) this;
		}

		public Criteria andImagesIsNotNull() {
			addCriterion("images is not null");
			return (Criteria) this;
		}

		public Criteria andImagesEqualTo(String value) {
			addCriterion("images =", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesNotEqualTo(String value) {
			addCriterion("images <>", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesGreaterThan(String value) {
			addCriterion("images >", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesGreaterThanOrEqualTo(String value) {
			addCriterion("images >=", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesLessThan(String value) {
			addCriterion("images <", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesLessThanOrEqualTo(String value) {
			addCriterion("images <=", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesLike(String value) {
			addCriterion("images like", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesNotLike(String value) {
			addCriterion("images not like", value, "images");
			return (Criteria) this;
		}

		public Criteria andImagesIn(List<String> values) {
			addCriterion("images in", values, "images");
			return (Criteria) this;
		}

		public Criteria andImagesNotIn(List<String> values) {
			addCriterion("images not in", values, "images");
			return (Criteria) this;
		}

		public Criteria andImagesBetween(String value1, String value2) {
			addCriterion("images between", value1, value2, "images");
			return (Criteria) this;
		}

		public Criteria andImagesNotBetween(String value1, String value2) {
			addCriterion("images not between", value1, value2, "images");
			return (Criteria) this;
		}

		public Criteria andVideosIsNull() {
			addCriterion("videos is null");
			return (Criteria) this;
		}

		public Criteria andVideosIsNotNull() {
			addCriterion("videos is not null");
			return (Criteria) this;
		}

		public Criteria andVideosEqualTo(String value) {
			addCriterion("videos =", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosNotEqualTo(String value) {
			addCriterion("videos <>", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosGreaterThan(String value) {
			addCriterion("videos >", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosGreaterThanOrEqualTo(String value) {
			addCriterion("videos >=", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosLessThan(String value) {
			addCriterion("videos <", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosLessThanOrEqualTo(String value) {
			addCriterion("videos <=", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosLike(String value) {
			addCriterion("videos like", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosNotLike(String value) {
			addCriterion("videos not like", value, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosIn(List<String> values) {
			addCriterion("videos in", values, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosNotIn(List<String> values) {
			addCriterion("videos not in", values, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosBetween(String value1, String value2) {
			addCriterion("videos between", value1, value2, "videos");
			return (Criteria) this;
		}

		public Criteria andVideosNotBetween(String value1, String value2) {
			addCriterion("videos not between", value1, value2, "videos");
			return (Criteria) this;
		}

		public Criteria andIsReplyIsNull() {
			addCriterion("is_reply is null");
			return (Criteria) this;
		}

		public Criteria andIsReplyIsNotNull() {
			addCriterion("is_reply is not null");
			return (Criteria) this;
		}

		public Criteria andIsReplyEqualTo(Byte value) {
			addCriterion("is_reply =", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyNotEqualTo(Byte value) {
			addCriterion("is_reply <>", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyGreaterThan(Byte value) {
			addCriterion("is_reply >", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_reply >=", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyLessThan(Byte value) {
			addCriterion("is_reply <", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyLessThanOrEqualTo(Byte value) {
			addCriterion("is_reply <=", value, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyIn(List<Byte> values) {
			addCriterion("is_reply in", values, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyNotIn(List<Byte> values) {
			addCriterion("is_reply not in", values, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyBetween(Byte value1, Byte value2) {
			addCriterion("is_reply between", value1, value2, "isReply");
			return (Criteria) this;
		}

		public Criteria andIsReplyNotBetween(Byte value1, Byte value2) {
			addCriterion("is_reply not between", value1, value2, "isReply");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleIsNull() {
			addCriterion("reply_people is null");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleIsNotNull() {
			addCriterion("reply_people is not null");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleEqualTo(String value) {
			addCriterion("reply_people =", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleNotEqualTo(String value) {
			addCriterion("reply_people <>", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleGreaterThan(String value) {
			addCriterion("reply_people >", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleGreaterThanOrEqualTo(String value) {
			addCriterion("reply_people >=", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleLessThan(String value) {
			addCriterion("reply_people <", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleLessThanOrEqualTo(String value) {
			addCriterion("reply_people <=", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleLike(String value) {
			addCriterion("reply_people like", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleNotLike(String value) {
			addCriterion("reply_people not like", value, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleIn(List<String> values) {
			addCriterion("reply_people in", values, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleNotIn(List<String> values) {
			addCriterion("reply_people not in", values, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleBetween(String value1, String value2) {
			addCriterion("reply_people between", value1, value2, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyPeopleNotBetween(String value1, String value2) {
			addCriterion("reply_people not between", value1, value2, "replyPeople");
			return (Criteria) this;
		}

		public Criteria andReplyContentIsNull() {
			addCriterion("reply_content is null");
			return (Criteria) this;
		}

		public Criteria andReplyContentIsNotNull() {
			addCriterion("reply_content is not null");
			return (Criteria) this;
		}

		public Criteria andReplyContentEqualTo(String value) {
			addCriterion("reply_content =", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentNotEqualTo(String value) {
			addCriterion("reply_content <>", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentGreaterThan(String value) {
			addCriterion("reply_content >", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentGreaterThanOrEqualTo(String value) {
			addCriterion("reply_content >=", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentLessThan(String value) {
			addCriterion("reply_content <", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentLessThanOrEqualTo(String value) {
			addCriterion("reply_content <=", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentLike(String value) {
			addCriterion("reply_content like", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentNotLike(String value) {
			addCriterion("reply_content not like", value, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentIn(List<String> values) {
			addCriterion("reply_content in", values, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentNotIn(List<String> values) {
			addCriterion("reply_content not in", values, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentBetween(String value1, String value2) {
			addCriterion("reply_content between", value1, value2, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyContentNotBetween(String value1, String value2) {
			addCriterion("reply_content not between", value1, value2, "replyContent");
			return (Criteria) this;
		}

		public Criteria andReplyTimeIsNull() {
			addCriterion("reply_time is null");
			return (Criteria) this;
		}

		public Criteria andReplyTimeIsNotNull() {
			addCriterion("reply_time is not null");
			return (Criteria) this;
		}

		public Criteria andReplyTimeEqualTo(Long value) {
			addCriterion("reply_time =", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeNotEqualTo(Long value) {
			addCriterion("reply_time <>", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeGreaterThan(Long value) {
			addCriterion("reply_time >", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("reply_time >=", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeLessThan(Long value) {
			addCriterion("reply_time <", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeLessThanOrEqualTo(Long value) {
			addCriterion("reply_time <=", value, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeIn(List<Long> values) {
			addCriterion("reply_time in", values, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeNotIn(List<Long> values) {
			addCriterion("reply_time not in", values, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeBetween(Long value1, Long value2) {
			addCriterion("reply_time between", value1, value2, "replyTime");
			return (Criteria) this;
		}

		public Criteria andReplyTimeNotBetween(Long value1, Long value2) {
			addCriterion("reply_time not between", value1, value2, "replyTime");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}