package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCode;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCodeExample;

/**
 * 验证码业务接口层 2021/9/30
 *
 * @author lijian
 */
public interface UpmsSmsCodeService extends BaseService<UpmsSmsCode, UpmsSmsCodeExample> {

    /**
     * 获得验证码
     *
     * @param codeDevice 设备号码
     * @param deviceType 设备类型 0：手机 1：邮箱
     * @return
     */
    BaseResult get(String codeDevice, Byte deviceType);

    /**
     * 校验验证码(设为已使用)
     *
     * @param codeDevice 设备号码
     * @param code       验证码
     * @return
     */
    BaseResult verification(String codeDevice, String code);

    /**
     * 核查验证码
     *
     * @param codeDevice 设备号码
     * @param code       验证码
     * @return
     */
    BaseResult check(String codeDevice, String code);

    /**
     * 设置已使用
     *
     * @param codeDevice
     * @param code
     * @return
     */
    BaseResult setIsUsed(String codeDevice, String code);

    /**
     * 验证设备是否发送
     *
     * @param codeDevice
     * @return
     */
    BaseResult checkCodeDeviceIsSendCode(String codeDevice);

    /**
     * 缓存验证码
     *
     * @param codeDevice
     * @param code
     * @param expireTime
     * @param duration
     */
    void cacheSmsCode(String codeDevice, String code, Long expireTime, Integer duration);

}
