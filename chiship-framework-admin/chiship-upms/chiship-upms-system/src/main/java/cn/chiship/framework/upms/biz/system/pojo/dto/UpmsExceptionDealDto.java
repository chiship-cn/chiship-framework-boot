package cn.chiship.framework.upms.biz.system.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * @author LiJian
 */
@ApiModel(value = "系统异常处理表单")
public class UpmsExceptionDealDto {

	@ApiModelProperty(value = "主键")
	@NotNull
	@Length(min = 15, max = 36)
	private String id;

	@ApiModelProperty(value = "处理人员信息")
	@NotNull
	@Length(min = 1, max = 100)
	private String dealUserInfo;

	@ApiModelProperty(value = "处理简述")
	@NotNull
	@Length(min = 1, max = 1000)
	private String dealDesc;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDealUserInfo() {
		return dealUserInfo;
	}

	public void setDealUserInfo(String dealUserInfo) {
		this.dealUserInfo = dealUserInfo;
	}

	public String getDealDesc() {
		return dealDesc;
	}

	public void setDealDesc(String dealDesc) {
		this.dealDesc = dealDesc;
	}

}