package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.common.pojo.dto.SystemOptionLogDto;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLogExample;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemOptionLogService;
import cn.chiship.sdk.core.annotation.Authorization;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.framework.base.BaseService;
import io.netty.util.internal.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 系统操作日志控制层 2021/9/30
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/systemOptionLog")
@Api(tags = "系统操作日志")
public class UpmsSystemOptionLogController extends BaseController<UpmsSystemOptionLog, UpmsSystemOptionLogExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSystemOptionLogController.class);

	@Resource
	private UpmsSystemOptionLogService upmsSystemOptionLogService;

	@Override
	public BaseService getService() {
		return upmsSystemOptionLogService;
	}

	@ApiOperation(value = "系统操作日志分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "+id", dataTypeClass = String.class,
					paramType = "query"),
			@ApiImplicitParam(name = "description", value = "操作描述", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "systemName", value = "系统名称", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "optionType", value = "操作类型", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "ip", value = "操作IP地址", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "requestType", value = "请求方式", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "userName", value = "用户名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"), })
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "description") String description,
			@RequestParam(required = false, value = "systemName") String systemName,
			@RequestParam(required = false, value = "optionType") String optionType,
			@RequestParam(required = false, value = "ip") String ip,
			@RequestParam(required = false, value = "requestType") String requestType,
			@RequestParam(required = false, value = "userName") String userName,
			@RequestParam(required = false, value = "realName") String realName,
			@RequestParam(required = false, value = "startTime") Long startTime,
			@RequestParam(required = false, value = "endTime") Long endTime) {

		UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
		// 创造条件
		UpmsSystemOptionLogExample.Criteria criteria = upmsSystemOptionLogExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(description)) {
			criteria.andDescriptionLike(description + "%");
		}
		if (!StringUtil.isNullOrEmpty(ip)) {
			criteria.andIpLike(ip + "%");
		}
		if (!StringUtil.isNullOrEmpty(requestType)) {
			criteria.andRequestTypeEqualTo(requestType);
		}
		if (!StringUtil.isNullOrEmpty(userName)) {
			criteria.andUserNameLike(userName + "%");
		}
		if (!StringUtil.isNullOrEmpty(realName)) {
			criteria.andRealNameLike(realName + "%");
		}
		if (!StringUtil.isNullOrEmpty(systemName)) {
			criteria.andSystemNameLike(systemName + "%");
		}
		if (startTime != null) {
			criteria.andStartTimeGreaterThanOrEqualTo(startTime);
		}
		if (endTime != null) {
			criteria.andStartTimeLessThanOrEqualTo(endTime);
		}
		if (!StringUtil.isNullOrEmpty(optionType)) {
			criteria.andOptionTypeEqualTo(optionType);
		}
		return super.responseEntity(BaseResult.ok(super.page(upmsSystemOptionLogExample)));
	}

	@ApiOperation(value = "当前用户操作历史分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "+id", dataTypeClass = String.class,
					paramType = "query"),
			@ApiImplicitParam(name = "description", value = "操作描述", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "systemName", value = "系统名称", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "optionType", value = "操作类型", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "ip", value = "操作IP地址", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "requestType", value = "请求方式", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"), })
	@GetMapping(value = "/current/page")
	public ResponseEntity<BaseResult> currentUserLogPage(
			@RequestParam(required = false, value = "description") String description,
			@RequestParam(required = false, value = "systemName") String systemName,
			@RequestParam(required = false, value = "optionType") String optionType,
			@RequestParam(required = false, value = "ip") String ip,
			@RequestParam(required = false, value = "requestType") String requestType,
			@RequestParam(required = false, value = "startTime") Long startTime,
			@RequestParam(required = false, value = "endTime") Long endTime) {
		UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
		// 创造条件
		UpmsSystemOptionLogExample.Criteria criteria = upmsSystemOptionLogExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO).andUserNameEqualTo(getUserName());
		if (!StringUtil.isNullOrEmpty(description)) {
			criteria.andDescriptionLike(description + "%");
		}
		if (!StringUtil.isNullOrEmpty(ip)) {
			criteria.andIpLike(ip + "%");
		}
		if (!StringUtil.isNullOrEmpty(requestType)) {
			criteria.andRequestTypeEqualTo(requestType);
		}
		if (!StringUtil.isNullOrEmpty(systemName)) {
			criteria.andSystemNameLike(systemName + "%");
		}
		if (startTime != null) {
			criteria.andStartTimeGreaterThanOrEqualTo(startTime);
		}
		if (endTime != null) {
			criteria.andStartTimeLessThanOrEqualTo(endTime);
		}
		if (!StringUtil.isNullOrEmpty(optionType)) {
			criteria.andOptionTypeEqualTo(optionType);
		}
		return super.responseEntity(BaseResult.ok(super.page(upmsSystemOptionLogExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除系统日志")
	@ApiOperation(value = "删除系统日志")
	@ApiImplicitParams({})
	@PostMapping(value = "/remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
		upmsSystemOptionLogExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(upmsSystemOptionLogExample));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空系统日志")
	@ApiOperation(value = "清空系统日志")
	@ApiImplicitParams({})
	@PostMapping(value = "/clear")
	public ResponseEntity<BaseResult> clear() {
		UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
		return super.responseEntity(super.remove(upmsSystemOptionLogExample));
	}

	/**
	 * 此方法禁止修改
	 * @param systemOptionLogDto
	 * @return
	 */
	@ApiOperation(value = "新增日志")
	@PostMapping(value = "/insertSelective")
	public BaseResult insertSelective(@RequestBody @Valid SystemOptionLogDto systemOptionLogDto) {
		UpmsSystemOptionLog upmsSystemOptionLog = new UpmsSystemOptionLog();
		BeanUtils.copyProperties(systemOptionLogDto, upmsSystemOptionLog);
		return BaseResult.ok(super.save(upmsSystemOptionLog));
	}

}
