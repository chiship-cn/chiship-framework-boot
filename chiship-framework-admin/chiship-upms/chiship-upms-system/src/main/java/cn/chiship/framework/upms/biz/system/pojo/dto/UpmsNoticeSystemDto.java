package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.framework.upms.core.enums.NoticeModuleTypeEnums;
import cn.chiship.framework.upms.core.enums.NoticePriorityEnums;
import cn.chiship.sdk.core.enums.UserTypeEnum;
import cn.chiship.sdk.core.util.StringUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Map;

/**
 * @author lijian
 */
@ApiModel(value = "系统消息表单")
public class UpmsNoticeSystemDto {

	@ApiModelProperty(value = "消息标题")
	private String title;

	@ApiModelProperty(value = "消息描述")
	private String desc;

	@ApiModelProperty(value = "优先级")
	NoticePriorityEnums noticePriorityEnums;

	@ApiModelProperty(value = "模块类型")
	NoticeModuleTypeEnums noticeModuleTypeEnums;

	@ApiModelProperty(value = "面向用户类型")
	UserTypeEnum userTypeEnum;

	@ApiModelProperty(value = "消息正文")
	private Map<String, Object> content;

	@ApiModelProperty(value = "用户主键")
	String userId;

	@ApiModelProperty(value = "真实姓名")
	String realName;

	public UpmsNoticeSystemDto(String title, String desc, NoticePriorityEnums noticePriorityEnums,
			NoticeModuleTypeEnums noticeModuleTypeEnums, UserTypeEnum userTypeEnum, String userId, String realName) {
		this.title = title;
		this.desc = desc;
		this.noticePriorityEnums = noticePriorityEnums;
		this.noticeModuleTypeEnums = noticeModuleTypeEnums;
		this.userTypeEnum = userTypeEnum;
		this.userId = userId;
		this.realName = realName;
	}

	public UpmsNoticeSystemDto(String title, String desc, NoticePriorityEnums noticePriorityEnums,
			NoticeModuleTypeEnums noticeModuleTypeEnums, UserTypeEnum userTypeEnum, Map<String, Object> content,
			String userId, String realName) {
		this.title = title;
		this.desc = desc;
		this.noticePriorityEnums = noticePriorityEnums;
		this.noticeModuleTypeEnums = noticeModuleTypeEnums;
		this.userTypeEnum = userTypeEnum;
		this.content = content;
		this.userId = userId;
		this.realName = realName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public NoticePriorityEnums getNoticePriorityEnums() {
		return noticePriorityEnums;
	}

	public void setNoticePriorityEnums(NoticePriorityEnums noticePriorityEnums) {
		this.noticePriorityEnums = noticePriorityEnums;
	}

	public NoticeModuleTypeEnums getNoticeModuleTypeEnums() {
		return noticeModuleTypeEnums;
	}

	public void setNoticeModuleTypeEnums(NoticeModuleTypeEnums noticeModuleTypeEnums) {
		this.noticeModuleTypeEnums = noticeModuleTypeEnums;
	}

	public UserTypeEnum getUserTypeEnum() {
		return userTypeEnum;
	}

	public void setUserTypeEnum(UserTypeEnum userTypeEnum) {
		this.userTypeEnum = userTypeEnum;
	}

	public Map<String, Object> getContent() {
		return content;
	}

	public void setContent(Map<String, Object> content) {
		this.content = content;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRealName() {
		if (StringUtil.isNullOrEmpty(realName)) {
			return "-";
		}
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

}
