package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 意见反馈表单 2022/5/16
 *
 * @author LiJian
 */
@ApiModel(value = "意见反馈表单")
public class UpmsFeedbackDto {

	@ApiModelProperty(value = "标题", required = true)
	@NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String title;

	@ApiModelProperty(value = "分类", required = true)
	@NotEmpty(message = "分类" + BaseTipConstants.NOT_EMPTY)
	private String category;

	@ApiModelProperty(value = "内容", required = true)
	@NotEmpty(message = "内容" + BaseTipConstants.NOT_EMPTY)
	private String content;

	@ApiModelProperty(value = "图片")
	private String images;

	@ApiModelProperty(value = "视频")
	private String videos;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getVideos() {
		return videos;
	}

	public void setVideos(String videos) {
		this.videos = videos;
	}

}
