package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsSmsLogDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.ObjectUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.mapper.UpmsSmsLogMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLogExample;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 短信发送日志业务接口实现层
 * 2025/2/27
 *
 * @author lijian
 */
@Service
public class UpmsSmsLogServiceImpl extends BaseServiceImpl<UpmsSmsLog, UpmsSmsLogExample> implements UpmsSmsLogService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSmsLogServiceImpl.class);

    @Resource
    UpmsSmsLogMapper upmsSmsLogMapper;

    @Override
    @Async("asyncServiceExecutor")
    public BaseResult save(UpmsSmsLogDto upmsSmsLogDto) {
        String content = upmsSmsLogDto.getTemplateContent();
        if (StringUtil.isNullOrEmpty(content)) {
            content = "数据库中还未配置短信模板";
        }
        Map<String, String> paramMap = upmsSmsLogDto.getParamsMap();
        if (ObjectUtil.isEmpty(paramMap)) {
            paramMap = new HashMap<>();
        }
        for (Map.Entry<String, String> entry : paramMap.entrySet()) {
            content = content.replace("${" + entry.getKey() + "}", entry.getValue());
        }
        UpmsSmsLog upmsSmsLog = new UpmsSmsLog();
        upmsSmsLog.setTemplate(upmsSmsLogDto.getTemplate());
        upmsSmsLog.setRecipient(upmsSmsLogDto.getRecipient());
        upmsSmsLog.setContent(content);
        upmsSmsLog.setStatus(upmsSmsLogDto.getStatus());
        CacheUserVO cacheUserVO = upmsSmsLogDto.getCacheUserVO();
        if (ObjectUtil.isNotEmpty(cacheUserVO)) {
            upmsSmsLog.setOperatorId(cacheUserVO.getId());
            upmsSmsLog.setOperatorName(cacheUserVO.getRealName());
        }
        upmsSmsLog.setErrorCode(upmsSmsLogDto.getErrorCode());
        upmsSmsLog.setMessageId(upmsSmsLogDto.getMessageId());
        upmsSmsLog.setErrorMessage(upmsSmsLogDto.getErrorMessage());
        return super.insertSelective(upmsSmsLog);
    }

}
