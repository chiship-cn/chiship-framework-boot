package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.core.base.BaseResult;

import java.util.List;
import java.util.Map;

/**
 * 数据库操作
 * @author lijian
 */
public interface DbService {

    /**
     * 查询所有表
     *
     * @return
     */
    BaseResult listTables();

    /**
     * 根据表名获取列
     * @param tableName
     * @return
     */
    BaseResult listColumns(String tableName);
}
