package cn.chiship.framework.upms.core.task;

import cn.chiship.framework.common.annotation.JobAnnotation;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLogExample;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemOptionLogService;
import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author lijian
 */
@Component
@JobAnnotation("日志任务")
public class LogTask {
    @Resource
    UpmsSystemOptionLogService upmsSystemOptionLogService;

    @JobAnnotation("清理操作日志,默认7天")
    public void clearSystemOptionLog(String param) {
        long day = 7L;
        if (!StringUtil.isNullOrEmpty(param)) {
            try {
                day = Long.parseLong(param);
            } catch (Exception e) {
            }
        }
        UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
        upmsSystemOptionLogExample.createCriteria().andGmtCreatedLessThanOrEqualTo(System.currentTimeMillis() - day * 24 * 60 * 60 * 1000);
        upmsSystemOptionLogService.deleteByExample(upmsSystemOptionLogExample);

    }


}
