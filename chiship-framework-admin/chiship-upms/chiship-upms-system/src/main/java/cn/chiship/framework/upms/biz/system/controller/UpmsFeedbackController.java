package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedback;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedbackExample;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsFeedbackDto;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsFeedbackReplyDto;
import cn.chiship.framework.upms.biz.system.service.UpmsFeedbackService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.sdk.framework.pojo.vo.PageVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 意见反馈控制层 2022/5/16
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/feedback")
@Api(tags = "意见反馈")
public class UpmsFeedbackController extends BaseController<UpmsFeedback, UpmsFeedbackExample> {

	@Resource
	private UpmsFeedbackService upmsFeedbackService;

	@Override
	public BaseService getService() {
		return upmsFeedbackService;
	}

	@SystemOptionAnnotation(describe = "意见反馈分页")
	@ApiOperation(value = "意见反馈分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		UpmsFeedbackExample upmsFeedbackExample = new UpmsFeedbackExample();
		// 创造条件
		UpmsFeedbackExample.Criteria upmsFeedbackCriteria = upmsFeedbackExample.createCriteria();
		upmsFeedbackCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
			upmsFeedbackCriteria.andTitleLike(keyword + "%");
		}
		PageVo pageVo = super.page(upmsFeedbackExample);
		pageVo.setRecords(upmsFeedbackService.assembleData(pageVo.getRecords()));
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@SystemOptionAnnotation(describe = "当前用户意见反馈分页")
	@ApiOperation(value = "当前用户意见反馈分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/current/page")
	public ResponseEntity<BaseResult> currentUserPage(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		UpmsFeedbackExample upmsFeedbackExample = new UpmsFeedbackExample();
		// 创造条件
		UpmsFeedbackExample.Criteria upmsFeedbackCriteria = upmsFeedbackExample.createCriteria();
		upmsFeedbackCriteria.andIsDeletedEqualTo(BaseConstants.NO).andCreatedUserIdEqualTo(getUserId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			upmsFeedbackCriteria.andTitleLike(keyword + "%");
		}
		PageVo pageVo = super.page(upmsFeedbackExample);
		pageVo.setRecords(upmsFeedbackService.assembleData(pageVo.getRecords()));
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@SystemOptionAnnotation(describe = "意见反馈列表数据")
	@ApiOperation(value = "意见反馈列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		UpmsFeedbackExample upmsFeedbackExample = new UpmsFeedbackExample();
		// 创造条件
		UpmsFeedbackExample.Criteria upmsFeedbackCriteria = upmsFeedbackExample.createCriteria();
		upmsFeedbackCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
			upmsFeedbackCriteria.andTitleLike(keyword + "%");
		}
		return super.responseEntity(BaseResult.ok(super.list(upmsFeedbackExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存意见反馈")
	@ApiOperation(value = "保存意见反馈")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsFeedbackDto upmsFeedbackDto) {
		UpmsFeedback upmsFeedback = new UpmsFeedback();
		BeanUtils.copyProperties(upmsFeedbackDto, upmsFeedback);
		upmsFeedback.setCreatedBy(getCurrentOptionUser());
		upmsFeedback.setCreatedUserId(getUserId());
		upmsFeedback.setUserType(getLoginUser().getUserTypeEnum().getType());
		return super.responseEntity(super.save(upmsFeedback));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新意见反馈")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path"), })
	@ApiOperation(value = "更新意见反馈")
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid UpmsFeedbackDto upmsFeedbackDto) {
		UpmsFeedback upmsFeedback = new UpmsFeedback();
		BeanUtils.copyProperties(upmsFeedbackDto, upmsFeedback);
		return super.responseEntity(super.update(id, upmsFeedback));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除意见反馈")
	@ApiOperation(value = "删除意见反馈")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		UpmsFeedbackExample upmsFeedbackExample = new UpmsFeedbackExample();
		upmsFeedbackExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(upmsFeedbackExample));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "回复意见反馈")
	@ApiOperation(value = "回复意见反馈")
	@PostMapping(value = "reply")
	public ResponseEntity<BaseResult> reply(@RequestBody @Valid UpmsFeedbackReplyDto upmsFeedbackReplyDto) {
		return super.responseEntity(upmsFeedbackService.reply(upmsFeedbackReplyDto, getCurrentOptionUser()));
	}

}