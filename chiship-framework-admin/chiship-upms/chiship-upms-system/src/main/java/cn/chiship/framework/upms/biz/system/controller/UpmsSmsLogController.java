package cn.chiship.framework.upms.biz.system.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.system.service.UpmsSmsLogService;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLogExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 短信发送日志控制层
 * 2025/2/27
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/upmsSmsLog")
@Api(tags = "短信发送日志")
public class UpmsSmsLogController extends BaseController<UpmsSmsLog, UpmsSmsLogExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSmsLogController.class);

    @Resource
    private UpmsSmsLogService upmsSmsLogService;

    @Override
    public BaseService getService() {
        return upmsSmsLogService;
    }

    @SystemOptionAnnotation(describe = "短信发送日志分页")
    @ApiOperation(value = "短信发送日志分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query")
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "", value = "status") Byte status,
                                           @RequestParam(required = false, value = "startTime") Long startTime,
                                           @RequestParam(required = false, value = "endTime") Long endTime) {
        UpmsSmsLogExample upmsSmsLogExample = new UpmsSmsLogExample();
        //创造条件
        UpmsSmsLogExample.Criteria upmsSmsLogCriteria = upmsSmsLogExample.createCriteria();
        upmsSmsLogCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            upmsSmsLogCriteria.andContentLike(keyword + "%");
        }
        if (status != null) {
            upmsSmsLogCriteria.andStatusEqualTo(status);
        }
        if (startTime != null) {
            upmsSmsLogCriteria.andGmtCreatedGreaterThanOrEqualTo(startTime);
        }
        if (endTime != null) {
            upmsSmsLogCriteria.andGmtCreatedLessThanOrEqualTo(endTime);
        }
        return super.responseEntity(BaseResult.ok(super.page(upmsSmsLogExample)));
    }

    @SystemOptionAnnotation(describe = "短信发送日志列表数据")
    @ApiOperation(value = "短信发送日志列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        UpmsSmsLogExample upmsSmsLogExample = new UpmsSmsLogExample();
        //创造条件
        UpmsSmsLogExample.Criteria upmsSmsLogCriteria = upmsSmsLogExample.createCriteria();
        upmsSmsLogCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        upmsSmsLogExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(upmsSmsLogExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除短信发送日志")
    @ApiOperation(value = "删除短信发送日志")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsSmsLogExample upmsSmsLogExample = new UpmsSmsLogExample();
        upmsSmsLogExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsSmsLogExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空短信发送日志")
    @ApiOperation(value = "清空短信发送日志")
    @ApiImplicitParams({})
    @PostMapping(value = "/clear")
    public ResponseEntity<BaseResult> clear() {
        UpmsSmsLogExample upmsSmsLogExample = new UpmsSmsLogExample();
        return super.responseEntity(super.remove(upmsSmsLogExample));
    }
}