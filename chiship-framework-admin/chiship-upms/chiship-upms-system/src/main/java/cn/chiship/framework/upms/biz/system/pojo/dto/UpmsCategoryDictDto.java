package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 分类字典表单 2024/10/13
 *
 * @author LiJian
 */
@ApiModel(value = "分类字典表单")
public class UpmsCategoryDictDto {

	@ApiModelProperty(value = "标题", required = true)
	@NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String name;

	@ApiModelProperty(value = "所属上级", required = true)
	@NotEmpty(message = "所属上级" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String pid;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	@Min(1)
	@Max(99999)
	private Long orders;

	@ApiModelProperty(value = "所属类型", required = true)
	@NotEmpty(message = "所属类型" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 10)
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}