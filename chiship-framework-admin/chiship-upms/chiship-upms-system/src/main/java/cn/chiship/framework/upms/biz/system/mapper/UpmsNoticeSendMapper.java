package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSend;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSendExample;

import java.util.HashMap;
import java.util.List;

import cn.chiship.framework.upms.biz.system.pojo.vo.UpmsNoticeSendVo;
import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface UpmsNoticeSendMapper extends BaseMapper<UpmsNoticeSend, UpmsNoticeSendExample> {

	/**
	 * 详情
	 * @param map
	 * @return
	 */
	List<UpmsNoticeSendVo> selectProfitDetails(HashMap<String, Object> map);

}