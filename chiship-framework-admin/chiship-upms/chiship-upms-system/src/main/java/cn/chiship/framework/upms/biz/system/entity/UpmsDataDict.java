package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lj
 * @date 2023-06-15
 */
public class UpmsDataDict implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 所属父级
	 */
	private String pid;

	/**
	 * 字典编码
	 */
	private String dataDictCode;

	/**
	 * 字典名称
	 */
	private String dataDictName;

	/**
	 * 颜色块
	 */
	private String colorValue;

	/**
	 * 排序
	 */
	private Long sortValue;

	/**
	 * 系统默认（0：否，1：是）
	 */
	private Byte isBuiltIn;

	/**
	 * 是否国家标准 0 否 1 是
	 */
	private Byte isNationalStandard;

	/**
	 * 备注
	 */
	private String remark;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getDataDictCode() {
		return dataDictCode;
	}

	public void setDataDictCode(String dataDictCode) {
		this.dataDictCode = dataDictCode;
	}

	public String getDataDictName() {
		return dataDictName;
	}

	public void setDataDictName(String dataDictName) {
		this.dataDictName = dataDictName;
	}

	public String getColorValue() {
		return colorValue;
	}

	public void setColorValue(String colorValue) {
		this.colorValue = colorValue;
	}

	public Long getSortValue() {
		return sortValue;
	}

	public void setSortValue(Long sortValue) {
		this.sortValue = sortValue;
	}

	public Byte getIsBuiltIn() {
		return isBuiltIn;
	}

	public void setIsBuiltIn(Byte isBuiltIn) {
		this.isBuiltIn = isBuiltIn;
	}

	public Byte getIsNationalStandard() {
		return isNationalStandard;
	}

	public void setIsNationalStandard(Byte isNationalStandard) {
		this.isNationalStandard = isNationalStandard;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", pid=").append(pid);
		sb.append(", dataDictCode=").append(dataDictCode);
		sb.append(", dataDictName=").append(dataDictName);
		sb.append(", colorValue=").append(colorValue);
		sb.append(", sortValue=").append(sortValue);
		sb.append(", isBuiltIn=").append(isBuiltIn);
		sb.append(", isNationalStandard=").append(isNationalStandard);
		sb.append(", remark=").append(remark);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsDataDict other = (UpmsDataDict) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
				&& (this.getDataDictCode() == null ? other.getDataDictCode() == null
						: this.getDataDictCode().equals(other.getDataDictCode()))
				&& (this.getDataDictName() == null ? other.getDataDictName() == null
						: this.getDataDictName().equals(other.getDataDictName()))
				&& (this.getColorValue() == null ? other.getColorValue() == null
						: this.getColorValue().equals(other.getColorValue()))
				&& (this.getSortValue() == null ? other.getSortValue() == null
						: this.getSortValue().equals(other.getSortValue()))
				&& (this.getIsBuiltIn() == null ? other.getIsBuiltIn() == null
						: this.getIsBuiltIn().equals(other.getIsBuiltIn()))
				&& (this.getIsNationalStandard() == null ? other.getIsNationalStandard() == null
						: this.getIsNationalStandard().equals(other.getIsNationalStandard()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
		result = prime * result + ((getDataDictCode() == null) ? 0 : getDataDictCode().hashCode());
		result = prime * result + ((getDataDictName() == null) ? 0 : getDataDictName().hashCode());
		result = prime * result + ((getColorValue() == null) ? 0 : getColorValue().hashCode());
		result = prime * result + ((getSortValue() == null) ? 0 : getSortValue().hashCode());
		result = prime * result + ((getIsBuiltIn() == null) ? 0 : getIsBuiltIn().hashCode());
		result = prime * result + ((getIsNationalStandard() == null) ? 0 : getIsNationalStandard().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		return result;
	}

}