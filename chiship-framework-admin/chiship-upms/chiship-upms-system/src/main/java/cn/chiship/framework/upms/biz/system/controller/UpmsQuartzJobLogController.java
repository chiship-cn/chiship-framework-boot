package cn.chiship.framework.upms.biz.system.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.system.service.UpmsQuartzJobLogService;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLogExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.validation.Valid;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 定时任务日志控制层
 * 2025/2/10
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/upmsQuartzJobLog")
@Api(tags = "定时任务日志")
public class UpmsQuartzJobLogController extends BaseController<UpmsQuartzJobLog, UpmsQuartzJobLogExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsQuartzJobLogController.class);

    @Resource
    private UpmsQuartzJobLogService upmsQuartzJobLogService;

    @Override
    public BaseService getService() {
        return upmsQuartzJobLogService;
    }

    @ApiOperation(value = "定时任务日志分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "jobName", value = "任务名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "startTime", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "endTime", value = "结束时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "jobClass", value = "任务类名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "jobMethod", value = "任务方法", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "jobName") String jobName,
                                           @RequestParam(required = false, defaultValue = "", value = "jobClass") String jobClass,
                                           @RequestParam(required = false, defaultValue = "", value = "jobMethod") String jobMethod,
                                           @RequestParam(required = false, value = "startTime") Long startTime,
                                           @RequestParam(required = false, value = "endTime") Long endTime) {
        UpmsQuartzJobLogExample upmsQuartzJobLogExample = new UpmsQuartzJobLogExample();
        //创造条件
        UpmsQuartzJobLogExample.Criteria upmsQuartzJobLogCriteria = upmsQuartzJobLogExample.createCriteria();
        upmsQuartzJobLogCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(jobName)) {
            upmsQuartzJobLogCriteria.andJobNameLike(jobName + "%");
        }
        if (!StringUtil.isNullOrEmpty(jobClass)) {
            upmsQuartzJobLogCriteria.andJobClassEqualTo(jobClass.substring(0, 1).toUpperCase() + jobClass.substring(1));
        }
        if (!StringUtil.isNullOrEmpty(jobMethod)) {
            upmsQuartzJobLogCriteria.andJobMethodEqualTo(jobMethod);
        }
        if (startTime != null) {
            upmsQuartzJobLogCriteria.andStartTimeGreaterThanOrEqualTo(startTime);
        }
        if (endTime != null) {
            upmsQuartzJobLogCriteria.andStartTimeLessThanOrEqualTo(endTime);
        }
        return super.responseEntity(BaseResult.ok(super.page(upmsQuartzJobLogExample)));
    }

    @SystemOptionAnnotation(describe = "定时任务日志列表数据")
    @ApiOperation(value = "定时任务日志列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        UpmsQuartzJobLogExample upmsQuartzJobLogExample = new UpmsQuartzJobLogExample();
        //创造条件
        UpmsQuartzJobLogExample.Criteria upmsQuartzJobLogCriteria = upmsQuartzJobLogExample.createCriteria();
        upmsQuartzJobLogCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        upmsQuartzJobLogExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(upmsQuartzJobLogExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除定时任务日志")
    @ApiOperation(value = "删除定时任务日志")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsQuartzJobLogExample upmsQuartzJobLogExample = new UpmsQuartzJobLogExample();
        upmsQuartzJobLogExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsQuartzJobLogExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "清空定时任务日志")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "jobClass", value = "任务类名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "jobMethod", value = "任务方法", dataTypeClass = String.class, paramType = "query"),
    })
    @ApiOperation(value = "清空定时任务日志")
    @GetMapping(value = "/clear")
    public ResponseEntity<BaseResult> clear(@RequestParam(required = false, defaultValue = "", value = "jobClass") String jobClass,
                                            @RequestParam(required = false, defaultValue = "", value = "jobMethod") String jobMethod) {
        UpmsQuartzJobLogExample upmsQuartzJobLogExample = new UpmsQuartzJobLogExample();
        UpmsQuartzJobLogExample.Criteria upmsQuartzJobLogCriteria = upmsQuartzJobLogExample.createCriteria();
        if (!StringUtil.isNullOrEmpty(jobClass)) {
            upmsQuartzJobLogCriteria.andJobClassEqualTo(jobClass.substring(0, 1).toUpperCase() + jobClass.substring(1));
        }
        if (!StringUtil.isNullOrEmpty(jobMethod)) {
            upmsQuartzJobLogCriteria.andJobMethodEqualTo(jobMethod);
        }
        return super.responseEntity(super.remove(upmsQuartzJobLogExample));
    }
}