package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLog;

import javax.validation.constraints.*;

/**
 * 定时任务日志表单
 * 2025/2/10
 * @author LiJian
 */
@ApiModel(value = "定时任务日志表单")
public class UpmsQuartzJobLogDto extends UpmsQuartzJobLog{

}