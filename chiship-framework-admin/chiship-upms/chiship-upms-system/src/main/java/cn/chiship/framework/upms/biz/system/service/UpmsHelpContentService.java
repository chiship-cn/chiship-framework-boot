package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.pojo.vo.ContentUnionAllCategoryVo;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContent;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContentExample;

import java.util.HashMap;
import java.util.List;

/**
 * 帮助文档业务接口层
 * 2025/2/9
 * @author lijian
 */
public interface UpmsHelpContentService extends BaseService<UpmsHelpContent,UpmsHelpContentExample> {
    /**
     * 分类与文章结合查询
     *
     * @param map
     * @return
     */
    List<ContentUnionAllCategoryVo> selectContentUnionAllCategory(HashMap<String, Object> map);
}
