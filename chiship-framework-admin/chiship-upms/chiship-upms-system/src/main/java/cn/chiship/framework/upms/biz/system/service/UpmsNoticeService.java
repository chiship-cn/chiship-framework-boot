package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsNoticeSystemDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsNotice;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeExample;

import java.util.List;

/**
 * 通知公告表业务接口层 2021/10/22
 *
 * @author lijian
 */
public interface UpmsNoticeService extends BaseService<UpmsNotice, UpmsNoticeExample> {

	/**
	 * 修改状态
	 * @param id
	 * @param userIds
	 * @return
	 */
	BaseResult publish(String id, List<String> userIds);

	/**
	 * 发送系统消息(站内信)
	 * @param upmsNoticeSystemDto
	 * @return
	 */
	BaseResult sendSystemMsg(UpmsNoticeSystemDto upmsNoticeSystemDto);

	/**
	 * 撤销
	 * @param id
	 * @return
	 */
	BaseResult revoke(String id);

}
