package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.upms.biz.system.service.DbService;
import cn.chiship.sdk.core.annotation.Authorization;
import cn.chiship.sdk.core.base.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/db")
@Api(tags = "DB数据库")
public class DbController {

    @Resource
    DbService dbService;

    @ApiOperation(value = "获取数据表")
    @GetMapping(value = "/listTables")
    public ResponseEntity<BaseResult> listTables() {
        return new ResponseEntity<>(dbService.listTables(), HttpStatus.OK);
    }

    @ApiOperation(value = "获取数据表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "tableName", value = "表名", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping(value = "/listColumns")
    public ResponseEntity<BaseResult> listColumns(@RequestParam(value = "tableName") String tableName) {
        return new ResponseEntity<>(dbService.listColumns(tableName), HttpStatus.OK);
    }

}
