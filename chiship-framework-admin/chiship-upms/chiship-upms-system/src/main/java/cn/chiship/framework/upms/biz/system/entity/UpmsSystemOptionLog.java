package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lj
 * @date 2023-06-15
 */
public class UpmsSystemOptionLog implements Serializable {

	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 用户ID
	 */
	private String userId;

	/**
	 * 用户名
	 */
	private String userName;

	/**
	 * 真实姓名
	 */
	private String realName;

	/**
	 * 操作类型
	 */
	private String optionType;

	/**
	 * 操作IP地址
	 */
	private String ip;

	/**
	 * 操作描述
	 */
	private String description;

	/**
	 * 请求方法
	 */
	private String method;

	/**
	 * 操作时间（时间戳）
	 */
	private Long startTime;

	/**
	 * 请求消耗时长（单位：毫秒）
	 */
	private Integer spendTime;

	/**
	 * 根路径
	 */
	private String basePath;

	/**
	 * 统一资源标识符
	 */
	private String uri;

	/**
	 * 请求类型
	 */
	private String requestType;

	/**
	 * 浏览器用户代理
	 */
	private String userAgent;

	/**
	 * 系统名称
	 */
	private String systemName;

	/**
	 * 扩展属性
	 */
	private String extAttribute;

	/**
	 * 机构编码
	 */
	private String organizationCode;

	/**
	 * 机构名称
	 */
	private String organizationName;

	/**
	 * 操作人类型
	 */
	private String operatorType;

	/**
	 * 请求参数
	 */
	private String parameter;

	/**
	 * 响应载荷
	 */
	private String result;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Integer getSpendTime() {
		return spendTime;
	}

	public void setSpendTime(Integer spendTime) {
		this.spendTime = spendTime;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getExtAttribute() {
		return extAttribute;
	}

	public void setExtAttribute(String extAttribute) {
		this.extAttribute = extAttribute;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public String getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", userId=").append(userId);
		sb.append(", userName=").append(userName);
		sb.append(", realName=").append(realName);
		sb.append(", optionType=").append(optionType);
		sb.append(", ip=").append(ip);
		sb.append(", description=").append(description);
		sb.append(", method=").append(method);
		sb.append(", startTime=").append(startTime);
		sb.append(", spendTime=").append(spendTime);
		sb.append(", basePath=").append(basePath);
		sb.append(", uri=").append(uri);
		sb.append(", requestType=").append(requestType);
		sb.append(", userAgent=").append(userAgent);
		sb.append(", systemName=").append(systemName);
		sb.append(", extAttribute=").append(extAttribute);
		sb.append(", organizationCode=").append(organizationCode);
		sb.append(", organizationName=").append(organizationName);
		sb.append(", operatorType=").append(operatorType);
		sb.append(", parameter=").append(parameter);
		sb.append(", result=").append(result);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsSystemOptionLog other = (UpmsSystemOptionLog) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserName() == null ? other.getUserName() == null
						: this.getUserName().equals(other.getUserName()))
				&& (this.getRealName() == null ? other.getRealName() == null
						: this.getRealName().equals(other.getRealName()))
				&& (this.getOptionType() == null ? other.getOptionType() == null
						: this.getOptionType().equals(other.getOptionType()))
				&& (this.getIp() == null ? other.getIp() == null : this.getIp().equals(other.getIp()))
				&& (this.getDescription() == null ? other.getDescription() == null
						: this.getDescription().equals(other.getDescription()))
				&& (this.getMethod() == null ? other.getMethod() == null : this.getMethod().equals(other.getMethod()))
				&& (this.getStartTime() == null ? other.getStartTime() == null
						: this.getStartTime().equals(other.getStartTime()))
				&& (this.getSpendTime() == null ? other.getSpendTime() == null
						: this.getSpendTime().equals(other.getSpendTime()))
				&& (this.getBasePath() == null ? other.getBasePath() == null
						: this.getBasePath().equals(other.getBasePath()))
				&& (this.getUri() == null ? other.getUri() == null : this.getUri().equals(other.getUri()))
				&& (this.getRequestType() == null ? other.getRequestType() == null
						: this.getRequestType().equals(other.getRequestType()))
				&& (this.getUserAgent() == null ? other.getUserAgent() == null
						: this.getUserAgent().equals(other.getUserAgent()))
				&& (this.getSystemName() == null ? other.getSystemName() == null
						: this.getSystemName().equals(other.getSystemName()))
				&& (this.getExtAttribute() == null ? other.getExtAttribute() == null
						: this.getExtAttribute().equals(other.getExtAttribute()))
				&& (this.getOrganizationCode() == null ? other.getOrganizationCode() == null
						: this.getOrganizationCode().equals(other.getOrganizationCode()))
				&& (this.getOrganizationName() == null ? other.getOrganizationName() == null
						: this.getOrganizationName().equals(other.getOrganizationName()))
				&& (this.getOperatorType() == null ? other.getOperatorType() == null
						: this.getOperatorType().equals(other.getOperatorType()))
				&& (this.getParameter() == null ? other.getParameter() == null
						: this.getParameter().equals(other.getParameter()))
				&& (this.getResult() == null ? other.getResult() == null : this.getResult().equals(other.getResult()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
		result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
		result = prime * result + ((getOptionType() == null) ? 0 : getOptionType().hashCode());
		result = prime * result + ((getIp() == null) ? 0 : getIp().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getMethod() == null) ? 0 : getMethod().hashCode());
		result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
		result = prime * result + ((getSpendTime() == null) ? 0 : getSpendTime().hashCode());
		result = prime * result + ((getBasePath() == null) ? 0 : getBasePath().hashCode());
		result = prime * result + ((getUri() == null) ? 0 : getUri().hashCode());
		result = prime * result + ((getRequestType() == null) ? 0 : getRequestType().hashCode());
		result = prime * result + ((getUserAgent() == null) ? 0 : getUserAgent().hashCode());
		result = prime * result + ((getSystemName() == null) ? 0 : getSystemName().hashCode());
		result = prime * result + ((getExtAttribute() == null) ? 0 : getExtAttribute().hashCode());
		result = prime * result + ((getOrganizationCode() == null) ? 0 : getOrganizationCode().hashCode());
		result = prime * result + ((getOrganizationName() == null) ? 0 : getOrganizationName().hashCode());
		result = prime * result + ((getOperatorType() == null) ? 0 : getOperatorType().hashCode());
		result = prime * result + ((getParameter() == null) ? 0 : getParameter().hashCode());
		result = prime * result + ((getResult() == null) ? 0 : getResult().hashCode());
		return result;
	}

}