package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsRegion;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegionExample;
import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * 地区Mapper
 *
 * @author lj
 */
public interface UpmsRegionMapper extends BaseMapper<UpmsRegion, UpmsRegionExample> {

}