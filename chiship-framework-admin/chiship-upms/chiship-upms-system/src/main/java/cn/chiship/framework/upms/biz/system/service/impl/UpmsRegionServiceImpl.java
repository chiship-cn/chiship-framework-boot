package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.common.constants.CommonConstants;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegion;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegionExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsRegionMapper;
import cn.chiship.framework.upms.biz.system.service.UpmsRegionService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.http.HttpUtils;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.sdk.framework.pojo.dto.export.ExportDto;
import cn.chiship.sdk.framework.pojo.dto.export.ExportTransferDataDto;
import cn.chiship.sdk.framework.pojo.vo.RegionVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 地区表业务接口实现层 2021/9/30
 *
 * @author lijian
 */
@Service
public class UpmsRegionServiceImpl extends BaseServiceImpl<UpmsRegion, UpmsRegionExample> implements UpmsRegionService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsRegionServiceImpl.class);

	@Resource
	UpmsRegionMapper upmsRegionMapper;

	@Resource
	RedisService redisService;

	private String fileName = "行政区划";

	private String sheetName = "行政区划";

	private String sheetTitle = "行政区划明细";

	@Override
	public String getCacheRegionNameById(Long regionId) {
		RegionVo regionVo = getCacheRegionById(regionId);
		if (ObjectUtils.isEmpty(regionVo)) {
			return "";
		}
		return regionVo.getName();
	}

	@Override
	public RegionVo getCacheRegionById(Long regionId) {
		if (StringUtil.isNull(regionId)) {
			return null;
		}
		String itemKey = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_REGION_ITEM_PREFIX) + ":" + regionId;
		if (!redisService.hasKey(itemKey)) {
			return null;
		}
		return (RegionVo) redisService.get(itemKey);
	}

	@Override
	public BaseResult loadGeoByPidAndName(Long pid, String name) {
		UpmsRegionExample upmsRegionExample = new UpmsRegionExample();
		UpmsRegionExample.Criteria criteria = upmsRegionExample.createCriteria();
		if (-1L == pid) {
			pid = 0L;
		}
		else {
			criteria.andNameEqualTo(name);
		}
		criteria.andPidEqualTo(pid);
		upmsRegionExample.setOrderByClause("id asc");
		List<UpmsRegion> regions = upmsRegionMapper.selectByExample(upmsRegionExample);
		if (regions.isEmpty()) {
			return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_NO, null);

		}
		UpmsRegion region = regions.get(0);
		BaseResult baseResult = HttpUtils.doGet("https://cdn-chiship.oss-cn-beijing.aliyuncs.com",
				"/geo/" + region.getAdCode() + "_full.json");
		if (!baseResult.isSuccess()) {
			return baseResult;
		}
		JSONObject json = JSON.parseObject(StringUtil.getString(baseResult.getData()));
		json.put("pid", region.getId());
		return BaseResult.ok(json);
	}

	@Override
	public BaseResult cacheListByPid(Long pid) {
		List<JSONObject> list = new ArrayList<>();
		if (StringUtil.isNull(pid)) {
			pid = CommonConstants.PROVINCE_PID;
		}
		String key = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_REGIONS_PREFIX);
		List<RegionVo> regions = (List<RegionVo>) redisService.hget(key, String.valueOf(pid));
		if (regions == null) {
			return BaseResult.ok(new ArrayList<>());

		}
		for (RegionVo region : regions) {
			JSONObject json = (JSONObject) JSON.toJSON(region);
			if ((redisService.hget(key, String.valueOf(region.getId()))) == null) {
				json.put("isParent", false);
				json.put("isLeaf", true);
			}
			else {
				json.put("isParent", true);
				json.put("isLeaf", false);
			}
			list.add(json);
		}
		return BaseResult.ok(list);
	}

	@Override
	public ExportTransferDataDto assemblyExportData(ExportDto exportDto) {
		ArrayList<String> labels = new ArrayList<>();
		labels.add("区域编码");
		labels.add("区域名称");
		labels.add("级别");
		UpmsRegionExample upmsRegionExample = new UpmsRegionExample();
		upmsRegionExample.setOrderByClause("id asc");
		List<UpmsRegion> regions = upmsRegionMapper.selectByExample(upmsRegionExample);

		List<List<String>> valueList = new ArrayList<>();
		for (UpmsRegion region : regions) {
			List<String> values = new ArrayList<>();
			values.add(region.getAdCode());
			values.add(region.getName());
			values.add(region.getLevel());
			valueList.add(values);
		}
		int total = regions.size();
		return new ExportTransferDataDto(fileName, sheetName, sheetTitle, labels, valueList, total);
	}

}
