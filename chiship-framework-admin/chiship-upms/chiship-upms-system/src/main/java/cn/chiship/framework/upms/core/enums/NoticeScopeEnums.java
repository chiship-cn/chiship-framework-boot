package cn.chiship.framework.upms.core.enums;

/**
 * 通知公告接收范围
 */
public enum NoticeScopeEnums {

	NOTICE_SCOPE_ALL("ALL"),

	NOTICE_SCOPE_USER("USER"),

	NOTICE_SCOPE_MEMBER("MEMBER"),;

	/**
	 * 类型
	 */
	private String type;

	NoticeScopeEnums(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
