package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCode;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsCodeExample;
import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface UpmsSmsCodeMapper extends BaseMapper<UpmsSmsCode, UpmsSmsCodeExample> {

}