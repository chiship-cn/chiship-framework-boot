package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJob;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobExample;

/**
 * @author lijian
 */
public interface UpmsQuartzJobMapper extends BaseMapper<UpmsQuartzJob, UpmsQuartzJobExample> {

}