package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDict;
import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDictExample;
import com.alibaba.fastjson.JSONObject;

/**
 * 分类字典业务接口层 2024/10/13
 *
 * @author lijian
 */
public interface UpmsCategoryDictService extends BaseService<UpmsCategoryDict, UpmsCategoryDictExample> {

	/**
	 * 表格树
	 * @param upmsCategoryDictExample
	 * @return
	 */
	BaseResult treeTable(UpmsCategoryDictExample upmsCategoryDictExample);

	/**
	 * 从缓存中加载树状
	 * @param type
	 * @return
	 */
	BaseResult cacheTreeTable(String type);

	/**
	 * 生成层级
	 * @param pid
	 * @return
	 */
	BaseResult generateTreeNumber(String pid);

	/**
	 * 缓存数据
	 */
	void cacheCategoryDict();

	/**
	 * 缓存获取TreeNumber
	 * @param id
	 * @return
	 */
	String cacheGetTreeNumberById(String id);

	/**
	 * 根据主键获取完整信息
	 * @param id 主键
	 * @return
	 */
	JSONObject getParentInfoById(String id);

	/**
	 * 根据层级编码获取完整信息
	 * @param treeNumber
	 * @return
	 */
	JSONObject getParentInfoByTreeNumber(String treeNumber);

	/**
	 * 根据主键获取机构名
	 * @param id 主键
	 * @param isFull 是否完整
	 * @return
	 */
	String getCategoryNameById(String id, Boolean isFull);

}
