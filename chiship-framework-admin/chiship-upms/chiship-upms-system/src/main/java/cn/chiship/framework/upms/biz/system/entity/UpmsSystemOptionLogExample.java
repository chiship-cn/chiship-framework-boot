package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lj
 * @date 2023-06-15
 */
public class UpmsSystemOptionLogExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsSystemOptionLogExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNull() {
			addCriterion("user_name is null");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNotNull() {
			addCriterion("user_name is not null");
			return (Criteria) this;
		}

		public Criteria andUserNameEqualTo(String value) {
			addCriterion("user_name =", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotEqualTo(String value) {
			addCriterion("user_name <>", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThan(String value) {
			addCriterion("user_name >", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("user_name >=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThan(String value) {
			addCriterion("user_name <", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThanOrEqualTo(String value) {
			addCriterion("user_name <=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLike(String value) {
			addCriterion("user_name like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotLike(String value) {
			addCriterion("user_name not like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameIn(List<String> values) {
			addCriterion("user_name in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotIn(List<String> values) {
			addCriterion("user_name not in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameBetween(String value1, String value2) {
			addCriterion("user_name between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotBetween(String value1, String value2) {
			addCriterion("user_name not between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNull() {
			addCriterion("real_name is null");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNotNull() {
			addCriterion("real_name is not null");
			return (Criteria) this;
		}

		public Criteria andRealNameEqualTo(String value) {
			addCriterion("real_name =", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotEqualTo(String value) {
			addCriterion("real_name <>", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThan(String value) {
			addCriterion("real_name >", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("real_name >=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThan(String value) {
			addCriterion("real_name <", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThanOrEqualTo(String value) {
			addCriterion("real_name <=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLike(String value) {
			addCriterion("real_name like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotLike(String value) {
			addCriterion("real_name not like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameIn(List<String> values) {
			addCriterion("real_name in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotIn(List<String> values) {
			addCriterion("real_name not in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameBetween(String value1, String value2) {
			addCriterion("real_name between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotBetween(String value1, String value2) {
			addCriterion("real_name not between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andOptionTypeIsNull() {
			addCriterion("option_type is null");
			return (Criteria) this;
		}

		public Criteria andOptionTypeIsNotNull() {
			addCriterion("option_type is not null");
			return (Criteria) this;
		}

		public Criteria andOptionTypeEqualTo(String value) {
			addCriterion("option_type =", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeNotEqualTo(String value) {
			addCriterion("option_type <>", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeGreaterThan(String value) {
			addCriterion("option_type >", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeGreaterThanOrEqualTo(String value) {
			addCriterion("option_type >=", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeLessThan(String value) {
			addCriterion("option_type <", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeLessThanOrEqualTo(String value) {
			addCriterion("option_type <=", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeLike(String value) {
			addCriterion("option_type like", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeNotLike(String value) {
			addCriterion("option_type not like", value, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeIn(List<String> values) {
			addCriterion("option_type in", values, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeNotIn(List<String> values) {
			addCriterion("option_type not in", values, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeBetween(String value1, String value2) {
			addCriterion("option_type between", value1, value2, "optionType");
			return (Criteria) this;
		}

		public Criteria andOptionTypeNotBetween(String value1, String value2) {
			addCriterion("option_type not between", value1, value2, "optionType");
			return (Criteria) this;
		}

		public Criteria andIpIsNull() {
			addCriterion("ip is null");
			return (Criteria) this;
		}

		public Criteria andIpIsNotNull() {
			addCriterion("ip is not null");
			return (Criteria) this;
		}

		public Criteria andIpEqualTo(String value) {
			addCriterion("ip =", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotEqualTo(String value) {
			addCriterion("ip <>", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpGreaterThan(String value) {
			addCriterion("ip >", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpGreaterThanOrEqualTo(String value) {
			addCriterion("ip >=", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLessThan(String value) {
			addCriterion("ip <", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLessThanOrEqualTo(String value) {
			addCriterion("ip <=", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLike(String value) {
			addCriterion("ip like", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotLike(String value) {
			addCriterion("ip not like", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpIn(List<String> values) {
			addCriterion("ip in", values, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotIn(List<String> values) {
			addCriterion("ip not in", values, "ip");
			return (Criteria) this;
		}

		public Criteria andIpBetween(String value1, String value2) {
			addCriterion("ip between", value1, value2, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotBetween(String value1, String value2) {
			addCriterion("ip not between", value1, value2, "ip");
			return (Criteria) this;
		}

		public Criteria andDescriptionIsNull() {
			addCriterion("description is null");
			return (Criteria) this;
		}

		public Criteria andDescriptionIsNotNull() {
			addCriterion("description is not null");
			return (Criteria) this;
		}

		public Criteria andDescriptionEqualTo(String value) {
			addCriterion("description =", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotEqualTo(String value) {
			addCriterion("description <>", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionGreaterThan(String value) {
			addCriterion("description >", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionGreaterThanOrEqualTo(String value) {
			addCriterion("description >=", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLessThan(String value) {
			addCriterion("description <", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLessThanOrEqualTo(String value) {
			addCriterion("description <=", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionLike(String value) {
			addCriterion("description like", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotLike(String value) {
			addCriterion("description not like", value, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionIn(List<String> values) {
			addCriterion("description in", values, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotIn(List<String> values) {
			addCriterion("description not in", values, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionBetween(String value1, String value2) {
			addCriterion("description between", value1, value2, "description");
			return (Criteria) this;
		}

		public Criteria andDescriptionNotBetween(String value1, String value2) {
			addCriterion("description not between", value1, value2, "description");
			return (Criteria) this;
		}

		public Criteria andMethodIsNull() {
			addCriterion("method is null");
			return (Criteria) this;
		}

		public Criteria andMethodIsNotNull() {
			addCriterion("method is not null");
			return (Criteria) this;
		}

		public Criteria andMethodEqualTo(String value) {
			addCriterion("method =", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodNotEqualTo(String value) {
			addCriterion("method <>", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodGreaterThan(String value) {
			addCriterion("method >", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodGreaterThanOrEqualTo(String value) {
			addCriterion("method >=", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodLessThan(String value) {
			addCriterion("method <", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodLessThanOrEqualTo(String value) {
			addCriterion("method <=", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodLike(String value) {
			addCriterion("method like", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodNotLike(String value) {
			addCriterion("method not like", value, "method");
			return (Criteria) this;
		}

		public Criteria andMethodIn(List<String> values) {
			addCriterion("method in", values, "method");
			return (Criteria) this;
		}

		public Criteria andMethodNotIn(List<String> values) {
			addCriterion("method not in", values, "method");
			return (Criteria) this;
		}

		public Criteria andMethodBetween(String value1, String value2) {
			addCriterion("method between", value1, value2, "method");
			return (Criteria) this;
		}

		public Criteria andMethodNotBetween(String value1, String value2) {
			addCriterion("method not between", value1, value2, "method");
			return (Criteria) this;
		}

		public Criteria andStartTimeIsNull() {
			addCriterion("start_time is null");
			return (Criteria) this;
		}

		public Criteria andStartTimeIsNotNull() {
			addCriterion("start_time is not null");
			return (Criteria) this;
		}

		public Criteria andStartTimeEqualTo(Long value) {
			addCriterion("start_time =", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotEqualTo(Long value) {
			addCriterion("start_time <>", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeGreaterThan(Long value) {
			addCriterion("start_time >", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("start_time >=", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeLessThan(Long value) {
			addCriterion("start_time <", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeLessThanOrEqualTo(Long value) {
			addCriterion("start_time <=", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeIn(List<Long> values) {
			addCriterion("start_time in", values, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotIn(List<Long> values) {
			addCriterion("start_time not in", values, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeBetween(Long value1, Long value2) {
			addCriterion("start_time between", value1, value2, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotBetween(Long value1, Long value2) {
			addCriterion("start_time not between", value1, value2, "startTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeIsNull() {
			addCriterion("spend_time is null");
			return (Criteria) this;
		}

		public Criteria andSpendTimeIsNotNull() {
			addCriterion("spend_time is not null");
			return (Criteria) this;
		}

		public Criteria andSpendTimeEqualTo(Integer value) {
			addCriterion("spend_time =", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeNotEqualTo(Integer value) {
			addCriterion("spend_time <>", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeGreaterThan(Integer value) {
			addCriterion("spend_time >", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeGreaterThanOrEqualTo(Integer value) {
			addCriterion("spend_time >=", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeLessThan(Integer value) {
			addCriterion("spend_time <", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeLessThanOrEqualTo(Integer value) {
			addCriterion("spend_time <=", value, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeIn(List<Integer> values) {
			addCriterion("spend_time in", values, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeNotIn(List<Integer> values) {
			addCriterion("spend_time not in", values, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeBetween(Integer value1, Integer value2) {
			addCriterion("spend_time between", value1, value2, "spendTime");
			return (Criteria) this;
		}

		public Criteria andSpendTimeNotBetween(Integer value1, Integer value2) {
			addCriterion("spend_time not between", value1, value2, "spendTime");
			return (Criteria) this;
		}

		public Criteria andBasePathIsNull() {
			addCriterion("base_path is null");
			return (Criteria) this;
		}

		public Criteria andBasePathIsNotNull() {
			addCriterion("base_path is not null");
			return (Criteria) this;
		}

		public Criteria andBasePathEqualTo(String value) {
			addCriterion("base_path =", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathNotEqualTo(String value) {
			addCriterion("base_path <>", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathGreaterThan(String value) {
			addCriterion("base_path >", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathGreaterThanOrEqualTo(String value) {
			addCriterion("base_path >=", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathLessThan(String value) {
			addCriterion("base_path <", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathLessThanOrEqualTo(String value) {
			addCriterion("base_path <=", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathLike(String value) {
			addCriterion("base_path like", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathNotLike(String value) {
			addCriterion("base_path not like", value, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathIn(List<String> values) {
			addCriterion("base_path in", values, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathNotIn(List<String> values) {
			addCriterion("base_path not in", values, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathBetween(String value1, String value2) {
			addCriterion("base_path between", value1, value2, "basePath");
			return (Criteria) this;
		}

		public Criteria andBasePathNotBetween(String value1, String value2) {
			addCriterion("base_path not between", value1, value2, "basePath");
			return (Criteria) this;
		}

		public Criteria andUriIsNull() {
			addCriterion("uri is null");
			return (Criteria) this;
		}

		public Criteria andUriIsNotNull() {
			addCriterion("uri is not null");
			return (Criteria) this;
		}

		public Criteria andUriEqualTo(String value) {
			addCriterion("uri =", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriNotEqualTo(String value) {
			addCriterion("uri <>", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriGreaterThan(String value) {
			addCriterion("uri >", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriGreaterThanOrEqualTo(String value) {
			addCriterion("uri >=", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriLessThan(String value) {
			addCriterion("uri <", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriLessThanOrEqualTo(String value) {
			addCriterion("uri <=", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriLike(String value) {
			addCriterion("uri like", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriNotLike(String value) {
			addCriterion("uri not like", value, "uri");
			return (Criteria) this;
		}

		public Criteria andUriIn(List<String> values) {
			addCriterion("uri in", values, "uri");
			return (Criteria) this;
		}

		public Criteria andUriNotIn(List<String> values) {
			addCriterion("uri not in", values, "uri");
			return (Criteria) this;
		}

		public Criteria andUriBetween(String value1, String value2) {
			addCriterion("uri between", value1, value2, "uri");
			return (Criteria) this;
		}

		public Criteria andUriNotBetween(String value1, String value2) {
			addCriterion("uri not between", value1, value2, "uri");
			return (Criteria) this;
		}

		public Criteria andRequestTypeIsNull() {
			addCriterion("request_type is null");
			return (Criteria) this;
		}

		public Criteria andRequestTypeIsNotNull() {
			addCriterion("request_type is not null");
			return (Criteria) this;
		}

		public Criteria andRequestTypeEqualTo(String value) {
			addCriterion("request_type =", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeNotEqualTo(String value) {
			addCriterion("request_type <>", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeGreaterThan(String value) {
			addCriterion("request_type >", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeGreaterThanOrEqualTo(String value) {
			addCriterion("request_type >=", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeLessThan(String value) {
			addCriterion("request_type <", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeLessThanOrEqualTo(String value) {
			addCriterion("request_type <=", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeLike(String value) {
			addCriterion("request_type like", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeNotLike(String value) {
			addCriterion("request_type not like", value, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeIn(List<String> values) {
			addCriterion("request_type in", values, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeNotIn(List<String> values) {
			addCriterion("request_type not in", values, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeBetween(String value1, String value2) {
			addCriterion("request_type between", value1, value2, "requestType");
			return (Criteria) this;
		}

		public Criteria andRequestTypeNotBetween(String value1, String value2) {
			addCriterion("request_type not between", value1, value2, "requestType");
			return (Criteria) this;
		}

		public Criteria andUserAgentIsNull() {
			addCriterion("user_agent is null");
			return (Criteria) this;
		}

		public Criteria andUserAgentIsNotNull() {
			addCriterion("user_agent is not null");
			return (Criteria) this;
		}

		public Criteria andUserAgentEqualTo(String value) {
			addCriterion("user_agent =", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentNotEqualTo(String value) {
			addCriterion("user_agent <>", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentGreaterThan(String value) {
			addCriterion("user_agent >", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentGreaterThanOrEqualTo(String value) {
			addCriterion("user_agent >=", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentLessThan(String value) {
			addCriterion("user_agent <", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentLessThanOrEqualTo(String value) {
			addCriterion("user_agent <=", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentLike(String value) {
			addCriterion("user_agent like", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentNotLike(String value) {
			addCriterion("user_agent not like", value, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentIn(List<String> values) {
			addCriterion("user_agent in", values, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentNotIn(List<String> values) {
			addCriterion("user_agent not in", values, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentBetween(String value1, String value2) {
			addCriterion("user_agent between", value1, value2, "userAgent");
			return (Criteria) this;
		}

		public Criteria andUserAgentNotBetween(String value1, String value2) {
			addCriterion("user_agent not between", value1, value2, "userAgent");
			return (Criteria) this;
		}

		public Criteria andSystemNameIsNull() {
			addCriterion("system_name is null");
			return (Criteria) this;
		}

		public Criteria andSystemNameIsNotNull() {
			addCriterion("system_name is not null");
			return (Criteria) this;
		}

		public Criteria andSystemNameEqualTo(String value) {
			addCriterion("system_name =", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameNotEqualTo(String value) {
			addCriterion("system_name <>", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameGreaterThan(String value) {
			addCriterion("system_name >", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameGreaterThanOrEqualTo(String value) {
			addCriterion("system_name >=", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameLessThan(String value) {
			addCriterion("system_name <", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameLessThanOrEqualTo(String value) {
			addCriterion("system_name <=", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameLike(String value) {
			addCriterion("system_name like", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameNotLike(String value) {
			addCriterion("system_name not like", value, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameIn(List<String> values) {
			addCriterion("system_name in", values, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameNotIn(List<String> values) {
			addCriterion("system_name not in", values, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameBetween(String value1, String value2) {
			addCriterion("system_name between", value1, value2, "systemName");
			return (Criteria) this;
		}

		public Criteria andSystemNameNotBetween(String value1, String value2) {
			addCriterion("system_name not between", value1, value2, "systemName");
			return (Criteria) this;
		}

		public Criteria andExtAttributeIsNull() {
			addCriterion("ext_attribute is null");
			return (Criteria) this;
		}

		public Criteria andExtAttributeIsNotNull() {
			addCriterion("ext_attribute is not null");
			return (Criteria) this;
		}

		public Criteria andExtAttributeEqualTo(String value) {
			addCriterion("ext_attribute =", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeNotEqualTo(String value) {
			addCriterion("ext_attribute <>", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeGreaterThan(String value) {
			addCriterion("ext_attribute >", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeGreaterThanOrEqualTo(String value) {
			addCriterion("ext_attribute >=", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeLessThan(String value) {
			addCriterion("ext_attribute <", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeLessThanOrEqualTo(String value) {
			addCriterion("ext_attribute <=", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeLike(String value) {
			addCriterion("ext_attribute like", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeNotLike(String value) {
			addCriterion("ext_attribute not like", value, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeIn(List<String> values) {
			addCriterion("ext_attribute in", values, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeNotIn(List<String> values) {
			addCriterion("ext_attribute not in", values, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeBetween(String value1, String value2) {
			addCriterion("ext_attribute between", value1, value2, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andExtAttributeNotBetween(String value1, String value2) {
			addCriterion("ext_attribute not between", value1, value2, "extAttribute");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIsNull() {
			addCriterion("organization_code is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIsNotNull() {
			addCriterion("organization_code is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeEqualTo(String value) {
			addCriterion("organization_code =", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotEqualTo(String value) {
			addCriterion("organization_code <>", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeGreaterThan(String value) {
			addCriterion("organization_code >", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeGreaterThanOrEqualTo(String value) {
			addCriterion("organization_code >=", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLessThan(String value) {
			addCriterion("organization_code <", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLessThanOrEqualTo(String value) {
			addCriterion("organization_code <=", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeLike(String value) {
			addCriterion("organization_code like", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotLike(String value) {
			addCriterion("organization_code not like", value, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeIn(List<String> values) {
			addCriterion("organization_code in", values, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotIn(List<String> values) {
			addCriterion("organization_code not in", values, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeBetween(String value1, String value2) {
			addCriterion("organization_code between", value1, value2, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationCodeNotBetween(String value1, String value2) {
			addCriterion("organization_code not between", value1, value2, "organizationCode");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIsNull() {
			addCriterion("organization_name is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIsNotNull() {
			addCriterion("organization_name is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameEqualTo(String value) {
			addCriterion("organization_name =", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotEqualTo(String value) {
			addCriterion("organization_name <>", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameGreaterThan(String value) {
			addCriterion("organization_name >", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameGreaterThanOrEqualTo(String value) {
			addCriterion("organization_name >=", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLessThan(String value) {
			addCriterion("organization_name <", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLessThanOrEqualTo(String value) {
			addCriterion("organization_name <=", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameLike(String value) {
			addCriterion("organization_name like", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotLike(String value) {
			addCriterion("organization_name not like", value, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameIn(List<String> values) {
			addCriterion("organization_name in", values, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotIn(List<String> values) {
			addCriterion("organization_name not in", values, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameBetween(String value1, String value2) {
			addCriterion("organization_name between", value1, value2, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOrganizationNameNotBetween(String value1, String value2) {
			addCriterion("organization_name not between", value1, value2, "organizationName");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeIsNull() {
			addCriterion("operator_type is null");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeIsNotNull() {
			addCriterion("operator_type is not null");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeEqualTo(String value) {
			addCriterion("operator_type =", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeNotEqualTo(String value) {
			addCriterion("operator_type <>", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeGreaterThan(String value) {
			addCriterion("operator_type >", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeGreaterThanOrEqualTo(String value) {
			addCriterion("operator_type >=", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeLessThan(String value) {
			addCriterion("operator_type <", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeLessThanOrEqualTo(String value) {
			addCriterion("operator_type <=", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeLike(String value) {
			addCriterion("operator_type like", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeNotLike(String value) {
			addCriterion("operator_type not like", value, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeIn(List<String> values) {
			addCriterion("operator_type in", values, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeNotIn(List<String> values) {
			addCriterion("operator_type not in", values, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeBetween(String value1, String value2) {
			addCriterion("operator_type between", value1, value2, "operatorType");
			return (Criteria) this;
		}

		public Criteria andOperatorTypeNotBetween(String value1, String value2) {
			addCriterion("operator_type not between", value1, value2, "operatorType");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}