package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-11
 */
public class UpmsNotice implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 创建者
	 */
	private String createdBy;

	/**
	 * 更新者
	 */
	private String modifyBy;

	/**
	 * 创建者用户ID
	 */
	private String createdUserId;

	/**
	 * 创建者组织ID
	 */
	private String createdOrgId;

	/**
	 * 公告标题
	 */
	private String noticeTitle;

	/**
	 * 摘要
	 */
	private String noticeAbstract;

	/**
	 * 附件
	 */
	private String noticeFile;

	/**
	 * 用户类型（admin:管理人员，member:会员）
	 */
	private String noticeUserType;

	/**
	 * 通告对象类型（USER:指定用户，ALL:全体用户）
	 */
	private String noticeCategory;

	/**
	 * 消息类型1:通知公告2:系统消息
	 */
	private Byte noticeType;

	/**
	 * 模块类型
	 */
	private String moduleType;

	/**
	 * 指定用户
	 */
	private String receiver;

	/**
	 * 开始时间
	 */
	private Long startTime;

	/**
	 * 结束时间
	 */
	private Long endTime;

	/**
	 * 发布人
	 */
	private String sender;

	/**
	 * 优先级（0低，1中，2高）
	 */
	private Byte priority;

	/**
	 * 发布状态（0未发布，1已发布，2已撤销）
	 */
	private Byte sendStatus;

	/**
	 * 发布时间
	 */
	private Long sendTime;

	/**
	 * 撤销时间
	 */
	private Long cancelTime;

	/**
	 * 业务类型(email:邮件 bpm:流程)
	 */
	private String busType;

	/**
	 * 业务id
	 */
	private String busId;

	/**
	 * 钉钉task_id，用于撤回消息
	 */
	private String dtTaskId;

	/**
	 * 公告内容
	 */
	private String noticeContent;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getCreatedOrgId() {
		return createdOrgId;
	}

	public void setCreatedOrgId(String createdOrgId) {
		this.createdOrgId = createdOrgId;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public String getNoticeAbstract() {
		return noticeAbstract;
	}

	public void setNoticeAbstract(String noticeAbstract) {
		this.noticeAbstract = noticeAbstract;
	}

	public String getNoticeFile() {
		return noticeFile;
	}

	public void setNoticeFile(String noticeFile) {
		this.noticeFile = noticeFile;
	}

	public String getNoticeUserType() {
		return noticeUserType;
	}

	public void setNoticeUserType(String noticeUserType) {
		this.noticeUserType = noticeUserType;
	}

	public String getNoticeCategory() {
		return noticeCategory;
	}

	public void setNoticeCategory(String noticeCategory) {
		this.noticeCategory = noticeCategory;
	}

	public Byte getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(Byte noticeType) {
		this.noticeType = noticeType;
	}

	public String getModuleType() {
		return moduleType;
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public Byte getPriority() {
		return priority;
	}

	public void setPriority(Byte priority) {
		this.priority = priority;
	}

	public Byte getSendStatus() {
		return sendStatus;
	}

	public void setSendStatus(Byte sendStatus) {
		this.sendStatus = sendStatus;
	}

	public Long getSendTime() {
		return sendTime;
	}

	public void setSendTime(Long sendTime) {
		this.sendTime = sendTime;
	}

	public Long getCancelTime() {
		return cancelTime;
	}

	public void setCancelTime(Long cancelTime) {
		this.cancelTime = cancelTime;
	}

	public String getBusType() {
		return busType;
	}

	public void setBusType(String busType) {
		this.busType = busType;
	}

	public String getBusId() {
		return busId;
	}

	public void setBusId(String busId) {
		this.busId = busId;
	}

	public String getDtTaskId() {
		return dtTaskId;
	}

	public void setDtTaskId(String dtTaskId) {
		this.dtTaskId = dtTaskId;
	}

	public String getNoticeContent() {
		return noticeContent;
	}

	public void setNoticeContent(String noticeContent) {
		this.noticeContent = noticeContent;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", modifyBy=").append(modifyBy);
		sb.append(", createdUserId=").append(createdUserId);
		sb.append(", createdOrgId=").append(createdOrgId);
		sb.append(", noticeTitle=").append(noticeTitle);
		sb.append(", noticeAbstract=").append(noticeAbstract);
		sb.append(", noticeFile=").append(noticeFile);
		sb.append(", noticeUserType=").append(noticeUserType);
		sb.append(", noticeCategory=").append(noticeCategory);
		sb.append(", noticeType=").append(noticeType);
		sb.append(", moduleType=").append(moduleType);
		sb.append(", receiver=").append(receiver);
		sb.append(", startTime=").append(startTime);
		sb.append(", endTime=").append(endTime);
		sb.append(", sender=").append(sender);
		sb.append(", priority=").append(priority);
		sb.append(", sendStatus=").append(sendStatus);
		sb.append(", sendTime=").append(sendTime);
		sb.append(", cancelTime=").append(cancelTime);
		sb.append(", busType=").append(busType);
		sb.append(", busId=").append(busId);
		sb.append(", dtTaskId=").append(dtTaskId);
		sb.append(", noticeContent=").append(noticeContent);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsNotice other = (UpmsNotice) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getModifyBy() == null ? other.getModifyBy() == null
						: this.getModifyBy().equals(other.getModifyBy()))
				&& (this.getCreatedUserId() == null ? other.getCreatedUserId() == null
						: this.getCreatedUserId().equals(other.getCreatedUserId()))
				&& (this.getCreatedOrgId() == null ? other.getCreatedOrgId() == null
						: this.getCreatedOrgId().equals(other.getCreatedOrgId()))
				&& (this.getNoticeTitle() == null ? other.getNoticeTitle() == null
						: this.getNoticeTitle().equals(other.getNoticeTitle()))
				&& (this.getNoticeAbstract() == null ? other.getNoticeAbstract() == null
						: this.getNoticeAbstract().equals(other.getNoticeAbstract()))
				&& (this.getNoticeFile() == null ? other.getNoticeFile() == null
						: this.getNoticeFile().equals(other.getNoticeFile()))
				&& (this.getNoticeUserType() == null ? other.getNoticeUserType() == null
						: this.getNoticeUserType().equals(other.getNoticeUserType()))
				&& (this.getNoticeCategory() == null ? other.getNoticeCategory() == null
						: this.getNoticeCategory().equals(other.getNoticeCategory()))
				&& (this.getNoticeType() == null ? other.getNoticeType() == null
						: this.getNoticeType().equals(other.getNoticeType()))
				&& (this.getModuleType() == null ? other.getModuleType() == null
						: this.getModuleType().equals(other.getModuleType()))
				&& (this.getReceiver() == null ? other.getReceiver() == null
						: this.getReceiver().equals(other.getReceiver()))
				&& (this.getStartTime() == null ? other.getStartTime() == null
						: this.getStartTime().equals(other.getStartTime()))
				&& (this.getEndTime() == null ? other.getEndTime() == null
						: this.getEndTime().equals(other.getEndTime()))
				&& (this.getSender() == null ? other.getSender() == null : this.getSender().equals(other.getSender()))
				&& (this.getPriority() == null ? other.getPriority() == null
						: this.getPriority().equals(other.getPriority()))
				&& (this.getSendStatus() == null ? other.getSendStatus() == null
						: this.getSendStatus().equals(other.getSendStatus()))
				&& (this.getSendTime() == null ? other.getSendTime() == null
						: this.getSendTime().equals(other.getSendTime()))
				&& (this.getCancelTime() == null ? other.getCancelTime() == null
						: this.getCancelTime().equals(other.getCancelTime()))
				&& (this.getBusType() == null ? other.getBusType() == null
						: this.getBusType().equals(other.getBusType()))
				&& (this.getBusId() == null ? other.getBusId() == null : this.getBusId().equals(other.getBusId()))
				&& (this.getDtTaskId() == null ? other.getDtTaskId() == null
						: this.getDtTaskId().equals(other.getDtTaskId()))
				&& (this.getNoticeContent() == null ? other.getNoticeContent() == null
						: this.getNoticeContent().equals(other.getNoticeContent()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
		result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
		result = prime * result + ((getCreatedOrgId() == null) ? 0 : getCreatedOrgId().hashCode());
		result = prime * result + ((getNoticeTitle() == null) ? 0 : getNoticeTitle().hashCode());
		result = prime * result + ((getNoticeAbstract() == null) ? 0 : getNoticeAbstract().hashCode());
		result = prime * result + ((getNoticeFile() == null) ? 0 : getNoticeFile().hashCode());
		result = prime * result + ((getNoticeUserType() == null) ? 0 : getNoticeUserType().hashCode());
		result = prime * result + ((getNoticeCategory() == null) ? 0 : getNoticeCategory().hashCode());
		result = prime * result + ((getNoticeType() == null) ? 0 : getNoticeType().hashCode());
		result = prime * result + ((getModuleType() == null) ? 0 : getModuleType().hashCode());
		result = prime * result + ((getReceiver() == null) ? 0 : getReceiver().hashCode());
		result = prime * result + ((getStartTime() == null) ? 0 : getStartTime().hashCode());
		result = prime * result + ((getEndTime() == null) ? 0 : getEndTime().hashCode());
		result = prime * result + ((getSender() == null) ? 0 : getSender().hashCode());
		result = prime * result + ((getPriority() == null) ? 0 : getPriority().hashCode());
		result = prime * result + ((getSendStatus() == null) ? 0 : getSendStatus().hashCode());
		result = prime * result + ((getSendTime() == null) ? 0 : getSendTime().hashCode());
		result = prime * result + ((getCancelTime() == null) ? 0 : getCancelTime().hashCode());
		result = prime * result + ((getBusType() == null) ? 0 : getBusType().hashCode());
		result = prime * result + ((getBusId() == null) ? 0 : getBusId().hashCode());
		result = prime * result + ((getDtTaskId() == null) ? 0 : getDtTaskId().hashCode());
		result = prime * result + ((getNoticeContent() == null) ? 0 : getNoticeContent().hashCode());
		return result;
	}

}