package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class UpmsQuartzJobExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsQuartzJobExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andJobTitleIsNull() {
			addCriterion("job_title is null");
			return (Criteria) this;
		}

		public Criteria andJobTitleIsNotNull() {
			addCriterion("job_title is not null");
			return (Criteria) this;
		}

		public Criteria andJobTitleEqualTo(String value) {
			addCriterion("job_title =", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleNotEqualTo(String value) {
			addCriterion("job_title <>", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleGreaterThan(String value) {
			addCriterion("job_title >", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleGreaterThanOrEqualTo(String value) {
			addCriterion("job_title >=", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleLessThan(String value) {
			addCriterion("job_title <", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleLessThanOrEqualTo(String value) {
			addCriterion("job_title <=", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleLike(String value) {
			addCriterion("job_title like", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleNotLike(String value) {
			addCriterion("job_title not like", value, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleIn(List<String> values) {
			addCriterion("job_title in", values, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleNotIn(List<String> values) {
			addCriterion("job_title not in", values, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleBetween(String value1, String value2) {
			addCriterion("job_title between", value1, value2, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobTitleNotBetween(String value1, String value2) {
			addCriterion("job_title not between", value1, value2, "jobTitle");
			return (Criteria) this;
		}

		public Criteria andJobClassIsNull() {
			addCriterion("job_class is null");
			return (Criteria) this;
		}

		public Criteria andJobClassIsNotNull() {
			addCriterion("job_class is not null");
			return (Criteria) this;
		}

		public Criteria andJobClassEqualTo(String value) {
			addCriterion("job_class =", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassNotEqualTo(String value) {
			addCriterion("job_class <>", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassGreaterThan(String value) {
			addCriterion("job_class >", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassGreaterThanOrEqualTo(String value) {
			addCriterion("job_class >=", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassLessThan(String value) {
			addCriterion("job_class <", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassLessThanOrEqualTo(String value) {
			addCriterion("job_class <=", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassLike(String value) {
			addCriterion("job_class like", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassNotLike(String value) {
			addCriterion("job_class not like", value, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassIn(List<String> values) {
			addCriterion("job_class in", values, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassNotIn(List<String> values) {
			addCriterion("job_class not in", values, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassBetween(String value1, String value2) {
			addCriterion("job_class between", value1, value2, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobClassNotBetween(String value1, String value2) {
			addCriterion("job_class not between", value1, value2, "jobClass");
			return (Criteria) this;
		}

		public Criteria andJobMethodIsNull() {
			addCriterion("job_method is null");
			return (Criteria) this;
		}

		public Criteria andJobMethodIsNotNull() {
			addCriterion("job_method is not null");
			return (Criteria) this;
		}

		public Criteria andJobMethodEqualTo(String value) {
			addCriterion("job_method =", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodNotEqualTo(String value) {
			addCriterion("job_method <>", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodGreaterThan(String value) {
			addCriterion("job_method >", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodGreaterThanOrEqualTo(String value) {
			addCriterion("job_method >=", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodLessThan(String value) {
			addCriterion("job_method <", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodLessThanOrEqualTo(String value) {
			addCriterion("job_method <=", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodLike(String value) {
			addCriterion("job_method like", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodNotLike(String value) {
			addCriterion("job_method not like", value, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodIn(List<String> values) {
			addCriterion("job_method in", values, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodNotIn(List<String> values) {
			addCriterion("job_method not in", values, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodBetween(String value1, String value2) {
			addCriterion("job_method between", value1, value2, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobMethodNotBetween(String value1, String value2) {
			addCriterion("job_method not between", value1, value2, "jobMethod");
			return (Criteria) this;
		}

		public Criteria andJobCronIsNull() {
			addCriterion("job_cron is null");
			return (Criteria) this;
		}

		public Criteria andJobCronIsNotNull() {
			addCriterion("job_cron is not null");
			return (Criteria) this;
		}

		public Criteria andJobCronEqualTo(String value) {
			addCriterion("job_cron =", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronNotEqualTo(String value) {
			addCriterion("job_cron <>", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronGreaterThan(String value) {
			addCriterion("job_cron >", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronGreaterThanOrEqualTo(String value) {
			addCriterion("job_cron >=", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronLessThan(String value) {
			addCriterion("job_cron <", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronLessThanOrEqualTo(String value) {
			addCriterion("job_cron <=", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronLike(String value) {
			addCriterion("job_cron like", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronNotLike(String value) {
			addCriterion("job_cron not like", value, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronIn(List<String> values) {
			addCriterion("job_cron in", values, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronNotIn(List<String> values) {
			addCriterion("job_cron not in", values, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronBetween(String value1, String value2) {
			addCriterion("job_cron between", value1, value2, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobCronNotBetween(String value1, String value2) {
			addCriterion("job_cron not between", value1, value2, "jobCron");
			return (Criteria) this;
		}

		public Criteria andJobParamIsNull() {
			addCriterion("job_param is null");
			return (Criteria) this;
		}

		public Criteria andJobParamIsNotNull() {
			addCriterion("job_param is not null");
			return (Criteria) this;
		}

		public Criteria andJobParamEqualTo(String value) {
			addCriterion("job_param =", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamNotEqualTo(String value) {
			addCriterion("job_param <>", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamGreaterThan(String value) {
			addCriterion("job_param >", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamGreaterThanOrEqualTo(String value) {
			addCriterion("job_param >=", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamLessThan(String value) {
			addCriterion("job_param <", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamLessThanOrEqualTo(String value) {
			addCriterion("job_param <=", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamLike(String value) {
			addCriterion("job_param like", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamNotLike(String value) {
			addCriterion("job_param not like", value, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamIn(List<String> values) {
			addCriterion("job_param in", values, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamNotIn(List<String> values) {
			addCriterion("job_param not in", values, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamBetween(String value1, String value2) {
			addCriterion("job_param between", value1, value2, "jobParam");
			return (Criteria) this;
		}

		public Criteria andJobParamNotBetween(String value1, String value2) {
			addCriterion("job_param not between", value1, value2, "jobParam");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNull() {
			addCriterion("modify_by is null");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNotNull() {
			addCriterion("modify_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifyByEqualTo(String value) {
			addCriterion("modify_by =", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotEqualTo(String value) {
			addCriterion("modify_by <>", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThan(String value) {
			addCriterion("modify_by >", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThanOrEqualTo(String value) {
			addCriterion("modify_by >=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThan(String value) {
			addCriterion("modify_by <", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThanOrEqualTo(String value) {
			addCriterion("modify_by <=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLike(String value) {
			addCriterion("modify_by like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotLike(String value) {
			addCriterion("modify_by not like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIn(List<String> values) {
			addCriterion("modify_by in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotIn(List<String> values) {
			addCriterion("modify_by not in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByBetween(String value1, String value2) {
			addCriterion("modify_by between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotBetween(String value1, String value2) {
			addCriterion("modify_by not between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("status is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("status is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Byte value) {
			addCriterion("status =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Byte value) {
			addCriterion("status <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Byte value) {
			addCriterion("status >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("status >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Byte value) {
			addCriterion("status <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Byte value) {
			addCriterion("status <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Byte> values) {
			addCriterion("status in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Byte> values) {
			addCriterion("status not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Byte value1, Byte value2) {
			addCriterion("status between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("status not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}