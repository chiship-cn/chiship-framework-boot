package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.upms.biz.base.service.UpmsSystemCacheService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.annotation.Authorization;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.framework.base.BaseService;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.system.service.UpmsAppVersionService;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersion;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersionExample;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsAppVersionDto;
import cn.chiship.sdk.framework.util.FrameworkUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * App版本控制层 2021/9/30
 *
 * @author lijian
 */
@RestController
@RequestMapping("/appVersion")
@Api(tags = "App版本管理")
public class UpmsAppVersionController extends BaseController<UpmsAppVersion, UpmsAppVersionExample> {

	@Resource
	private UpmsAppVersionService upmsAppVersionService;

	@Resource
	private UpmsSystemCacheService upmsCacheService;

	@Resource
	RedisService redisService;

	@Override
	public BaseService getService() {
		return upmsAppVersionService;
	}

	@SystemOptionAnnotation(describe = "APP版本分页")
	@ApiOperation(value = "APP版本分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "name", value = "名称", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "appType", value = "类型", dataTypeClass = Byte.class, paramType = "query"), })
	@Authorization
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "name") String name,
			@RequestParam(required = false, value = "appType") Byte appType) {

		UpmsAppVersionExample upmsAppVersionExample = new UpmsAppVersionExample();
		UpmsAppVersionExample.Criteria criteria = upmsAppVersionExample.createCriteria()
				.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(name)) {
			criteria.andNameLike("%" + name + "%");
		}
		if (!StringUtil.isNull(appType)) {
			criteria.andAppTypeEqualTo(appType);
		}

		PageVo pageVo = super.page(upmsAppVersionExample);
		upmsCacheService.cacheApp();
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@SystemOptionAnnotation(describe = "根据APP编码获得列表")
	@ApiOperation(value = "根据APP编码获得列表")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "appCode", value = "名称", dataTypeClass = String.class, paramType = "query") })
	@GetMapping(value = "/listByCode")
	public ResponseEntity<BaseResult> listByCode(@RequestParam(value = "appCode") String appCode) {

		UpmsAppVersionExample upmsAppVersionExample = new UpmsAppVersionExample();
		upmsAppVersionExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO).andAppCodeEqualTo(appCode);
		upmsAppVersionExample.setOrderByClause(FrameworkUtil.formatSort("-appVersion"));
		List<UpmsAppVersion> appVersions = upmsAppVersionService.selectByExample(upmsAppVersionExample);
		return super.responseEntity(BaseResult.ok(appVersions));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存App版本信息")
	@ApiOperation(value = "保存App版本信息")
	@PostMapping(value = "/save")
	@Authorization
	public ResponseEntity<BaseResult> saveAppInfo(@RequestBody @Valid UpmsAppVersionDto upmsAppVersionDto) {
		UpmsAppVersion upmsAppVersion = new UpmsAppVersion();
		BeanUtils.copyProperties(upmsAppVersionDto, upmsAppVersion);
		return super.responseEntity(super.save(upmsAppVersion));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新App版本信息")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", required = true, dataTypeClass = String.class,
			paramType = "path") })
	@ApiOperation(value = "更新App版本信息")
	@PostMapping(value = "update/{id}")
	@Authorization
	public ResponseEntity<BaseResult> updateAppInfo(@PathVariable("id") String id,
			@RequestBody @Valid UpmsAppVersionDto upmsAppVersionDto) {
		UpmsAppVersion upmsAppVersion = new UpmsAppVersion();
		BeanUtils.copyProperties(upmsAppVersionDto, upmsAppVersion);
		return super.responseEntity(super.update(id, upmsAppVersion));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除App版本信息")
	@ApiOperation(value = "删除App版本信息")
	@PostMapping(value = "remove")
	@Authorization
	public ResponseEntity<BaseResult> removeAppInfo(@RequestBody @Valid List<String> ids) {
		UpmsAppVersionExample upmsAppVersionExample = new UpmsAppVersionExample();
		upmsAppVersionExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(upmsAppVersionExample));
	}

	@SystemOptionAnnotation(describe = "获取最新版本")
	@ApiOperation(value = "获取最新版本")
	@ApiImplicitParams({ @ApiImplicitParam(name = "appCode", value = "编号", required = true,
			dataTypeClass = String.class, paramType = "path") })
	@GetMapping(value = "/getNew/{appCode}")
	public ResponseEntity<BaseResult> getNew(@PathVariable("appCode") String appCode) {
		Object o = redisService.hget(CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_APP_VERSION_PREFIX),
				appCode);
		if (StringUtil.isNull(o)) {
			return super.responseEntity(BaseResult.ok());
		}
		else {
			return super.responseEntity(BaseResult.ok(o));
		}
	}

}
