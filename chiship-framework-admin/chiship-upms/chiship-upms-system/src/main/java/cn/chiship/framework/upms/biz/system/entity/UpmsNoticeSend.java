package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-10
 */
public class UpmsNoticeSend implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 通告ID
	 */
	private String noticeId;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 阅读状态（0未读，1已读）
	 */
	private Byte readFlag;

	/**
	 * 阅读时间
	 */
	private Long readTime;

	/**
	 * 消息类型1:通知公告2:系统消息
	 */
	private Byte noticeType;

	/**
	 * 用户类型（admin:管理人员，member:会员）
	 */
	private String noticeUserType;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getNoticeId() {
		return noticeId;
	}

	public void setNoticeId(String noticeId) {
		this.noticeId = noticeId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Byte getReadFlag() {
		return readFlag;
	}

	public void setReadFlag(Byte readFlag) {
		this.readFlag = readFlag;
	}

	public Long getReadTime() {
		return readTime;
	}

	public void setReadTime(Long readTime) {
		this.readTime = readTime;
	}

	public Byte getNoticeType() {
		return noticeType;
	}

	public void setNoticeType(Byte noticeType) {
		this.noticeType = noticeType;
	}

	public String getNoticeUserType() {
		return noticeUserType;
	}

	public void setNoticeUserType(String noticeUserType) {
		this.noticeUserType = noticeUserType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", noticeId=").append(noticeId);
		sb.append(", userId=").append(userId);
		sb.append(", readFlag=").append(readFlag);
		sb.append(", readTime=").append(readTime);
		sb.append(", noticeType=").append(noticeType);
		sb.append(", noticeUserType=").append(noticeUserType);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsNoticeSend other = (UpmsNoticeSend) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getNoticeId() == null ? other.getNoticeId() == null
						: this.getNoticeId().equals(other.getNoticeId()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getReadFlag() == null ? other.getReadFlag() == null
						: this.getReadFlag().equals(other.getReadFlag()))
				&& (this.getReadTime() == null ? other.getReadTime() == null
						: this.getReadTime().equals(other.getReadTime()))
				&& (this.getNoticeType() == null ? other.getNoticeType() == null
						: this.getNoticeType().equals(other.getNoticeType()))
				&& (this.getNoticeUserType() == null ? other.getNoticeUserType() == null
						: this.getNoticeUserType().equals(other.getNoticeUserType()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getNoticeId() == null) ? 0 : getNoticeId().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getReadFlag() == null) ? 0 : getReadFlag().hashCode());
		result = prime * result + ((getReadTime() == null) ? 0 : getReadTime().hashCode());
		result = prime * result + ((getNoticeType() == null) ? 0 : getNoticeType().hashCode());
		result = prime * result + ((getNoticeUserType() == null) ? 0 : getNoticeUserType().hashCode());
		return result;
	}

}