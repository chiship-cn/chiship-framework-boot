package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;

/**
 * 意见反馈回复表单 2022/5/16
 *
 * @author LiJian
 */
@ApiModel(value = "意见反馈回复表单")
public class UpmsFeedbackReplyDto {

	@ApiModelProperty(value = "主键", required = true)
	@NotEmpty(message = "主键" + BaseTipConstants.NOT_EMPTY)
	private String id;

	@ApiModelProperty(value = "回复内容", required = true)
	@NotEmpty(message = "回复内容" + BaseTipConstants.NOT_EMPTY)
	private String replyContent;

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
