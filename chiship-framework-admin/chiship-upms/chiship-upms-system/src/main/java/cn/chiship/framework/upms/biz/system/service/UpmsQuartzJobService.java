package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJob;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobExample;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;

/**
 * 定时任务业务接口层 2021/12/2
 *
 * @author lijian
 */
public interface UpmsQuartzJobService extends BaseService<UpmsQuartzJob, UpmsQuartzJobExample> {

    /**
     * 获取系统中所有任务
     * @return
     */
    BaseResult listTasks();
}
