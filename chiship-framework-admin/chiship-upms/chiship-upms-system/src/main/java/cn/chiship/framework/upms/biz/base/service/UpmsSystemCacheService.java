package cn.chiship.framework.upms.biz.base.service;

/**
 * 缓存数据
 *
 * @author lijian
 */
public interface UpmsSystemCacheService {

	/**
	 * 缓存参数配置
	 */
	void cacheConfigs();

	/**
	 * 缓存密钥
	 */
	void cacheProofs();

	/**
	 * 缓存数据字典组
	 */
	void cacheDataDict();

	/**
	 * 缓存地区
	 */
	void cacheRegions();

	/**
	 * 启动定时任务
	 */
	void initQuartzJob();

	/**
	 * 缓存APP
	 */
	void cacheApp();

	/**
	 * 分类字典
	 */
	void cacheCategoryDict();

}
