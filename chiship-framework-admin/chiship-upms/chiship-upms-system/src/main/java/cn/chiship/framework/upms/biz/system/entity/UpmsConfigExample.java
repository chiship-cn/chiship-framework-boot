package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2023-08-28
 */
public class UpmsConfigExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsConfigExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andPidIsNull() {
			addCriterion("pid is null");
			return (Criteria) this;
		}

		public Criteria andPidIsNotNull() {
			addCriterion("pid is not null");
			return (Criteria) this;
		}

		public Criteria andPidEqualTo(String value) {
			addCriterion("pid =", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotEqualTo(String value) {
			addCriterion("pid <>", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThan(String value) {
			addCriterion("pid >", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidGreaterThanOrEqualTo(String value) {
			addCriterion("pid >=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThan(String value) {
			addCriterion("pid <", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLessThanOrEqualTo(String value) {
			addCriterion("pid <=", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidLike(String value) {
			addCriterion("pid like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotLike(String value) {
			addCriterion("pid not like", value, "pid");
			return (Criteria) this;
		}

		public Criteria andPidIn(List<String> values) {
			addCriterion("pid in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotIn(List<String> values) {
			addCriterion("pid not in", values, "pid");
			return (Criteria) this;
		}

		public Criteria andPidBetween(String value1, String value2) {
			addCriterion("pid between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andPidNotBetween(String value1, String value2) {
			addCriterion("pid not between", value1, value2, "pid");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andParamCodeIsNull() {
			addCriterion("param_code is null");
			return (Criteria) this;
		}

		public Criteria andParamCodeIsNotNull() {
			addCriterion("param_code is not null");
			return (Criteria) this;
		}

		public Criteria andParamCodeEqualTo(String value) {
			addCriterion("param_code =", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotEqualTo(String value) {
			addCriterion("param_code <>", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeGreaterThan(String value) {
			addCriterion("param_code >", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeGreaterThanOrEqualTo(String value) {
			addCriterion("param_code >=", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLessThan(String value) {
			addCriterion("param_code <", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLessThanOrEqualTo(String value) {
			addCriterion("param_code <=", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeLike(String value) {
			addCriterion("param_code like", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotLike(String value) {
			addCriterion("param_code not like", value, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeIn(List<String> values) {
			addCriterion("param_code in", values, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotIn(List<String> values) {
			addCriterion("param_code not in", values, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeBetween(String value1, String value2) {
			addCriterion("param_code between", value1, value2, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamCodeNotBetween(String value1, String value2) {
			addCriterion("param_code not between", value1, value2, "paramCode");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNull() {
			addCriterion("param_name is null");
			return (Criteria) this;
		}

		public Criteria andParamNameIsNotNull() {
			addCriterion("param_name is not null");
			return (Criteria) this;
		}

		public Criteria andParamNameEqualTo(String value) {
			addCriterion("param_name =", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotEqualTo(String value) {
			addCriterion("param_name <>", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThan(String value) {
			addCriterion("param_name >", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameGreaterThanOrEqualTo(String value) {
			addCriterion("param_name >=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThan(String value) {
			addCriterion("param_name <", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLessThanOrEqualTo(String value) {
			addCriterion("param_name <=", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameLike(String value) {
			addCriterion("param_name like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotLike(String value) {
			addCriterion("param_name not like", value, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameIn(List<String> values) {
			addCriterion("param_name in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotIn(List<String> values) {
			addCriterion("param_name not in", values, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameBetween(String value1, String value2) {
			addCriterion("param_name between", value1, value2, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamNameNotBetween(String value1, String value2) {
			addCriterion("param_name not between", value1, value2, "paramName");
			return (Criteria) this;
		}

		public Criteria andParamValueIsNull() {
			addCriterion("param_value is null");
			return (Criteria) this;
		}

		public Criteria andParamValueIsNotNull() {
			addCriterion("param_value is not null");
			return (Criteria) this;
		}

		public Criteria andParamValueEqualTo(String value) {
			addCriterion("param_value =", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueNotEqualTo(String value) {
			addCriterion("param_value <>", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueGreaterThan(String value) {
			addCriterion("param_value >", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueGreaterThanOrEqualTo(String value) {
			addCriterion("param_value >=", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueLessThan(String value) {
			addCriterion("param_value <", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueLessThanOrEqualTo(String value) {
			addCriterion("param_value <=", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueLike(String value) {
			addCriterion("param_value like", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueNotLike(String value) {
			addCriterion("param_value not like", value, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueIn(List<String> values) {
			addCriterion("param_value in", values, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueNotIn(List<String> values) {
			addCriterion("param_value not in", values, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueBetween(String value1, String value2) {
			addCriterion("param_value between", value1, value2, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamValueNotBetween(String value1, String value2) {
			addCriterion("param_value not between", value1, value2, "paramValue");
			return (Criteria) this;
		}

		public Criteria andParamDescIsNull() {
			addCriterion("param_desc is null");
			return (Criteria) this;
		}

		public Criteria andParamDescIsNotNull() {
			addCriterion("param_desc is not null");
			return (Criteria) this;
		}

		public Criteria andParamDescEqualTo(String value) {
			addCriterion("param_desc =", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescNotEqualTo(String value) {
			addCriterion("param_desc <>", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescGreaterThan(String value) {
			addCriterion("param_desc >", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescGreaterThanOrEqualTo(String value) {
			addCriterion("param_desc >=", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescLessThan(String value) {
			addCriterion("param_desc <", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescLessThanOrEqualTo(String value) {
			addCriterion("param_desc <=", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescLike(String value) {
			addCriterion("param_desc like", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescNotLike(String value) {
			addCriterion("param_desc not like", value, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescIn(List<String> values) {
			addCriterion("param_desc in", values, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescNotIn(List<String> values) {
			addCriterion("param_desc not in", values, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescBetween(String value1, String value2) {
			addCriterion("param_desc between", value1, value2, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andParamDescNotBetween(String value1, String value2) {
			addCriterion("param_desc not between", value1, value2, "paramDesc");
			return (Criteria) this;
		}

		public Criteria andFormTypeIsNull() {
			addCriterion("form_type is null");
			return (Criteria) this;
		}

		public Criteria andFormTypeIsNotNull() {
			addCriterion("form_type is not null");
			return (Criteria) this;
		}

		public Criteria andFormTypeEqualTo(String value) {
			addCriterion("form_type =", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeNotEqualTo(String value) {
			addCriterion("form_type <>", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeGreaterThan(String value) {
			addCriterion("form_type >", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeGreaterThanOrEqualTo(String value) {
			addCriterion("form_type >=", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeLessThan(String value) {
			addCriterion("form_type <", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeLessThanOrEqualTo(String value) {
			addCriterion("form_type <=", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeLike(String value) {
			addCriterion("form_type like", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeNotLike(String value) {
			addCriterion("form_type not like", value, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeIn(List<String> values) {
			addCriterion("form_type in", values, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeNotIn(List<String> values) {
			addCriterion("form_type not in", values, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeBetween(String value1, String value2) {
			addCriterion("form_type between", value1, value2, "formType");
			return (Criteria) this;
		}

		public Criteria andFormTypeNotBetween(String value1, String value2) {
			addCriterion("form_type not between", value1, value2, "formType");
			return (Criteria) this;
		}

		public Criteria andFormValueIsNull() {
			addCriterion("form_value is null");
			return (Criteria) this;
		}

		public Criteria andFormValueIsNotNull() {
			addCriterion("form_value is not null");
			return (Criteria) this;
		}

		public Criteria andFormValueEqualTo(String value) {
			addCriterion("form_value =", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueNotEqualTo(String value) {
			addCriterion("form_value <>", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueGreaterThan(String value) {
			addCriterion("form_value >", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueGreaterThanOrEqualTo(String value) {
			addCriterion("form_value >=", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueLessThan(String value) {
			addCriterion("form_value <", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueLessThanOrEqualTo(String value) {
			addCriterion("form_value <=", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueLike(String value) {
			addCriterion("form_value like", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueNotLike(String value) {
			addCriterion("form_value not like", value, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueIn(List<String> values) {
			addCriterion("form_value in", values, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueNotIn(List<String> values) {
			addCriterion("form_value not in", values, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueBetween(String value1, String value2) {
			addCriterion("form_value between", value1, value2, "formValue");
			return (Criteria) this;
		}

		public Criteria andFormValueNotBetween(String value1, String value2) {
			addCriterion("form_value not between", value1, value2, "formValue");
			return (Criteria) this;
		}

		public Criteria andParamExtIsNull() {
			addCriterion("param_ext is null");
			return (Criteria) this;
		}

		public Criteria andParamExtIsNotNull() {
			addCriterion("param_ext is not null");
			return (Criteria) this;
		}

		public Criteria andParamExtEqualTo(String value) {
			addCriterion("param_ext =", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtNotEqualTo(String value) {
			addCriterion("param_ext <>", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtGreaterThan(String value) {
			addCriterion("param_ext >", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtGreaterThanOrEqualTo(String value) {
			addCriterion("param_ext >=", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtLessThan(String value) {
			addCriterion("param_ext <", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtLessThanOrEqualTo(String value) {
			addCriterion("param_ext <=", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtLike(String value) {
			addCriterion("param_ext like", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtNotLike(String value) {
			addCriterion("param_ext not like", value, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtIn(List<String> values) {
			addCriterion("param_ext in", values, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtNotIn(List<String> values) {
			addCriterion("param_ext not in", values, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtBetween(String value1, String value2) {
			addCriterion("param_ext between", value1, value2, "paramExt");
			return (Criteria) this;
		}

		public Criteria andParamExtNotBetween(String value1, String value2) {
			addCriterion("param_ext not between", value1, value2, "paramExt");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdIsNull() {
			addCriterion("organization_id is null");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdIsNotNull() {
			addCriterion("organization_id is not null");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdEqualTo(String value) {
			addCriterion("organization_id =", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdNotEqualTo(String value) {
			addCriterion("organization_id <>", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdGreaterThan(String value) {
			addCriterion("organization_id >", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdGreaterThanOrEqualTo(String value) {
			addCriterion("organization_id >=", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdLessThan(String value) {
			addCriterion("organization_id <", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdLessThanOrEqualTo(String value) {
			addCriterion("organization_id <=", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdLike(String value) {
			addCriterion("organization_id like", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdNotLike(String value) {
			addCriterion("organization_id not like", value, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdIn(List<String> values) {
			addCriterion("organization_id in", values, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdNotIn(List<String> values) {
			addCriterion("organization_id not in", values, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdBetween(String value1, String value2) {
			addCriterion("organization_id between", value1, value2, "organizationId");
			return (Criteria) this;
		}

		public Criteria andOrganizationIdNotBetween(String value1, String value2) {
			addCriterion("organization_id not between", value1, value2, "organizationId");
			return (Criteria) this;
		}

		public Criteria andIsTemplateIsNull() {
			addCriterion("is_template is null");
			return (Criteria) this;
		}

		public Criteria andIsTemplateIsNotNull() {
			addCriterion("is_template is not null");
			return (Criteria) this;
		}

		public Criteria andIsTemplateEqualTo(Byte value) {
			addCriterion("is_template =", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateNotEqualTo(Byte value) {
			addCriterion("is_template <>", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateGreaterThan(Byte value) {
			addCriterion("is_template >", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_template >=", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateLessThan(Byte value) {
			addCriterion("is_template <", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateLessThanOrEqualTo(Byte value) {
			addCriterion("is_template <=", value, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateIn(List<Byte> values) {
			addCriterion("is_template in", values, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateNotIn(List<Byte> values) {
			addCriterion("is_template not in", values, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateBetween(Byte value1, Byte value2) {
			addCriterion("is_template between", value1, value2, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andIsTemplateNotBetween(Byte value1, Byte value2) {
			addCriterion("is_template not between", value1, value2, "isTemplate");
			return (Criteria) this;
		}

		public Criteria andOrdersIsNull() {
			addCriterion("orders is null");
			return (Criteria) this;
		}

		public Criteria andOrdersIsNotNull() {
			addCriterion("orders is not null");
			return (Criteria) this;
		}

		public Criteria andOrdersEqualTo(Long value) {
			addCriterion("orders =", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotEqualTo(Long value) {
			addCriterion("orders <>", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersGreaterThan(Long value) {
			addCriterion("orders >", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersGreaterThanOrEqualTo(Long value) {
			addCriterion("orders >=", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersLessThan(Long value) {
			addCriterion("orders <", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersLessThanOrEqualTo(Long value) {
			addCriterion("orders <=", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersIn(List<Long> values) {
			addCriterion("orders in", values, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotIn(List<Long> values) {
			addCriterion("orders not in", values, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersBetween(Long value1, Long value2) {
			addCriterion("orders between", value1, value2, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotBetween(Long value1, Long value2) {
			addCriterion("orders not between", value1, value2, "orders");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}