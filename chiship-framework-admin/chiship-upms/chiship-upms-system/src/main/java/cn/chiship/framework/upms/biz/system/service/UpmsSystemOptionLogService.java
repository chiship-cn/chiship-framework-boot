package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLogExample;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;

/**
 * 系统操作日志业务接口层 2021/9/30
 *
 * @author lijian
 */
public interface UpmsSystemOptionLogService extends BaseService<UpmsSystemOptionLog, UpmsSystemOptionLogExample> {

	/**
	 * 根据ID获取详情
	 * @param id
	 * @return
	 */
	BaseResult getById(String id);

}
