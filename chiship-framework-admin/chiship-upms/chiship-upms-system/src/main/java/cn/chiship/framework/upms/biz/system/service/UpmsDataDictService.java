package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.entity.UpmsDataDictExample;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsDataDict;
import cn.chiship.sdk.framework.pojo.vo.DataDictItemVo;

import java.util.List;

/**
 * UpmsDataDictService接口 2021/9/27
 *
 * @author lijian
 */
public interface UpmsDataDictService extends BaseService<UpmsDataDict, UpmsDataDictExample> {

	/**
	 * 根据字典组编号获取数据字典数据
	 * @param dictCode
	 * @return
	 */
	List<DataDictItemVo> findByCodeFromCache(String dictCode);

	/**
	 * 根据字段组编号及字典编号获取描述
	 * @param dictCode
	 * @param dictItemCode
	 * @return
	 */
	String getDictItemNameFromCache(String dictCode, String dictItemCode);

}
