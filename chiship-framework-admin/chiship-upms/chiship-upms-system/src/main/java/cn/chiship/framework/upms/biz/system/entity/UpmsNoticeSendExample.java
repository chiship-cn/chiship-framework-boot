package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-10
 */
public class UpmsNoticeSendExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsNoticeSendExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andNoticeIdIsNull() {
			addCriterion("notice_id is null");
			return (Criteria) this;
		}

		public Criteria andNoticeIdIsNotNull() {
			addCriterion("notice_id is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeIdEqualTo(String value) {
			addCriterion("notice_id =", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdNotEqualTo(String value) {
			addCriterion("notice_id <>", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdGreaterThan(String value) {
			addCriterion("notice_id >", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdGreaterThanOrEqualTo(String value) {
			addCriterion("notice_id >=", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdLessThan(String value) {
			addCriterion("notice_id <", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdLessThanOrEqualTo(String value) {
			addCriterion("notice_id <=", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdLike(String value) {
			addCriterion("notice_id like", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdNotLike(String value) {
			addCriterion("notice_id not like", value, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdIn(List<String> values) {
			addCriterion("notice_id in", values, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdNotIn(List<String> values) {
			addCriterion("notice_id not in", values, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdBetween(String value1, String value2) {
			addCriterion("notice_id between", value1, value2, "noticeId");
			return (Criteria) this;
		}

		public Criteria andNoticeIdNotBetween(String value1, String value2) {
			addCriterion("notice_id not between", value1, value2, "noticeId");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andReadFlagIsNull() {
			addCriterion("read_flag is null");
			return (Criteria) this;
		}

		public Criteria andReadFlagIsNotNull() {
			addCriterion("read_flag is not null");
			return (Criteria) this;
		}

		public Criteria andReadFlagEqualTo(Byte value) {
			addCriterion("read_flag =", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagNotEqualTo(Byte value) {
			addCriterion("read_flag <>", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagGreaterThan(Byte value) {
			addCriterion("read_flag >", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagGreaterThanOrEqualTo(Byte value) {
			addCriterion("read_flag >=", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagLessThan(Byte value) {
			addCriterion("read_flag <", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagLessThanOrEqualTo(Byte value) {
			addCriterion("read_flag <=", value, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagIn(List<Byte> values) {
			addCriterion("read_flag in", values, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagNotIn(List<Byte> values) {
			addCriterion("read_flag not in", values, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagBetween(Byte value1, Byte value2) {
			addCriterion("read_flag between", value1, value2, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadFlagNotBetween(Byte value1, Byte value2) {
			addCriterion("read_flag not between", value1, value2, "readFlag");
			return (Criteria) this;
		}

		public Criteria andReadTimeIsNull() {
			addCriterion("read_time is null");
			return (Criteria) this;
		}

		public Criteria andReadTimeIsNotNull() {
			addCriterion("read_time is not null");
			return (Criteria) this;
		}

		public Criteria andReadTimeEqualTo(Long value) {
			addCriterion("read_time =", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeNotEqualTo(Long value) {
			addCriterion("read_time <>", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeGreaterThan(Long value) {
			addCriterion("read_time >", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("read_time >=", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeLessThan(Long value) {
			addCriterion("read_time <", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeLessThanOrEqualTo(Long value) {
			addCriterion("read_time <=", value, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeIn(List<Long> values) {
			addCriterion("read_time in", values, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeNotIn(List<Long> values) {
			addCriterion("read_time not in", values, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeBetween(Long value1, Long value2) {
			addCriterion("read_time between", value1, value2, "readTime");
			return (Criteria) this;
		}

		public Criteria andReadTimeNotBetween(Long value1, Long value2) {
			addCriterion("read_time not between", value1, value2, "readTime");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIsNull() {
			addCriterion("notice_type is null");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIsNotNull() {
			addCriterion("notice_type is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeEqualTo(Byte value) {
			addCriterion("notice_type =", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotEqualTo(Byte value) {
			addCriterion("notice_type <>", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeGreaterThan(Byte value) {
			addCriterion("notice_type >", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("notice_type >=", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeLessThan(Byte value) {
			addCriterion("notice_type <", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeLessThanOrEqualTo(Byte value) {
			addCriterion("notice_type <=", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIn(List<Byte> values) {
			addCriterion("notice_type in", values, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotIn(List<Byte> values) {
			addCriterion("notice_type not in", values, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeBetween(Byte value1, Byte value2) {
			addCriterion("notice_type between", value1, value2, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("notice_type not between", value1, value2, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIsNull() {
			addCriterion("notice_user_type is null");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIsNotNull() {
			addCriterion("notice_user_type is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeEqualTo(String value) {
			addCriterion("notice_user_type =", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotEqualTo(String value) {
			addCriterion("notice_user_type <>", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeGreaterThan(String value) {
			addCriterion("notice_user_type >", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeGreaterThanOrEqualTo(String value) {
			addCriterion("notice_user_type >=", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLessThan(String value) {
			addCriterion("notice_user_type <", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLessThanOrEqualTo(String value) {
			addCriterion("notice_user_type <=", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLike(String value) {
			addCriterion("notice_user_type like", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotLike(String value) {
			addCriterion("notice_user_type not like", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIn(List<String> values) {
			addCriterion("notice_user_type in", values, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotIn(List<String> values) {
			addCriterion("notice_user_type not in", values, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeBetween(String value1, String value2) {
			addCriterion("notice_user_type between", value1, value2, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotBetween(String value1, String value2) {
			addCriterion("notice_user_type not between", value1, value2, "noticeUserType");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}