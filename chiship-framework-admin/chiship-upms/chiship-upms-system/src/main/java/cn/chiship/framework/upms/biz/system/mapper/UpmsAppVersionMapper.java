package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersion;
import cn.chiship.framework.upms.biz.system.entity.UpmsAppVersionExample;

import java.util.List;

/**
 * APP Mapper
 *
 * @author lj
 */
public interface UpmsAppVersionMapper extends BaseMapper<UpmsAppVersion, UpmsAppVersionExample> {

	/**
	 * 获得所有得Code
	 * @return
	 */
	List<String> listAppCode();

}