package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-11
 */
public class UpmsFeedback implements Serializable {

	/**
	 * 公告ID
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 创建者
	 */
	private String createdBy;

	/**
	 * 创建者用户ID
	 */
	private String createdUserId;

	/**
	 * 用户类型（admin:管理人员，member:会员）
	 */
	private String userType;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 分类
	 */
	private String category;

	/**
	 * 内容
	 */
	private String content;

	/**
	 * 图片
	 */
	private String images;

	/**
	 * 视频
	 */
	private String videos;

	/**
	 * 是否回复
	 */
	private Byte isReply;

	/**
	 * 回复人
	 */
	private String replyPeople;

	/**
	 * 回复信息
	 */
	private String replyContent;

	/**
	 * 回复时间
	 */
	private Long replyTime;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getImages() {
		return images;
	}

	public void setImages(String images) {
		this.images = images;
	}

	public String getVideos() {
		return videos;
	}

	public void setVideos(String videos) {
		this.videos = videos;
	}

	public Byte getIsReply() {
		return isReply;
	}

	public void setIsReply(Byte isReply) {
		this.isReply = isReply;
	}

	public String getReplyPeople() {
		return replyPeople;
	}

	public void setReplyPeople(String replyPeople) {
		this.replyPeople = replyPeople;
	}

	public String getReplyContent() {
		return replyContent;
	}

	public void setReplyContent(String replyContent) {
		this.replyContent = replyContent;
	}

	public Long getReplyTime() {
		return replyTime;
	}

	public void setReplyTime(Long replyTime) {
		this.replyTime = replyTime;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", createdUserId=").append(createdUserId);
		sb.append(", userType=").append(userType);
		sb.append(", title=").append(title);
		sb.append(", category=").append(category);
		sb.append(", content=").append(content);
		sb.append(", images=").append(images);
		sb.append(", videos=").append(videos);
		sb.append(", isReply=").append(isReply);
		sb.append(", replyPeople=").append(replyPeople);
		sb.append(", replyContent=").append(replyContent);
		sb.append(", replyTime=").append(replyTime);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsFeedback other = (UpmsFeedback) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getCreatedUserId() == null ? other.getCreatedUserId() == null
						: this.getCreatedUserId().equals(other.getCreatedUserId()))
				&& (this.getUserType() == null ? other.getUserType() == null
						: this.getUserType().equals(other.getUserType()))
				&& (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
				&& (this.getCategory() == null ? other.getCategory() == null
						: this.getCategory().equals(other.getCategory()))
				&& (this.getContent() == null ? other.getContent() == null
						: this.getContent().equals(other.getContent()))
				&& (this.getImages() == null ? other.getImages() == null : this.getImages().equals(other.getImages()))
				&& (this.getVideos() == null ? other.getVideos() == null : this.getVideos().equals(other.getVideos()))
				&& (this.getIsReply() == null ? other.getIsReply() == null
						: this.getIsReply().equals(other.getIsReply()))
				&& (this.getReplyPeople() == null ? other.getReplyPeople() == null
						: this.getReplyPeople().equals(other.getReplyPeople()))
				&& (this.getReplyContent() == null ? other.getReplyContent() == null
						: this.getReplyContent().equals(other.getReplyContent()))
				&& (this.getReplyTime() == null ? other.getReplyTime() == null
						: this.getReplyTime().equals(other.getReplyTime()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
		result = prime * result + ((getUserType() == null) ? 0 : getUserType().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + ((getCategory() == null) ? 0 : getCategory().hashCode());
		result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
		result = prime * result + ((getImages() == null) ? 0 : getImages().hashCode());
		result = prime * result + ((getVideos() == null) ? 0 : getVideos().hashCode());
		result = prime * result + ((getIsReply() == null) ? 0 : getIsReply().hashCode());
		result = prime * result + ((getReplyPeople() == null) ? 0 : getReplyPeople().hashCode());
		result = prime * result + ((getReplyContent() == null) ? 0 : getReplyContent().hashCode());
		result = prime * result + ((getReplyTime() == null) ? 0 : getReplyTime().hashCode());
		return result;
	}

}