package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsDataDictExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsDataDict;

/**
 * @author lj 数据字典Mapper
 */
public interface UpmsDataDictMapper extends BaseMapper<UpmsDataDict, UpmsDataDictExample> {

}