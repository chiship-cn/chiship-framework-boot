package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLogExample;

import java.util.List;

import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lijian
 * @date 2025-02-27
 */
public interface UpmsSmsLogMapper extends BaseMapper<UpmsSmsLog, UpmsSmsLogExample> {

}