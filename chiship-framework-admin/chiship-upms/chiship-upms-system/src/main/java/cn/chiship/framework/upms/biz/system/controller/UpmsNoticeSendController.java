package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.system.service.UpmsNoticeSendService;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSend;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSendExample;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import com.google.common.collect.Maps;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

/**
 * 用户通告阅读标记表控制层 2022/2/4
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/upmsNoticeSend")
@Api(tags = "用户通告阅读标记表")
public class UpmsNoticeSendController extends BaseController<UpmsNoticeSend, UpmsNoticeSendExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsNoticeSendController.class);

    @Resource
    private UpmsNoticeSendService upmsNoticeSendService;

    @Override
    public BaseService getService() {
        return upmsNoticeSendService;
    }

    @ApiOperation(value = "当前登录用户通告阅读标记表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true, dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "noticeTitle", value = "标题", defaultValue = "", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "noticeType", value = "1 通知公告 2 系统消息 ", defaultValue = "1", dataTypeClass = Byte.class, paramType = "query")

    })
    @GetMapping(value = "/currentUserPage")
    public ResponseEntity<BaseResult> currentUserPage(
            @RequestParam(required = false, defaultValue = "1", value = "page") Long page,
            @RequestParam(required = false, defaultValue = "10", value = "limit") Long limit,
            @RequestParam(required = false, defaultValue = "", value = "noticeTitle") String noticeTitle,
            @RequestParam(value = "noticeType") Byte noticeType) {
        HashMap<String, Object> map = Maps.newHashMapWithExpectedSize(7);
        map.put("page", page);
        map.put("limit", limit);
        map.put("msgType", noticeType);
        map.put("userId", getUserId());
        map.put("noticeTitle", noticeTitle);
        BaseResult baseResult = upmsNoticeSendService.pageProfitDetails(map);
        return super.responseEntity(baseResult);
    }

    @ApiOperation(value = "获取当前登录用户未读消息(前6条)")
    @GetMapping(value = "/currentUserUnRead")
    public ResponseEntity<BaseResult> currentUserUnRead() {
        HashMap<String, Object> msgMap = Maps.newHashMapWithExpectedSize(7);
        HashMap<String, Object> map = Maps.newHashMapWithExpectedSize(7);
        map.put("userId", getUserId());
        map.put("page", 1L);
        map.put("limit", 6L);
        map.put("msgType", 1);
        map.put("readFlag", 0);
        BaseResult baseResult = upmsNoticeSendService.pageProfitDetails(map);
        PageVo pageVo = (PageVo) baseResult.getData();
        msgMap.put("notice", pageVo.getRecords());

        map.put("msgType", 2);
        baseResult = upmsNoticeSendService.pageProfitDetails(map);
        pageVo = (PageVo) baseResult.getData();
        msgMap.put("system", pageVo.getRecords());
        return super.responseEntity(BaseResult.ok(msgMap));
    }

    @ApiOperation(value = "设置当前登录用户消息全部已读")
    @PostMapping(value = "/currentUserSetRead")
    public ResponseEntity<BaseResult> currentUserSetRead(@RequestBody Byte noticeType) {
        return super.responseEntity(upmsNoticeSendService.currentUserSetRead(noticeType, getUserId()));
    }

    @ApiOperation(value = "设置当前登录用户消息全部已读")
    @PostMapping(value = "/setAsRead")
    public ResponseEntity<BaseResult> setAsRead(@RequestBody String id) {
        return super.responseEntity(upmsNoticeSendService.setAsRead(id, getUserId()));
    }

}
