package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "字典组表单")
public class UpmsDataDictDto {

	@ApiModelProperty(value = "字典组代码")
	@NotNull(message = "字典组代码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 45)
	private String dataDictCode;

	@ApiModelProperty(value = "字典组名称")
	@NotNull(message = "字典组名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 45)
	private String dataDictName;

	@ApiModelProperty(value = "是否标准数据(0,1)", required = true)
	@NotNull(message = "是否标准数据" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isNationalStandard;

	@ApiModelProperty(value = "备注")
	@Length(max = 200)
	private String remark;

	public String getDataDictCode() {
		return dataDictCode;
	}

	public void setDataDictCode(String dataDictCode) {
		this.dataDictCode = dataDictCode;
	}

	public String getDataDictName() {
		return dataDictName;
	}

	public void setDataDictName(String dataDictName) {
		this.dataDictName = dataDictName;
	}

	public Byte getIsNationalStandard() {
		return isNationalStandard;
	}

	public void setIsNationalStandard(Byte isNationalStandard) {
		this.isNationalStandard = isNationalStandard;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
