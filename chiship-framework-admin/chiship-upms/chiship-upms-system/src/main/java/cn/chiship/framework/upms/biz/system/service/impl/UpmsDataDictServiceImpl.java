package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.upms.biz.system.entity.UpmsDataDictExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsDataDictMapper;
import cn.chiship.framework.upms.biz.system.service.UpmsDataDictService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.framework.upms.biz.system.entity.UpmsDataDict;

import javax.annotation.Resource;

import cn.chiship.sdk.framework.pojo.vo.DataDictItemVo;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * UpmsDataDictService实现 2021/9/27
 *
 * @author lijian
 */
@Service
public class UpmsDataDictServiceImpl extends BaseServiceImpl<UpmsDataDict, UpmsDataDictExample>
        implements UpmsDataDictService {

    @Resource
    UpmsDataDictMapper upmsDataDictMapper;

    @Resource
    RedisService redisService;

    @Override
    public List<DataDictItemVo> findByCodeFromCache(String dictCode) {
        String dataDictItemKey = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_DATA_DICT_ITEM_PREFIX);
        List<DataDictItemVo> dataDictItemVos = new ArrayList<>();

        List<UpmsDataDict> dataDictItems = (List<UpmsDataDict>) redisService.hget(dataDictItemKey, dictCode);
        if (!StringUtil.isNull(dataDictItems)) {
            dataDictItems.sort((o1, o2) -> {
                if(!StringUtil.isNull(o1.getSortValue()) && !StringUtil.isNull(o2.getSortValue())){
                    return o2.getSortValue().compareTo(o1.getSortValue());
                }else{
                    return 0;
                }
            });
            dataDictItems.forEach(upmsDataDictItem -> {
                DataDictItemVo dataDictItemVo = new DataDictItemVo();
                dataDictItemVo.setId(upmsDataDictItem.getId());
                dataDictItemVo.setDataDictId(upmsDataDictItem.getPid());
                dataDictItemVo.setDataDictItemCode(upmsDataDictItem.getDataDictCode());
                dataDictItemVo.setDataDictItemName(upmsDataDictItem.getDataDictName());
                dataDictItemVo.setColorValue(upmsDataDictItem.getColorValue());
                dataDictItemVos.add(dataDictItemVo);
            });
        }
        return dataDictItemVos;
    }

    @Override
    public String getDictItemNameFromCache(String dictCode, String dictItemCode) {
        if (StringUtil.isNullOrEmpty(dictItemCode)) {
            return "-";
        }
        List<DataDictItemVo> dataDictItemVos = findByCodeFromCache(dictCode);
        if (dataDictItemVos.isEmpty()) {
            return "-";
        }
        for (DataDictItemVo dataDictItemVo : dataDictItemVos) {
            if (dictItemCode.equals(dataDictItemVo.getDataDictItemCode())) {
                return dataDictItemVo.getDataDictItemName();
            }
        }
        return "-";
    }

    /**
     * 新增字典
     */
    @Override
    public BaseResult insertSelective(UpmsDataDict upmsDataDict) {
        UpmsDataDictExample dataDictExample = new UpmsDataDictExample();
        UpmsDataDictExample.Criteria criteria = dataDictExample.createCriteria();
        criteria.andDataDictNameEqualTo(upmsDataDict.getDataDictCode());
        criteria.andPidEqualTo(upmsDataDict.getPid());
        String tip = "字典组编码已存在，请重新输入";
        if (String.valueOf(BaseConstants.NO).equals(upmsDataDict.getPid())) {
            tip = "字典编码已存在，请重新输入";
        }
        if (upmsDataDictMapper.countByExample(dataDictExample) == 0) {
            return super.insertSelective(upmsDataDict);
        } else {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, tip);
        }
    }

    /**
     * 更新字典
     */
    @Override
    public BaseResult updateByPrimaryKeySelective(UpmsDataDict upmsDataDict) {
        UpmsDataDictExample dataDictExample = new UpmsDataDictExample();
        UpmsDataDictExample.Criteria criteria = dataDictExample.createCriteria();
        criteria.andDataDictNameEqualTo(upmsDataDict.getDataDictCode());
        criteria.andPidEqualTo(upmsDataDict.getPid());
        criteria.andIdNotEqualTo(upmsDataDict.getId());
        String tip = "字典组编码已存在，请重新输入";
        if (String.valueOf(BaseConstants.NO).equals(upmsDataDict.getPid())) {
            tip = "字典编码已存在，请重新输入";
        }
        UpmsDataDict dataDict = upmsDataDictMapper.selectByPrimaryKey(upmsDataDict.getId());
        dataDict.setDataDictCode(upmsDataDict.getDataDictCode());
        dataDict.setDataDictName(upmsDataDict.getDataDictName());
        dataDict.setIsNationalStandard(upmsDataDict.getIsNationalStandard());
        dataDict.setColorValue(upmsDataDict.getColorValue());
        dataDict.setSortValue(upmsDataDict.getSortValue());
        if (upmsDataDictMapper.countByExample(dataDictExample) == 0) {
            upmsDataDictMapper.updateByPrimaryKey(dataDict);
            return BaseResult.ok();
        } else {
            return BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_REPEAT, tip);
        }
    }

}
