package cn.chiship.framework.upms.biz.base.controller;

import cn.chiship.framework.upms.biz.base.service.CaptchaService;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * @author lijian
 */
@RestController
@RequestMapping("/captcha")
@Api(tags = "图形验证码")
public class CaptchaController {

	@Resource
	private CaptchaService captchaService;

	@ApiOperation(value = "获取验证码图片")
	@GetMapping("getCaptchaCode")
	@NoParamsSign
	public void getCaptchaCode(HttpServletResponse response) {
		captchaService.getCaptchaCodeImage(response);
	}

	@ApiOperation(value = "获取验证码文本")
	@GetMapping("getCaptchaCodeText")
	@NoParamsSign
	public ResponseEntity<BaseResult> getCaptchaCodeText() {
		return new ResponseEntity<>(captchaService.getCaptchaCodeText(), HttpStatus.OK);
	}

}
