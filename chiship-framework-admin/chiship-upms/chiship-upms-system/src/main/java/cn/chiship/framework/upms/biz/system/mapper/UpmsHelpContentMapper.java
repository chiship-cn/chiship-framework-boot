package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContent;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContentExample;

import java.util.HashMap;
import java.util.List;

import cn.chiship.framework.upms.biz.system.pojo.vo.ContentUnionAllCategoryVo;
import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lijian
 * @date 2025-02-09
 */
public interface UpmsHelpContentMapper extends BaseMapper<UpmsHelpContent, UpmsHelpContentExample> {

    List<UpmsHelpContent> selectByExampleWithBLOBs(UpmsHelpContentExample example);

    int updateByExampleWithBLOBs(@Param("record") UpmsHelpContent record, @Param("example") UpmsHelpContentExample example);

    int updateByPrimaryKeyWithBLOBs(UpmsHelpContent record);

    /**
     * 分类与文章结合查询
     *
     * @param map
     * @return
     */
    List<ContentUnionAllCategoryVo> selectContentUnionAllCategory(HashMap<String, Object> map);
}
