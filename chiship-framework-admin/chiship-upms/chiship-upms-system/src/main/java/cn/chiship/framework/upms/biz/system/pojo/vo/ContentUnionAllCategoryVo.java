package cn.chiship.framework.upms.biz.system.pojo.vo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author lijian
 */
@ApiModel(value = "帮助文档及分类视图")
public class ContentUnionAllCategoryVo implements Serializable {
    private String id;
    private String title;
    private String pid;
    private Boolean isContent;
    private Long orders;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public Boolean getContent() {
        return isContent;
    }

    public void setContent(Boolean content) {
        isContent = content;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }
}