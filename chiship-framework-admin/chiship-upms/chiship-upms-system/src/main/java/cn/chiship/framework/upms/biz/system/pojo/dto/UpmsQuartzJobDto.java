package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author lijian
 */
@ApiModel(value = "定时任务表单")
public class UpmsQuartzJobDto {

	@ApiModelProperty(value = "任务名称", required = true)
	@NotEmpty(message = "任务名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String jobTitle;

	@ApiModelProperty(value = "任务类名", required = true)
	@NotEmpty(message = "任务类名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String jobClass;

	@ApiModelProperty(value = "任务方法", required = true)
	@NotEmpty(message = "任务方法" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String jobMethod;

	@ApiModelProperty(value = "任务表达式", required = true)
	@NotEmpty(message = "任务表达式" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String jobCron;

	@ApiModelProperty(value = "任务参数", required = true)
	@Length(min = 1, max = 300)
	private String jobParam;

	@ApiModelProperty(value = "备注")
	private String remark;

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getJobClass() {
		return jobClass;
	}

	public void setJobClass(String jobClass) {
		this.jobClass = jobClass;
	}

	public String getJobMethod() {
		return jobMethod;
	}

	public void setJobMethod(String jobMethod) {
		this.jobMethod = jobMethod;
	}

	public String getJobCron() {
		return jobCron;
	}

	public void setJobCron(String jobCron) {
		this.jobCron = jobCron;
	}

	public String getJobParam() {
		return jobParam;
	}

	public void setJobParam(String jobParam) {
		this.jobParam = jobParam;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
