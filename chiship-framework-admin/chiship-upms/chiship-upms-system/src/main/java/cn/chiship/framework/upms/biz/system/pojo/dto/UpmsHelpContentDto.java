package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContent;

import javax.validation.constraints.*;
import java.io.Serializable;

/**
 * 帮助文档表单
 * 2025/2/9
 * @author LiJian
 */
/**
 * @author lijian
 */
@ApiModel(value = "帮助文档表单")
public class UpmsHelpContentDto implements Serializable {

    @ApiModelProperty(value = "标题", required = true)
    @NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 50)
    private String title;

    @ApiModelProperty(value = "分类", required = true)
    @NotEmpty(message = "分类" + BaseTipConstants.NOT_EMPTY)
    private String categoryId;

    @ApiModelProperty(value = "封面")
    private String photo;

    @ApiModelProperty(value = "内容", required = true)
    @NotEmpty(message = "内容" + BaseTipConstants.NOT_EMPTY)
    private String content;

    @ApiModelProperty(value = "附件")
    private String attachment;

    @ApiModelProperty(value = "排序", required = true)
    @NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
    private Long orders;

    private static final long serialVersionUID = 1L;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }
}