package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsQuartzJobLogExample;


import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2025-02-10
 */
public interface UpmsQuartzJobLogMapper extends BaseMapper<UpmsQuartzJobLog, UpmsQuartzJobLogExample> {
}