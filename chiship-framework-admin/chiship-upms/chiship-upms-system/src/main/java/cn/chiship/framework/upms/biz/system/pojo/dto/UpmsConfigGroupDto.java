package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "参数分组表单")
public class UpmsConfigGroupDto {

	@ApiModelProperty(value = "分组编号", required = true)
	@NotEmpty(message = "分组编号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 10)
	private String groupCode;

	@ApiModelProperty(value = "分组名称", required = true)
	@NotEmpty(message = "分组名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 8)
	private String groupName;

	@ApiModelProperty(value = "所属上级", required = true)
	@NotEmpty(message = "所属上级" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String pid;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	private Long orders;

	public String getGroupCode() {
		return groupCode;
	}

	public void setGroupCode(String groupCode) {
		this.groupCode = groupCode;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

}
