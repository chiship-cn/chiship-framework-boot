package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.entity.UpmsRegion;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegionExample;
import cn.chiship.sdk.framework.pojo.vo.RegionVo;

/**
 * 地区表业务接口层 2021/9/30
 *
 * @author lijian
 */
public interface UpmsRegionService extends BaseService<UpmsRegion, UpmsRegionExample> {

	/**
	 * 根据主键获取地区名称
	 * @param regionId
	 * @return
	 */
	String getCacheRegionNameById(Long regionId);

	/**
	 * 根据主键获取地区
	 * @param regionId
	 * @return
	 */
	RegionVo getCacheRegionById(Long regionId);

	/**
	 * 根据PID获取地理文件
	 * @param pid
	 * @param name
	 * @return
	 */
	BaseResult loadGeoByPidAndName(Long pid, String name);

	/**
	 * 从缓存中获取数据
	 * @param pid
	 * @return
	 */
	BaseResult cacheListByPid(Long pid);

}
