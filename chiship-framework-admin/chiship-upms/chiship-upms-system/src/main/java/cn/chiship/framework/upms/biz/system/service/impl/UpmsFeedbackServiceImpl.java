package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsFeedbackReplyDto;
import cn.chiship.framework.upms.biz.system.service.UpmsCategoryDictService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.mapper.UpmsFeedbackMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedback;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedbackExample;
import cn.chiship.framework.upms.biz.system.service.UpmsFeedbackService;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

/**
 * 意见反馈业务接口实现层 2022/6/9
 *
 * @author lijian
 */
@Service
public class UpmsFeedbackServiceImpl extends BaseServiceImpl<UpmsFeedback, UpmsFeedbackExample>
		implements UpmsFeedbackService {

	@Resource
	UpmsFeedbackMapper upmsFeedbackMapper;

	@Resource
	UpmsCategoryDictService upmsCategoryDictService;

	@Override
	public BaseResult reply(UpmsFeedbackReplyDto upmsFeedbackReplyDto, String replyPeople) {
		UpmsFeedback upmsFeedback = upmsFeedbackMapper.selectByPrimaryKey(upmsFeedbackReplyDto.getId());
		if (StringUtil.isNull(upmsFeedback)) {
			return BaseResult.error("记录不存在!");
		}
		upmsFeedback.setIsReply(Byte.valueOf("1"));
		upmsFeedback.setReplyContent(upmsFeedbackReplyDto.getReplyContent());
		upmsFeedback.setReplyPeople(replyPeople);
		upmsFeedback.setReplyTime(System.currentTimeMillis());
		return super.updateByPrimaryKeySelective(upmsFeedback);
	}

	@Override
	public BaseResult selectDetailsByPrimaryKey(Object id) {
		return BaseResult.ok(assembleData(selectByPrimaryKey(id)));
	}

	@Override
	public JSONObject assembleData(UpmsFeedback upmsFeedback) {
		JSONObject json = JSON.parseObject(JSON.toJSONString(upmsFeedback));
		json.put("_category", upmsCategoryDictService.getCategoryNameById(upmsFeedback.getCategory(), true));
		return json;
	}

}
