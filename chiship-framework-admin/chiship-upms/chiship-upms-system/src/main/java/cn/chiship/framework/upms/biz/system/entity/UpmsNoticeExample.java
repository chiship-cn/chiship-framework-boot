package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-11
 */
public class UpmsNoticeExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsNoticeExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNull() {
			addCriterion("modify_by is null");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNotNull() {
			addCriterion("modify_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifyByEqualTo(String value) {
			addCriterion("modify_by =", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotEqualTo(String value) {
			addCriterion("modify_by <>", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThan(String value) {
			addCriterion("modify_by >", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThanOrEqualTo(String value) {
			addCriterion("modify_by >=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThan(String value) {
			addCriterion("modify_by <", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThanOrEqualTo(String value) {
			addCriterion("modify_by <=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLike(String value) {
			addCriterion("modify_by like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotLike(String value) {
			addCriterion("modify_by not like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIn(List<String> values) {
			addCriterion("modify_by in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotIn(List<String> values) {
			addCriterion("modify_by not in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByBetween(String value1, String value2) {
			addCriterion("modify_by between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotBetween(String value1, String value2) {
			addCriterion("modify_by not between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNull() {
			addCriterion("created_user_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNotNull() {
			addCriterion("created_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdEqualTo(String value) {
			addCriterion("created_user_id =", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotEqualTo(String value) {
			addCriterion("created_user_id <>", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThan(String value) {
			addCriterion("created_user_id >", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_id >=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThan(String value) {
			addCriterion("created_user_id <", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThanOrEqualTo(String value) {
			addCriterion("created_user_id <=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLike(String value) {
			addCriterion("created_user_id like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotLike(String value) {
			addCriterion("created_user_id not like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIn(List<String> values) {
			addCriterion("created_user_id in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotIn(List<String> values) {
			addCriterion("created_user_id not in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdBetween(String value1, String value2) {
			addCriterion("created_user_id between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotBetween(String value1, String value2) {
			addCriterion("created_user_id not between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIsNull() {
			addCriterion("created_org_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIsNotNull() {
			addCriterion("created_org_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdEqualTo(String value) {
			addCriterion("created_org_id =", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotEqualTo(String value) {
			addCriterion("created_org_id <>", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdGreaterThan(String value) {
			addCriterion("created_org_id >", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_org_id >=", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLessThan(String value) {
			addCriterion("created_org_id <", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLessThanOrEqualTo(String value) {
			addCriterion("created_org_id <=", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLike(String value) {
			addCriterion("created_org_id like", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotLike(String value) {
			addCriterion("created_org_id not like", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIn(List<String> values) {
			addCriterion("created_org_id in", values, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotIn(List<String> values) {
			addCriterion("created_org_id not in", values, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdBetween(String value1, String value2) {
			addCriterion("created_org_id between", value1, value2, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotBetween(String value1, String value2) {
			addCriterion("created_org_id not between", value1, value2, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleIsNull() {
			addCriterion("notice_title is null");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleIsNotNull() {
			addCriterion("notice_title is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleEqualTo(String value) {
			addCriterion("notice_title =", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleNotEqualTo(String value) {
			addCriterion("notice_title <>", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleGreaterThan(String value) {
			addCriterion("notice_title >", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleGreaterThanOrEqualTo(String value) {
			addCriterion("notice_title >=", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleLessThan(String value) {
			addCriterion("notice_title <", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleLessThanOrEqualTo(String value) {
			addCriterion("notice_title <=", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleLike(String value) {
			addCriterion("notice_title like", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleNotLike(String value) {
			addCriterion("notice_title not like", value, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleIn(List<String> values) {
			addCriterion("notice_title in", values, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleNotIn(List<String> values) {
			addCriterion("notice_title not in", values, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleBetween(String value1, String value2) {
			addCriterion("notice_title between", value1, value2, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeTitleNotBetween(String value1, String value2) {
			addCriterion("notice_title not between", value1, value2, "noticeTitle");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractIsNull() {
			addCriterion("notice_abstract is null");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractIsNotNull() {
			addCriterion("notice_abstract is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractEqualTo(String value) {
			addCriterion("notice_abstract =", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractNotEqualTo(String value) {
			addCriterion("notice_abstract <>", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractGreaterThan(String value) {
			addCriterion("notice_abstract >", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractGreaterThanOrEqualTo(String value) {
			addCriterion("notice_abstract >=", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractLessThan(String value) {
			addCriterion("notice_abstract <", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractLessThanOrEqualTo(String value) {
			addCriterion("notice_abstract <=", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractLike(String value) {
			addCriterion("notice_abstract like", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractNotLike(String value) {
			addCriterion("notice_abstract not like", value, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractIn(List<String> values) {
			addCriterion("notice_abstract in", values, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractNotIn(List<String> values) {
			addCriterion("notice_abstract not in", values, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractBetween(String value1, String value2) {
			addCriterion("notice_abstract between", value1, value2, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeAbstractNotBetween(String value1, String value2) {
			addCriterion("notice_abstract not between", value1, value2, "noticeAbstract");
			return (Criteria) this;
		}

		public Criteria andNoticeFileIsNull() {
			addCriterion("notice_file is null");
			return (Criteria) this;
		}

		public Criteria andNoticeFileIsNotNull() {
			addCriterion("notice_file is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeFileEqualTo(String value) {
			addCriterion("notice_file =", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileNotEqualTo(String value) {
			addCriterion("notice_file <>", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileGreaterThan(String value) {
			addCriterion("notice_file >", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileGreaterThanOrEqualTo(String value) {
			addCriterion("notice_file >=", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileLessThan(String value) {
			addCriterion("notice_file <", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileLessThanOrEqualTo(String value) {
			addCriterion("notice_file <=", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileLike(String value) {
			addCriterion("notice_file like", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileNotLike(String value) {
			addCriterion("notice_file not like", value, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileIn(List<String> values) {
			addCriterion("notice_file in", values, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileNotIn(List<String> values) {
			addCriterion("notice_file not in", values, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileBetween(String value1, String value2) {
			addCriterion("notice_file between", value1, value2, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeFileNotBetween(String value1, String value2) {
			addCriterion("notice_file not between", value1, value2, "noticeFile");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIsNull() {
			addCriterion("notice_user_type is null");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIsNotNull() {
			addCriterion("notice_user_type is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeEqualTo(String value) {
			addCriterion("notice_user_type =", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotEqualTo(String value) {
			addCriterion("notice_user_type <>", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeGreaterThan(String value) {
			addCriterion("notice_user_type >", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeGreaterThanOrEqualTo(String value) {
			addCriterion("notice_user_type >=", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLessThan(String value) {
			addCriterion("notice_user_type <", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLessThanOrEqualTo(String value) {
			addCriterion("notice_user_type <=", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeLike(String value) {
			addCriterion("notice_user_type like", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotLike(String value) {
			addCriterion("notice_user_type not like", value, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeIn(List<String> values) {
			addCriterion("notice_user_type in", values, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotIn(List<String> values) {
			addCriterion("notice_user_type not in", values, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeBetween(String value1, String value2) {
			addCriterion("notice_user_type between", value1, value2, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeUserTypeNotBetween(String value1, String value2) {
			addCriterion("notice_user_type not between", value1, value2, "noticeUserType");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryIsNull() {
			addCriterion("notice_category is null");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryIsNotNull() {
			addCriterion("notice_category is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryEqualTo(String value) {
			addCriterion("notice_category =", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryNotEqualTo(String value) {
			addCriterion("notice_category <>", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryGreaterThan(String value) {
			addCriterion("notice_category >", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryGreaterThanOrEqualTo(String value) {
			addCriterion("notice_category >=", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryLessThan(String value) {
			addCriterion("notice_category <", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryLessThanOrEqualTo(String value) {
			addCriterion("notice_category <=", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryLike(String value) {
			addCriterion("notice_category like", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryNotLike(String value) {
			addCriterion("notice_category not like", value, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryIn(List<String> values) {
			addCriterion("notice_category in", values, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryNotIn(List<String> values) {
			addCriterion("notice_category not in", values, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryBetween(String value1, String value2) {
			addCriterion("notice_category between", value1, value2, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeCategoryNotBetween(String value1, String value2) {
			addCriterion("notice_category not between", value1, value2, "noticeCategory");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIsNull() {
			addCriterion("notice_type is null");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIsNotNull() {
			addCriterion("notice_type is not null");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeEqualTo(Byte value) {
			addCriterion("notice_type =", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotEqualTo(Byte value) {
			addCriterion("notice_type <>", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeGreaterThan(Byte value) {
			addCriterion("notice_type >", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("notice_type >=", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeLessThan(Byte value) {
			addCriterion("notice_type <", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeLessThanOrEqualTo(Byte value) {
			addCriterion("notice_type <=", value, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeIn(List<Byte> values) {
			addCriterion("notice_type in", values, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotIn(List<Byte> values) {
			addCriterion("notice_type not in", values, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeBetween(Byte value1, Byte value2) {
			addCriterion("notice_type between", value1, value2, "noticeType");
			return (Criteria) this;
		}

		public Criteria andNoticeTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("notice_type not between", value1, value2, "noticeType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeIsNull() {
			addCriterion("module_type is null");
			return (Criteria) this;
		}

		public Criteria andModuleTypeIsNotNull() {
			addCriterion("module_type is not null");
			return (Criteria) this;
		}

		public Criteria andModuleTypeEqualTo(String value) {
			addCriterion("module_type =", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeNotEqualTo(String value) {
			addCriterion("module_type <>", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeGreaterThan(String value) {
			addCriterion("module_type >", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeGreaterThanOrEqualTo(String value) {
			addCriterion("module_type >=", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeLessThan(String value) {
			addCriterion("module_type <", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeLessThanOrEqualTo(String value) {
			addCriterion("module_type <=", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeLike(String value) {
			addCriterion("module_type like", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeNotLike(String value) {
			addCriterion("module_type not like", value, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeIn(List<String> values) {
			addCriterion("module_type in", values, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeNotIn(List<String> values) {
			addCriterion("module_type not in", values, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeBetween(String value1, String value2) {
			addCriterion("module_type between", value1, value2, "moduleType");
			return (Criteria) this;
		}

		public Criteria andModuleTypeNotBetween(String value1, String value2) {
			addCriterion("module_type not between", value1, value2, "moduleType");
			return (Criteria) this;
		}

		public Criteria andReceiverIsNull() {
			addCriterion("receiver is null");
			return (Criteria) this;
		}

		public Criteria andReceiverIsNotNull() {
			addCriterion("receiver is not null");
			return (Criteria) this;
		}

		public Criteria andReceiverEqualTo(String value) {
			addCriterion("receiver =", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotEqualTo(String value) {
			addCriterion("receiver <>", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverGreaterThan(String value) {
			addCriterion("receiver >", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverGreaterThanOrEqualTo(String value) {
			addCriterion("receiver >=", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverLessThan(String value) {
			addCriterion("receiver <", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverLessThanOrEqualTo(String value) {
			addCriterion("receiver <=", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverLike(String value) {
			addCriterion("receiver like", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotLike(String value) {
			addCriterion("receiver not like", value, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverIn(List<String> values) {
			addCriterion("receiver in", values, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotIn(List<String> values) {
			addCriterion("receiver not in", values, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverBetween(String value1, String value2) {
			addCriterion("receiver between", value1, value2, "receiver");
			return (Criteria) this;
		}

		public Criteria andReceiverNotBetween(String value1, String value2) {
			addCriterion("receiver not between", value1, value2, "receiver");
			return (Criteria) this;
		}

		public Criteria andStartTimeIsNull() {
			addCriterion("start_time is null");
			return (Criteria) this;
		}

		public Criteria andStartTimeIsNotNull() {
			addCriterion("start_time is not null");
			return (Criteria) this;
		}

		public Criteria andStartTimeEqualTo(Long value) {
			addCriterion("start_time =", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotEqualTo(Long value) {
			addCriterion("start_time <>", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeGreaterThan(Long value) {
			addCriterion("start_time >", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("start_time >=", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeLessThan(Long value) {
			addCriterion("start_time <", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeLessThanOrEqualTo(Long value) {
			addCriterion("start_time <=", value, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeIn(List<Long> values) {
			addCriterion("start_time in", values, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotIn(List<Long> values) {
			addCriterion("start_time not in", values, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeBetween(Long value1, Long value2) {
			addCriterion("start_time between", value1, value2, "startTime");
			return (Criteria) this;
		}

		public Criteria andStartTimeNotBetween(Long value1, Long value2) {
			addCriterion("start_time not between", value1, value2, "startTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeIsNull() {
			addCriterion("end_time is null");
			return (Criteria) this;
		}

		public Criteria andEndTimeIsNotNull() {
			addCriterion("end_time is not null");
			return (Criteria) this;
		}

		public Criteria andEndTimeEqualTo(Long value) {
			addCriterion("end_time =", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotEqualTo(Long value) {
			addCriterion("end_time <>", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeGreaterThan(Long value) {
			addCriterion("end_time >", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("end_time >=", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeLessThan(Long value) {
			addCriterion("end_time <", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeLessThanOrEqualTo(Long value) {
			addCriterion("end_time <=", value, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeIn(List<Long> values) {
			addCriterion("end_time in", values, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotIn(List<Long> values) {
			addCriterion("end_time not in", values, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeBetween(Long value1, Long value2) {
			addCriterion("end_time between", value1, value2, "endTime");
			return (Criteria) this;
		}

		public Criteria andEndTimeNotBetween(Long value1, Long value2) {
			addCriterion("end_time not between", value1, value2, "endTime");
			return (Criteria) this;
		}

		public Criteria andSenderIsNull() {
			addCriterion("sender is null");
			return (Criteria) this;
		}

		public Criteria andSenderIsNotNull() {
			addCriterion("sender is not null");
			return (Criteria) this;
		}

		public Criteria andSenderEqualTo(String value) {
			addCriterion("sender =", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotEqualTo(String value) {
			addCriterion("sender <>", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderGreaterThan(String value) {
			addCriterion("sender >", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderGreaterThanOrEqualTo(String value) {
			addCriterion("sender >=", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderLessThan(String value) {
			addCriterion("sender <", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderLessThanOrEqualTo(String value) {
			addCriterion("sender <=", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderLike(String value) {
			addCriterion("sender like", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotLike(String value) {
			addCriterion("sender not like", value, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderIn(List<String> values) {
			addCriterion("sender in", values, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotIn(List<String> values) {
			addCriterion("sender not in", values, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderBetween(String value1, String value2) {
			addCriterion("sender between", value1, value2, "sender");
			return (Criteria) this;
		}

		public Criteria andSenderNotBetween(String value1, String value2) {
			addCriterion("sender not between", value1, value2, "sender");
			return (Criteria) this;
		}

		public Criteria andPriorityIsNull() {
			addCriterion("priority is null");
			return (Criteria) this;
		}

		public Criteria andPriorityIsNotNull() {
			addCriterion("priority is not null");
			return (Criteria) this;
		}

		public Criteria andPriorityEqualTo(Byte value) {
			addCriterion("priority =", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotEqualTo(Byte value) {
			addCriterion("priority <>", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityGreaterThan(Byte value) {
			addCriterion("priority >", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityGreaterThanOrEqualTo(Byte value) {
			addCriterion("priority >=", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityLessThan(Byte value) {
			addCriterion("priority <", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityLessThanOrEqualTo(Byte value) {
			addCriterion("priority <=", value, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityIn(List<Byte> values) {
			addCriterion("priority in", values, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotIn(List<Byte> values) {
			addCriterion("priority not in", values, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityBetween(Byte value1, Byte value2) {
			addCriterion("priority between", value1, value2, "priority");
			return (Criteria) this;
		}

		public Criteria andPriorityNotBetween(Byte value1, Byte value2) {
			addCriterion("priority not between", value1, value2, "priority");
			return (Criteria) this;
		}

		public Criteria andSendStatusIsNull() {
			addCriterion("send_status is null");
			return (Criteria) this;
		}

		public Criteria andSendStatusIsNotNull() {
			addCriterion("send_status is not null");
			return (Criteria) this;
		}

		public Criteria andSendStatusEqualTo(Byte value) {
			addCriterion("send_status =", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusNotEqualTo(Byte value) {
			addCriterion("send_status <>", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusGreaterThan(Byte value) {
			addCriterion("send_status >", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("send_status >=", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusLessThan(Byte value) {
			addCriterion("send_status <", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusLessThanOrEqualTo(Byte value) {
			addCriterion("send_status <=", value, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusIn(List<Byte> values) {
			addCriterion("send_status in", values, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusNotIn(List<Byte> values) {
			addCriterion("send_status not in", values, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusBetween(Byte value1, Byte value2) {
			addCriterion("send_status between", value1, value2, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("send_status not between", value1, value2, "sendStatus");
			return (Criteria) this;
		}

		public Criteria andSendTimeIsNull() {
			addCriterion("send_time is null");
			return (Criteria) this;
		}

		public Criteria andSendTimeIsNotNull() {
			addCriterion("send_time is not null");
			return (Criteria) this;
		}

		public Criteria andSendTimeEqualTo(Long value) {
			addCriterion("send_time =", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeNotEqualTo(Long value) {
			addCriterion("send_time <>", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeGreaterThan(Long value) {
			addCriterion("send_time >", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("send_time >=", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeLessThan(Long value) {
			addCriterion("send_time <", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeLessThanOrEqualTo(Long value) {
			addCriterion("send_time <=", value, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeIn(List<Long> values) {
			addCriterion("send_time in", values, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeNotIn(List<Long> values) {
			addCriterion("send_time not in", values, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeBetween(Long value1, Long value2) {
			addCriterion("send_time between", value1, value2, "sendTime");
			return (Criteria) this;
		}

		public Criteria andSendTimeNotBetween(Long value1, Long value2) {
			addCriterion("send_time not between", value1, value2, "sendTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeIsNull() {
			addCriterion("cancel_time is null");
			return (Criteria) this;
		}

		public Criteria andCancelTimeIsNotNull() {
			addCriterion("cancel_time is not null");
			return (Criteria) this;
		}

		public Criteria andCancelTimeEqualTo(Long value) {
			addCriterion("cancel_time =", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeNotEqualTo(Long value) {
			addCriterion("cancel_time <>", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeGreaterThan(Long value) {
			addCriterion("cancel_time >", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("cancel_time >=", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeLessThan(Long value) {
			addCriterion("cancel_time <", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeLessThanOrEqualTo(Long value) {
			addCriterion("cancel_time <=", value, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeIn(List<Long> values) {
			addCriterion("cancel_time in", values, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeNotIn(List<Long> values) {
			addCriterion("cancel_time not in", values, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeBetween(Long value1, Long value2) {
			addCriterion("cancel_time between", value1, value2, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andCancelTimeNotBetween(Long value1, Long value2) {
			addCriterion("cancel_time not between", value1, value2, "cancelTime");
			return (Criteria) this;
		}

		public Criteria andBusTypeIsNull() {
			addCriterion("bus_type is null");
			return (Criteria) this;
		}

		public Criteria andBusTypeIsNotNull() {
			addCriterion("bus_type is not null");
			return (Criteria) this;
		}

		public Criteria andBusTypeEqualTo(String value) {
			addCriterion("bus_type =", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeNotEqualTo(String value) {
			addCriterion("bus_type <>", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeGreaterThan(String value) {
			addCriterion("bus_type >", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeGreaterThanOrEqualTo(String value) {
			addCriterion("bus_type >=", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeLessThan(String value) {
			addCriterion("bus_type <", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeLessThanOrEqualTo(String value) {
			addCriterion("bus_type <=", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeLike(String value) {
			addCriterion("bus_type like", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeNotLike(String value) {
			addCriterion("bus_type not like", value, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeIn(List<String> values) {
			addCriterion("bus_type in", values, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeNotIn(List<String> values) {
			addCriterion("bus_type not in", values, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeBetween(String value1, String value2) {
			addCriterion("bus_type between", value1, value2, "busType");
			return (Criteria) this;
		}

		public Criteria andBusTypeNotBetween(String value1, String value2) {
			addCriterion("bus_type not between", value1, value2, "busType");
			return (Criteria) this;
		}

		public Criteria andBusIdIsNull() {
			addCriterion("bus_id is null");
			return (Criteria) this;
		}

		public Criteria andBusIdIsNotNull() {
			addCriterion("bus_id is not null");
			return (Criteria) this;
		}

		public Criteria andBusIdEqualTo(String value) {
			addCriterion("bus_id =", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdNotEqualTo(String value) {
			addCriterion("bus_id <>", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdGreaterThan(String value) {
			addCriterion("bus_id >", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdGreaterThanOrEqualTo(String value) {
			addCriterion("bus_id >=", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdLessThan(String value) {
			addCriterion("bus_id <", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdLessThanOrEqualTo(String value) {
			addCriterion("bus_id <=", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdLike(String value) {
			addCriterion("bus_id like", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdNotLike(String value) {
			addCriterion("bus_id not like", value, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdIn(List<String> values) {
			addCriterion("bus_id in", values, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdNotIn(List<String> values) {
			addCriterion("bus_id not in", values, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdBetween(String value1, String value2) {
			addCriterion("bus_id between", value1, value2, "busId");
			return (Criteria) this;
		}

		public Criteria andBusIdNotBetween(String value1, String value2) {
			addCriterion("bus_id not between", value1, value2, "busId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdIsNull() {
			addCriterion("dt_task_id is null");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdIsNotNull() {
			addCriterion("dt_task_id is not null");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdEqualTo(String value) {
			addCriterion("dt_task_id =", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdNotEqualTo(String value) {
			addCriterion("dt_task_id <>", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdGreaterThan(String value) {
			addCriterion("dt_task_id >", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdGreaterThanOrEqualTo(String value) {
			addCriterion("dt_task_id >=", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdLessThan(String value) {
			addCriterion("dt_task_id <", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdLessThanOrEqualTo(String value) {
			addCriterion("dt_task_id <=", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdLike(String value) {
			addCriterion("dt_task_id like", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdNotLike(String value) {
			addCriterion("dt_task_id not like", value, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdIn(List<String> values) {
			addCriterion("dt_task_id in", values, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdNotIn(List<String> values) {
			addCriterion("dt_task_id not in", values, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdBetween(String value1, String value2) {
			addCriterion("dt_task_id between", value1, value2, "dtTaskId");
			return (Criteria) this;
		}

		public Criteria andDtTaskIdNotBetween(String value1, String value2) {
			addCriterion("dt_task_id not between", value1, value2, "dtTaskId");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}