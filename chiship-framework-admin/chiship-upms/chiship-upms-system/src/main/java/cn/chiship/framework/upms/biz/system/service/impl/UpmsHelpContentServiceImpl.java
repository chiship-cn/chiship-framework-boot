package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.mapper.UpmsCategoryDictMapper;
import cn.chiship.framework.upms.biz.system.pojo.vo.ContentUnionAllCategoryVo;
import cn.chiship.framework.upms.biz.system.service.UpmsCategoryDictService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.mapper.UpmsHelpContentMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContent;
import cn.chiship.framework.upms.biz.system.entity.UpmsHelpContentExample;
import cn.chiship.framework.upms.biz.system.service.UpmsHelpContentService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 帮助文档业务接口实现层
 * 2025/2/9
 *
 * @author lijian
 */
@Service
public class UpmsHelpContentServiceImpl extends BaseServiceImpl<UpmsHelpContent, UpmsHelpContentExample> implements UpmsHelpContentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsHelpContentServiceImpl.class);

    @Resource
    UpmsHelpContentMapper upmsHelpContentMapper;
    @Resource
    UpmsCategoryDictService upmsCategoryDictService;

    @Override
    public JSONObject assembleData(UpmsHelpContent upmsHelpContent) {
        if (upmsHelpContent == null) {
            return new JSONObject();
        }
        JSONObject json = JSON.parseObject(JSON.toJSONString(upmsHelpContent));
        if (!StringUtil.isNullOrEmpty(upmsHelpContent.getCategoryId())) {
            json.putAll(upmsCategoryDictService.getParentInfoById(upmsHelpContent.getCategoryId()));
        }
        return json;
    }

    @Override
    public BaseResult selectDetailsByPrimaryKey(Object id) {
        UpmsHelpContentExample upmsHelpContentExample = new UpmsHelpContentExample();
        upmsHelpContentExample.createCriteria().andIdEqualTo(id.toString());
        List<UpmsHelpContent> upmsHelpContents = upmsHelpContentMapper.selectByExampleWithBLOBs(upmsHelpContentExample);
        if (upmsHelpContents.isEmpty()) {
            return null;
        }
        UpmsHelpContent upmsHelpContent = upmsHelpContents.get(0);
        JSONObject json = assembleData(upmsHelpContent);
        return BaseResult.ok(json);
    }


    @Override
    public List<ContentUnionAllCategoryVo> selectContentUnionAllCategory(HashMap<String, Object> map) {
        return upmsHelpContentMapper.selectContentUnionAllCategory(map);
    }
}
