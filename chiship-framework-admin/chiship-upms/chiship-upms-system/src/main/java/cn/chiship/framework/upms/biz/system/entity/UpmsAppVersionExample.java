package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2023-02-26
 */
public class UpmsAppVersionExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public UpmsAppVersionExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andAppCodeIsNull() {
			addCriterion("app_code is null");
			return (Criteria) this;
		}

		public Criteria andAppCodeIsNotNull() {
			addCriterion("app_code is not null");
			return (Criteria) this;
		}

		public Criteria andAppCodeEqualTo(String value) {
			addCriterion("app_code =", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeNotEqualTo(String value) {
			addCriterion("app_code <>", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeGreaterThan(String value) {
			addCriterion("app_code >", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeGreaterThanOrEqualTo(String value) {
			addCriterion("app_code >=", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeLessThan(String value) {
			addCriterion("app_code <", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeLessThanOrEqualTo(String value) {
			addCriterion("app_code <=", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeLike(String value) {
			addCriterion("app_code like", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeNotLike(String value) {
			addCriterion("app_code not like", value, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeIn(List<String> values) {
			addCriterion("app_code in", values, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeNotIn(List<String> values) {
			addCriterion("app_code not in", values, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeBetween(String value1, String value2) {
			addCriterion("app_code between", value1, value2, "appCode");
			return (Criteria) this;
		}

		public Criteria andAppCodeNotBetween(String value1, String value2) {
			addCriterion("app_code not between", value1, value2, "appCode");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andAppVersionIsNull() {
			addCriterion("app_version is null");
			return (Criteria) this;
		}

		public Criteria andAppVersionIsNotNull() {
			addCriterion("app_version is not null");
			return (Criteria) this;
		}

		public Criteria andAppVersionEqualTo(Integer value) {
			addCriterion("app_version =", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionNotEqualTo(Integer value) {
			addCriterion("app_version <>", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionGreaterThan(Integer value) {
			addCriterion("app_version >", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionGreaterThanOrEqualTo(Integer value) {
			addCriterion("app_version >=", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionLessThan(Integer value) {
			addCriterion("app_version <", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionLessThanOrEqualTo(Integer value) {
			addCriterion("app_version <=", value, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionIn(List<Integer> values) {
			addCriterion("app_version in", values, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionNotIn(List<Integer> values) {
			addCriterion("app_version not in", values, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionBetween(Integer value1, Integer value2) {
			addCriterion("app_version between", value1, value2, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionNotBetween(Integer value1, Integer value2) {
			addCriterion("app_version not between", value1, value2, "appVersion");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameIsNull() {
			addCriterion("app_version_name is null");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameIsNotNull() {
			addCriterion("app_version_name is not null");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameEqualTo(String value) {
			addCriterion("app_version_name =", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameNotEqualTo(String value) {
			addCriterion("app_version_name <>", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameGreaterThan(String value) {
			addCriterion("app_version_name >", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameGreaterThanOrEqualTo(String value) {
			addCriterion("app_version_name >=", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameLessThan(String value) {
			addCriterion("app_version_name <", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameLessThanOrEqualTo(String value) {
			addCriterion("app_version_name <=", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameLike(String value) {
			addCriterion("app_version_name like", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameNotLike(String value) {
			addCriterion("app_version_name not like", value, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameIn(List<String> values) {
			addCriterion("app_version_name in", values, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameNotIn(List<String> values) {
			addCriterion("app_version_name not in", values, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameBetween(String value1, String value2) {
			addCriterion("app_version_name between", value1, value2, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppVersionNameNotBetween(String value1, String value2) {
			addCriterion("app_version_name not between", value1, value2, "appVersionName");
			return (Criteria) this;
		}

		public Criteria andAppRemarkIsNull() {
			addCriterion("app_remark is null");
			return (Criteria) this;
		}

		public Criteria andAppRemarkIsNotNull() {
			addCriterion("app_remark is not null");
			return (Criteria) this;
		}

		public Criteria andAppRemarkEqualTo(String value) {
			addCriterion("app_remark =", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkNotEqualTo(String value) {
			addCriterion("app_remark <>", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkGreaterThan(String value) {
			addCriterion("app_remark >", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("app_remark >=", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkLessThan(String value) {
			addCriterion("app_remark <", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkLessThanOrEqualTo(String value) {
			addCriterion("app_remark <=", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkLike(String value) {
			addCriterion("app_remark like", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkNotLike(String value) {
			addCriterion("app_remark not like", value, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkIn(List<String> values) {
			addCriterion("app_remark in", values, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkNotIn(List<String> values) {
			addCriterion("app_remark not in", values, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkBetween(String value1, String value2) {
			addCriterion("app_remark between", value1, value2, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppRemarkNotBetween(String value1, String value2) {
			addCriterion("app_remark not between", value1, value2, "appRemark");
			return (Criteria) this;
		}

		public Criteria andAppUrlIsNull() {
			addCriterion("app_url is null");
			return (Criteria) this;
		}

		public Criteria andAppUrlIsNotNull() {
			addCriterion("app_url is not null");
			return (Criteria) this;
		}

		public Criteria andAppUrlEqualTo(String value) {
			addCriterion("app_url =", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlNotEqualTo(String value) {
			addCriterion("app_url <>", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlGreaterThan(String value) {
			addCriterion("app_url >", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlGreaterThanOrEqualTo(String value) {
			addCriterion("app_url >=", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlLessThan(String value) {
			addCriterion("app_url <", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlLessThanOrEqualTo(String value) {
			addCriterion("app_url <=", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlLike(String value) {
			addCriterion("app_url like", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlNotLike(String value) {
			addCriterion("app_url not like", value, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlIn(List<String> values) {
			addCriterion("app_url in", values, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlNotIn(List<String> values) {
			addCriterion("app_url not in", values, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlBetween(String value1, String value2) {
			addCriterion("app_url between", value1, value2, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppUrlNotBetween(String value1, String value2) {
			addCriterion("app_url not between", value1, value2, "appUrl");
			return (Criteria) this;
		}

		public Criteria andAppTypeIsNull() {
			addCriterion("app_type is null");
			return (Criteria) this;
		}

		public Criteria andAppTypeIsNotNull() {
			addCriterion("app_type is not null");
			return (Criteria) this;
		}

		public Criteria andAppTypeEqualTo(Byte value) {
			addCriterion("app_type =", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeNotEqualTo(Byte value) {
			addCriterion("app_type <>", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeGreaterThan(Byte value) {
			addCriterion("app_type >", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("app_type >=", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeLessThan(Byte value) {
			addCriterion("app_type <", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeLessThanOrEqualTo(Byte value) {
			addCriterion("app_type <=", value, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeIn(List<Byte> values) {
			addCriterion("app_type in", values, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeNotIn(List<Byte> values) {
			addCriterion("app_type not in", values, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeBetween(Byte value1, Byte value2) {
			addCriterion("app_type between", value1, value2, "appType");
			return (Criteria) this;
		}

		public Criteria andAppTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("app_type not between", value1, value2, "appType");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}