package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsFeedbackReplyDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedback;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedbackExample;

/**
 * 意见反馈业务接口层 2022/6/9
 *
 * @author lijian
 */
public interface UpmsFeedbackService extends BaseService<UpmsFeedback, UpmsFeedbackExample> {

	/**
	 * 回复
	 * @param upmsFeedbackReplyDto 回复实体
	 * @param replyPeople 回复人员信息
	 * @return 结果
	 */
	BaseResult reply(UpmsFeedbackReplyDto upmsFeedbackReplyDto, String replyPeople);

}
