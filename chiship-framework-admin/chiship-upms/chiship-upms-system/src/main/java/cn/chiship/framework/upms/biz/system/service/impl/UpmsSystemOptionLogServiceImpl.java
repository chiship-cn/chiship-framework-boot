package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemOptionLogExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsSystemOptionLogMapper;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemOptionLogService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 系统操作日志业务接口实现层 2021/9/30
 *
 * @author lijian
 */
@Service
public class UpmsSystemOptionLogServiceImpl extends BaseServiceImpl<UpmsSystemOptionLog, UpmsSystemOptionLogExample>
		implements UpmsSystemOptionLogService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSystemOptionLogServiceImpl.class);

	@Resource
	UpmsSystemOptionLogMapper upmsSystemOptionLogMapper;

	@Override
	public BaseResult getById(String id) {
		UpmsSystemOptionLogExample upmsSystemOptionLogExample = new UpmsSystemOptionLogExample();
		upmsSystemOptionLogExample.createCriteria().andIdEqualTo(id);
		List<UpmsSystemOptionLog> upmsSystemOptionLogs = upmsSystemOptionLogMapper
				.selectByExampleWithBLOBs(upmsSystemOptionLogExample);
		if (upmsSystemOptionLogs.isEmpty()) {
			return BaseResult.ok(null);
		}
		return BaseResult.ok(upmsSystemOptionLogs.get(0));
	}

}
