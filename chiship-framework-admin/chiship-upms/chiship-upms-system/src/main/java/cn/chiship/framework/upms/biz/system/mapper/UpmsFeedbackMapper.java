package cn.chiship.framework.upms.biz.system.mapper;

import cn.chiship.framework.upms.biz.system.entity.UpmsFeedback;
import cn.chiship.framework.upms.biz.system.entity.UpmsFeedbackExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface UpmsFeedbackMapper extends BaseMapper<UpmsFeedback, UpmsFeedbackExample> {

}