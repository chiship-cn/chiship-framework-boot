package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.upms.biz.base.service.UpmsSystemCacheService;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegion;
import cn.chiship.framework.upms.biz.system.service.UpmsRegionService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.upms.biz.system.entity.UpmsRegionExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 地区表控制层 2021/9/30
 *
 * @author lijian
 */
@RestController
@RequestMapping("/region")
@Api(tags = "地区表")
public class UpmsRegionController extends BaseController<UpmsRegion, UpmsRegionExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsRegionController.class);

	@Resource
	private UpmsRegionService upmsRegionService;

	@Resource
	private UpmsSystemCacheService upmsCacheService;

	@Override
	public BaseService getService() {
		return upmsRegionService;
	}

	@ApiOperation(value = "地区分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "pid", value = "地区ID", defaultValue = "100000", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "regionName", value = "区域名称", dataTypeClass = String.class,
					paramType = "query"), })
	@GetMapping(value = "/page")
	@Authorization
	public ResponseEntity<BaseResult> treePage(
			@RequestParam(required = false, defaultValue = "100000", value = "pid") Long pid,
			@RequestParam(required = false, defaultValue = "", value = "regionName") String regionName) {
		UpmsRegionExample upmsRegionExample = new UpmsRegionExample();
		// 创造条件
		UpmsRegionExample.Criteria criteria = upmsRegionExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO).andPidEqualTo(pid);
		if (!StringUtil.isNullOrEmpty(regionName)) {
			criteria.andNameLike("%" + regionName + "%");
		}

		PageVo pageVo = super.page(upmsRegionExample);
		return super.responseEntity(BaseResult.ok(pageVo));
	}

	@ApiOperation(value = "地址获取")
	@ApiImplicitParams({ @ApiImplicitParam(name = "pid", value = "所属父级", defaultValue = "100000", required = true,
			dataTypeClass = Long.class, paramType = "query") })
	@GetMapping(value = "/loadByPid")
	public ResponseEntity<BaseResult> loadByPid(
			@RequestParam(required = false, defaultValue = "100000", value = "pid") Long pid) {
		if (StringUtil.isNull(pid)) {
			pid = 100000L;
		}
		UpmsRegionExample upmsRegionExample = new UpmsRegionExample();
		upmsRegionExample.createCriteria().andPidEqualTo(pid);
		upmsRegionExample.setOrderByClause("id asc");
		List<UpmsRegion> regions = upmsRegionService.selectByExample(upmsRegionExample);
		return super.responseEntity(BaseResult.ok(regions));
	}

	@ApiOperation(value = "根据地区名称和所属父级查找")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "pid", value = "父级", defaultValue = "-1", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "name", value = "地区名称", required = true, dataTypeClass = String.class,
					paramType = "query") })
	@GetMapping(value = "/loadGeoByPidAndName")
	public ResponseEntity<BaseResult> regionByNameAndPid(
			@RequestParam(required = false, defaultValue = "-1", value = "pid") Long pid,
			@RequestParam(defaultValue = "", value = "name") String name) {

		return super.responseEntity(upmsRegionService.loadGeoByPidAndName(pid, name));
	}

	@SystemOptionAnnotation(describe = "缓存地区", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
	@ApiOperation(value = "缓存地区")
	@GetMapping(value = "/cacheRegion")
	@Authorization
	public ResponseEntity<BaseResult> cacheRegion() {
		upmsCacheService.cacheRegions();
		return super.responseEntity(BaseResult.ok());
	}

	@ApiOperation(value = "地址获取(缓存)")
	@ApiImplicitParams({ @ApiImplicitParam(name = "pid", value = "所属父级地区ID", defaultValue = "100000", required = true,
			dataTypeClass = Long.class, paramType = "query") })
	@GetMapping(value = "/cacheListByPid")
	public ResponseEntity<BaseResult> cacheListByPid(
			@RequestParam(required = false, defaultValue = "100000", value = "pid") Long pid) {
		return new ResponseEntity<>(upmsRegionService.cacheListByPid(pid), HttpStatus.OK);
	}

}
