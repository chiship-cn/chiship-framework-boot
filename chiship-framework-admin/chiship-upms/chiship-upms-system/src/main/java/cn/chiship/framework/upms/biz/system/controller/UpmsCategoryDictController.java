package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.upms.biz.system.service.UpmsCategoryDictService;
import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDict;
import cn.chiship.framework.upms.biz.system.entity.UpmsCategoryDictExample;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsCategoryDictDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 分类字典控制层 2024/10/13
 *
 * @author lijian
 */
@RestController
@RequestMapping("/upmsCategoryDict")
@Api(tags = "分类字典")
public class UpmsCategoryDictController extends BaseController<UpmsCategoryDict, UpmsCategoryDictExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpmsCategoryDictController.class);

    @Resource
    private UpmsCategoryDictService upmsCategoryDictService;

    @Override
    public BaseService getService() {
        return upmsCategoryDictService;
    }

    @ApiOperation(value = "根据类型加载树形表格")
    @ApiImplicitParams({@ApiImplicitParam(name = "type", required = true, value = "所属类型", dataTypeClass = String.class,
            paramType = "query"),

    })
    @GetMapping(value = "/treeByType")
    @Authorization
    public ResponseEntity<BaseResult> treeByType(@RequestParam(value = "type") String type) {
        UpmsCategoryDictExample upmsCategoryDictExample = new UpmsCategoryDictExample();
        /**
         * 创造条件
         */
        UpmsCategoryDictExample.Criteria upmsCategoryDictCriteria = upmsCategoryDictExample.createCriteria();
        upmsCategoryDictCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(type)) {
            upmsCategoryDictCriteria.andTypeEqualTo(type);
        }
        upmsCategoryDictExample.setOrderByClause("orders desc");
        return super.responseEntity(upmsCategoryDictService.treeTable(upmsCategoryDictExample));
    }

    @ApiOperation(value = "根据类型加载树形表格(缓存)")
    @ApiImplicitParams({@ApiImplicitParam(name = "type", value = "所属类型", required = true, dataTypeClass = String.class,
            paramType = "query"),

    })
    @GetMapping(value = "/cacheTreeByType")
    public ResponseEntity<BaseResult> cacheTreeByType(@RequestParam(value = "type") String type) {
        return super.responseEntity(upmsCategoryDictService.cacheTreeTable(type));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存分类字典")
    @ApiOperation(value = "保存分类字典")
    @PostMapping(value = "save")
    @Authorization
    public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsCategoryDictDto upmsCategoryDictDto) {
        UpmsCategoryDict upmsCategoryDict = new UpmsCategoryDict();
        BeanUtils.copyProperties(upmsCategoryDictDto, upmsCategoryDict);
        return super.responseEntity(super.save(upmsCategoryDict));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新分类字典")
    @ApiOperation(value = "更新分类字典")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
            required = true)})
    @PostMapping(value = "update/{id}")
    @Authorization
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid UpmsCategoryDictDto upmsCategoryDictDto) {
        UpmsCategoryDict upmsCategoryDict = new UpmsCategoryDict();
        BeanUtils.copyProperties(upmsCategoryDictDto, upmsCategoryDict);
        return super.responseEntity(super.update(id, upmsCategoryDict));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除分类字典")
    @ApiOperation(value = "删除分类字典")
    @PostMapping(value = "remove")
    @Authorization
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        UpmsCategoryDictExample upmsCategoryDictExample = new UpmsCategoryDictExample();
        upmsCategoryDictExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(upmsCategoryDictExample));
    }

}