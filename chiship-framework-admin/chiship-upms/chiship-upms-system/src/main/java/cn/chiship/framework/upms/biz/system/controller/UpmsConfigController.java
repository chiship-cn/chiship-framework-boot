package cn.chiship.framework.upms.biz.system.controller;

import cn.chiship.framework.common.util.FrameworkUtil2;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfig;
import cn.chiship.framework.upms.biz.system.entity.UpmsConfigExample;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigDto;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigGroupDto;
import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsConfigSettingDto;
import cn.chiship.framework.upms.biz.system.service.UpmsConfigService;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.util.FrameworkUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统配置控制层 2021/9/27
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/systemConfig")
@Api(tags = "系统配置")
public class UpmsConfigController extends BaseController<UpmsConfig, UpmsConfigExample> {

	@Resource
	private UpmsConfigService upmsConfigService;

	@Override
	public BaseService getService() {
		return upmsConfigService;
	}

	@ApiOperation(value = "加载配置分组")
	@ApiImplicitParams({})
	@GetMapping(value = "group/list")
	public ResponseEntity<BaseResult> listGroup() {
		UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
		UpmsConfigExample.Criteria criteria = upmsConfigExample.createCriteria();
		criteria.andIsDeletedEqualTo(BaseConstants.NO).andTypeEqualTo(Byte.valueOf("0"));
		upmsConfigExample.setOrderByClause(FrameworkUtil.formatSort("+orders"));
		List<UpmsConfig> upmsConfigs = upmsConfigService.selectByExample(upmsConfigExample);
		return super.responseEntity(BaseResult.ok(FrameworkUtil2.assemblyDataTree("0", upmsConfigs)));
	}

	@ApiOperation(value = "新增配置分组")
	@PostMapping(value = "group/save")
	public ResponseEntity<BaseResult> groupSave(@RequestBody @Valid UpmsConfigGroupDto upmsConfigGroupDto) {
		BaseResult baseResult = upmsConfigService.saveGroup(upmsConfigGroupDto);
		if (baseResult.isSuccess()) {
			upmsConfigService.cacheConfigs();
		}
		return super.responseEntity(baseResult);
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path"), })
	@ApiOperation(value = "编辑配置分组")
	@PostMapping(value = "group/update/{id}")
	public ResponseEntity<BaseResult> groupUpdate(@PathVariable("id") String id,
			@RequestBody @Valid UpmsConfigGroupDto upmsConfigGroupDto) {
		return super.responseEntity(upmsConfigService.updateGroup(id, upmsConfigGroupDto));
	}

	@ApiOperation(value = "根据分组编号获取记录及孩子元素")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupCode", value = "分组编号", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/getByGroupCode")
	public ResponseEntity<BaseResult> getByGroupCode(@RequestParam(value = "groupCode") String groupCode) {
		return super.responseEntity(upmsConfigService.getByGroupCode(groupCode));
	}

	@ApiOperation(value = "根据分组主键获取参数配置项")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupId", value = "分组ID", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/listByGroupId")
	public ResponseEntity<BaseResult> listByGroupId(@RequestParam(value = "groupId") String groupId) {
		return super.responseEntity(upmsConfigService.listByGroupId(groupId));
	}

	@ApiOperation(value = "根据分组编号获取参数配置项")
	@ApiImplicitParams({ @ApiImplicitParam(name = "groupCode", value = "分组编号", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/listByGroupCode")
	public ResponseEntity<BaseResult> listByGroupCode(@RequestParam(value = "groupCode") String groupCode) {
		return super.responseEntity(upmsConfigService.listByGroupCode(groupCode));
	}

	@ApiOperation(value = "新增配置项")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid UpmsConfigDto upmsConfigDto) {
		UpmsConfig upmsConfig = new UpmsConfig();
		BeanUtils.copyProperties(upmsConfigDto, upmsConfig);
		upmsConfig.setType(Byte.valueOf("1"));
		return super.responseEntity(super.save(upmsConfig));
	}

	@ApiImplicitParams({
			@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path"), })
	@ApiOperation(value = "编辑配置项")
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid UpmsConfigDto upmsConfigDto) {
		UpmsConfig upmsConfig = new UpmsConfig();
		BeanUtils.copyProperties(upmsConfigDto, upmsConfig);
		return super.responseEntity(super.update(id, upmsConfig));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "修改配置参数值")
	@ApiOperation(value = "修改配置参数值")
	@PostMapping(value = "/updateConfig")
	public ResponseEntity<BaseResult> updateConfig(@RequestBody List<UpmsConfigSettingDto> configSettingDtos) {
		if (configSettingDtos.isEmpty()) {
			return super.responseEntity(BaseResult.error(BaseResultEnum.EXCEPTION_DATA_BASE_UPDATE, false));
		}
		BaseResult baseResult = upmsConfigService.updateConfig(configSettingDtos);
		if (baseResult.isSuccess()) {
			upmsConfigService.cacheConfigs();
		}
		return super.responseEntity(baseResult);
	}

	@SystemOptionAnnotation(describe = "删除系统配置", option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE)
	@ApiOperation(value = "删除系统配置")
	@PostMapping(value = "/remove")
	public ResponseEntity<BaseResult> dataDictItemRemove(@RequestBody @Valid List<String> ids) {

		UpmsConfigExample upmsConfigExample = new UpmsConfigExample();
		upmsConfigExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(upmsConfigExample));
	}

}
