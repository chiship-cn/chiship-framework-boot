package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class UpmsRegion implements Serializable {

	/**
	 * 主键
	 *
	 * @mbg.generated
	 */
	private Long id;

	private Long gmtCreated;

	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 名字
	 *
	 * @mbg.generated
	 */
	private String name;

	private Long pid;

	/**
	 * 首字母
	 *
	 * @mbg.generated
	 */
	private String firstLetter;

	/**
	 * 全拼
	 *
	 * @mbg.generated
	 */
	private String fullPinyin;

	private String level;

	private String adCode;

	private static final long serialVersionUID = 1L;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPid() {
		return pid;
	}

	public void setPid(Long pid) {
		this.pid = pid;
	}

	public String getFirstLetter() {
		return firstLetter;
	}

	public void setFirstLetter(String firstLetter) {
		this.firstLetter = firstLetter;
	}

	public String getFullPinyin() {
		return fullPinyin;
	}

	public void setFullPinyin(String fullPinyin) {
		this.fullPinyin = fullPinyin;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getAdCode() {
		return adCode;
	}

	public void setAdCode(String adCode) {
		this.adCode = adCode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", name=").append(name);
		sb.append(", pid=").append(pid);
		sb.append(", firstLetter=").append(firstLetter);
		sb.append(", fullPinyin=").append(fullPinyin);
		sb.append(", level=").append(level);
		sb.append(", adCode=").append(adCode);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsRegion other = (UpmsRegion) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
				&& (this.getFirstLetter() == null ? other.getFirstLetter() == null
						: this.getFirstLetter().equals(other.getFirstLetter()))
				&& (this.getFullPinyin() == null ? other.getFullPinyin() == null
						: this.getFullPinyin().equals(other.getFullPinyin()))
				&& (this.getLevel() == null ? other.getLevel() == null : this.getLevel().equals(other.getLevel()))
				&& (this.getAdCode() == null ? other.getAdCode() == null : this.getAdCode().equals(other.getAdCode()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
		result = prime * result + ((getFirstLetter() == null) ? 0 : getFirstLetter().hashCode());
		result = prime * result + ((getFullPinyin() == null) ? 0 : getFullPinyin().hashCode());
		result = prime * result + ((getLevel() == null) ? 0 : getLevel().hashCode());
		result = prime * result + ((getAdCode() == null) ? 0 : getAdCode().hashCode());
		return result;
	}

}