package cn.chiship.framework.upms.biz.base.controller;

import cn.chiship.framework.upms.biz.base.service.IRedisService;
import cn.chiship.sdk.core.monitor.ServerMonitorSingleton;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.properties.ChishipDefaultProperties;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author lijian
 */
@RestController
@Api(tags = "监控控制器")
@RequestMapping("/monitor")
public class MonitorController {

	@Resource
	private IRedisService iRedisService;

	@Resource
	ChishipDefaultProperties chishipDefaultProperties;

	@ApiOperation(value = "获取软件信息")
	@GetMapping(value = "/softInfo")
	public ResponseEntity<BaseResult> softInfo() {
		Map<String, Object> map = new HashMap<>(7);
		map.put("id", chishipDefaultProperties.getId());
		map.put("name", chishipDefaultProperties.getName());
		map.put("title", chishipDefaultProperties.getTitle());
		map.put("version", chishipDefaultProperties.getVersion());
		map.put("copyrightYear", chishipDefaultProperties.getCopyrightYear());
		map.put("demoEnabled", chishipDefaultProperties.getDemoEnabled());
		map.put("keyGenerator", chishipDefaultProperties.getKeyGenerator());
		map.put("interfaceWhite", chishipDefaultProperties.getInterfaceWhite());
		map.put("importDataCatalogId", chishipDefaultProperties.getImportDataCatalog());
		return new ResponseEntity<>(BaseResult.ok(map), HttpStatus.OK);
	}

	@ApiOperation(value = "服务器监控")
	@GetMapping(value = "server")
	public ResponseEntity<BaseResult> system() {
		ServerMonitorSingleton server = ServerMonitorSingleton.getInstance();
		server.copyTo();
		return new ResponseEntity<>(BaseResult.ok(server), HttpStatus.OK);
	}

	@ApiOperation(value = "Redis监控")
	@GetMapping(value = "redis")
	public ResponseEntity<BaseResult> monitor() {
		return new ResponseEntity(iRedisService.monitor(), HttpStatus.OK);
	}

	@ApiOperation(value = "Redis所有键值")
	@GetMapping(value = "redis/keys")
	public ResponseEntity<BaseResult> keys() {
		return new ResponseEntity(iRedisService.keyTrees(), HttpStatus.OK);
	}

	@ApiOperation(value = "Redis根据键值获取value")
	@PostMapping(value = "redis/value")
	public ResponseEntity<BaseResult> value(@RequestBody String key) {
		return new ResponseEntity(iRedisService.value(key), HttpStatus.OK);
	}

}
