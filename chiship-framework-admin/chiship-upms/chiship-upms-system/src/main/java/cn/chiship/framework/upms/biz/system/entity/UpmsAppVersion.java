package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2023-02-26
 */
public class UpmsAppVersion implements Serializable {

	/**
	 * 主键uuid
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 识别码
	 */
	private String appCode;

	/**
	 * app名称
	 */
	private String name;

	/**
	 * 版本
	 */
	private Integer appVersion;

	/**
	 * 版本名称
	 */
	private String appVersionName;

	/**
	 * 备注
	 */
	private String appRemark;

	/**
	 * 下载路径
	 */
	private String appUrl;

	/**
	 * 类型 0 android 1 IOS
	 */
	private Byte appType;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAppCode() {
		return appCode;
	}

	public void setAppCode(String appCode) {
		this.appCode = appCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(Integer appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppVersionName() {
		return appVersionName;
	}

	public void setAppVersionName(String appVersionName) {
		this.appVersionName = appVersionName;
	}

	public String getAppRemark() {
		return appRemark;
	}

	public void setAppRemark(String appRemark) {
		this.appRemark = appRemark;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public void setAppUrl(String appUrl) {
		this.appUrl = appUrl;
	}

	public Byte getAppType() {
		return appType;
	}

	public void setAppType(Byte appType) {
		this.appType = appType;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", appCode=").append(appCode);
		sb.append(", name=").append(name);
		sb.append(", appVersion=").append(appVersion);
		sb.append(", appVersionName=").append(appVersionName);
		sb.append(", appRemark=").append(appRemark);
		sb.append(", appUrl=").append(appUrl);
		sb.append(", appType=").append(appType);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsAppVersion other = (UpmsAppVersion) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getAppCode() == null ? other.getAppCode() == null
						: this.getAppCode().equals(other.getAppCode()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getAppVersion() == null ? other.getAppVersion() == null
						: this.getAppVersion().equals(other.getAppVersion()))
				&& (this.getAppVersionName() == null ? other.getAppVersionName() == null
						: this.getAppVersionName().equals(other.getAppVersionName()))
				&& (this.getAppRemark() == null ? other.getAppRemark() == null
						: this.getAppRemark().equals(other.getAppRemark()))
				&& (this.getAppUrl() == null ? other.getAppUrl() == null : this.getAppUrl().equals(other.getAppUrl()))
				&& (this.getAppType() == null ? other.getAppType() == null
						: this.getAppType().equals(other.getAppType()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getAppCode() == null) ? 0 : getAppCode().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getAppVersion() == null) ? 0 : getAppVersion().hashCode());
		result = prime * result + ((getAppVersionName() == null) ? 0 : getAppVersionName().hashCode());
		result = prime * result + ((getAppRemark() == null) ? 0 : getAppRemark().hashCode());
		result = prime * result + ((getAppUrl() == null) ? 0 : getAppUrl().hashCode());
		result = prime * result + ((getAppType() == null) ? 0 : getAppType().hashCode());
		return result;
	}

}