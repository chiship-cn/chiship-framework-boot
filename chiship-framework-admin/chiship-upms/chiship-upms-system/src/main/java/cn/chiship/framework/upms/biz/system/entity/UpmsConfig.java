package cn.chiship.framework.upms.biz.system.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2023-08-28
 */
public class UpmsConfig implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 所属上级
	 */
	private String pid;

	/**
	 * 类型 0 分组 1 参数项
	 */
	private Byte type;

	/**
	 * 参数编号
	 */
	private String paramCode;

	/**
	 * 参数名称
	 */
	private String paramName;

	/**
	 * 参数值
	 */
	private String paramValue;

	/**
	 * 参数描述
	 */
	private String paramDesc;

	/**
	 * 表单类型：字典值
	 */
	private String formType;

	/**
	 * 表单值
	 */
	private String formValue;

	/**
	 * 参数扩展
	 */
	private String paramExt;

	/**
	 * 机构主键(预留)
	 */
	private String organizationId;

	/**
	 * 是否模板(预留)
	 */
	private Byte isTemplate;

	/**
	 * 排序
	 */
	private Long orders;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getParamDesc() {
		return paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormValue() {
		return formValue;
	}

	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}

	public String getParamExt() {
		return paramExt;
	}

	public void setParamExt(String paramExt) {
		this.paramExt = paramExt;
	}

	public String getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}

	public Byte getIsTemplate() {
		return isTemplate;
	}

	public void setIsTemplate(Byte isTemplate) {
		this.isTemplate = isTemplate;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", pid=").append(pid);
		sb.append(", type=").append(type);
		sb.append(", paramCode=").append(paramCode);
		sb.append(", paramName=").append(paramName);
		sb.append(", paramValue=").append(paramValue);
		sb.append(", paramDesc=").append(paramDesc);
		sb.append(", formType=").append(formType);
		sb.append(", formValue=").append(formValue);
		sb.append(", paramExt=").append(paramExt);
		sb.append(", organizationId=").append(organizationId);
		sb.append(", isTemplate=").append(isTemplate);
		sb.append(", orders=").append(orders);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		UpmsConfig other = (UpmsConfig) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getParamCode() == null ? other.getParamCode() == null
						: this.getParamCode().equals(other.getParamCode()))
				&& (this.getParamName() == null ? other.getParamName() == null
						: this.getParamName().equals(other.getParamName()))
				&& (this.getParamValue() == null ? other.getParamValue() == null
						: this.getParamValue().equals(other.getParamValue()))
				&& (this.getParamDesc() == null ? other.getParamDesc() == null
						: this.getParamDesc().equals(other.getParamDesc()))
				&& (this.getFormType() == null ? other.getFormType() == null
						: this.getFormType().equals(other.getFormType()))
				&& (this.getFormValue() == null ? other.getFormValue() == null
						: this.getFormValue().equals(other.getFormValue()))
				&& (this.getParamExt() == null ? other.getParamExt() == null
						: this.getParamExt().equals(other.getParamExt()))
				&& (this.getOrganizationId() == null ? other.getOrganizationId() == null
						: this.getOrganizationId().equals(other.getOrganizationId()))
				&& (this.getIsTemplate() == null ? other.getIsTemplate() == null
						: this.getIsTemplate().equals(other.getIsTemplate()))
				&& (this.getOrders() == null ? other.getOrders() == null : this.getOrders().equals(other.getOrders()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getParamCode() == null) ? 0 : getParamCode().hashCode());
		result = prime * result + ((getParamName() == null) ? 0 : getParamName().hashCode());
		result = prime * result + ((getParamValue() == null) ? 0 : getParamValue().hashCode());
		result = prime * result + ((getParamDesc() == null) ? 0 : getParamDesc().hashCode());
		result = prime * result + ((getFormType() == null) ? 0 : getFormType().hashCode());
		result = prime * result + ((getFormValue() == null) ? 0 : getFormValue().hashCode());
		result = prime * result + ((getParamExt() == null) ? 0 : getParamExt().hashCode());
		result = prime * result + ((getOrganizationId() == null) ? 0 : getOrganizationId().hashCode());
		result = prime * result + ((getIsTemplate() == null) ? 0 : getIsTemplate().hashCode());
		result = prime * result + ((getOrders() == null) ? 0 : getOrders().hashCode());
		return result;
	}

}