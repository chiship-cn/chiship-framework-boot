package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.pojo.vo.UpmsNoticeSendVo;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.ObjectUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.upms.biz.system.mapper.UpmsNoticeSendMapper;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSend;
import cn.chiship.framework.upms.biz.system.entity.UpmsNoticeSendExample;
import cn.chiship.framework.upms.biz.system.service.UpmsNoticeSendService;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;

/**
 * 用户通告阅读标记表业务接口实现层 2022/2/4
 *
 * @author lijian
 */
@Service
public class UpmsNoticeSendServiceImpl extends BaseServiceImpl<UpmsNoticeSend, UpmsNoticeSendExample>
		implements UpmsNoticeSendService {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsNoticeSendServiceImpl.class);

	@Resource
	UpmsNoticeSendMapper upmsNoticeSendMapper;

	@Override
	public BaseResult pageProfitDetails(HashMap<String, Object> map) {
		Long current = (Long) map.get("page");
		Long limit = (Long) map.get("limit");
		Page page = PageMethod.startPage(current.intValue(), limit.intValue());
		PageVo pageVo = new PageVo();
		pageVo.setCurrent(current);
		pageVo.setSize(limit);
		List<UpmsNoticeSendVo> records = upmsNoticeSendMapper.selectProfitDetails(map);
		pageVo.setRecords(records);
		pageVo.setTotal(page.getTotal());
		pageVo.setPages((long) page.getPages());
		return BaseResult.ok(pageVo);
	}

	@Override
	@Transactional(rollbackFor = Exception.class)
	public BaseResult selectDetailsByPrimaryKey(Object id) {
		HashMap<String, Object> map = Maps.newHashMapWithExpectedSize(7);
		map.put("id", id);
		List<UpmsNoticeSendVo> noticeSendVos = upmsNoticeSendMapper.selectProfitDetails(map);
		if (noticeSendVos.isEmpty()) {
			return BaseResult.error("不存在的消息！");
		}
		UpmsNoticeSendVo upmsNoticeSendVo = noticeSendVos.get(0);
		return BaseResult.ok(upmsNoticeSendVo);
	}

	@Override
	public BaseResult currentUserSetRead(Byte noticeType, String userId) {
		UpmsNoticeSendExample upmsNoticeSendExample = new UpmsNoticeSendExample();
		upmsNoticeSendExample.createCriteria().andUserIdEqualTo(userId).andNoticeTypeEqualTo(noticeType);
		UpmsNoticeSend upmsNoticeSend = new UpmsNoticeSend();
		upmsNoticeSend.setReadFlag(Byte.valueOf("1"));
		upmsNoticeSend.setReadTime(System.currentTimeMillis());
		upmsNoticeSendMapper.updateByExampleSelective(upmsNoticeSend, upmsNoticeSendExample);
		return BaseResult.ok();
	}

	@Override
	public BaseResult setAsRead(String id, String userId) {
		UpmsNoticeSend upmsNoticeSend = selectByPrimaryKey(id);
		if (ObjectUtil.isEmpty(upmsNoticeSend)) {
			return BaseResult.ok("不存在的消息");
		}
		if (!upmsNoticeSend.getUserId().equals(userId)) {
			return BaseResult.error("消息接收用户与当前登录用户不匹配！");
		}
		if (Byte.valueOf("1").equals(upmsNoticeSend.getReadFlag())) {
			return BaseResult.ok();
		}
		upmsNoticeSend.setReadFlag(Byte.valueOf("1"));
		upmsNoticeSend.setReadTime(System.currentTimeMillis());
		upmsNoticeSendMapper.updateByPrimaryKeySelective(upmsNoticeSend);
		return BaseResult.ok();
	}

}
