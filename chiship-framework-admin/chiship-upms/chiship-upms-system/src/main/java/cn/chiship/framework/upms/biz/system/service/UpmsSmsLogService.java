package cn.chiship.framework.upms.biz.system.service;

import cn.chiship.framework.upms.biz.system.pojo.dto.UpmsSmsLogDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLog;
import cn.chiship.framework.upms.biz.system.entity.UpmsSmsLogExample;
/**
 * 短信发送日志业务接口层
 * 2025/2/27
 * @author lijian
 */
public interface UpmsSmsLogService extends BaseService<UpmsSmsLog,UpmsSmsLogExample> {

    /**
     * 保存日志
     * @param upmsSmsLogDto
     * @return
     */
    BaseResult save(UpmsSmsLogDto upmsSmsLogDto);
}
