package cn.chiship.framework.upms.biz.system.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lijian
 */
@ApiModel(value = "参数配置新建表单")
public class UpmsConfigDto {

	@ApiModelProperty(value = "配置项编码", required = true)
	@NotEmpty(message = "配置项编码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String paramCode;

	@ApiModelProperty(value = "配置项名称", required = true)
	@NotEmpty(message = "配置项名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String paramName;

	@ApiModelProperty(value = "所属上级", required = true)
	@NotEmpty(message = "所属上级" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String pid;

	@ApiModelProperty(value = "配置项描述")
	@Length(max = 500)
	private String paramDesc;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	private Long orders;

	@ApiModelProperty(value = "表单类型", required = true)
	@NotNull(message = "表单类型" + BaseTipConstants.NOT_EMPTY)
	private String formType;

	@ApiModelProperty(value = "表单配置值")
	@Length(max = 200)
	private String formValue;

	@ApiModelProperty(value = "配置扩展字段")
	private String paramExt;

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getParamDesc() {
		return paramDesc;
	}

	public void setParamDesc(String paramDesc) {
		this.paramDesc = paramDesc;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	public String getFormType() {
		return formType;
	}

	public void setFormType(String formType) {
		this.formType = formType;
	}

	public String getFormValue() {
		return formValue;
	}

	public void setFormValue(String formValue) {
		this.formValue = formValue;
	}

	public String getParamExt() {
		return paramExt;
	}

	public void setParamExt(String paramExt) {
		this.paramExt = paramExt;
	}

}
