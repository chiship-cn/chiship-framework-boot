package cn.chiship.framework.upms.core.runner;

import cn.chiship.framework.upms.biz.base.service.UpmsSystemCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化数据
 *
 * @author lijian
 */
@Component
@Order(1)
public class UpmsSystemDataRunner implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(UpmsSystemDataRunner.class);

	@Resource
	UpmsSystemCacheService upmsSystemCacheService;

	@Override
	public void run(String... args) {
		LOGGER.info("----------------UpmsSystemRunner---------------------");

		upmsSystemCacheService.cacheConfigs();
		LOGGER.info("缓存系统配置数据结束！");

		upmsSystemCacheService.cacheProofs();
		LOGGER.info("缓存密钥数据结束！");

		upmsSystemCacheService.cacheDataDict();
		LOGGER.info("缓存数据字典数据结束！");

		upmsSystemCacheService.initQuartzJob();
		LOGGER.info("启动所有任务！");

		upmsSystemCacheService.cacheApp();
		LOGGER.info("缓存最新APP结束");

		upmsSystemCacheService.cacheCategoryDict();
		LOGGER.info("缓存数据分类字典结束");
	}

}
