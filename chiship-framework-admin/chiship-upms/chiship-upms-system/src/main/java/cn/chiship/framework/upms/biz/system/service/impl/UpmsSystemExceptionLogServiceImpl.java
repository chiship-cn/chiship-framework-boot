package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLogExample;
import cn.chiship.framework.upms.biz.system.mapper.UpmsSystemExceptionLogMapper;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemExceptionLogService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLog;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统异常日志业务接口实现层 2021/10/23
 *
 * @author lijian
 */
@Service
public class UpmsSystemExceptionLogServiceImpl
		extends BaseServiceImpl<UpmsSystemExceptionLog, UpmsSystemExceptionLogExample>
		implements UpmsSystemExceptionLogService {

	@Resource
	UpmsSystemExceptionLogMapper upmsSystemExceptionLogMapper;

	@Override
	public BaseResult getById(String id) {
		UpmsSystemExceptionLogExample systemExceptionLogExample = new UpmsSystemExceptionLogExample();
		systemExceptionLogExample.createCriteria().andIdEqualTo(id);
		List<UpmsSystemExceptionLog> systemExceptionLogs = upmsSystemExceptionLogMapper
				.selectByExampleWithBLOBs(systemExceptionLogExample);
		if (systemExceptionLogs.isEmpty()) {
			return BaseResult.ok(null);
		}
		return BaseResult.ok(systemExceptionLogs.get(0));
	}

}
