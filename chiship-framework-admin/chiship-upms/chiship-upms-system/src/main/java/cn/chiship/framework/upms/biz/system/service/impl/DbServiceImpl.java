package cn.chiship.framework.upms.biz.system.service.impl;

import cn.chiship.framework.upms.biz.system.service.DbService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.JdbcUtil;
import cn.chiship.sdk.third.properties.ChishipZfbMpProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lijian
 */
@Service
public class DbServiceImpl implements DbService {
    @Value("${spring.datasource.dynamic.datasource.master.driver-class-name}")
    private String driver;
    @Value("${spring.datasource.dynamic.datasource.master.url}")
    private String url;
    @Value("${spring.datasource.dynamic.datasource.slave.username}")
    private String username;
    @Value("${spring.datasource.dynamic.datasource.master.password}")
    private String password;
    @Value("${spring.datasource.dynamic.datasource.master.database}")
    private String database;
    @Value("${pagehelper.helper-dialect}")
    private String databaseType;
    @Override
    public BaseResult listTables() {
        try {
            JdbcUtil jdbcUtil = new JdbcUtil(driver, url, username, password);
            List<Map<String, Object>> tables = jdbcUtil.selectTablesByDatabase(database);
            jdbcUtil.release();
            Map<String, Object> result = new HashMap();
            result.put("tables", tables);
            result.put("databaseType", databaseType);
            return BaseResult.ok(result);
        } catch (Exception e) {
            return BaseResult.error(e.getMessage());
        }
    }

    @Override
    public BaseResult listColumns(String tableName) {
        try {
            JdbcUtil jdbcUtil = new JdbcUtil(driver, url, username, password);
            List<Map<String, Object>> columns = jdbcUtil.selectColumnsByTable(database,tableName);
            jdbcUtil.release();
            return BaseResult.ok(columns);
        } catch (Exception e) {
            return BaseResult.error(e.getMessage());
        }
    }
}
