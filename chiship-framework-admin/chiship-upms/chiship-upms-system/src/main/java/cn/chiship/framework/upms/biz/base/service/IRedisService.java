package cn.chiship.framework.upms.biz.base.service;

import cn.chiship.sdk.core.base.BaseResult;

/**
 * @author lj
 */
public interface IRedisService {

	/**
	 * Redis监控
	 * @return
	 */
	BaseResult monitor();

	/**
	 * 返回Redis键值树状结构
	 * @return
	 */
	BaseResult keyTrees();

	/**
	 * 根据键值获取对应数据
	 * @param key
	 * @return
	 */
	BaseResult value(String key);

}