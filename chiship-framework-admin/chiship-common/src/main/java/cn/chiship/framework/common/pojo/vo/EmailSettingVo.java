package cn.chiship.framework.common.pojo.vo;

import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * @author LiJian
 */
@ApiModel(value = "邮箱配置视图")
public class EmailSettingVo implements Serializable {

	private String id;

	private String subject;

	private String host;

	private String transportProtocol;

	private Byte smtpAuth;

	private Byte isSsl;

	private String smtpSslPort;

	private String smtpPort;

	private String sendFromAddress;

	private String sendFromAddressPwd;

	private Long emailId;

	private Byte state;

	private Byte type;

	private String outreachInfoId;

	private String outreachInfoName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getTransportProtocol() {
		return transportProtocol;
	}

	public void setTransportProtocol(String transportProtocol) {
		this.transportProtocol = transportProtocol;
	}

	public Byte getSmtpAuth() {
		return smtpAuth;
	}

	public void setSmtpAuth(Byte smtpAuth) {
		this.smtpAuth = smtpAuth;
	}

	public Byte getIsSsl() {
		return isSsl;
	}

	public void setIsSsl(Byte isSsl) {
		this.isSsl = isSsl;
	}

	public String getSmtpSslPort() {
		return smtpSslPort;
	}

	public void setSmtpSslPort(String smtpSslPort) {
		this.smtpSslPort = smtpSslPort;
	}

	public String getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(String smtpPort) {
		this.smtpPort = smtpPort;
	}

	public String getSendFromAddress() {
		return sendFromAddress;
	}

	public void setSendFromAddress(String sendFromAddress) {
		this.sendFromAddress = sendFromAddress;
	}

	public String getSendFromAddressPwd() {
		return sendFromAddressPwd;
	}

	public void setSendFromAddressPwd(String sendFromAddressPwd) {
		this.sendFromAddressPwd = sendFromAddressPwd;
	}

	public Long getEmailId() {
		return emailId;
	}

	public void setEmailId(Long emailId) {
		this.emailId = emailId;
	}

	public Byte getState() {
		return state;
	}

	public void setState(Byte state) {
		this.state = state;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getOutreachInfoId() {
		return outreachInfoId;
	}

	public void setOutreachInfoId(String outreachInfoId) {
		this.outreachInfoId = outreachInfoId;
	}

	public String getOutreachInfoName() {
		return outreachInfoName;
	}

	public void setOutreachInfoName(String outreachInfoName) {
		this.outreachInfoName = outreachInfoName;
	}

}