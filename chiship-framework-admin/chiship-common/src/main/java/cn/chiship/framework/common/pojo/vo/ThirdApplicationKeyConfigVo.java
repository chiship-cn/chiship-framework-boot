package cn.chiship.framework.common.pojo.vo;

import io.swagger.annotations.ApiModel;


/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-11
 */
@ApiModel(value = "三方应用配置视图")
public class ThirdApplicationKeyConfigVo {

    private String id;

    /**
     * 三方应用名称
     */
    private String name;

    /**
     * 项目标识
     */
    private String projectId;

    /**
     * 标识
     */
    private String identification;

    /**
     * 三方应用APP_ID
     */
    private String appId;

    /**
     * 三方应用Key
     */
    private String appKey;

    /**
     * 三方应用密钥
     */
    private String appSecret;

    /**
     * 服务器地址
     */
    private String serverUrl;

    /**
     * 请求Token
     */
    private String serverToken;

    /**
     * 描述
     */
    private String remark;

    /**
     * 微信类型 1 公众号 2 小程序 3 企业微信 4 钉钉
     */
    private Byte type;

    /**
     * 备用地址1
     */
    private String url1;

    /**
     * 备用地址2
     */
    private String url2;

    /**
     * 备用地址3
     */
    private String url3;

    /**
     * 备用数值
     */
    private String value1;

    /**
     * 备用数值
     */
    private String value2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getIdentification() {
        return identification;
    }

    public void setIdentification(String identification) {
        this.identification = identification;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Byte getType() {
        return type;
    }

    public void setType(Byte type) {
        this.type = type;
    }

    public String getUrl1() {
        return url1;
    }

    public void setUrl1(String url1) {
        this.url1 = url1;
    }

    public String getUrl2() {
        return url2;
    }

    public void setUrl2(String url2) {
        this.url2 = url2;
    }

    public String getUrl3() {
        return url3;
    }

    public void setUrl3(String url3) {
        this.url3 = url3;
    }

    public String getValue1() {
        return value1;
    }

    public void setValue1(String value1) {
        this.value1 = value1;
    }

    public String getValue2() {
        return value2;
    }

    public void setValue2(String value2) {
        this.value2 = value2;
    }
}