package cn.chiship.framework.common.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 系统日志表单
 *
 * @author lijian
 */
@ApiModel(value = "系统异常表单")
public class SystemExceptionLogDto implements Serializable {

	@ApiModelProperty(value = "当前用户主键")
	private String userId;

	@ApiModelProperty(value = "当前用户名")
	private String userName;

	@ApiModelProperty(value = "当前用户真实姓名")
	private String realName;

	@ApiModelProperty(value = "请求码")
	private String requestId;

	@ApiModelProperty(value = "当前IP")
	private String ip;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "基本路径")
	private String basePath;

	@ApiModelProperty(value = "全路径")
	private String uri;

	@ApiModelProperty(value = "请求方式")
	private String requestType;

	@ApiModelProperty(value = "参数")
	private String parameter;

	@ApiModelProperty(value = "浏览器标识")
	private String userAgent;

	@ApiModelProperty(value = "系统名称")
	private String systemName;

	@ApiModelProperty(value = "错误发生方法")
	private String method;

	@ApiModelProperty(value = "异常类")
	private String errorClass;

	@ApiModelProperty(value = "异常详情描述")
	private String errorLocalizedMessage;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getErrorClass() {
		return errorClass;
	}

	public void setErrorClass(String errorClass) {
		this.errorClass = errorClass;
	}

	public String getErrorLocalizedMessage() {
		return errorLocalizedMessage;
	}

	public void setErrorLocalizedMessage(String errorLocalizedMessage) {
		this.errorLocalizedMessage = errorLocalizedMessage;
	}

}