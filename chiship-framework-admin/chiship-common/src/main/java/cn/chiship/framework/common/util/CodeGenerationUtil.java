package cn.chiship.framework.common.util;

import cn.chiship.sdk.core.util.RandomUtil;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 编号生产工具
 *
 * @author lijian
 */
public class CodeGenerationUtil {

	private final static String FORMAT_CODE_FIVE = "00000";

	private static String date = new SimpleDateFormat("yyyyMMdd").format(new Date());

	private CodeGenerationUtil() {
	}

	/**
	 * 生成订单编号
	 * @param serialNumber 流水号
	 */
	public static String generateOrderId(long serialNumber) {
		return date + RandomUtil.number(4) + serialNumberFmt(serialNumber, FORMAT_CODE_FIVE);
	}

	private static String serialNumberFmt(long serialNumber, String fmt) {
		DecimalFormat dft = new DecimalFormat(fmt);
		String code = dft.format(serialNumber);
		return code;
	}

}
