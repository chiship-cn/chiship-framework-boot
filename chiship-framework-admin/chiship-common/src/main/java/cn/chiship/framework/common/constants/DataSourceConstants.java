package cn.chiship.framework.common.constants;

/**
 * 多数据源常量
 *
 * @author lijian
 */
public class DataSourceConstants {

	private DataSourceConstants() {
	}

	/**
	 * 主数据库
	 */
	public static final String MASTER = "master";

	/**
	 * 从数据库
	 */
	public static final String SLAVE = "slave";

}
