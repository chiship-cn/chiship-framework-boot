package cn.chiship.framework.common.pojo.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author lijian
 */
@ApiModel(value = "系统配置视图")
public class SystemConfigVo {

	private String id;

	@ApiModelProperty(value = "参数编号")
	private String paramCode;

	@ApiModelProperty(value = "参数名称")
	private String paramName;

	@ApiModelProperty(value = "参数值")
	private String paramValue;

	@ApiModelProperty(value = "备注")
	private String remark;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamName() {
		return paramName;
	}

	public void setParamName(String paramName) {
		this.paramName = paramName;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
}
