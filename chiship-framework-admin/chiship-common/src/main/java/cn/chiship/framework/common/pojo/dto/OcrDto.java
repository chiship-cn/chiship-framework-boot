package cn.chiship.framework.common.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author lj
 */
@ApiModel(value = "卡片OCR识别")
public class OcrDto {

	@ApiModelProperty(value = "Base64图片", required = true)
	private String base64Image;

	@ApiModelProperty(value = "卡片类型(1.身份证 2.银行卡 3.营业执照 4..护照 5.社保卡)", required = true)
	@NotNull(message = "卡片类型")
	@Min(1)
	@Max(6)
	private Integer type;

	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

}
