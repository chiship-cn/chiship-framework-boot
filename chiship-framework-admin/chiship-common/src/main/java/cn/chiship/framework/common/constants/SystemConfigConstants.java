package cn.chiship.framework.common.constants;

/**
 * @author lijian 系统配置常量
 */
public class SystemConfigConstants {

    private SystemConfigConstants() {
    }

    /**
     * 是否启用系统IP白名单
     */
    public static final String IS_SYSTEM_IP = "isSystemIp";

    /**
     * 系统IP白名单
     */
    public static final String SYSTEM_IP_WHITELIST = "systemIpWhitelist";

    /**
     * 是否启用
     */
    public static final String IS_USE_SMS = "isUseSms";

    /**
     * 短信厂家
     */
    public static final String SMS_TYPE = "smsType";

    /**
     * 短信ACCESS_KEY
     */

    public static final String SMS_ACCESS_KEY = "smsAccessKey";

    /**
     * 短信SECRET_KEY
     */

    public static final String SMS_SECRET_KEY = "smsSecretKey";

    /**
     * 签名内容
     */
    public static final String SIGN_CONTENT = "signContent";

    /**
     * 短信验证码模板
     */
    public static final String SMS_CODE_TEMPLATE = "smsCodeTemplate";

    /**
     * 邀请新用户
     */
    public static final String SMS_INVITE_USER_TEMPLATE = "smsInviteUserTemplate";

    /**
     * 有效时间(分钟)
     */
    public static final String EXPIRY_TIME = "expiryTime";

    /**
     * 存储位置
     */
    public static final String STORAGE_LOCATION = "storageLocation";

    /**
     * 初始密码
     */
    public static final String INIT_PASSWORD = "initPassword";

    /**
     * 默认角色
     */
    public static final String DEFAULT_ROLE = "defaultRole";

    /**
     * ACCESS_KEY
     */
    public static final String STORAGE_ACCESS_KEY = "storageAccessKey";

    /**
     * SECRET_KEY
     */
    public static final String STORAGE_SECRET_KEY = "storageSecretKey";

    /**
     * 空间域名
     */
    public static final String STORAGE_END_PORT = "storageEndPort";

    /**
     * 存储空间名称
     */
    public static final String STORAGE_BUCK_NAME = "storageBuckName";

    /**
     * 存储根名称
     */
    public static final String STORAGE_ROOT = "storageRoot";

}
