package cn.chiship.framework.common.config;

import cn.chiship.sdk.cache.service.UserCacheService;
import cn.chiship.sdk.cache.vo.CacheRoleVo;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.annotation.RequiredPermissions;
import cn.chiship.sdk.core.annotation.RequiredRoles;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.enums.PermissionsEnum;
import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.exception.custom.PermissionsException;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author lijian 权限校验
 */
@Component
public class UserPermissionsCheckService {


    @Resource
    UserCacheService userCacheService;

    public BaseResult checkOption(Method method, HttpServletRequest request) {
        if (method.getAnnotation(RequiredPermissions.class) != null
                || method.getAnnotation(RequiredRoles.class) != null) {

            CacheUserVO userVO = userCacheService.getUser();
            if (userVO == null) {
                return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "Token失效,无效用户？");
            }
            /**
             * 获得权限
             */
            List<String> perms = userVO.getPerms();
            if (method.getAnnotation(RequiredPermissions.class) != null) {
                RequiredPermissions requiredPermissions = method.getAnnotation(RequiredPermissions.class);
                String[] permsValue = requiredPermissions.value();
                if (permsValue.length == 0) {
                    return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "请配置所需要权限");
                }
                if (requiredPermissions.logical().equals(PermissionsEnum.AND)) {
                    if (!perms.containsAll(Arrays.asList(permsValue))) {
                        return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "接口【" + request.getRequestURI() + "】必须拥有"
                                + JSON.toJSONString(requiredPermissions.value()) + "权限!");

                    }
                } else {
                    Integer count = 0;
                    for (String permValue : permsValue) {
                        if (perms.contains(permValue)) {
                            count++;
                        }
                    }
                    if (count == 0) {
                        return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "接口【" + request.getRequestURI() + "】至少拥有"
                                + JSON.toJSONString(requiredPermissions.value()) + "其中一种权限!");
                    }
                }
            }
            /**
             * 获得角色
             */
            List<String> roles = new ArrayList<>();
            for (CacheRoleVo roleVo : userVO.getCacheRoleVos()) {
                roles.add(roleVo.getRoleCode());
            }
            if (method.getAnnotation(RequiredRoles.class) != null) {
                RequiredRoles requiredRoles = method.getAnnotation(RequiredRoles.class);
                String[] rolesValue = requiredRoles.value();
                if (rolesValue.length == 0) {
                    return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "请配置所需要角色！");
                }
                if (requiredRoles.logical().equals(PermissionsEnum.AND)) {
                    if (!roles.containsAll(Arrays.asList(rolesValue))) {
                        return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "接口【" + request.getRequestURI() + "】必须拥有"
                                + JSON.toJSONString(requiredRoles.value()) + "角色!");

                    }
                } else {
                    Integer count = 0;
                    for (String roleValue : rolesValue) {
                        if (roles.contains(roleValue)) {
                            count++;
                        }
                    }
                    if (count == 0) {
                        return BaseResult.error(BaseResultEnum.PERMISSIONS_ERROR, "接口【" + request.getRequestURI() + "】至少拥有"
                                + JSON.toJSONString(requiredRoles.value()) + "其中一种角色!");
                    }
                }
            }

        }
        return BaseResult.ok();
    }

}
