package cn.chiship.framework.common.enums;

/**
 * @author LiJian 业务类型
 */
public enum BusinessTypeEnum {

	/**
	 * 登录
	 */
	SYSTEM_OPTION_LOGIN("LOGIN", "登录"),

	/**
	 * 成功
	 */
	SYSTEM_OPTION_LOGOUT("LOGOUT", "退出"),

	/**
	 * 保存
	 */
	SYSTEM_OPTION_SAVE("SAVE", "保存"),
	/**
	 * 修改
	 */
	SYSTEM_OPTION_UPDATE("UPDATE", "修改"),
	/**
	 * 删除
	 */
	SYSTEM_OPTION_REMOVE("REMOVE", "删除"),

	/**
	 * 查找
	 */
	SYSTEM_OPTION_SEARCH("SEARCH", "查找"),

	/**
	 * 其他
	 */
	SYSTEM_OPTION_OTHER("OTHER ", "其他"),
	/**
	 * 导出
	 */
	SYSTEM_OPTION_EXPORT("EXPORT ", "导出"),

	/**
	 * 导入
	 */
	SYSTEM_OPTION_IMPORT("IMPORT ", "导入"),

	/**
	 * 强退
	 */
	SYSTEM_OPTION_FORCE("FORCE ", "强退"),

	/**
	 * 清空数据
	 */
	SYSTEM_OPTION_CLEAN("CLEAN ", "清空数据");

	/**
	 * 业务编号
	 */
	private String code;

	/**
	 * 业务描述
	 */
	private String message;

	BusinessTypeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
