package cn.chiship.framework.common.constants;

/**
 * @author lijian 缓存常量
 */
public class CommonCacheConstants {

	public static final String REDIS_COMMON_PROJECT_MEMBER_NAME = CommonConstants.COMMON_PROJECT_NAME + "-member";

	public static final String REDIS_QRCODE_LOGIN_PREFIX = "QRCODE_LOGIN";

	/**
	 * 验证码
	 */
	public static final String REDIS_SMS_CODE_PREFIX = "SMS_CODE";

	/**
	 * 分类字典
	 */
	public static final String REDIS_CATEGORY_DICT_PREFIX = "CATEGORY_DICT";

	/**
	 * 缓存地区条目
	 */
	public static final String REDIS_REGION_ITEM_PREFIX = "REGION_ITEM";

	/**
	 * 缓存APP版本
	 */
	public static final String REDIS_APP_VERSION_PREFIX = "APP_VERSION";

	/**
	 * 缓存已经删除的文件
	 */
	public static final String REDIS_REMOVE_FILE_UUID = "REMOVE_FILE_UUID";

	/**
	 * 缓存广告数据
	 */
	public static final String REDIS_AD_DATA = "REDIS_AD_DATA";

	/**
	 * 缓存已经删除的文件
	 */
	public static final String REDIS_CHUNK_IDENTIFIER = "UPLOAD_CHUNK_IDENTIFIER";

	public static final String REDIS_WECHAT_USER_LOCATION_PREFIX = "WECHAT_USER_LOCATION";

	/**
	 * 超时关闭订单
	 */
	public static final String REDIS_TIMEOUT_CLOSED_ORDER_PREFIX = "TIMEOUT_CLOSED_ORDER";

	/**
	 * 订单实时状态
	 */
	public static final String REDIS_ORDER_STATUS_PREFIX = "ORDER_STATUS";


	/**
	 * 商品分类字典
	 */
	public static final String REDIS_PRODUCT_CATEGORY_PREFIX = "PRODUCT_CATEGORY";

	public static String buildKey() {
		return buildKey(null);
	}

	public static String buildKey(String key) {
		return CommonConstants.COMMON_PROJECT_NAME + ":" + (key == null ? "" : key);
	}

}
