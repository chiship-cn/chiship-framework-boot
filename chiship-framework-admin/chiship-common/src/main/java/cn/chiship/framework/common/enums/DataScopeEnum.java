package cn.chiship.framework.common.enums;

/**
 * @author lijian
 */

public enum DataScopeEnum {

	/**
	 * 全部数据权限
	 */
	DATA_SCOPE_ALL(Byte.valueOf("1")),
	/**
	 * 自定数据权限
	 */
	DATA_SCOPE_CUSTOM(Byte.valueOf("2")),

	/**
	 * 单位数据权限
	 */
	DATA_SCOPE_ORG(Byte.valueOf("3")),

	/**
	 * 单位及以下数据权限
	 */
	DATA_SCOPE_ORG_AND_CHILD(Byte.valueOf("4")),

	/**
	 * 仅本人数据权限
	 */
	DATA_SCOPE_SELF(Byte.valueOf("7")),;

	private Byte type;

	DataScopeEnum(Byte type) {
		this.type = type;
	}

	public Byte getType() {
		return type;
	}

}
