package cn.chiship.framework.common.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * 系统日志表单
 *
 * @author lijian
 */
@ApiModel(value = "系统日志表单")
public class SystemOptionLogDto implements Serializable {

	@ApiModelProperty(value = "当前用户主键")
	private String userId;

	@ApiModelProperty(value = "当前用户名")
	private String userName;

	@ApiModelProperty(value = "当前用户真实姓名")
	private String realName;

	@ApiModelProperty(value = "操作类型")
	private String optionType;

	@ApiModelProperty(value = "当前IP")
	private String ip;

	@ApiModelProperty(value = "描述")
	private String description;

	@ApiModelProperty(value = "开始时间")
	private Long startTime;

	@ApiModelProperty(value = "花费时间")
	private Integer spendTime;

	@ApiModelProperty(value = "基本路径")
	private String basePath;

	@ApiModelProperty(value = "全路径")
	private String uri;

	@ApiModelProperty(value = "请求方式")
	private String requestType;

	@ApiModelProperty(value = "参数")
	private String parameter;

	@ApiModelProperty(value = "浏览器标识")
	private String userAgent;

	@ApiModelProperty(value = "结果")
	private String result;

	@ApiModelProperty(value = "系统名称")
	private String systemName;

	@ApiModelProperty(value = "错误发生方法")
	private String method;

	@ApiModelProperty(value = "操作员类型")
	private String operatorType;

	@ApiModelProperty(value = "扩展属性")
	private String extAttribute;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getOptionType() {
		return optionType;
	}

	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Integer getSpendTime() {
		return spendTime;
	}

	public void setSpendTime(Integer spendTime) {
		this.spendTime = spendTime;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getOperatorType() {
		return operatorType;
	}

	public void setOperatorType(String operatorType) {
		this.operatorType = operatorType;
	}

	public String getExtAttribute() {
		return extAttribute;
	}

	public void setExtAttribute(String extAttribute) {
		this.extAttribute = extAttribute;
	}

}