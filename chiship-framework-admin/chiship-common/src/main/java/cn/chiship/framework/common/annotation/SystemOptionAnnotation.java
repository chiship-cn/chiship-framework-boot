package cn.chiship.framework.common.annotation;

import cn.chiship.framework.common.constants.CommonConstants;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.enums.UserTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 在Controller的方法上使用此注解，该方法在映射时,拦截日志
 * @author lj
 */
@Target({ ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
public @interface SystemOptionAnnotation {

	/**
	 * 操作业务
	 * @return
	 */
	BusinessTypeEnum option() default BusinessTypeEnum.SYSTEM_OPTION_SEARCH;

	/**
	 * 系统名称
	 * @return
	 */
	String systemName() default CommonConstants.SYSTEM_NAME_UPMS;

	/**
	 * 操作人类别
	 */
	UserTypeEnum operatorType() default UserTypeEnum.ADMIN;

	/**
	 * 具体业务描述
	 * @return
	 */
	String describe();

}
