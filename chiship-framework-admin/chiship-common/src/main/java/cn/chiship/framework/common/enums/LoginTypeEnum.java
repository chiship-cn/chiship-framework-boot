package cn.chiship.framework.common.enums;

/**
 * 登录类别
 *
 * @author lijian
 */
public enum LoginTypeEnum {

    /**
     * PC
     */
    LOGIN_PC("pc", "PC登录"),

    /**
     * Scan
     */
    LOGIN_SCAN("scan", "扫码登录"),

    /**
     * APP
     */
    LOGIN_APP("app", "APP登录"),

    /**
     * 钉钉授权登录
     */
    LOGIN_DING_TALK("dingTalk", "钉钉授权"),

    /**
     * 企业微信授权登录
     */
    LOGIN_WX_WORK("wxWork", "企业微信授权"),
    ;

    /**
     * 业务编号
     */
    private String code;

    /**
     * 业务描述
     */
    private String message;

    LoginTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
