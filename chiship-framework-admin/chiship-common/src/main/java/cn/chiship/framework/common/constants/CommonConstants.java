package cn.chiship.framework.common.constants;

import cn.chiship.framework.common.util.FrameworkUtil2;

/**
 * @author lijian 常用常量
 */
public class CommonConstants {

	public static final String COMMON_PROJECT_NAME = FrameworkUtil2.getChishipDefaultProperties().getName();

	public static final String COMMON_PROJECT_NAME_DESC = FrameworkUtil2.getChishipDefaultProperties().getTitle();

	public static final Long PROVINCE_PID = 100000L;

	public static final String SYSTEM_NAME_UPMS = "权限系统";

	public static final String SYSTEM_NAME_DOCS = "文档系统";

	public static final String SYSTEM_NAME_WECHAT = "公众号系统";

}
