package cn.chiship.framework.common.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author lijian
 */
@ConfigurationProperties(FrameworkProperties.PREFIX)
public class FrameworkProperties {

	public static final String PREFIX = "chiship";

	/**
	 * 订单超时时间,默认10分钟
	 */
	private Integer orderTimeout = 10;

	public Integer getOrderTimeout() {
		return orderTimeout;
	}

	public void setOrderTimeout(Integer orderTimeout) {
		this.orderTimeout = orderTimeout;
	}

}
