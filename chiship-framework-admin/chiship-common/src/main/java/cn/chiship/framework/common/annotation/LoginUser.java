package cn.chiship.framework.common.annotation;

import java.lang.annotation.*;

/**
 * 获取当前登录用户注解,在方法参数使用此注解，可获取当前登录用户
 *
 * @author lijian
 */
@Documented
@Target({ ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginUser {

}