package cn.chiship.framework.common.pojo.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Maps;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Entity基类
 *
 * @author lj
 */
@ApiModel(value = "基本查询条件")
public class BaseQueryEntityDto implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "请求参数")
	private Map<String, Object> params;

	@ApiModelProperty(value = "用户主键集合")
	private List<String> userIds;

	@ApiModelProperty(value = "部门主键集合")
	private List<String> orgIds;

	@ApiModelProperty(value = "是否所有（最大权限）")
	private Boolean dataScopeAll;

	public Map<String, Object> getParams() {
		if (params == null) {
			params = Maps.newHashMapWithExpectedSize(7);
		}
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public List<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	public List<String> getOrgIds() {
		return orgIds;
	}

	public void setOrgIds(List<String> orgIds) {
		this.orgIds = orgIds;
	}

	public Boolean getDataScopeAll() {
		if (dataScopeAll == null) {
			dataScopeAll = true;
		}
		return dataScopeAll;
	}

	public void setDataScopeAll(Boolean dataScopeAll) {
		this.dataScopeAll = dataScopeAll;
	}

}
