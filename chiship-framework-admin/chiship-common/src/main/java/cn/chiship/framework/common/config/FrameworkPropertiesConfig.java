package cn.chiship.framework.common.config;

import cn.chiship.framework.common.properties.FrameworkProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lijian
 */
@Configuration
@EnableConfigurationProperties(value = { FrameworkProperties.class })
public class FrameworkPropertiesConfig {

	@Bean
	public FrameworkProperties frameworkProperties() {
		return new FrameworkProperties();
	}

}
