package cn.chiship.framework.common.enums;

/**
 * @author lijian 用户二级类型枚举
 */

public enum UserSecondTypeEnum {

	/**
	 * 后台用户
	 */
	ADMIN(Byte.valueOf("0"), "后台用户"),

	/**
	 * 前台用户
	 */
	MEMBER(Byte.valueOf("1"), "前台用户"),
	/**
	 * 其他
	 */
	OTHER(Byte.valueOf("2"), "其他"),;

	/**
	 * 类型
	 */
	private Byte type;

	/**
	 * 描述
	 */
	private String desc;

	private UserSecondTypeEnum(Byte type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public Byte getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
