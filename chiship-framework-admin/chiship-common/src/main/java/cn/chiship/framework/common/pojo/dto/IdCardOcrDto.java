package cn.chiship.framework.common.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author lj
 */
@ApiModel(value = "身份证识别OCR")
public class IdCardOcrDto {

	@ApiModelProperty(value = "Base64图片", required = true)
	private String base64Image;

	@ApiModelProperty(value = "是否正面照", required = true)
	private Boolean isFront;

	public String getBase64Image() {
		return base64Image;
	}

	public void setBase64Image(String base64Image) {
		this.base64Image = base64Image;
	}

	public Boolean getIsFront() {
		return isFront;
	}

	public void setIsFront(Boolean isFront) {
		this.isFront = isFront;
	}
}
