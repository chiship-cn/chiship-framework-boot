package cn.chiship.framework.common.pojo.vo;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.StringUtil;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author lijian
 */
public class ConfigJson {

	private final Map<String, Object> map;

	public ConfigJson() {
		this(16, false);
	}

	public ConfigJson(int initialCapacity, boolean ordered) {
		if (ordered) {
			this.map = new LinkedHashMap(initialCapacity);
		}
		else {
			this.map = new HashMap(initialCapacity);
		}
	}

	public Object put(String key, Object value) {
		return this.map.put(key, value);
	}

	public String getString(String key) {
		Object value = this.get(key);
		return value == null ? null : value.toString();
	}

	public Object get(Object key) {
		Object val = this.map.get(key);
		if (val == null && (key instanceof Number || key instanceof Character || key instanceof Boolean
				|| key instanceof UUID)) {
			val = this.map.get(key.toString());
		}
		if (StringUtil.isNull(val)) {
			throw new BusinessException("参数配置缺少【" + key + "】配置项");
		}
		return val;
	}

}
