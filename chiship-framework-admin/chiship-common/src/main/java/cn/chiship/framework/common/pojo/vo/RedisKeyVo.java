package cn.chiship.framework.common.pojo.vo;

import java.util.List;

/**
 * @author lijian
 */
public class RedisKeyVo {

	private String key;

	private String path;

	private String type;

	private List<RedisKeyVo> children;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<RedisKeyVo> getChildren() {
		return children;
	}

	public void setChildren(List<RedisKeyVo> children) {
		this.children = children;
	}

}
