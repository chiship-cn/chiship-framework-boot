package cn.chiship.framework.common.util;

import cn.chiship.framework.common.constants.CommonConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import io.swagger.annotations.Api;
import io.swagger.models.auth.In;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ScalarType;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import org.springframework.http.HttpMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian Swagger工具
 */
public class SwaggerUtil {

	public static List globalResponseMessage() {
		List<Response> globalResponses = new ArrayList<>();
		for (BaseResultEnum item : BaseResultEnum.values()) {
			globalResponses.add(
					new ResponseBuilder().code(String.valueOf(item.getCode())).description(item.getMessage()).build());
		}
		return globalResponses;
	}

	/**
	 * 公共人员西悉尼
	 * @return
	 */
	public static ApiInfo buildCommonInfo(String title, String version) {
		return new ApiInfoBuilder().title(CommonConstants.COMMON_PROJECT_NAME_DESC + "●" + title)
				.termsOfServiceUrl("http://www.chiship.cn")
				.contact(new Contact("李剑", "http://www.chiship.cn", "li1991-hello@163.com")).version(version).build();
	}

	public static Docket buildDocket(String groupName, String title, String version, String path) {
		return new Docket(DocumentationType.OAS_30)
				.apiInfo(buildCommonInfo(title, version))
				.groupName(groupName)
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				// .apis(RequestHandlerSelectors.basePackage(path))
				.paths(PathSelectors.any()).build().globalResponses(HttpMethod.GET, globalResponseMessage())
				.globalResponses(HttpMethod.POST, globalResponseMessage())
				.globalResponses(HttpMethod.PUT, globalResponseMessage())
				.globalResponses(HttpMethod.DELETE, globalResponseMessage())
				.globalRequestParameters(generateRequestParameters());

	}

	private static List<RequestParameter> generateRequestParameters() {
		List<RequestParameter> parameters = new ArrayList();
		parameters.add(new RequestParameterBuilder().name("ProjectsId").description("ProjectsId")
				.in(In.HEADER.toValue()).required(true)
				.query(builder -> builder.defaultValue("1883442361").model(m -> m.scalarModel(ScalarType.STRING)))
				.build());
		parameters.add(new RequestParameterBuilder().name("App-Id").description("appId").in(In.HEADER.toValue())
				.required(true).query(builder -> builder.defaultValue("602394140839161856")
						.model(m -> m.scalarModel(ScalarType.STRING)))
				.build());
		parameters.add(new RequestParameterBuilder()
				.name("App-Key").description("appKey").in(In.HEADER.toValue()).required(true).query(builder -> builder
						.defaultValue("e027565ee9954929a41c152deecdf1c1").model(m -> m.scalarModel(ScalarType.STRING)))
				.build());
		parameters.add(new RequestParameterBuilder().name("Sign").description("参数签名(RSA私钥签名或MD5参数加密)")
				.in(In.HEADER.toValue()).required(false)
				.query(builder -> builder.defaultValue("MD5=").model(m -> m.scalarModel(ScalarType.STRING))).build());
		parameters.add(new RequestParameterBuilder().name("Access-Token").description("登录令牌").in(In.HEADER.toValue())
				.required(false)
				.query(builder -> builder.defaultValue("1883442361").model(m -> m.scalarModel(ScalarType.STRING)))
				.build());
		return parameters;
	}

}
