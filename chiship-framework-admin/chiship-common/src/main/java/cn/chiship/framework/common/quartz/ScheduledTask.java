package cn.chiship.framework.common.quartz;

import java.util.concurrent.ScheduledFuture;

/**
 * @author lijian
 */
public final class ScheduledTask {

	volatile ScheduledFuture<?> future;

	/**
	 * 取消定时任务
	 */
	public void cancel() {
		ScheduledFuture<?> future = this.future;
		if (future != null) {
			future.cancel(true);
		}
	}

}