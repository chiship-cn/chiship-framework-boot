package cn.chiship.framework.common.service;

import cn.chiship.framework.common.pojo.vo.ConfigJson;
import cn.chiship.framework.common.pojo.vo.EmailSettingVo;
import cn.chiship.framework.common.pojo.vo.SystemConfigVo;
import cn.chiship.framework.common.pojo.vo.ThirdApplicationKeyConfigVo;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.third.core.model.WeiXinConfigModel;

import java.util.List;

/**
 * 全局缓存服务接口
 *
 * @author lijian
 */
public interface GlobalCacheService {

    /**
     * 校验登录IP
     *
     * @param ip
     * @return
     */
    BaseResult validateLoginIp(String ip);

    /**
     * 获取邮箱配置
     *
     * @return UpmsEmailSettingVo
     */
    EmailSettingVo getEmailSettingConfig();

    /**
     * 获得私钥
     *
     * @return
     */
    String getRsaPrivateKey();

    /**
     * 获得公钥
     *
     * @return
     */
    String getRsaPublicKey();

    /**
     * 根据Key获得配置项
     *
     * @param key
     * @return
     */
    SystemConfigVo getSystemConfig(String key);

    /**
     * 根据Key集合获得配置项
     *
     * @param keys
     * @return
     */
    List<SystemConfigVo> getSystemConfig(List<String> keys);

    /**
     * 根据Key集合获得配置项
     *
     * @param keys
     * @return
     */
    ConfigJson getSystemConfigJson(List<String> keys);

    /**
     * 根据Key获得配置项
     *
     * @param key
     * @return
     */
    ConfigJson getSystemConfigJson(String key);

    /**
     * rsa解密
     *
     * @param value
     * @param title
     * @return
     */
    String rsaDecrypt(String value, String title);

    /**
     * 校验验证码(设为已使用)
     *
     * @param codeDevice 设备号码
     * @param code       验证码
     * @return BaseResult
     */
    BaseResult verificationSmsCode(String codeDevice, String code);


    /**
     * 获取三方应用配置信息
     *
     * @param appId 应用标识
     * @return ThirdApplicationKeyConfigVo
     */
    ThirdApplicationKeyConfigVo getThirdApplicationConfigByAppId(String appId);

    /**
     * 获取微信配置实体
     *
     * @param appId
     * @return WeiXinConfigModel
     */
    WeiXinConfigModel getWeiXinConfigByAppId(String appId);



}
