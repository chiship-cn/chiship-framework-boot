package cn.chiship.framework.common.pojo.vo;

import java.util.List;

/**
 * @author lj
 */
public class ExportDataResult {

	/**
	 * 标题
	 */
	private List<String> labels;

	/**
	 * 数值集合
	 */
	private List<List<String>> values;

	/**
	 * 总数
	 */
	private Integer total;

	public ExportDataResult(List<String> labels, List<List<String>> values, Integer total) {
		this.labels = labels;
		this.values = values;
		this.total = total;
	}

	public List<String> getLabels() {
		return labels;
	}

	public void setLabels(List<String> labels) {
		this.labels = labels;
	}

	public List<List<String>> getValues() {
		return values;
	}

	public void setValues(List<List<String>> values) {
		this.values = values;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

}
