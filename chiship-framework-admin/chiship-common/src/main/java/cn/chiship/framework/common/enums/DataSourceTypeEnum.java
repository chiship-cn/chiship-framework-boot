package cn.chiship.framework.common.enums;

/**
 * @author lijian
 */

public enum DataSourceTypeEnum {

	/**
	 * MYSQL57
	 */
	DB_TYPE_MYSQL_57("02:MYSQL57+"),

	;

	/**
	 * 数据库编号
	 */
	private String type;

	DataSourceTypeEnum(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

}
