package cn.chiship.framework.business.biz.cashier.enums;

/**
 * 订单状态
 *
 * @author lijian
 */
public enum OrderStatusEnum {

	/**
	 * 待付款
	 */
	ORDER_STATUS_WAIT_PAY(Byte.valueOf("0"), "待付款"),

	/**
	 * 付款失败
	 */
	ORDER_STATUS_PAY_FAIL(Byte.valueOf("1"), "付款失败"),

	/**
	 * 已付款
	 */
	ORDER_STATUS_PAY_SUCCESS(Byte.valueOf("2"), "已付款"),

	/**
	 * 已关闭
	 */
	ORDER_STATUS_CLOSE(Byte.valueOf("3"), "已关闭"),;

	/**
	 * 业务编号
	 */
	private Byte status;

	/**
	 * 业务描述
	 */
	private String message;

	OrderStatusEnum(Byte status, String message) {
		this.status = status;
		this.message = message;
	}

	/**
	 * 根据标识返回枚举
	 * @param status 标识
	 * @return TradeTypeEnum
	 */
	public static OrderStatusEnum getOrderStatusEnum(Byte status) {
		OrderStatusEnum[] orderStatusEnums = values();
		for (OrderStatusEnum orderStatusEnum : orderStatusEnums) {
			if (orderStatusEnum.getStatus().equals(status)) {
				return orderStatusEnum;
			}
		}
		throw new RuntimeException("无效的订单状态！");
	}

	public Byte getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}
