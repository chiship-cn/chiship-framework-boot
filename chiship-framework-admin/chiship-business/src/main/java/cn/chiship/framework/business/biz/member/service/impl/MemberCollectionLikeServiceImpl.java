package cn.chiship.framework.business.biz.member.service.impl;

import cn.chiship.framework.business.biz.base.service.impl.BusinessAsyncService;
import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeEnum;
import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeModuleEnum;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeDto;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeIsAddDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.member.mapper.MemberCollectionLikeMapper;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLike;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLikeExample;
import cn.chiship.framework.business.biz.member.service.MemberCollectionLikeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收藏点赞业务接口实现层 2024/11/21
 *
 * @author lijian
 */
@Service
public class MemberCollectionLikeServiceImpl extends BaseServiceImpl<MemberCollectionLike, MemberCollectionLikeExample>
		implements MemberCollectionLikeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberCollectionLikeServiceImpl.class);

	@Resource
	MemberCollectionLikeMapper memberCollectionLikeMapper;

	@Resource
	BusinessAsyncService businessAsyncService;

	@Override
	public BaseResult add(MemberCollectionLikeDto memberCollectionLikeDto, CacheUserVO cacheUserVO) {
		String modelId = memberCollectionLikeDto.getModuleId();
		MemberCollectionLikeModuleEnum favoriteLikeModuleEnum = memberCollectionLikeDto.getModuleEnum();

		MemberCollectionLikeExample collectionLikeExample = new MemberCollectionLikeExample();
		collectionLikeExample.createCriteria().andModuleEqualTo(favoriteLikeModuleEnum.getType())
				.andModuleIdEqualTo(modelId).andTypeEqualTo(memberCollectionLikeDto.getTypeEnum().getType())
				.andCreatedUserIdEqualTo(cacheUserVO.getId());
		if (memberCollectionLikeMapper.countByExample(collectionLikeExample) > 0) {
			return BaseResult.error("您已添加至收藏！");
		}
		String userName = cacheUserVO.getNickName();
		if (StringUtil.isNullOrEmpty(userName)) {
			userName = cacheUserVO.getMobile();
		}
		MemberCollectionLike memberCollectionLike = new MemberCollectionLike();
		memberCollectionLike.setModuleId(modelId);
		memberCollectionLike.setModule(favoriteLikeModuleEnum.getType());
		memberCollectionLike.setType(memberCollectionLikeDto.getTypeEnum().getType());
		memberCollectionLike.setCreatedUserId(cacheUserVO.getId());
		memberCollectionLike.setCreatedUserName(userName);
		memberCollectionLike.setCreatedUserAvatar(cacheUserVO.getAvatar());
		super.insertSelective(memberCollectionLike);
		businessAsyncService.updateModuleCollectLike(memberCollectionLikeDto, Boolean.TRUE);
		return BaseResult.ok();
	}

	@Override
	public BaseResult cancel(MemberCollectionLikeDto memberCollectionLikeDto, CacheUserVO cacheUserVO) {
		String modelId = memberCollectionLikeDto.getModuleId();
		MemberCollectionLikeModuleEnum favoriteLikeModuleEnum = memberCollectionLikeDto.getModuleEnum();

		MemberCollectionLikeExample collectionLikeExample = new MemberCollectionLikeExample();
		collectionLikeExample.createCriteria().andModuleEqualTo(favoriteLikeModuleEnum.getType())
				.andModuleIdEqualTo(modelId).andTypeEqualTo(memberCollectionLikeDto.getTypeEnum().getType())
				.andCreatedUserIdEqualTo(cacheUserVO.getId());
		deleteByExample(collectionLikeExample);
		businessAsyncService.updateModuleCollectLike(memberCollectionLikeDto, Boolean.FALSE);
		return BaseResult.ok();
	}

	@Override
	public BaseResult isAdd(MemberCollectionLikeIsAddDto memberCollectionLikeIsAddDto, CacheUserVO cacheUserVO) {
		String modelId = memberCollectionLikeIsAddDto.getModuleId();
		MemberCollectionLikeModuleEnum favoriteLikeModuleEnum = memberCollectionLikeIsAddDto.getModuleEnum();

		MemberCollectionLikeExample collectionLikeExample = new MemberCollectionLikeExample();
		collectionLikeExample.createCriteria().andModuleEqualTo(favoriteLikeModuleEnum.getType())
				.andModuleIdEqualTo(modelId).andCreatedUserIdEqualTo(cacheUserVO.getId());
		List<MemberCollectionLike> memberCollectionLikes = memberCollectionLikeMapper
				.selectByExample(collectionLikeExample);

		Map<String, Boolean> map = new HashMap<>(7);
		boolean collect = false;
		boolean like = false;
		for (MemberCollectionLike memberCollectionLike : memberCollectionLikes) {
			if (memberCollectionLike.getType().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_SC.getType())) {
				collect = true;
			}
			if (memberCollectionLike.getType().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_DZ.getType())) {
				like = true;
			}
		}
		map.put("collect", collect);
		map.put("like", like);
		return BaseResult.ok(map);
	}

}
