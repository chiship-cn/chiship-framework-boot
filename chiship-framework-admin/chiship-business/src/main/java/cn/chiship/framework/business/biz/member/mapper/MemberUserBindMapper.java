package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberUserBind;
import cn.chiship.framework.business.biz.member.entity.MemberUserBindExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface MemberUserBindMapper extends BaseMapper<MemberUserBind, MemberUserBindExample> {

}