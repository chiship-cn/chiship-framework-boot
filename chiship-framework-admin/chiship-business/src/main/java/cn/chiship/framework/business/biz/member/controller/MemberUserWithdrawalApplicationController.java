package cn.chiship.framework.business.biz.member.controller;


import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserWithdrawalExamineDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberUserWithdrawalApplicationService;
import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplication;
import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplicationExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 提现申请控制层
 * 2024/12/17
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/memberUserWithdrawalApplication")
@Api(tags = "提现申请")
public class MemberUserWithdrawalApplicationController extends BaseController<MemberUserWithdrawalApplication, MemberUserWithdrawalApplicationExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserWithdrawalApplicationController.class);

    @Resource
    private MemberUserWithdrawalApplicationService memberUserWithdrawalApplicationService;

    @Override
    public BaseService getService() {
        return memberUserWithdrawalApplicationService;
    }

    @SystemOptionAnnotation(describe = "提现申请分页")
    @ApiOperation(value = "提现申请分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "memberId", value = "会员主键", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态（提现状态 0:审核中 1：审批通过 2：审批拒绝  3：打款中 4：转账成功 5：转账失败）", dataTypeClass = Byte.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "", value = "status") Byte status,
                                           @RequestParam(required = false, defaultValue = "", value = "memberId") String memberId) {
        MemberUserWithdrawalApplicationExample memberUserWithdrawalApplicationExample = new MemberUserWithdrawalApplicationExample();
        //创造条件
        MemberUserWithdrawalApplicationExample.Criteria memberUserWithdrawalApplicationCriteria = memberUserWithdrawalApplicationExample.createCriteria();
        memberUserWithdrawalApplicationCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        if (!StringUtil.isNullOrEmpty(memberId)) {
            memberUserWithdrawalApplicationCriteria.andUserIdEqualTo(memberId);
        }
        if (!StringUtil.isNull(status)) {
            if (!Byte.valueOf("-1").equals(status)) {
                memberUserWithdrawalApplicationCriteria.andStatusEqualTo(status);
            }
        }
        return super.responseEntity(BaseResult.ok(super.page(memberUserWithdrawalApplicationExample)));
    }

    @SystemOptionAnnotation(describe = "当前用户提现申请分页")
    @ApiOperation(value = "当前用户提现申请分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "status", value = "状态（提现状态 0:审核中 1：审批通过 2：审批拒绝  3：打款中 4：转账成功 5：转账失败）", dataTypeClass = Byte.class, paramType = "query"),

    })
    @GetMapping(value = "/page/current")
    public ResponseEntity<BaseResult> pageCurrent(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                                  @RequestParam(required = false, defaultValue = "", value = "status") Byte status) {
        MemberUserWithdrawalApplicationExample memberUserWithdrawalApplicationExample = new MemberUserWithdrawalApplicationExample();
        //创造条件
        MemberUserWithdrawalApplicationExample.Criteria memberUserWithdrawalApplicationCriteria = memberUserWithdrawalApplicationExample.createCriteria();
        memberUserWithdrawalApplicationCriteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(getUserId());
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        if (!StringUtil.isNull(status)) {
            memberUserWithdrawalApplicationCriteria.andStatusEqualTo(status);
        }
        return super.responseEntity(BaseResult.ok(super.page(memberUserWithdrawalApplicationExample)));
    }

    @SystemOptionAnnotation(describe = "提现申请列表数据")
    @ApiOperation(value = "提现申请列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        MemberUserWithdrawalApplicationExample memberUserWithdrawalApplicationExample = new MemberUserWithdrawalApplicationExample();
        //创造条件
        MemberUserWithdrawalApplicationExample.Criteria memberUserWithdrawalApplicationCriteria = memberUserWithdrawalApplicationExample.createCriteria();
        memberUserWithdrawalApplicationCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        memberUserWithdrawalApplicationExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(memberUserWithdrawalApplicationExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "提现审批")
    @ApiOperation(value = "提现审批")
    @PostMapping(value = "examine")
    public ResponseEntity<BaseResult> examine(@RequestBody @Valid MemberUserWithdrawalExamineDto memberUserWithdrawalExamineDto) {
        return super.responseEntity(memberUserWithdrawalApplicationService.examine(memberUserWithdrawalExamineDto, getLoginUser()));
    }


    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除提现申请")
    @ApiOperation(value = "删除提现申请")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        MemberUserWithdrawalApplicationExample memberUserWithdrawalApplicationExample = new MemberUserWithdrawalApplicationExample();
        memberUserWithdrawalApplicationExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(memberUserWithdrawalApplicationExample));
    }
}