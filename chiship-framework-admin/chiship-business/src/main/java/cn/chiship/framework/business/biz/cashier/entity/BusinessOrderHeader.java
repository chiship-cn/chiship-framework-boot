package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-26
 */
public class BusinessOrderHeader implements Serializable {

	/**
	 * 订单号
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 订单名称
	 */
	private String orderName;

	/**
	 * 下单日期
	 */
	private Long orderDate;

	/**
	 * 订单状态 0 待付款 1 付款失败 2 已付款 3 已关闭
	 */
	private Byte orderStatus;

	/**
	 * 订单类型 0：普通订单
	 */
	private Byte orderType;

	/**
	 * 下单人员id
	 */
	private String createdBy;

	/**
	 * 下单人员名称
	 */
	private String createdName;

	/**
	 * 金额总计
	 */
	private BigDecimal grandTotal;

	/**
	 * 退款金额
	 */
	private BigDecimal refundTotal;

	/**
	 * 交易号
	 */
	private String tradeNo;

	/**
	 * 付款时间
	 */
	private Long payTime;

	/**
	 * 交易方式
	 */
	private String tradeType;

	/**
	 * 支付渠道
	 */
	private String channelPayWay;

	/**
	 * 关闭时间
	 */
	private Long closedTime;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 用户真实姓名
	 */
	private String userRealName;

	/**
	 * 用户手机号
	 */
	private String userPhone;

	/**
	 * 小程序或微信公众号用户唯一码
	 */
	private String openId;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public Long getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(Long orderDate) {
		this.orderDate = orderDate;
	}

	public Byte getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(Byte orderStatus) {
		this.orderStatus = orderStatus;
	}

	public Byte getOrderType() {
		return orderType;
	}

	public void setOrderType(Byte orderType) {
		this.orderType = orderType;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public BigDecimal getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(BigDecimal grandTotal) {
		this.grandTotal = grandTotal;
	}

	public BigDecimal getRefundTotal() {
		return refundTotal;
	}

	public void setRefundTotal(BigDecimal refundTotal) {
		this.refundTotal = refundTotal;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

	public Long getClosedTime() {
		return closedTime;
	}

	public void setClosedTime(Long closedTime) {
		this.closedTime = closedTime;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", orderName=").append(orderName);
		sb.append(", orderDate=").append(orderDate);
		sb.append(", orderStatus=").append(orderStatus);
		sb.append(", orderType=").append(orderType);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", createdName=").append(createdName);
		sb.append(", grandTotal=").append(grandTotal);
		sb.append(", refundTotal=").append(refundTotal);
		sb.append(", tradeNo=").append(tradeNo);
		sb.append(", payTime=").append(payTime);
		sb.append(", tradeType=").append(tradeType);
		sb.append(", channelPayWay=").append(channelPayWay);
		sb.append(", closedTime=").append(closedTime);
		sb.append(", userId=").append(userId);
		sb.append(", userRealName=").append(userRealName);
		sb.append(", userPhone=").append(userPhone);
		sb.append(", openId=").append(openId);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		BusinessOrderHeader other = (BusinessOrderHeader) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getOrderName() == null ? other.getOrderName() == null
						: this.getOrderName().equals(other.getOrderName()))
				&& (this.getOrderDate() == null ? other.getOrderDate() == null
						: this.getOrderDate().equals(other.getOrderDate()))
				&& (this.getOrderStatus() == null ? other.getOrderStatus() == null
						: this.getOrderStatus().equals(other.getOrderStatus()))
				&& (this.getOrderType() == null ? other.getOrderType() == null
						: this.getOrderType().equals(other.getOrderType()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getCreatedName() == null ? other.getCreatedName() == null
						: this.getCreatedName().equals(other.getCreatedName()))
				&& (this.getGrandTotal() == null ? other.getGrandTotal() == null
						: this.getGrandTotal().equals(other.getGrandTotal()))
				&& (this.getRefundTotal() == null ? other.getRefundTotal() == null
						: this.getRefundTotal().equals(other.getRefundTotal()))
				&& (this.getTradeNo() == null ? other.getTradeNo() == null
						: this.getTradeNo().equals(other.getTradeNo()))
				&& (this.getPayTime() == null ? other.getPayTime() == null
						: this.getPayTime().equals(other.getPayTime()))
				&& (this.getTradeType() == null ? other.getTradeType() == null
						: this.getTradeType().equals(other.getTradeType()))
				&& (this.getChannelPayWay() == null ? other.getChannelPayWay() == null
						: this.getChannelPayWay().equals(other.getChannelPayWay()))
				&& (this.getClosedTime() == null ? other.getClosedTime() == null
						: this.getClosedTime().equals(other.getClosedTime()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserRealName() == null ? other.getUserRealName() == null
						: this.getUserRealName().equals(other.getUserRealName()))
				&& (this.getUserPhone() == null ? other.getUserPhone() == null
						: this.getUserPhone().equals(other.getUserPhone()))
				&& (this.getOpenId() == null ? other.getOpenId() == null : this.getOpenId().equals(other.getOpenId()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getOrderName() == null) ? 0 : getOrderName().hashCode());
		result = prime * result + ((getOrderDate() == null) ? 0 : getOrderDate().hashCode());
		result = prime * result + ((getOrderStatus() == null) ? 0 : getOrderStatus().hashCode());
		result = prime * result + ((getOrderType() == null) ? 0 : getOrderType().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getCreatedName() == null) ? 0 : getCreatedName().hashCode());
		result = prime * result + ((getGrandTotal() == null) ? 0 : getGrandTotal().hashCode());
		result = prime * result + ((getRefundTotal() == null) ? 0 : getRefundTotal().hashCode());
		result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
		result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
		result = prime * result + ((getTradeType() == null) ? 0 : getTradeType().hashCode());
		result = prime * result + ((getChannelPayWay() == null) ? 0 : getChannelPayWay().hashCode());
		result = prime * result + ((getClosedTime() == null) ? 0 : getClosedTime().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserRealName() == null) ? 0 : getUserRealName().hashCode());
		result = prime * result + ((getUserPhone() == null) ? 0 : getUserPhone().hashCode());
		result = prime * result + ((getOpenId() == null) ? 0 : getOpenId().hashCode());
		return result;
	}

}