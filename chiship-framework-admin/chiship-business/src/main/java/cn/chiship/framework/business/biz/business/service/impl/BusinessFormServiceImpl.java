package cn.chiship.framework.business.biz.business.service.impl;

import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormConfigDto;
import cn.chiship.framework.upms.biz.system.service.UpmsDataDictService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.business.mapper.BusinessFormMapper;
import cn.chiship.framework.business.biz.business.entity.BusinessForm;
import cn.chiship.framework.business.biz.business.entity.BusinessFormExample;
import cn.chiship.framework.business.biz.business.service.BusinessFormService;
import cn.chiship.sdk.framework.pojo.vo.DataDictItemVo;
import cn.chiship.sdk.framework.pojo.vo.TreeVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 自定义表单业务接口实现层
 * 2024/12/13
 *
 * @author lijian
 */
@Service
public class BusinessFormServiceImpl extends BaseServiceImpl<BusinessForm, BusinessFormExample> implements BusinessFormService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessFormServiceImpl.class);

    @Resource
    BusinessFormMapper businessFormMapper;

    @Resource
    UpmsDataDictService upmsDataDictService;

    @Override
    public BaseResult tree(Boolean isAll) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        BusinessFormExample.Criteria criteria = businessFormExample.createCriteria()
                .andIsDeletedEqualTo(BaseConstants.NO);
        if (!Boolean.TRUE.equals(isAll)) {
            criteria.andStatusEqualTo(BaseConstants.NO);
        }
        List<BusinessForm> businessForms = businessFormMapper.selectByExample(businessFormExample);

        List<DataDictItemVo> dataDictItemVos = upmsDataDictService.findByCodeFromCache("collectForm");
        List<TreeVo> treeVos = new ArrayList<>();
        for (DataDictItemVo dataDictItemVo : dataDictItemVos) {
            TreeVo treeVo = new TreeVo();
            treeVo.setId(dataDictItemVo.getDataDictItemCode());
            treeVo.setLabel(dataDictItemVo.getDataDictItemName());
            treeVo.setExt("1");
            List<TreeVo> children = new ArrayList<>();
            for (BusinessForm form : businessForms.stream().filter(businessForm -> businessForm.getCategoryId().equals(dataDictItemVo.getDataDictItemCode())).collect(Collectors.toList())) {
                TreeVo child = new TreeVo();
                child.setId(form.getId());
                child.setLabel(form.getName());
                child.setIcon(form.getIcon());
                child.setExt("2");
                children.add(child);
            }
            treeVo.setChildren(children);
            treeVos.add(treeVo);
        }
        return BaseResult.ok(treeVos);
    }

    @Override
    public BusinessForm selectByPrimaryKey(Object id) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        businessFormExample.createCriteria().andIdEqualTo(id.toString());
        List<BusinessForm> businessForms = businessFormMapper.selectByExampleWithBLOBs(businessFormExample);
        if (businessForms.isEmpty()) {
            return new BusinessForm();
        }
        return businessForms.get(0);
    }

    @Override
    public BaseResult insertSelective(BusinessForm businessForm) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        businessFormExample.createCriteria().andCodeEqualTo(businessForm.getCode());
        if (businessFormMapper.countByExample(businessFormExample) > 0) {
            return BaseResult.error("表单编号已存在，请重新输入！");
        }
        return super.insertSelective(businessForm);
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(BusinessForm businessForm) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        businessFormExample.createCriteria().andCodeEqualTo(businessForm.getCode()).andIdNotEqualTo(businessForm.getId());
        if (businessFormMapper.countByExample(businessFormExample) > 0) {
            return BaseResult.error("表单编号已存在，请重新输入！");
        }
        return super.updateByPrimaryKeySelective(businessForm);
    }

    @Override
    public BaseResult saveConfig(BusinessFormConfigDto businessFormConfigDto) {
        BusinessForm businessForm = businessFormMapper.selectByPrimaryKey(businessFormConfigDto.getId());
        if (businessForm == null) {
            return BaseResult.error("无效的表单");
        }
        Boolean isH5 = businessFormConfigDto.getH5();
        if (Boolean.TRUE.equals(isH5)) {
            businessForm.setFormDescriptionH5(businessFormConfigDto.getFormDescription());
            businessForm.setFormContentH5(businessFormConfigDto.getFormContent());
            businessForm.setFormOptionsH5(businessFormConfigDto.getFormOption());
        } else {
            businessForm.setFormDescription(businessFormConfigDto.getFormDescription());
            businessForm.setFormContent(businessFormConfigDto.getFormContent());
            businessForm.setFormOptions(businessFormConfigDto.getFormOption());
        }
        businessForm.setStatus(Byte.valueOf("1"));
        super.updateByPrimaryKeySelective(businessForm);
        return BaseResult.ok(businessForm);
    }

    @Override
    public BaseResult publish(String id) {
        BusinessForm businessForm = businessFormMapper.selectByPrimaryKey(id);
        if (businessForm == null) {
            return BaseResult.error("无效的表单");
        }
        businessForm.setStatus(Byte.valueOf("0"));
        super.updateByPrimaryKeySelective(businessForm);
        return BaseResult.ok();
    }

    @Override
    public BaseResult revoke(String id) {
        BusinessForm businessForm = businessFormMapper.selectByPrimaryKey(id);
        if (businessForm == null) {
            return BaseResult.error("无效的表单");
        }
        businessForm.setStatus(Byte.valueOf("1"));
        super.updateByPrimaryKeySelective(businessForm);
        return BaseResult.ok();
    }
}
