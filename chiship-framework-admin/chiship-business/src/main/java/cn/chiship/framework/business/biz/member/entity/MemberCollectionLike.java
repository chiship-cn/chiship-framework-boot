package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-11-21
 */
public class MemberCollectionLike implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 修改时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 类型(0:点赞 1:收藏)
	 */
	private Byte type;

	/**
	 * 模块
	 */
	private String module;

	/**
	 * 模块对应的id
	 */
	private String moduleId;

	/**
	 * 用户
	 */
	private String createdUserId;

	/**
	 * 用户
	 */
	private String createdUserName;

	/**
	 * 用户头像
	 */
	private String createdUserAvatar;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getCreatedUserName() {
		return createdUserName;
	}

	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}

	public String getCreatedUserAvatar() {
		return createdUserAvatar;
	}

	public void setCreatedUserAvatar(String createdUserAvatar) {
		this.createdUserAvatar = createdUserAvatar;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", type=").append(type);
		sb.append(", module=").append(module);
		sb.append(", moduleId=").append(moduleId);
		sb.append(", createdUserId=").append(createdUserId);
		sb.append(", createdUserName=").append(createdUserName);
		sb.append(", createdUserAvatar=").append(createdUserAvatar);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MemberCollectionLike other = (MemberCollectionLike) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
				&& (this.getModuleId() == null ? other.getModuleId() == null
						: this.getModuleId().equals(other.getModuleId()))
				&& (this.getCreatedUserId() == null ? other.getCreatedUserId() == null
						: this.getCreatedUserId().equals(other.getCreatedUserId()))
				&& (this.getCreatedUserName() == null ? other.getCreatedUserName() == null
						: this.getCreatedUserName().equals(other.getCreatedUserName()))
				&& (this.getCreatedUserAvatar() == null ? other.getCreatedUserAvatar() == null
						: this.getCreatedUserAvatar().equals(other.getCreatedUserAvatar()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
		result = prime * result + ((getModuleId() == null) ? 0 : getModuleId().hashCode());
		result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
		result = prime * result + ((getCreatedUserName() == null) ? 0 : getCreatedUserName().hashCode());
		result = prime * result + ((getCreatedUserAvatar() == null) ? 0 : getCreatedUserAvatar().hashCode());
		return result;
	}

}