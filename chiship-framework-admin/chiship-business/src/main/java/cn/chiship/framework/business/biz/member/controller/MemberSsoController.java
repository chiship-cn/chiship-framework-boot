package cn.chiship.framework.business.biz.member.controller;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserModifyDto;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserRegisterDto;
import cn.chiship.framework.common.annotation.LoginUser;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.business.biz.member.service.MemberUserService;
import cn.chiship.framework.third.core.common.ThirdApplicationOauthTypeEnum;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsCodeService;
import cn.chiship.sdk.cache.service.UserCacheService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.annotation.Authorization;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.ip.IpUtils;
import cn.chiship.sdk.framework.pojo.dto.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

/**
 * 登录相关
 *
 * @author lijian
 */
@RestController
@RequestMapping("/memberSso")
@Api(tags = "会员登录控制器")
public class MemberSsoController {

    @Resource
    UserCacheService userCacheService;

    @Resource
    private MemberUserService memberUserService;

    @Resource
    private UpmsSmsCodeService upmsSmsCodeService;

    @SystemOptionAnnotation(describe = "手机号+验证码登录", option = BusinessTypeEnum.SYSTEM_OPTION_LOGIN)
    @ApiOperation(value = "手机号+验证码登录")
    @PostMapping("/mobileLogin")
    @NoParamsSign
    public ResponseEntity<BaseResult> memberAppletMobileLogin(HttpServletRequest request,
                                                              @RequestBody @Valid UserMobileLoginDto mobileLoginDto) {

        BaseResult baseResult = upmsSmsCodeService.verification(mobileLoginDto.getMobile(),
                mobileLoginDto.getMobileVerificationCode());
        if (!baseResult.isSuccess()) {
            return new ResponseEntity<>(baseResult, HttpStatus.OK);
        }
        return new ResponseEntity<>(
                memberUserService.mobileLogin(mobileLoginDto.getMobile(), IpUtils.getIpAddr(request)), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "用户名+密码登录", option = BusinessTypeEnum.SYSTEM_OPTION_LOGIN)
    @ApiOperation(value = "用户名+密码登录")
    @PostMapping("basicLogin")
    public ResponseEntity<BaseResult> basicLogin(HttpServletRequest request,
                                                 @RequestBody @Valid UserBaseLoginDto baseLoginDto) {
        String ip = IpUtils.getIpAddr(request);
        return new ResponseEntity<>(memberUserService.login(baseLoginDto.getUsername(), baseLoginDto.getPassword(), ip),
                HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "小程序登录", option = BusinessTypeEnum.SYSTEM_OPTION_LOGIN)
    @ApiOperation(value = "小程序登录")
    @PostMapping("/appletLogin")
    @NoParamsSign
    public ResponseEntity<BaseResult> appletLogin(HttpServletRequest request, @RequestBody @Valid String openId) {
        BaseResult baseResult = memberUserService.appletLogin(ThirdUtil.getAppId(), openId, IpUtils.getIpAddr(request));
        return new ResponseEntity<>(baseResult, HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "忘记密码", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
    @ApiOperation(value = "忘记密码")
    @PostMapping(value = "forgotPassword")
    public ResponseEntity<BaseResult> forgotPassword(@RequestBody @Valid UserForgotPasswordDto userForgotPasswordDto) {
        BaseResult baseResult = upmsSmsCodeService.verification(userForgotPasswordDto.getMobile(),
                userForgotPasswordDto.getVerificationCode());
        if (!baseResult.isSuccess()) {
            return new ResponseEntity<>(baseResult, HttpStatus.OK);
        }
        return new ResponseEntity<>(memberUserService.forgotPassword(userForgotPasswordDto), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "修改密码", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
    @ApiOperation(value = "修改密码")
    @Authorization
    @PostMapping(value = "modifyPassword")
    public ResponseEntity<BaseResult> modifyPassword(
            @RequestBody @Valid UserModifyPasswordDto userModifyPasswordDto,
            @ApiIgnore @LoginUser CacheUserVO userVO) {
        BaseResult baseResult = upmsSmsCodeService.verification(userVO.getMobile(),userModifyPasswordDto.getVerificationCode());
        if (!baseResult.isSuccess()) {
            return new ResponseEntity<>(baseResult, HttpStatus.OK);
        }
        return new ResponseEntity<>(memberUserService.modifyPassword(userModifyPasswordDto, userVO), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "用户注册", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
    @ApiOperation(value = "用户注册")
    @PostMapping(value = "register")
    public ResponseEntity<BaseResult> register(@RequestBody @Valid MemberUserRegisterDto memberUserRegisterDto) {
        BaseResult baseResult = upmsSmsCodeService.verification(memberUserRegisterDto.getMobile(),
                memberUserRegisterDto.getVerificationCode());
        if (!baseResult.isSuccess()) {
            return new ResponseEntity<>(baseResult, HttpStatus.OK);
        }
        return new ResponseEntity<>(memberUserService.register(memberUserRegisterDto), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "微信小程序授权注册", option = BusinessTypeEnum.SYSTEM_OPTION_LOGIN)
    @ApiOperation(value = "微信小程序授权注册")
    @PostMapping("/wxMpRegister")
    @NoParamsSign
    public ResponseEntity<BaseResult> wxMpRegister(HttpServletRequest request,
                                                   @RequestBody @Valid AppletLoginOrRegisterDto appletLoginOrRegisterDto) {
        BaseResult baseResult = memberUserService.grantAuthRegister(ThirdUtil.getAppId(), appletLoginOrRegisterDto,
                ThirdApplicationOauthTypeEnum.APPLICATION_OAUTH_WX_MINI, IpUtils.getIpAddr(request));
        return new ResponseEntity<>(baseResult, HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "支付宝小程序授权注册", option = BusinessTypeEnum.SYSTEM_OPTION_LOGIN)
    @ApiOperation(value = "支付宝小程序授权注册")
    @PostMapping("/zfbMpRegister")
    @NoParamsSign
    public ResponseEntity<BaseResult> zfbMpRegister(HttpServletRequest request,
                                                    @RequestBody @Valid AppletLoginOrRegisterDto appletLoginOrRegisterDto) {
        BaseResult baseResult = memberUserService.grantAuthRegister(ThirdUtil.getAppId(), appletLoginOrRegisterDto,
                ThirdApplicationOauthTypeEnum.APPLICATION_OAUTH_ZFB_MINI, IpUtils.getIpAddr(request));
        return new ResponseEntity<>(baseResult, HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "设置用户名")
    @ApiOperation(value = "设置用户名")
    @PostMapping(value = "modifyUserName")
    @Authorization
    public ResponseEntity<BaseResult> modifyUserName(@RequestBody(required = false) String userName,
                                                     @ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(memberUserService.modifyUserName(userVO.getId(), userName), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "修改昵称")
    @ApiOperation(value = "修改昵称")
    @PostMapping(value = "modifyNickName")
    @Authorization
    public ResponseEntity<BaseResult> modifyNickName(@RequestBody(required = false) String nickName,
                                                     @ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(memberUserService.modifyNickName(userVO.getId(), nickName), HttpStatus.OK);
    }

    @ApiOperation(value = "修改用户信息")
    @PostMapping(value = "modifyUser")
    @Authorization
    public ResponseEntity<BaseResult> modifyUser(@RequestBody @Valid @NotEmpty MemberUserModifyDto memberUserModifyDto,
                                                 @ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(
                memberUserService.modifyUser(memberUserModifyDto, StringUtil.getString(userVO.getId())), HttpStatus.OK);
    }

    @ApiOperation(value = "获得钱包信息")
    @GetMapping("/getWallet")
    @Authorization
    public ResponseEntity<BaseResult> getWallet(@ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(memberUserService.walletByUserId(userVO.getId()), HttpStatus.OK);
    }

    @ApiOperation(value = "获得用户信息")
    @GetMapping("/getInfo")
    @Authorization
    public ResponseEntity<BaseResult> getInfo(@ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(BaseResult.ok(userVO), HttpStatus.OK);
    }

    @ApiOperation(value = "获得当前登录用户信息")
    @GetMapping("/getCurrentUserInfo")
    @Authorization
    public ResponseEntity<BaseResult> getCurrentUserInfo(@ApiIgnore @LoginUser CacheUserVO userVO) {
        return new ResponseEntity<>(memberUserService.selectDetailsByPrimaryKey(userVO.getId()), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "注销登录", option = BusinessTypeEnum.SYSTEM_OPTION_LOGOUT)
    @ApiOperation(value = "注销登录")
    @PostMapping("/logout")
    @Authorization
    public ResponseEntity<BaseResult> logout() {
        userCacheService.removeUser();
        return new ResponseEntity<>(BaseResult.ok(), HttpStatus.OK);
    }

}
