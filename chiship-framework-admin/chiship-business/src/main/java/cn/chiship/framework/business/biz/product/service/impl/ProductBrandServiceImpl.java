package cn.chiship.framework.business.biz.product.service.impl;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.product.mapper.ProductBrandMapper;
import cn.chiship.framework.business.biz.product.entity.ProductBrand;
import cn.chiship.framework.business.biz.product.entity.ProductBrandExample;
import cn.chiship.framework.business.biz.product.service.ProductBrandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 品牌业务接口实现层
 * 2024/12/11
 *
 * @author lijian
 */
@Service
public class ProductBrandServiceImpl extends BaseServiceImpl<ProductBrand, ProductBrandExample> implements ProductBrandService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductBrandServiceImpl.class);

    @Resource
    ProductBrandMapper productBrandMapper;

    @Override
    public BaseResult insertSelective(ProductBrand productBrand) {
        ProductBrandExample productBrandExample = new ProductBrandExample();
        productBrandExample.createCriteria().andBrandNameEqualTo(productBrand.getBrandName());
        if (!productBrandMapper.selectByExample(productBrandExample).isEmpty()) {
            return BaseResult.error("品牌名称已存在，请重新输入！");
        }
        return super.insertSelective(productBrand);
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(ProductBrand productBrand) {
        ProductBrandExample productBrandExample = new ProductBrandExample();
        productBrandExample.createCriteria().andBrandNameEqualTo(productBrand.getBrandName()).andIdNotEqualTo(productBrand.getId());
        if (!productBrandMapper.selectByExample(productBrandExample).isEmpty()) {
            return BaseResult.error("品牌名称已存在，请重新输入！");
        }
        return super.updateByPrimaryKeySelective(productBrand);
    }
}
