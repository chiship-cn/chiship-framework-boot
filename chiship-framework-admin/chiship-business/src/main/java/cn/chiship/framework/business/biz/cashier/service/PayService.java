package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.framework.business.biz.cashier.pojo.dto.*;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.pay.core.enums.TradeTypeEnum;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * 支付相关服务
 *
 * @author lijian
 */
public interface PayService {

    /**
     * 订单创建
     *
     * @param orderCreateDto
     * @param cacheUserVO
     * @return
     */
    BaseResult createOrder(OrderRechargeCreateDto orderCreateDto, CacheUserVO cacheUserVO);

    /**
     * 根据订单号查询订单状态
     *
     * @param orderId
     * @return
     */
    BaseResult getOrderStatusByOrderId(String orderId);

    /**
     * 普通支付
     *
     * @param tradeTypeEnum
     * @param normalPayDto
     * @param ip
     * @return
     */
    BaseResult normalPay(TradeTypeEnum tradeTypeEnum, NormalPayDto normalPayDto, String ip);

    /**
     * 预支付
     *
     * @param prepayDto
     * @param ip
     * @return
     */
    BaseResult prepay(PrepayDto prepayDto, String ip);

    /**
     * 退款
     *
     * @param refundDto
     * @param cacheUserVO
     * @return
     */
    BaseResult refund(RefundDto refundDto, CacheUserVO cacheUserVO);

    /**
     * 提现
     *
     * @param withdrawalApplicationDto
     * @param cacheUserVO
     * @return
     */
    BaseResult withdrawal(WithdrawalApplicationDto withdrawalApplicationDto, CacheUserVO cacheUserVO);

    /**
     * 订单电子对账
     *
     * @param orderNo       订单号
     * @param channelPayWay 支付通道
     * @return
     */
    BaseResult reconciliationOrder(String orderNo, String channelPayWay);

    /**
     * 退款对账
     *
     * @param refundNo 退款单号
     * @return
     */
    BaseResult reconciliationRefund(String refundNo);

    /**
     * 微信异步通知业务处理
     *
     * @param tradeType
     * @param baseResult
     */
    void dealWxPayNotify(String tradeType, BaseResult baseResult);

    /**
     * 微信异步转转通知业务处理
     *
     * @param baseResult
     */
    void dealWxTransferNotify(BaseResult baseResult);

    /**
     * 微信异步退款通知业务处理
     *
     * @param baseResult
     */
    void dealWxRefundNotify(BaseResult baseResult);

    /**
     * 支付宝异步通知业务处理
     *
     * @param tradeType
     * @param dataMap
     * @param response
     * @throws IOException
     */
    void dealAliPayNotify(String tradeType, Map<String, Object> dataMap, HttpServletResponse response)
            throws IOException;

}
