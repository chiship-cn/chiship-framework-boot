package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrder;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrderExample;
import cn.chiship.framework.business.biz.cashier.enums.RefundStatusEnum;
import cn.chiship.framework.business.biz.cashier.mapper.BusinessRefundOrderMapper;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessRefundOrderDto;
import cn.chiship.framework.business.biz.cashier.service.BusinessRefundOrderService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 退款订单业务接口实现层 2024/6/5
 *
 * @author lijian
 */
@Service
public class BusinessRefundOrderServiceImpl extends BaseServiceImpl<BusinessRefundOrder, BusinessRefundOrderExample>
		implements BusinessRefundOrderService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRefundOrderServiceImpl.class);

	@Resource
	BusinessRefundOrderMapper businessRefundOrderMapper;

	@Override
	public BaseResult createRefund(BusinessRefundOrderDto businessRefundOrderDto, CacheUserVO cacheUserVO) {
		BusinessRefundOrder businessRefundOrder = new BusinessRefundOrder();
		businessRefundOrder.setId(businessRefundOrderDto.getRefundNo());
		businessRefundOrder.setGmtCreated(System.currentTimeMillis());
		businessRefundOrder.setGmtModified(System.currentTimeMillis());
		businessRefundOrder.setIsDeleted(Byte.valueOf("0"));
		businessRefundOrder.setRefundStatus(RefundStatusEnum.REFUND_APPLICATION.getStatus());
		businessRefundOrder.setOrderNo(businessRefundOrderDto.getOrderNo());
		businessRefundOrder.setTradeNo(businessRefundOrderDto.getTradeNo());
		businessRefundOrder.setCreatedBy(cacheUserVO.getId());
		businessRefundOrder.setCreatedName(cacheUserVO.getRealName());
		businessRefundOrder.setRefundTotal(businessRefundOrderDto.getRefundTotal());
		businessRefundOrder.setRefundReason(businessRefundOrderDto.getRefundReason());
		businessRefundOrder.setChannelPayWay(businessRefundOrderDto.getChannelPayWay());
		businessRefundOrder.setIsNotify(Byte.valueOf("0"));
		businessRefundOrder.setUserId(cacheUserVO.getId());
		businessRefundOrder.setUserRealName(cacheUserVO.getRealName());
		businessRefundOrder.setUserPhone(cacheUserVO.getPhone());
		businessRefundOrderMapper.insertSelective(businessRefundOrder);
		return BaseResult.ok();
	}

	@Override
	public BaseResult refundSuccess(String refundNo, BigDecimal realRefundTotal, String refundTradeNo,
			String receivedAccount, Long time) {
		BusinessRefundOrder businessRefundOrder = businessRefundOrderMapper.selectByPrimaryKey(refundNo);
		businessRefundOrder.setRefundStatus(RefundStatusEnum.REFUND_SUCCESS.getStatus());
		businessRefundOrder.setRefundFinishTime(time);
		businessRefundOrder.setIsNotify(Byte.valueOf("1"));
		businessRefundOrder.setRealRefundTotal(realRefundTotal);
		if (!StringUtil.isNull(refundTradeNo)) {
			businessRefundOrder.setRefundTradeNo(refundTradeNo);
		}
		if (!StringUtil.isNull(receivedAccount)) {
			businessRefundOrder.setReceivedAccount(receivedAccount);
		}
		businessRefundOrderMapper.updateByPrimaryKeySelective(businessRefundOrder);
		return BaseResult.ok();
	}

	@Override
	public BaseResult refundNotifyStatus(String refundNo, RefundStatusEnum refundStatusEnum) {
		BusinessRefundOrder businessRefundOrder = businessRefundOrderMapper.selectByPrimaryKey(refundNo);
		businessRefundOrder.setRefundStatus(refundStatusEnum.getStatus());
		businessRefundOrder.setIsNotify(Byte.valueOf("1"));
		businessRefundOrderMapper.updateByPrimaryKeySelective(businessRefundOrder);
		return BaseResult.ok();
	}

}
