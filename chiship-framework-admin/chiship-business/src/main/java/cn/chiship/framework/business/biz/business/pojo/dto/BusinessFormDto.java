package cn.chiship.framework.business.biz.business.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 自定义表单表单
 * 2024/12/13
 *
 * @author LiJian
 */
@ApiModel(value = "自定义表单表单")
public class BusinessFormDto {

    @ApiModelProperty(value = "编号", required = true)
    @NotEmpty(message = "编号")
    @Length(min = 1, max = 4)
    private String code;

    @ApiModelProperty(value = "名称", required = true)
    @NotEmpty(message = "名称")
    @Length(min = 1, max = 15)
    private String name;

    @ApiModelProperty(value = "分类ID", required = true)
    @NotEmpty(message = "分类ID")
    @Length(min = 1, max = 36)
    private String categoryId;

    @ApiModelProperty(value = "是否重复提交（0：否，1：是）", required = true)
    @NotNull(message = "是否重复提交")
    @Min(0)
    @Max(1)
    private Byte isRepeatedSubmission;

    @ApiModelProperty(value = "图标")
    private String icon;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }


    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Byte getIsRepeatedSubmission() {
        return isRepeatedSubmission;
    }

    public void setIsRepeatedSubmission(Byte isRepeatedSubmission) {
        this.isRepeatedSubmission = isRepeatedSubmission;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}