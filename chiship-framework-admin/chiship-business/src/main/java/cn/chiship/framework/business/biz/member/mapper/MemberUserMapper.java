package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberUser;
import cn.chiship.framework.business.biz.member.entity.MemberUserExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface MemberUserMapper extends BaseMapper<MemberUser, MemberUserExample> {

}