package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 提现申请表单
 * 2024/7/1
 *
 * @author LiJian
 */
@ApiModel(value = "提现审批")
public class MemberUserWithdrawalExamineDto {

    @ApiModelProperty(value = "主键", required = true)
    @NotEmpty(message = "主键" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 36)
    private String id;

    @ApiModelProperty(value = "审批状态", required = true)
    @NotNull(message = "审批状态" + BaseTipConstants.NOT_EMPTY)
    @Min(1)
    @Max(2)
    private Byte status;

    @ApiModelProperty(value = "审批描述")
    private String statusDesc;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
}