package cn.chiship.framework.business.biz.content.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.content.service.ContentAdvertService;
import cn.chiship.framework.business.biz.content.entity.ContentAdvert;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertExample;
import cn.chiship.framework.business.biz.content.pojo.dto.ContentAdvertDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 广告版位控制层 2024/10/31
 *
 * @author lijian
 */
@RestController
@RequestMapping("/contentAdvert")
@Api(tags = "广告版位")
public class ContentAdvertController extends BaseController<ContentAdvert, ContentAdvertExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContentAdvertController.class);

	@Resource
	private ContentAdvertService contentAdvertService;

	@Override
	public BaseService getService() {
		return contentAdvertService;
	}

	@SystemOptionAnnotation(describe = "广告版位分页")
	@ApiOperation(value = "广告版位分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "slotId", value = "版位ID", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	@Authorization
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "slotId") String slotId) {
		ContentAdvertExample contentAdvertExample = new ContentAdvertExample();
		// 创造条件
		ContentAdvertExample.Criteria contentAdvertCriteria = contentAdvertExample.createCriteria();
		contentAdvertCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(slotId)) {
			contentAdvertCriteria.andSlotIdEqualTo(slotId);
		}
		return super.responseEntity(BaseResult.ok(super.page(contentAdvertExample)));
	}

	@SystemOptionAnnotation(describe = "广告版位列表数据")
	@ApiOperation(value = "广告版位列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		ContentAdvertExample contentAdvertExample = new ContentAdvertExample();
		// 创造条件
		ContentAdvertExample.Criteria contentAdvertCriteria = contentAdvertExample.createCriteria();
		contentAdvertCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		return super.responseEntity(BaseResult.ok(super.list(contentAdvertExample)));
	}

	@ApiOperation(value = "根据编码获取广告")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "code", value = "编码", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/getByCode")
	public ResponseEntity<BaseResult> getByCode(@RequestParam(value = "code") String code) {
		return super.responseEntity(BaseResult.ok(contentAdvertService.getCacheAdvertByCode(code)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存广告版位")
	@ApiOperation(value = "保存广告版位")
	@PostMapping(value = "save")
	@Authorization
	public ResponseEntity<BaseResult> save(@RequestBody @Valid ContentAdvertDto contentAdvertDto) {
		ContentAdvert contentAdvert = new ContentAdvert();
		BeanUtils.copyProperties(contentAdvertDto, contentAdvert);
		return super.responseEntity(super.save(contentAdvert));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新广告版位")
	@ApiOperation(value = "更新广告版位")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	@Authorization
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ContentAdvertDto contentAdvertDto) {
		ContentAdvert contentAdvert = new ContentAdvert();
		BeanUtils.copyProperties(contentAdvertDto, contentAdvert);
		return super.responseEntity(super.update(id, contentAdvert));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除广告版位")
	@ApiOperation(value = "删除广告版位")
	@PostMapping(value = "remove")
	@Authorization
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		ContentAdvertExample contentAdvertExample = new ContentAdvertExample();
		contentAdvertExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(contentAdvertExample));
	}

}