package cn.chiship.framework.business.biz.member.service;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberCommentDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberComment;
import cn.chiship.framework.business.biz.member.entity.MemberCommentExample;

/**
 * 评论表业务接口层 2024/11/22
 *
 * @author lijian
 */
public interface MemberCommentService extends BaseService<MemberComment, MemberCommentExample> {

	/**
	 * 新增评论
	 * @param memberCommentDto
	 * @param cacheUserVO
	 * @param ip
	 * @return
	 */
	BaseResult create(MemberCommentDto memberCommentDto, CacheUserVO cacheUserVO, String ip);

	/**
	 * 更变状态
	 * @param id
	 * @param status
	 * @param cacheUserVO
	 * @return
	 */
	BaseResult changeStatus(String id, Byte status, CacheUserVO cacheUserVO);

}
