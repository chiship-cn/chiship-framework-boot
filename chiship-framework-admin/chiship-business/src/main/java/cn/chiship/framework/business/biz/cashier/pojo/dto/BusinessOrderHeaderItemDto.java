package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItem;

/**
 * 订单明细表单
 * 2024/12/16
 * @author LiJian
 */
@ApiModel(value = "订单明细表单")
public class BusinessOrderHeaderItemDto extends BusinessOrderHeaderItem{

}