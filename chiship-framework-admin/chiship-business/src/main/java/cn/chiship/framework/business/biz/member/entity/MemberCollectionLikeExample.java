package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-11-21
 */
public class MemberCollectionLikeExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public MemberCollectionLikeExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andModuleIsNull() {
			addCriterion("module is null");
			return (Criteria) this;
		}

		public Criteria andModuleIsNotNull() {
			addCriterion("module is not null");
			return (Criteria) this;
		}

		public Criteria andModuleEqualTo(String value) {
			addCriterion("module =", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotEqualTo(String value) {
			addCriterion("module <>", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleGreaterThan(String value) {
			addCriterion("module >", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleGreaterThanOrEqualTo(String value) {
			addCriterion("module >=", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLessThan(String value) {
			addCriterion("module <", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLessThanOrEqualTo(String value) {
			addCriterion("module <=", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLike(String value) {
			addCriterion("module like", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotLike(String value) {
			addCriterion("module not like", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleIn(List<String> values) {
			addCriterion("module in", values, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotIn(List<String> values) {
			addCriterion("module not in", values, "module");
			return (Criteria) this;
		}

		public Criteria andModuleBetween(String value1, String value2) {
			addCriterion("module between", value1, value2, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotBetween(String value1, String value2) {
			addCriterion("module not between", value1, value2, "module");
			return (Criteria) this;
		}

		public Criteria andModuleIdIsNull() {
			addCriterion("module_id is null");
			return (Criteria) this;
		}

		public Criteria andModuleIdIsNotNull() {
			addCriterion("module_id is not null");
			return (Criteria) this;
		}

		public Criteria andModuleIdEqualTo(String value) {
			addCriterion("module_id =", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotEqualTo(String value) {
			addCriterion("module_id <>", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdGreaterThan(String value) {
			addCriterion("module_id >", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdGreaterThanOrEqualTo(String value) {
			addCriterion("module_id >=", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLessThan(String value) {
			addCriterion("module_id <", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLessThanOrEqualTo(String value) {
			addCriterion("module_id <=", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLike(String value) {
			addCriterion("module_id like", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotLike(String value) {
			addCriterion("module_id not like", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdIn(List<String> values) {
			addCriterion("module_id in", values, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotIn(List<String> values) {
			addCriterion("module_id not in", values, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdBetween(String value1, String value2) {
			addCriterion("module_id between", value1, value2, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotBetween(String value1, String value2) {
			addCriterion("module_id not between", value1, value2, "moduleId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNull() {
			addCriterion("created_user_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNotNull() {
			addCriterion("created_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdEqualTo(String value) {
			addCriterion("created_user_id =", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotEqualTo(String value) {
			addCriterion("created_user_id <>", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThan(String value) {
			addCriterion("created_user_id >", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_id >=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThan(String value) {
			addCriterion("created_user_id <", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThanOrEqualTo(String value) {
			addCriterion("created_user_id <=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLike(String value) {
			addCriterion("created_user_id like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotLike(String value) {
			addCriterion("created_user_id not like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIn(List<String> values) {
			addCriterion("created_user_id in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotIn(List<String> values) {
			addCriterion("created_user_id not in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdBetween(String value1, String value2) {
			addCriterion("created_user_id between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotBetween(String value1, String value2) {
			addCriterion("created_user_id not between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIsNull() {
			addCriterion("created_user_name is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIsNotNull() {
			addCriterion("created_user_name is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameEqualTo(String value) {
			addCriterion("created_user_name =", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotEqualTo(String value) {
			addCriterion("created_user_name <>", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameGreaterThan(String value) {
			addCriterion("created_user_name >", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_name >=", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLessThan(String value) {
			addCriterion("created_user_name <", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLessThanOrEqualTo(String value) {
			addCriterion("created_user_name <=", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLike(String value) {
			addCriterion("created_user_name like", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotLike(String value) {
			addCriterion("created_user_name not like", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIn(List<String> values) {
			addCriterion("created_user_name in", values, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotIn(List<String> values) {
			addCriterion("created_user_name not in", values, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameBetween(String value1, String value2) {
			addCriterion("created_user_name between", value1, value2, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotBetween(String value1, String value2) {
			addCriterion("created_user_name not between", value1, value2, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIsNull() {
			addCriterion("created_user_avatar is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIsNotNull() {
			addCriterion("created_user_avatar is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarEqualTo(String value) {
			addCriterion("created_user_avatar =", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotEqualTo(String value) {
			addCriterion("created_user_avatar <>", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarGreaterThan(String value) {
			addCriterion("created_user_avatar >", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_avatar >=", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLessThan(String value) {
			addCriterion("created_user_avatar <", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLessThanOrEqualTo(String value) {
			addCriterion("created_user_avatar <=", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLike(String value) {
			addCriterion("created_user_avatar like", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotLike(String value) {
			addCriterion("created_user_avatar not like", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIn(List<String> values) {
			addCriterion("created_user_avatar in", values, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotIn(List<String> values) {
			addCriterion("created_user_avatar not in", values, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarBetween(String value1, String value2) {
			addCriterion("created_user_avatar between", value1, value2, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotBetween(String value1, String value2) {
			addCriterion("created_user_avatar not between", value1, value2, "createdUserAvatar");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}