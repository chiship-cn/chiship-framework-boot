package cn.chiship.framework.business.biz.content.service.impl;

import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlot;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlotExample;
import cn.chiship.framework.business.biz.content.mapper.ContentAdvertSlotMapper;
import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.content.mapper.ContentAdvertMapper;
import cn.chiship.framework.business.biz.content.entity.ContentAdvert;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertExample;
import cn.chiship.framework.business.biz.content.service.ContentAdvertService;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 广告版位业务接口实现层 2024/10/31
 *
 * @author lijian
 */
@Service
public class ContentAdvertServiceImpl extends BaseServiceImpl<ContentAdvert, ContentAdvertExample>
		implements ContentAdvertService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContentAdvertServiceImpl.class);

	@Resource
	ContentAdvertMapper contentAdvertMapper;

	@Resource
	ContentAdvertSlotMapper contentAdvertSlotMapper;

	@Resource
	RedisService redisService;

	@Override
	public JSONObject getCacheAdvertByCode(String code) {
		String itemKey = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_AD_DATA) + ":" + code;
		if (!redisService.hasKey(itemKey)) {
			return null;
		}
		JSONObject adSlotJson = (JSONObject) redisService.get(itemKey);
		JSONArray adArray = adSlotJson.getJSONArray("children");
		JSONArray newAdArray = new JSONArray();

		for (int i = 0; i < adArray.size(); i++) {
			JSONObject adJson = adArray.getJSONObject(i);
			long beginDate = 0L;
			long endDate = System.currentTimeMillis() + 24 * 60 * 60 * 1000;
			long now = System.currentTimeMillis();
			if (ObjectUtils.isNotEmpty(adJson.get("beginDate"))) {
				beginDate = adJson.getDate("beginDate").getTime();
			}
			if (ObjectUtils.isNotEmpty(adJson.get("endDate"))) {
				endDate = adJson.getDate("endDate").getTime();
			}
			if (now >= beginDate && now <= endDate) {
				newAdArray.add(adJson);
			}
		}
		adSlotJson.put("children", newAdArray);
		return adSlotJson;
	}

	@Override
	public BaseResult insertSelective(ContentAdvert contentAdvert) {
		super.insertSelective(contentAdvert);
		cacheAdvertData();
		return BaseResult.ok();
	}

	@Override
	public BaseResult updateByPrimaryKeySelective(ContentAdvert contentAdvert) {
		super.updateByPrimaryKeySelective(contentAdvert);
		cacheAdvertData();
		return BaseResult.ok();
	}

	@Override
	public BaseResult deleteByExample(ContentAdvertExample contentAdvertExample) {
		super.deleteByExample(contentAdvertExample);
		cacheAdvertData();
		return BaseResult.ok();

	}

	@Override
	public void cacheAdvertData() {
		String itemKey = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_AD_DATA);
		ContentAdvertSlotExample advertSlotExample = new ContentAdvertSlotExample();
		advertSlotExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO);
		List<ContentAdvertSlot> advertSlots = contentAdvertSlotMapper.selectByExample(advertSlotExample);
		for (ContentAdvertSlot advertSlot : advertSlots) {
			JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(advertSlot));
			ContentAdvertExample advertExample = new ContentAdvertExample();
			advertExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO).andSlotIdEqualTo(advertSlot.getId());
			advertExample.setOrderByClause("+seq");
			List<ContentAdvert> adverts = contentAdvertMapper.selectByExample(advertExample);
			jsonObject.put("children", adverts);
			redisService.set(itemKey + ":" + advertSlot.getCode(), jsonObject);
		}
	}

}
