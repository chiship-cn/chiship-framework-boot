package cn.chiship.framework.business.biz.content.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.constraints.*;

/**
 * 广告版位表单 2024/10/31
 *
 * @author LiJian
 */
@ApiModel(value = "广告版位表单")
public class ContentAdvertDto {

	@ApiModelProperty(value = "插槽主键", required = true)
	private String slotId;

	@ApiModelProperty(value = "图片", required = true)
	private String imageUrl;

	@ApiModelProperty(value = "链接")
	private String linkUrl;

	@ApiModelProperty(value = "文字内容")
	private String text;

	@ApiModelProperty(value = "开始时间")
	private Long beginDate;

	@ApiModelProperty(value = "结束时间")
	private Long endDate;

	@ApiModelProperty(value = "排序", required = true)
	@NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	private Integer seq;

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Long beginDate) {
		this.beginDate = beginDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

}