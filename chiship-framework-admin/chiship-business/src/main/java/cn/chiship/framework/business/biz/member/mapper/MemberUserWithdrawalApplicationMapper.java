package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplication;
import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplicationExample;


import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-17
 */
public interface MemberUserWithdrawalApplicationMapper extends BaseMapper<MemberUserWithdrawalApplication, MemberUserWithdrawalApplicationExample> {

}