package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberCollectionLike;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLikeExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-11-21
 */
public interface MemberCollectionLikeMapper extends BaseMapper<MemberCollectionLike, MemberCollectionLikeExample> {

}