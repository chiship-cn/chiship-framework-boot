package cn.chiship.framework.business.biz.business.pojo.vo;

import cn.chiship.framework.business.biz.business.entity.BusinessFormValue;
import io.swagger.annotations.ApiModel;

/**
 * 自封有表单提交数据表单
 * 2024/12/13
 *
 * @author LiJian
 */
@ApiModel(value = "提交数据表单视图")
public class BusinessFormValueVo extends BusinessFormValue {

    /**
     * 表单分类
     */
    private String categoryId;
    /**
     * 表单名称
     */
    private String formName;

    /**
     * 表单设计内容
     */
    private String formContent;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public String getFormContent() {
        return formContent;
    }

    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }
}