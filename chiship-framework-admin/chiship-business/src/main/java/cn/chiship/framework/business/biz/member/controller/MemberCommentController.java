package cn.chiship.framework.business.biz.member.controller;

import cn.chiship.framework.business.biz.member.enmus.MemberCommentModuleEnum;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.util.ip.IpUtils;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberCommentService;
import cn.chiship.framework.business.biz.member.entity.MemberComment;
import cn.chiship.framework.business.biz.member.entity.MemberCommentExample;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCommentDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 评论表控制层 2024/11/22
 *
 * @author lijian
 */
@RestController
@RequestMapping("/memberComment")
@Api(tags = "评论表")
public class MemberCommentController extends BaseController<MemberComment, MemberCommentExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberCommentController.class);

	@Resource
	private MemberCommentService memberCommentService;

	@Override
	public BaseService getService() {
		return memberCommentService;
	}

	@SystemOptionAnnotation(describe = "评论表分页")
	@ApiOperation(value = "评论表分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "module", value = "模块枚举", defaultValue = "",
					dataTypeClass = MemberCommentModuleEnum.class, paramType = "query"),
			@ApiImplicitParam(name = "moduleId", value = "模块ID", defaultValue = "", dataTypeClass = String.class,
					paramType = "query"), })
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "module") MemberCommentModuleEnum module,
			@RequestParam(required = false, defaultValue = "", value = "moduleId") String moduleId) {
		MemberCommentExample memberCommentExample = new MemberCommentExample();
		// 创造条件
		MemberCommentExample.Criteria memberCommentCriteria = memberCommentExample.createCriteria();
		memberCommentCriteria.andIsDeletedEqualTo(BaseConstants.NO).andParentIdEqualTo("0");
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		if (!StringUtil.isNull(module)) {
			memberCommentCriteria.andModuleEqualTo(module.getType());
		}
		if (!StringUtil.isNullOrEmpty(moduleId)) {
			memberCommentCriteria.andModuleIdEqualTo(moduleId);
		}
		return super.responseEntity(BaseResult.ok(super.page(memberCommentExample)));
	}

	@SystemOptionAnnotation(describe = "评论表分页(审核通过)")
	@ApiOperation(value = "评论表分页(审核通过)")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "module", value = "模块枚举", required = true,
					dataTypeClass = MemberCommentModuleEnum.class, paramType = "query"),
			@ApiImplicitParam(name = "moduleId", value = "模块ID", required = true, dataTypeClass = String.class,
					paramType = "query"), })
	@GetMapping(value = "/pageV1")
	public ResponseEntity<BaseResult> pageV1(@RequestParam(value = "module") MemberCommentModuleEnum module,
			@RequestParam(value = "moduleId") String moduleId) {
		MemberCommentExample memberCommentExample = new MemberCommentExample();
		// 创造条件
		MemberCommentExample.Criteria memberCommentCriteria = memberCommentExample.createCriteria();
		memberCommentCriteria.andIsDeletedEqualTo(BaseConstants.NO).andParentIdEqualTo("0")
				.andStatusEqualTo(Byte.valueOf("1"));
		if (!StringUtil.isNull(module)) {
			memberCommentCriteria.andModuleEqualTo(module.getType());
		}
		if (!StringUtil.isNullOrEmpty(moduleId)) {
			memberCommentCriteria.andModuleIdEqualTo(moduleId);
		}
		return super.responseEntity(BaseResult.ok(super.page(memberCommentExample)));
	}

	@SystemOptionAnnotation(describe = "评论表列表数据")
	@ApiOperation(value = "评论表列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
		MemberCommentExample memberCommentExample = new MemberCommentExample();
		// 创造条件
		MemberCommentExample.Criteria memberCommentCriteria = memberCommentExample.createCriteria();
		memberCommentCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		memberCommentExample.setOrderByClause(StringUtil.getOrderByValue(sort));
		return super.responseEntity(BaseResult.ok(super.list(memberCommentExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "新增评论")
	@ApiOperation(value = "新增评论")
	@PostMapping(value = "save")
	@Authorization
	public ResponseEntity<BaseResult> save(HttpServletRequest request,
			@RequestBody @Valid MemberCommentDto memberCommentDto) {
		String ip = IpUtils.getIpAddr(request);
		return super.responseEntity(memberCommentService.create(memberCommentDto, getLoginUser(), ip));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除评论表")
	@ApiOperation(value = "删除评论表")
	@PostMapping(value = "remove")
	@Authorization
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		MemberCommentExample memberCommentExample = new MemberCommentExample();
		memberCommentExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(memberCommentExample));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "审核通过")
	@ApiOperation(value = "审核通过")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "pass/{id}")
	public ResponseEntity<BaseResult> pass(@PathVariable("id") String id) {
		return super.responseEntity(memberCommentService.changeStatus(id, Byte.valueOf("1"), getLoginUser()));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "审核屏蔽")
	@ApiOperation(value = "审核屏蔽")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "shield/{id}")
	public ResponseEntity<BaseResult> shield(@PathVariable("id") String id) {
		return super.responseEntity(memberCommentService.changeStatus(id, Byte.valueOf("2"), getLoginUser()));
	}

}