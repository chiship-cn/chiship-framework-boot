package cn.chiship.framework.business.biz.cashier.mapper;

import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrder;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrderExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-07-23
 */
public interface BusinessRefundOrderMapper extends BaseMapper<BusinessRefundOrder, BusinessRefundOrderExample> {

}
