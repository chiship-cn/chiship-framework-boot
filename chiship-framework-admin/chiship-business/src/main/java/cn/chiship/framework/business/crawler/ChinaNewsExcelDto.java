package cn.chiship.framework.business.crawler;

import com.alibaba.excel.annotation.ExcelProperty;

/**
 * 中国新闻 Excel解析实体
 *
 * @author lijian
 */
public class ChinaNewsExcelDto {

	@ExcelProperty(value = "标题")
	private String title;

	@ExcelProperty(value = "类型")
	private String type;

	@ExcelProperty(value = "时间")
	private String time;

	@ExcelProperty(value = "链接")
	private String keywords;

	@ExcelProperty(value = "链接")
	private String link;

	@ExcelProperty(value = "描述")
	private String description;

	@ExcelProperty(value = "内容")
	private String content;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
