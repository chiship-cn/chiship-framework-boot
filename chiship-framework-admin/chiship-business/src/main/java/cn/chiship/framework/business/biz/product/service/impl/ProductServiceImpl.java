package cn.chiship.framework.business.biz.product.service.impl;

import cn.chiship.framework.business.biz.product.service.ProductCategoryService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.product.mapper.ProductMapper;
import cn.chiship.framework.business.biz.product.entity.Product;
import cn.chiship.framework.business.biz.product.entity.ProductExample;
import cn.chiship.framework.business.biz.product.service.ProductService;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.github.pagehelper.page.PageMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 商品业务接口实现层
 * 2024/12/11
 *
 * @author lijian
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product, ProductExample> implements ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    @Resource
    ProductMapper productMapper;
    @Resource
    ProductCategoryService productCategoryService;

    @Override
    public BaseResult page(PageVo pageVo, Map<String, String> paramsMap) {
        Page<Object> pageResult = PageMethod.startPage(pageVo.getCurrent().intValue(), pageVo.getSize().intValue());
        List<Product> products = productMapper.select(paramsMap);
        List<JSONObject> results = new ArrayList<>();
        for (Product product : products) {
            JSONObject json = JSON.parseObject(JSON.toJSONString(product));
            json.putAll(productCategoryService.getParentInfoById(product.getCategoryId()));
            results.add(json);
        }
        pageVo.setRecords(results);
        pageVo.setTotal(pageResult.getTotal());
        pageVo.setPages((long) pageResult.getPages());
        return BaseResult.ok(pageVo);
    }

    @Override
    public JSONObject assembleData(Product product) {
        JSONObject json = JSON.parseObject(JSON.toJSONString(product));
        json.putAll(productCategoryService.getParentInfoById(product.getCategoryId()));
        return json;
    }

    @Override
    public Product selectByPrimaryKey(Object id) {
        ProductExample productExample = new ProductExample();
        productExample.createCriteria().andIdEqualTo(id.toString());
        List<Product> products = productMapper.selectByExampleWithBLOBs(productExample);
        if (products.isEmpty()) {
            return new Product();
        }
        return products.get(0);
    }

    @Override
    public BaseResult selectDetailsByPrimaryKey(Object id) {
        Product product = selectByPrimaryKey(id);
        if (StringUtil.isNullOrEmpty(product.getId())) {
            return BaseResult.error("无效的商品");
        }
        JSONObject json = JSON.parseObject(JSON.toJSONString(product));
        json.putAll(productCategoryService.getParentInfoById(product.getCategoryId()));
        return BaseResult.ok(json);
    }

    @Override
    public BaseResult putShelves(String id) {
        Product product = selectByPrimaryKey(id);
        product.setIsSale(Byte.valueOf("1"));
        productMapper.updateByPrimaryKeySelective(product);
        return BaseResult.ok();
    }

    @Override
    public BaseResult removeShelves(String id) {
        Product product = selectByPrimaryKey(id);
        product.setIsSale(Byte.valueOf("0"));
        productMapper.updateByPrimaryKeySelective(product);
        return BaseResult.ok();
    }
}
