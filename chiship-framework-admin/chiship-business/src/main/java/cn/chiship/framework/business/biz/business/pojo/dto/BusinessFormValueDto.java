package cn.chiship.framework.business.biz.business.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 自封有表单提交数据表单
 * 2024/12/13
 *
 * @author LiJian
 */
@ApiModel(value = "自封有表单提交数据表单")
public class BusinessFormValueDto {

    @ApiModelProperty(value = "所属表单", required = true)
    @NotEmpty(message = "所属表单")
    @Length(min = 1, max = 36)
    private String formId;

    @ApiModelProperty(value = "表单内容", required = true)
    @NotEmpty(message = "表单内容")
    @Length(min = 1)
    private String formValue;

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormValue() {
        return formValue;
    }

    public void setFormValue(String formValue) {
        this.formValue = formValue;
    }
}