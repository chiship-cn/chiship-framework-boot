package cn.chiship.framework.business.biz.content.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-31
 */
public class ContentAdvert implements Serializable {

	/**
	 * ID
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 创建者
	 */
	private String createdBy;

	/**
	 * 更新者
	 */
	private String modifyBy;

	/**
	 * 插槽主键
	 */
	private String slotId;

	/**
	 * 图片
	 */
	private String imageUrl;

	/**
	 * 链接
	 */
	private String linkUrl;

	/**
	 * 文字
	 */
	private String text;

	/**
	 * 开始时间
	 */
	private Long beginDate;

	/**
	 * 结束时间
	 */
	private Long endDate;

	/**
	 * 排序
	 */
	private Integer seq;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public String getSlotId() {
		return slotId;
	}

	public void setSlotId(String slotId) {
		this.slotId = slotId;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Long getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Long beginDate) {
		this.beginDate = beginDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", modifyBy=").append(modifyBy);
		sb.append(", slotId=").append(slotId);
		sb.append(", imageUrl=").append(imageUrl);
		sb.append(", linkUrl=").append(linkUrl);
		sb.append(", text=").append(text);
		sb.append(", beginDate=").append(beginDate);
		sb.append(", endDate=").append(endDate);
		sb.append(", seq=").append(seq);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ContentAdvert other = (ContentAdvert) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getModifyBy() == null ? other.getModifyBy() == null
						: this.getModifyBy().equals(other.getModifyBy()))
				&& (this.getSlotId() == null ? other.getSlotId() == null : this.getSlotId().equals(other.getSlotId()))
				&& (this.getImageUrl() == null ? other.getImageUrl() == null
						: this.getImageUrl().equals(other.getImageUrl()))
				&& (this.getLinkUrl() == null ? other.getLinkUrl() == null
						: this.getLinkUrl().equals(other.getLinkUrl()))
				&& (this.getText() == null ? other.getText() == null : this.getText().equals(other.getText()))
				&& (this.getBeginDate() == null ? other.getBeginDate() == null
						: this.getBeginDate().equals(other.getBeginDate()))
				&& (this.getEndDate() == null ? other.getEndDate() == null
						: this.getEndDate().equals(other.getEndDate()))
				&& (this.getSeq() == null ? other.getSeq() == null : this.getSeq().equals(other.getSeq()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
		result = prime * result + ((getSlotId() == null) ? 0 : getSlotId().hashCode());
		result = prime * result + ((getImageUrl() == null) ? 0 : getImageUrl().hashCode());
		result = prime * result + ((getLinkUrl() == null) ? 0 : getLinkUrl().hashCode());
		result = prime * result + ((getText() == null) ? 0 : getText().hashCode());
		result = prime * result + ((getBeginDate() == null) ? 0 : getBeginDate().hashCode());
		result = prime * result + ((getEndDate() == null) ? 0 : getEndDate().hashCode());
		result = prime * result + ((getSeq() == null) ? 0 : getSeq().hashCode());
		return result;
	}

}