package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeEnum;
import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeModuleEnum;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;

/**
 * 收藏点赞表单 2024/11/21
 *
 * @author LiJian
 */
@ApiModel(value = "收藏点赞表单")
public class MemberCollectionLikeDto {

	@ApiModelProperty(value = "类型枚举", required = true)
	@NotNull(message = "类型枚举" + BaseTipConstants.NOT_EMPTY)
	private MemberCollectionLikeEnum typeEnum;

	@ApiModelProperty(value = "模块枚举", required = true)
	@NotNull(message = "模块枚举" + BaseTipConstants.NOT_EMPTY)
	private MemberCollectionLikeModuleEnum moduleEnum;

	@ApiModelProperty(value = "模块对应的id", required = true)
	@NotEmpty(message = "模块对应的id" + BaseTipConstants.NOT_EMPTY)
	private String moduleId;

	public MemberCollectionLikeEnum getTypeEnum() {
		return typeEnum;
	}

	public void setTypeEnum(MemberCollectionLikeEnum typeEnum) {
		this.typeEnum = typeEnum;
	}

	public MemberCollectionLikeModuleEnum getModuleEnum() {
		return moduleEnum;
	}

	public void setModuleEnum(MemberCollectionLikeModuleEnum moduleEnum) {
		this.moduleEnum = moduleEnum;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

}