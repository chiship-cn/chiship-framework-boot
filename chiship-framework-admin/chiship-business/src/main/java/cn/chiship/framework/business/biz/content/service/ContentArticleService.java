package cn.chiship.framework.business.biz.content.service;

import cn.chiship.framework.business.biz.content.enums.ContentStatusEnum;
import cn.chiship.framework.business.biz.content.pojo.dto.ContentSyncWxPubDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.content.entity.ContentArticle;
import cn.chiship.framework.business.biz.content.entity.ContentArticleExample;
import cn.chiship.sdk.framework.pojo.vo.PageVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * 文档详细表业务接口层 2024/10/19
 *
 * @author lijian
 */
public interface ContentArticleService extends BaseService<ContentArticle, ContentArticleExample> {

    /**
     * 分页
     *
     * @param pageVo    分页
     * @param paramsMap 参数
     * @return BaseResult
     */
    BaseResult page(PageVo pageVo, Map<String, String> paramsMap);

    /**
     * 修改文章状态
     *
     * @param ids               主键集合
     * @param contentStatusEnum 状态
     * @return BaseResult
     */
    BaseResult changeStatus(List<String> ids, ContentStatusEnum contentStatusEnum);

    /**
     * 移入回收站
     *
     * @param ids 主键集合
     * @return BaseResult
     */
    BaseResult moveRecycleBin(List<String> ids);

    /**
     * 回收站恢复
     *
     * @param ids 主键集合
     * @return BaseResult
     */
    BaseResult recovery(List<String> ids);

    /**
     * 文章预览
     *
     * @param id          主键
     * @param cacheUserVO 用户
     * @return BaseResult
     */
    BaseResult view(HttpServletRequest request, String id, CacheUserVO cacheUserVO);

    /**
     * 时间维度统计文章数量
     *
     * @param dateTime 时间戳
     * @return BaseResult
     */
    BaseResult analysisByTime(Long dateTime);

    /**
     * 上一篇及下一篇
     *
     * @param id         主键
     * @param categoryId 分类频道 all：所有频道
     * @param isAll      是否加载所有
     * @return
     */
    BaseResult findPreAndNext(String id, String categoryId, Boolean isAll);

    /**
     * 设置是否允许评论
     *
     * @param id           主键
     * @param allowComment 是否允许
     * @return
     */
    BaseResult setAllowComment(String id, Boolean allowComment);

    /**
     * 文章同步至微信素材库
     *
     * @param contentSyncWxPubDto
     * @return
     */
    BaseResult syncWxPub(ContentSyncWxPubDto contentSyncWxPubDto);

    /**
     * 文章从微信素材库撤销
     *
     * @param id
     * @return
     */
    BaseResult revokeWxPub(String id);

}
