package cn.chiship.framework.business.biz.member.enmus;

/**
 * 用户钱包变化模块枚举
 *
 * @author lijian
 */
public enum MemberWalletChangeModuleEnum {

    MEMBER_WALLET_CHANGE_MODULE_WITHDRAWAL("withdrawal", "提现申请"),
    MEMBER_WALLET_CHANGE_MODULE_RECHARGE("recharge", "钱包充值"),
    ;

    /**
     * 业务类型
     */
    private String type;

    /**
     * 业务描述
     */
    private String message;

    MemberWalletChangeModuleEnum(String type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

}
