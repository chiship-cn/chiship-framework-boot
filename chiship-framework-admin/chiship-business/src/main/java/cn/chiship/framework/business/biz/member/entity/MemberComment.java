package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-11-23
 */
public class MemberComment implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 修改时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 父评论ID
	 */
	private String parentId;

	/**
	 * 审核人
	 */
	private String auditorId;

	/**
	 * 审核时间
	 */
	private Long auditDate;

	/**
	 * IP地址
	 */
	private String ip;

	/**
	 * IP归属地
	 */
	private String ipAddress;

	/**
	 * 0:未审核;1:已审核;2:推荐;3:屏蔽
	 */
	private Byte status;

	/**
	 * 层级树
	 */
	private String treeNumber;

	/**
	 * 内容
	 */
	private String text;

	/**
	 * 模块
	 */
	private String module;

	/**
	 * 模块对应的id
	 */
	private String moduleId;

	/**
	 * 用户
	 */
	private String createdUserId;

	/**
	 * 用户
	 */
	private String createdUserName;

	/**
	 * 用户头像
	 */
	private String createdUserAvatar;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getAuditorId() {
		return auditorId;
	}

	public void setAuditorId(String auditorId) {
		this.auditorId = auditorId;
	}

	public Long getAuditDate() {
		return auditDate;
	}

	public void setAuditDate(Long auditDate) {
		this.auditDate = auditDate;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getTreeNumber() {
		return treeNumber;
	}

	public void setTreeNumber(String treeNumber) {
		this.treeNumber = treeNumber;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getCreatedUserName() {
		return createdUserName;
	}

	public void setCreatedUserName(String createdUserName) {
		this.createdUserName = createdUserName;
	}

	public String getCreatedUserAvatar() {
		return createdUserAvatar;
	}

	public void setCreatedUserAvatar(String createdUserAvatar) {
		this.createdUserAvatar = createdUserAvatar;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", parentId=").append(parentId);
		sb.append(", auditorId=").append(auditorId);
		sb.append(", auditDate=").append(auditDate);
		sb.append(", ip=").append(ip);
		sb.append(", ipAddress=").append(ipAddress);
		sb.append(", status=").append(status);
		sb.append(", treeNumber=").append(treeNumber);
		sb.append(", text=").append(text);
		sb.append(", module=").append(module);
		sb.append(", moduleId=").append(moduleId);
		sb.append(", createdUserId=").append(createdUserId);
		sb.append(", createdUserName=").append(createdUserName);
		sb.append(", createdUserAvatar=").append(createdUserAvatar);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MemberComment other = (MemberComment) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getParentId() == null ? other.getParentId() == null
						: this.getParentId().equals(other.getParentId()))
				&& (this.getAuditorId() == null ? other.getAuditorId() == null
						: this.getAuditorId().equals(other.getAuditorId()))
				&& (this.getAuditDate() == null ? other.getAuditDate() == null
						: this.getAuditDate().equals(other.getAuditDate()))
				&& (this.getIp() == null ? other.getIp() == null : this.getIp().equals(other.getIp()))
				&& (this.getIpAddress() == null ? other.getIpAddress() == null
						: this.getIpAddress().equals(other.getIpAddress()))
				&& (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
				&& (this.getTreeNumber() == null ? other.getTreeNumber() == null
						: this.getTreeNumber().equals(other.getTreeNumber()))
				&& (this.getText() == null ? other.getText() == null : this.getText().equals(other.getText()))
				&& (this.getModule() == null ? other.getModule() == null : this.getModule().equals(other.getModule()))
				&& (this.getModuleId() == null ? other.getModuleId() == null
						: this.getModuleId().equals(other.getModuleId()))
				&& (this.getCreatedUserId() == null ? other.getCreatedUserId() == null
						: this.getCreatedUserId().equals(other.getCreatedUserId()))
				&& (this.getCreatedUserName() == null ? other.getCreatedUserName() == null
						: this.getCreatedUserName().equals(other.getCreatedUserName()))
				&& (this.getCreatedUserAvatar() == null ? other.getCreatedUserAvatar() == null
						: this.getCreatedUserAvatar().equals(other.getCreatedUserAvatar()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getParentId() == null) ? 0 : getParentId().hashCode());
		result = prime * result + ((getAuditorId() == null) ? 0 : getAuditorId().hashCode());
		result = prime * result + ((getAuditDate() == null) ? 0 : getAuditDate().hashCode());
		result = prime * result + ((getIp() == null) ? 0 : getIp().hashCode());
		result = prime * result + ((getIpAddress() == null) ? 0 : getIpAddress().hashCode());
		result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
		result = prime * result + ((getTreeNumber() == null) ? 0 : getTreeNumber().hashCode());
		result = prime * result + ((getText() == null) ? 0 : getText().hashCode());
		result = prime * result + ((getModule() == null) ? 0 : getModule().hashCode());
		result = prime * result + ((getModuleId() == null) ? 0 : getModuleId().hashCode());
		result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
		result = prime * result + ((getCreatedUserName() == null) ? 0 : getCreatedUserName().hashCode());
		result = prime * result + ((getCreatedUserAvatar() == null) ? 0 : getCreatedUserAvatar().hashCode());
		return result;
	}

}