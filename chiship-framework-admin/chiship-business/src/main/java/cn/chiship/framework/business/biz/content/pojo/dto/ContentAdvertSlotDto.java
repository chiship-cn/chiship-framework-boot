package cn.chiship.framework.business.biz.content.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.*;

/**
 * 广告版位表表单 2024/10/31
 *
 * @author LiJian
 */
@ApiModel(value = "广告版位表表单")
public class ContentAdvertSlotDto {

	@ApiModelProperty(value = "编码", required = true)
	private String code;

	@ApiModelProperty(value = "名称", required = true)
	private String name;

	@ApiModelProperty(value = "长度", required = true)
	@Min(1)
	private Integer width;

	@ApiModelProperty(value = "宽度", required = true)
	@Min(1)
	private Integer height;

	@ApiModelProperty(value = "描述")
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public @Min(1) Integer getWidth() {
		return width;
	}

	public void setWidth(@Min(1) Integer width) {
		this.width = width;
	}

	public @Min(1) Integer getHeight() {
		return height;
	}

	public void setHeight(@Min(1) Integer height) {
		this.height = height;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}