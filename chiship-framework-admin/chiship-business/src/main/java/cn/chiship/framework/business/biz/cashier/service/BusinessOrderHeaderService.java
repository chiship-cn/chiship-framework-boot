package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.framework.business.biz.cashier.pojo.dto.OrderProductCreateDto;
import cn.chiship.framework.business.biz.cashier.pojo.dto.OrderRechargeCreateDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderExample;

/**
 * 订单主表业务接口层 2023/3/26
 *
 * @author lijian
 */
public interface BusinessOrderHeaderService extends BaseService<BusinessOrderHeader, BusinessOrderHeaderExample> {

	/**
	 * 充值订单创建
	 * @param orderRechargeCreateDto
	 * @param cacheUserVO
	 * @return
	 */
	BaseResult createOrder(OrderRechargeCreateDto orderRechargeCreateDto, CacheUserVO cacheUserVO);

	/**
	 * 商品订单创建
	 * @param orderProductCreateDto
	 * @param cacheUserVO
	 * @return
	 */
	BaseResult createOrder(OrderProductCreateDto orderProductCreateDto, CacheUserVO cacheUserVO);


	/**
	 * 根据订单号查询订单状态
	 * @param orderId
	 * @return
	 */
	BaseResult getOrderStatusByOrderId(String orderId);

	/**
	 * 关闭订单
	 * @param orderNo
	 * @return
	 */
	BaseResult closedOrder(String orderNo);

}
