package cn.chiship.framework.business.biz.business.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 自封有表单提交数据表单
 * 2024/12/13
 *
 * @author LiJian
 */
@ApiModel(value = "游客表单提交数据表单")
public class BusinessFormValueTouristDto extends BusinessFormValueDto {

    @ApiModelProperty(value = "填报人真实姓名", required = true)
    @NotEmpty(message = "填报人真实姓名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 2, max = 5)
    private String fillRealName;

    @ApiModelProperty(value = "填报人手机号", required = true)
    @NotEmpty(message = "填报人手机号" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 11, max = 11)
    @Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
    private String fillMobile;

    public String getFillRealName() {
        return fillRealName;
    }

    public void setFillRealName(String fillRealName) {
        this.fillRealName = fillRealName;
    }

    public String getFillMobile() {
        return fillMobile;
    }

    public void setFillMobile(String fillMobile) {
        this.fillMobile = fillMobile;
    }
}