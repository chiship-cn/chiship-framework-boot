package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrder;
import cn.chiship.framework.business.biz.cashier.enums.OrderStatusEnum;
import cn.chiship.framework.business.biz.cashier.enums.PayStatusEnum;
import cn.chiship.framework.business.biz.cashier.enums.RefundStatusEnum;
import cn.chiship.framework.business.biz.cashier.pojo.dto.*;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderHeaderService;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderPaymentRecordService;
import cn.chiship.framework.business.biz.cashier.service.BusinessRefundOrderService;
import cn.chiship.framework.business.biz.cashier.service.PayService;
import cn.chiship.framework.business.biz.member.service.MemberUserWithdrawalApplicationService;
import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.pay.core.enums.PayEnum;
import cn.chiship.sdk.pay.core.enums.PayNotifyEnum;
import cn.chiship.sdk.pay.core.enums.TradeTypeEnum;
import cn.chiship.sdk.pay.core.model.PayDTO;
import cn.chiship.sdk.pay.core.model.PayParamsModel;
import cn.chiship.sdk.pay.core.model.PayRefundModel;
import cn.chiship.sdk.pay.factory.PayFactory;
import cn.chiship.sdk.pay.services.PaymentService;
import cn.chiship.sdk.third.properties.ChishipZfbMpProperties;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 */
@Service
public class PayServiceImpl implements PayService {

    public static final String SUCCESS = "SUCCESS";

    @Value("${chiship.pay.domain.notify}")
    private String notifyDomain;

    @Value("${chiship.pay.wx.app-id2}")
    private String wxAppId2;

    @Resource
    ChishipZfbMpProperties chishipZfbMpProperties;

    @Resource
    BusinessOrderHeaderService businessOrderHeaderService;

    @Resource
    BusinessOrderPaymentRecordService businessOrderPaymentRecordService;

    @Resource
    BusinessRefundOrderService businessRefundOrderService;

    @Resource
    MemberUserWithdrawalApplicationService memberUserWithdrawalApplicationService;

    @Resource
    AsyncCashierService asyncCashierService;

    @Resource
    RedisService redisService;

    @Override
    public BaseResult createOrder(OrderRechargeCreateDto orderCreateDto, CacheUserVO cacheUserVO) {
        return businessOrderHeaderService.createOrder(orderCreateDto, cacheUserVO);
    }

    @Override
    public BaseResult getOrderStatusByOrderId(String orderId) {
        return businessOrderHeaderService.getOrderStatusByOrderId(orderId);
    }

    @Override
    public BaseResult normalPay(TradeTypeEnum tradeTypeEnum, NormalPayDto normalPayDto, String ip) {
        if (!Arrays.asList(TradeTypeEnum.QR_CODE, TradeTypeEnum.H5).contains(tradeTypeEnum)) {
            return BaseResult.ok("普通支付不支持" + tradeTypeEnum.getMessage());
        }
        String orderNo = normalPayDto.getOrderNo();
        String payMethod = normalPayDto.getPayMethod();
        BusinessOrderHeader orderHeader = businessOrderHeaderService.selectByPrimaryKey(orderNo);
        if (StringUtil.isNull(orderHeader)) {
            return BaseResult.error("订单不存在!");
        }
        if (!OrderStatusEnum.ORDER_STATUS_WAIT_PAY.getStatus().equals(orderHeader.getOrderStatus())) {
            return BaseResult
                    .error("该订单" + OrderStatusEnum.getOrderStatusEnum(orderHeader.getOrderStatus()) + "，无法付款!");
        }
        PaymentService paymentService = PayFactory.getPaymentService(PayEnum.getPayEnum(payMethod));

        PayParamsModel payParamsModel = new PayParamsModel(orderNo, orderHeader.getOrderName(),
                orderHeader.getOrderName(), orderHeader.getGrandTotal().doubleValue(), notifyDomain, null);
        if (!StringUtil.isNullOrEmpty(normalPayDto.getReturnUrl())) {
            payParamsModel.setReturnUrl(normalPayDto.getReturnUrl());
        }
        return paymentService.doPay(payParamsModel, tradeTypeEnum);
    }

    @Override
    public BaseResult prepay(PrepayDto prepayDto, String ip) {
        String orderNo = prepayDto.getOrderNo();
        String openId = prepayDto.getOpenId();
        PayEnum payEnum = PayEnum.getPayEnum(prepayDto.getPayMethod());
        TradeTypeEnum tradeTypeEnum = TradeTypeEnum.getTradeTypeEnum(prepayDto.getTradeType());

        if (!Arrays.asList(TradeTypeEnum.OFFICIAL_ACCOUNT, TradeTypeEnum.MINI_PROGRAM).contains(tradeTypeEnum)) {
            return BaseResult.error(tradeTypeEnum.getMessage() + "不支持预支付交易");
        }
        BusinessOrderHeader orderHeader = businessOrderHeaderService.selectByPrimaryKey(orderNo);
        if (StringUtil.isNull(orderHeader)) {
            return BaseResult.error("订单不存在!");
        }
        if (!OrderStatusEnum.ORDER_STATUS_WAIT_PAY.getStatus().equals(orderHeader.getOrderStatus())) {
            return BaseResult
                    .error("该订单" + OrderStatusEnum.getOrderStatusEnum(orderHeader.getOrderStatus()) + "，无法付款!");
        }

        PaymentService paymentService;
        PayDTO payDTO = null;
        if (PayEnum.PAY_WX_V3.getType().equals(payEnum.getType())) {
            try {
                payDTO = new PayDTO(ThirdUtil.getAppId());
            } catch (BusinessException e) {
                // 若没有传递appId参数则使用默认应用
                if (TradeTypeEnum.OFFICIAL_ACCOUNT.getCode().equals(tradeTypeEnum.getCode())) {
                    payDTO = new PayDTO(wxAppId2);
                }
            }
        } else if (PayEnum.PAY_ZFB.getType().equals(payEnum.getType())) {
            payDTO = new PayDTO(
                    chishipZfbMpProperties.getAppId(),
                    chishipZfbMpProperties.getPrivateKey(),
                    chishipZfbMpProperties.getPublicKey());
        }
        paymentService = PayFactory.getPaymentService(payEnum, payDTO);

        PayParamsModel payParamsModel = new PayParamsModel(orderNo, orderHeader.getOrderName(),
                orderHeader.getOrderName(), orderHeader.getGrandTotal().doubleValue(), notifyDomain, null);
        payParamsModel.setOpenId(openId);
        return paymentService.doPay(payParamsModel, tradeTypeEnum);
    }

    @Override
    public BaseResult refund(RefundDto refundDto, CacheUserVO cacheUserVO) {
        BusinessOrderHeader orderHeader = businessOrderHeaderService.selectByPrimaryKey(refundDto.getOrderNo());
        if (StringUtil.isNull(orderHeader)) {
            return BaseResult.error("订单不存在!");
        }
        String tradeNo = orderHeader.getTradeNo();
        String channelPayWay = orderHeader.getChannelPayWay();

        String refundNo = SnowflakeIdUtil.generateStrId();
        PayRefundModel payRefundModel = new PayRefundModel(refundNo, tradeNo, orderHeader.getGrandTotal().doubleValue(),
                refundDto.getRefundAmount(), refundDto.getRefundReason(), notifyDomain);
        BusinessRefundOrderDto refundOrderDto = new BusinessRefundOrderDto(refundNo, orderHeader.getId(), tradeNo,
                BigDecimal.valueOf(refundDto.getRefundAmount()), refundDto.getRefundReason(),
                orderHeader.getChannelPayWay());
        businessRefundOrderService.createRefund(refundOrderDto, cacheUserVO);
        PaymentService paymentService = PayFactory.getPaymentService(PayEnum.getPayEnum(channelPayWay));
        BaseResult baseResult = paymentService.doRefund(payRefundModel);
        if (baseResult.isSuccess()) {
            dealRefundResult((JSONObject) baseResult.getData(), channelPayWay, orderHeader);
            if (PayEnum.PAY_ZFB.getType().equals(channelPayWay)) {
                baseResult = paymentService.doRefundQuery(refundNo, orderHeader.getId());
                dealRefundResult((JSONObject) baseResult.getData(), channelPayWay, orderHeader);
            }
            return BaseResult.ok();
        } else {
            return baseResult;
        }

    }

    @Override
    public BaseResult withdrawal(WithdrawalApplicationDto withdrawalApplicationDto, CacheUserVO cacheUserVO) {
        return memberUserWithdrawalApplicationService.withdrawalApplication(withdrawalApplicationDto, cacheUserVO);
    }

    @Override
    public BaseResult reconciliationOrder(String orderNo, String channelPayWay) {
        BusinessOrderHeader orderHeader = businessOrderHeaderService.selectByPrimaryKey(orderNo);
        if (StringUtil.isNull(orderHeader)) {
            return BaseResult.error("订单不存在!");
        }
        PaymentService paymentService = PayFactory.getPaymentService(PayEnum.getPayEnum(channelPayWay));
        return paymentService.doQuery(orderNo, true);
    }

    @Override
    public BaseResult reconciliationRefund(String refundNo) {
        BusinessRefundOrder businessRefundOrder = businessRefundOrderService.selectByPrimaryKey(refundNo);
        if (StringUtil.isNull(businessRefundOrder)) {
            return BaseResult.error("退款单不存在!");
        }

        PaymentService paymentService = PayFactory
                .getPaymentService(PayEnum.getPayEnum(businessRefundOrder.getChannelPayWay()));
        BaseResult baseResult = paymentService.doRefundQuery(businessRefundOrder.getId(),
                businessRefundOrder.getOrderNo());
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        BusinessOrderHeader orderHeader = businessOrderHeaderService
                .selectByPrimaryKey(businessRefundOrder.getOrderNo());
        if (!RefundStatusEnum.REFUND_SUCCESS.getStatus().equals(businessRefundOrder.getRefundStatus())) {
            dealRefundResult((JSONObject) baseResult.getData(), businessRefundOrder.getChannelPayWay(), orderHeader);
        }
        return baseResult;
    }

    @Override
    public void dealWxPayNotify(String tradeType, BaseResult baseResult) {
        BusinessPayNotifyDto payNotifyDto = new BusinessPayNotifyDto(System.currentTimeMillis(),
                PayNotifyEnum.PAY_NOTIFY, PayEnum.PAY_WX_V3, JSON.toJSONString(baseResult.getData()));
        asyncCashierService.savePayNotify(payNotifyDto);
        if (baseResult.isSuccess()) {
            JSONObject json = (JSONObject) baseResult.getData();
            String outTradeNo = json.getString("out_trade_no");
            BusinessOrderPaymentRecordDto orderPaymentRecordDto = new BusinessOrderPaymentRecordDto();
            orderPaymentRecordDto.setOrderId(outTradeNo);
            String tradeState = json.getString("trade_state");
            if (tradeState.equals(SUCCESS)) {
                String format = new DecimalFormat("#0.00")
                        .format(json.getJSONObject("amount").getDouble("total") / 100);
                BigDecimal totalFee = new BigDecimal(format);
                orderPaymentRecordDto.setTradeNo(json.getString("transaction_id"));
                orderPaymentRecordDto.setTradeType(tradeType);
                orderPaymentRecordDto.setChannelPayWay(PayEnum.PAY_WX_V3.getType());
                orderPaymentRecordDto.setPayStatus(PayStatusEnum.PAY_STATUS_SUCCESS.getStatus());

                orderPaymentRecordDto.setTotalFee(totalFee);

                orderPaymentRecordDto.setAppId(json.getString("appid"));
                orderPaymentRecordDto.setPartnerId(json.getString("mchid"));
                orderPaymentRecordDto.setUserId(json.getJSONObject("payer").getString("openid"));
                orderPaymentRecordDto.setPayTime(DateUtils
                        .dateTime(DateUtils.YYYY_MM_DD_HH_MM_SS, json.getString("success_time").replace("T", " "))
                        .getTime());
                orderPaymentRecordDto.setSubject(json.getString("attach"));

            } else {
                orderPaymentRecordDto.setPayStatus(PayStatusEnum.PAY_STATUS_FAIL.getStatus());
            }
            businessOrderPaymentRecordService.payNotify(orderPaymentRecordDto);
        } else {
            PrintUtil.console("微信支付异步通知解密失败！");

        }
    }

    @Override
    public void dealWxTransferNotify(BaseResult baseResult) {
        JSONObject resultJson = (JSONObject) baseResult.getData();
        memberUserWithdrawalApplicationService.reconciliation(resultJson.getString("out_batch_no"));
    }

    @Override
    public void dealWxRefundNotify(BaseResult baseResult) {
        BusinessPayNotifyDto payNotifyDto = new BusinessPayNotifyDto(System.currentTimeMillis(),
                PayNotifyEnum.PAY_REFUND, PayEnum.PAY_WX_V3, JSON.toJSONString(baseResult.getData()));
        asyncCashierService.savePayNotify(payNotifyDto);
        if (!baseResult.isSuccess()) {
            return;
        }
        JSONObject refundResultJson = (JSONObject) baseResult.getData();
        String refundNo = refundResultJson.getString("out_refund_no");
        String refundStatus = refundResultJson.getString("refund_status");
        String orderNo = refundResultJson.getString("out_trade_no");
        if ("SUCCESS".equals(refundStatus)) {
            BusinessOrderHeader orderHeader = businessOrderHeaderService.selectByPrimaryKey(orderNo);
            String format = new DecimalFormat("#0.00")
                    .format(refundResultJson.getJSONObject("amount").getDouble("refund") / 100);
            BigDecimal refundAmount = new BigDecimal(format);
            Long payTime = DateUtils
                    .dateTime("yyyy-MM-dd HH:mm:ss", refundResultJson.getString("success_time").replace("T", " "))
                    .getTime();
            businessRefundOrderService.refundSuccess(refundNo, refundAmount, refundResultJson.getString("refund_id"),
                    refundResultJson.getString("user_received_account"), payTime);
            orderHeader.setRefundTotal(orderHeader.getRefundTotal().add(refundAmount));
            businessOrderHeaderService.updateByPrimaryKeySelective(orderHeader);
        } else if ("PROCESSING".equals(refundStatus)) {
            businessRefundOrderService.refundNotifyStatus(refundNo, RefundStatusEnum.REFUND_PROCESSING);
        } else if ("CLOSED".equals(refundStatus)) {
            businessRefundOrderService.refundNotifyStatus(refundNo, RefundStatusEnum.REFUND_CLOSED);
        }
    }

    @Override
    public void dealAliPayNotify(String tradeType, Map<String, Object> dataMap, HttpServletResponse response)
            throws IOException {
        BusinessPayNotifyDto payNotifyDto = new BusinessPayNotifyDto(System.currentTimeMillis(),
                PayNotifyEnum.PAY_NOTIFY, PayEnum.PAY_ZFB, JSON.toJSONString(dataMap));
        asyncCashierService.savePayNotify(payNotifyDto);
        String tradeStatus = StringUtil.getString(dataMap.get("trade_status"));
        String outTradeNo = StringUtil.getString(dataMap.get("out_trade_no"));
        BusinessOrderPaymentRecordDto orderPaymentRecordDto = new BusinessOrderPaymentRecordDto();
        orderPaymentRecordDto.setOrderId(outTradeNo);
        if ("TRADE_SUCCESS".equals(tradeStatus)) {
            String tradeNo = StringUtil.getString(dataMap.get("trade_no"));
            Double totalFree = Double.valueOf(StringUtil.getString(dataMap.get("total_amount")));
            String subject = StringUtil.getString(dataMap.get("subject"));
            // 应用标识
            String appId = StringUtil.getString(dataMap.get("app_id"));
            // 商户号
            String sellerId = StringUtil.getString(dataMap.get("seller_id"));
            // 商户账号
            String sellerEmail = StringUtil.getString(dataMap.get("seller_email"));
            // 付款号
            String buyerId = StringUtil.getString(dataMap.get("buyer_id"));
            orderPaymentRecordDto.setTradeNo(tradeNo);
            orderPaymentRecordDto.setTradeType(tradeType);
            orderPaymentRecordDto.setChannelPayWay(PayEnum.PAY_ZFB.getType());
            orderPaymentRecordDto.setPayStatus(PayStatusEnum.PAY_STATUS_SUCCESS.getStatus());
            orderPaymentRecordDto.setTotalFee(BigDecimal.valueOf(totalFree));

            orderPaymentRecordDto.setAppId(appId);
            orderPaymentRecordDto.setPartnerId(sellerId);
            orderPaymentRecordDto.setAccount(sellerEmail);
            orderPaymentRecordDto.setUserId(buyerId);
            orderPaymentRecordDto.setPayTime(DateUtils
                    .dateTime("yyyy-MM-dd HH:mm:ss", StringUtil.getString(dataMap.get("gmt_payment"))).getTime());
            orderPaymentRecordDto.setSubject(subject);
        } else {
            orderPaymentRecordDto.setPayStatus(PayStatusEnum.PAY_STATUS_FAIL.getStatus());
        }
        businessOrderPaymentRecordService.payNotify(orderPaymentRecordDto);
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.print("success");
        out.flush();
    }

    /**
     * 处理退款返回结果（正确结果）
     *
     * @param refundResultJson
     * @param channelPayWay
     * @param orderHeader
     */
    public void dealRefundResult(JSONObject refundResultJson, String channelPayWay, BusinessOrderHeader orderHeader) {
        if (PayEnum.PAY_WX_V3.getType().equals(channelPayWay)) {
            BusinessPayNotifyDto payNotifyDto = new BusinessPayNotifyDto(System.currentTimeMillis(),
                    PayNotifyEnum.PAY_REFUND, PayEnum.PAY_WX_V3, JSON.toJSONString(refundResultJson));
            asyncCashierService.savePayNotify(payNotifyDto);
            /**
             * SUCCESS: 退款成功 CLOSED: 退款关闭 PROCESSING: 退款处理中 ABNORMAL: 退款异常
             *
             * 退款申请返回 { "code": 200, "data": { "transaction_id":
             * "4200002231202406040781042805", "amount": { "payer_total": 1,
             * "settlement_total": 1, "total": 1, "payer_refund": 1, "refund_fee": 0,
             * "discount_refund": 0, "currency": "CNY", "from": [
             *
             * ], "settlement_refund": 1, "refund": 1 }, "out_trade_no":
             * "20240604584700014", "create_time": "2024-06-04T16:52:45+08:00",
             * "promotion_detail": [
             *
             * ], "out_refund_no": "957893340984500224", "funds_account": "AVAILABLE",
             * "channel": "ORIGINAL", "user_received_account": "支付用户零钱", "refund_id":
             * "50303709842024060409708752005", "status": "PROCESSING" }, "message":
             * "操作成功", "success": true }
             *
             * 退款查询 { "code": 200, "data": { "transaction_id":
             * "4200002231202406040781042805", "amount": { "payer_total": 1,
             * "settlement_total": 1, "total": 1, "payer_refund": 1, "refund_fee": 0,
             * "discount_refund": 0, "currency": "CNY", "from": [
             *
             * ], "settlement_refund": 1, "refund": 1 }, "out_trade_no":
             * "20240604584700014", "create_time": "2024-06-04T16:52:45+08:00",
             * "promotion_detail": [
             *
             * ], "out_refund_no": "957893340984500224", "funds_account": "AVAILABLE",
             * "channel": "ORIGINAL", "success_time": "2024-06-04T16:52:48+08:00",
             * "user_received_account": "支付用户零钱", "refund_id":
             * "50303709842024060409708752005", "status": "SUCCESS" }, "message": "操作成功",
             * "success": true }
             *
             */

            String refundNo = refundResultJson.getString("out_refund_no");
            String refundStatus = refundResultJson.getString("status");
            if ("SUCCESS".equals(refundStatus)) {
                String format = new DecimalFormat("#0.00")
                        .format(refundResultJson.getJSONObject("amount").getDouble("refund") / 100);
                BigDecimal refundAmount = new BigDecimal(format);
                Long payTime = DateUtils
                        .dateTime("yyyy-MM-dd HH:mm:ss", refundResultJson.getString("success_time").replace("T", " "))
                        .getTime();
                businessRefundOrderService.refundSuccess(refundNo, refundAmount,
                        refundResultJson.getString("refund_id"), refundResultJson.getString("user_received_account"),
                        payTime);
                orderHeader.setRefundTotal(orderHeader.getRefundTotal().add(refundAmount));
                businessOrderHeaderService.updateByPrimaryKeySelective(orderHeader);
            } else if ("PROCESSING".equals(refundStatus)) {
                businessRefundOrderService.refundNotifyStatus(refundNo, RefundStatusEnum.REFUND_PROCESSING);
            } else if ("CLOSED".equals(refundStatus)) {
                businessRefundOrderService.refundNotifyStatus(refundNo, RefundStatusEnum.REFUND_CLOSED);
            }
        }
        if (PayEnum.PAY_ZFB.getType().equals(channelPayWay)) {
            BusinessPayNotifyDto payNotifyDto = new BusinessPayNotifyDto(System.currentTimeMillis(),
                    PayNotifyEnum.PAY_REFUND, PayEnum.PAY_ZFB, JSON.toJSONString(refundResultJson));
            asyncCashierService.savePayNotify(payNotifyDto);
            /**
             * 退款发起返回 { "code": 200, "data": { "msg": "Success",
             * "refund_detail_item_list": [ { "amount": "0.01", "fund_channel":
             * "ALIPAYACCOUNT" } ], "code": "10000", "out_trade_no": "20240604632300022",
             * "refundNo": "958235980770828288", "refund_fee": "0.05", "gmt_refund_pay":
             * "2024-06-05 15:34:17", "send_back_fee": "0.01", "trade_no":
             * "2024060422001473621453797446", "buyer_logon_id": "183******21",
             * "buyer_user_id": "2088702563173626", "fund_change": "Y" }, "message":
             * "操作成功", "success": true }
             *
             * 退款查询 { "code": 200, "data": { "msg": "Success", "refund_detail_item_list":
             * [ { "amount": "0.01", "fund_channel": "ALIPAYACCOUNT" } ], "code": "10000",
             * "out_trade_no": "20240604632300022", "refund_status": "REFUND_SUCCESS",
             * "refundNo": "958235980770828288", "total_amount": "0.10", "refund_amount":
             * "0.01", "send_back_fee": "0.01", "trade_no":
             * "2024060422001473621453797446", "out_request_no": "958235980770828288" },
             * "message": "操作成功", "success": true }
             */
            String refundNo = refundResultJson.getString("out_request_no");
            if (refundResultJson.containsKey("refund_status")) {
                String refundStatus = refundResultJson.getString("refund_status");
                if ("REFUND_SUCCESS".equals(refundStatus)) {
                    String refundAmount = refundResultJson.getString("send_back_fee");
                    businessRefundOrderService.refundSuccess(refundNo, BigDecimal.valueOf(Double.valueOf(refundAmount)),
                            null, null, System.currentTimeMillis());
                    orderHeader.setRefundTotal(
                            orderHeader.getRefundTotal().add(BigDecimal.valueOf(Double.valueOf(refundAmount))));
                    businessOrderHeaderService.updateByPrimaryKeySelective(orderHeader);
                }
            }
        }
    }

    /**
     * 保存订单状态
     *
     * @param orderNo
     * @param orderStatusEnum
     */
    private void saveOrderStatus(String orderNo, OrderStatusEnum orderStatusEnum) {
        String key = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_ORDER_STATUS_PREFIX);
        Map<String, Object> status = new HashMap<>(7);
        status.put("status", orderStatusEnum.getStatus());
        status.put("message", orderStatusEnum.getMessage());
        redisService.hset(key, orderNo, status);
    }

}
