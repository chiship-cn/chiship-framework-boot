package cn.chiship.framework.business.biz.business.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2025-02-08
 */
public class BusinessForm implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 创建时间
     */
    private Long gmtCreated;

    /**
     * 更新时间
     */
    private Long gmtModified;

    /**
     * 逻辑删除（0：否，1：是）
     */
    private Byte isDeleted;

    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 分类ID
     */
    private String categoryId;

    /**
     * 是否重复提交（0：否，1：是）
     */
    private Byte isRepeatedSubmission;

    /**
     * 表单配置
     */
    private String formOptions;

    /**
     * H5表单配置
     */
    private String formOptionsH5;

    /**
     * 状态 0 正常 1 未发布
     */
    private Byte status;

    /**
     * 图标
     */
    private String icon;

    /**
     * 创建者
     */
    private String createdBy;

    /**
     * 更新者
     */
    private String modifyBy;

    /**
     * 创建者用户ID
     */
    private String createdUserId;

    /**
     * 创建者组织ID
     */
    private String createdOrgId;

    /**
     * 表单组件的规则描述
     */
    private String formDescription;

    /**
     * 表单内容
     */
    private String formContent;

    /**
     * 表单组件的规则描述
     */
    private String formDescriptionH5;

    /**
     * H5表单内容
     */
    private String formContentH5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Long gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public Byte getIsRepeatedSubmission() {
        return isRepeatedSubmission;
    }

    public void setIsRepeatedSubmission(Byte isRepeatedSubmission) {
        this.isRepeatedSubmission = isRepeatedSubmission;
    }

    public String getFormOptions() {
        return formOptions;
    }

    public void setFormOptions(String formOptions) {
        this.formOptions = formOptions;
    }

    public String getFormOptionsH5() {
        return formOptionsH5;
    }

    public void setFormOptionsH5(String formOptionsH5) {
        this.formOptionsH5 = formOptionsH5;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getCreatedOrgId() {
        return createdOrgId;
    }

    public void setCreatedOrgId(String createdOrgId) {
        this.createdOrgId = createdOrgId;
    }

    public String getFormDescription() {
        return formDescription;
    }

    public void setFormDescription(String formDescription) {
        this.formDescription = formDescription;
    }

    public String getFormContent() {
        return formContent;
    }

    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }

    public String getFormDescriptionH5() {
        return formDescriptionH5;
    }

    public void setFormDescriptionH5(String formDescriptionH5) {
        this.formDescriptionH5 = formDescriptionH5;
    }

    public String getFormContentH5() {
        return formContentH5;
    }

    public void setFormContentH5(String formContentH5) {
        this.formContentH5 = formContentH5;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreated=").append(gmtCreated);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", code=").append(code);
        sb.append(", name=").append(name);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", isRepeatedSubmission=").append(isRepeatedSubmission);
        sb.append(", formOptions=").append(formOptions);
        sb.append(", formOptionsH5=").append(formOptionsH5);
        sb.append(", status=").append(status);
        sb.append(", icon=").append(icon);
        sb.append(", createdBy=").append(createdBy);
        sb.append(", modifyBy=").append(modifyBy);
        sb.append(", createdUserId=").append(createdUserId);
        sb.append(", createdOrgId=").append(createdOrgId);
        sb.append(", formDescription=").append(formDescription);
        sb.append(", formContent=").append(formContent);
        sb.append(", formDescriptionH5=").append(formDescriptionH5);
        sb.append(", formContentH5=").append(formContentH5);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BusinessForm other = (BusinessForm) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGmtCreated() == null ? other.getGmtCreated() == null : this.getGmtCreated().equals(other.getGmtCreated()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getCode() == null ? other.getCode() == null : this.getCode().equals(other.getCode()))
            && (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
            && (this.getCategoryId() == null ? other.getCategoryId() == null : this.getCategoryId().equals(other.getCategoryId()))
            && (this.getIsRepeatedSubmission() == null ? other.getIsRepeatedSubmission() == null : this.getIsRepeatedSubmission().equals(other.getIsRepeatedSubmission()))
            && (this.getFormOptions() == null ? other.getFormOptions() == null : this.getFormOptions().equals(other.getFormOptions()))
            && (this.getFormOptionsH5() == null ? other.getFormOptionsH5() == null : this.getFormOptionsH5().equals(other.getFormOptionsH5()))
            && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
            && (this.getIcon() == null ? other.getIcon() == null : this.getIcon().equals(other.getIcon()))
            && (this.getCreatedBy() == null ? other.getCreatedBy() == null : this.getCreatedBy().equals(other.getCreatedBy()))
            && (this.getModifyBy() == null ? other.getModifyBy() == null : this.getModifyBy().equals(other.getModifyBy()))
            && (this.getCreatedUserId() == null ? other.getCreatedUserId() == null : this.getCreatedUserId().equals(other.getCreatedUserId()))
            && (this.getCreatedOrgId() == null ? other.getCreatedOrgId() == null : this.getCreatedOrgId().equals(other.getCreatedOrgId()))
            && (this.getFormDescription() == null ? other.getFormDescription() == null : this.getFormDescription().equals(other.getFormDescription()))
            && (this.getFormContent() == null ? other.getFormContent() == null : this.getFormContent().equals(other.getFormContent()))
            && (this.getFormDescriptionH5() == null ? other.getFormDescriptionH5() == null : this.getFormDescriptionH5().equals(other.getFormDescriptionH5()))
            && (this.getFormContentH5() == null ? other.getFormContentH5() == null : this.getFormContentH5().equals(other.getFormContentH5()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getCode() == null) ? 0 : getCode().hashCode());
        result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
        result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
        result = prime * result + ((getIsRepeatedSubmission() == null) ? 0 : getIsRepeatedSubmission().hashCode());
        result = prime * result + ((getFormOptions() == null) ? 0 : getFormOptions().hashCode());
        result = prime * result + ((getFormOptionsH5() == null) ? 0 : getFormOptionsH5().hashCode());
        result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
        result = prime * result + ((getIcon() == null) ? 0 : getIcon().hashCode());
        result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
        result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
        result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
        result = prime * result + ((getCreatedOrgId() == null) ? 0 : getCreatedOrgId().hashCode());
        result = prime * result + ((getFormDescription() == null) ? 0 : getFormDescription().hashCode());
        result = prime * result + ((getFormContent() == null) ? 0 : getFormContent().hashCode());
        result = prime * result + ((getFormDescriptionH5() == null) ? 0 : getFormDescriptionH5().hashCode());
        result = prime * result + ((getFormContentH5() == null) ? 0 : getFormContentH5().hashCode());
        return result;
    }
}