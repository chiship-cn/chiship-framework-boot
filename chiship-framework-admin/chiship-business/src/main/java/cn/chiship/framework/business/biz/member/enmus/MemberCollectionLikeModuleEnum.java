package cn.chiship.framework.business.biz.member.enmus;

/**
 * 用户收藏点赞模块枚举
 *
 * @author lijian
 */
public enum MemberCollectionLikeModuleEnum {

	MEMBER_COLLECTION_LIKE_MODULE_ARTICLE("article", "文章"),
	MEMBER_COLLECTION_LIKE_MODULE_WX_PUB_ARTICLE("wxPubArticle", "微信公众号图文"),;

	/**
	 * 业务类型
	 */
	private String type;

	/**
	 * 业务描述
	 */
	private String message;

	MemberCollectionLikeModuleEnum(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
