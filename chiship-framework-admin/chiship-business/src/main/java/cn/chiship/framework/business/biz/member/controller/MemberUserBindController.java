package cn.chiship.framework.business.biz.member.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberUserBindService;
import cn.chiship.framework.business.biz.member.entity.MemberUserBind;
import cn.chiship.framework.business.biz.member.entity.MemberUserBindExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员绑定控制层 2022/2/23
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/memberUserBind")
@Api(tags = "会员绑定")
public class MemberUserBindController extends BaseController<MemberUserBind, MemberUserBindExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserBindController.class);

    @Resource
    private MemberUserBindService memberUserBindService;

    @Override
    public BaseService getService() {
        return memberUserBindService;
    }

    @SystemOptionAnnotation(describe = "会员绑定分页")
    @ApiOperation(value = "会员绑定分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        MemberUserBindExample memberUserBindExample = new MemberUserBindExample();
        // 创造条件
        MemberUserBindExample.Criteria memberUserBindCriteria = memberUserBindExample.createCriteria();
        memberUserBindCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        return super.responseEntity(BaseResult.ok(super.page(memberUserBindExample)));
    }
    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除会员绑定")
    @ApiOperation(value = "删除会员绑定")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        MemberUserBindExample memberUserBindExample = new MemberUserBindExample();
        memberUserBindExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(memberUserBindExample));
    }

}
