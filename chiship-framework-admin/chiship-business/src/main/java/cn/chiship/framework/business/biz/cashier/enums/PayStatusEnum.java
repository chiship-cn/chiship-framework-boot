package cn.chiship.framework.business.biz.cashier.enums;

/**
 * 订单状态
 *
 * @author lijian
 */
public enum PayStatusEnum {

	/**
	 * 付款成功
	 */
	PAY_STATUS_SUCCESS(Byte.valueOf("0"), "付款成功"),

	/**
	 * 付款失败
	 */
	PAY_STATUS_FAIL(Byte.valueOf("1"), "付款失败"),

	;

	/**
	 * 业务编号
	 */
	private Byte status;

	/**
	 * 业务描述
	 */
	private String message;

	PayStatusEnum(Byte status, String message) {
		this.status = status;
		this.message = message;
	}

	public Byte getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}
