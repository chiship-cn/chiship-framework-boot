package cn.chiship.framework.business.biz.cashier.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 退款实体类
 *
 * @author lj
 */
@ApiModel(value = "退款表单")
public class RefundDto {

	@ApiModelProperty(value = "订单号", required = true)
	@NotNull(message = "订单号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 6, max = 20, message = "订单号" + BaseTipConstants.LENGTH_MIN_MAX)
	String orderNo;

	@ApiModelProperty(value = "退款金额(元)", required = true)
	@NotNull(message = "退款金额" + BaseTipConstants.NOT_EMPTY)
	private Double refundAmount;

	@ApiModelProperty(value = "退款原因", required = true)
	@NotNull(message = "退款原因" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 80, message = "退款原因" + BaseTipConstants.LENGTH_MIN_MAX)
	private String refundReason;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public Double getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Double refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

}
