package cn.chiship.framework.business.biz.base.service;

/**
 * 缓存数据
 *
 * @author lijian
 */
public interface BusinessCacheService {

	/**
	 * 缓存广告
	 */
	void cacheAdvert();

	/**
	 * 缓存商品分类
	 */
	void cacheProductCategory();

}
