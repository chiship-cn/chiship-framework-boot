package cn.chiship.framework.business.biz.base.service.impl;

import cn.chiship.framework.business.biz.base.service.BusinessCacheService;
import cn.chiship.framework.business.biz.content.service.ContentAdvertService;
import cn.chiship.framework.business.biz.product.service.ProductCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lijian
 */
@Service
public class BusinessCacheServiceImpl implements BusinessCacheService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessCacheServiceImpl.class);

    @Resource
    ContentAdvertService contentAdvertService;

    @Resource
    ProductCategoryService productCategoryService;

    @Override
    public void cacheAdvert() {
        contentAdvertService.cacheAdvertData();
    }

    @Override
    public void cacheProductCategory() {
        productCategoryService.cacheProductCategory();
    }
}
