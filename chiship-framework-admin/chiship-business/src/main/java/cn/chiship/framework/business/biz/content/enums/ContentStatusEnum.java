package cn.chiship.framework.business.biz.content.enums;

/**
 * 文章状态
 *
 * @author lijian
 */
public enum ContentStatusEnum {

	CONTENT_STATUS_DRAFT(Byte.valueOf("0"), "草稿"),

	CONTENT_STATUS_RELEASED(Byte.valueOf("1"), "待发布"),

	CONTENT_STATUS_PUBLISHED(Byte.valueOf("2"), "已发布"),

	;

	/**
	 * 业务编号
	 */
	private Byte status;

	/**
	 * 业务描述
	 */
	private String message;

	ContentStatusEnum(Byte status, String message) {
		this.status = status;
		this.message = message;
	}

	public Byte getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

}
