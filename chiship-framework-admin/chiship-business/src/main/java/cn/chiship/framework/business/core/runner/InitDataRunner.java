package cn.chiship.framework.business.core.runner;

import cn.chiship.framework.business.biz.base.service.BusinessCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化数据
 *
 * @author lijian
 */
@Component
@Order(1)
public class InitDataRunner implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(InitDataRunner.class);

    @Resource
    BusinessCacheService businessCacheService;

    @Override
    public void run(String... args) {
        businessCacheService.cacheAdvert();
        businessCacheService.cacheProductCategory();
    }

}
