package cn.chiship.framework.business.biz.member.service;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeDto;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeIsAddDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLike;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLikeExample;

/**
 * 收藏点赞业务接口层 2024/11/21
 *
 * @author lijian
 */
public interface MemberCollectionLikeService extends BaseService<MemberCollectionLike, MemberCollectionLikeExample> {

	/**
	 * 添加收藏点赞
	 * @param memberCollectionLikeDto 表单
	 * @param cacheUserVO 当前登录用户
	 * @return 结果
	 */
	BaseResult add(MemberCollectionLikeDto memberCollectionLikeDto, CacheUserVO cacheUserVO);

	/**
	 * 取消收藏点赞
	 * @param memberCollectionLikeDto 表单
	 * @param cacheUserVO 当前登录用户
	 * @return 结果
	 */
	BaseResult cancel(MemberCollectionLikeDto memberCollectionLikeDto, CacheUserVO cacheUserVO);

	/**
	 * 是否添加收藏点赞
	 * @param memberCollectionLikeIsAddDto 表单
	 * @param cacheUserVO 当前登录用户
	 * @return 结果
	 */
	BaseResult isAdd(MemberCollectionLikeIsAddDto memberCollectionLikeIsAddDto, CacheUserVO cacheUserVO);

}
