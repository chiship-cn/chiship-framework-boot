package cn.chiship.framework.business.biz.product.mapper;

import cn.chiship.framework.business.biz.product.entity.Product;
import cn.chiship.framework.business.biz.product.entity.ProductExample;

import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-11
 */
public interface ProductMapper extends BaseMapper<Product, ProductExample> {

    /**
     * 查询
     *
     * @param params 参数
     * @return List<Product>
     */
    List<Product> select(Map<String, String> params);

    List<Product> selectByExampleWithBLOBs(ProductExample example);


    int updateByExampleWithBLOBs(@Param("record") Product record, @Param("example") ProductExample example);


    int updateByPrimaryKeyWithBLOBs(Product record);

}