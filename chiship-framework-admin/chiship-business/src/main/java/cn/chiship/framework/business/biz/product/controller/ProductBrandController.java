package cn.chiship.framework.business.biz.product.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.product.service.ProductBrandService;
import cn.chiship.framework.business.biz.product.entity.ProductBrand;
import cn.chiship.framework.business.biz.product.entity.ProductBrandExample;
import cn.chiship.framework.business.biz.product.pojo.dto.ProductBrandDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 品牌控制层
 * 2024/12/11
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/productBrand")
@Api(tags = "品牌")
public class ProductBrandController extends BaseController<ProductBrand, ProductBrandExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductBrandController.class);

    @Resource
    private ProductBrandService productBrandService;

    @Override
    public BaseService getService() {
        return productBrandService;
    }

    @SystemOptionAnnotation(describe = "品牌分页")
    @ApiOperation(value = "品牌分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        ProductBrandExample productBrandExample = new ProductBrandExample();
        //创造条件
        ProductBrandExample.Criteria productBrandCriteria = productBrandExample.createCriteria();
        productBrandCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            productBrandCriteria.andBrandNameLike(keyword + "%");
        }
        return super.responseEntity(BaseResult.ok(super.page(productBrandExample)));
    }

    @SystemOptionAnnotation(describe = "品牌列表数据")
    @ApiOperation(value = "品牌列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        ProductBrandExample productBrandExample = new ProductBrandExample();
        //创造条件
        ProductBrandExample.Criteria productBrandCriteria = productBrandExample.createCriteria();
        productBrandCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        productBrandExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(productBrandExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存品牌")
    @ApiOperation(value = "保存品牌")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid ProductBrandDto productBrandDto) {
        ProductBrand productBrand = new ProductBrand();
        BeanUtils.copyProperties(productBrandDto, productBrand);
        return super.responseEntity(super.save(productBrand));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新品牌")
    @ApiOperation(value = "更新品牌")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid ProductBrandDto productBrandDto) {
        ProductBrand productBrand = new ProductBrand();
        BeanUtils.copyProperties(productBrandDto, productBrand);
        return super.responseEntity(super.update(id, productBrand));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除品牌")
    @ApiOperation(value = "删除品牌")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        ProductBrandExample productBrandExample = new ProductBrandExample();
        productBrandExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(productBrandExample));
    }
}