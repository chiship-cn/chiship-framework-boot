package cn.chiship.framework.business.biz.product.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.product.entity.ProductCategory;
import cn.chiship.framework.business.biz.product.entity.ProductCategoryExample;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 商品分类业务接口层
 * 2024/12/11
 *
 * @author lijian
 */
public interface ProductCategoryService extends BaseService<ProductCategory, ProductCategoryExample> {


    /**
     * 表格树
     *
     * @param productCategoryExample
     * @return
     */
    BaseResult treeTable(ProductCategoryExample productCategoryExample);

    /**
     * 从缓存中加载树状
     *
     * @param pid
     * @return
     */
    BaseResult cacheTreeTable(String pid);

    /**
     * 生成层级
     *
     * @param pid
     * @return
     */
    BaseResult generateTreeNumber(String pid);


    /**
     * 缓存数据
     */
    void cacheProductCategory();

    /**
     * 缓存获取TreeNumber
     *
     * @param id
     * @return
     */
    String cacheGetTreeNumberById(String id);

    /**
     * 根据主键获取完整信息
     *
     * @param id 主键
     * @return
     */
    JSONObject getParentInfoById(String id);

    /**
     * 根据层级编码获取完整信息
     *
     * @param treeNumber
     * @return
     */
    JSONObject getParentInfoByTreeNumber(String treeNumber);

    /**
     * 根据主键获取机构名
     *
     * @param id     主键
     * @param isFull 是否完整
     * @return
     */
    String getCategoryNameById(String id, Boolean isFull);


    /**
     * 绑定品牌
     *
     * @param id
     * @param brandIds
     * @return
     */
    BaseResult bindBrand(String id, List<String> brandIds);

    /**
     * 获取绑定的品牌
     *
     * @param id
     * @return
     */
    BaseResult listBindBrand(String id);

}
