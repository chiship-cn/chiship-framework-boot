package cn.chiship.framework.business.biz.member.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberUserBind;
import cn.chiship.framework.business.biz.member.entity.MemberUserBindExample;

/**
 * 会员绑定业务接口层 2022/2/23
 *
 * @author lijian
 */
public interface MemberUserBindService extends BaseService<MemberUserBind, MemberUserBindExample> {

}
