package cn.chiship.framework.business.biz.content.mapper;

import cn.chiship.framework.business.biz.content.entity.ContentAdvert;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-10-31
 */
public interface ContentAdvertMapper extends BaseMapper<ContentAdvert, ContentAdvertExample> {

}