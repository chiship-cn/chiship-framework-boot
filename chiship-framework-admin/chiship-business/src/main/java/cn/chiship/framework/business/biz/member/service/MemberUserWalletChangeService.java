package cn.chiship.framework.business.biz.member.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChange;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChangeExample;
/**
 * 会员钱包变化记录业务接口层
 * 2024/12/17
 * @author lijian
 */
public interface MemberUserWalletChangeService extends BaseService<MemberUserWalletChange,MemberUserWalletChangeExample> {

}
