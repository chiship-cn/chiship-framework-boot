package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrder;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrderExample;
import cn.chiship.framework.business.biz.cashier.enums.RefundStatusEnum;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessRefundOrderDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;

import java.math.BigDecimal;

/**
 * 退款订单业务接口层 2024/6/5
 *
 * @author lijian
 */
public interface BusinessRefundOrderService extends BaseService<BusinessRefundOrder, BusinessRefundOrderExample> {

	/**
	 * 创建退款申请
	 * @param businessRefundOrderDto
	 * @param cacheUserVO
	 * @return
	 */
	BaseResult createRefund(BusinessRefundOrderDto businessRefundOrderDto, CacheUserVO cacheUserVO);

	/**
	 * 退款成功
	 * @param refundNo
	 * @param realRefundTotal
	 * @param refundTradeNo
	 * @param receivedAccount
	 * @param time
	 * @return
	 */
	BaseResult refundSuccess(String refundNo, BigDecimal realRefundTotal, String refundTradeNo, String receivedAccount,
			Long time);

	/**
	 * 修改退款通知状态
	 * @param refundNo
	 * @param refundStatusEnum
	 * @return
	 */
	BaseResult refundNotifyStatus(String refundNo, RefundStatusEnum refundStatusEnum);

}
