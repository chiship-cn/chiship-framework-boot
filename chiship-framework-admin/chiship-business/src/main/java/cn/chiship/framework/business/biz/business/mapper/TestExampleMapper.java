package cn.chiship.framework.business.biz.business.mapper;

import cn.chiship.framework.business.biz.business.entity.TestExample;
import cn.chiship.framework.business.biz.business.entity.TestExampleExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lj
 * @date 2022-09-15
 */
public interface TestExampleMapper extends BaseMapper<TestExample, TestExampleExample> {

}