package cn.chiship.framework.business.biz.member.enmus;

import cn.chiship.framework.third.core.common.ThirdApplicationOauthTypeEnum;

/**
 * 会员来源
 *
 * @author lijian
 */
public enum MemberComeSourceEnum {

	MEMBER_COME_SOURCE_PC(Byte.valueOf("0"), "pc", "pc"),

	MEMBER_COME_SOURCE_H5(Byte.valueOf("1"), "h5", "h5"),

	MEMBER_COME_SOURCE_WX_MP(Byte.valueOf("2"), "wx_mini", "微信小程序"),

	MEMBER_COME_SOURCE_DD_MP(Byte.valueOf("3"), "ding_talk", "钉钉小程序"),

	MEMBER_COME_SOURCE_DY_MP(Byte.valueOf("4"), "dy_mini", "抖音小程序"),

	MEMBER_COME_SOURCE_WX_PUB(Byte.valueOf("5"), "wx_pub", "微信公众号"),

	MEMBER_COME_SOURCE_UN_KNOW(Byte.valueOf("-1"), "un_know", "未知"),;

	/**
	 * 业务类型
	 */
	private Byte sources;

	/**
	 * 建议编码与ThirdApplicationOauthTypeEnum的type保持一致
	 */
	private String code;

	/**
	 * 业务描述
	 */
	private String message;

	MemberComeSourceEnum(Byte sources, String code, String message) {
		this.sources = sources;
		this.code = code;
		this.message = message;
	}

	/**
	 * 根据标识返回枚举
	 * @param sources 标识
	 * @return TradeTypeEnum
	 */
	public static MemberComeSourceEnum getMemberComeSourceEnum(Byte sources) {
		MemberComeSourceEnum[] memberComeSourceEnums = values();
		for (MemberComeSourceEnum memberComeSourceEnum : memberComeSourceEnums) {
			if (memberComeSourceEnum.getSources().equals(sources)) {
				return memberComeSourceEnum;
			}
		}
		return MEMBER_COME_SOURCE_UN_KNOW;
	}

	/**
	 * 从三方登录枚举转换会员来源
	 * @param thirdApplicationOauthTypeEnum 三方小程序枚举
	 * @return MemberComeSourceEnum
	 */
	public static MemberComeSourceEnum fromThirdApplicationOauthTypeEnum(
			ThirdApplicationOauthTypeEnum thirdApplicationOauthTypeEnum) {
		String code = thirdApplicationOauthTypeEnum.getType();
		MemberComeSourceEnum[] memberComeSourceEnums = values();
		for (MemberComeSourceEnum memberComeSourceEnum : memberComeSourceEnums) {
			if (memberComeSourceEnum.getCode().equals(code)) {
				return memberComeSourceEnum;
			}
		}
		return MEMBER_COME_SOURCE_UN_KNOW;
	}

	public Byte getSources() {
		return sources;
	}

	public String getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
