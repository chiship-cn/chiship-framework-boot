package cn.chiship.framework.business.biz.product.mapper;

import cn.chiship.framework.business.biz.product.entity.ProductBrand;
import cn.chiship.framework.business.biz.product.entity.ProductBrandExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-11
 */
public interface ProductBrandMapper extends BaseMapper<ProductBrand,ProductBrandExample> {

}