package cn.chiship.framework.business.biz.cashier.mapper;

import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2023-03-26
 */
public interface BusinessOrderHeaderMapper extends BaseMapper<BusinessOrderHeader, BusinessOrderHeaderExample> {

	/**
	 * 获得编号后缀
	 * @return
	 */
	String getNoSuffix();

}
