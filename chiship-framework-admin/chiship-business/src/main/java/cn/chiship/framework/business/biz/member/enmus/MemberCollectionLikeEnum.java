package cn.chiship.framework.business.biz.member.enmus;

/**
 * 用户收藏点赞枚举
 *
 * @author lijian
 */
public enum MemberCollectionLikeEnum {

	MEMBER_COLLECTION_LIKE_SC(Byte.valueOf("0"), "收藏"),

	MEMBER_COLLECTION_LIKE_DZ(Byte.valueOf("1"), "点赞"),;

	/**
	 * 业务类型
	 */
	private Byte type;

	/**
	 * 业务描述
	 */
	private String message;

	MemberCollectionLikeEnum(Byte type, String message) {
		this.type = type;
		this.message = message;
	}

	public Byte getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
