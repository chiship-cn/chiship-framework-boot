package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotify;
import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotifyExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessPayNotifyDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;

/**
 * 支付通知业务接口层 2024/6/10
 *
 * @author lijian
 */
public interface BusinessPayNotifyService extends BaseService<BusinessPayNotify, BusinessPayNotifyExample> {

	/**
	 * 保存通知
	 * @param businessPayNotifyDto
	 * @return
	 */
	BaseResult saveNotify(BusinessPayNotifyDto businessPayNotifyDto);

}
