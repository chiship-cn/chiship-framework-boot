package cn.chiship.framework.business.biz.product.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.product.service.ProductCategoryService;
import cn.chiship.framework.business.biz.product.entity.ProductCategory;
import cn.chiship.framework.business.biz.product.entity.ProductCategoryExample;
import cn.chiship.framework.business.biz.product.pojo.dto.ProductCategoryDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商品分类控制层
 * 2024/12/11
 *
 * @author lijian
 */
@RestController
@RequestMapping("/productCategory")
@Api(tags = "商品分类")
public class ProductCategoryController extends BaseController<ProductCategory, ProductCategoryExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductCategoryController.class);

    @Resource
    private ProductCategoryService productCategoryService;

    @Override
    public BaseService getService() {
        return productCategoryService;
    }

    @ApiOperation(value = "根据类型加载树形表格")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/treeTable")
    @Authorization
    public ResponseEntity<BaseResult> treeTable(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        boolean isTree = true;
        ProductCategoryExample productCategoryExample = new ProductCategoryExample();
        ProductCategoryExample.Criteria productCategoryExampleCriteria = productCategoryExample.createCriteria();
        productCategoryExampleCriteria.andIsDeletedEqualTo(BaseConstants.NO).andTreeNumberIsNotNull();
        if (!StringUtil.isNullOrEmpty(keyword)) {
            productCategoryExampleCriteria.andCategoryNameLike(keyword + "%");
            isTree = false;
        }
        productCategoryExample.setOrderByClause("orders desc");
        if (isTree) {
            return super.responseEntity(productCategoryService.treeTable(productCategoryExample));
        } else {
            return super.responseEntity(BaseResult.ok(productCategoryService.selectByExample(productCategoryExample)));
        }
    }

    @ApiOperation(value = "根据类型加载树形表格(缓存)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pid", value = "父级", defaultValue = "0", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/cacheTreeTable")
    public ResponseEntity<BaseResult> cacheTreeTable(@RequestParam(required = false, defaultValue = "0", value = "pid") String pid) {
        return super.responseEntity(productCategoryService.cacheTreeTable(pid));
    }

    @SystemOptionAnnotation(describe = "商品分类列表数据")
    @ApiOperation(value = "商品分类列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        ProductCategoryExample productCategoryExample = new ProductCategoryExample();
        //创造条件
        ProductCategoryExample.Criteria productCategoryCriteria = productCategoryExample.createCriteria();
        productCategoryCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        productCategoryExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(productCategoryExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存商品分类")
    @ApiOperation(value = "保存商品分类")
    @PostMapping(value = "save")
    @Authorization
    public ResponseEntity<BaseResult> save(@RequestBody @Valid ProductCategoryDto productCategoryDto) {
        ProductCategory productCategory = new ProductCategory();
        BeanUtils.copyProperties(productCategoryDto, productCategory);
        return super.responseEntity(super.save(productCategory));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新商品分类")
    @ApiOperation(value = "更新商品分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    @Authorization
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid ProductCategoryDto productCategoryDto) {
        ProductCategory productCategory = new ProductCategory();
        BeanUtils.copyProperties(productCategoryDto, productCategory);
        return super.responseEntity(super.update(id, productCategory));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "绑定品牌")
    @ApiOperation(value = "绑定品牌")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "bindBrand/{id}")
    @Authorization
    public ResponseEntity<BaseResult> bindBrand(@PathVariable("id") String id, @RequestBody @Valid List<String> brandIds) {
        return super.responseEntity(productCategoryService.bindBrand(id, brandIds));
    }

    @ApiOperation(value = "获取绑定的品牌")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "query", required = true)
    })
    @GetMapping(value = "listBindBrand")
    @Authorization
    public ResponseEntity<BaseResult> listBindBrand(@RequestParam(value = "id") String id) {
        return super.responseEntity(productCategoryService.listBindBrand(id));
    }


    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除商品分类")
    @ApiOperation(value = "删除商品分类")
    @PostMapping(value = "remove")
    @Authorization
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        ProductCategoryExample productCategoryExample = new ProductCategoryExample();
        productCategoryExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(productCategoryExample));
    }
}