package cn.chiship.framework.business.biz.business.service;

import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormConfigDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.business.entity.BusinessForm;
import cn.chiship.framework.business.biz.business.entity.BusinessFormExample;

/**
 * 自定义表单业务接口层
 * 2024/12/13
 *
 * @author lijian
 */
public interface BusinessFormService extends BaseService<BusinessForm, BusinessFormExample> {

    /**
     * 树状结构数据
     *
     * @return
     */
    BaseResult tree(Boolean isAll);

    /**
     * 保存设计表单
     *
     * @param businessFormConfigDto
     * @return
     */
    BaseResult saveConfig(BusinessFormConfigDto businessFormConfigDto);

    /**
     * 发布
     *
     * @param id
     * @return
     */
    BaseResult publish(String id);

    /**
     * 撤销
     *
     * @param id
     * @return
     */
    BaseResult revoke(String id);

}
