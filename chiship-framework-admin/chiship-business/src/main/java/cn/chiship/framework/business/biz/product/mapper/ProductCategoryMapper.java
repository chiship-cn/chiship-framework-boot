package cn.chiship.framework.business.biz.product.mapper;

import cn.chiship.framework.business.biz.product.entity.ProductCategory;
import cn.chiship.framework.business.biz.product.entity.ProductCategoryExample;


import cn.chiship.sdk.framework.base.BaseMapper;

import java.util.Map;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-11
 */
public interface ProductCategoryMapper extends BaseMapper<ProductCategory, ProductCategoryExample> {
    /**
     * 根据父级获取最大编码
     *
     * @param pid
     * @return
     */
    Map<String, String> getTreeNumberByPid(String pid);
}