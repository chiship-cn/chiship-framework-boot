package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @author lijian
 */
@ApiModel(value = "创建充值订单表单")
public class OrderRechargeCreateDto {

	@ApiModelProperty(value = "小程序或公众号唯一识别码")
	private String openId;

	@ApiModelProperty(value = "订单名称")
	private String orderName;

	@ApiModelProperty(value = "支付金额")
	private Double price;

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getOrderName() {
		return orderName;
	}

	public void setOrderName(String orderName) {
		this.orderName = orderName;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
