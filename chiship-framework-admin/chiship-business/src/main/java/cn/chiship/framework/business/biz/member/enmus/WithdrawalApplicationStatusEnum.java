package cn.chiship.framework.business.biz.member.enmus;

import cn.chiship.framework.business.biz.cashier.enums.OrderStatusEnum;

/**
 * 退款状态
 *
 * @author lijian
 */
public enum WithdrawalApplicationStatusEnum {

    WITHDRAWAL_IN_REVIEW(Byte.valueOf("0"), "审核中"),

    WITHDRAWAL_APPROVED(Byte.valueOf("1"), "审批通过"),

    WITHDRAWAL_APPROVED_REJECTED(Byte.valueOf("2"), "审批拒绝"),

    WITHDRAWAL_PAYING(Byte.valueOf("3"), "打款中"),

    WITHDRAWAL_SUCCESS(Byte.valueOf("4"), "转账成功"),

    WITHDRAWAL_FAIL(Byte.valueOf("5"), "转账失败"),
    ;

    /**
     * 业务编号
     */
    private Byte status;

    /**
     * 业务描述
     */
    private String message;

    WithdrawalApplicationStatusEnum(Byte status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * 根据标识返回枚举
     *
     * @param status 标识
     * @return TradeTypeEnum
     */
    public static WithdrawalApplicationStatusEnum getWithdrawalApplicationStatusEnum(Byte status) {
        WithdrawalApplicationStatusEnum[] applicationStatusEnums = values();
        for (WithdrawalApplicationStatusEnum applicationStatusEnum : applicationStatusEnums) {
            if (applicationStatusEnum.getStatus().equals(status)) {
                return applicationStatusEnum;
            }
        }
        throw new RuntimeException("无效的申请状态！");
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
