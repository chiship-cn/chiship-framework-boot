package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-11-23
 */
public class MemberCommentExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public MemberCommentExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andParentIdIsNull() {
			addCriterion("parent_id is null");
			return (Criteria) this;
		}

		public Criteria andParentIdIsNotNull() {
			addCriterion("parent_id is not null");
			return (Criteria) this;
		}

		public Criteria andParentIdEqualTo(String value) {
			addCriterion("parent_id =", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdNotEqualTo(String value) {
			addCriterion("parent_id <>", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdGreaterThan(String value) {
			addCriterion("parent_id >", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdGreaterThanOrEqualTo(String value) {
			addCriterion("parent_id >=", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdLessThan(String value) {
			addCriterion("parent_id <", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdLessThanOrEqualTo(String value) {
			addCriterion("parent_id <=", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdLike(String value) {
			addCriterion("parent_id like", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdNotLike(String value) {
			addCriterion("parent_id not like", value, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdIn(List<String> values) {
			addCriterion("parent_id in", values, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdNotIn(List<String> values) {
			addCriterion("parent_id not in", values, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdBetween(String value1, String value2) {
			addCriterion("parent_id between", value1, value2, "parentId");
			return (Criteria) this;
		}

		public Criteria andParentIdNotBetween(String value1, String value2) {
			addCriterion("parent_id not between", value1, value2, "parentId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdIsNull() {
			addCriterion("auditor_id is null");
			return (Criteria) this;
		}

		public Criteria andAuditorIdIsNotNull() {
			addCriterion("auditor_id is not null");
			return (Criteria) this;
		}

		public Criteria andAuditorIdEqualTo(String value) {
			addCriterion("auditor_id =", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdNotEqualTo(String value) {
			addCriterion("auditor_id <>", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdGreaterThan(String value) {
			addCriterion("auditor_id >", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdGreaterThanOrEqualTo(String value) {
			addCriterion("auditor_id >=", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdLessThan(String value) {
			addCriterion("auditor_id <", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdLessThanOrEqualTo(String value) {
			addCriterion("auditor_id <=", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdLike(String value) {
			addCriterion("auditor_id like", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdNotLike(String value) {
			addCriterion("auditor_id not like", value, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdIn(List<String> values) {
			addCriterion("auditor_id in", values, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdNotIn(List<String> values) {
			addCriterion("auditor_id not in", values, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdBetween(String value1, String value2) {
			addCriterion("auditor_id between", value1, value2, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditorIdNotBetween(String value1, String value2) {
			addCriterion("auditor_id not between", value1, value2, "auditorId");
			return (Criteria) this;
		}

		public Criteria andAuditDateIsNull() {
			addCriterion("audit_date is null");
			return (Criteria) this;
		}

		public Criteria andAuditDateIsNotNull() {
			addCriterion("audit_date is not null");
			return (Criteria) this;
		}

		public Criteria andAuditDateEqualTo(Long value) {
			addCriterion("audit_date =", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateNotEqualTo(Long value) {
			addCriterion("audit_date <>", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateGreaterThan(Long value) {
			addCriterion("audit_date >", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateGreaterThanOrEqualTo(Long value) {
			addCriterion("audit_date >=", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateLessThan(Long value) {
			addCriterion("audit_date <", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateLessThanOrEqualTo(Long value) {
			addCriterion("audit_date <=", value, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateIn(List<Long> values) {
			addCriterion("audit_date in", values, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateNotIn(List<Long> values) {
			addCriterion("audit_date not in", values, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateBetween(Long value1, Long value2) {
			addCriterion("audit_date between", value1, value2, "auditDate");
			return (Criteria) this;
		}

		public Criteria andAuditDateNotBetween(Long value1, Long value2) {
			addCriterion("audit_date not between", value1, value2, "auditDate");
			return (Criteria) this;
		}

		public Criteria andIpIsNull() {
			addCriterion("ip is null");
			return (Criteria) this;
		}

		public Criteria andIpIsNotNull() {
			addCriterion("ip is not null");
			return (Criteria) this;
		}

		public Criteria andIpEqualTo(String value) {
			addCriterion("ip =", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotEqualTo(String value) {
			addCriterion("ip <>", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpGreaterThan(String value) {
			addCriterion("ip >", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpGreaterThanOrEqualTo(String value) {
			addCriterion("ip >=", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLessThan(String value) {
			addCriterion("ip <", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLessThanOrEqualTo(String value) {
			addCriterion("ip <=", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpLike(String value) {
			addCriterion("ip like", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotLike(String value) {
			addCriterion("ip not like", value, "ip");
			return (Criteria) this;
		}

		public Criteria andIpIn(List<String> values) {
			addCriterion("ip in", values, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotIn(List<String> values) {
			addCriterion("ip not in", values, "ip");
			return (Criteria) this;
		}

		public Criteria andIpBetween(String value1, String value2) {
			addCriterion("ip between", value1, value2, "ip");
			return (Criteria) this;
		}

		public Criteria andIpNotBetween(String value1, String value2) {
			addCriterion("ip not between", value1, value2, "ip");
			return (Criteria) this;
		}

		public Criteria andIpAddressIsNull() {
			addCriterion("ip_address is null");
			return (Criteria) this;
		}

		public Criteria andIpAddressIsNotNull() {
			addCriterion("ip_address is not null");
			return (Criteria) this;
		}

		public Criteria andIpAddressEqualTo(String value) {
			addCriterion("ip_address =", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressNotEqualTo(String value) {
			addCriterion("ip_address <>", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressGreaterThan(String value) {
			addCriterion("ip_address >", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressGreaterThanOrEqualTo(String value) {
			addCriterion("ip_address >=", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressLessThan(String value) {
			addCriterion("ip_address <", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressLessThanOrEqualTo(String value) {
			addCriterion("ip_address <=", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressLike(String value) {
			addCriterion("ip_address like", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressNotLike(String value) {
			addCriterion("ip_address not like", value, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressIn(List<String> values) {
			addCriterion("ip_address in", values, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressNotIn(List<String> values) {
			addCriterion("ip_address not in", values, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressBetween(String value1, String value2) {
			addCriterion("ip_address between", value1, value2, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andIpAddressNotBetween(String value1, String value2) {
			addCriterion("ip_address not between", value1, value2, "ipAddress");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("status is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("status is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Byte value) {
			addCriterion("status =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Byte value) {
			addCriterion("status <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Byte value) {
			addCriterion("status >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("status >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Byte value) {
			addCriterion("status <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Byte value) {
			addCriterion("status <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Byte> values) {
			addCriterion("status in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Byte> values) {
			addCriterion("status not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Byte value1, Byte value2) {
			addCriterion("status between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("status not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIsNull() {
			addCriterion("tree_number is null");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIsNotNull() {
			addCriterion("tree_number is not null");
			return (Criteria) this;
		}

		public Criteria andTreeNumberEqualTo(String value) {
			addCriterion("tree_number =", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotEqualTo(String value) {
			addCriterion("tree_number <>", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberGreaterThan(String value) {
			addCriterion("tree_number >", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberGreaterThanOrEqualTo(String value) {
			addCriterion("tree_number >=", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLessThan(String value) {
			addCriterion("tree_number <", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLessThanOrEqualTo(String value) {
			addCriterion("tree_number <=", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberLike(String value) {
			addCriterion("tree_number like", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotLike(String value) {
			addCriterion("tree_number not like", value, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberIn(List<String> values) {
			addCriterion("tree_number in", values, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotIn(List<String> values) {
			addCriterion("tree_number not in", values, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberBetween(String value1, String value2) {
			addCriterion("tree_number between", value1, value2, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTreeNumberNotBetween(String value1, String value2) {
			addCriterion("tree_number not between", value1, value2, "treeNumber");
			return (Criteria) this;
		}

		public Criteria andTextIsNull() {
			addCriterion("text is null");
			return (Criteria) this;
		}

		public Criteria andTextIsNotNull() {
			addCriterion("text is not null");
			return (Criteria) this;
		}

		public Criteria andTextEqualTo(String value) {
			addCriterion("text =", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotEqualTo(String value) {
			addCriterion("text <>", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextGreaterThan(String value) {
			addCriterion("text >", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextGreaterThanOrEqualTo(String value) {
			addCriterion("text >=", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLessThan(String value) {
			addCriterion("text <", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLessThanOrEqualTo(String value) {
			addCriterion("text <=", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLike(String value) {
			addCriterion("text like", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotLike(String value) {
			addCriterion("text not like", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextIn(List<String> values) {
			addCriterion("text in", values, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotIn(List<String> values) {
			addCriterion("text not in", values, "text");
			return (Criteria) this;
		}

		public Criteria andTextBetween(String value1, String value2) {
			addCriterion("text between", value1, value2, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotBetween(String value1, String value2) {
			addCriterion("text not between", value1, value2, "text");
			return (Criteria) this;
		}

		public Criteria andModuleIsNull() {
			addCriterion("module is null");
			return (Criteria) this;
		}

		public Criteria andModuleIsNotNull() {
			addCriterion("module is not null");
			return (Criteria) this;
		}

		public Criteria andModuleEqualTo(String value) {
			addCriterion("module =", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotEqualTo(String value) {
			addCriterion("module <>", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleGreaterThan(String value) {
			addCriterion("module >", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleGreaterThanOrEqualTo(String value) {
			addCriterion("module >=", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLessThan(String value) {
			addCriterion("module <", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLessThanOrEqualTo(String value) {
			addCriterion("module <=", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleLike(String value) {
			addCriterion("module like", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotLike(String value) {
			addCriterion("module not like", value, "module");
			return (Criteria) this;
		}

		public Criteria andModuleIn(List<String> values) {
			addCriterion("module in", values, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotIn(List<String> values) {
			addCriterion("module not in", values, "module");
			return (Criteria) this;
		}

		public Criteria andModuleBetween(String value1, String value2) {
			addCriterion("module between", value1, value2, "module");
			return (Criteria) this;
		}

		public Criteria andModuleNotBetween(String value1, String value2) {
			addCriterion("module not between", value1, value2, "module");
			return (Criteria) this;
		}

		public Criteria andModuleIdIsNull() {
			addCriterion("module_id is null");
			return (Criteria) this;
		}

		public Criteria andModuleIdIsNotNull() {
			addCriterion("module_id is not null");
			return (Criteria) this;
		}

		public Criteria andModuleIdEqualTo(String value) {
			addCriterion("module_id =", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotEqualTo(String value) {
			addCriterion("module_id <>", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdGreaterThan(String value) {
			addCriterion("module_id >", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdGreaterThanOrEqualTo(String value) {
			addCriterion("module_id >=", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLessThan(String value) {
			addCriterion("module_id <", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLessThanOrEqualTo(String value) {
			addCriterion("module_id <=", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdLike(String value) {
			addCriterion("module_id like", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotLike(String value) {
			addCriterion("module_id not like", value, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdIn(List<String> values) {
			addCriterion("module_id in", values, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotIn(List<String> values) {
			addCriterion("module_id not in", values, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdBetween(String value1, String value2) {
			addCriterion("module_id between", value1, value2, "moduleId");
			return (Criteria) this;
		}

		public Criteria andModuleIdNotBetween(String value1, String value2) {
			addCriterion("module_id not between", value1, value2, "moduleId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNull() {
			addCriterion("created_user_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNotNull() {
			addCriterion("created_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdEqualTo(String value) {
			addCriterion("created_user_id =", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotEqualTo(String value) {
			addCriterion("created_user_id <>", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThan(String value) {
			addCriterion("created_user_id >", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_id >=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThan(String value) {
			addCriterion("created_user_id <", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThanOrEqualTo(String value) {
			addCriterion("created_user_id <=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLike(String value) {
			addCriterion("created_user_id like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotLike(String value) {
			addCriterion("created_user_id not like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIn(List<String> values) {
			addCriterion("created_user_id in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotIn(List<String> values) {
			addCriterion("created_user_id not in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdBetween(String value1, String value2) {
			addCriterion("created_user_id between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotBetween(String value1, String value2) {
			addCriterion("created_user_id not between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIsNull() {
			addCriterion("created_user_name is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIsNotNull() {
			addCriterion("created_user_name is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameEqualTo(String value) {
			addCriterion("created_user_name =", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotEqualTo(String value) {
			addCriterion("created_user_name <>", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameGreaterThan(String value) {
			addCriterion("created_user_name >", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_name >=", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLessThan(String value) {
			addCriterion("created_user_name <", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLessThanOrEqualTo(String value) {
			addCriterion("created_user_name <=", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameLike(String value) {
			addCriterion("created_user_name like", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotLike(String value) {
			addCriterion("created_user_name not like", value, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameIn(List<String> values) {
			addCriterion("created_user_name in", values, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotIn(List<String> values) {
			addCriterion("created_user_name not in", values, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameBetween(String value1, String value2) {
			addCriterion("created_user_name between", value1, value2, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserNameNotBetween(String value1, String value2) {
			addCriterion("created_user_name not between", value1, value2, "createdUserName");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIsNull() {
			addCriterion("created_user_avatar is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIsNotNull() {
			addCriterion("created_user_avatar is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarEqualTo(String value) {
			addCriterion("created_user_avatar =", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotEqualTo(String value) {
			addCriterion("created_user_avatar <>", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarGreaterThan(String value) {
			addCriterion("created_user_avatar >", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_avatar >=", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLessThan(String value) {
			addCriterion("created_user_avatar <", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLessThanOrEqualTo(String value) {
			addCriterion("created_user_avatar <=", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarLike(String value) {
			addCriterion("created_user_avatar like", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotLike(String value) {
			addCriterion("created_user_avatar not like", value, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarIn(List<String> values) {
			addCriterion("created_user_avatar in", values, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotIn(List<String> values) {
			addCriterion("created_user_avatar not in", values, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarBetween(String value1, String value2) {
			addCriterion("created_user_avatar between", value1, value2, "createdUserAvatar");
			return (Criteria) this;
		}

		public Criteria andCreatedUserAvatarNotBetween(String value1, String value2) {
			addCriterion("created_user_avatar not between", value1, value2, "createdUserAvatar");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}