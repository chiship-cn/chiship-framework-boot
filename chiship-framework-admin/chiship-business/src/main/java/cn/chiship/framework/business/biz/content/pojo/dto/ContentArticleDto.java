package cn.chiship.framework.business.biz.content.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 文档详细表表单 2024/10/19
 *
 * @author LiJian
 */
@ApiModel(value = "文档详细表表单")
public class ContentArticleDto {

    @ApiModelProperty(value = "所属栏目", required = true)
    @NotEmpty(message = "所属栏目" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1)
    private String categoryId;

    @ApiModelProperty(value = "标题", required = true)
    @NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 150)
    private String title;

    @ApiModelProperty(value = "作者", required = true)
    @NotEmpty(message = "作者" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 10)
    private String author;

    @ApiModelProperty(value = "封面1")
    private String image1;

    @ApiModelProperty(value = "封面2")
    private String image2;

    @ApiModelProperty(value = "封面3")
    private String image3;

    @ApiModelProperty(value = "简述")
    @Length(max = 200)
    private String summary;

    @ApiModelProperty(value = "keyword关键字", required = true)
    @NotEmpty(message = "keyword关键字" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 200)
    private String keywords;

    @ApiModelProperty(value = "meta描述", required = true)
    @NotEmpty(message = "meta描述" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 450)
    private String metaDescription;

    @ApiModelProperty(value = "是否置顶（0：否，1：是）", required = true)
    @NotNull(message = "是否置顶" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isTop;

    @ApiModelProperty(value = "是否原创（0：否，1：是）", required = true)
    @NotNull(message = "是否原创" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isOriginal;

    @ApiModelProperty(value = "来源")
    @Length(max = 50)
    private String source;

    @ApiModelProperty(value = "来源url")
    @Length(max = 100)
    private String sourceUrl;

    @ApiModelProperty(value = "附件")
    private String file;

    @ApiModelProperty(value = "是否允许评论（0：禁止，1：允许）", required = true)
    @NotNull(message = "是否允许评论" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isAllowComment;


    @ApiModelProperty(value = "浏览总数）", required = true)
    @NotNull(message = "浏览总数" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    private Long views;

    @ApiModelProperty(value = "点赞次数）", required = true)
    @NotNull(message = "点赞次数" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    private Long thumbUpNum;

    @ApiModelProperty(value = "收藏次数）", required = true)
    @NotNull(message = "收藏次数" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    private Long collections;


    @ApiModelProperty(value = "正文")
    private String content;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public  String getTitle() {
        return title;
    }

    public void setTitle( String title) {
        this.title = title;
    }

    public  String getAuthor() {
        return author;
    }

    public void setAuthor( String author) {
        this.author = author;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public @Length(max = 200) String getSummary() {
        return summary;
    }

    public void setSummary(@Length(max = 200) String summary) {
        this.summary = summary;
    }

    public  String getKeywords() {
        return keywords;
    }

    public void setKeywords( String keywords) {
        this.keywords = keywords;
    }

    public  String getMetaDescription() {
        return metaDescription;
    }

    public void setMetaDescription( String metaDescription) {
        this.metaDescription = metaDescription;
    }

    public  Byte getIsTop() {
        return isTop;
    }

    public void setIsTop( Byte isTop) {
        this.isTop = isTop;
    }

    public  Byte getIsOriginal() {
        return isOriginal;
    }

    public void setIsOriginal( Byte isOriginal) {
        this.isOriginal = isOriginal;
    }

    public @Length(max = 50) String getSource() {
        return source;
    }

    public void setSource(@Length(max = 50) String source) {
        this.source = source;
    }

    public @Length(max = 100) String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(@Length(max = 100) String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public  Byte getIsAllowComment() {
        return isAllowComment;
    }

    public void setIsAllowComment( Byte isAllowComment) {
        this.isAllowComment = isAllowComment;
    }

    public  Long getViews() {
        return views;
    }

    public void setViews( Long views) {
        this.views = views;
    }

    public  Long getThumbUpNum() {
        return thumbUpNum;
    }

    public void setThumbUpNum( Long thumbUpNum) {
        this.thumbUpNum = thumbUpNum;
    }

    public  Long getCollections() {
        return collections;
    }

    public void setCollections( Long collections) {
        this.collections = collections;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}