package cn.chiship.framework.business.biz.base.service.impl;

import cn.chiship.framework.business.biz.content.entity.ContentArticle;
import cn.chiship.framework.business.biz.content.mapper.ContentArticleMapper;
import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeEnum;
import cn.chiship.framework.business.biz.member.enmus.MemberWalletChangeModuleEnum;
import cn.chiship.framework.business.biz.member.entity.MemberUser;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChange;
import cn.chiship.framework.business.biz.member.mapper.MemberUserMapper;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeDto;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberCommentDto;
import cn.chiship.framework.business.biz.member.service.MemberUserWalletChangeService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatArticle;
import cn.chiship.framework.third.biz.wxpub.mapper.ThirdWechatArticleMapper;
import cn.chiship.sdk.core.util.StringUtil;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * 业务异步处理
 *
 * @author lijian
 */
@Component
public class BusinessAsyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessAsyncService.class);

    @Resource
    ContentArticleMapper contentArticleMapper;
    @Resource
    ThirdWechatArticleMapper wechatArticleMapper;
    @Resource
    MemberUserMapper memberUserMapper;
    @Resource
    MemberUserWalletChangeService memberUserWalletChangeService;

    /**
     * 异步更新各个模块点赞收藏数量
     */
    @Async("asyncServiceExecutor")
    public void updateModuleCollectLike(MemberCollectionLikeDto memberCollectionLikeDto, Boolean isPlus) {
        String modelId = memberCollectionLikeDto.getModuleId();
        switch (memberCollectionLikeDto.getModuleEnum().getType()) {
            case "article":
                ContentArticle contentArticle = contentArticleMapper.selectByPrimaryKey(modelId);
                if (ObjectUtils.isNotEmpty(contentArticle)) {
                    if (memberCollectionLikeDto.getTypeEnum().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_SC)) {
                        contentArticle.setCollections(Boolean.TRUE.equals(isPlus) ? contentArticle.getCollections() + 1
                                : contentArticle.getCollections() - 1);
                    }
                    if (memberCollectionLikeDto.getTypeEnum().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_DZ)) {
                        contentArticle.setThumbUpNum(Boolean.TRUE.equals(isPlus) ? contentArticle.getThumbUpNum() + 1
                                : contentArticle.getThumbUpNum() - 1);
                    }
                    contentArticleMapper.updateByPrimaryKeySelective(contentArticle);
                }
                break;
            case "wxPubArticle":
                ThirdWechatArticle thirdWechatArticle = wechatArticleMapper.selectByPrimaryKey(modelId);
                if (ObjectUtils.isNotEmpty(thirdWechatArticle)) {
                    if (memberCollectionLikeDto.getTypeEnum().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_SC)) {
                        thirdWechatArticle.setCollectCount(Boolean.TRUE.equals(isPlus) ? thirdWechatArticle.getCollectCount() + 1 : thirdWechatArticle.getCollectCount() - 1);
                    }
                    if (memberCollectionLikeDto.getTypeEnum().equals(MemberCollectionLikeEnum.MEMBER_COLLECTION_LIKE_DZ)) {
                        thirdWechatArticle.setLikeCount(Boolean.TRUE.equals(isPlus) ? thirdWechatArticle.getLikeCount() + 1 : thirdWechatArticle.getLikeCount() - 1);
                    }
                    wechatArticleMapper.updateByPrimaryKeySelective(thirdWechatArticle);
                }
                break;
        }
    }

    /**
     * 异步更新各个模块评论数量
     */
    @Async("asyncServiceExecutor")
    public void updateModuleComment(MemberCommentDto memberCommentDto) {
        String modelId = memberCommentDto.getModuleId();
        switch (memberCommentDto.getModuleEnum().getType()) {
            case "article":
                ContentArticle contentArticle = contentArticleMapper.selectByPrimaryKey(modelId);
                if (ObjectUtils.isNotEmpty(contentArticle)) {
                    contentArticle.setComments(contentArticle.getComments() + 1);
                    contentArticleMapper.updateByPrimaryKeySelective(contentArticle);
                }
                break;
            case "wxPubArticle":
                ThirdWechatArticle thirdWechatArticle = wechatArticleMapper.selectByPrimaryKey(modelId);
                if (ObjectUtils.isNotEmpty(thirdWechatArticle)) {
                    thirdWechatArticle.setCommentCount(thirdWechatArticle.getCommentCount() + 1);
                    wechatArticleMapper.updateByPrimaryKeySelective(thirdWechatArticle);
                }
                break;
        }
    }

    /**
     * 异步更新钱包及记录变化
     */
    @Async("asyncServiceExecutor")
    public void changeMemberWallet(String memberId, BigDecimal money, Boolean consumption, String remark, MemberWalletChangeModuleEnum memberWalletChangeModuleEnum, String moduleId) {
        if (StringUtil.isNullOrEmpty(memberId)) {
            return;
        }
        MemberUser memberUser = memberUserMapper.selectByPrimaryKey(memberId);
        if (ObjectUtils.isEmpty(memberUser)) {
            return;
        }
        MemberUserWalletChange memberUserWalletChange = new MemberUserWalletChange();
        memberUserWalletChange.setUserId(memberUser.getId());
        memberUserWalletChange.setChangeNumber(money);
        memberUserWalletChange.setModuleId(moduleId);
        memberUserWalletChange.setModule(memberWalletChangeModuleEnum.getType());
        //类型  0 消费 1:进账
        if (Boolean.TRUE.equals(consumption)) {
            memberUserWalletChange.setType(Byte.valueOf("0"));
            memberUser.setWallet(memberUser.getWallet().subtract(money));
        } else {
            memberUserWalletChange.setType(Byte.valueOf("1"));
            memberUser.setWallet(memberUser.getWallet().add(money));
        }
        memberUserMapper.updateByPrimaryKeySelective(memberUser);
        memberUserWalletChange.setRemark(remark);

        memberUserWalletChangeService.insertSelective(memberUserWalletChange);
    }

}