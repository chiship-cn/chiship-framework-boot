package cn.chiship.framework.business.biz.product.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 品牌表单
 * 2024/12/11
 *
 * @author LiJian
 */
@ApiModel(value = "品牌表单")
public class ProductBrandDto {

    @ApiModelProperty(value = "品牌名称", required = true)
    @NotNull(message = "品牌名称" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 10, message = "品牌名称" + BaseTipConstants.LENGTH_MIN_MAX)
    private String brandName;

    @ApiModelProperty(value = "品牌Logo")
    private String brandLogo;

    @ApiModelProperty(value = "描述")
    @Length(max = 500, message = "品牌描述" + BaseTipConstants.LENGTH_MIN_MAX)
    private String brandDesc;

    @ApiModelProperty(value = "官网")
    private String brandSiteUrl;

    @ApiModelProperty(value = "排序", required = true)
    private Long orders;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandLogo() {
        return brandLogo;
    }

    public void setBrandLogo(String brandLogo) {
        this.brandLogo = brandLogo;
    }

    public String getBrandDesc() {
        return brandDesc;
    }

    public void setBrandDesc(String brandDesc) {
        this.brandDesc = brandDesc;
    }

    public String getBrandSiteUrl() {
        return brandSiteUrl;
    }

    public void setBrandSiteUrl(String brandSiteUrl) {
        this.brandSiteUrl = brandSiteUrl;
    }

    public Long getOrders() {
        return orders;
    }

    public void setOrders(Long orders) {
        this.orders = orders;
    }
}