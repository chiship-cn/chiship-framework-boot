package cn.chiship.framework.business.biz.content.service.impl;

import cn.chiship.framework.business.biz.content.service.ContentAdvertService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.content.mapper.ContentAdvertSlotMapper;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlot;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlotExample;
import cn.chiship.framework.business.biz.content.service.ContentAdvertSlotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 广告版位表业务接口实现层 2024/10/31
 *
 * @author lijian
 */
@Service
public class ContentAdvertSlotServiceImpl extends BaseServiceImpl<ContentAdvertSlot, ContentAdvertSlotExample>
		implements ContentAdvertSlotService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContentAdvertSlotServiceImpl.class);

	@Resource
	ContentAdvertSlotMapper contentAdvertSlotMapper;

	@Resource
	ContentAdvertService contentAdvertService;

	@Override
	public BaseResult updateByPrimaryKeySelective(ContentAdvertSlot contentAdvertSlot) {
		super.updateByPrimaryKeySelective(contentAdvertSlot);
		contentAdvertService.cacheAdvertData();
		return BaseResult.ok();
	}

	@Override
	public BaseResult deleteByExample(ContentAdvertSlotExample contentAdvertSlotExample) {
		super.deleteByExample(contentAdvertSlotExample);
		contentAdvertService.cacheAdvertData();
		return BaseResult.ok();
	}

}
