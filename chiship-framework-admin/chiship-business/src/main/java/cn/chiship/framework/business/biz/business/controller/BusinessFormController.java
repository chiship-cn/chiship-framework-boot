package cn.chiship.framework.business.biz.business.controller;


import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormConfigDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.business.service.BusinessFormService;
import cn.chiship.framework.business.biz.business.entity.BusinessForm;
import cn.chiship.framework.business.biz.business.entity.BusinessFormExample;
import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 自定义表单控制层
 * 2024/12/13
 *
 * @author lijian
 */
@RestController
@RequestMapping("/businessForm")
@Api(tags = "自定义表单")
public class BusinessFormController extends BaseController<BusinessForm, BusinessFormExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessFormController.class);

    @Resource
    private BusinessFormService businessFormService;

    @Override
    public BaseService getService() {
        return businessFormService;
    }

    @SystemOptionAnnotation(describe = "自定义表单分页")
    @ApiOperation(value = "自定义表单分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "code", value = "编号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "分类", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    @Authorization
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "name") String name,
                                           @RequestParam(required = false, defaultValue = "", value = "code") String code,
                                           @RequestParam(required = false, defaultValue = "", value = "categoryId") String categoryId) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        //创造条件
        BusinessFormExample.Criteria businessFormCriteria = businessFormExample.createCriteria();
        businessFormCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(categoryId) && !"-".equals(categoryId)) {
            businessFormCriteria.andCategoryIdEqualTo(categoryId);
        }
        if (!StringUtil.isNullOrEmpty(code)) {
            businessFormCriteria.andCodeLike(code + "%");
        }
        if (!StringUtil.isNullOrEmpty(name)) {
            businessFormCriteria.andNameLike(name + "%");
        }
        return super.responseEntity(BaseResult.ok(super.page(businessFormExample)));
    }

    @ApiOperation(value = "表单树状结构")
    @ApiImplicitParams({
    })
    @GetMapping(value = "/tree")
    public ResponseEntity<BaseResult> tree() {
        return super.responseEntity(businessFormService.tree(false));
    }
    @ApiOperation(value = "表单树状结构")
    @ApiImplicitParams({
    })
    @GetMapping(value = "/treeAll")
    public ResponseEntity<BaseResult> treeAll() {
        return super.responseEntity(businessFormService.tree(true));
    }


    @SystemOptionAnnotation(describe = "自定义表单列表数据")
    @ApiOperation(value = "自定义表单列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "code", value = "编号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "分类", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "name") String name,
                                           @RequestParam(required = false, defaultValue = "", value = "code") String code,
                                           @RequestParam(required = false, defaultValue = "", value = "categoryId") String categoryId,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        //创造条件
        BusinessFormExample.Criteria businessFormCriteria = businessFormExample.createCriteria();
        businessFormCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(categoryId) && !"-".equals(categoryId)) {
            businessFormCriteria.andCategoryIdEqualTo(categoryId);
        }
        if (!StringUtil.isNullOrEmpty(code)) {
            businessFormCriteria.andCodeLike(code + "%");
        }
        if (!StringUtil.isNullOrEmpty(name)) {
            businessFormCriteria.andNameLike(name + "%");
        }
        businessFormExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(businessFormExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存自定义表单")
    @ApiOperation(value = "保存自定义表单")
    @PostMapping(value = "save")
    @Authorization
    public ResponseEntity<BaseResult> save(@RequestBody @Valid BusinessFormDto businessFormDto) {
        BusinessForm businessForm = new BusinessForm();
        BeanUtils.copyProperties(businessFormDto, businessForm);
        return super.responseEntity(super.save(businessForm));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新自定义表单")
    @ApiOperation(value = "更新自定义表单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    @Authorization
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid BusinessFormDto businessFormDto) {
        BusinessForm businessForm = new BusinessForm();
        BeanUtils.copyProperties(businessFormDto, businessForm);
        return super.responseEntity(super.update(id, businessForm));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除自定义表单")
    @ApiOperation(value = "删除自定义表单")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        BusinessFormExample businessFormExample = new BusinessFormExample();
        businessFormExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(businessFormExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "保存表单设计内容")
    @ApiOperation(value = "保存表单设计内容")
    @ApiImplicitParams({
    })
    @PostMapping(value = "saveConfig")
    @NoParamsSign
    @Authorization
    public ResponseEntity<BaseResult> saveConfig(@RequestBody @Valid BusinessFormConfigDto businessFormConfigDto) {
        return super.responseEntity(businessFormService.saveConfig(businessFormConfigDto));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "发布表单")
    @ApiOperation(value = "发布表单")
    @ApiImplicitParams({
    })
    @PostMapping(value = "publish")
    @NoParamsSign
    @Authorization
    public ResponseEntity<BaseResult> publish(@RequestBody @Valid String id) {
        return super.responseEntity(businessFormService.publish(id));
    }
    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "撤销表单")
    @ApiOperation(value = "撤销表单")
    @ApiImplicitParams({
    })
    @PostMapping(value = "revoke")
    @NoParamsSign
    @Authorization
    public ResponseEntity<BaseResult> revoke(@RequestBody @Valid String id) {
        return super.responseEntity(businessFormService.revoke(id));
    }
}