package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-07-23
 */
public class BusinessPayNotifyExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public BusinessPayNotifyExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andNotifyDateIsNull() {
			addCriterion("notify_date is null");
			return (Criteria) this;
		}

		public Criteria andNotifyDateIsNotNull() {
			addCriterion("notify_date is not null");
			return (Criteria) this;
		}

		public Criteria andNotifyDateEqualTo(Long value) {
			addCriterion("notify_date =", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateNotEqualTo(Long value) {
			addCriterion("notify_date <>", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateGreaterThan(Long value) {
			addCriterion("notify_date >", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateGreaterThanOrEqualTo(Long value) {
			addCriterion("notify_date >=", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateLessThan(Long value) {
			addCriterion("notify_date <", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateLessThanOrEqualTo(Long value) {
			addCriterion("notify_date <=", value, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateIn(List<Long> values) {
			addCriterion("notify_date in", values, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateNotIn(List<Long> values) {
			addCriterion("notify_date not in", values, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateBetween(Long value1, Long value2) {
			addCriterion("notify_date between", value1, value2, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyDateNotBetween(Long value1, Long value2) {
			addCriterion("notify_date not between", value1, value2, "notifyDate");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeIsNull() {
			addCriterion("notify_type is null");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeIsNotNull() {
			addCriterion("notify_type is not null");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeEqualTo(String value) {
			addCriterion("notify_type =", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeNotEqualTo(String value) {
			addCriterion("notify_type <>", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeGreaterThan(String value) {
			addCriterion("notify_type >", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeGreaterThanOrEqualTo(String value) {
			addCriterion("notify_type >=", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeLessThan(String value) {
			addCriterion("notify_type <", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeLessThanOrEqualTo(String value) {
			addCriterion("notify_type <=", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeLike(String value) {
			addCriterion("notify_type like", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeNotLike(String value) {
			addCriterion("notify_type not like", value, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeIn(List<String> values) {
			addCriterion("notify_type in", values, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeNotIn(List<String> values) {
			addCriterion("notify_type not in", values, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeBetween(String value1, String value2) {
			addCriterion("notify_type between", value1, value2, "notifyType");
			return (Criteria) this;
		}

		public Criteria andNotifyTypeNotBetween(String value1, String value2) {
			addCriterion("notify_type not between", value1, value2, "notifyType");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIsNull() {
			addCriterion("channel_pay_way is null");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIsNotNull() {
			addCriterion("channel_pay_way is not null");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayEqualTo(String value) {
			addCriterion("channel_pay_way =", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotEqualTo(String value) {
			addCriterion("channel_pay_way <>", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayGreaterThan(String value) {
			addCriterion("channel_pay_way >", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayGreaterThanOrEqualTo(String value) {
			addCriterion("channel_pay_way >=", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLessThan(String value) {
			addCriterion("channel_pay_way <", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLessThanOrEqualTo(String value) {
			addCriterion("channel_pay_way <=", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLike(String value) {
			addCriterion("channel_pay_way like", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotLike(String value) {
			addCriterion("channel_pay_way not like", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIn(List<String> values) {
			addCriterion("channel_pay_way in", values, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotIn(List<String> values) {
			addCriterion("channel_pay_way not in", values, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayBetween(String value1, String value2) {
			addCriterion("channel_pay_way between", value1, value2, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotBetween(String value1, String value2) {
			addCriterion("channel_pay_way not between", value1, value2, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andNotifyContentIsNull() {
			addCriterion("notify_content is null");
			return (Criteria) this;
		}

		public Criteria andNotifyContentIsNotNull() {
			addCriterion("notify_content is not null");
			return (Criteria) this;
		}

		public Criteria andNotifyContentEqualTo(String value) {
			addCriterion("notify_content =", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentNotEqualTo(String value) {
			addCriterion("notify_content <>", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentGreaterThan(String value) {
			addCriterion("notify_content >", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentGreaterThanOrEqualTo(String value) {
			addCriterion("notify_content >=", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentLessThan(String value) {
			addCriterion("notify_content <", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentLessThanOrEqualTo(String value) {
			addCriterion("notify_content <=", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentLike(String value) {
			addCriterion("notify_content like", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentNotLike(String value) {
			addCriterion("notify_content not like", value, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentIn(List<String> values) {
			addCriterion("notify_content in", values, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentNotIn(List<String> values) {
			addCriterion("notify_content not in", values, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentBetween(String value1, String value2) {
			addCriterion("notify_content between", value1, value2, "notifyContent");
			return (Criteria) this;
		}

		public Criteria andNotifyContentNotBetween(String value1, String value2) {
			addCriterion("notify_content not between", value1, value2, "notifyContent");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}