package cn.chiship.framework.business.biz.content.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlot;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlotExample;

/**
 * 广告版位表业务接口层 2024/10/31
 *
 * @author lijian
 */
public interface ContentAdvertSlotService extends BaseService<ContentAdvertSlot, ContentAdvertSlotExample> {

}
