package cn.chiship.framework.business.biz.content.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 文档同步微信素材库表单 2024/10/19
 *
 * @author LiJian
 */
@ApiModel(value = "文档同步微信素材库表单")
public class ContentSyncWxPubDto {

    @ApiModelProperty(value = "文章主键", required = true)
    private String id;

    @ApiModelProperty(value = "素材库分类", required = true)
    private String category;

    @ApiModelProperty(value = "三方应用ID", required = true)
    private String appId;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
