package cn.chiship.framework.business.biz.cashier.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 微信预支付
 *
 * @author lijian
 */
@ApiModel(value = "微信预支付表单")
public class PrepayDto {

    @ApiModelProperty(value = "订单号", required = true)
    @NotNull(message = "订单号" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 6, max = 20, message = "订单号" + BaseTipConstants.LENGTH_MIN_MAX)
    String orderNo;

    @ApiModelProperty(value = "用户标识", required = true)
    @NotNull(message = "用户标识" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 6, max = 50, message = "用户标识" + BaseTipConstants.LENGTH_MIN_MAX)
    String openId;

    @ApiModelProperty(value = "交易方式（OFFICIAL_ACCOUNT：微信公众号；MINI_PROGRAM：小程序）", required = true)
    @NotNull(message = "交易方式" + BaseTipConstants.NOT_EMPTY)
    @Pattern(regexp = "OFFICIAL_ACCOUNT|MINI_PROGRAM", message = "支付渠道：OFFICIAL_ACCOUNT，MINI_PROGRAM")
    String tradeType;

    @ApiModelProperty(value = "支付渠道（wx_v3：微信；zfb：支付宝）", required = true)
    @NotNull(message = "支付渠道" + BaseTipConstants.NOT_EMPTY)
    @Pattern(regexp = "wx_v3|zfb", message = "支付渠道：wx_v3，zfb")
    String payMethod;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public String getTradeType() {
        return tradeType;
    }

    public void setTradeType(String tradeType) {
        this.tradeType = tradeType;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(String payMethod) {
        this.payMethod = payMethod;
    }
}
