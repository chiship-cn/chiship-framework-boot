package cn.chiship.framework.business.biz.cashier.pojo.dto;

import cn.chiship.sdk.pay.core.enums.PayEnum;
import cn.chiship.sdk.pay.core.enums.PayNotifyEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * 支付通知表单 2024/6/10
 *
 * @author LiJian
 */
@ApiModel(value = "支付通知表单")
public class BusinessPayNotifyDto {

	@ApiModelProperty(value = "通知时间")
	@NotNull
	private Long notifyDate;

	@ApiModelProperty(value = "通知类型")
	@NotNull
	private PayNotifyEnum payNotifyEnum;

	@ApiModelProperty(value = "渠道")
	@NotNull
	private PayEnum payEnum;

	@ApiModelProperty(value = "通知内容")
	@NotNull
	@Length(min = 1)
	private String notifyContent;

	public BusinessPayNotifyDto(@NotNull Long notifyDate, @NotNull PayNotifyEnum payNotifyEnum,
			@NotNull PayEnum payEnum, @NotNull @Length(min = 1) String notifyContent) {
		this.notifyDate = notifyDate;
		this.payNotifyEnum = payNotifyEnum;
		this.payEnum = payEnum;
		this.notifyContent = notifyContent;
	}

	public Long getNotifyDate() {
		return notifyDate;
	}

	public void setNotifyDate(Long notifyDate) {
		this.notifyDate = notifyDate;
	}

	public PayNotifyEnum getPayNotifyEnum() {
		return payNotifyEnum;
	}

	public void setPayNotifyEnum(PayNotifyEnum payNotifyEnum) {
		this.payNotifyEnum = payNotifyEnum;
	}

	public PayEnum getPayEnum() {
		return payEnum;
	}

	public void setPayEnum(PayEnum payEnum) {
		this.payEnum = payEnum;
	}

	public String getNotifyContent() {
		return notifyContent;
	}

	public void setNotifyContent(String notifyContent) {
		this.notifyContent = notifyContent;
	}

}
