package cn.chiship.framework.business.biz.business.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.business.entity.TestExample;
import cn.chiship.framework.business.biz.business.entity.TestExampleExample;

import javax.servlet.http.HttpServletResponse;

/**
 * 示例（正式环境可删除）业务接口层 2022/9/15
 *
 * @author lijian
 */
public interface TestExampleService extends BaseService<TestExample, TestExampleExample> {

    /**
     * 生成pdf
     *
     * @param id
     * @param response
     */
    void generatePdf(String id, HttpServletResponse response) throws Exception;

    /**
     * 生成Word
     *
     * @param id
     * @param response
     */
    void generateWord(String id, HttpServletResponse response) throws Exception;
}
