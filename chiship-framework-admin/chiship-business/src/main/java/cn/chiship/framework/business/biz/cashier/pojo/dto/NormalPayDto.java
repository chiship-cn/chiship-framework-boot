package cn.chiship.framework.business.biz.cashier.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * 普通支付表单
 *
 * @author lijian
 */
@ApiModel(value = "普通支付表单")
public class NormalPayDto {

	@ApiModelProperty(value = "订单号", required = true)
	@NotNull(message = "订单号" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 6, max = 20, message = "订单号" + BaseTipConstants.LENGTH_MIN_MAX)
	String orderNo;

	@ApiModelProperty(value = "支付渠道（wx_v3：微信；zfb：支付宝）", required = true)
	@NotNull(message = "支付渠道" + BaseTipConstants.NOT_EMPTY)
	@Pattern(regexp = "wx_v3|zfb", message = "支付渠道：wx_v3，zfb")
	String payMethod;

	@ApiModelProperty(value = "支付成功跳转页面（支付宝H5支付、PC支付）")
	String returnUrl;

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getReturnUrl() {
		return returnUrl;
	}

	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}

}
