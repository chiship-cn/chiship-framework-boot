package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.framework.business.biz.member.enmus.MemberComeSourceEnum;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import cn.chiship.sdk.core.base.constants.RegexConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

/**
 * 会员表单 2022/2/23
 *
 * @author LiJian
 */
@ApiModel(value = "会员表单")
public class MemberUserDto {

    @ApiModelProperty(value = "昵称")
    @Length(max = 100)
    private String nickName;

    @ApiModelProperty(value = "用户名", required = true)
    @NotEmpty(message = "用户名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 20)
    private String userName;

    @ApiModelProperty(value = "姓名", required = true)
    @NotEmpty(message = "姓名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 20)
    private String realName;

    @ApiModelProperty(value = "手机号", required = true)
    @NotEmpty(message = "手机号" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 11, max = 11)
    @Pattern(regexp = RegexConstants.MOBILE, message = BaseTipConstants.MOBILE)
    private String mobile;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "性别  0未知 1男 2女")
    private Byte gender;

    @ApiModelProperty(value = "出生日期")
    private Long birthday;

    @ApiModelProperty(value = "简介")
    @Length(max = 300)
    private String introduction;

    @ApiModelProperty(value = "来源枚举", required = true)
    private MemberComeSourceEnum comeSourceEnum;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public MemberComeSourceEnum getComeSourceEnum() {
        return comeSourceEnum;
    }

    public void setComeSourceEnum(MemberComeSourceEnum comeSourceEnum) {
        this.comeSourceEnum = comeSourceEnum;
    }
}