package cn.chiship.framework.business.biz.cashier.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;
import javax.annotation.Resource;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderHeaderItemService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItem;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItemExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessOrderHeaderItemDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单明细控制层
 * 2024/12/16
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/businessOrderHeaderItem")
@Api(tags = "订单明细")
public class BusinessOrderHeaderItemController extends BaseController <BusinessOrderHeaderItem,BusinessOrderHeaderItemExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessOrderHeaderItemController.class);

    @Resource
    private BusinessOrderHeaderItemService businessOrderHeaderItemService;
    @Override
    public BaseService getService() {
        return businessOrderHeaderItemService;
    }

    @SystemOptionAnnotation(describe = "订单明细分页")
    @ApiOperation(value = "订单明细分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        BusinessOrderHeaderItemExample businessOrderHeaderItemExample = new BusinessOrderHeaderItemExample();
        //创造条件
        BusinessOrderHeaderItemExample.Criteria businessOrderHeaderItemCriteria = businessOrderHeaderItemExample.createCriteria();
        businessOrderHeaderItemCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        return super.responseEntity(BaseResult.ok(super.page(businessOrderHeaderItemExample)));
    }

    @SystemOptionAnnotation(describe = "订单明细列表数据")
    @ApiOperation(value = "订单明细列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        BusinessOrderHeaderItemExample businessOrderHeaderItemExample = new BusinessOrderHeaderItemExample();
        //创造条件
        BusinessOrderHeaderItemExample.Criteria businessOrderHeaderItemCriteria = businessOrderHeaderItemExample.createCriteria();
        businessOrderHeaderItemCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        businessOrderHeaderItemExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(businessOrderHeaderItemExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存订单明细")
    @ApiOperation(value = "保存订单明细")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid BusinessOrderHeaderItemDto businessOrderHeaderItemDto) {
        BusinessOrderHeaderItem businessOrderHeaderItem = new BusinessOrderHeaderItem();
        BeanUtils.copyProperties(businessOrderHeaderItemDto, businessOrderHeaderItem);
        return super.responseEntity(super.save(businessOrderHeaderItem));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新订单明细")
    @ApiOperation(value = "更新订单明细")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid BusinessOrderHeaderItemDto businessOrderHeaderItemDto) {
        BusinessOrderHeaderItem businessOrderHeaderItem = new BusinessOrderHeaderItem();
        BeanUtils.copyProperties(businessOrderHeaderItemDto, businessOrderHeaderItem);
        return super.responseEntity(super.update(id,businessOrderHeaderItem));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除订单明细")
    @ApiOperation(value = "删除订单明细")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        BusinessOrderHeaderItemExample businessOrderHeaderItemExample = new BusinessOrderHeaderItemExample();
        businessOrderHeaderItemExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(businessOrderHeaderItemExample));
    }
}