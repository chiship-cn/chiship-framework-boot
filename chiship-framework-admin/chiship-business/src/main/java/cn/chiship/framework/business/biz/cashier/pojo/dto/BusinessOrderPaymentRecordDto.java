package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * 订单支付表单 2024/5/31
 *
 * @author LiJian
 */
@ApiModel(value = "订单支付表单")
public class BusinessOrderPaymentRecordDto {

	@ApiModelProperty(value = "订单号")
	private String orderId;

	@ApiModelProperty(value = "交易号")
	private String tradeNo;

	@ApiModelProperty(value = "交易方式")
	private String tradeType;

	@ApiModelProperty(value = "支付方式")
	private String channelPayWay;

	@ApiModelProperty(value = "总费用")
	private BigDecimal totalFee;

	@ApiModelProperty(value = "支付状态")
	private Byte payStatus;

	@ApiModelProperty(value = "商户号")
	private String partnerId;

	@ApiModelProperty(value = "应用唯一标识")
	private String appId;

	@ApiModelProperty(value = "交易概述")
	private String subject;

	@ApiModelProperty(value = "描述")
	private String remark;

	@ApiModelProperty(value = "支付用户")
	private String userId;

	@ApiModelProperty(value = "收款帐号")
	private String account;

	@ApiModelProperty(value = "付款时间")
	private Long payTime;

	public BusinessOrderPaymentRecordDto() {
	}

	public BusinessOrderPaymentRecordDto(String orderId, String tradeNo, String tradeType, String channelPayWay,
			BigDecimal totalFee, Byte payStatus, String partnerId, String subject, String remark, String account,
			Long payTime) {
		this.orderId = orderId;
		this.tradeNo = tradeNo;
		this.tradeType = tradeType;
		this.channelPayWay = channelPayWay;
		this.totalFee = totalFee;
		this.payStatus = payStatus;
		this.partnerId = partnerId;
		this.subject = subject;
		this.remark = remark;
		this.account = account;
		this.payTime = payTime;
	}

	public BusinessOrderPaymentRecordDto(String orderId, String tradeNo, String tradeType, String channelPayWay,
			BigDecimal totalFee, Byte payStatus, String partnerId, String subject, String remark, String userId,
			String account, Long payTime) {
		this.orderId = orderId;
		this.tradeNo = tradeNo;
		this.tradeType = tradeType;
		this.channelPayWay = channelPayWay;
		this.totalFee = totalFee;
		this.payStatus = payStatus;
		this.partnerId = partnerId;
		this.subject = subject;
		this.remark = remark;
		this.userId = userId;
		this.account = account;
		this.payTime = payTime;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

	public BigDecimal getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(BigDecimal totalFee) {
		this.totalFee = totalFee;
	}

	public Byte getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Byte payStatus) {
		this.payStatus = payStatus;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

}
