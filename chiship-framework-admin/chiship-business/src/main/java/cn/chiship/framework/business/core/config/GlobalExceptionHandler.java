package cn.chiship.framework.business.core.config;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.pojo.dto.SystemExceptionLogDto;
import cn.chiship.framework.common.util.FrameworkUtil2;
import cn.chiship.framework.upms.biz.system.entity.UpmsSystemExceptionLog;
import cn.chiship.framework.upms.biz.system.service.UpmsSystemExceptionLogService;
import cn.chiship.sdk.cache.service.UserCacheService;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.exception.ExceptionUtil;
import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.exception.custom.PermissionsException;
import cn.chiship.sdk.core.exception.model.CommonExceptionVo;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.http.CustomRequestWrapper;
import cn.chiship.sdk.core.util.http.RequestUtil;
import cn.chiship.sdk.core.util.ip.IpUtils;
import cn.chiship.sdk.framework.exception.FrameworkBuildException;
import cn.chiship.sdk.framework.util.BindingResultUtil;
import cn.chiship.sdk.framework.util.ServletUtil;
import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 使用@ControllerAdvice+@ExceptionHandler统一处理异常 controller中不需捕获异常
 *
 * @author lijian
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	private static final String REQUEST_ID = "requestId";

	private static final String CONTENT_TYPE_APPLICATION_JSON = "application/json";

	private static final String STRING_METHOD_NAME = "methodName";

	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@Resource
	private UpmsSystemExceptionLogService upmsSystemExceptionLogService;

	@Resource
	private UserCacheService userCacheService;

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<BaseResult> validationBodyException(MethodArgumentNotValidException exception) {
		BindingResult result = exception.getBindingResult();
		BaseResult baseResult = BindingResultUtil.format(result);
		saveExceptionLog(baseResult, new BusinessException("DTO校验错误"));
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

	@ExceptionHandler(MissingServletRequestParameterException.class)
	public ResponseEntity<BaseResult> validationRequestParameterException(
			MissingServletRequestParameterException exception) {
		Map<String, Object> errorMap = Maps.newHashMapWithExpectedSize(7);
		errorMap.put(exception.getParameterName(),
				"缺少[" + exception.getParameterType() + "]类型参数[" + exception.getParameterName() + "]");
		BaseResult baseResult = BaseResult.error(BaseResultEnum.PARAM_CHECK_FAILED, errorMap);
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

	@ExceptionHandler({ BusinessException.class, PermissionsException.class })
	public ResponseEntity<BaseResult> handleBusinessException(BusinessException exception) {
		BaseResult baseResult = ExceptionUtil.formatException(exception);
		saveExceptionLog(baseResult, exception);
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

	/**
	 * 业务异常外的所有Exception
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_GATEWAY)
	public ResponseEntity<BaseResult> handleException(Exception e) {
		HttpServletRequest request = ServletUtil.getRequest();
		String requestId = null;
		if (!StringUtil.isNull(request.getAttribute(REQUEST_ID))) {
			requestId = request.getAttribute(REQUEST_ID).toString();
		}
		BaseResult baseResult = new FrameworkBuildException().buildException(requestId, e);
		saveExceptionLog(baseResult, e);
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

	public void saveExceptionLog(BaseResult baseResult, Exception exception) {
		if (FrameworkUtil2.getChishipDefaultProperties().isEnableLogInterception()) {
			logger.error(exception.getMessage(), exception);
		}
		CommonExceptionVo exceptionVo = ExceptionUtil.formatExceptionVo(exception);
		HttpServletRequest request = ServletUtil.getRequest();
		UpmsSystemExceptionLog systemExceptionLog = new UpmsSystemExceptionLog();
		systemExceptionLog.setBasePath(RequestUtil.getBasePath(request));
		systemExceptionLog.setIp(IpUtils.getIpAddr(request));
		systemExceptionLog.setRequestType(request.getMethod());
		systemExceptionLog.setUri(request.getRequestURI());
		systemExceptionLog.setUserAgent(request.getHeader("User-Agent"));
		systemExceptionLog.setErrorClass(exception.getClass().getName());

		// 没用二级错误
		if (StringUtil.isNull(exceptionVo)) {
			systemExceptionLog.setErrorLocalizedMessage(ExceptionUtil.getErrorLocalizedMessage(exception));
			systemExceptionLog.setDescription(baseResult.getMessage());
		}
		else {
			systemExceptionLog.setErrorLocalizedMessage(exceptionVo.getErrorLocalizedMessage());
			systemExceptionLog.setDescription(exceptionVo.getErrorMessage());
		}

		if (request.getAttribute(STRING_METHOD_NAME) != null) {
			systemExceptionLog.setMethod(request.getAttribute(STRING_METHOD_NAME).toString());
		}
		if (request.getAttribute(REQUEST_ID) != null) {
			systemExceptionLog.setRequestId(request.getAttribute(REQUEST_ID).toString());
		}

		try {
			if (BaseConstants.HTTP_REQUEST_TYPE_GET.equalsIgnoreCase(request.getMethod())) {
				systemExceptionLog.setParameter(request.getQueryString());
			}
			else {
				String contentType = request.getHeader("Content-Type");
				if (contentType.indexOf(CONTENT_TYPE_APPLICATION_JSON) >= 0) {
					CustomRequestWrapper requestWrapper = new CustomRequestWrapper(request);
					systemExceptionLog.setParameter(requestWrapper.getBody());
				}
				else {
					systemExceptionLog.setParameter(JSON.toJSONString(request.getParameterMap()));
				}
			}
		}
		catch (Exception e) {
		}
		try {
			CacheUserVO userVO = userCacheService.getUser();
			if (userVO != null) {
				systemExceptionLog.setUserId(userVO.getId());
				systemExceptionLog.setUserName(userVO.getUsername());
				systemExceptionLog.setRealName(userVO.getRealName());
			}
		}
		catch (Exception e) {
		}
		try {
			SystemOptionAnnotation systemOption = (SystemOptionAnnotation) request.getAttribute("systemOption");
			if (null != systemOption) {
				systemExceptionLog.setSystemName(systemOption.systemName());
			}
		}
		catch (Exception e) {
		}
		SystemExceptionLogDto systemExceptionLogDto = new SystemExceptionLogDto();
		BeanUtils.copyProperties(systemExceptionLog, systemExceptionLogDto);
		FrameworkUtil2.formatErrorMessage(systemExceptionLogDto);
		upmsSystemExceptionLogService.insertSelective(systemExceptionLog);
	}

}
