package cn.chiship.framework.business.biz.product.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 商品分类表单
 * 2024/12/11
 * @author LiJian
 */
@ApiModel(value = "商品分类表单")
public class ProductCategoryDto{

    @ApiModelProperty(value = "分类名称", required = true)
    @NotNull(message = "分类名称" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 10, message = "分类名称" + BaseTipConstants.LENGTH_MIN_MAX)
    private String categoryName;


    @ApiModelProperty(value = "所属上级", required = true)
    @NotNull(message = "所属上级" + BaseTipConstants.NOT_EMPTY)
    private String parentId;

    @ApiModelProperty(value = "分类描述")
    @Length( max = 100, message = "品牌名称" + BaseTipConstants.LENGTH_MIN_MAX)
    private String categoryDesc;

    @ApiModelProperty(value = "分类图标")
    private String categoryIcon;

    @ApiModelProperty(value = "是否显示", required = true)
    @NotNull(message = "是否显示" + BaseTipConstants.NOT_EMPTY)
    @Min(0)
    @Max(1)
    private Byte isShow;

    @ApiModelProperty(value = "排序", required = true)
    @NotNull(message = "排序" + BaseTipConstants.NOT_EMPTY)
    private Integer orders;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCategoryDesc() {
        return categoryDesc;
    }

    public void setCategoryDesc(String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }

    public String getCategoryIcon() {
        return categoryIcon;
    }

    public void setCategoryIcon(String categoryIcon) {
        this.categoryIcon = categoryIcon;
    }

    public Byte getIsShow() {
        return isShow;
    }

    public void setIsShow(Byte isShow) {
        this.isShow = isShow;
    }

    public Integer getOrders() {
        return orders;
    }

    public void setOrders(Integer orders) {
        this.orders = orders;
    }
}