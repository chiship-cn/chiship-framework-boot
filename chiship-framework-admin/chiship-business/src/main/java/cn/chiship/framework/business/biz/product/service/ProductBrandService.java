package cn.chiship.framework.business.biz.product.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.product.entity.ProductBrand;
import cn.chiship.framework.business.biz.product.entity.ProductBrandExample;
/**
 * 品牌业务接口层
 * 2024/12/11
 * @author lijian
 */
public interface ProductBrandService extends BaseService<ProductBrand,ProductBrandExample> {

}
