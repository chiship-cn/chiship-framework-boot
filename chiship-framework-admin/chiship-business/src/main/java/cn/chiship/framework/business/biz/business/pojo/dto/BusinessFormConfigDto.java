package cn.chiship.framework.business.biz.business.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 自定义表单配置表单
 * 2024/12/13
 *
 * @author LiJian
 */
@ApiModel(value = "自定义表单配置表单")
public class BusinessFormConfigDto {

    @ApiModelProperty(value = "主键", required = true)
    @NotEmpty(message = "主键")
    @Length(min = 1, max = 36)
    private String id;


    @ApiModelProperty(value = "表单组件的规则描述", required = true)
    @NotEmpty(message = "表单组件的规则描述")
    @Length(min = 1)
    private String formDescription;

    @ApiModelProperty(value = "表单设计内容", required = true)
    @NotEmpty(message = "表单设计内容")
    @Length(min = 1)
    private String formContent;

    @ApiModelProperty(value = "表单配置项", required = true)
    @NotEmpty(message = "表单配置项")
    @Length(min = 1)
    private String formOption;

    @ApiModelProperty(value = "是否H5表单", required = true)
    @NotNull(message = "是否H5表单")
    private Boolean h5;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormDescription() {
        return formDescription;
    }

    public void setFormDescription(String formDescription) {
        this.formDescription = formDescription;
    }

    public String getFormContent() {
        return formContent;
    }

    public void setFormContent(String formContent) {
        this.formContent = formContent;
    }

    public String getFormOption() {
        return formOption;
    }

    public void setFormOption(String formOption) {
        this.formOption = formOption;
    }

    public Boolean getH5() {
        return h5;
    }

    public void setH5(Boolean h5) {
        this.h5 = h5;
    }
}