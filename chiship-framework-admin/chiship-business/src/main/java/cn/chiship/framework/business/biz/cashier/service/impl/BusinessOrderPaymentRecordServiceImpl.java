package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.framework.business.biz.base.service.impl.BusinessAsyncService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecord;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecordExample;
import cn.chiship.framework.business.biz.cashier.enums.OrderStatusEnum;
import cn.chiship.framework.business.biz.cashier.enums.OrderTypeEnum;
import cn.chiship.framework.business.biz.member.enmus.MemberWalletChangeModuleEnum;
import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;
import cn.chiship.framework.business.biz.cashier.mapper.BusinessOrderHeaderMapper;
import cn.chiship.framework.business.biz.cashier.mapper.BusinessOrderPaymentRecordMapper;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessOrderPaymentRecordDto;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderPaymentRecordService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 订单支付业务接口实现层 2024/5/31
 *
 * @author lijian
 */
@Service
public class BusinessOrderPaymentRecordServiceImpl
        extends BaseServiceImpl<BusinessOrderPaymentRecord, BusinessOrderPaymentRecordExample>
        implements BusinessOrderPaymentRecordService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessOrderPaymentRecordServiceImpl.class);

    @Resource
    BusinessOrderPaymentRecordMapper businessOrderPaymentRecordMapper;

    @Resource
    BusinessOrderHeaderMapper businessOrderHeaderMapper;

    @Resource
    RedisService redisService;

    @Resource
    BusinessAsyncService businessAsyncService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public BaseResult payNotify(BusinessOrderPaymentRecordDto orderPaymentRecordDto) {
        String key = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_TIMEOUT_CLOSED_ORDER_PREFIX) + ":"
                + orderPaymentRecordDto.getOrderId();
        redisService.del(key);
        Byte payStatus = orderPaymentRecordDto.getPayStatus();
        if (Byte.valueOf("0").equals(payStatus)) {
            BusinessOrderHeader orderHeader = businessOrderHeaderMapper.selectByPrimaryKey(orderPaymentRecordDto.getOrderId());
            orderHeader.setOrderStatus(OrderStatusEnum.ORDER_STATUS_PAY_SUCCESS.getStatus());
            orderHeader.setPayTime(orderPaymentRecordDto.getPayTime());
            orderHeader.setTradeNo(orderPaymentRecordDto.getTradeNo());
            orderHeader.setChannelPayWay(orderPaymentRecordDto.getChannelPayWay());
            orderHeader.setTradeType(orderPaymentRecordDto.getTradeType());
            businessOrderHeaderMapper.updateByPrimaryKeySelective(orderHeader);
            BusinessOrderPaymentRecord orderPaymentRecord = new BusinessOrderPaymentRecord();
            BeanUtils.copyProperties(orderPaymentRecordDto, orderPaymentRecord);
            BusinessOrderPaymentRecordExample userOrderPaymentRecordExample = new BusinessOrderPaymentRecordExample();
            userOrderPaymentRecordExample.createCriteria().andTradeNoEqualTo(orderPaymentRecordDto.getTradeNo());
            if (businessOrderPaymentRecordMapper.countByExample(userOrderPaymentRecordExample) == 0) {
                super.insertSelective(orderPaymentRecord);
            }
            saveOrderStatus(orderHeader.getId(), OrderStatusEnum.ORDER_STATUS_PAY_SUCCESS);
            //订单成功，检验是否充值订单，更新钱包
            if (OrderTypeEnum.ORDER_TYPE_RECHARGE.getType().equals(orderHeader.getOrderType())) {
                businessAsyncService.changeMemberWallet(
                        orderHeader.getUserId(),
                        orderPaymentRecord.getTotalFee(),
                        Boolean.FALSE,
                        orderHeader.getOrderName(),
                        MemberWalletChangeModuleEnum.MEMBER_WALLET_CHANGE_MODULE_RECHARGE,
                        orderHeader.getId()
                );
            }
        } else {
            BusinessOrderHeader orderHeader = new BusinessOrderHeader();
            orderHeader.setId(orderPaymentRecordDto.getOrderId());
            orderHeader.setOrderStatus(OrderStatusEnum.ORDER_STATUS_PAY_FAIL.getStatus());
            businessOrderHeaderMapper.updateByPrimaryKeySelective(orderHeader);
            saveOrderStatus(orderHeader.getId(), OrderStatusEnum.ORDER_STATUS_PAY_FAIL);
        }
        return BaseResult.ok();
    }

    @Override
    public BaseResult getByOrderNo(String orderNo) {
        BusinessOrderPaymentRecordExample businessOrderPaymentRecordExample = new BusinessOrderPaymentRecordExample();
        businessOrderPaymentRecordExample.createCriteria().andOrderIdEqualTo(orderNo);
        List<BusinessOrderPaymentRecord> orderPaymentRecords = businessOrderPaymentRecordMapper
                .selectByExample(businessOrderPaymentRecordExample);
        if (orderPaymentRecords.isEmpty()) {
            return BaseResult.error("支付流水不存在");
        }
        return BaseResult.ok(orderPaymentRecords.get(0));
    }

    /**
     * 保存订单状态
     *
     * @param orderNo
     * @param orderStatusEnum
     */
    private BaseResult saveOrderStatus(String orderNo, OrderStatusEnum orderStatusEnum) {
        String key = CommonCacheConstants.buildKey(CommonCacheConstants.REDIS_ORDER_STATUS_PREFIX);
        Map<String, Object> status = new HashMap<>(7);
        status.put("status", orderStatusEnum.getStatus());
        status.put("message", orderStatusEnum.getMessage());
        redisService.hset(key, orderNo, status);
        return BaseResult.ok(status);
    }

}
