package cn.chiship.framework.business.biz.member.service;

import cn.chiship.framework.business.biz.cashier.pojo.dto.WithdrawalApplicationDto;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserWithdrawalExamineDto;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplication;
import cn.chiship.framework.business.biz.member.entity.MemberUserWithdrawalApplicationExample;

/**
 * 提现申请业务接口层
 * 2024/12/17
 *
 * @author lijian
 */
public interface MemberUserWithdrawalApplicationService extends BaseService<MemberUserWithdrawalApplication, MemberUserWithdrawalApplicationExample> {

    /**
     * 提现申请
     * @param withdrawalApplicationDto
     * @param cacheUserVO
     * @return
     */
    BaseResult withdrawalApplication(WithdrawalApplicationDto withdrawalApplicationDto, CacheUserVO cacheUserVO);

    /**
     * 审批提现
     *
     * @param memberUserWithdrawalExamineDto
     * @param cacheUserVO
     * @return
     */
    BaseResult examine(MemberUserWithdrawalExamineDto memberUserWithdrawalExamineDto, CacheUserVO cacheUserVO);

    /**
     * 对账
     *
     * @param no
     * @return
     */
    BaseResult reconciliation(String no);
}
