package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-07-23
 */
public class BusinessRefundOrderExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public BusinessRefundOrderExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andRefundStatusIsNull() {
			addCriterion("refund_status is null");
			return (Criteria) this;
		}

		public Criteria andRefundStatusIsNotNull() {
			addCriterion("refund_status is not null");
			return (Criteria) this;
		}

		public Criteria andRefundStatusEqualTo(Byte value) {
			addCriterion("refund_status =", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusNotEqualTo(Byte value) {
			addCriterion("refund_status <>", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusGreaterThan(Byte value) {
			addCriterion("refund_status >", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("refund_status >=", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusLessThan(Byte value) {
			addCriterion("refund_status <", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusLessThanOrEqualTo(Byte value) {
			addCriterion("refund_status <=", value, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusIn(List<Byte> values) {
			addCriterion("refund_status in", values, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusNotIn(List<Byte> values) {
			addCriterion("refund_status not in", values, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusBetween(Byte value1, Byte value2) {
			addCriterion("refund_status between", value1, value2, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andRefundStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("refund_status not between", value1, value2, "refundStatus");
			return (Criteria) this;
		}

		public Criteria andOrderNoIsNull() {
			addCriterion("order_no is null");
			return (Criteria) this;
		}

		public Criteria andOrderNoIsNotNull() {
			addCriterion("order_no is not null");
			return (Criteria) this;
		}

		public Criteria andOrderNoEqualTo(String value) {
			addCriterion("order_no =", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoNotEqualTo(String value) {
			addCriterion("order_no <>", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoGreaterThan(String value) {
			addCriterion("order_no >", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoGreaterThanOrEqualTo(String value) {
			addCriterion("order_no >=", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoLessThan(String value) {
			addCriterion("order_no <", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoLessThanOrEqualTo(String value) {
			addCriterion("order_no <=", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoLike(String value) {
			addCriterion("order_no like", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoNotLike(String value) {
			addCriterion("order_no not like", value, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoIn(List<String> values) {
			addCriterion("order_no in", values, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoNotIn(List<String> values) {
			addCriterion("order_no not in", values, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoBetween(String value1, String value2) {
			addCriterion("order_no between", value1, value2, "orderNo");
			return (Criteria) this;
		}

		public Criteria andOrderNoNotBetween(String value1, String value2) {
			addCriterion("order_no not between", value1, value2, "orderNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoIsNull() {
			addCriterion("trade_no is null");
			return (Criteria) this;
		}

		public Criteria andTradeNoIsNotNull() {
			addCriterion("trade_no is not null");
			return (Criteria) this;
		}

		public Criteria andTradeNoEqualTo(String value) {
			addCriterion("trade_no =", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoNotEqualTo(String value) {
			addCriterion("trade_no <>", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoGreaterThan(String value) {
			addCriterion("trade_no >", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoGreaterThanOrEqualTo(String value) {
			addCriterion("trade_no >=", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoLessThan(String value) {
			addCriterion("trade_no <", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoLessThanOrEqualTo(String value) {
			addCriterion("trade_no <=", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoLike(String value) {
			addCriterion("trade_no like", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoNotLike(String value) {
			addCriterion("trade_no not like", value, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoIn(List<String> values) {
			addCriterion("trade_no in", values, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoNotIn(List<String> values) {
			addCriterion("trade_no not in", values, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoBetween(String value1, String value2) {
			addCriterion("trade_no between", value1, value2, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andTradeNoNotBetween(String value1, String value2) {
			addCriterion("trade_no not between", value1, value2, "tradeNo");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedNameIsNull() {
			addCriterion("created_name is null");
			return (Criteria) this;
		}

		public Criteria andCreatedNameIsNotNull() {
			addCriterion("created_name is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedNameEqualTo(String value) {
			addCriterion("created_name =", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameNotEqualTo(String value) {
			addCriterion("created_name <>", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameGreaterThan(String value) {
			addCriterion("created_name >", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameGreaterThanOrEqualTo(String value) {
			addCriterion("created_name >=", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameLessThan(String value) {
			addCriterion("created_name <", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameLessThanOrEqualTo(String value) {
			addCriterion("created_name <=", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameLike(String value) {
			addCriterion("created_name like", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameNotLike(String value) {
			addCriterion("created_name not like", value, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameIn(List<String> values) {
			addCriterion("created_name in", values, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameNotIn(List<String> values) {
			addCriterion("created_name not in", values, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameBetween(String value1, String value2) {
			addCriterion("created_name between", value1, value2, "createdName");
			return (Criteria) this;
		}

		public Criteria andCreatedNameNotBetween(String value1, String value2) {
			addCriterion("created_name not between", value1, value2, "createdName");
			return (Criteria) this;
		}

		public Criteria andRefundTotalIsNull() {
			addCriterion("refund_total is null");
			return (Criteria) this;
		}

		public Criteria andRefundTotalIsNotNull() {
			addCriterion("refund_total is not null");
			return (Criteria) this;
		}

		public Criteria andRefundTotalEqualTo(BigDecimal value) {
			addCriterion("refund_total =", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalNotEqualTo(BigDecimal value) {
			addCriterion("refund_total <>", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalGreaterThan(BigDecimal value) {
			addCriterion("refund_total >", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("refund_total >=", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalLessThan(BigDecimal value) {
			addCriterion("refund_total <", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalLessThanOrEqualTo(BigDecimal value) {
			addCriterion("refund_total <=", value, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalIn(List<BigDecimal> values) {
			addCriterion("refund_total in", values, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalNotIn(List<BigDecimal> values) {
			addCriterion("refund_total not in", values, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("refund_total between", value1, value2, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTotalNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("refund_total not between", value1, value2, "refundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundReasonIsNull() {
			addCriterion("refund_reason is null");
			return (Criteria) this;
		}

		public Criteria andRefundReasonIsNotNull() {
			addCriterion("refund_reason is not null");
			return (Criteria) this;
		}

		public Criteria andRefundReasonEqualTo(String value) {
			addCriterion("refund_reason =", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonNotEqualTo(String value) {
			addCriterion("refund_reason <>", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonGreaterThan(String value) {
			addCriterion("refund_reason >", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonGreaterThanOrEqualTo(String value) {
			addCriterion("refund_reason >=", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonLessThan(String value) {
			addCriterion("refund_reason <", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonLessThanOrEqualTo(String value) {
			addCriterion("refund_reason <=", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonLike(String value) {
			addCriterion("refund_reason like", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonNotLike(String value) {
			addCriterion("refund_reason not like", value, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonIn(List<String> values) {
			addCriterion("refund_reason in", values, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonNotIn(List<String> values) {
			addCriterion("refund_reason not in", values, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonBetween(String value1, String value2) {
			addCriterion("refund_reason between", value1, value2, "refundReason");
			return (Criteria) this;
		}

		public Criteria andRefundReasonNotBetween(String value1, String value2) {
			addCriterion("refund_reason not between", value1, value2, "refundReason");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIsNull() {
			addCriterion("channel_pay_way is null");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIsNotNull() {
			addCriterion("channel_pay_way is not null");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayEqualTo(String value) {
			addCriterion("channel_pay_way =", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotEqualTo(String value) {
			addCriterion("channel_pay_way <>", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayGreaterThan(String value) {
			addCriterion("channel_pay_way >", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayGreaterThanOrEqualTo(String value) {
			addCriterion("channel_pay_way >=", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLessThan(String value) {
			addCriterion("channel_pay_way <", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLessThanOrEqualTo(String value) {
			addCriterion("channel_pay_way <=", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayLike(String value) {
			addCriterion("channel_pay_way like", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotLike(String value) {
			addCriterion("channel_pay_way not like", value, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayIn(List<String> values) {
			addCriterion("channel_pay_way in", values, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotIn(List<String> values) {
			addCriterion("channel_pay_way not in", values, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayBetween(String value1, String value2) {
			addCriterion("channel_pay_way between", value1, value2, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andChannelPayWayNotBetween(String value1, String value2) {
			addCriterion("channel_pay_way not between", value1, value2, "channelPayWay");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalIsNull() {
			addCriterion("real_refund_total is null");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalIsNotNull() {
			addCriterion("real_refund_total is not null");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalEqualTo(BigDecimal value) {
			addCriterion("real_refund_total =", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalNotEqualTo(BigDecimal value) {
			addCriterion("real_refund_total <>", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalGreaterThan(BigDecimal value) {
			addCriterion("real_refund_total >", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalGreaterThanOrEqualTo(BigDecimal value) {
			addCriterion("real_refund_total >=", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalLessThan(BigDecimal value) {
			addCriterion("real_refund_total <", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalLessThanOrEqualTo(BigDecimal value) {
			addCriterion("real_refund_total <=", value, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalIn(List<BigDecimal> values) {
			addCriterion("real_refund_total in", values, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalNotIn(List<BigDecimal> values) {
			addCriterion("real_refund_total not in", values, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("real_refund_total between", value1, value2, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRealRefundTotalNotBetween(BigDecimal value1, BigDecimal value2) {
			addCriterion("real_refund_total not between", value1, value2, "realRefundTotal");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoIsNull() {
			addCriterion("refund_trade_no is null");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoIsNotNull() {
			addCriterion("refund_trade_no is not null");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoEqualTo(String value) {
			addCriterion("refund_trade_no =", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoNotEqualTo(String value) {
			addCriterion("refund_trade_no <>", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoGreaterThan(String value) {
			addCriterion("refund_trade_no >", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoGreaterThanOrEqualTo(String value) {
			addCriterion("refund_trade_no >=", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoLessThan(String value) {
			addCriterion("refund_trade_no <", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoLessThanOrEqualTo(String value) {
			addCriterion("refund_trade_no <=", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoLike(String value) {
			addCriterion("refund_trade_no like", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoNotLike(String value) {
			addCriterion("refund_trade_no not like", value, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoIn(List<String> values) {
			addCriterion("refund_trade_no in", values, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoNotIn(List<String> values) {
			addCriterion("refund_trade_no not in", values, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoBetween(String value1, String value2) {
			addCriterion("refund_trade_no between", value1, value2, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andRefundTradeNoNotBetween(String value1, String value2) {
			addCriterion("refund_trade_no not between", value1, value2, "refundTradeNo");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountIsNull() {
			addCriterion("received_account is null");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountIsNotNull() {
			addCriterion("received_account is not null");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountEqualTo(String value) {
			addCriterion("received_account =", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountNotEqualTo(String value) {
			addCriterion("received_account <>", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountGreaterThan(String value) {
			addCriterion("received_account >", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountGreaterThanOrEqualTo(String value) {
			addCriterion("received_account >=", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountLessThan(String value) {
			addCriterion("received_account <", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountLessThanOrEqualTo(String value) {
			addCriterion("received_account <=", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountLike(String value) {
			addCriterion("received_account like", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountNotLike(String value) {
			addCriterion("received_account not like", value, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountIn(List<String> values) {
			addCriterion("received_account in", values, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountNotIn(List<String> values) {
			addCriterion("received_account not in", values, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountBetween(String value1, String value2) {
			addCriterion("received_account between", value1, value2, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andReceivedAccountNotBetween(String value1, String value2) {
			addCriterion("received_account not between", value1, value2, "receivedAccount");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeIsNull() {
			addCriterion("refund_finish_time is null");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeIsNotNull() {
			addCriterion("refund_finish_time is not null");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeEqualTo(Long value) {
			addCriterion("refund_finish_time =", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeNotEqualTo(Long value) {
			addCriterion("refund_finish_time <>", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeGreaterThan(Long value) {
			addCriterion("refund_finish_time >", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("refund_finish_time >=", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeLessThan(Long value) {
			addCriterion("refund_finish_time <", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeLessThanOrEqualTo(Long value) {
			addCriterion("refund_finish_time <=", value, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeIn(List<Long> values) {
			addCriterion("refund_finish_time in", values, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeNotIn(List<Long> values) {
			addCriterion("refund_finish_time not in", values, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeBetween(Long value1, Long value2) {
			addCriterion("refund_finish_time between", value1, value2, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andRefundFinishTimeNotBetween(Long value1, Long value2) {
			addCriterion("refund_finish_time not between", value1, value2, "refundFinishTime");
			return (Criteria) this;
		}

		public Criteria andIsNotifyIsNull() {
			addCriterion("is_notify is null");
			return (Criteria) this;
		}

		public Criteria andIsNotifyIsNotNull() {
			addCriterion("is_notify is not null");
			return (Criteria) this;
		}

		public Criteria andIsNotifyEqualTo(Byte value) {
			addCriterion("is_notify =", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyNotEqualTo(Byte value) {
			addCriterion("is_notify <>", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyGreaterThan(Byte value) {
			addCriterion("is_notify >", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_notify >=", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyLessThan(Byte value) {
			addCriterion("is_notify <", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyLessThanOrEqualTo(Byte value) {
			addCriterion("is_notify <=", value, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyIn(List<Byte> values) {
			addCriterion("is_notify in", values, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyNotIn(List<Byte> values) {
			addCriterion("is_notify not in", values, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyBetween(Byte value1, Byte value2) {
			addCriterion("is_notify between", value1, value2, "isNotify");
			return (Criteria) this;
		}

		public Criteria andIsNotifyNotBetween(Byte value1, Byte value2) {
			addCriterion("is_notify not between", value1, value2, "isNotify");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserRealNameIsNull() {
			addCriterion("user_real_name is null");
			return (Criteria) this;
		}

		public Criteria andUserRealNameIsNotNull() {
			addCriterion("user_real_name is not null");
			return (Criteria) this;
		}

		public Criteria andUserRealNameEqualTo(String value) {
			addCriterion("user_real_name =", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameNotEqualTo(String value) {
			addCriterion("user_real_name <>", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameGreaterThan(String value) {
			addCriterion("user_real_name >", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("user_real_name >=", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameLessThan(String value) {
			addCriterion("user_real_name <", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameLessThanOrEqualTo(String value) {
			addCriterion("user_real_name <=", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameLike(String value) {
			addCriterion("user_real_name like", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameNotLike(String value) {
			addCriterion("user_real_name not like", value, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameIn(List<String> values) {
			addCriterion("user_real_name in", values, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameNotIn(List<String> values) {
			addCriterion("user_real_name not in", values, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameBetween(String value1, String value2) {
			addCriterion("user_real_name between", value1, value2, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserRealNameNotBetween(String value1, String value2) {
			addCriterion("user_real_name not between", value1, value2, "userRealName");
			return (Criteria) this;
		}

		public Criteria andUserPhoneIsNull() {
			addCriterion("user_phone is null");
			return (Criteria) this;
		}

		public Criteria andUserPhoneIsNotNull() {
			addCriterion("user_phone is not null");
			return (Criteria) this;
		}

		public Criteria andUserPhoneEqualTo(String value) {
			addCriterion("user_phone =", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneNotEqualTo(String value) {
			addCriterion("user_phone <>", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneGreaterThan(String value) {
			addCriterion("user_phone >", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneGreaterThanOrEqualTo(String value) {
			addCriterion("user_phone >=", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneLessThan(String value) {
			addCriterion("user_phone <", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneLessThanOrEqualTo(String value) {
			addCriterion("user_phone <=", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneLike(String value) {
			addCriterion("user_phone like", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneNotLike(String value) {
			addCriterion("user_phone not like", value, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneIn(List<String> values) {
			addCriterion("user_phone in", values, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneNotIn(List<String> values) {
			addCriterion("user_phone not in", values, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneBetween(String value1, String value2) {
			addCriterion("user_phone between", value1, value2, "userPhone");
			return (Criteria) this;
		}

		public Criteria andUserPhoneNotBetween(String value1, String value2) {
			addCriterion("user_phone not between", value1, value2, "userPhone");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}