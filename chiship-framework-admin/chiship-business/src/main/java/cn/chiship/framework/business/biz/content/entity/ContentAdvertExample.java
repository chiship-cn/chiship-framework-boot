package cn.chiship.framework.business.biz.content.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-31
 */
public class ContentAdvertExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ContentAdvertExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNull() {
			addCriterion("modify_by is null");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNotNull() {
			addCriterion("modify_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifyByEqualTo(String value) {
			addCriterion("modify_by =", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotEqualTo(String value) {
			addCriterion("modify_by <>", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThan(String value) {
			addCriterion("modify_by >", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThanOrEqualTo(String value) {
			addCriterion("modify_by >=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThan(String value) {
			addCriterion("modify_by <", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThanOrEqualTo(String value) {
			addCriterion("modify_by <=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLike(String value) {
			addCriterion("modify_by like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotLike(String value) {
			addCriterion("modify_by not like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIn(List<String> values) {
			addCriterion("modify_by in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotIn(List<String> values) {
			addCriterion("modify_by not in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByBetween(String value1, String value2) {
			addCriterion("modify_by between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotBetween(String value1, String value2) {
			addCriterion("modify_by not between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andSlotIdIsNull() {
			addCriterion("slot_id is null");
			return (Criteria) this;
		}

		public Criteria andSlotIdIsNotNull() {
			addCriterion("slot_id is not null");
			return (Criteria) this;
		}

		public Criteria andSlotIdEqualTo(String value) {
			addCriterion("slot_id =", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdNotEqualTo(String value) {
			addCriterion("slot_id <>", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdGreaterThan(String value) {
			addCriterion("slot_id >", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdGreaterThanOrEqualTo(String value) {
			addCriterion("slot_id >=", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdLessThan(String value) {
			addCriterion("slot_id <", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdLessThanOrEqualTo(String value) {
			addCriterion("slot_id <=", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdLike(String value) {
			addCriterion("slot_id like", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdNotLike(String value) {
			addCriterion("slot_id not like", value, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdIn(List<String> values) {
			addCriterion("slot_id in", values, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdNotIn(List<String> values) {
			addCriterion("slot_id not in", values, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdBetween(String value1, String value2) {
			addCriterion("slot_id between", value1, value2, "slotId");
			return (Criteria) this;
		}

		public Criteria andSlotIdNotBetween(String value1, String value2) {
			addCriterion("slot_id not between", value1, value2, "slotId");
			return (Criteria) this;
		}

		public Criteria andImageUrlIsNull() {
			addCriterion("image_url is null");
			return (Criteria) this;
		}

		public Criteria andImageUrlIsNotNull() {
			addCriterion("image_url is not null");
			return (Criteria) this;
		}

		public Criteria andImageUrlEqualTo(String value) {
			addCriterion("image_url =", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlNotEqualTo(String value) {
			addCriterion("image_url <>", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlGreaterThan(String value) {
			addCriterion("image_url >", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlGreaterThanOrEqualTo(String value) {
			addCriterion("image_url >=", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlLessThan(String value) {
			addCriterion("image_url <", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlLessThanOrEqualTo(String value) {
			addCriterion("image_url <=", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlLike(String value) {
			addCriterion("image_url like", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlNotLike(String value) {
			addCriterion("image_url not like", value, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlIn(List<String> values) {
			addCriterion("image_url in", values, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlNotIn(List<String> values) {
			addCriterion("image_url not in", values, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlBetween(String value1, String value2) {
			addCriterion("image_url between", value1, value2, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andImageUrlNotBetween(String value1, String value2) {
			addCriterion("image_url not between", value1, value2, "imageUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlIsNull() {
			addCriterion("link_url is null");
			return (Criteria) this;
		}

		public Criteria andLinkUrlIsNotNull() {
			addCriterion("link_url is not null");
			return (Criteria) this;
		}

		public Criteria andLinkUrlEqualTo(String value) {
			addCriterion("link_url =", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlNotEqualTo(String value) {
			addCriterion("link_url <>", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlGreaterThan(String value) {
			addCriterion("link_url >", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlGreaterThanOrEqualTo(String value) {
			addCriterion("link_url >=", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlLessThan(String value) {
			addCriterion("link_url <", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlLessThanOrEqualTo(String value) {
			addCriterion("link_url <=", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlLike(String value) {
			addCriterion("link_url like", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlNotLike(String value) {
			addCriterion("link_url not like", value, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlIn(List<String> values) {
			addCriterion("link_url in", values, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlNotIn(List<String> values) {
			addCriterion("link_url not in", values, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlBetween(String value1, String value2) {
			addCriterion("link_url between", value1, value2, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andLinkUrlNotBetween(String value1, String value2) {
			addCriterion("link_url not between", value1, value2, "linkUrl");
			return (Criteria) this;
		}

		public Criteria andTextIsNull() {
			addCriterion("text is null");
			return (Criteria) this;
		}

		public Criteria andTextIsNotNull() {
			addCriterion("text is not null");
			return (Criteria) this;
		}

		public Criteria andTextEqualTo(String value) {
			addCriterion("text =", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotEqualTo(String value) {
			addCriterion("text <>", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextGreaterThan(String value) {
			addCriterion("text >", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextGreaterThanOrEqualTo(String value) {
			addCriterion("text >=", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLessThan(String value) {
			addCriterion("text <", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLessThanOrEqualTo(String value) {
			addCriterion("text <=", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextLike(String value) {
			addCriterion("text like", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotLike(String value) {
			addCriterion("text not like", value, "text");
			return (Criteria) this;
		}

		public Criteria andTextIn(List<String> values) {
			addCriterion("text in", values, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotIn(List<String> values) {
			addCriterion("text not in", values, "text");
			return (Criteria) this;
		}

		public Criteria andTextBetween(String value1, String value2) {
			addCriterion("text between", value1, value2, "text");
			return (Criteria) this;
		}

		public Criteria andTextNotBetween(String value1, String value2) {
			addCriterion("text not between", value1, value2, "text");
			return (Criteria) this;
		}

		public Criteria andBeginDateIsNull() {
			addCriterion("begin_date is null");
			return (Criteria) this;
		}

		public Criteria andBeginDateIsNotNull() {
			addCriterion("begin_date is not null");
			return (Criteria) this;
		}

		public Criteria andBeginDateEqualTo(Long value) {
			addCriterion("begin_date =", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateNotEqualTo(Long value) {
			addCriterion("begin_date <>", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateGreaterThan(Long value) {
			addCriterion("begin_date >", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateGreaterThanOrEqualTo(Long value) {
			addCriterion("begin_date >=", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateLessThan(Long value) {
			addCriterion("begin_date <", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateLessThanOrEqualTo(Long value) {
			addCriterion("begin_date <=", value, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateIn(List<Long> values) {
			addCriterion("begin_date in", values, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateNotIn(List<Long> values) {
			addCriterion("begin_date not in", values, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateBetween(Long value1, Long value2) {
			addCriterion("begin_date between", value1, value2, "beginDate");
			return (Criteria) this;
		}

		public Criteria andBeginDateNotBetween(Long value1, Long value2) {
			addCriterion("begin_date not between", value1, value2, "beginDate");
			return (Criteria) this;
		}

		public Criteria andEndDateIsNull() {
			addCriterion("end_date is null");
			return (Criteria) this;
		}

		public Criteria andEndDateIsNotNull() {
			addCriterion("end_date is not null");
			return (Criteria) this;
		}

		public Criteria andEndDateEqualTo(Long value) {
			addCriterion("end_date =", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateNotEqualTo(Long value) {
			addCriterion("end_date <>", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateGreaterThan(Long value) {
			addCriterion("end_date >", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateGreaterThanOrEqualTo(Long value) {
			addCriterion("end_date >=", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateLessThan(Long value) {
			addCriterion("end_date <", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateLessThanOrEqualTo(Long value) {
			addCriterion("end_date <=", value, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateIn(List<Long> values) {
			addCriterion("end_date in", values, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateNotIn(List<Long> values) {
			addCriterion("end_date not in", values, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateBetween(Long value1, Long value2) {
			addCriterion("end_date between", value1, value2, "endDate");
			return (Criteria) this;
		}

		public Criteria andEndDateNotBetween(Long value1, Long value2) {
			addCriterion("end_date not between", value1, value2, "endDate");
			return (Criteria) this;
		}

		public Criteria andSeqIsNull() {
			addCriterion("seq is null");
			return (Criteria) this;
		}

		public Criteria andSeqIsNotNull() {
			addCriterion("seq is not null");
			return (Criteria) this;
		}

		public Criteria andSeqEqualTo(Integer value) {
			addCriterion("seq =", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqNotEqualTo(Integer value) {
			addCriterion("seq <>", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqGreaterThan(Integer value) {
			addCriterion("seq >", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqGreaterThanOrEqualTo(Integer value) {
			addCriterion("seq >=", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqLessThan(Integer value) {
			addCriterion("seq <", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqLessThanOrEqualTo(Integer value) {
			addCriterion("seq <=", value, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqIn(List<Integer> values) {
			addCriterion("seq in", values, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqNotIn(List<Integer> values) {
			addCriterion("seq not in", values, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqBetween(Integer value1, Integer value2) {
			addCriterion("seq between", value1, value2, "seq");
			return (Criteria) this;
		}

		public Criteria andSeqNotBetween(Integer value1, Integer value2) {
			addCriterion("seq not between", value1, value2, "seq");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}