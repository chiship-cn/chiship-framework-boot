package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecord;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecordExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessOrderPaymentRecordDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;

/**
 * 订单支付业务接口层 2024/5/31
 *
 * @author lijian
 */
public interface BusinessOrderPaymentRecordService
		extends BaseService<BusinessOrderPaymentRecord, BusinessOrderPaymentRecordExample> {

	/**
	 * 支付通知结果
	 * @param orderPaymentRecordDto
	 * @return
	 */
	BaseResult payNotify(BusinessOrderPaymentRecordDto orderPaymentRecordDto);

	/**
	 * 根据订单号查询支付流水
	 * @param orderNo
	 * @return
	 */
	BaseResult getByOrderNo(String orderNo);

}
