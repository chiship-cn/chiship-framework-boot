package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * 会员表单 2022/2/23
 *
 * @author LiJian
 */
@ApiModel(value = "会员修改表单")
public class MemberUserModifyDto {

	@ApiModelProperty(value = "头像", required = true)
	@NotEmpty(message = "头像" + BaseTipConstants.NOT_EMPTY)
	private String avatar;

	@ApiModelProperty(value = "昵称", required = true)
	@NotEmpty(message = "昵称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 10)
	private String nickName;

	@ApiModelProperty(value = "真实姓名", required = true)
	@NotEmpty(message = "真实姓名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 2, max = 5)
	private String realName;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "性别(0：保密  1：男   2：女)")
	private Byte gender;

	@ApiModelProperty(value = "生日")
	private Long birthday;

	@ApiModelProperty(value = "个性签名")
	private String signature;

	@ApiModelProperty(value = "简介")
	private String introduction;

	@ApiModelProperty(value = "居住地省")
	private Long regionLevel1;

	@ApiModelProperty(value = "居住地市")
	private Long regionLevel2;

	@ApiModelProperty(value = "居住地县/区")
	private Long regionLevel3;

	@ApiModelProperty(value = "居住地街道")
	private Long regionLevel4;

	@ApiModelProperty(value = "地址")
	private String address;

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Byte getGender() {
		return gender;
	}

	public void setGender(Byte gender) {
		this.gender = gender;
	}

	public Long getBirthday() {
		return birthday;
	}

	public void setBirthday(Long birthday) {
		this.birthday = birthday;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public Long getRegionLevel1() {
		return regionLevel1;
	}

	public void setRegionLevel1(Long regionLevel1) {
		this.regionLevel1 = regionLevel1;
	}

	public Long getRegionLevel2() {
		return regionLevel2;
	}

	public void setRegionLevel2(Long regionLevel2) {
		this.regionLevel2 = regionLevel2;
	}

	public Long getRegionLevel3() {
		return regionLevel3;
	}

	public void setRegionLevel3(Long regionLevel3) {
		this.regionLevel3 = regionLevel3;
	}

	public Long getRegionLevel4() {
		return regionLevel4;
	}

	public void setRegionLevel4(Long regionLevel4) {
		this.regionLevel4 = regionLevel4;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}