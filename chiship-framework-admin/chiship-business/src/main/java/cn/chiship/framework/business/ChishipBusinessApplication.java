package cn.chiship.framework.business;

import cn.chiship.sdk.framework.util.SpringContextUtil;
import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author lijian
 */
@MapperScan(basePackages = {"cn.chiship.framework.business.biz.**.mapper", "cn.chiship.framework.upms.biz.**.mapper",
        "cn.chiship.framework.docs.biz.**.mapper", "cn.chiship.framework.third.biz.**.mapper"})
@ComponentScan(basePackages = {"cn.chiship"})
@EnableAsync
@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
public class ChishipBusinessApplication {

    public static void main(String[] args) {
        try {
            SpringApplication.run(ChishipBusinessApplication.class, args);
            SpringContextUtil.printRunningInfo();
            System.out.println("###################### ChishipBusinessApplication Finish ######################");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
