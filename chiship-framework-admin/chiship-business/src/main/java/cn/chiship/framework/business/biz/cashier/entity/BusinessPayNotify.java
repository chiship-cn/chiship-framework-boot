package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-23
 */
public class BusinessPayNotify implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 通知时间
	 */
	private Long notifyDate;

	/**
	 * 通知类型：pay、refund、transfer
	 */
	private String notifyType;

	/**
	 * 渠道
	 */
	private String channelPayWay;

	/**
	 * 通知内容
	 */
	private String notifyContent;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Long getNotifyDate() {
		return notifyDate;
	}

	public void setNotifyDate(Long notifyDate) {
		this.notifyDate = notifyDate;
	}

	public String getNotifyType() {
		return notifyType;
	}

	public void setNotifyType(String notifyType) {
		this.notifyType = notifyType;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

	public String getNotifyContent() {
		return notifyContent;
	}

	public void setNotifyContent(String notifyContent) {
		this.notifyContent = notifyContent;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", notifyDate=").append(notifyDate);
		sb.append(", notifyType=").append(notifyType);
		sb.append(", channelPayWay=").append(channelPayWay);
		sb.append(", notifyContent=").append(notifyContent);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		BusinessPayNotify other = (BusinessPayNotify) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getNotifyDate() == null ? other.getNotifyDate() == null
						: this.getNotifyDate().equals(other.getNotifyDate()))
				&& (this.getNotifyType() == null ? other.getNotifyType() == null
						: this.getNotifyType().equals(other.getNotifyType()))
				&& (this.getChannelPayWay() == null ? other.getChannelPayWay() == null
						: this.getChannelPayWay().equals(other.getChannelPayWay()))
				&& (this.getNotifyContent() == null ? other.getNotifyContent() == null
						: this.getNotifyContent().equals(other.getNotifyContent()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getNotifyDate() == null) ? 0 : getNotifyDate().hashCode());
		result = prime * result + ((getNotifyType() == null) ? 0 : getNotifyType().hashCode());
		result = prime * result + ((getChannelPayWay() == null) ? 0 : getChannelPayWay().hashCode());
		result = prime * result + ((getNotifyContent() == null) ? 0 : getNotifyContent().hashCode());
		return result;
	}

}