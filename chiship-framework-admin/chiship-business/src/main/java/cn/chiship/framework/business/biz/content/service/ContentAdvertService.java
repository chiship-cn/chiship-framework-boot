package cn.chiship.framework.business.biz.content.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.content.entity.ContentAdvert;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertExample;
import com.alibaba.fastjson.JSONObject;

/**
 * 广告版位业务接口层 2024/10/31
 *
 * @author lijian
 */
public interface ContentAdvertService extends BaseService<ContentAdvert, ContentAdvertExample> {

	/**
	 * 从缓存获取广告数据
	 * @param code 编码
	 * @return JSONObject
	 */
	JSONObject getCacheAdvertByCode(String code);

	/**
	 * 缓存广告
	 */
	void cacheAdvertData();

}
