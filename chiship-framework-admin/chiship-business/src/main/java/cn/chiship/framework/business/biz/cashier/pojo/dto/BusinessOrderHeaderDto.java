package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;


/**
 * 订单主表表单 2024/7/23
 *
 * @author LiJian
 */
@ApiModel(value = "订单主表表单")
public class BusinessOrderHeaderDto extends BusinessOrderHeader {

}