package cn.chiship.framework.business.biz.member.service.impl;

import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.member.mapper.MemberUserBindMapper;
import cn.chiship.framework.business.biz.member.entity.MemberUserBind;
import cn.chiship.framework.business.biz.member.entity.MemberUserBindExample;
import cn.chiship.framework.business.biz.member.service.MemberUserBindService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 会员绑定业务接口实现层 2022/2/23
 *
 * @author lijian
 */
@Service
public class MemberUserBindServiceImpl extends BaseServiceImpl<MemberUserBind, MemberUserBindExample>
		implements MemberUserBindService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserBindServiceImpl.class);

	@Resource
	MemberUserBindMapper memberUserBindMapper;

}
