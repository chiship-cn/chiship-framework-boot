package cn.chiship.framework.business.biz.member.service.impl;

import cn.chiship.framework.business.biz.member.entity.*;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.member.mapper.MemberUserWalletChangeMapper;
import cn.chiship.framework.business.biz.member.service.MemberUserWalletChangeService;
import cn.chiship.sdk.framework.pojo.dto.export.ExportDto;
import cn.chiship.sdk.framework.pojo.dto.export.ExportTransferDataDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 会员钱包变化记录业务接口实现层
 * 2024/12/17
 *
 * @author lijian
 */
@Service
public class MemberUserWalletChangeServiceImpl extends BaseServiceImpl<MemberUserWalletChange, MemberUserWalletChangeExample> implements MemberUserWalletChangeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserWalletChangeServiceImpl.class);

    @Resource
    MemberUserWalletChangeMapper memberUserWalletChangeMapper;

    public static void main(String[] args) {
        Map<String, Object> paramMap = new HashMap<String, Object>();
        String type = null;
        if (!StringUtil.isNull(paramMap.get("type"))) {
            type = paramMap.get("type").toString();
        }
        System.out.println("---->" + type);
    }

    @Override
    public ExportTransferDataDto assemblyExportData(ExportDto exportDto) {
        Map<String, Object> paramMap = exportDto.getParamMap();
        if (!paramMap.containsKey("memberId")) {
            throw new BusinessException("缺少memberId参数");
        }
        String memberId = paramMap.get("memberId").toString();
        String type = null;
        String timeStart = null;
        String timeEnd = null;
        if (!StringUtil.isNull(paramMap.get("type"))) {
            type = paramMap.get("type").toString();
        }
        if (!StringUtil.isNull(paramMap.get("timeStart"))) {
            timeStart = paramMap.get("timeStart").toString();
        }
        if (!StringUtil.isNull(paramMap.get("timeEnd"))) {
            timeEnd = paramMap.get("timeEnd").toString();
        }

        MemberUserWalletChangeExample memberUserWalletChangeExample = new MemberUserWalletChangeExample();
        //创造条件
        MemberUserWalletChangeExample.Criteria memberUserWalletChangeCriteria = memberUserWalletChangeExample.createCriteria();
        memberUserWalletChangeCriteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(memberId);

        if (!StringUtil.isNullOrEmpty(type)) {
            memberUserWalletChangeCriteria.andTypeEqualTo(Byte.valueOf(type));
        }
        if (!StringUtil.isNullOrEmpty(timeStart)) {
            memberUserWalletChangeCriteria.andGmtCreatedGreaterThanOrEqualTo(Long.valueOf(timeStart));
        }
        if (!StringUtil.isNullOrEmpty(timeEnd)) {
            memberUserWalletChangeCriteria.andGmtCreatedLessThanOrEqualTo(Long.valueOf(timeEnd));
        }
        memberUserWalletChangeExample.setOrderByClause("gmt_created desc");
        List<MemberUserWalletChange> memberUserWalletChanges = memberUserWalletChangeMapper.selectByExample(memberUserWalletChangeExample);

        String fileName = "钱包记录清单";
        String sheetName = "钱包记录清单";
        String sheetTitle = "钱包记录清单";
        List<String> labels = new ArrayList<>();
        List<List<String>> valueList = new ArrayList<>();
        labels.add("时间");
        labels.add("金额");
        labels.add("类型");
        labels.add("说明");
        for (MemberUserWalletChange memberUserWalletChange : memberUserWalletChanges) {
            List<String> values = new ArrayList<>();
            values.add(DateUtils.dateTime(memberUserWalletChange.getGmtCreated()));
            values.add(memberUserWalletChange.getChangeNumber().toString());
            values.add(Byte.valueOf("0").equals(memberUserWalletChange.getType()) ? "消费" : "收入");
            values.add(memberUserWalletChange.getRemark());
            valueList.add(values);
        }

        return new ExportTransferDataDto(fileName, sheetName, sheetTitle, labels, valueList, valueList.size());
    }

}
