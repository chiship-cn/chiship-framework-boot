package cn.chiship.framework.business.biz.base.controller;

import cn.chiship.framework.business.biz.business.service.TestExampleService;
import cn.chiship.framework.business.core.common.BusinessCommonConstants;
import cn.chiship.framework.common.annotation.LoginUser;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.pojo.dto.export.ImportDto;
import cn.chiship.sdk.framework.util.ServletUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Map;

/**
 * controller 2020/8/5
 *
 * @author lijian
 */
@RestController
@RequestMapping("/import")
@Api(tags = "数据导入控制器")
public class ImportController {

	@Resource
	private TestExampleService testExampleService;

	@SystemOptionAnnotation(describe = "根据类导入数据")
	@ApiOperation(value = "根据类导入数据")
	@ApiImplicitParams({ @ApiImplicitParam(name = "className", value = "类型", required = true,
			dataTypeClass = String.class, paramType = "path") })
	@PostMapping(value = "/data/{className}")
	@NoParamsSign
	public ResponseEntity<BaseResult> importData(@PathVariable("className") String className,
			@RequestParam("file") MultipartFile multipartFile, @ApiIgnore @LoginUser CacheUserVO cacheUserVO)
			throws IOException {
		if (multipartFile.isEmpty()) {
			return new ResponseEntity<>(BaseResult.error("上传失败，请选择文件"), HttpStatus.OK);
		}
		Map<String, Object> paramMap = ServletUtil.getParameterValues();
		Boolean fileSave = Boolean.valueOf(paramMap.get("fileSave").toString());
		paramMap.remove("fileSave");
		ImportDto importDto = new ImportDto(multipartFile.getOriginalFilename(), fileSave,
				multipartFile.getContentType(), cacheUserVO, paramMap);
		String taskId = DateUtils.dateTimeNow() + SnowflakeIdUtil.generateId();
		switch (className) {
		case BusinessCommonConstants.EXPORT_IMPORT_CLASS_TEST_EXAMPLE:
			testExampleService.asyncImportData(taskId, multipartFile.getInputStream(), importDto);
			break;
		default:
			return new ResponseEntity<>(BaseResult.error("不支持的数据导入此表:" + className), HttpStatus.OK);
		}
		return new ResponseEntity<>(BaseResult.ok(taskId), HttpStatus.OK);

	}

	@ApiOperation(value = "根据任务ID及类获取异步任务实时进度")
	@PostMapping("/getProcessStatus/{className}")
	@NoParamsSign
	public ResponseEntity getProcessStatus(@PathVariable("className") String className, @RequestBody String taskId) {
		if (StringUtil.isNullOrEmpty(taskId)) {
			BaseResult baseResult = BaseResult.error(BaseResultEnum.FAILED, "缺少参数[taskId]");
			return new ResponseEntity<>(baseResult, HttpStatus.OK);
		}
		BaseResult baseResult;
		switch (className) {
		/**
		 * 测试示例
		 */
		case BusinessCommonConstants.EXPORT_IMPORT_CLASS_TEST_EXAMPLE:
			baseResult = testExampleService.getImportProcessStatus(taskId);
			break;
		default:
			baseResult = BaseResult.error("不支持的查看此表[" + className + "]进度");
		}
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

}
