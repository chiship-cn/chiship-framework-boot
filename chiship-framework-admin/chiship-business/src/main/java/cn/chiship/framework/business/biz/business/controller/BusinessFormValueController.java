package cn.chiship.framework.business.biz.business.controller;


import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormValueTouristDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.util.http.RequestUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.business.service.BusinessFormValueService;
import cn.chiship.framework.business.biz.business.entity.BusinessFormValue;
import cn.chiship.framework.business.biz.business.entity.BusinessFormValueExample;
import cn.chiship.framework.business.biz.business.pojo.dto.BusinessFormValueDto;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.sdk.framework.util.ServletUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 自定义表单提交数据控制层
 * 2024/12/13
 *
 * @author lijian
 */
@RestController
@RequestMapping("/businessFormValue")
@Api(tags = "自定义表单提交数据")
public class BusinessFormValueController extends BaseController<BusinessFormValue, BusinessFormValueExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessFormValueController.class);

    @Resource
    private BusinessFormValueService businessFormValueService;

    @Override
    public BaseService getService() {
        return businessFormValueService;
    }

    @ApiOperation(value = "自定义表单提交数据分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "formId", value = "所属表单", required = true, dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    @Authorization
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "realName") String realName,
                                           @RequestParam(required = false, defaultValue = "", value = "mobile") String mobile,
                                           @RequestParam(value = "formId") String formId) {
        BusinessFormValueExample businessFormValueExample = new BusinessFormValueExample();
        //创造条件
        BusinessFormValueExample.Criteria businessFormValueCriteria = businessFormValueExample.createCriteria();
        businessFormValueCriteria.andIsDeletedEqualTo(BaseConstants.NO).andFormIdEqualTo(formId);
        if (!StringUtil.isNullOrEmpty(realName)) {
            businessFormValueCriteria.andCreatedUserRealNameLike(realName + "%");
        }
        if (!StringUtil.isNullOrEmpty(mobile)) {
            businessFormValueCriteria.andCreatedUserMobileLike(mobile + "%");
        }

        return super.responseEntity(BaseResult.ok(super.page(businessFormValueExample)));
    }

    @ApiOperation(value = "自定义表单提交数据分页（含表单设计数据）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "formId", value = "所属表单", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "formName", value = "所属表单名称", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "mobile", value = "手机号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "所属表单分类", dataTypeClass = String.class, paramType = "query"),
    })
    @Authorization
    @GetMapping(value = "/pageV1")
    public ResponseEntity<BaseResult> pageV1(HttpServletRequest request) {
        PageVo pageVo = ServletUtil.getPageVo();
        String sort = pageVo.getSort();
        Map<String, String> paramsMap = RequestUtil.getParameterMap(request);
        paramsMap.put("sort", sort);
        if (paramsMap.containsKey("categoryId") && "-".equals(paramsMap.get("categoryId"))) {
            paramsMap.put("categoryId", null);
        }
        return super.responseEntity(businessFormValueService.page(pageVo, paramsMap));
    }

    @ApiOperation(value = "自定义表单提交数据分页(会员专用)")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "formId", value = "所属表单", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "formName", value = "所属表单名称", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "所属表单分类", dataTypeClass = String.class, paramType = "query"),
    })
    @Authorization
    @GetMapping(value = "/pageV2")
    public ResponseEntity<BaseResult> pageV2(HttpServletRequest request) {
        PageVo pageVo = ServletUtil.getPageVo();
        String sort = pageVo.getSort();
        Map<String, String> paramsMap = RequestUtil.getParameterMap(request);
        paramsMap.put("sort", sort);
        paramsMap.put("userId", getUserId());
        if (paramsMap.containsKey("categoryId") && "-".equals(paramsMap.get("categoryId"))) {
            paramsMap.put("categoryId", null);
        }
        return super.responseEntity(businessFormValueService.page(pageVo, paramsMap));
    }

    @SystemOptionAnnotation(describe = "自定义表单提交数据列表数据")
    @ApiOperation(value = "自定义表单提交数据列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @Authorization
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        BusinessFormValueExample businessFormValueExample = new BusinessFormValueExample();
        //创造条件
        BusinessFormValueExample.Criteria businessFormValueCriteria = businessFormValueExample.createCriteria();
        businessFormValueCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        businessFormValueExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(businessFormValueExample)));
    }
    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存自定义表单提交数据")
    @ApiOperation(value = "游客保存自定义表单提交数据")
    @PostMapping(value = "touristSave")
    public ResponseEntity<BaseResult> touristSave(@RequestBody @Valid BusinessFormValueTouristDto businessFormValueTouristDto) {
        BusinessFormValue businessFormValue = new BusinessFormValue();
        BeanUtils.copyProperties(businessFormValueTouristDto, businessFormValue);
        businessFormValue.setCreatedUserRealName(businessFormValueTouristDto.getFillRealName());
        businessFormValue.setCreatedUserMobile(businessFormValueTouristDto.getFillMobile());
        return super.responseEntity(businessFormValueService.insertSelective(businessFormValue));
    }
    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存自定义表单提交数据")
    @ApiOperation(value = "保存自定义表单提交数据")
    @Authorization
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid BusinessFormValueDto businessFormValueDto) {
        BusinessFormValue businessFormValue = new BusinessFormValue();
        BeanUtils.copyProperties(businessFormValueDto, businessFormValue);
        businessFormValue.setCreatedUserRealName(getRealName());
        businessFormValue.setCreatedUserMobile(getLoginUser().getMobile());
        return super.responseEntity(super.save(businessFormValue));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新自定义表单提交数据")
    @ApiOperation(value = "更新自定义表单提交数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @Authorization
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid BusinessFormValueDto businessFormValueDto) {
        BusinessFormValue businessFormValue = new BusinessFormValue();
        BeanUtils.copyProperties(businessFormValueDto, businessFormValue);
        return super.responseEntity(super.update(id, businessFormValue));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除自定义表单提交数据")
    @ApiOperation(value = "删除自定义表单提交数据")
    @Authorization
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        BusinessFormValueExample businessFormValueExample = new BusinessFormValueExample();
        businessFormValueExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(businessFormValueExample));
    }
}