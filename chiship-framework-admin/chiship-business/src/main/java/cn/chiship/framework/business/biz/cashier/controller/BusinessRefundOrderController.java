package cn.chiship.framework.business.biz.cashier.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.cashier.service.BusinessRefundOrderService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrder;
import cn.chiship.framework.business.biz.cashier.entity.BusinessRefundOrderExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessRefundOrderDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 退款订单控制层 2024/7/23
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/businessRefundOrder")
@Api(tags = "退款订单")
public class BusinessRefundOrderController extends BaseController<BusinessRefundOrder, BusinessRefundOrderExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessRefundOrderController.class);

    @Resource
    private BusinessRefundOrderService businessRefundOrderService;

    @Override
    public BaseService getService() {
        return businessRefundOrderService;
    }

    @SystemOptionAnnotation(describe = "退款订单分页")
    @ApiOperation(value = "退款订单分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
                    dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundStatus", value = "退款状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "refundNo", value = "退款单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundTradeNo", value = "退款交易号", dataTypeClass = String.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "orderNo", value = "原始订单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "tradeNo", value = "原始交易号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundFinishTimeBegin", value = "退款开始时间", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "refundFinishTimeEnd", value = "退款结束时间", dataTypeClass = Long.class,
                    paramType = "query"),})
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(
            @RequestParam(required = false, defaultValue = "", value = "refundTradeNo") String refundTradeNo,
            @RequestParam(required = false, defaultValue = "", value = "refundNo") String refundNo,
            @RequestParam(required = false, defaultValue = "", value = "orderNo") String orderNo,
            @RequestParam(required = false, defaultValue = "", value = "tradeNo") String tradeNo,
            @RequestParam(required = false, defaultValue = "", value = "refundFinishTimeBegin") Long refundFinishTimeBegin,
            @RequestParam(required = false, defaultValue = "", value = "refundFinishTimeEnd") Long refundFinishTimeEnd,
            @RequestParam(required = false, defaultValue = "", value = "refundStatus") Byte refundStatus) {
        BusinessRefundOrderExample businessRefundOrderExample = new BusinessRefundOrderExample();
        // 创造条件
        BusinessRefundOrderExample.Criteria businessRefundOrderCriteria = businessRefundOrderExample.createCriteria();
        businessRefundOrderCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(refundTradeNo)) {
            businessRefundOrderCriteria.andRefundTradeNoLike(refundTradeNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(refundNo)) {
            businessRefundOrderCriteria.andIdLike(refundNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(orderNo)) {
            businessRefundOrderCriteria.andIdLike(orderNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(tradeNo)) {
            businessRefundOrderCriteria.andTradeNoEqualTo(tradeNo + "%");
        }
        if (!StringUtil.isNull(refundFinishTimeBegin)) {
            businessRefundOrderCriteria.andRefundFinishTimeGreaterThanOrEqualTo(refundFinishTimeBegin);
        }
        if (!StringUtil.isNull(refundFinishTimeEnd)) {
            businessRefundOrderCriteria.andRefundFinishTimeLessThanOrEqualTo(refundFinishTimeEnd);
        }
        if (!StringUtil.isNull(refundStatus)) {
            businessRefundOrderCriteria.andRefundStatusEqualTo(refundStatus);
        }
        return super.responseEntity(BaseResult.ok(super.page(businessRefundOrderExample)));
    }

    @SystemOptionAnnotation(describe = "退款订单分页")
    @ApiOperation(value = "退款订单分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
                    dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundStatus", value = "退款状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "refundNo", value = "退款单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundTradeNo", value = "退款交易号", dataTypeClass = String.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "orderNo", value = "原始订单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "tradeNo", value = "原始交易号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "refundFinishTimeBegin", value = "退款开始时间", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "refundFinishTimeEnd", value = "退款结束时间", dataTypeClass = Long.class,
                    paramType = "query"),})
    @GetMapping(value = "/current/page")
    public ResponseEntity<BaseResult> currentPage(
            @RequestParam(required = false, defaultValue = "", value = "refundTradeNo") String refundTradeNo,
            @RequestParam(required = false, defaultValue = "", value = "refundNo") String refundNo,
            @RequestParam(required = false, defaultValue = "", value = "orderNo") String orderNo,
            @RequestParam(required = false, defaultValue = "", value = "tradeNo") String tradeNo,
            @RequestParam(required = false, defaultValue = "", value = "refundFinishTimeBegin") Long refundFinishTimeBegin,
            @RequestParam(required = false, defaultValue = "", value = "refundFinishTimeEnd") Long refundFinishTimeEnd,
            @RequestParam(required = false, defaultValue = "", value = "refundStatus") Byte refundStatus) {
        BusinessRefundOrderExample businessRefundOrderExample = new BusinessRefundOrderExample();
        // 创造条件
        BusinessRefundOrderExample.Criteria businessRefundOrderCriteria = businessRefundOrderExample.createCriteria();
        businessRefundOrderCriteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(getUserId());
        if (!StringUtil.isNullOrEmpty(refundTradeNo)) {
            businessRefundOrderCriteria.andRefundTradeNoLike(refundTradeNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(refundNo)) {
            businessRefundOrderCriteria.andIdLike(refundNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(orderNo)) {
            businessRefundOrderCriteria.andIdLike(orderNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(tradeNo)) {
            businessRefundOrderCriteria.andTradeNoEqualTo(tradeNo + "%");
        }
        if (!StringUtil.isNull(refundFinishTimeBegin)) {
            businessRefundOrderCriteria.andRefundFinishTimeGreaterThanOrEqualTo(refundFinishTimeBegin);
        }
        if (!StringUtil.isNull(refundFinishTimeEnd)) {
            businessRefundOrderCriteria.andRefundFinishTimeLessThanOrEqualTo(refundFinishTimeEnd);
        }
        if (!StringUtil.isNull(refundStatus)) {
            businessRefundOrderCriteria.andRefundStatusEqualTo(refundStatus);
        }
        return super.responseEntity(BaseResult.ok(super.page(businessRefundOrderExample)));
    }

    @SystemOptionAnnotation(describe = "退款订单列表数据")
    @ApiOperation(value = "退款订单列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),})
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        BusinessRefundOrderExample businessRefundOrderExample = new BusinessRefundOrderExample();
        // 创造条件
        BusinessRefundOrderExample.Criteria businessRefundOrderCriteria = businessRefundOrderExample.createCriteria();
        businessRefundOrderCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        return super.responseEntity(BaseResult.ok(super.list(businessRefundOrderExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存退款订单")
    @ApiOperation(value = "保存退款订单")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid BusinessRefundOrderDto businessRefundOrderDto) {
        BusinessRefundOrder businessRefundOrder = new BusinessRefundOrder();
        BeanUtils.copyProperties(businessRefundOrderDto, businessRefundOrder);
        return super.responseEntity(super.save(businessRefundOrder));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新退款订单")
    @ApiOperation(value = "更新退款订单")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
            required = true)})
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid BusinessRefundOrderDto businessRefundOrderDto) {
        BusinessRefundOrder businessRefundOrder = new BusinessRefundOrder();
        BeanUtils.copyProperties(businessRefundOrderDto, businessRefundOrder);
        return super.responseEntity(super.update(id, businessRefundOrder));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除退款订单")
    @ApiOperation(value = "删除退款订单")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        BusinessRefundOrderExample businessRefundOrderExample = new BusinessRefundOrderExample();
        businessRefundOrderExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(businessRefundOrderExample));
    }

}
