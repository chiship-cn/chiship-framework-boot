package cn.chiship.framework.business.biz.base.controller;

import cn.chiship.framework.business.biz.business.service.BusinessFormValueService;
import cn.chiship.framework.business.biz.business.service.TestExampleService;
import cn.chiship.framework.business.biz.member.service.MemberUserWithdrawalApplicationService;
import cn.chiship.framework.business.core.common.BusinessCommonConstants;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.upms.biz.system.service.UpmsSmsCodeService;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.enums.BaseResultEnum;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.core.util.DateUtils;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.http.ResponseUtil;
import cn.chiship.sdk.framework.pojo.dto.export.ExportDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * controller 2020/8/5
 *
 * @author lijian
 */
@RestController
@RequestMapping("/export")
@Api(tags = "数据导出控制器")
public class ExportController {

    @Resource
    private UpmsSmsCodeService upmsSmsCodeService;

    @Resource
    private TestExampleService testExampleService;
    @Resource
    private BusinessFormValueService businessFormValueService;
    @Resource
    private MemberUserWithdrawalApplicationService memberUserWithdrawalApplicationService;

    @SystemOptionAnnotation(describe = "根据类与参数集合导出数据")
    @ApiOperation(value = "根据类与参数集合导出数据")
    @ApiImplicitParams({@ApiImplicitParam(name = "paramsMap", value = "数据集合", required = true,
            dataTypeClass = Map.class, paramType = "body")})
    @PostMapping(value = "data/{className}")
    @NoParamsSign
    public void exportData(HttpServletResponse response, @PathVariable("className") String className,
                           @RequestBody ExportDto exportDto) throws Exception {
        boolean async = exportDto.getAsync();
        String taskId = DateUtils.dateTimeNow() + SnowflakeIdUtil.generateId();
        switch (className) {
            //导出短信
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_SMS_CODE:
                if (async) {
                    upmsSmsCodeService.asyncExportData(response, taskId, exportDto);
                } else {
                    upmsSmsCodeService.exportData(response, exportDto);
                }
                break;
            //导出测试示例
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_TEST_EXAMPLE:
                if (async) {
                    testExampleService.asyncExportData(response, taskId, exportDto);
                } else {
                    testExampleService.exportData(response, exportDto);
                }
                break;
            //表单数据
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_FORM_VALUE:
                if (async) {
                    businessFormValueService.asyncExportData(response, taskId, exportDto);
                } else {
                    businessFormValueService.exportData(response, exportDto);
                }
                break;
            //提现数据
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_MEMBER_WITHDRAWAL:
                if (async) {
                    memberUserWithdrawalApplicationService.asyncExportData(response, taskId, exportDto);
                } else {
                    memberUserWithdrawalApplicationService.exportData(response, exportDto);
                }
                break;
            default:
                BaseResult baseResult = BaseResult
                        .error("不支持的导出此表:" + className + "，请在" + this.getClass().getSimpleName() + "配置业务!");
                ResponseUtil.writeJson(response, baseResult);

        }
        if (async) {
            ResponseUtil.writeJson(response, BaseResult.ok(taskId));
        }
    }

    @ApiOperation(value = "根据任务ID获取异步任务实时进度")
    @ApiImplicitParams({})
    @PostMapping(value = "getProcessStatus/{className}")
    public void getProcessStatus(HttpServletResponse response, @PathVariable("className") String className,
                                 @RequestBody String taskId) throws Exception {
        if (StringUtil.isNullOrEmpty(taskId)) {
            BaseResult baseResult = BaseResult.error(BaseResultEnum.FAILED, "缺少参数[taskId]");
            ResponseUtil.writeJson(response, baseResult);
        }
        switch (className) {
            //导出短信
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_SMS_CODE:
                upmsSmsCodeService.getExportProcessStatus(response, taskId);
                break;
            //导出测试示例
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_TEST_EXAMPLE:
                testExampleService.getExportProcessStatus(response, taskId);
                break;
            //表单数据
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_FORM_VALUE:
                businessFormValueService.getExportProcessStatus(response, taskId);
                break;
            //提现数据
            case BusinessCommonConstants.EXPORT_IMPORT_CLASS_MEMBER_WITHDRAWAL:
                memberUserWithdrawalApplicationService.getExportProcessStatus(response, taskId);
                break;
            default:
                ResponseUtil.writeJson(response, BaseResult.error(BaseResultEnum.FAILED, "不支持的查看此表[" + className + "]进度"));
        }
    }

}
