package cn.chiship.framework.business.biz.cashier.enums;

/**
 * 退款状态
 *
 * @author lijian
 */
public enum RefundStatusEnum {

	/**
	 * 退款申请中
	 */
	REFUND_APPLICATION(Byte.valueOf("0"), "申请中"),

	/**
	 * 退款成功
	 */
	REFUND_SUCCESS(Byte.valueOf("1"), "退款成功"),

	/**
	 * 退款处理中
	 */
	REFUND_PROCESSING(Byte.valueOf("2"), "退款处理中"),

	/**
	 * 退款关闭
	 */
	REFUND_CLOSED(Byte.valueOf("3"), "退款关闭"),

	/**
	 * 退款异常
	 */
	REFUND_ABNORMAL(Byte.valueOf("4"), "退款异常"),

	/**
	 * 退款失败
	 */
	REFUND_FAIL(Byte.valueOf("5"), "退款失败"),;

	/**
	 * 业务编号
	 */
	private Byte status;

	/**
	 * 业务描述
	 */
	private String message;

	RefundStatusEnum(Byte status, String message) {
		this.status = status;
		this.message = message;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
