package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.framework.business.biz.member.enmus.MemberComeSourceEnum;
import cn.chiship.sdk.framework.pojo.dto.UserRegisterDto;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 会员注册表单 2022/2/23
 *
 * @author LiJian
 */
@ApiModel(value = "会员注册表单")
public class MemberUserRegisterDto extends UserRegisterDto {

	@ApiModelProperty(value = "来源枚举", required = true)
	private MemberComeSourceEnum comeSourceEnum;

	public MemberComeSourceEnum getComeSourceEnum() {
		return comeSourceEnum;
	}

	public void setComeSourceEnum(MemberComeSourceEnum comeSourceEnum) {
		this.comeSourceEnum = comeSourceEnum;
	}

}
