package cn.chiship.framework.business.biz.member.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberUserWalletChangeService;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChange;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChangeExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员钱包变化记录控制层
 * 2024/12/17
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/memberUserWalletChange")
@Api(tags = "会员钱包变化记录")
public class MemberUserWalletChangeController extends BaseController<MemberUserWalletChange, MemberUserWalletChangeExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserWalletChangeController.class);

    @Resource
    private MemberUserWalletChangeService memberUserWalletChangeService;

    @Override
    public BaseService getService() {
        return memberUserWalletChangeService;
    }

    @ApiOperation(value = "会员钱包变化记录分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "memberId", value = "会员主键", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型  0 消费 1:进账", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "timeStart", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "timeEnd", value = "结束时间", dataTypeClass = Long.class, paramType = "query"),
    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "memberId") String memberId,
                                           @RequestParam(required = false, defaultValue = "", value = "type") Byte type,
                                           @RequestParam(required = false, defaultValue = "", value = "timeStart") Long timeStart,
                                           @RequestParam(required = false, defaultValue = "", value = "timeEnd") Long timeEnd) {
        MemberUserWalletChangeExample memberUserWalletChangeExample = new MemberUserWalletChangeExample();
        //创造条件
        MemberUserWalletChangeExample.Criteria memberUserWalletChangeCriteria = memberUserWalletChangeExample.createCriteria();
        memberUserWalletChangeCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNull(type)) {
            memberUserWalletChangeCriteria.andTypeEqualTo(type);
        }
        if (!StringUtil.isNull(timeStart)) {
            memberUserWalletChangeCriteria.andGmtCreatedGreaterThanOrEqualTo(timeStart);
        }
        if (!StringUtil.isNull(timeEnd)) {
            memberUserWalletChangeCriteria.andGmtCreatedLessThanOrEqualTo(timeEnd);
        }
        if (!StringUtil.isNullOrEmpty(memberId)) {
            memberUserWalletChangeCriteria.andUserIdEqualTo(memberId);
        }
        return super.responseEntity(BaseResult.ok(super.page(memberUserWalletChangeExample)));
    }

    @ApiOperation(value = "当前用户会员钱包变化记录分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "memberId", value = "会员主键", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型  0 消费 1:进账", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "timeStart", value = "开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "timeEnd", value = "结束时间", dataTypeClass = Long.class, paramType = "query"),
    })
    @GetMapping(value = "/page/current")
    public ResponseEntity<BaseResult> pageCurrent(@RequestParam(required = false, defaultValue = "", value = "type") Byte type,
                                                  @RequestParam(required = false, defaultValue = "", value = "timeStart") Long timeStart,
                                                  @RequestParam(required = false, defaultValue = "", value = "timeEnd") Long timeEnd) {
        MemberUserWalletChangeExample memberUserWalletChangeExample = new MemberUserWalletChangeExample();
        //创造条件
        MemberUserWalletChangeExample.Criteria memberUserWalletChangeCriteria = memberUserWalletChangeExample.createCriteria();
        memberUserWalletChangeCriteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(getUserId());
        if (!StringUtil.isNull(type)) {
            memberUserWalletChangeCriteria.andTypeEqualTo(type);
        }
        if (!StringUtil.isNull(timeStart)) {
            memberUserWalletChangeCriteria.andGmtCreatedGreaterThanOrEqualTo(timeStart);
        }
        if (!StringUtil.isNull(timeEnd)) {
            memberUserWalletChangeCriteria.andGmtCreatedLessThanOrEqualTo(timeEnd);
        }
        return super.responseEntity(BaseResult.ok(super.page(memberUserWalletChangeExample)));
    }


    @SystemOptionAnnotation(describe = "会员钱包变化记录列表数据")
    @ApiOperation(value = "会员钱包变化记录列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        MemberUserWalletChangeExample memberUserWalletChangeExample = new MemberUserWalletChangeExample();
        //创造条件
        MemberUserWalletChangeExample.Criteria memberUserWalletChangeCriteria = memberUserWalletChangeExample.createCriteria();
        memberUserWalletChangeCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        memberUserWalletChangeExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(memberUserWalletChangeExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除会员钱包变化记录")
    @ApiOperation(value = "删除会员钱包变化记录")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        MemberUserWalletChangeExample memberUserWalletChangeExample = new MemberUserWalletChangeExample();
        memberUserWalletChangeExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(memberUserWalletChangeExample));
    }
}