package cn.chiship.framework.business.biz.cashier.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItem;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItemExample;
/**
 * 订单明细业务接口层
 * 2024/12/16
 * @author lijian
 */
public interface BusinessOrderHeaderItemService extends BaseService<BusinessOrderHeaderItem,BusinessOrderHeaderItemExample> {

}
