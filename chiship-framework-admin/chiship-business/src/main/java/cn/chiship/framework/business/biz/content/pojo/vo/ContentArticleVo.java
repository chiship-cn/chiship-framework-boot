package cn.chiship.framework.business.biz.content.pojo.vo;

import cn.chiship.framework.business.biz.content.entity.ContentArticle;

/**
 * 文档详细视图
 *
 * @author lijian
 */
public class ContentArticleVo extends ContentArticle {

	/**
	 * 栏目名称
	 */
	private String categoryName;

	/**
	 * 栏目层级
	 */
	private String categoryTree;

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryTree() {
		return categoryTree;
	}

	public void setCategoryTree(String categoryTree) {
		this.categoryTree = categoryTree;
	}

}
