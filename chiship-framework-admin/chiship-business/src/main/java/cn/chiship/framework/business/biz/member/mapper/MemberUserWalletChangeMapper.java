package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChange;
import cn.chiship.framework.business.biz.member.entity.MemberUserWalletChangeExample;


import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-17
 */
public interface MemberUserWalletChangeMapper extends BaseMapper<MemberUserWalletChange, MemberUserWalletChangeExample> {

}