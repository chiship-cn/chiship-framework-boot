package cn.chiship.framework.business.biz.member.controller;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberUserService;
import cn.chiship.framework.business.biz.member.entity.MemberUser;
import cn.chiship.framework.business.biz.member.entity.MemberUserExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 会员控制层 2022/2/23
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/memberUser")
@Api(tags = "会员")
public class MemberUserController extends BaseController<MemberUser, MemberUserExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberUserController.class);

	@Resource
	private MemberUserService memberUserService;

	@Override
	public BaseService getService() {
		return memberUserService;
	}

	@SystemOptionAnnotation(describe = "会员分页")
	@ApiOperation(value = "会员分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "userName", value = "用户名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "realName", value = "真实姓名", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "mobile", value = "手机号", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "userName") String userName,
			@RequestParam(required = false, defaultValue = "", value = "realName") String realName,
			@RequestParam(required = false, defaultValue = "", value = "mobile") String mobile) {
		MemberUserExample memberUserExample = new MemberUserExample();
		// 创造条件
		MemberUserExample.Criteria memberUserCriteria = memberUserExample.createCriteria();
		memberUserCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(userName)) {
			memberUserCriteria.andUserNameLike(userName + "%");
		}
		if (!StringUtil.isNullOrEmpty(mobile)) {
			memberUserCriteria.andMobileLike(mobile + "%");
		}
		if (!StringUtil.isNullOrEmpty(realName)) {
			memberUserCriteria.andRealNameLike(realName + "%");
		}
		return super.responseEntity(BaseResult.ok(super.page(memberUserExample)));
	}


	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新会员")
	@ApiOperation(value = "更新会员")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid MemberUserDto memberUserDto) {
		MemberUser memberUser = new MemberUser();
		BeanUtils.copyProperties(memberUserDto, memberUser);
		return super.responseEntity(super.update(id, memberUser));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除会员")
	@ApiOperation(value = "删除会员")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		MemberUserExample memberUserExample = new MemberUserExample();
		memberUserExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(memberUserExample));
	}

	@SystemOptionAnnotation(describe = "通知公告发布", option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE)
	@ApiOperation(value = "通知公告发布")
	@ApiImplicitParams({})
	@PostMapping(value = "publishNotice/{noticeId}")
	public ResponseEntity<BaseResult> publishNotice(@PathVariable("noticeId") String noticeId,
			@Valid @RequestBody String scope) {
		return super.responseEntity(memberUserService.publishNotice(noticeId, scope));
	}

}
