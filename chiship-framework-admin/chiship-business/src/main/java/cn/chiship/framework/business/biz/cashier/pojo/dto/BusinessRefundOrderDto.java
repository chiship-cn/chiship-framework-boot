package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.math.BigDecimal;

/**
 * 退款订单表单 2024/6/5
 *
 * @author LiJian
 */
@ApiModel(value = "退款订单表单")
public class BusinessRefundOrderDto {

	@ApiModelProperty(value = "退款单号")
	private String refundNo;

	@ApiModelProperty(value = "订单号")
	private String orderNo;

	@ApiModelProperty(value = "交易号")
	private String tradeNo;

	@ApiModelProperty(value = "退款金额")
	private BigDecimal refundTotal;

	@ApiModelProperty(value = "退款原因")
	private String refundReason;

	@ApiModelProperty(value = "退款渠道")
	private String channelPayWay;

	public BusinessRefundOrderDto(String refundNo, String orderNo, String tradeNo, BigDecimal refundTotal,
			String refundReason, String channelPayWay) {
		this.refundNo = refundNo;
		this.orderNo = orderNo;
		this.tradeNo = tradeNo;
		this.refundTotal = refundTotal;
		this.refundReason = refundReason;
		this.channelPayWay = channelPayWay;
	}

	public String getRefundNo() {
		return refundNo;
	}

	public void setRefundNo(String refundNo) {
		this.refundNo = refundNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public BigDecimal getRefundTotal() {
		return refundTotal;
	}

	public void setRefundTotal(BigDecimal refundTotal) {
		this.refundTotal = refundTotal;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

}
