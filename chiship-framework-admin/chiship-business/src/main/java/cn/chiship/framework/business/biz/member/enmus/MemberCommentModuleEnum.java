package cn.chiship.framework.business.biz.member.enmus;

/**
 * 用户评论模块枚举
 *
 * @author lijian
 */
public enum MemberCommentModuleEnum {

	MEMBER_COMMENT_MODULE_ARTICLE("article", "文章"),;

	/**
	 * 业务类型
	 */
	private String type;

	/**
	 * 业务描述
	 */
	private String message;

	MemberCommentModuleEnum(String type, String message) {
		this.type = type;
		this.message = message;
	}

	public String getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
