package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.cashier.mapper.BusinessOrderHeaderItemMapper;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItem;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItemExample;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderHeaderItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 订单明细业务接口实现层
 * 2024/12/16
 * @author lijian
 */
@Service
public class BusinessOrderHeaderItemServiceImpl extends BaseServiceImpl<BusinessOrderHeaderItem,BusinessOrderHeaderItemExample> implements BusinessOrderHeaderItemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessOrderHeaderItemServiceImpl.class);

    @Resource
    BusinessOrderHeaderItemMapper businessOrderHeaderItemMapper;

}
