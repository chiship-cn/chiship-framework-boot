package cn.chiship.framework.business.biz.content.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-30
 */
public class ContentArticleExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ContentArticleExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNull() {
			addCriterion("created_by is null");
			return (Criteria) this;
		}

		public Criteria andCreatedByIsNotNull() {
			addCriterion("created_by is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedByEqualTo(String value) {
			addCriterion("created_by =", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotEqualTo(String value) {
			addCriterion("created_by <>", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThan(String value) {
			addCriterion("created_by >", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByGreaterThanOrEqualTo(String value) {
			addCriterion("created_by >=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThan(String value) {
			addCriterion("created_by <", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLessThanOrEqualTo(String value) {
			addCriterion("created_by <=", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByLike(String value) {
			addCriterion("created_by like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotLike(String value) {
			addCriterion("created_by not like", value, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByIn(List<String> values) {
			addCriterion("created_by in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotIn(List<String> values) {
			addCriterion("created_by not in", values, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByBetween(String value1, String value2) {
			addCriterion("created_by between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andCreatedByNotBetween(String value1, String value2) {
			addCriterion("created_by not between", value1, value2, "createdBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNull() {
			addCriterion("modify_by is null");
			return (Criteria) this;
		}

		public Criteria andModifyByIsNotNull() {
			addCriterion("modify_by is not null");
			return (Criteria) this;
		}

		public Criteria andModifyByEqualTo(String value) {
			addCriterion("modify_by =", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotEqualTo(String value) {
			addCriterion("modify_by <>", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThan(String value) {
			addCriterion("modify_by >", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByGreaterThanOrEqualTo(String value) {
			addCriterion("modify_by >=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThan(String value) {
			addCriterion("modify_by <", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLessThanOrEqualTo(String value) {
			addCriterion("modify_by <=", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByLike(String value) {
			addCriterion("modify_by like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotLike(String value) {
			addCriterion("modify_by not like", value, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByIn(List<String> values) {
			addCriterion("modify_by in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotIn(List<String> values) {
			addCriterion("modify_by not in", values, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByBetween(String value1, String value2) {
			addCriterion("modify_by between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andModifyByNotBetween(String value1, String value2) {
			addCriterion("modify_by not between", value1, value2, "modifyBy");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNull() {
			addCriterion("created_user_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIsNotNull() {
			addCriterion("created_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdEqualTo(String value) {
			addCriterion("created_user_id =", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotEqualTo(String value) {
			addCriterion("created_user_id <>", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThan(String value) {
			addCriterion("created_user_id >", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_user_id >=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThan(String value) {
			addCriterion("created_user_id <", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLessThanOrEqualTo(String value) {
			addCriterion("created_user_id <=", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdLike(String value) {
			addCriterion("created_user_id like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotLike(String value) {
			addCriterion("created_user_id not like", value, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdIn(List<String> values) {
			addCriterion("created_user_id in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotIn(List<String> values) {
			addCriterion("created_user_id not in", values, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdBetween(String value1, String value2) {
			addCriterion("created_user_id between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedUserIdNotBetween(String value1, String value2) {
			addCriterion("created_user_id not between", value1, value2, "createdUserId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIsNull() {
			addCriterion("created_org_id is null");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIsNotNull() {
			addCriterion("created_org_id is not null");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdEqualTo(String value) {
			addCriterion("created_org_id =", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotEqualTo(String value) {
			addCriterion("created_org_id <>", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdGreaterThan(String value) {
			addCriterion("created_org_id >", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdGreaterThanOrEqualTo(String value) {
			addCriterion("created_org_id >=", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLessThan(String value) {
			addCriterion("created_org_id <", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLessThanOrEqualTo(String value) {
			addCriterion("created_org_id <=", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdLike(String value) {
			addCriterion("created_org_id like", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotLike(String value) {
			addCriterion("created_org_id not like", value, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdIn(List<String> values) {
			addCriterion("created_org_id in", values, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotIn(List<String> values) {
			addCriterion("created_org_id not in", values, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdBetween(String value1, String value2) {
			addCriterion("created_org_id between", value1, value2, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCreatedOrgIdNotBetween(String value1, String value2) {
			addCriterion("created_org_id not between", value1, value2, "createdOrgId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIsNull() {
			addCriterion("category_id is null");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIsNotNull() {
			addCriterion("category_id is not null");
			return (Criteria) this;
		}

		public Criteria andCategoryIdEqualTo(String value) {
			addCriterion("category_id =", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotEqualTo(String value) {
			addCriterion("category_id <>", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdGreaterThan(String value) {
			addCriterion("category_id >", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdGreaterThanOrEqualTo(String value) {
			addCriterion("category_id >=", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdLessThan(String value) {
			addCriterion("category_id <", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdLessThanOrEqualTo(String value) {
			addCriterion("category_id <=", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdLike(String value) {
			addCriterion("category_id like", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotLike(String value) {
			addCriterion("category_id not like", value, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdIn(List<String> values) {
			addCriterion("category_id in", values, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotIn(List<String> values) {
			addCriterion("category_id not in", values, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdBetween(String value1, String value2) {
			addCriterion("category_id between", value1, value2, "categoryId");
			return (Criteria) this;
		}

		public Criteria andCategoryIdNotBetween(String value1, String value2) {
			addCriterion("category_id not between", value1, value2, "categoryId");
			return (Criteria) this;
		}

		public Criteria andTitleIsNull() {
			addCriterion("title is null");
			return (Criteria) this;
		}

		public Criteria andTitleIsNotNull() {
			addCriterion("title is not null");
			return (Criteria) this;
		}

		public Criteria andTitleEqualTo(String value) {
			addCriterion("title =", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotEqualTo(String value) {
			addCriterion("title <>", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThan(String value) {
			addCriterion("title >", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThanOrEqualTo(String value) {
			addCriterion("title >=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThan(String value) {
			addCriterion("title <", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThanOrEqualTo(String value) {
			addCriterion("title <=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLike(String value) {
			addCriterion("title like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotLike(String value) {
			addCriterion("title not like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleIn(List<String> values) {
			addCriterion("title in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotIn(List<String> values) {
			addCriterion("title not in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleBetween(String value1, String value2) {
			addCriterion("title between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotBetween(String value1, String value2) {
			addCriterion("title not between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andSummaryIsNull() {
			addCriterion("summary is null");
			return (Criteria) this;
		}

		public Criteria andSummaryIsNotNull() {
			addCriterion("summary is not null");
			return (Criteria) this;
		}

		public Criteria andSummaryEqualTo(String value) {
			addCriterion("summary =", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryNotEqualTo(String value) {
			addCriterion("summary <>", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryGreaterThan(String value) {
			addCriterion("summary >", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryGreaterThanOrEqualTo(String value) {
			addCriterion("summary >=", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryLessThan(String value) {
			addCriterion("summary <", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryLessThanOrEqualTo(String value) {
			addCriterion("summary <=", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryLike(String value) {
			addCriterion("summary like", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryNotLike(String value) {
			addCriterion("summary not like", value, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryIn(List<String> values) {
			addCriterion("summary in", values, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryNotIn(List<String> values) {
			addCriterion("summary not in", values, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryBetween(String value1, String value2) {
			addCriterion("summary between", value1, value2, "summary");
			return (Criteria) this;
		}

		public Criteria andSummaryNotBetween(String value1, String value2) {
			addCriterion("summary not between", value1, value2, "summary");
			return (Criteria) this;
		}

		public Criteria andKeywordsIsNull() {
			addCriterion("keywords is null");
			return (Criteria) this;
		}

		public Criteria andKeywordsIsNotNull() {
			addCriterion("keywords is not null");
			return (Criteria) this;
		}

		public Criteria andKeywordsEqualTo(String value) {
			addCriterion("keywords =", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsNotEqualTo(String value) {
			addCriterion("keywords <>", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsGreaterThan(String value) {
			addCriterion("keywords >", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsGreaterThanOrEqualTo(String value) {
			addCriterion("keywords >=", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsLessThan(String value) {
			addCriterion("keywords <", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsLessThanOrEqualTo(String value) {
			addCriterion("keywords <=", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsLike(String value) {
			addCriterion("keywords like", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsNotLike(String value) {
			addCriterion("keywords not like", value, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsIn(List<String> values) {
			addCriterion("keywords in", values, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsNotIn(List<String> values) {
			addCriterion("keywords not in", values, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsBetween(String value1, String value2) {
			addCriterion("keywords between", value1, value2, "keywords");
			return (Criteria) this;
		}

		public Criteria andKeywordsNotBetween(String value1, String value2) {
			addCriterion("keywords not between", value1, value2, "keywords");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionIsNull() {
			addCriterion("meta_description is null");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionIsNotNull() {
			addCriterion("meta_description is not null");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionEqualTo(String value) {
			addCriterion("meta_description =", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionNotEqualTo(String value) {
			addCriterion("meta_description <>", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionGreaterThan(String value) {
			addCriterion("meta_description >", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionGreaterThanOrEqualTo(String value) {
			addCriterion("meta_description >=", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionLessThan(String value) {
			addCriterion("meta_description <", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionLessThanOrEqualTo(String value) {
			addCriterion("meta_description <=", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionLike(String value) {
			addCriterion("meta_description like", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionNotLike(String value) {
			addCriterion("meta_description not like", value, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionIn(List<String> values) {
			addCriterion("meta_description in", values, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionNotIn(List<String> values) {
			addCriterion("meta_description not in", values, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionBetween(String value1, String value2) {
			addCriterion("meta_description between", value1, value2, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andMetaDescriptionNotBetween(String value1, String value2) {
			addCriterion("meta_description not between", value1, value2, "metaDescription");
			return (Criteria) this;
		}

		public Criteria andIsTopIsNull() {
			addCriterion("is_top is null");
			return (Criteria) this;
		}

		public Criteria andIsTopIsNotNull() {
			addCriterion("is_top is not null");
			return (Criteria) this;
		}

		public Criteria andIsTopEqualTo(Byte value) {
			addCriterion("is_top =", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopNotEqualTo(Byte value) {
			addCriterion("is_top <>", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopGreaterThan(Byte value) {
			addCriterion("is_top >", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_top >=", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopLessThan(Byte value) {
			addCriterion("is_top <", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopLessThanOrEqualTo(Byte value) {
			addCriterion("is_top <=", value, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopIn(List<Byte> values) {
			addCriterion("is_top in", values, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopNotIn(List<Byte> values) {
			addCriterion("is_top not in", values, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopBetween(Byte value1, Byte value2) {
			addCriterion("is_top between", value1, value2, "isTop");
			return (Criteria) this;
		}

		public Criteria andIsTopNotBetween(Byte value1, Byte value2) {
			addCriterion("is_top not between", value1, value2, "isTop");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("status is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("status is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Byte value) {
			addCriterion("status =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Byte value) {
			addCriterion("status <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Byte value) {
			addCriterion("status >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("status >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Byte value) {
			addCriterion("status <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Byte value) {
			addCriterion("status <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Byte> values) {
			addCriterion("status in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Byte> values) {
			addCriterion("status not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Byte value1, Byte value2) {
			addCriterion("status between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("status not between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andPublishDateIsNull() {
			addCriterion("publish_date is null");
			return (Criteria) this;
		}

		public Criteria andPublishDateIsNotNull() {
			addCriterion("publish_date is not null");
			return (Criteria) this;
		}

		public Criteria andPublishDateEqualTo(Long value) {
			addCriterion("publish_date =", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateNotEqualTo(Long value) {
			addCriterion("publish_date <>", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateGreaterThan(Long value) {
			addCriterion("publish_date >", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateGreaterThanOrEqualTo(Long value) {
			addCriterion("publish_date >=", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateLessThan(Long value) {
			addCriterion("publish_date <", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateLessThanOrEqualTo(Long value) {
			addCriterion("publish_date <=", value, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateIn(List<Long> values) {
			addCriterion("publish_date in", values, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateNotIn(List<Long> values) {
			addCriterion("publish_date not in", values, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateBetween(Long value1, Long value2) {
			addCriterion("publish_date between", value1, value2, "publishDate");
			return (Criteria) this;
		}

		public Criteria andPublishDateNotBetween(Long value1, Long value2) {
			addCriterion("publish_date not between", value1, value2, "publishDate");
			return (Criteria) this;
		}

		public Criteria andIsOriginalIsNull() {
			addCriterion("is_original is null");
			return (Criteria) this;
		}

		public Criteria andIsOriginalIsNotNull() {
			addCriterion("is_original is not null");
			return (Criteria) this;
		}

		public Criteria andIsOriginalEqualTo(Byte value) {
			addCriterion("is_original =", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalNotEqualTo(Byte value) {
			addCriterion("is_original <>", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalGreaterThan(Byte value) {
			addCriterion("is_original >", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_original >=", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalLessThan(Byte value) {
			addCriterion("is_original <", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalLessThanOrEqualTo(Byte value) {
			addCriterion("is_original <=", value, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalIn(List<Byte> values) {
			addCriterion("is_original in", values, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalNotIn(List<Byte> values) {
			addCriterion("is_original not in", values, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalBetween(Byte value1, Byte value2) {
			addCriterion("is_original between", value1, value2, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andIsOriginalNotBetween(Byte value1, Byte value2) {
			addCriterion("is_original not between", value1, value2, "isOriginal");
			return (Criteria) this;
		}

		public Criteria andSourceIsNull() {
			addCriterion("source is null");
			return (Criteria) this;
		}

		public Criteria andSourceIsNotNull() {
			addCriterion("source is not null");
			return (Criteria) this;
		}

		public Criteria andSourceEqualTo(String value) {
			addCriterion("source =", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceNotEqualTo(String value) {
			addCriterion("source <>", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceGreaterThan(String value) {
			addCriterion("source >", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceGreaterThanOrEqualTo(String value) {
			addCriterion("source >=", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceLessThan(String value) {
			addCriterion("source <", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceLessThanOrEqualTo(String value) {
			addCriterion("source <=", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceLike(String value) {
			addCriterion("source like", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceNotLike(String value) {
			addCriterion("source not like", value, "source");
			return (Criteria) this;
		}

		public Criteria andSourceIn(List<String> values) {
			addCriterion("source in", values, "source");
			return (Criteria) this;
		}

		public Criteria andSourceNotIn(List<String> values) {
			addCriterion("source not in", values, "source");
			return (Criteria) this;
		}

		public Criteria andSourceBetween(String value1, String value2) {
			addCriterion("source between", value1, value2, "source");
			return (Criteria) this;
		}

		public Criteria andSourceNotBetween(String value1, String value2) {
			addCriterion("source not between", value1, value2, "source");
			return (Criteria) this;
		}

		public Criteria andSourceUrlIsNull() {
			addCriterion("source_url is null");
			return (Criteria) this;
		}

		public Criteria andSourceUrlIsNotNull() {
			addCriterion("source_url is not null");
			return (Criteria) this;
		}

		public Criteria andSourceUrlEqualTo(String value) {
			addCriterion("source_url =", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlNotEqualTo(String value) {
			addCriterion("source_url <>", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlGreaterThan(String value) {
			addCriterion("source_url >", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlGreaterThanOrEqualTo(String value) {
			addCriterion("source_url >=", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlLessThan(String value) {
			addCriterion("source_url <", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlLessThanOrEqualTo(String value) {
			addCriterion("source_url <=", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlLike(String value) {
			addCriterion("source_url like", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlNotLike(String value) {
			addCriterion("source_url not like", value, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlIn(List<String> values) {
			addCriterion("source_url in", values, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlNotIn(List<String> values) {
			addCriterion("source_url not in", values, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlBetween(String value1, String value2) {
			addCriterion("source_url between", value1, value2, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andSourceUrlNotBetween(String value1, String value2) {
			addCriterion("source_url not between", value1, value2, "sourceUrl");
			return (Criteria) this;
		}

		public Criteria andAuthorIsNull() {
			addCriterion("author is null");
			return (Criteria) this;
		}

		public Criteria andAuthorIsNotNull() {
			addCriterion("author is not null");
			return (Criteria) this;
		}

		public Criteria andAuthorEqualTo(String value) {
			addCriterion("author =", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorNotEqualTo(String value) {
			addCriterion("author <>", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorGreaterThan(String value) {
			addCriterion("author >", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorGreaterThanOrEqualTo(String value) {
			addCriterion("author >=", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorLessThan(String value) {
			addCriterion("author <", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorLessThanOrEqualTo(String value) {
			addCriterion("author <=", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorLike(String value) {
			addCriterion("author like", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorNotLike(String value) {
			addCriterion("author not like", value, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorIn(List<String> values) {
			addCriterion("author in", values, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorNotIn(List<String> values) {
			addCriterion("author not in", values, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorBetween(String value1, String value2) {
			addCriterion("author between", value1, value2, "author");
			return (Criteria) this;
		}

		public Criteria andAuthorNotBetween(String value1, String value2) {
			addCriterion("author not between", value1, value2, "author");
			return (Criteria) this;
		}

		public Criteria andImage1IsNull() {
			addCriterion("image1 is null");
			return (Criteria) this;
		}

		public Criteria andImage1IsNotNull() {
			addCriterion("image1 is not null");
			return (Criteria) this;
		}

		public Criteria andImage1EqualTo(String value) {
			addCriterion("image1 =", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1NotEqualTo(String value) {
			addCriterion("image1 <>", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1GreaterThan(String value) {
			addCriterion("image1 >", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1GreaterThanOrEqualTo(String value) {
			addCriterion("image1 >=", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1LessThan(String value) {
			addCriterion("image1 <", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1LessThanOrEqualTo(String value) {
			addCriterion("image1 <=", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1Like(String value) {
			addCriterion("image1 like", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1NotLike(String value) {
			addCriterion("image1 not like", value, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1In(List<String> values) {
			addCriterion("image1 in", values, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1NotIn(List<String> values) {
			addCriterion("image1 not in", values, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1Between(String value1, String value2) {
			addCriterion("image1 between", value1, value2, "image1");
			return (Criteria) this;
		}

		public Criteria andImage1NotBetween(String value1, String value2) {
			addCriterion("image1 not between", value1, value2, "image1");
			return (Criteria) this;
		}

		public Criteria andImage2IsNull() {
			addCriterion("image2 is null");
			return (Criteria) this;
		}

		public Criteria andImage2IsNotNull() {
			addCriterion("image2 is not null");
			return (Criteria) this;
		}

		public Criteria andImage2EqualTo(String value) {
			addCriterion("image2 =", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2NotEqualTo(String value) {
			addCriterion("image2 <>", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2GreaterThan(String value) {
			addCriterion("image2 >", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2GreaterThanOrEqualTo(String value) {
			addCriterion("image2 >=", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2LessThan(String value) {
			addCriterion("image2 <", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2LessThanOrEqualTo(String value) {
			addCriterion("image2 <=", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2Like(String value) {
			addCriterion("image2 like", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2NotLike(String value) {
			addCriterion("image2 not like", value, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2In(List<String> values) {
			addCriterion("image2 in", values, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2NotIn(List<String> values) {
			addCriterion("image2 not in", values, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2Between(String value1, String value2) {
			addCriterion("image2 between", value1, value2, "image2");
			return (Criteria) this;
		}

		public Criteria andImage2NotBetween(String value1, String value2) {
			addCriterion("image2 not between", value1, value2, "image2");
			return (Criteria) this;
		}

		public Criteria andImage3IsNull() {
			addCriterion("image3 is null");
			return (Criteria) this;
		}

		public Criteria andImage3IsNotNull() {
			addCriterion("image3 is not null");
			return (Criteria) this;
		}

		public Criteria andImage3EqualTo(String value) {
			addCriterion("image3 =", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3NotEqualTo(String value) {
			addCriterion("image3 <>", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3GreaterThan(String value) {
			addCriterion("image3 >", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3GreaterThanOrEqualTo(String value) {
			addCriterion("image3 >=", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3LessThan(String value) {
			addCriterion("image3 <", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3LessThanOrEqualTo(String value) {
			addCriterion("image3 <=", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3Like(String value) {
			addCriterion("image3 like", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3NotLike(String value) {
			addCriterion("image3 not like", value, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3In(List<String> values) {
			addCriterion("image3 in", values, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3NotIn(List<String> values) {
			addCriterion("image3 not in", values, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3Between(String value1, String value2) {
			addCriterion("image3 between", value1, value2, "image3");
			return (Criteria) this;
		}

		public Criteria andImage3NotBetween(String value1, String value2) {
			addCriterion("image3 not between", value1, value2, "image3");
			return (Criteria) this;
		}

		public Criteria andFileIsNull() {
			addCriterion("file is null");
			return (Criteria) this;
		}

		public Criteria andFileIsNotNull() {
			addCriterion("file is not null");
			return (Criteria) this;
		}

		public Criteria andFileEqualTo(String value) {
			addCriterion("file =", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileNotEqualTo(String value) {
			addCriterion("file <>", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileGreaterThan(String value) {
			addCriterion("file >", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileGreaterThanOrEqualTo(String value) {
			addCriterion("file >=", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileLessThan(String value) {
			addCriterion("file <", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileLessThanOrEqualTo(String value) {
			addCriterion("file <=", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileLike(String value) {
			addCriterion("file like", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileNotLike(String value) {
			addCriterion("file not like", value, "file");
			return (Criteria) this;
		}

		public Criteria andFileIn(List<String> values) {
			addCriterion("file in", values, "file");
			return (Criteria) this;
		}

		public Criteria andFileNotIn(List<String> values) {
			addCriterion("file not in", values, "file");
			return (Criteria) this;
		}

		public Criteria andFileBetween(String value1, String value2) {
			addCriterion("file between", value1, value2, "file");
			return (Criteria) this;
		}

		public Criteria andFileNotBetween(String value1, String value2) {
			addCriterion("file not between", value1, value2, "file");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentIsNull() {
			addCriterion("is_allow_comment is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentIsNotNull() {
			addCriterion("is_allow_comment is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentEqualTo(Byte value) {
			addCriterion("is_allow_comment =", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentNotEqualTo(Byte value) {
			addCriterion("is_allow_comment <>", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentGreaterThan(Byte value) {
			addCriterion("is_allow_comment >", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_comment >=", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentLessThan(Byte value) {
			addCriterion("is_allow_comment <", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_comment <=", value, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentIn(List<Byte> values) {
			addCriterion("is_allow_comment in", values, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentNotIn(List<Byte> values) {
			addCriterion("is_allow_comment not in", values, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_comment between", value1, value2, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andIsAllowCommentNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_comment not between", value1, value2, "isAllowComment");
			return (Criteria) this;
		}

		public Criteria andViewsIsNull() {
			addCriterion("views is null");
			return (Criteria) this;
		}

		public Criteria andViewsIsNotNull() {
			addCriterion("views is not null");
			return (Criteria) this;
		}

		public Criteria andViewsEqualTo(Long value) {
			addCriterion("views =", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsNotEqualTo(Long value) {
			addCriterion("views <>", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsGreaterThan(Long value) {
			addCriterion("views >", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsGreaterThanOrEqualTo(Long value) {
			addCriterion("views >=", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsLessThan(Long value) {
			addCriterion("views <", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsLessThanOrEqualTo(Long value) {
			addCriterion("views <=", value, "views");
			return (Criteria) this;
		}

		public Criteria andViewsIn(List<Long> values) {
			addCriterion("views in", values, "views");
			return (Criteria) this;
		}

		public Criteria andViewsNotIn(List<Long> values) {
			addCriterion("views not in", values, "views");
			return (Criteria) this;
		}

		public Criteria andViewsBetween(Long value1, Long value2) {
			addCriterion("views between", value1, value2, "views");
			return (Criteria) this;
		}

		public Criteria andViewsNotBetween(Long value1, Long value2) {
			addCriterion("views not between", value1, value2, "views");
			return (Criteria) this;
		}

		public Criteria andCommentsIsNull() {
			addCriterion("comments is null");
			return (Criteria) this;
		}

		public Criteria andCommentsIsNotNull() {
			addCriterion("comments is not null");
			return (Criteria) this;
		}

		public Criteria andCommentsEqualTo(Long value) {
			addCriterion("comments =", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotEqualTo(Long value) {
			addCriterion("comments <>", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsGreaterThan(Long value) {
			addCriterion("comments >", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsGreaterThanOrEqualTo(Long value) {
			addCriterion("comments >=", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsLessThan(Long value) {
			addCriterion("comments <", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsLessThanOrEqualTo(Long value) {
			addCriterion("comments <=", value, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsIn(List<Long> values) {
			addCriterion("comments in", values, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotIn(List<Long> values) {
			addCriterion("comments not in", values, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsBetween(Long value1, Long value2) {
			addCriterion("comments between", value1, value2, "comments");
			return (Criteria) this;
		}

		public Criteria andCommentsNotBetween(Long value1, Long value2) {
			addCriterion("comments not between", value1, value2, "comments");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumIsNull() {
			addCriterion("thumb_up_num is null");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumIsNotNull() {
			addCriterion("thumb_up_num is not null");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumEqualTo(Long value) {
			addCriterion("thumb_up_num =", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumNotEqualTo(Long value) {
			addCriterion("thumb_up_num <>", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumGreaterThan(Long value) {
			addCriterion("thumb_up_num >", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumGreaterThanOrEqualTo(Long value) {
			addCriterion("thumb_up_num >=", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumLessThan(Long value) {
			addCriterion("thumb_up_num <", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumLessThanOrEqualTo(Long value) {
			addCriterion("thumb_up_num <=", value, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumIn(List<Long> values) {
			addCriterion("thumb_up_num in", values, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumNotIn(List<Long> values) {
			addCriterion("thumb_up_num not in", values, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumBetween(Long value1, Long value2) {
			addCriterion("thumb_up_num between", value1, value2, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andThumbUpNumNotBetween(Long value1, Long value2) {
			addCriterion("thumb_up_num not between", value1, value2, "thumbUpNum");
			return (Criteria) this;
		}

		public Criteria andCollectionsIsNull() {
			addCriterion("collections is null");
			return (Criteria) this;
		}

		public Criteria andCollectionsIsNotNull() {
			addCriterion("collections is not null");
			return (Criteria) this;
		}

		public Criteria andCollectionsEqualTo(Long value) {
			addCriterion("collections =", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsNotEqualTo(Long value) {
			addCriterion("collections <>", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsGreaterThan(Long value) {
			addCriterion("collections >", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsGreaterThanOrEqualTo(Long value) {
			addCriterion("collections >=", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsLessThan(Long value) {
			addCriterion("collections <", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsLessThanOrEqualTo(Long value) {
			addCriterion("collections <=", value, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsIn(List<Long> values) {
			addCriterion("collections in", values, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsNotIn(List<Long> values) {
			addCriterion("collections not in", values, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsBetween(Long value1, Long value2) {
			addCriterion("collections between", value1, value2, "collections");
			return (Criteria) this;
		}

		public Criteria andCollectionsNotBetween(Long value1, Long value2) {
			addCriterion("collections not between", value1, value2, "collections");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryIsNull() {
			addCriterion("sync_material_library is null");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryIsNotNull() {
			addCriterion("sync_material_library is not null");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryEqualTo(Byte value) {
			addCriterion("sync_material_library =", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryNotEqualTo(Byte value) {
			addCriterion("sync_material_library <>", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryGreaterThan(Byte value) {
			addCriterion("sync_material_library >", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryGreaterThanOrEqualTo(Byte value) {
			addCriterion("sync_material_library >=", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryLessThan(Byte value) {
			addCriterion("sync_material_library <", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryLessThanOrEqualTo(Byte value) {
			addCriterion("sync_material_library <=", value, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryIn(List<Byte> values) {
			addCriterion("sync_material_library in", values, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryNotIn(List<Byte> values) {
			addCriterion("sync_material_library not in", values, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryBetween(Byte value1, Byte value2) {
			addCriterion("sync_material_library between", value1, value2, "syncMaterialLibrary");
			return (Criteria) this;
		}

		public Criteria andSyncMaterialLibraryNotBetween(Byte value1, Byte value2) {
			addCriterion("sync_material_library not between", value1, value2, "syncMaterialLibrary");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}