package cn.chiship.framework.business.biz.cashier.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

/**
 * 提现申请表单
 * 2024/12/17
 *
 * @author LiJian
 */
@ApiModel(value = "提现申请表单")
public class WithdrawalApplicationDto {
    @ApiModelProperty(value = "小程序或微信公众号用户唯一码", required = true)
    @NotEmpty(message = "小程序或微信公众号用户唯一码" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 50)
    private String openId;

    @ApiModelProperty(value = "提现金额", required = true)
    @NotNull(message = "提现金额" + BaseTipConstants.NOT_EMPTY)
    private BigDecimal money;

    @ApiModelProperty(value = "提现渠道（wx_v3：微信；zfb：支付宝）", required = true)
    @NotNull(message = "提现渠道" + BaseTipConstants.NOT_EMPTY)
    @Pattern(regexp = "wx_v3|zfb", message = "支付渠道：wx_v3，zfb")
    String channelWay;


    @ApiModelProperty(value = "提现理由", required = true)
    @NotNull(message = "提现理由" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 50)
    private String reason;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getChannelWay() {
        return channelWay;
    }

    public void setChannelWay(String channelWay) {
        this.channelWay = channelWay;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}