package cn.chiship.framework.business.biz.member.service;

import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserModifyDto;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberUserRegisterDto;
import cn.chiship.framework.third.core.common.ThirdApplicationOauthTypeEnum;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.member.entity.MemberUser;
import cn.chiship.framework.business.biz.member.entity.MemberUserExample;
import cn.chiship.sdk.framework.pojo.dto.AppletLoginOrRegisterDto;
import cn.chiship.sdk.framework.pojo.dto.UserForgotPasswordDto;
import cn.chiship.sdk.framework.pojo.dto.UserModifyPasswordDto;

/**
 * 会员业务接口层 2022/2/23
 *
 * @author lijian
 */
public interface MemberUserService extends BaseService<MemberUser, MemberUserExample> {

    /**
     * 用户名+密码登录
     *
     * @param username
     * @param password
     * @param ip
     * @return
     */
    BaseResult login(String username, String password, String ip);

    /**
     * 手机登录
     *
     * @param mobile
     * @param ip
     * @return
     */
    BaseResult mobileLogin(String mobile, String ip);

    /**
     * 小程序登录
     *
     * @param appId
     * @param openId
     * @param ip
     * @return
     */
    BaseResult appletLogin(String appId, String openId, String ip);

    /**
     * 用户注册
     *
     * @param memberUserRegisterDto
     * @return
     */
    BaseResult register(MemberUserRegisterDto memberUserRegisterDto);

    /**
     * 授权注册
     *
     * @param appId
     * @param appletLoginOrRegisterDto
     * @param thirdApplicationOauthTypeEnum
     * @param ip
     * @return
     */
    BaseResult grantAuthRegister(String appId, AppletLoginOrRegisterDto appletLoginOrRegisterDto,
                                 ThirdApplicationOauthTypeEnum thirdApplicationOauthTypeEnum, String ip);

    /**
     * 设置用户名
     *
     * @param userName
     * @param id
     * @return
     */
    BaseResult modifyUserName(String id, String userName);

    /**
     * 修改昵称
     *
     * @param id
     * @param nickName
     * @return
     */
    BaseResult modifyNickName(String id, String nickName);

    /**
     * 修改用户信息
     *
     * @param memberUserModifyDto 用户实体
     * @param id                  主键
     * @return BaseResult
     */
    BaseResult modifyUser(MemberUserModifyDto memberUserModifyDto, String id);

    /**
     * 忘记密码
     *
     * @param userForgotPasswordDto
     * @return
     */
    BaseResult forgotPassword(UserForgotPasswordDto userForgotPasswordDto);

    /**
     * 修改密码
     *
     * @param userModifyPasswordDto
     * @param cacheUserVO
     * @return
     */
    BaseResult modifyPassword(UserModifyPasswordDto userModifyPasswordDto, CacheUserVO cacheUserVO);

    /**
     * 发布通知公告
     *
     * @param noticeId
     * @param scope
     * @return
     */
    BaseResult publishNotice(String noticeId, String scope);

    /**
     * 根据会员ID获取钱包数据
     *
     * @param userId
     * @return
     */
    BaseResult walletByUserId(String userId);

}
