package cn.chiship.framework.business.biz.member.controller;

import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeEnum;
import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeModuleEnum;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeIsAddDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.member.service.MemberCollectionLikeService;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLike;
import cn.chiship.framework.business.biz.member.entity.MemberCollectionLikeExample;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCollectionLikeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 收藏点赞控制层 2024/11/21
 *
 * @author lijian
 */
@RestController
@RequestMapping("/memberCollectionLike")
@Api(tags = "收藏点赞")
public class MemberCollectionLikeController extends BaseController<MemberCollectionLike, MemberCollectionLikeExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberCollectionLikeController.class);

	@Resource
	private MemberCollectionLikeService memberCollectionLikeService;

	@Override
	public BaseService getService() {
		return memberCollectionLikeService;
	}

	@SystemOptionAnnotation(describe = "收藏点赞分页")
	@ApiOperation(value = "收藏点赞分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "module", value = "模块枚举", defaultValue = "",
					dataTypeClass = MemberCollectionLikeModuleEnum.class, paramType = "query"),
			@ApiImplicitParam(name = "moduleId", value = "模块ID", defaultValue = "", dataTypeClass = String.class,
					paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型枚举", defaultValue = "",
					dataTypeClass = MemberCollectionLikeEnum.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "type") MemberCollectionLikeEnum type,
			@RequestParam(required = false, defaultValue = "", value = "module") MemberCollectionLikeModuleEnum module,
			@RequestParam(required = false, defaultValue = "", value = "moduleId") String moduleId) {
		MemberCollectionLikeExample memberCollectionLikeExample = new MemberCollectionLikeExample();
		// 创造条件
		MemberCollectionLikeExample.Criteria memberCollectionLikeCriteria = memberCollectionLikeExample
				.createCriteria();
		memberCollectionLikeCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		if (!StringUtil.isNull(type)) {
			memberCollectionLikeCriteria.andTypeEqualTo(type.getType());
		}
		if (!StringUtil.isNull(module)) {
			memberCollectionLikeCriteria.andModuleEqualTo(module.getType());
		}
		if (!StringUtil.isNullOrEmpty(moduleId)) {
			memberCollectionLikeCriteria.andModuleIdEqualTo(moduleId);
		}
		return super.responseEntity(BaseResult.ok(super.page(memberCollectionLikeExample)));
	}

	@SystemOptionAnnotation(describe = "收藏点赞列表数据")
	@ApiOperation(value = "收藏点赞列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
		MemberCollectionLikeExample memberCollectionLikeExample = new MemberCollectionLikeExample();
		// 创造条件
		MemberCollectionLikeExample.Criteria memberCollectionLikeCriteria = memberCollectionLikeExample
				.createCriteria();
		memberCollectionLikeCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		memberCollectionLikeExample.setOrderByClause(StringUtil.getOrderByValue(sort));
		return super.responseEntity(BaseResult.ok(super.list(memberCollectionLikeExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存收藏点赞")
	@ApiOperation(value = "保存收藏点赞")
	@PostMapping(value = "save")
	@Authorization
	public ResponseEntity<BaseResult> save(@RequestBody @Valid MemberCollectionLikeDto memberCollectionLikeDto) {
		return super.responseEntity(memberCollectionLikeService.add(memberCollectionLikeDto, getLoginUser()));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "取消收藏点赞")
	@ApiOperation(value = "取消收藏点赞")
	@PostMapping(value = "cancel")
	@Authorization
	public ResponseEntity<BaseResult> cancel(@RequestBody @Valid MemberCollectionLikeDto memberCollectionLikeDto) {
		return super.responseEntity(memberCollectionLikeService.cancel(memberCollectionLikeDto, getLoginUser()));
	}

	@ApiOperation(value = "是否加入收藏点赞")
	@PostMapping(value = "isAdd")
	@Authorization
	public ResponseEntity<BaseResult> isAdd(
			@RequestBody @Valid MemberCollectionLikeIsAddDto memberCollectionLikeIsAddDto) {
		return super.responseEntity(memberCollectionLikeService.isAdd(memberCollectionLikeIsAddDto, getLoginUser()));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除收藏点赞")
	@ApiOperation(value = "删除收藏点赞")
	@PostMapping(value = "remove")
	@Authorization
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		MemberCollectionLikeExample memberCollectionLikeExample = new MemberCollectionLikeExample();
		memberCollectionLikeExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(memberCollectionLikeExample));
	}

}