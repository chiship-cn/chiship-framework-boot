package cn.chiship.framework.business.biz.business.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 示例（正式环境可删除）表单 2022/9/15
 *
 * @author LiJian
 */
@ApiModel(value = "示例（正式环境可删除）表单")
public class TestExampleDto {

	@ApiModelProperty(value = "名字", required = true)
	@NotEmpty(message = "名字")
	@Length(min = 1, max = 20)
	private String name;

	@ApiModelProperty(value = "代码", required = true)
	@NotEmpty(message = "代码")
	@Length(min = 1, max = 10)
	private String adCode;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdCode() {
		return adCode;
	}

	public void setAdCode(String adCode) {
		this.adCode = adCode;
	}

}