package cn.chiship.framework.business.biz.member.mapper;

import cn.chiship.framework.business.biz.member.entity.MemberComment;
import cn.chiship.framework.business.biz.member.entity.MemberCommentExample;

import cn.chiship.sdk.framework.base.BaseMapper;

import java.util.Map;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-11-22
 */
public interface MemberCommentMapper extends BaseMapper<MemberComment, MemberCommentExample> {

	/**
	 * 根据父级获取最大编码
	 * @param pid
	 * @return
	 */
	Map<String, String> getTreeNumberByPid(String pid);

}