package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotify;
import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotifyExample;
import cn.chiship.framework.business.biz.cashier.mapper.BusinessPayNotifyMapper;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessPayNotifyDto;
import cn.chiship.framework.business.biz.cashier.service.BusinessPayNotifyService;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 支付通知业务接口实现层 2024/6/10
 *
 * @author lijian
 */
@Service
public class BusinessPayNotifyServiceImpl extends BaseServiceImpl<BusinessPayNotify, BusinessPayNotifyExample>
		implements BusinessPayNotifyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BusinessPayNotifyServiceImpl.class);

	@Resource
	BusinessPayNotifyMapper businessPayNotifyMapper;

	@Override
	public BaseResult saveNotify(BusinessPayNotifyDto businessPayNotifyDto) {
		BusinessPayNotify businessPayNotify = new BusinessPayNotify();
		businessPayNotify.setNotifyDate(businessPayNotifyDto.getNotifyDate());
		businessPayNotify.setNotifyType(businessPayNotifyDto.getPayNotifyEnum().getType());
		businessPayNotify.setChannelPayWay(businessPayNotifyDto.getPayEnum().getType());
		businessPayNotify.setNotifyContent(businessPayNotifyDto.getNotifyContent());
		return super.insertSelective(businessPayNotify);
	}

}
