package cn.chiship.framework.business.biz.product.controller;


import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.util.http.RequestUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.product.service.ProductService;
import cn.chiship.framework.business.biz.product.entity.Product;
import cn.chiship.framework.business.biz.product.entity.ProductExample;
import cn.chiship.framework.business.biz.product.pojo.dto.ProductDto;
import cn.chiship.sdk.framework.pojo.vo.PageVo;
import cn.chiship.sdk.framework.util.ServletUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 商品控制层
 * 2024/12/11
 *
 * @author lijian
 */
@RestController
@RequestMapping("/product")
@Api(tags = "商品")
public class ProductController extends BaseController<Product, ProductExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

    @Resource
    private ProductService productService;

    @Override
    public BaseService getService() {
        return productService;
    }

    @SystemOptionAnnotation(describe = "商品分页")
    @ApiOperation(value = "商品分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sn", value = "SN码", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "isSale", value = "状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "categoryId", value = "品类", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/page")
    @Authorization
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, defaultValue = "", value = "name") String name,
                                           @RequestParam(required = false, defaultValue = "", value = "sn") String sn,
                                           @RequestParam(required = false, defaultValue = "", value = "categoryId") String categoryId,
                                           @RequestParam(required = false, defaultValue = "", value = "isSale") Byte isSale) {
        ProductExample productExample = new ProductExample();
        //创造条件
        ProductExample.Criteria productCriteria = productExample.createCriteria();
        productCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(name)) {
            productCriteria.andNameLike(name + "%");
        }
        if (!StringUtil.isNullOrEmpty(sn)) {
            productCriteria.andSnLike(sn + "%");
        }
        if (!StringUtil.isNullOrEmpty(categoryId)) {
            productCriteria.andCategoryIdEqualTo(categoryId);
        }
        if (!StringUtil.isNull(isSale)) {
            productCriteria.andIsSaleEqualTo(isSale);
        }
        PageVo pageVo = super.page(productExample);
        pageVo.setRecords(productService.assembleData(pageVo.getRecords()));
        return super.responseEntity(BaseResult.ok(pageVo));
    }

    @ApiOperation(value = "商品分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sn", value = "SN码", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "isSale", value = "状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "treeNumber", value = "品类", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/pageV2")
    @Authorization
    public ResponseEntity<BaseResult> pageV2(HttpServletRequest request) {
        PageVo pageVo = ServletUtil.getPageVo();
        String sort = pageVo.getSort();
        Map<String, String> paramsMap = RequestUtil.getParameterMap(request);
        paramsMap.put("sort", sort);
        return super.responseEntity(productService.page(pageVo, paramsMap));
    }


    @ApiOperation(value = "商品分页（会员专用）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "name", value = "名称", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sn", value = "SN码", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "treeNumber", value = "品类", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/pageV3")
    public ResponseEntity<BaseResult> pageV3(HttpServletRequest request) {
        PageVo pageVo = ServletUtil.getPageVo();
        String sort = pageVo.getSort();
        Map<String, String> paramsMap = RequestUtil.getParameterMap(request);
        paramsMap.put("sort", sort);
        paramsMap.put("isSale", "1");
        return super.responseEntity(productService.page(pageVo, paramsMap));
    }

    @SystemOptionAnnotation(describe = "商品列表数据")
    @ApiOperation(value = "商品列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified", dataTypeClass = String.class, paramType = "query"),
    })
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
                                           @RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
        ProductExample productExample = new ProductExample();
        //创造条件
        ProductExample.Criteria productCriteria = productExample.createCriteria();
        productCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        productExample.setOrderByClause(StringUtil.getOrderByValue(sort));
        return super.responseEntity(BaseResult.ok(super.list(productExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存商品")
    @ApiOperation(value = "保存商品")
    @PostMapping(value = "save")
    @Authorization
    public ResponseEntity<BaseResult> save(@RequestBody @Valid ProductDto productDto) {
        Product product = new Product();
        BeanUtils.copyProperties(productDto, product);
        return super.responseEntity(super.save(product));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新商品")
    @ApiOperation(value = "更新商品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "update/{id}")
    @Authorization
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id, @RequestBody @Valid ProductDto productDto) {
        Product product = new Product();
        BeanUtils.copyProperties(productDto, product);
        return super.responseEntity(super.update(id, product));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除商品")
    @ApiOperation(value = "删除商品")
    @PostMapping(value = "remove")
    @Authorization
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        ProductExample productExample = new ProductExample();
        productExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(productExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "商品上架")
    @ApiOperation(value = "商品上架")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "putShelves/{id}")
    @Authorization
    public ResponseEntity<BaseResult> putShelves(@PathVariable("id") String id) {
        return super.responseEntity(productService.putShelves(id));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "商品下架")
    @ApiOperation(value = "商品下架")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path", required = true)
    })
    @PostMapping(value = "removeShelves/{id}")
    @Authorization
    public ResponseEntity<BaseResult> removeShelves(@PathVariable("id") String id) {
        return super.responseEntity(productService.removeShelves(id));
    }
}