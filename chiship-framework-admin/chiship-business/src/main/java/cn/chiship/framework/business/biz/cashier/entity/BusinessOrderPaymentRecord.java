package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-23
 */
public class BusinessOrderPaymentRecord implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 订单号
	 */
	private String orderId;

	/**
	 * 交易号
	 */
	private String tradeNo;

	/**
	 * 交易方式
	 */
	private String tradeType;

	/**
	 * 渠道类型
	 */
	private String channelPayWay;

	/**
	 * 总费用
	 */
	private BigDecimal totalFee;

	/**
	 * 0 付款成功 1 付款失败
	 */
	private Byte payStatus;

	/**
	 * 商户号
	 */
	private String partnerId;

	/**
	 * 收款帐号
	 */
	private String account;

	/**
	 * 交易概述
	 */
	private String subject;

	/**
	 * 描述
	 */
	private String remark;

	/**
	 * 买家
	 */
	private String userId;

	/**
	 * 付款时间
	 */
	private Long payTime;

	/**
	 * 应用标识
	 */
	private String appId;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getTradeType() {
		return tradeType;
	}

	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

	public BigDecimal getTotalFee() {
		return totalFee;
	}

	public void setTotalFee(BigDecimal totalFee) {
		this.totalFee = totalFee;
	}

	public Byte getPayStatus() {
		return payStatus;
	}

	public void setPayStatus(Byte payStatus) {
		this.payStatus = payStatus;
	}

	public String getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getPayTime() {
		return payTime;
	}

	public void setPayTime(Long payTime) {
		this.payTime = payTime;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", orderId=").append(orderId);
		sb.append(", tradeNo=").append(tradeNo);
		sb.append(", tradeType=").append(tradeType);
		sb.append(", channelPayWay=").append(channelPayWay);
		sb.append(", totalFee=").append(totalFee);
		sb.append(", payStatus=").append(payStatus);
		sb.append(", partnerId=").append(partnerId);
		sb.append(", account=").append(account);
		sb.append(", subject=").append(subject);
		sb.append(", remark=").append(remark);
		sb.append(", userId=").append(userId);
		sb.append(", payTime=").append(payTime);
		sb.append(", appId=").append(appId);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		BusinessOrderPaymentRecord other = (BusinessOrderPaymentRecord) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getOrderId() == null ? other.getOrderId() == null
						: this.getOrderId().equals(other.getOrderId()))
				&& (this.getTradeNo() == null ? other.getTradeNo() == null
						: this.getTradeNo().equals(other.getTradeNo()))
				&& (this.getTradeType() == null ? other.getTradeType() == null
						: this.getTradeType().equals(other.getTradeType()))
				&& (this.getChannelPayWay() == null ? other.getChannelPayWay() == null
						: this.getChannelPayWay().equals(other.getChannelPayWay()))
				&& (this.getTotalFee() == null ? other.getTotalFee() == null
						: this.getTotalFee().equals(other.getTotalFee()))
				&& (this.getPayStatus() == null ? other.getPayStatus() == null
						: this.getPayStatus().equals(other.getPayStatus()))
				&& (this.getPartnerId() == null ? other.getPartnerId() == null
						: this.getPartnerId().equals(other.getPartnerId()))
				&& (this.getAccount() == null ? other.getAccount() == null
						: this.getAccount().equals(other.getAccount()))
				&& (this.getSubject() == null ? other.getSubject() == null
						: this.getSubject().equals(other.getSubject()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getPayTime() == null ? other.getPayTime() == null
						: this.getPayTime().equals(other.getPayTime()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
		result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
		result = prime * result + ((getTradeType() == null) ? 0 : getTradeType().hashCode());
		result = prime * result + ((getChannelPayWay() == null) ? 0 : getChannelPayWay().hashCode());
		result = prime * result + ((getTotalFee() == null) ? 0 : getTotalFee().hashCode());
		result = prime * result + ((getPayStatus() == null) ? 0 : getPayStatus().hashCode());
		result = prime * result + ((getPartnerId() == null) ? 0 : getPartnerId().hashCode());
		result = prime * result + ((getAccount() == null) ? 0 : getAccount().hashCode());
		result = prime * result + ((getSubject() == null) ? 0 : getSubject().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getPayTime() == null) ? 0 : getPayTime().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		return result;
	}

}