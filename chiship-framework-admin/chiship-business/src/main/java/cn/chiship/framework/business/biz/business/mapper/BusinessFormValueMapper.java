package cn.chiship.framework.business.biz.business.mapper;

import cn.chiship.framework.business.biz.business.entity.BusinessFormValue;
import cn.chiship.framework.business.biz.business.entity.BusinessFormValueExample;

import java.util.List;
import java.util.Map;

import cn.chiship.framework.business.biz.business.pojo.vo.BusinessFormValueVo;
import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-13
 */
public interface BusinessFormValueMapper extends BaseMapper<BusinessFormValue, BusinessFormValueExample> {


    /**
     * 查询详情
     *
     * @param params 参数
     * @return List<BusinessFormValueVo>
     */
    List<BusinessFormValueVo> selectBaseContentDetail(Map<String, String> params);

    List<BusinessFormValue> selectByExampleWithBLOBs(BusinessFormValueExample example);


    int updateByExampleWithBLOBs(@Param("record") BusinessFormValue record, @Param("example") BusinessFormValueExample example);


    int updateByPrimaryKeyWithBLOBs(BusinessFormValue record);

}