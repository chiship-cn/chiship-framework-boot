package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.framework.business.biz.member.enmus.MemberCommentModuleEnum;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 评论表表单 2024/11/22
 *
 * @author LiJian
 */
@ApiModel(value = "评论表表单")
public class MemberCommentDto {

	@ApiModelProperty(value = "父评论ID", required = true)
	@NotEmpty(message = "父评论ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String parentId;

	@ApiModelProperty(value = "模块枚举", required = true)
	@NotNull(message = "模块枚举" + BaseTipConstants.NOT_EMPTY)
	private MemberCommentModuleEnum moduleEnum;

	@ApiModelProperty(value = "模块对应的id", required = true)
	@NotEmpty(message = "模块对应的id" + BaseTipConstants.NOT_EMPTY)
	private String moduleId;

	@ApiModelProperty(value = "评论内容", required = true)
	@NotEmpty(message = "评论内容" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 1000)
	private String text;

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public MemberCommentModuleEnum getModuleEnum() {
		return moduleEnum;
	}

	public void setModuleEnum(MemberCommentModuleEnum moduleEnum) {
		this.moduleEnum = moduleEnum;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

}