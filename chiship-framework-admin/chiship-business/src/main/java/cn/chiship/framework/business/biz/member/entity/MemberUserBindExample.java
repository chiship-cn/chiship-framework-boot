package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-10-07
 */
public class MemberUserBindExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public MemberUserBindExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNull() {
			addCriterion("app_id is null");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNotNull() {
			addCriterion("app_id is not null");
			return (Criteria) this;
		}

		public Criteria andAppIdEqualTo(String value) {
			addCriterion("app_id =", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotEqualTo(String value) {
			addCriterion("app_id <>", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThan(String value) {
			addCriterion("app_id >", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThanOrEqualTo(String value) {
			addCriterion("app_id >=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThan(String value) {
			addCriterion("app_id <", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThanOrEqualTo(String value) {
			addCriterion("app_id <=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLike(String value) {
			addCriterion("app_id like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotLike(String value) {
			addCriterion("app_id not like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdIn(List<String> values) {
			addCriterion("app_id in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotIn(List<String> values) {
			addCriterion("app_id not in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdBetween(String value1, String value2) {
			addCriterion("app_id between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotBetween(String value1, String value2) {
			addCriterion("app_id not between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andBindKeyIsNull() {
			addCriterion("bind_key is null");
			return (Criteria) this;
		}

		public Criteria andBindKeyIsNotNull() {
			addCriterion("bind_key is not null");
			return (Criteria) this;
		}

		public Criteria andBindKeyEqualTo(String value) {
			addCriterion("bind_key =", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyNotEqualTo(String value) {
			addCriterion("bind_key <>", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyGreaterThan(String value) {
			addCriterion("bind_key >", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyGreaterThanOrEqualTo(String value) {
			addCriterion("bind_key >=", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyLessThan(String value) {
			addCriterion("bind_key <", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyLessThanOrEqualTo(String value) {
			addCriterion("bind_key <=", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyLike(String value) {
			addCriterion("bind_key like", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyNotLike(String value) {
			addCriterion("bind_key not like", value, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyIn(List<String> values) {
			addCriterion("bind_key in", values, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyNotIn(List<String> values) {
			addCriterion("bind_key not in", values, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyBetween(String value1, String value2) {
			addCriterion("bind_key between", value1, value2, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindKeyNotBetween(String value1, String value2) {
			addCriterion("bind_key not between", value1, value2, "bindKey");
			return (Criteria) this;
		}

		public Criteria andBindValueIsNull() {
			addCriterion("bind_value is null");
			return (Criteria) this;
		}

		public Criteria andBindValueIsNotNull() {
			addCriterion("bind_value is not null");
			return (Criteria) this;
		}

		public Criteria andBindValueEqualTo(String value) {
			addCriterion("bind_value =", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueNotEqualTo(String value) {
			addCriterion("bind_value <>", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueGreaterThan(String value) {
			addCriterion("bind_value >", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueGreaterThanOrEqualTo(String value) {
			addCriterion("bind_value >=", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueLessThan(String value) {
			addCriterion("bind_value <", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueLessThanOrEqualTo(String value) {
			addCriterion("bind_value <=", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueLike(String value) {
			addCriterion("bind_value like", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueNotLike(String value) {
			addCriterion("bind_value not like", value, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueIn(List<String> values) {
			addCriterion("bind_value in", values, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueNotIn(List<String> values) {
			addCriterion("bind_value not in", values, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueBetween(String value1, String value2) {
			addCriterion("bind_value between", value1, value2, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindValueNotBetween(String value1, String value2) {
			addCriterion("bind_value not between", value1, value2, "bindValue");
			return (Criteria) this;
		}

		public Criteria andBindNickNameIsNull() {
			addCriterion("bind_nick_name is null");
			return (Criteria) this;
		}

		public Criteria andBindNickNameIsNotNull() {
			addCriterion("bind_nick_name is not null");
			return (Criteria) this;
		}

		public Criteria andBindNickNameEqualTo(String value) {
			addCriterion("bind_nick_name =", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameNotEqualTo(String value) {
			addCriterion("bind_nick_name <>", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameGreaterThan(String value) {
			addCriterion("bind_nick_name >", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameGreaterThanOrEqualTo(String value) {
			addCriterion("bind_nick_name >=", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameLessThan(String value) {
			addCriterion("bind_nick_name <", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameLessThanOrEqualTo(String value) {
			addCriterion("bind_nick_name <=", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameLike(String value) {
			addCriterion("bind_nick_name like", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameNotLike(String value) {
			addCriterion("bind_nick_name not like", value, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameIn(List<String> values) {
			addCriterion("bind_nick_name in", values, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameNotIn(List<String> values) {
			addCriterion("bind_nick_name not in", values, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameBetween(String value1, String value2) {
			addCriterion("bind_nick_name between", value1, value2, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindNickNameNotBetween(String value1, String value2) {
			addCriterion("bind_nick_name not between", value1, value2, "bindNickName");
			return (Criteria) this;
		}

		public Criteria andBindAvatarIsNull() {
			addCriterion("bind_avatar is null");
			return (Criteria) this;
		}

		public Criteria andBindAvatarIsNotNull() {
			addCriterion("bind_avatar is not null");
			return (Criteria) this;
		}

		public Criteria andBindAvatarEqualTo(String value) {
			addCriterion("bind_avatar =", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarNotEqualTo(String value) {
			addCriterion("bind_avatar <>", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarGreaterThan(String value) {
			addCriterion("bind_avatar >", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarGreaterThanOrEqualTo(String value) {
			addCriterion("bind_avatar >=", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarLessThan(String value) {
			addCriterion("bind_avatar <", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarLessThanOrEqualTo(String value) {
			addCriterion("bind_avatar <=", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarLike(String value) {
			addCriterion("bind_avatar like", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarNotLike(String value) {
			addCriterion("bind_avatar not like", value, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarIn(List<String> values) {
			addCriterion("bind_avatar in", values, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarNotIn(List<String> values) {
			addCriterion("bind_avatar not in", values, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarBetween(String value1, String value2) {
			addCriterion("bind_avatar between", value1, value2, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindAvatarNotBetween(String value1, String value2) {
			addCriterion("bind_avatar not between", value1, value2, "bindAvatar");
			return (Criteria) this;
		}

		public Criteria andBindTimeIsNull() {
			addCriterion("bind_time is null");
			return (Criteria) this;
		}

		public Criteria andBindTimeIsNotNull() {
			addCriterion("bind_time is not null");
			return (Criteria) this;
		}

		public Criteria andBindTimeEqualTo(Long value) {
			addCriterion("bind_time =", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeNotEqualTo(Long value) {
			addCriterion("bind_time <>", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeGreaterThan(Long value) {
			addCriterion("bind_time >", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("bind_time >=", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeLessThan(Long value) {
			addCriterion("bind_time <", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeLessThanOrEqualTo(Long value) {
			addCriterion("bind_time <=", value, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeIn(List<Long> values) {
			addCriterion("bind_time in", values, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeNotIn(List<Long> values) {
			addCriterion("bind_time not in", values, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeBetween(Long value1, Long value2) {
			addCriterion("bind_time between", value1, value2, "bindTime");
			return (Criteria) this;
		}

		public Criteria andBindTimeNotBetween(Long value1, Long value2) {
			addCriterion("bind_time not between", value1, value2, "bindTime");
			return (Criteria) this;
		}

		public Criteria andStatusIsNull() {
			addCriterion("status is null");
			return (Criteria) this;
		}

		public Criteria andStatusIsNotNull() {
			addCriterion("status is not null");
			return (Criteria) this;
		}

		public Criteria andStatusEqualTo(Byte value) {
			addCriterion("status =", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotEqualTo(Byte value) {
			addCriterion("status <>", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThan(Byte value) {
			addCriterion("status >", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("status >=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThan(Byte value) {
			addCriterion("status <", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusLessThanOrEqualTo(Byte value) {
			addCriterion("status <=", value, "status");
			return (Criteria) this;
		}

		public Criteria andStatusIn(List<Byte> values) {
			addCriterion("status in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotIn(List<Byte> values) {
			addCriterion("status not in", values, "status");
			return (Criteria) this;
		}

		public Criteria andStatusBetween(Byte value1, Byte value2) {
			addCriterion("status between", value1, value2, "status");
			return (Criteria) this;
		}

		public Criteria andStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("status not between", value1, value2, "status");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}