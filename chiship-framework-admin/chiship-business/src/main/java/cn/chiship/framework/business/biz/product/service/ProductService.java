package cn.chiship.framework.business.biz.product.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.product.entity.Product;
import cn.chiship.framework.business.biz.product.entity.ProductExample;
import cn.chiship.sdk.framework.pojo.vo.PageVo;

import java.util.Map;

/**
 * 商品业务接口层
 * 2024/12/11
 *
 * @author lijian
 */
public interface ProductService extends BaseService<Product, ProductExample> {
    /**
     * 分页
     *
     * @param pageVo    分页
     * @param paramsMap 参数
     * @return BaseResult
     */
    BaseResult page(PageVo pageVo, Map<String, String> paramsMap);

    /**
     * 上架
     *
     * @param id
     * @return
     */
    BaseResult putShelves(String id);

    /**
     * 下架
     *
     * @param id
     * @return
     */
    BaseResult removeShelves(String id);
}
