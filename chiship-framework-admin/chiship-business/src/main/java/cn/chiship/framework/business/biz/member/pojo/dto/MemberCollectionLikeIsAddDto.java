package cn.chiship.framework.business.biz.member.pojo.dto;

import cn.chiship.framework.business.biz.member.enmus.MemberCollectionLikeModuleEnum;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 检测是否加入收藏点赞表单 2024/11/21
 *
 * @author LiJian
 */
@ApiModel(value = "收藏点赞表单")
public class MemberCollectionLikeIsAddDto {

	@ApiModelProperty(value = "模块枚举", required = true)
	@NotNull(message = "模块枚举" + BaseTipConstants.NOT_EMPTY)
	private MemberCollectionLikeModuleEnum moduleEnum;

	@ApiModelProperty(value = "模块对应的id", required = true)
	@NotEmpty(message = "模块对应的id" + BaseTipConstants.NOT_EMPTY)
	private String moduleId;

	public MemberCollectionLikeModuleEnum getModuleEnum() {
		return moduleEnum;
	}

	public void setModuleEnum(MemberCollectionLikeModuleEnum moduleEnum) {
		this.moduleEnum = moduleEnum;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

}