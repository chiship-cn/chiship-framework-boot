package cn.chiship.framework.business.biz.member.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-07
 */
public class MemberUserBind implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 三方ID
	 */
	private String appId;

	/**
	 * 用户主键
	 */
	private String userId;

	/**
	 * 绑定键
	 */
	private String bindKey;

	/**
	 * 绑定值
	 */
	private String bindValue;

	/**
	 * 昵称
	 */
	private String bindNickName;

	/**
	 * 头像
	 */
	private String bindAvatar;

	/**
	 * 绑定时间
	 */
	private Long bindTime;

	/**
	 * 状态
	 */
	private Byte status;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBindKey() {
		return bindKey;
	}

	public void setBindKey(String bindKey) {
		this.bindKey = bindKey;
	}

	public String getBindValue() {
		return bindValue;
	}

	public void setBindValue(String bindValue) {
		this.bindValue = bindValue;
	}

	public String getBindNickName() {
		return bindNickName;
	}

	public void setBindNickName(String bindNickName) {
		this.bindNickName = bindNickName;
	}

	public String getBindAvatar() {
		return bindAvatar;
	}

	public void setBindAvatar(String bindAvatar) {
		this.bindAvatar = bindAvatar;
	}

	public Long getBindTime() {
		return bindTime;
	}

	public void setBindTime(Long bindTime) {
		this.bindTime = bindTime;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", appId=").append(appId);
		sb.append(", userId=").append(userId);
		sb.append(", bindKey=").append(bindKey);
		sb.append(", bindValue=").append(bindValue);
		sb.append(", bindNickName=").append(bindNickName);
		sb.append(", bindAvatar=").append(bindAvatar);
		sb.append(", bindTime=").append(bindTime);
		sb.append(", status=").append(status);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		MemberUserBind other = (MemberUserBind) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getBindKey() == null ? other.getBindKey() == null
						: this.getBindKey().equals(other.getBindKey()))
				&& (this.getBindValue() == null ? other.getBindValue() == null
						: this.getBindValue().equals(other.getBindValue()))
				&& (this.getBindNickName() == null ? other.getBindNickName() == null
						: this.getBindNickName().equals(other.getBindNickName()))
				&& (this.getBindAvatar() == null ? other.getBindAvatar() == null
						: this.getBindAvatar().equals(other.getBindAvatar()))
				&& (this.getBindTime() == null ? other.getBindTime() == null
						: this.getBindTime().equals(other.getBindTime()))
				&& (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getBindKey() == null) ? 0 : getBindKey().hashCode());
		result = prime * result + ((getBindValue() == null) ? 0 : getBindValue().hashCode());
		result = prime * result + ((getBindNickName() == null) ? 0 : getBindNickName().hashCode());
		result = prime * result + ((getBindAvatar() == null) ? 0 : getBindAvatar().hashCode());
		result = prime * result + ((getBindTime() == null) ? 0 : getBindTime().hashCode());
		result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
		return result;
	}

}