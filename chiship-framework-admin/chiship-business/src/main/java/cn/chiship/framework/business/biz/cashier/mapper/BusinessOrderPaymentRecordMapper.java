package cn.chiship.framework.business.biz.cashier.mapper;

import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecord;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecordExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2023-03-26
 */
public interface BusinessOrderPaymentRecordMapper
		extends BaseMapper<BusinessOrderPaymentRecord, BusinessOrderPaymentRecordExample> {

}
