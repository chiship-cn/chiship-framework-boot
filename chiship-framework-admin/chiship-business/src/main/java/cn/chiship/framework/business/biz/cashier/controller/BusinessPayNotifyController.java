package cn.chiship.framework.business.biz.cashier.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.cashier.service.BusinessPayNotifyService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotify;
import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotifyExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessPayNotifyDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 支付通知控制层 2024/7/23
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/businessPayNotify")
@Api(tags = "支付通知")
public class BusinessPayNotifyController extends BaseController<BusinessPayNotify, BusinessPayNotifyExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(BusinessPayNotifyController.class);

	@Resource
	private BusinessPayNotifyService businessPayNotifyService;

	@Override
	public BaseService getService() {
		return businessPayNotifyService;
	}

	@SystemOptionAnnotation(describe = "支付通知分页")
	@ApiOperation(value = "支付通知分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "notifyType", value = "通知类型", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "notifyDateStart", value = "通知开始时间", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "notifyDateEnd", value = "通知结束时间", dataTypeClass = Long.class,
					paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "notifyDateStart") Long notifyDateStart,
			@RequestParam(required = false, defaultValue = "", value = "notifyDateEnd") Long notifyDateEnd,
			@RequestParam(required = false, defaultValue = "", value = "notifyType") String notifyType) {
		BusinessPayNotifyExample businessPayNotifyExample = new BusinessPayNotifyExample();
		// 创造条件
		BusinessPayNotifyExample.Criteria businessPayNotifyCriteria = businessPayNotifyExample.createCriteria();
		businessPayNotifyCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
			businessPayNotifyCriteria.andNotifyContentLike("%\"" + keyword + "\"%");
		}
		if (!StringUtil.isNull(notifyDateStart)) {
			businessPayNotifyCriteria.andNotifyDateGreaterThanOrEqualTo(notifyDateStart);
		}
		if (!StringUtil.isNull(notifyDateEnd)) {
			businessPayNotifyCriteria.andNotifyDateLessThanOrEqualTo(notifyDateEnd);
		}
		if (!StringUtil.isNullOrEmpty(notifyType)) {
			businessPayNotifyCriteria.andNotifyTypeEqualTo(notifyType);
		}
		return super.responseEntity(BaseResult.ok(super.page(businessPayNotifyExample)));
	}

	@SystemOptionAnnotation(describe = "支付通知列表数据")
	@ApiOperation(value = "支付通知列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		BusinessPayNotifyExample businessPayNotifyExample = new BusinessPayNotifyExample();
		// 创造条件
		BusinessPayNotifyExample.Criteria businessPayNotifyCriteria = businessPayNotifyExample.createCriteria();
		businessPayNotifyCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		return super.responseEntity(BaseResult.ok(super.list(businessPayNotifyExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存支付通知")
	@ApiOperation(value = "保存支付通知")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid BusinessPayNotifyDto businessPayNotifyDto) {
		BusinessPayNotify businessPayNotify = new BusinessPayNotify();
		BeanUtils.copyProperties(businessPayNotifyDto, businessPayNotify);
		return super.responseEntity(super.save(businessPayNotify));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新支付通知")
	@ApiOperation(value = "更新支付通知")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid BusinessPayNotifyDto businessPayNotifyDto) {
		BusinessPayNotify businessPayNotify = new BusinessPayNotify();
		BeanUtils.copyProperties(businessPayNotifyDto, businessPayNotify);
		return super.responseEntity(super.update(id, businessPayNotify));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除支付通知")
	@ApiOperation(value = "删除支付通知")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		BusinessPayNotifyExample businessPayNotifyExample = new BusinessPayNotifyExample();
		businessPayNotifyExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(businessPayNotifyExample));
	}

}
