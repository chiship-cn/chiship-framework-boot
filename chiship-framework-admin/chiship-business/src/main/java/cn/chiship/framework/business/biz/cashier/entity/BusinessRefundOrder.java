package cn.chiship.framework.business.biz.cashier.entity;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-23
 */
public class BusinessRefundOrder implements Serializable {

	/**
	 * 退款单号
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 退款状态
	 */
	private Byte refundStatus;

	/**
	 * 订单号
	 */
	private String orderNo;

	/**
	 * 交易号
	 */
	private String tradeNo;

	/**
	 * 创建退款人员id
	 */
	private String createdBy;

	/**
	 * 创建退款人员名称
	 */
	private String createdName;

	/**
	 * 退款金额
	 */
	private BigDecimal refundTotal;

	/**
	 * 退款原因
	 */
	private String refundReason;

	/**
	 * 退款渠道
	 */
	private String channelPayWay;

	/**
	 * 实际退款金额
	 */
	private BigDecimal realRefundTotal;

	/**
	 * 退款流水号
	 */
	private String refundTradeNo;

	/**
	 * 返还途径
	 */
	private String receivedAccount;

	/**
	 * 退款完成日期
	 */
	private Long refundFinishTime;

	/**
	 * 是否通知
	 */
	private Byte isNotify;

	/**
	 * 用户id
	 */
	private String userId;

	/**
	 * 用户真实姓名
	 */
	private String userRealName;

	/**
	 * 用户手机号
	 */
	private String userPhone;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Byte getRefundStatus() {
		return refundStatus;
	}

	public void setRefundStatus(Byte refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedName() {
		return createdName;
	}

	public void setCreatedName(String createdName) {
		this.createdName = createdName;
	}

	public BigDecimal getRefundTotal() {
		return refundTotal;
	}

	public void setRefundTotal(BigDecimal refundTotal) {
		this.refundTotal = refundTotal;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	public String getChannelPayWay() {
		return channelPayWay;
	}

	public void setChannelPayWay(String channelPayWay) {
		this.channelPayWay = channelPayWay;
	}

	public BigDecimal getRealRefundTotal() {
		return realRefundTotal;
	}

	public void setRealRefundTotal(BigDecimal realRefundTotal) {
		this.realRefundTotal = realRefundTotal;
	}

	public String getRefundTradeNo() {
		return refundTradeNo;
	}

	public void setRefundTradeNo(String refundTradeNo) {
		this.refundTradeNo = refundTradeNo;
	}

	public String getReceivedAccount() {
		return receivedAccount;
	}

	public void setReceivedAccount(String receivedAccount) {
		this.receivedAccount = receivedAccount;
	}

	public Long getRefundFinishTime() {
		return refundFinishTime;
	}

	public void setRefundFinishTime(Long refundFinishTime) {
		this.refundFinishTime = refundFinishTime;
	}

	public Byte getIsNotify() {
		return isNotify;
	}

	public void setIsNotify(Byte isNotify) {
		this.isNotify = isNotify;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}

	public String getUserPhone() {
		return userPhone;
	}

	public void setUserPhone(String userPhone) {
		this.userPhone = userPhone;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", refundStatus=").append(refundStatus);
		sb.append(", orderNo=").append(orderNo);
		sb.append(", tradeNo=").append(tradeNo);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", createdName=").append(createdName);
		sb.append(", refundTotal=").append(refundTotal);
		sb.append(", refundReason=").append(refundReason);
		sb.append(", channelPayWay=").append(channelPayWay);
		sb.append(", realRefundTotal=").append(realRefundTotal);
		sb.append(", refundTradeNo=").append(refundTradeNo);
		sb.append(", receivedAccount=").append(receivedAccount);
		sb.append(", refundFinishTime=").append(refundFinishTime);
		sb.append(", isNotify=").append(isNotify);
		sb.append(", userId=").append(userId);
		sb.append(", userRealName=").append(userRealName);
		sb.append(", userPhone=").append(userPhone);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		BusinessRefundOrder other = (BusinessRefundOrder) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getRefundStatus() == null ? other.getRefundStatus() == null
						: this.getRefundStatus().equals(other.getRefundStatus()))
				&& (this.getOrderNo() == null ? other.getOrderNo() == null
						: this.getOrderNo().equals(other.getOrderNo()))
				&& (this.getTradeNo() == null ? other.getTradeNo() == null
						: this.getTradeNo().equals(other.getTradeNo()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getCreatedName() == null ? other.getCreatedName() == null
						: this.getCreatedName().equals(other.getCreatedName()))
				&& (this.getRefundTotal() == null ? other.getRefundTotal() == null
						: this.getRefundTotal().equals(other.getRefundTotal()))
				&& (this.getRefundReason() == null ? other.getRefundReason() == null
						: this.getRefundReason().equals(other.getRefundReason()))
				&& (this.getChannelPayWay() == null ? other.getChannelPayWay() == null
						: this.getChannelPayWay().equals(other.getChannelPayWay()))
				&& (this.getRealRefundTotal() == null ? other.getRealRefundTotal() == null
						: this.getRealRefundTotal().equals(other.getRealRefundTotal()))
				&& (this.getRefundTradeNo() == null ? other.getRefundTradeNo() == null
						: this.getRefundTradeNo().equals(other.getRefundTradeNo()))
				&& (this.getReceivedAccount() == null ? other.getReceivedAccount() == null
						: this.getReceivedAccount().equals(other.getReceivedAccount()))
				&& (this.getRefundFinishTime() == null ? other.getRefundFinishTime() == null
						: this.getRefundFinishTime().equals(other.getRefundFinishTime()))
				&& (this.getIsNotify() == null ? other.getIsNotify() == null
						: this.getIsNotify().equals(other.getIsNotify()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserRealName() == null ? other.getUserRealName() == null
						: this.getUserRealName().equals(other.getUserRealName()))
				&& (this.getUserPhone() == null ? other.getUserPhone() == null
						: this.getUserPhone().equals(other.getUserPhone()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getRefundStatus() == null) ? 0 : getRefundStatus().hashCode());
		result = prime * result + ((getOrderNo() == null) ? 0 : getOrderNo().hashCode());
		result = prime * result + ((getTradeNo() == null) ? 0 : getTradeNo().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getCreatedName() == null) ? 0 : getCreatedName().hashCode());
		result = prime * result + ((getRefundTotal() == null) ? 0 : getRefundTotal().hashCode());
		result = prime * result + ((getRefundReason() == null) ? 0 : getRefundReason().hashCode());
		result = prime * result + ((getChannelPayWay() == null) ? 0 : getChannelPayWay().hashCode());
		result = prime * result + ((getRealRefundTotal() == null) ? 0 : getRealRefundTotal().hashCode());
		result = prime * result + ((getRefundTradeNo() == null) ? 0 : getRefundTradeNo().hashCode());
		result = prime * result + ((getReceivedAccount() == null) ? 0 : getReceivedAccount().hashCode());
		result = prime * result + ((getRefundFinishTime() == null) ? 0 : getRefundFinishTime().hashCode());
		result = prime * result + ((getIsNotify() == null) ? 0 : getIsNotify().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserRealName() == null) ? 0 : getUserRealName().hashCode());
		result = prime * result + ((getUserPhone() == null) ? 0 : getUserPhone().hashCode());
		return result;
	}

}