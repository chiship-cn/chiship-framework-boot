package cn.chiship.framework.business.biz.product.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

/**
 * 商品表单
 * 2024/12/11
 *
 * @author LiJian
 */
@ApiModel(value = "商品表单")
public class ProductDto {
    @ApiModelProperty(value = "商品货号", required = true)
    @Length(max = 12, message = "商品货号" + BaseTipConstants.LENGTH_MIN_MAX)
    private String sn;

    @ApiModelProperty(value = "商品名称", required = true)
    @Length(max = 100, message = "商品名称" + BaseTipConstants.LENGTH_MIN_MAX)
    private String name;

    @ApiModelProperty(value = "商品品类", required = true)
    private String categoryId;

    @ApiModelProperty(value = "商品品牌")
    private String brandId;

    @ApiModelProperty(value = "商品缩略图", required = true)
    private String smallImage;

    @ApiModelProperty(value = "商品轮播图", required = true)
    @Length(max = 2000, message = "品牌名称" + BaseTipConstants.LENGTH_MIN_MAX)
    private String largeImage;

    @ApiModelProperty(value = "商品售价", required = true)
    private BigDecimal price;

    @ApiModelProperty(value = "商品成本价", required = true)
    private BigDecimal costPrice;

    @ApiModelProperty(value = "是否在售", required = true)
    @Min(0)
    @Max(1)
    private Byte isSale;

    @ApiModelProperty(value = "计量单位", required = true)
    @Length(max = 5, min = 1, message = "计量单位" + BaseTipConstants.LENGTH_MIN_MAX)
    private String unit;

    @ApiModelProperty(value = "商品库存", required = true)
    @Min(0)
    private Long totalInventory;

    @ApiModelProperty(value = "商品介绍")
    private String introduce;

    @ApiModelProperty(value = "图文描述", required = true)
    @Length(min = 1, message = "图文描述" + BaseTipConstants.LENGTH_MIN_MAX)
    private String imageText;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSmallImage() {
        return smallImage;
    }

    public void setSmallImage(String smallImage) {
        this.smallImage = smallImage;
    }

    public String getLargeImage() {
        return largeImage;
    }

    public void setLargeImage(String largeImage) {
        this.largeImage = largeImage;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(BigDecimal costPrice) {
        this.costPrice = costPrice;
    }

    public Byte getIsSale() {
        return isSale;
    }

    public void setIsSale(Byte isSale) {
        this.isSale = isSale;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Long getTotalInventory() {
        return totalInventory;
    }

    public void setTotalInventory(Long totalInventory) {
        this.totalInventory = totalInventory;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getImageText() {
        return imageText;
    }

    public void setImageText(String imageText) {
        this.imageText = imageText;
    }
}