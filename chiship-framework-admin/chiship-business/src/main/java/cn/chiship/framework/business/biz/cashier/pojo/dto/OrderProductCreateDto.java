package cn.chiship.framework.business.biz.cashier.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Min;

/**
 * @author lijian
 */
@ApiModel(value = "创建商品订单表单")
public class OrderProductCreateDto {

    @ApiModelProperty(value = "小程序或公众号唯一识别码")
    private String openId;

    @ApiModelProperty(value = "商品主键", required = true)
    private String productId;

    @ApiModelProperty(value = "商品数量", required = true)
    @Min(1)
    private Integer productNumber;

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }


    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductId() {
        return productId;
    }

    public Integer getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(Integer productNumber) {
        this.productNumber = productNumber;
    }
}
