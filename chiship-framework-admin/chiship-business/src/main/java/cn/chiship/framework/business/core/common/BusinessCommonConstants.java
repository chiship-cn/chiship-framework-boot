package cn.chiship.framework.business.core.common;

import cn.chiship.framework.common.constants.CommonConstants;

/**
 * @author lijian
 */
public class BusinessCommonConstants extends CommonConstants {

    /**
     * 组织部门类型
     */
    public static final Byte ORGANIZATION_INDEX = 2;

    /**
     * 组织单位类型
     */
    public static final Byte ORGANIZATION_UNIT = 1;

    // 导出导入数据表类型
    /**
     * 短信
     */
    public static final String EXPORT_IMPORT_CLASS_SMS_CODE = "smsCode";

    /**
     * 测试示例
     */
    public static final String EXPORT_IMPORT_CLASS_TEST_EXAMPLE = "testExample";

    /**
     * 表单数据
     */
    public static final String EXPORT_IMPORT_CLASS_FORM_VALUE = "businessFormValue";

    /**
     * 提现申请
     */
    public static final String EXPORT_IMPORT_CLASS_MEMBER_WITHDRAWAL = "memberUserWithdrawalApplication";

}
