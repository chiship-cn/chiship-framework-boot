package cn.chiship.framework.business.biz.cashier.service.impl;

import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessPayNotifyDto;
import cn.chiship.framework.business.biz.cashier.service.BusinessOrderHeaderService;
import cn.chiship.framework.business.biz.cashier.service.BusinessPayNotifyService;
import cn.chiship.sdk.cache.service.RedisService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 支付异步业务
 *
 * @author lijian
 */
@Component
public class AsyncCashierService {

	@Resource
	BusinessPayNotifyService businessPayNotifyService;

	@Resource
	BusinessOrderHeaderService businessOrderHeaderService;

	@Resource
	RedisService redisService;

	/**
	 * 保存支付通知
	 */
	@Async("asyncServiceExecutor")
	public void savePayNotify(BusinessPayNotifyDto businessPayNotifyDto) {
		businessPayNotifyService.saveNotify(businessPayNotifyDto);
	}

	/**
	 * 超时关闭订单
	 */
	@Async("asyncServiceExecutor")
	public void orderTimeoutClosed(String orderNo) {
		businessOrderHeaderService.closedOrder(orderNo);
	}

}
