package cn.chiship.framework.business.biz.business.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.annotation.RequiredPermissions;
import cn.chiship.sdk.core.annotation.RequiredRoles;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.enums.PermissionsEnum;
import cn.chiship.sdk.core.util.*;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.business.service.TestExampleService;
import cn.chiship.framework.business.biz.business.entity.TestExample;
import cn.chiship.framework.business.biz.business.entity.TestExampleExample;
import cn.chiship.framework.business.biz.business.pojo.dto.TestExampleDto;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 示例（正式环境可删除）控制层 2022/9/15
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/testExample")
@Api(tags = "示例（正式环境可删除）")
public class TestExampleController extends BaseController<TestExample, TestExampleExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestExampleController.class);

    @Resource
    private TestExampleService testExampleService;

    @Override
    public BaseService getService() {
        return testExampleService;
    }

    @ApiOperation(value = "测试是否拥有指定权限")
    @GetMapping("/permissions1")
    @RequiredRoles({"admin"})
    public ResponseEntity<BaseResult> permissions1() {
        return new ResponseEntity<>(BaseResult.ok(), HttpStatus.OK);
    }

    @ApiOperation(value = "测试是否拥有指定权限")
    @GetMapping("/permissions2")
    @RequiredPermissions(value = {"deleted", "updated"}, logical = PermissionsEnum.OR)
    public ResponseEntity<BaseResult> permissions2() {
        return new ResponseEntity<>(BaseResult.ok(), HttpStatus.OK);
    }

    @SystemOptionAnnotation(describe = "示例（正式环境可删除）分页")
    @ApiOperation("示例（正式环境可删除）分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", example = "1", required = true, dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", example = "10", required = true,
                    dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", example = "-gmtModified", dataTypeClass = String.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),})
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        TestExampleExample testExampleExample = new TestExampleExample();
        // 创造条件
        TestExampleExample.Criteria testExampleCriteria = testExampleExample.createCriteria();
        testExampleCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            testExampleCriteria.andNameLike(keyword + "%");

        }
        return super.responseEntity(BaseResult.ok(super.page(testExampleExample)));
    }

    @SystemOptionAnnotation(describe = "示例（正式环境可删除）列表数据")
    @ApiOperation("示例（正式环境可删除）列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),})
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        TestExampleExample testExampleExample = new TestExampleExample();
        // 创造条件
        TestExampleExample.Criteria testExampleCriteria = testExampleExample.createCriteria();
        testExampleCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            testExampleCriteria.andNameLike(keyword + "%");

        }
        return super.responseEntity(BaseResult.ok(super.list(testExampleExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存示例（正式环境可删除）")
    @ApiOperation("保存示例（正式环境可删除）")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid TestExampleDto testExampleDto) {
        TestExample testExample = new TestExample();
        BeanUtils.copyProperties(testExampleDto, testExample);
        return super.responseEntity(super.save(testExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新示例（正式环境可删除）")
    @ApiOperation("更新示例（正式环境可删除）")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
            required = true)})
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid TestExampleDto testExampleDto) {
        TestExample testExample = new TestExample();
        BeanUtils.copyProperties(testExampleDto, testExample);
        return super.responseEntity(super.update(id, testExample));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除示例（正式环境可删除）")
    @ApiOperation("删除示例（正式环境可删除）")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        TestExampleExample testExampleExample = new TestExampleExample();
        testExampleExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(testExampleExample));
    }

    @ApiOperation("生成PDF（正式环境可删除）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path")
    })
    @PostMapping(value = "generatePdf/{id}")
    public void generatePdf(@PathVariable("id") String id, HttpServletResponse response) throws Exception {
        testExampleService.generatePdf(id, response);

    }

    @ApiOperation("生成WORD（正式环境可删除）")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path")
    })
    @PostMapping(value = "generateWord/{id}")
    public void generateWord(@PathVariable("id") String id, HttpServletResponse response) throws Exception {
        testExampleService.generateWord(id, response);

    }
}
