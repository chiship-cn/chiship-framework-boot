package cn.chiship.framework.business.biz.business.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lj
 * @date 2022-09-15
 */
public class TestExampleExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public TestExampleExample() {
		oredCriteria = new ArrayList<>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andFirstLetterIsNull() {
			addCriterion("first_letter is null");
			return (Criteria) this;
		}

		public Criteria andFirstLetterIsNotNull() {
			addCriterion("first_letter is not null");
			return (Criteria) this;
		}

		public Criteria andFirstLetterEqualTo(String value) {
			addCriterion("first_letter =", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterNotEqualTo(String value) {
			addCriterion("first_letter <>", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterGreaterThan(String value) {
			addCriterion("first_letter >", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterGreaterThanOrEqualTo(String value) {
			addCriterion("first_letter >=", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterLessThan(String value) {
			addCriterion("first_letter <", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterLessThanOrEqualTo(String value) {
			addCriterion("first_letter <=", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterLike(String value) {
			addCriterion("first_letter like", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterNotLike(String value) {
			addCriterion("first_letter not like", value, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterIn(List<String> values) {
			addCriterion("first_letter in", values, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterNotIn(List<String> values) {
			addCriterion("first_letter not in", values, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterBetween(String value1, String value2) {
			addCriterion("first_letter between", value1, value2, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFirstLetterNotBetween(String value1, String value2) {
			addCriterion("first_letter not between", value1, value2, "firstLetter");
			return (Criteria) this;
		}

		public Criteria andFullPinyinIsNull() {
			addCriterion("full_pinyin is null");
			return (Criteria) this;
		}

		public Criteria andFullPinyinIsNotNull() {
			addCriterion("full_pinyin is not null");
			return (Criteria) this;
		}

		public Criteria andFullPinyinEqualTo(String value) {
			addCriterion("full_pinyin =", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinNotEqualTo(String value) {
			addCriterion("full_pinyin <>", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinGreaterThan(String value) {
			addCriterion("full_pinyin >", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinGreaterThanOrEqualTo(String value) {
			addCriterion("full_pinyin >=", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinLessThan(String value) {
			addCriterion("full_pinyin <", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinLessThanOrEqualTo(String value) {
			addCriterion("full_pinyin <=", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinLike(String value) {
			addCriterion("full_pinyin like", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinNotLike(String value) {
			addCriterion("full_pinyin not like", value, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinIn(List<String> values) {
			addCriterion("full_pinyin in", values, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinNotIn(List<String> values) {
			addCriterion("full_pinyin not in", values, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinBetween(String value1, String value2) {
			addCriterion("full_pinyin between", value1, value2, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andFullPinyinNotBetween(String value1, String value2) {
			addCriterion("full_pinyin not between", value1, value2, "fullPinyin");
			return (Criteria) this;
		}

		public Criteria andAdCodeIsNull() {
			addCriterion("ad_code is null");
			return (Criteria) this;
		}

		public Criteria andAdCodeIsNotNull() {
			addCriterion("ad_code is not null");
			return (Criteria) this;
		}

		public Criteria andAdCodeEqualTo(String value) {
			addCriterion("ad_code =", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeNotEqualTo(String value) {
			addCriterion("ad_code <>", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeGreaterThan(String value) {
			addCriterion("ad_code >", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeGreaterThanOrEqualTo(String value) {
			addCriterion("ad_code >=", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeLessThan(String value) {
			addCriterion("ad_code <", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeLessThanOrEqualTo(String value) {
			addCriterion("ad_code <=", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeLike(String value) {
			addCriterion("ad_code like", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeNotLike(String value) {
			addCriterion("ad_code not like", value, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeIn(List<String> values) {
			addCriterion("ad_code in", values, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeNotIn(List<String> values) {
			addCriterion("ad_code not in", values, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeBetween(String value1, String value2) {
			addCriterion("ad_code between", value1, value2, "adCode");
			return (Criteria) this;
		}

		public Criteria andAdCodeNotBetween(String value1, String value2) {
			addCriterion("ad_code not between", value1, value2, "adCode");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}