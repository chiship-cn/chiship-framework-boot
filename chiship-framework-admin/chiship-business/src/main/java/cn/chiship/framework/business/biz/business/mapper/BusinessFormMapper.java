package cn.chiship.framework.business.biz.business.mapper;

import cn.chiship.framework.business.biz.business.entity.BusinessForm;

import cn.chiship.framework.business.biz.business.entity.BusinessFormExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-13
 */
public interface BusinessFormMapper extends BaseMapper<BusinessForm, BusinessFormExample> {

    List<BusinessForm> selectByExampleWithBLOBs(BusinessFormExample example);


    int updateByExampleWithBLOBs(@Param("record") BusinessForm record, @Param("example") BusinessFormExample example);


    int updateByPrimaryKeyWithBLOBs(BusinessForm record);

}