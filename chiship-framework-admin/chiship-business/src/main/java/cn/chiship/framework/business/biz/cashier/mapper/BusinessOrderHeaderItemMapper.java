package cn.chiship.framework.business.biz.cashier.mapper;

import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItem;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderItemExample;


import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-12-16
 */
public interface BusinessOrderHeaderItemMapper extends BaseMapper<BusinessOrderHeaderItem, BusinessOrderHeaderItemExample> {
}