package cn.chiship.framework.business.biz.cashier.enums;

/**
 * 订单类型
 *
 * @author lijian
 */
public enum OrderTypeEnum {

    /**
     * 商品订单
     */
    ORDER_TYPE_PRODUCT(Byte.valueOf("1"), "商品订单"),

    /**
     * 充值订单
     */
    ORDER_TYPE_RECHARGE(Byte.valueOf("0"), "充值订单"),

    ;

    /**
     * 业务编号
     */
    private Byte type;

    /**
     * 业务描述
     */
    private String message;

    OrderTypeEnum(Byte type, String message) {
        this.type = type;
        this.message = message;
    }

    public Byte getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

}
