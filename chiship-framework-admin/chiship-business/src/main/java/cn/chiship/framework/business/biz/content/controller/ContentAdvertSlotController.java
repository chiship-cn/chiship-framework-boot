package cn.chiship.framework.business.biz.content.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.content.service.ContentAdvertSlotService;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlot;
import cn.chiship.framework.business.biz.content.entity.ContentAdvertSlotExample;
import cn.chiship.framework.business.biz.content.pojo.dto.ContentAdvertSlotDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 广告版位表控制层 2024/10/31
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/contentAdvertSlot")
@Api(tags = "广告版位表")
public class ContentAdvertSlotController extends BaseController<ContentAdvertSlot, ContentAdvertSlotExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ContentAdvertSlotController.class);

	@Resource
	private ContentAdvertSlotService contentAdvertSlotService;

	@Override
	public BaseService getService() {
		return contentAdvertSlotService;
	}

	@SystemOptionAnnotation(describe = "广告版位表分页")
	@ApiOperation(value = "广告版位表分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		ContentAdvertSlotExample contentAdvertSlotExample = new ContentAdvertSlotExample();
		// 创造条件
		ContentAdvertSlotExample.Criteria contentAdvertSlotCriteria = contentAdvertSlotExample.createCriteria();
		contentAdvertSlotCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
			contentAdvertSlotCriteria.andNameLike(keyword + "%");
		}
		return super.responseEntity(BaseResult.ok(super.page(contentAdvertSlotExample)));
	}

	@SystemOptionAnnotation(describe = "广告版位表列表数据")
	@ApiOperation(value = "广告版位表列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		ContentAdvertSlotExample contentAdvertSlotExample = new ContentAdvertSlotExample();
		// 创造条件
		ContentAdvertSlotExample.Criteria contentAdvertSlotCriteria = contentAdvertSlotExample.createCriteria();
		contentAdvertSlotCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		return super.responseEntity(BaseResult.ok(super.list(contentAdvertSlotExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存广告版位表")
	@ApiOperation(value = "保存广告版位表")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid ContentAdvertSlotDto contentAdvertSlotDto) {
		ContentAdvertSlot contentAdvertSlot = new ContentAdvertSlot();
		BeanUtils.copyProperties(contentAdvertSlotDto, contentAdvertSlot);
		return super.responseEntity(super.save(contentAdvertSlot));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新广告版位表")
	@ApiOperation(value = "更新广告版位表")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ContentAdvertSlotDto contentAdvertSlotDto) {
		ContentAdvertSlot contentAdvertSlot = new ContentAdvertSlot();
		BeanUtils.copyProperties(contentAdvertSlotDto, contentAdvertSlot);
		return super.responseEntity(super.update(id, contentAdvertSlot));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除广告版位表")
	@ApiOperation(value = "删除广告版位表")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		ContentAdvertSlotExample contentAdvertSlotExample = new ContentAdvertSlotExample();
		contentAdvertSlotExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(contentAdvertSlotExample));
	}

}