package cn.chiship.framework.business.biz.content.mapper;

import cn.chiship.framework.business.biz.content.entity.ContentArticle;
import cn.chiship.framework.business.biz.content.entity.ContentArticleExample;

import java.util.List;
import java.util.Map;

import cn.chiship.framework.business.biz.content.pojo.vo.ContentArticleVo;
import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-10-19
 */
public interface ContentArticleMapper extends BaseMapper<ContentArticle, ContentArticleExample> {

    /**
     * 查询基本内容详情
     *
     * @param params 参数
     * @return List<ContentArticleVo>
     */
    List<ContentArticleVo> selectBaseContentDetail(Map<String, String> params);

    List<ContentArticle> selectByExampleWithBLOBs(ContentArticleExample example);

    int updateByExampleWithBLOBs(@Param("record") ContentArticle record,
                                 @Param("example") ContentArticleExample example);

    int updateByPrimaryKeyWithBLOBs(ContentArticle record);

    /**
     * 查询下一条文章
     *
     * @param id         文章ID
     * @param treeNumber 栏目ID
     * @param isAll      是否加载所有
     * @return 下一条文章
     */
    ContentArticle findNext(@Param("id") String id, @Param("treeNumber") String treeNumber, @Param("isAll") Boolean isAll);

    /**
     * 查询上一条文章
     *
     * @param id         文章ID
     * @param treeNumber 栏目ID
     * @param isAll      是否加载所有
     * @return 上一条文章
     */
    ContentArticle findPrev(@Param("id") String id, @Param("treeNumber") String treeNumber, @Param("isAll") Boolean isAll);

    /**
     * 时间维度统计文章数量
     *
     * @param dateTime 月份 格式如：2024-10
     * @return List<Map < String, Object>>
     */
    List<Map<String, Object>> analysisByTime(@Param("dateTime") String dateTime);

}