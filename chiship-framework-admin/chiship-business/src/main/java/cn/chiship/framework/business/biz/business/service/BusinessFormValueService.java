package cn.chiship.framework.business.biz.business.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.business.biz.business.entity.BusinessFormValue;
import cn.chiship.framework.business.biz.business.entity.BusinessFormValueExample;
import cn.chiship.sdk.framework.pojo.vo.PageVo;

import java.util.Map;


/**
 * 自封有表单提交数据业务接口层
 * 2024/12/13
 *
 * @author lijian
 */
public interface BusinessFormValueService extends BaseService<BusinessFormValue, BusinessFormValueExample> {
    /**
     * 分页
     *
     * @param pageVo    分页
     * @param paramsMap 参数
     * @return BaseResult
     */
    BaseResult page(PageVo pageVo, Map<String, String> paramsMap);
}
