package cn.chiship.framework.business.biz.content.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-10-30
 */
public class ContentArticle implements Serializable {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 修改时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 创建者
	 */
	private String createdBy;

	/**
	 * 更新者
	 */
	private String modifyBy;

	/**
	 * 创建者用户ID
	 */
	private String createdUserId;

	/**
	 * 创建者组织ID
	 */
	private String createdOrgId;

	/**
	 * 栏目
	 */
	private String categoryId;

	/**
	 * 主标题
	 */
	private String title;

	/**
	 * 简述
	 */
	private String summary;

	/**
	 * keyword关键字
	 */
	private String keywords;

	/**
	 * meta描述
	 */
	private String metaDescription;

	/**
	 * 是否置顶
	 */
	private Byte isTop;

	/**
	 * 状态 0:草稿 1:未发布 2:已发布 3:回收站
	 */
	private Byte status;

	/**
	 * 发布日期
	 */
	private Long publishDate;

	/**
	 * 是否原创（0：否，1：是）
	 */
	private Byte isOriginal;

	/**
	 * 来源
	 */
	private String source;

	/**
	 * 来源url
	 */
	private String sourceUrl;

	/**
	 * 作者
	 */
	private String author;

	/**
	 * 图片
	 */
	private String image1;

	/**
	 * 图片
	 */
	private String image2;

	/**
	 * 图片
	 */
	private String image3;

	/**
	 * 文件
	 */
	private String file;

	/**
	 * 是否允许评论
	 */
	private Byte isAllowComment;

	/**
	 * 浏览总数
	 */
	private Long views;

	/**
	 * 评论总数
	 */
	private Long comments;

	/**
	 * 点赞次数
	 */
	private Long thumbUpNum;

	/**
	 * 收藏次数
	 */
	private Long collections;

	/**
	 * 同步素材库 0 未同步 1 已同步
	 */
	private Byte syncMaterialLibrary;

	/**
	 * 正文
	 */
	private String content;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifyBy() {
		return modifyBy;
	}

	public void setModifyBy(String modifyBy) {
		this.modifyBy = modifyBy;
	}

	public String getCreatedUserId() {
		return createdUserId;
	}

	public void setCreatedUserId(String createdUserId) {
		this.createdUserId = createdUserId;
	}

	public String getCreatedOrgId() {
		return createdOrgId;
	}

	public void setCreatedOrgId(String createdOrgId) {
		this.createdOrgId = createdOrgId;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getMetaDescription() {
		return metaDescription;
	}

	public void setMetaDescription(String metaDescription) {
		this.metaDescription = metaDescription;
	}

	public Byte getIsTop() {
		return isTop;
	}

	public void setIsTop(Byte isTop) {
		this.isTop = isTop;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

	public Long getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Long publishDate) {
		this.publishDate = publishDate;
	}

	public Byte getIsOriginal() {
		return isOriginal;
	}

	public void setIsOriginal(Byte isOriginal) {
		this.isOriginal = isOriginal;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceUrl() {
		return sourceUrl;
	}

	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}

	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}

	public String getImage3() {
		return image3;
	}

	public void setImage3(String image3) {
		this.image3 = image3;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public Byte getIsAllowComment() {
		return isAllowComment;
	}

	public void setIsAllowComment(Byte isAllowComment) {
		this.isAllowComment = isAllowComment;
	}

	public Long getViews() {
		return views;
	}

	public void setViews(Long views) {
		this.views = views;
	}

	public Long getComments() {
		return comments;
	}

	public void setComments(Long comments) {
		this.comments = comments;
	}

	public Long getThumbUpNum() {
		return thumbUpNum;
	}

	public void setThumbUpNum(Long thumbUpNum) {
		this.thumbUpNum = thumbUpNum;
	}

	public Long getCollections() {
		return collections;
	}

	public void setCollections(Long collections) {
		this.collections = collections;
	}

	public Byte getSyncMaterialLibrary() {
		return syncMaterialLibrary;
	}

	public void setSyncMaterialLibrary(Byte syncMaterialLibrary) {
		this.syncMaterialLibrary = syncMaterialLibrary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", createdBy=").append(createdBy);
		sb.append(", modifyBy=").append(modifyBy);
		sb.append(", createdUserId=").append(createdUserId);
		sb.append(", createdOrgId=").append(createdOrgId);
		sb.append(", categoryId=").append(categoryId);
		sb.append(", title=").append(title);
		sb.append(", summary=").append(summary);
		sb.append(", keywords=").append(keywords);
		sb.append(", metaDescription=").append(metaDescription);
		sb.append(", isTop=").append(isTop);
		sb.append(", status=").append(status);
		sb.append(", publishDate=").append(publishDate);
		sb.append(", isOriginal=").append(isOriginal);
		sb.append(", source=").append(source);
		sb.append(", sourceUrl=").append(sourceUrl);
		sb.append(", author=").append(author);
		sb.append(", image1=").append(image1);
		sb.append(", image2=").append(image2);
		sb.append(", image3=").append(image3);
		sb.append(", file=").append(file);
		sb.append(", isAllowComment=").append(isAllowComment);
		sb.append(", views=").append(views);
		sb.append(", comments=").append(comments);
		sb.append(", thumbUpNum=").append(thumbUpNum);
		sb.append(", collections=").append(collections);
		sb.append(", syncMaterialLibrary=").append(syncMaterialLibrary);
		sb.append(", content=").append(content);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ContentArticle other = (ContentArticle) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCreatedBy() == null ? other.getCreatedBy() == null
						: this.getCreatedBy().equals(other.getCreatedBy()))
				&& (this.getModifyBy() == null ? other.getModifyBy() == null
						: this.getModifyBy().equals(other.getModifyBy()))
				&& (this.getCreatedUserId() == null ? other.getCreatedUserId() == null
						: this.getCreatedUserId().equals(other.getCreatedUserId()))
				&& (this.getCreatedOrgId() == null ? other.getCreatedOrgId() == null
						: this.getCreatedOrgId().equals(other.getCreatedOrgId()))
				&& (this.getCategoryId() == null ? other.getCategoryId() == null
						: this.getCategoryId().equals(other.getCategoryId()))
				&& (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
				&& (this.getSummary() == null ? other.getSummary() == null
						: this.getSummary().equals(other.getSummary()))
				&& (this.getKeywords() == null ? other.getKeywords() == null
						: this.getKeywords().equals(other.getKeywords()))
				&& (this.getMetaDescription() == null ? other.getMetaDescription() == null
						: this.getMetaDescription().equals(other.getMetaDescription()))
				&& (this.getIsTop() == null ? other.getIsTop() == null : this.getIsTop().equals(other.getIsTop()))
				&& (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(other.getStatus()))
				&& (this.getPublishDate() == null ? other.getPublishDate() == null
						: this.getPublishDate().equals(other.getPublishDate()))
				&& (this.getIsOriginal() == null ? other.getIsOriginal() == null
						: this.getIsOriginal().equals(other.getIsOriginal()))
				&& (this.getSource() == null ? other.getSource() == null : this.getSource().equals(other.getSource()))
				&& (this.getSourceUrl() == null ? other.getSourceUrl() == null
						: this.getSourceUrl().equals(other.getSourceUrl()))
				&& (this.getAuthor() == null ? other.getAuthor() == null : this.getAuthor().equals(other.getAuthor()))
				&& (this.getImage1() == null ? other.getImage1() == null : this.getImage1().equals(other.getImage1()))
				&& (this.getImage2() == null ? other.getImage2() == null : this.getImage2().equals(other.getImage2()))
				&& (this.getImage3() == null ? other.getImage3() == null : this.getImage3().equals(other.getImage3()))
				&& (this.getFile() == null ? other.getFile() == null : this.getFile().equals(other.getFile()))
				&& (this.getIsAllowComment() == null ? other.getIsAllowComment() == null
						: this.getIsAllowComment().equals(other.getIsAllowComment()))
				&& (this.getViews() == null ? other.getViews() == null : this.getViews().equals(other.getViews()))
				&& (this.getComments() == null ? other.getComments() == null
						: this.getComments().equals(other.getComments()))
				&& (this.getThumbUpNum() == null ? other.getThumbUpNum() == null
						: this.getThumbUpNum().equals(other.getThumbUpNum()))
				&& (this.getCollections() == null ? other.getCollections() == null
						: this.getCollections().equals(other.getCollections()))
				&& (this.getSyncMaterialLibrary() == null ? other.getSyncMaterialLibrary() == null
						: this.getSyncMaterialLibrary().equals(other.getSyncMaterialLibrary()))
				&& (this.getContent() == null ? other.getContent() == null
						: this.getContent().equals(other.getContent()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
		result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
		result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
		result = prime * result + ((getCreatedOrgId() == null) ? 0 : getCreatedOrgId().hashCode());
		result = prime * result + ((getCategoryId() == null) ? 0 : getCategoryId().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + ((getSummary() == null) ? 0 : getSummary().hashCode());
		result = prime * result + ((getKeywords() == null) ? 0 : getKeywords().hashCode());
		result = prime * result + ((getMetaDescription() == null) ? 0 : getMetaDescription().hashCode());
		result = prime * result + ((getIsTop() == null) ? 0 : getIsTop().hashCode());
		result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
		result = prime * result + ((getPublishDate() == null) ? 0 : getPublishDate().hashCode());
		result = prime * result + ((getIsOriginal() == null) ? 0 : getIsOriginal().hashCode());
		result = prime * result + ((getSource() == null) ? 0 : getSource().hashCode());
		result = prime * result + ((getSourceUrl() == null) ? 0 : getSourceUrl().hashCode());
		result = prime * result + ((getAuthor() == null) ? 0 : getAuthor().hashCode());
		result = prime * result + ((getImage1() == null) ? 0 : getImage1().hashCode());
		result = prime * result + ((getImage2() == null) ? 0 : getImage2().hashCode());
		result = prime * result + ((getImage3() == null) ? 0 : getImage3().hashCode());
		result = prime * result + ((getFile() == null) ? 0 : getFile().hashCode());
		result = prime * result + ((getIsAllowComment() == null) ? 0 : getIsAllowComment().hashCode());
		result = prime * result + ((getViews() == null) ? 0 : getViews().hashCode());
		result = prime * result + ((getComments() == null) ? 0 : getComments().hashCode());
		result = prime * result + ((getThumbUpNum() == null) ? 0 : getThumbUpNum().hashCode());
		result = prime * result + ((getCollections() == null) ? 0 : getCollections().hashCode());
		result = prime * result + ((getSyncMaterialLibrary() == null) ? 0 : getSyncMaterialLibrary().hashCode());
		result = prime * result + ((getContent() == null) ? 0 : getContent().hashCode());
		return result;
	}

}