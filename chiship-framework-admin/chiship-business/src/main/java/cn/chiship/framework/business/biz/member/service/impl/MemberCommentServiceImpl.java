package cn.chiship.framework.business.biz.member.service.impl;

import cn.chiship.framework.business.biz.base.service.impl.BusinessAsyncService;
import cn.chiship.framework.business.biz.member.pojo.dto.MemberCommentDto;
import cn.chiship.framework.common.util.FrameworkUtil2;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.ObjectUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.ip.AddressUtils;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.business.biz.member.mapper.MemberCommentMapper;
import cn.chiship.framework.business.biz.member.entity.MemberComment;
import cn.chiship.framework.business.biz.member.entity.MemberCommentExample;
import cn.chiship.framework.business.biz.member.service.MemberCommentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


/**
 * 评论表业务接口实现层 2024/11/22
 *
 * @author lijian
 */
@Service
public class MemberCommentServiceImpl extends BaseServiceImpl<MemberComment, MemberCommentExample>
		implements MemberCommentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemberCommentServiceImpl.class);

	@Resource
	MemberCommentMapper memberCommentMapper;

	@Resource
	BusinessAsyncService businessAsyncService;

	@Override
	public BaseResult create(MemberCommentDto memberCommentDto, CacheUserVO cacheUserVO, String ip) {
		String treeNumber = FrameworkUtil2
				.generateTreeNumber(memberCommentMapper.getTreeNumberByPid(memberCommentDto.getParentId()), 6);
		if (StringUtil.isNullOrEmpty(treeNumber)) {
			return BaseResult.error("生成层级出错？");
		}

		MemberComment memberComment = new MemberComment();

		String userName = cacheUserVO.getNickName();
		if (StringUtil.isNullOrEmpty(userName)) {
			userName = cacheUserVO.getMobile();
		}
		memberComment.setModuleId(memberCommentDto.getModuleId());
		memberComment.setModule(memberCommentDto.getModuleEnum().getType());
		memberComment.setCreatedUserId(cacheUserVO.getId());
		memberComment.setCreatedUserName(userName);
		memberComment.setCreatedUserAvatar(cacheUserVO.getAvatar());
		memberComment.setIp(ip);
		memberComment.setIpAddress(AddressUtils.getRealAddressByIp(ip));
		memberComment.setParentId(memberCommentDto.getParentId());
		memberComment.setText(memberCommentDto.getText());
		memberComment.setTreeNumber(treeNumber);
		super.insertSelective(memberComment);
		businessAsyncService.updateModuleComment(memberCommentDto);

		return BaseResult.ok(memberComment);
	}

	@Override
	public BaseResult changeStatus(String id, Byte status, CacheUserVO cacheUserVO) {
		MemberComment memberComment = selectByPrimaryKey(id);
		if (ObjectUtil.isEmpty(memberComment)) {
			return BaseResult.error("无效的评论");
		}
		memberComment.setAuditorId(cacheUserVO.getId());
		memberComment.setAuditDate(System.currentTimeMillis());
		memberComment.setStatus(status);
		return super.updateByPrimaryKeySelective(memberComment);
	}

}
