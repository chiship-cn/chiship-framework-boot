package cn.chiship.framework.business.biz.cashier.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.business.biz.cashier.service.BusinessOrderPaymentRecordService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecord;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderPaymentRecordExample;
import cn.chiship.framework.business.biz.cashier.pojo.dto.BusinessOrderPaymentRecordDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单支付控制层 2023/3/26
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/businessOrderPaymentRecord")
@Api(tags = "订单支付")
public class BusinessOrderPaymentRecordController
		extends BaseController<BusinessOrderPaymentRecord, BusinessOrderPaymentRecordExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(BusinessOrderPaymentRecordController.class);

	@Resource
	private BusinessOrderPaymentRecordService businessOrderPaymentRecordService;

	@Override
	public BaseService getService() {
		return businessOrderPaymentRecordService;
	}

	@SystemOptionAnnotation(describe = "订单支付分页")
	@ApiOperation(value = "订单支付分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
					dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "orderNo", value = "订单号", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "tradeNo", value = "交易号", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "payTimeStart", value = "支付开始时间", dataTypeClass = Long.class, paramType = "query"),
			@ApiImplicitParam(name = "payTimeEnd", value = "支付结束时间", dataTypeClass = Long.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "orderNo") String orderNo,
			@RequestParam(required = false, defaultValue = "", value = "tradeNo") String tradeNo,
			@RequestParam(required = false, defaultValue = "", value = "payTimeStart") Long payTimeStart,
			@RequestParam(required = false, defaultValue = "", value = "payTimeEnd") Long payTimeEnd) {
		BusinessOrderPaymentRecordExample businessOrderPaymentRecordExample = new BusinessOrderPaymentRecordExample();
		// 创造条件
		BusinessOrderPaymentRecordExample.Criteria businessOrderPaymentRecordCriteria = businessOrderPaymentRecordExample
				.createCriteria();
		businessOrderPaymentRecordCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(orderNo)) {
			businessOrderPaymentRecordCriteria.andOrderIdLike(orderNo + "%");
		}
		if (!StringUtil.isNullOrEmpty(tradeNo)) {
			businessOrderPaymentRecordCriteria.andTradeNoEqualTo(tradeNo + "%");
		}
		if (!StringUtil.isNull(payTimeStart)) {
			businessOrderPaymentRecordCriteria.andPayTimeGreaterThanOrEqualTo(payTimeStart);
		}
		if (!StringUtil.isNull(payTimeEnd)) {
			businessOrderPaymentRecordCriteria.andPayTimeLessThanOrEqualTo(payTimeEnd);
		}
		return super.responseEntity(BaseResult.ok(super.page(businessOrderPaymentRecordExample)));
	}

	@SystemOptionAnnotation(describe = "订单支付列表数据")
	@ApiOperation(value = "订单支付列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		BusinessOrderPaymentRecordExample businessOrderPaymentRecordExample = new BusinessOrderPaymentRecordExample();
		// 创造条件
		BusinessOrderPaymentRecordExample.Criteria businessOrderPaymentRecordCriteria = businessOrderPaymentRecordExample
				.createCriteria();
		businessOrderPaymentRecordCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		return super.responseEntity(BaseResult.ok(super.list(businessOrderPaymentRecordExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存订单支付")
	@ApiOperation(value = "保存订单支付")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(
			@RequestBody @Valid BusinessOrderPaymentRecordDto businessOrderPaymentRecordDto) {
		BusinessOrderPaymentRecord businessOrderPaymentRecord = new BusinessOrderPaymentRecord();
		BeanUtils.copyProperties(businessOrderPaymentRecordDto, businessOrderPaymentRecord);
		return super.responseEntity(super.save(businessOrderPaymentRecord));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新订单支付")
	@ApiOperation(value = "更新订单支付")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid BusinessOrderPaymentRecordDto businessOrderPaymentRecordDto) {
		BusinessOrderPaymentRecord businessOrderPaymentRecord = new BusinessOrderPaymentRecord();
		BeanUtils.copyProperties(businessOrderPaymentRecordDto, businessOrderPaymentRecord);
		return super.responseEntity(super.update(id, businessOrderPaymentRecord));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除订单支付")
	@ApiOperation(value = "删除订单支付")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody List<String> ids) {
		BusinessOrderPaymentRecordExample businessOrderPaymentRecordExample = new BusinessOrderPaymentRecordExample();
		businessOrderPaymentRecordExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(businessOrderPaymentRecordExample));
	}

}
