package cn.chiship.framework.business.biz.cashier.controller;

import cn.chiship.framework.business.biz.cashier.pojo.dto.OrderProductCreateDto;
import cn.chiship.framework.business.biz.cashier.pojo.dto.OrderRechargeCreateDto;
import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;
import javax.validation.Valid;

import cn.chiship.framework.business.biz.cashier.service.BusinessOrderHeaderService;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeader;
import cn.chiship.framework.business.biz.cashier.entity.BusinessOrderHeaderExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 订单主表控制层 2023/3/26
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/businessOrderHeader")
@Api(tags = "订单主表")
public class BusinessOrderHeaderController extends BaseController<BusinessOrderHeader, BusinessOrderHeaderExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessOrderHeaderController.class);

    @Resource
    private BusinessOrderHeaderService businessOrderHeaderService;

    @Override
    public BaseService getService() {
        return businessOrderHeaderService;
    }

    @SystemOptionAnnotation(describe = "订单主表分页")
    @ApiOperation(value = "订单主表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
                    dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
                    dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
                    dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "orderNo", value = "订单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "tradeNo", value = "交易号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "payTimeStart", value = "支付开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "payTimeEnd", value = "支付结束时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "orderStatus", value = "订单状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "orderType", value = "订单类型(0：充值订单 1：商品订单)", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "memberId", value = "会员主键", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(
            @RequestParam(required = false, defaultValue = "", value = "orderNo") String orderNo,
            @RequestParam(required = false, defaultValue = "", value = "tradeNo") String tradeNo,
            @RequestParam(required = false, defaultValue = "", value = "payTimeStart") Long payTimeStart,
            @RequestParam(required = false, defaultValue = "", value = "payTimeEnd") Long payTimeEnd,
            @RequestParam(required = false, defaultValue = "", value = "orderStatus") Byte orderStatus,
            @RequestParam(required = false, defaultValue = "", value = "orderType") Byte orderType,
            @RequestParam(required = false, defaultValue = "", value = "memberId") String memberId) {
        BusinessOrderHeaderExample businessOrderHeaderExample = new BusinessOrderHeaderExample();
        // 创造条件
        BusinessOrderHeaderExample.Criteria businessOrderHeaderCriteria = businessOrderHeaderExample.createCriteria();
        businessOrderHeaderCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(orderNo)) {
            businessOrderHeaderCriteria.andIdLike(orderNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(tradeNo)) {
            businessOrderHeaderCriteria.andTradeNoEqualTo(tradeNo + "%");
        }
        if (!StringUtil.isNull(payTimeStart)) {
            businessOrderHeaderCriteria.andPayTimeGreaterThanOrEqualTo(payTimeStart);
        }
        if (!StringUtil.isNull(payTimeEnd)) {
            businessOrderHeaderCriteria.andPayTimeLessThanOrEqualTo(payTimeEnd);
        }
        if (!StringUtil.isNull(orderStatus)) {
            businessOrderHeaderCriteria.andOrderStatusEqualTo(orderStatus);
        }
        if (!StringUtil.isNull(orderType)) {
            businessOrderHeaderCriteria.andOrderTypeEqualTo(orderType);
        }
        if (!StringUtil.isNullOrEmpty(memberId)) {
            businessOrderHeaderCriteria.andUserIdEqualTo(memberId);
        }
        return super.responseEntity(BaseResult.ok(super.page(businessOrderHeaderExample)));
    }

    @SystemOptionAnnotation(describe = "订单主表分页")
    @ApiOperation(value = "订单主表分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", required = true,
                    dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", required = true,
                    dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
                    dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "orderNo", value = "订单号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "tradeNo", value = "交易号", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "payTimeStart", value = "支付开始时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "payTimeEnd", value = "支付结束时间", dataTypeClass = Long.class, paramType = "query"),
            @ApiImplicitParam(name = "orderStatus", value = "订单状态", dataTypeClass = Byte.class, paramType = "query"),
            @ApiImplicitParam(name = "orderType", value = "订单类型(0：充值订单 1：商品订单)", dataTypeClass = Byte.class, paramType = "query"),

    })
    @GetMapping(value = "/current/page")
    public ResponseEntity<BaseResult> currentPage(
            @RequestParam(required = false, defaultValue = "", value = "orderNo") String orderNo,
            @RequestParam(required = false, defaultValue = "", value = "tradeNo") String tradeNo,
            @RequestParam(required = false, defaultValue = "", value = "payTimeStart") Long payTimeStart,
            @RequestParam(required = false, defaultValue = "", value = "payTimeEnd") Long payTimeEnd,
            @RequestParam(required = false, defaultValue = "", value = "orderStatus") Byte orderStatus,
            @RequestParam(required = false, defaultValue = "", value = "orderType") Byte orderType) {
        BusinessOrderHeaderExample businessOrderHeaderExample = new BusinessOrderHeaderExample();
        // 创造条件
        BusinessOrderHeaderExample.Criteria businessOrderHeaderCriteria = businessOrderHeaderExample.createCriteria();
        businessOrderHeaderCriteria.andIsDeletedEqualTo(BaseConstants.NO).andUserIdEqualTo(getUserId());
        if (!StringUtil.isNullOrEmpty(orderNo)) {
            businessOrderHeaderCriteria.andIdLike(orderNo + "%");
        }
        if (!StringUtil.isNullOrEmpty(tradeNo)) {
            businessOrderHeaderCriteria.andTradeNoEqualTo(tradeNo + "%");
        }
        if (!StringUtil.isNull(payTimeStart)) {
            businessOrderHeaderCriteria.andPayTimeGreaterThanOrEqualTo(payTimeStart);
        }
        if (!StringUtil.isNull(payTimeEnd)) {
            businessOrderHeaderCriteria.andPayTimeLessThanOrEqualTo(payTimeEnd);
        }
        if (!StringUtil.isNull(orderStatus)) {
            businessOrderHeaderCriteria.andOrderStatusEqualTo(orderStatus);
        }
        if (!StringUtil.isNull(orderType)) {
            businessOrderHeaderCriteria.andOrderTypeEqualTo(orderType);
        }
        return super.responseEntity(BaseResult.ok(super.page(businessOrderHeaderExample)));
    }

    @SystemOptionAnnotation(describe = "订单主表列表数据")
    @ApiOperation(value = "订单主表列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),})
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        BusinessOrderHeaderExample businessOrderHeaderExample = new BusinessOrderHeaderExample();
        // 创造条件
        BusinessOrderHeaderExample.Criteria businessOrderHeaderCriteria = businessOrderHeaderExample.createCriteria();
        businessOrderHeaderCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        return super.responseEntity(BaseResult.ok(super.list(businessOrderHeaderExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除订单主表")
    @ApiOperation(value = "删除订单主表")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
        BusinessOrderHeaderExample businessOrderHeaderExample = new BusinessOrderHeaderExample();
        businessOrderHeaderExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(businessOrderHeaderExample));
    }

    @ApiOperation(value = "创建充值订单")
    @PostMapping(value = "/createRechargeOrder")
    public ResponseEntity<BaseResult> createRechargeOrder(@RequestBody @Valid OrderRechargeCreateDto orderRechargeCreateDto) {
        return new ResponseEntity(businessOrderHeaderService.createOrder(orderRechargeCreateDto, getLoginUser()),
                HttpStatus.OK);
    }

    @ApiOperation(value = "创建商品订单")
    @PostMapping(value = "/createProductOrder")
    public ResponseEntity<BaseResult> createProductOrder(@RequestBody @Valid OrderProductCreateDto orderProductCreateDto) {
        return new ResponseEntity(businessOrderHeaderService.createOrder(orderProductCreateDto, getLoginUser()),
                HttpStatus.OK);
    }

}
