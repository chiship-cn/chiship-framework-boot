package cn.chiship.framework.business.biz.business.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-12-13
 */
public class BusinessFormValue implements Serializable {
    /**
     * 主键
     */
    private String id;

    /**
     * 创建时间
     */
    private Long gmtCreated;

    /**
     * 更新时间
     */
    private Long gmtModified;

    /**
     * 逻辑删除（0：否，1：是）
     */
    private Byte isDeleted;

    /**
     * 表单
     */
    private String formId;

    /**
     * 表单编号
     */
    private String formCode;

    /**
     * 创建者
     */
    private String createdBy;

    /**
     * 更新者
     */
    private String modifyBy;

    /**
     * 创建者用户ID
     */
    private String createdUserId;

    /**
     * 创建者组织ID
     */
    private String createdOrgId;

    /**
     * 创建者姓名
     */
    private String createdUserRealName;

    /**
     * 创建者手机
     */
    private String createdUserMobile;

    /**
     * 表单内容
     */
    private String formValue;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getGmtCreated() {
        return gmtCreated;
    }

    public void setGmtCreated(Long gmtCreated) {
        this.gmtCreated = gmtCreated;
    }

    public Long getGmtModified() {
        return gmtModified;
    }

    public void setGmtModified(Long gmtModified) {
        this.gmtModified = gmtModified;
    }

    public Byte getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Byte isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getFormId() {
        return formId;
    }

    public void setFormId(String formId) {
        this.formId = formId;
    }

    public String getFormCode() {
        return formCode;
    }

    public void setFormCode(String formCode) {
        this.formCode = formCode;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getModifyBy() {
        return modifyBy;
    }

    public void setModifyBy(String modifyBy) {
        this.modifyBy = modifyBy;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public String getCreatedOrgId() {
        return createdOrgId;
    }

    public void setCreatedOrgId(String createdOrgId) {
        this.createdOrgId = createdOrgId;
    }

    public String getCreatedUserRealName() {
        return createdUserRealName;
    }

    public void setCreatedUserRealName(String createdUserRealName) {
        this.createdUserRealName = createdUserRealName;
    }

    public String getCreatedUserMobile() {
        return createdUserMobile;
    }

    public void setCreatedUserMobile(String createdUserMobile) {
        this.createdUserMobile = createdUserMobile;
    }

    public String getFormValue() {
        return formValue;
    }

    public void setFormValue(String formValue) {
        this.formValue = formValue;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", gmtCreated=").append(gmtCreated);
        sb.append(", gmtModified=").append(gmtModified);
        sb.append(", isDeleted=").append(isDeleted);
        sb.append(", formId=").append(formId);
        sb.append(", formCode=").append(formCode);
        sb.append(", createdBy=").append(createdBy);
        sb.append(", modifyBy=").append(modifyBy);
        sb.append(", createdUserId=").append(createdUserId);
        sb.append(", createdOrgId=").append(createdOrgId);
        sb.append(", createdUserRealName=").append(createdUserRealName);
        sb.append(", createdUserMobile=").append(createdUserMobile);
        sb.append(", formValue=").append(formValue);
        sb.append("]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        BusinessFormValue other = (BusinessFormValue) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getGmtCreated() == null ? other.getGmtCreated() == null : this.getGmtCreated().equals(other.getGmtCreated()))
            && (this.getGmtModified() == null ? other.getGmtModified() == null : this.getGmtModified().equals(other.getGmtModified()))
            && (this.getIsDeleted() == null ? other.getIsDeleted() == null : this.getIsDeleted().equals(other.getIsDeleted()))
            && (this.getFormId() == null ? other.getFormId() == null : this.getFormId().equals(other.getFormId()))
            && (this.getFormCode() == null ? other.getFormCode() == null : this.getFormCode().equals(other.getFormCode()))
            && (this.getCreatedBy() == null ? other.getCreatedBy() == null : this.getCreatedBy().equals(other.getCreatedBy()))
            && (this.getModifyBy() == null ? other.getModifyBy() == null : this.getModifyBy().equals(other.getModifyBy()))
            && (this.getCreatedUserId() == null ? other.getCreatedUserId() == null : this.getCreatedUserId().equals(other.getCreatedUserId()))
            && (this.getCreatedOrgId() == null ? other.getCreatedOrgId() == null : this.getCreatedOrgId().equals(other.getCreatedOrgId()))
            && (this.getCreatedUserRealName() == null ? other.getCreatedUserRealName() == null : this.getCreatedUserRealName().equals(other.getCreatedUserRealName()))
            && (this.getCreatedUserMobile() == null ? other.getCreatedUserMobile() == null : this.getCreatedUserMobile().equals(other.getCreatedUserMobile()))
            && (this.getFormValue() == null ? other.getFormValue() == null : this.getFormValue().equals(other.getFormValue()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
        result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
        result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
        result = prime * result + ((getFormId() == null) ? 0 : getFormId().hashCode());
        result = prime * result + ((getFormCode() == null) ? 0 : getFormCode().hashCode());
        result = prime * result + ((getCreatedBy() == null) ? 0 : getCreatedBy().hashCode());
        result = prime * result + ((getModifyBy() == null) ? 0 : getModifyBy().hashCode());
        result = prime * result + ((getCreatedUserId() == null) ? 0 : getCreatedUserId().hashCode());
        result = prime * result + ((getCreatedOrgId() == null) ? 0 : getCreatedOrgId().hashCode());
        result = prime * result + ((getCreatedUserRealName() == null) ? 0 : getCreatedUserRealName().hashCode());
        result = prime * result + ((getCreatedUserMobile() == null) ? 0 : getCreatedUserMobile().hashCode());
        result = prime * result + ((getFormValue() == null) ? 0 : getFormValue().hashCode());
        return result;
    }
}