package cn.chiship.framework.business.core.task;

import cn.chiship.framework.common.annotation.JobAnnotation;
import org.springframework.stereotype.Component;

/**
 * @author lijian
 */
@Component
@JobAnnotation("示例任务1")
public class Example1Task {

    @JobAnnotation("有参方法")
    public void task1WithParams(String params) {
    }

    @JobAnnotation("无参方法")
    public void task1NoParams() {
    }

}
