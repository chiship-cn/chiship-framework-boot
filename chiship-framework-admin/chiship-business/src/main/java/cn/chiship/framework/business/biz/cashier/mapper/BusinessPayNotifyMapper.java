package cn.chiship.framework.business.biz.cashier.mapper;

import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotify;
import cn.chiship.framework.business.biz.cashier.entity.BusinessPayNotifyExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-07-23
 */
public interface BusinessPayNotifyMapper extends BaseMapper<BusinessPayNotify, BusinessPayNotifyExample> {

}
