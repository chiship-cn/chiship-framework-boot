@echo off
set port1=10020
for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":%port1%"') do (
    echo kill the process %%m who use the port1 
    taskkill /pid %%m -t -f
)
pause