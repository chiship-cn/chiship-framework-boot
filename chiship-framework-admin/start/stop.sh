#!/bin/bash

echo  "Application stop start............."

ID=`ps -ef | grep "chiship-business-3.1.jar" | grep -v "grep" | awk '{print $2}'`

echo $ID

for id in $ID
do

kill -9 $id

echo "killed $id"

done

sleep 5s

echo  "Application stop successful............."
