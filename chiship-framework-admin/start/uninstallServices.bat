@echo off
net.exe session 1>NUL 2>NUL && (
    goto as_admin
) || (
    goto not_admin
)

:as_admin
echo Service uninstall in progress.....
ChishipFrameWorkBootServices.exe uninstall
echo Service uninstall successful.....
goto end

:not_admin
echo Please run this bat file as an administrator in 'PowerSheel'!
goto end

:end
pause
