@echo off

if not "%JAVA_HOME%"=="" (
    chcp 65001 && title ChishipFrameWorkBoot && java -Dfile.encoding=utf-8 -Xms1024m -Xmx2048m -Xss1024K -XX:PermSize=128m -XX:-UseGCOverheadLimit -XX:MaxPermSize=1024m -jar chiship-business-3.1.jar
) else (
    echo Please config "JAVA_HOME"
)

:q