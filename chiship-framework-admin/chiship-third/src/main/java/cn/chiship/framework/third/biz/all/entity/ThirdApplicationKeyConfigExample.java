package cn.chiship.framework.third.biz.all.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-07-11
 */
public class ThirdApplicationKeyConfigExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ThirdApplicationKeyConfigExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andPhotoIsNull() {
			addCriterion("photo is null");
			return (Criteria) this;
		}

		public Criteria andPhotoIsNotNull() {
			addCriterion("photo is not null");
			return (Criteria) this;
		}

		public Criteria andPhotoEqualTo(String value) {
			addCriterion("photo =", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoNotEqualTo(String value) {
			addCriterion("photo <>", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoGreaterThan(String value) {
			addCriterion("photo >", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoGreaterThanOrEqualTo(String value) {
			addCriterion("photo >=", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoLessThan(String value) {
			addCriterion("photo <", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoLessThanOrEqualTo(String value) {
			addCriterion("photo <=", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoLike(String value) {
			addCriterion("photo like", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoNotLike(String value) {
			addCriterion("photo not like", value, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoIn(List<String> values) {
			addCriterion("photo in", values, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoNotIn(List<String> values) {
			addCriterion("photo not in", values, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoBetween(String value1, String value2) {
			addCriterion("photo between", value1, value2, "photo");
			return (Criteria) this;
		}

		public Criteria andPhotoNotBetween(String value1, String value2) {
			addCriterion("photo not between", value1, value2, "photo");
			return (Criteria) this;
		}

		public Criteria andNameIsNull() {
			addCriterion("name is null");
			return (Criteria) this;
		}

		public Criteria andNameIsNotNull() {
			addCriterion("name is not null");
			return (Criteria) this;
		}

		public Criteria andNameEqualTo(String value) {
			addCriterion("name =", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotEqualTo(String value) {
			addCriterion("name <>", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThan(String value) {
			addCriterion("name >", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameGreaterThanOrEqualTo(String value) {
			addCriterion("name >=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThan(String value) {
			addCriterion("name <", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLessThanOrEqualTo(String value) {
			addCriterion("name <=", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameLike(String value) {
			addCriterion("name like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotLike(String value) {
			addCriterion("name not like", value, "name");
			return (Criteria) this;
		}

		public Criteria andNameIn(List<String> values) {
			addCriterion("name in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotIn(List<String> values) {
			addCriterion("name not in", values, "name");
			return (Criteria) this;
		}

		public Criteria andNameBetween(String value1, String value2) {
			addCriterion("name between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andNameNotBetween(String value1, String value2) {
			addCriterion("name not between", value1, value2, "name");
			return (Criteria) this;
		}

		public Criteria andProjectIdIsNull() {
			addCriterion("project_id is null");
			return (Criteria) this;
		}

		public Criteria andProjectIdIsNotNull() {
			addCriterion("project_id is not null");
			return (Criteria) this;
		}

		public Criteria andProjectIdEqualTo(String value) {
			addCriterion("project_id =", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotEqualTo(String value) {
			addCriterion("project_id <>", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdGreaterThan(String value) {
			addCriterion("project_id >", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdGreaterThanOrEqualTo(String value) {
			addCriterion("project_id >=", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdLessThan(String value) {
			addCriterion("project_id <", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdLessThanOrEqualTo(String value) {
			addCriterion("project_id <=", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdLike(String value) {
			addCriterion("project_id like", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotLike(String value) {
			addCriterion("project_id not like", value, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdIn(List<String> values) {
			addCriterion("project_id in", values, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotIn(List<String> values) {
			addCriterion("project_id not in", values, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdBetween(String value1, String value2) {
			addCriterion("project_id between", value1, value2, "projectId");
			return (Criteria) this;
		}

		public Criteria andProjectIdNotBetween(String value1, String value2) {
			addCriterion("project_id not between", value1, value2, "projectId");
			return (Criteria) this;
		}

		public Criteria andIdentificationIsNull() {
			addCriterion("identification is null");
			return (Criteria) this;
		}

		public Criteria andIdentificationIsNotNull() {
			addCriterion("identification is not null");
			return (Criteria) this;
		}

		public Criteria andIdentificationEqualTo(String value) {
			addCriterion("identification =", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationNotEqualTo(String value) {
			addCriterion("identification <>", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationGreaterThan(String value) {
			addCriterion("identification >", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationGreaterThanOrEqualTo(String value) {
			addCriterion("identification >=", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationLessThan(String value) {
			addCriterion("identification <", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationLessThanOrEqualTo(String value) {
			addCriterion("identification <=", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationLike(String value) {
			addCriterion("identification like", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationNotLike(String value) {
			addCriterion("identification not like", value, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationIn(List<String> values) {
			addCriterion("identification in", values, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationNotIn(List<String> values) {
			addCriterion("identification not in", values, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationBetween(String value1, String value2) {
			addCriterion("identification between", value1, value2, "identification");
			return (Criteria) this;
		}

		public Criteria andIdentificationNotBetween(String value1, String value2) {
			addCriterion("identification not between", value1, value2, "identification");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNull() {
			addCriterion("app_id is null");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNotNull() {
			addCriterion("app_id is not null");
			return (Criteria) this;
		}

		public Criteria andAppIdEqualTo(String value) {
			addCriterion("app_id =", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotEqualTo(String value) {
			addCriterion("app_id <>", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThan(String value) {
			addCriterion("app_id >", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThanOrEqualTo(String value) {
			addCriterion("app_id >=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThan(String value) {
			addCriterion("app_id <", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThanOrEqualTo(String value) {
			addCriterion("app_id <=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLike(String value) {
			addCriterion("app_id like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotLike(String value) {
			addCriterion("app_id not like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdIn(List<String> values) {
			addCriterion("app_id in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotIn(List<String> values) {
			addCriterion("app_id not in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdBetween(String value1, String value2) {
			addCriterion("app_id between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotBetween(String value1, String value2) {
			addCriterion("app_id not between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppKeyIsNull() {
			addCriterion("app_key is null");
			return (Criteria) this;
		}

		public Criteria andAppKeyIsNotNull() {
			addCriterion("app_key is not null");
			return (Criteria) this;
		}

		public Criteria andAppKeyEqualTo(String value) {
			addCriterion("app_key =", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyNotEqualTo(String value) {
			addCriterion("app_key <>", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyGreaterThan(String value) {
			addCriterion("app_key >", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyGreaterThanOrEqualTo(String value) {
			addCriterion("app_key >=", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyLessThan(String value) {
			addCriterion("app_key <", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyLessThanOrEqualTo(String value) {
			addCriterion("app_key <=", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyLike(String value) {
			addCriterion("app_key like", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyNotLike(String value) {
			addCriterion("app_key not like", value, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyIn(List<String> values) {
			addCriterion("app_key in", values, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyNotIn(List<String> values) {
			addCriterion("app_key not in", values, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyBetween(String value1, String value2) {
			addCriterion("app_key between", value1, value2, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppKeyNotBetween(String value1, String value2) {
			addCriterion("app_key not between", value1, value2, "appKey");
			return (Criteria) this;
		}

		public Criteria andAppSecretIsNull() {
			addCriterion("app_secret is null");
			return (Criteria) this;
		}

		public Criteria andAppSecretIsNotNull() {
			addCriterion("app_secret is not null");
			return (Criteria) this;
		}

		public Criteria andAppSecretEqualTo(String value) {
			addCriterion("app_secret =", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretNotEqualTo(String value) {
			addCriterion("app_secret <>", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretGreaterThan(String value) {
			addCriterion("app_secret >", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretGreaterThanOrEqualTo(String value) {
			addCriterion("app_secret >=", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretLessThan(String value) {
			addCriterion("app_secret <", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretLessThanOrEqualTo(String value) {
			addCriterion("app_secret <=", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretLike(String value) {
			addCriterion("app_secret like", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretNotLike(String value) {
			addCriterion("app_secret not like", value, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretIn(List<String> values) {
			addCriterion("app_secret in", values, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretNotIn(List<String> values) {
			addCriterion("app_secret not in", values, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretBetween(String value1, String value2) {
			addCriterion("app_secret between", value1, value2, "appSecret");
			return (Criteria) this;
		}

		public Criteria andAppSecretNotBetween(String value1, String value2) {
			addCriterion("app_secret not between", value1, value2, "appSecret");
			return (Criteria) this;
		}

		public Criteria andServerUrlIsNull() {
			addCriterion("server_url is null");
			return (Criteria) this;
		}

		public Criteria andServerUrlIsNotNull() {
			addCriterion("server_url is not null");
			return (Criteria) this;
		}

		public Criteria andServerUrlEqualTo(String value) {
			addCriterion("server_url =", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlNotEqualTo(String value) {
			addCriterion("server_url <>", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlGreaterThan(String value) {
			addCriterion("server_url >", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlGreaterThanOrEqualTo(String value) {
			addCriterion("server_url >=", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlLessThan(String value) {
			addCriterion("server_url <", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlLessThanOrEqualTo(String value) {
			addCriterion("server_url <=", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlLike(String value) {
			addCriterion("server_url like", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlNotLike(String value) {
			addCriterion("server_url not like", value, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlIn(List<String> values) {
			addCriterion("server_url in", values, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlNotIn(List<String> values) {
			addCriterion("server_url not in", values, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlBetween(String value1, String value2) {
			addCriterion("server_url between", value1, value2, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerUrlNotBetween(String value1, String value2) {
			addCriterion("server_url not between", value1, value2, "serverUrl");
			return (Criteria) this;
		}

		public Criteria andServerTokenIsNull() {
			addCriterion("server_token is null");
			return (Criteria) this;
		}

		public Criteria andServerTokenIsNotNull() {
			addCriterion("server_token is not null");
			return (Criteria) this;
		}

		public Criteria andServerTokenEqualTo(String value) {
			addCriterion("server_token =", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenNotEqualTo(String value) {
			addCriterion("server_token <>", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenGreaterThan(String value) {
			addCriterion("server_token >", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenGreaterThanOrEqualTo(String value) {
			addCriterion("server_token >=", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenLessThan(String value) {
			addCriterion("server_token <", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenLessThanOrEqualTo(String value) {
			addCriterion("server_token <=", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenLike(String value) {
			addCriterion("server_token like", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenNotLike(String value) {
			addCriterion("server_token not like", value, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenIn(List<String> values) {
			addCriterion("server_token in", values, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenNotIn(List<String> values) {
			addCriterion("server_token not in", values, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenBetween(String value1, String value2) {
			addCriterion("server_token between", value1, value2, "serverToken");
			return (Criteria) this;
		}

		public Criteria andServerTokenNotBetween(String value1, String value2) {
			addCriterion("server_token not between", value1, value2, "serverToken");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andUrl1IsNull() {
			addCriterion("url1 is null");
			return (Criteria) this;
		}

		public Criteria andUrl1IsNotNull() {
			addCriterion("url1 is not null");
			return (Criteria) this;
		}

		public Criteria andUrl1EqualTo(String value) {
			addCriterion("url1 =", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1NotEqualTo(String value) {
			addCriterion("url1 <>", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1GreaterThan(String value) {
			addCriterion("url1 >", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1GreaterThanOrEqualTo(String value) {
			addCriterion("url1 >=", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1LessThan(String value) {
			addCriterion("url1 <", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1LessThanOrEqualTo(String value) {
			addCriterion("url1 <=", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1Like(String value) {
			addCriterion("url1 like", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1NotLike(String value) {
			addCriterion("url1 not like", value, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1In(List<String> values) {
			addCriterion("url1 in", values, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1NotIn(List<String> values) {
			addCriterion("url1 not in", values, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1Between(String value1, String value2) {
			addCriterion("url1 between", value1, value2, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl1NotBetween(String value1, String value2) {
			addCriterion("url1 not between", value1, value2, "url1");
			return (Criteria) this;
		}

		public Criteria andUrl2IsNull() {
			addCriterion("url2 is null");
			return (Criteria) this;
		}

		public Criteria andUrl2IsNotNull() {
			addCriterion("url2 is not null");
			return (Criteria) this;
		}

		public Criteria andUrl2EqualTo(String value) {
			addCriterion("url2 =", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2NotEqualTo(String value) {
			addCriterion("url2 <>", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2GreaterThan(String value) {
			addCriterion("url2 >", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2GreaterThanOrEqualTo(String value) {
			addCriterion("url2 >=", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2LessThan(String value) {
			addCriterion("url2 <", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2LessThanOrEqualTo(String value) {
			addCriterion("url2 <=", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2Like(String value) {
			addCriterion("url2 like", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2NotLike(String value) {
			addCriterion("url2 not like", value, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2In(List<String> values) {
			addCriterion("url2 in", values, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2NotIn(List<String> values) {
			addCriterion("url2 not in", values, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2Between(String value1, String value2) {
			addCriterion("url2 between", value1, value2, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl2NotBetween(String value1, String value2) {
			addCriterion("url2 not between", value1, value2, "url2");
			return (Criteria) this;
		}

		public Criteria andUrl3IsNull() {
			addCriterion("url3 is null");
			return (Criteria) this;
		}

		public Criteria andUrl3IsNotNull() {
			addCriterion("url3 is not null");
			return (Criteria) this;
		}

		public Criteria andUrl3EqualTo(String value) {
			addCriterion("url3 =", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3NotEqualTo(String value) {
			addCriterion("url3 <>", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3GreaterThan(String value) {
			addCriterion("url3 >", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3GreaterThanOrEqualTo(String value) {
			addCriterion("url3 >=", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3LessThan(String value) {
			addCriterion("url3 <", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3LessThanOrEqualTo(String value) {
			addCriterion("url3 <=", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3Like(String value) {
			addCriterion("url3 like", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3NotLike(String value) {
			addCriterion("url3 not like", value, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3In(List<String> values) {
			addCriterion("url3 in", values, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3NotIn(List<String> values) {
			addCriterion("url3 not in", values, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3Between(String value1, String value2) {
			addCriterion("url3 between", value1, value2, "url3");
			return (Criteria) this;
		}

		public Criteria andUrl3NotBetween(String value1, String value2) {
			addCriterion("url3 not between", value1, value2, "url3");
			return (Criteria) this;
		}

		public Criteria andValue1IsNull() {
			addCriterion("value1 is null");
			return (Criteria) this;
		}

		public Criteria andValue1IsNotNull() {
			addCriterion("value1 is not null");
			return (Criteria) this;
		}

		public Criteria andValue1EqualTo(String value) {
			addCriterion("value1 =", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1NotEqualTo(String value) {
			addCriterion("value1 <>", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1GreaterThan(String value) {
			addCriterion("value1 >", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1GreaterThanOrEqualTo(String value) {
			addCriterion("value1 >=", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1LessThan(String value) {
			addCriterion("value1 <", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1LessThanOrEqualTo(String value) {
			addCriterion("value1 <=", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1Like(String value) {
			addCriterion("value1 like", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1NotLike(String value) {
			addCriterion("value1 not like", value, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1In(List<String> values) {
			addCriterion("value1 in", values, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1NotIn(List<String> values) {
			addCriterion("value1 not in", values, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1Between(String value1, String value2) {
			addCriterion("value1 between", value1, value2, "value1");
			return (Criteria) this;
		}

		public Criteria andValue1NotBetween(String value1, String value2) {
			addCriterion("value1 not between", value1, value2, "value1");
			return (Criteria) this;
		}

		public Criteria andValue2IsNull() {
			addCriterion("value2 is null");
			return (Criteria) this;
		}

		public Criteria andValue2IsNotNull() {
			addCriterion("value2 is not null");
			return (Criteria) this;
		}

		public Criteria andValue2EqualTo(String value) {
			addCriterion("value2 =", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2NotEqualTo(String value) {
			addCriterion("value2 <>", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2GreaterThan(String value) {
			addCriterion("value2 >", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2GreaterThanOrEqualTo(String value) {
			addCriterion("value2 >=", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2LessThan(String value) {
			addCriterion("value2 <", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2LessThanOrEqualTo(String value) {
			addCriterion("value2 <=", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2Like(String value) {
			addCriterion("value2 like", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2NotLike(String value) {
			addCriterion("value2 not like", value, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2In(List<String> values) {
			addCriterion("value2 in", values, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2NotIn(List<String> values) {
			addCriterion("value2 not in", values, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2Between(String value1, String value2) {
			addCriterion("value2 between", value1, value2, "value2");
			return (Criteria) this;
		}

		public Criteria andValue2NotBetween(String value1, String value2) {
			addCriterion("value2 not between", value1, value2, "value2");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}