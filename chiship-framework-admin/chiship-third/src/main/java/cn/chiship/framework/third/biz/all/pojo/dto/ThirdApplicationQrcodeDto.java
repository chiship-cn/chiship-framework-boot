package cn.chiship.framework.third.biz.all.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 二维码表单 2024/11/25
 *
 * @author LiJian
 */
@ApiModel(value = "二维码表单")
public class ThirdApplicationQrcodeDto {

	@ApiModelProperty(value = "标题")
	@NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String title;

	@ApiModelProperty(value = "二维码类型（0 临时二维码  1 永久二维码）")
	@NotNull(message = "二维码类型" + BaseTipConstants.NOT_EMPTY)
	@Min(value = 0)
	@Max(value = 1)
	private Byte type;

	@ApiModelProperty(value = "超时时间")
	private Integer expireSeconds;

	@ApiModelProperty(value = "场景值")
	private String scene;

	@ApiModelProperty(value = "跳转页面")
	private String page;

	@ApiModelProperty(value = "扩展参数1")
	private String ext1;

	@ApiModelProperty(value = "扩展参数2")
	private String ext2;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Integer getExpireSeconds() {
		return expireSeconds;
	}

	public void setExpireSeconds(Integer expireSeconds) {
		this.expireSeconds = expireSeconds;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

}