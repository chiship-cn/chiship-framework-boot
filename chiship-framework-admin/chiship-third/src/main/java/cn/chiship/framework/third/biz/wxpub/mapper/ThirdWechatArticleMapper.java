package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatArticle;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatArticleExample;

import java.util.List;

import cn.chiship.sdk.framework.base.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author lijian
 */
public interface ThirdWechatArticleMapper extends BaseMapper<ThirdWechatArticle, ThirdWechatArticleExample> {

    /**
     * 查询大文本
     *
     * @param example
     * @return
     */
    List<ThirdWechatArticle> selectByExampleWithBLOBs(ThirdWechatArticleExample example);

    /**
     * 根据条件更新大文本
     *
     * @param record
     * @param example
     * @return
     */
    int updateByExampleWithBLOBs(@Param("record") ThirdWechatArticle record,
                                 @Param("example") ThirdWechatArticleExample example);

    /**
     * 更新大文本
     *
     * @param row
     * @return
     */
    int updateByPrimaryKeyWithBLOBs(ThirdWechatArticle row);


    /**
     * 查询下一条文章
     *
     * @param id         文章ID
     * @param categoryId 栏目ID
     * @param isAll  是否所有
     * @return 下一条文章
     */
    ThirdWechatArticle findNext(@Param("id") String id, @Param("categoryId") String categoryId, @Param("isAll") Boolean isAll);

    /**
     * 查询上一条文章
     *
     * @param id         文章ID
     * @param categoryId 栏目ID
     * @param isAll  是否所有
     * @return 上一条文章
     */
    ThirdWechatArticle findPrev(@Param("id") String id, @Param("categoryId") String categoryId, @Param("isAll") Boolean isAll);


}
