package cn.chiship.framework.third.core.runner;

import cn.chiship.framework.third.biz.all.service.ThirdCacheService;
import cn.chiship.sdk.cache.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化数据
 *
 * @author lijian
 */
@Component
@Order(1)
public class ThridDataRunner implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThridDataRunner.class);

	@Resource
	private RedisService redisService;

	@Resource
	private ThirdCacheService cacheService;

	@Override
	public void run(String... args) throws Exception {

		LOGGER.info("初始化配置数据开始！");
		cacheService.cacheConfig();
		LOGGER.info("初始化配置数据结束！");
	}

}
