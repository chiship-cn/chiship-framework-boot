package cn.chiship.framework.third.biz.all.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-11-26
 */
public class ThirdApplicationQrcodeExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ThirdApplicationQrcodeExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeIsNull() {
			addCriterion("application_type is null");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeIsNotNull() {
			addCriterion("application_type is not null");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeEqualTo(Byte value) {
			addCriterion("application_type =", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeNotEqualTo(Byte value) {
			addCriterion("application_type <>", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeGreaterThan(Byte value) {
			addCriterion("application_type >", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("application_type >=", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeLessThan(Byte value) {
			addCriterion("application_type <", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeLessThanOrEqualTo(Byte value) {
			addCriterion("application_type <=", value, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeIn(List<Byte> values) {
			addCriterion("application_type in", values, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeNotIn(List<Byte> values) {
			addCriterion("application_type not in", values, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeBetween(Byte value1, Byte value2) {
			addCriterion("application_type between", value1, value2, "applicationType");
			return (Criteria) this;
		}

		public Criteria andApplicationTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("application_type not between", value1, value2, "applicationType");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNull() {
			addCriterion("app_id is null");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNotNull() {
			addCriterion("app_id is not null");
			return (Criteria) this;
		}

		public Criteria andAppIdEqualTo(String value) {
			addCriterion("app_id =", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotEqualTo(String value) {
			addCriterion("app_id <>", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThan(String value) {
			addCriterion("app_id >", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThanOrEqualTo(String value) {
			addCriterion("app_id >=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThan(String value) {
			addCriterion("app_id <", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThanOrEqualTo(String value) {
			addCriterion("app_id <=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLike(String value) {
			addCriterion("app_id like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotLike(String value) {
			addCriterion("app_id not like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdIn(List<String> values) {
			addCriterion("app_id in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotIn(List<String> values) {
			addCriterion("app_id not in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdBetween(String value1, String value2) {
			addCriterion("app_id between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotBetween(String value1, String value2) {
			addCriterion("app_id not between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andTitleIsNull() {
			addCriterion("title is null");
			return (Criteria) this;
		}

		public Criteria andTitleIsNotNull() {
			addCriterion("title is not null");
			return (Criteria) this;
		}

		public Criteria andTitleEqualTo(String value) {
			addCriterion("title =", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotEqualTo(String value) {
			addCriterion("title <>", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThan(String value) {
			addCriterion("title >", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThanOrEqualTo(String value) {
			addCriterion("title >=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThan(String value) {
			addCriterion("title <", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThanOrEqualTo(String value) {
			addCriterion("title <=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLike(String value) {
			addCriterion("title like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotLike(String value) {
			addCriterion("title not like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleIn(List<String> values) {
			addCriterion("title in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotIn(List<String> values) {
			addCriterion("title not in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleBetween(String value1, String value2) {
			addCriterion("title between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotBetween(String value1, String value2) {
			addCriterion("title not between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTypeIsNull() {
			addCriterion("type is null");
			return (Criteria) this;
		}

		public Criteria andTypeIsNotNull() {
			addCriterion("type is not null");
			return (Criteria) this;
		}

		public Criteria andTypeEqualTo(Byte value) {
			addCriterion("type =", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotEqualTo(Byte value) {
			addCriterion("type <>", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThan(Byte value) {
			addCriterion("type >", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("type >=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThan(Byte value) {
			addCriterion("type <", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeLessThanOrEqualTo(Byte value) {
			addCriterion("type <=", value, "type");
			return (Criteria) this;
		}

		public Criteria andTypeIn(List<Byte> values) {
			addCriterion("type in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotIn(List<Byte> values) {
			addCriterion("type not in", values, "type");
			return (Criteria) this;
		}

		public Criteria andTypeBetween(Byte value1, Byte value2) {
			addCriterion("type between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("type not between", value1, value2, "type");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsIsNull() {
			addCriterion("expire_seconds is null");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsIsNotNull() {
			addCriterion("expire_seconds is not null");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsEqualTo(Integer value) {
			addCriterion("expire_seconds =", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsNotEqualTo(Integer value) {
			addCriterion("expire_seconds <>", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsGreaterThan(Integer value) {
			addCriterion("expire_seconds >", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsGreaterThanOrEqualTo(Integer value) {
			addCriterion("expire_seconds >=", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsLessThan(Integer value) {
			addCriterion("expire_seconds <", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsLessThanOrEqualTo(Integer value) {
			addCriterion("expire_seconds <=", value, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsIn(List<Integer> values) {
			addCriterion("expire_seconds in", values, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsNotIn(List<Integer> values) {
			addCriterion("expire_seconds not in", values, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsBetween(Integer value1, Integer value2) {
			addCriterion("expire_seconds between", value1, value2, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andExpireSecondsNotBetween(Integer value1, Integer value2) {
			addCriterion("expire_seconds not between", value1, value2, "expireSeconds");
			return (Criteria) this;
		}

		public Criteria andArticleIsNull() {
			addCriterion("article is null");
			return (Criteria) this;
		}

		public Criteria andArticleIsNotNull() {
			addCriterion("article is not null");
			return (Criteria) this;
		}

		public Criteria andArticleEqualTo(String value) {
			addCriterion("article =", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleNotEqualTo(String value) {
			addCriterion("article <>", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleGreaterThan(String value) {
			addCriterion("article >", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleGreaterThanOrEqualTo(String value) {
			addCriterion("article >=", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleLessThan(String value) {
			addCriterion("article <", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleLessThanOrEqualTo(String value) {
			addCriterion("article <=", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleLike(String value) {
			addCriterion("article like", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleNotLike(String value) {
			addCriterion("article not like", value, "article");
			return (Criteria) this;
		}

		public Criteria andArticleIn(List<String> values) {
			addCriterion("article in", values, "article");
			return (Criteria) this;
		}

		public Criteria andArticleNotIn(List<String> values) {
			addCriterion("article not in", values, "article");
			return (Criteria) this;
		}

		public Criteria andArticleBetween(String value1, String value2) {
			addCriterion("article between", value1, value2, "article");
			return (Criteria) this;
		}

		public Criteria andArticleNotBetween(String value1, String value2) {
			addCriterion("article not between", value1, value2, "article");
			return (Criteria) this;
		}

		public Criteria andArticleTypeIsNull() {
			addCriterion("article_type is null");
			return (Criteria) this;
		}

		public Criteria andArticleTypeIsNotNull() {
			addCriterion("article_type is not null");
			return (Criteria) this;
		}

		public Criteria andArticleTypeEqualTo(Byte value) {
			addCriterion("article_type =", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeNotEqualTo(Byte value) {
			addCriterion("article_type <>", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeGreaterThan(Byte value) {
			addCriterion("article_type >", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("article_type >=", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeLessThan(Byte value) {
			addCriterion("article_type <", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeLessThanOrEqualTo(Byte value) {
			addCriterion("article_type <=", value, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeIn(List<Byte> values) {
			addCriterion("article_type in", values, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeNotIn(List<Byte> values) {
			addCriterion("article_type not in", values, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeBetween(Byte value1, Byte value2) {
			addCriterion("article_type between", value1, value2, "articleType");
			return (Criteria) this;
		}

		public Criteria andArticleTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("article_type not between", value1, value2, "articleType");
			return (Criteria) this;
		}

		public Criteria andPageIsNull() {
			addCriterion("page is null");
			return (Criteria) this;
		}

		public Criteria andPageIsNotNull() {
			addCriterion("page is not null");
			return (Criteria) this;
		}

		public Criteria andPageEqualTo(String value) {
			addCriterion("page =", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageNotEqualTo(String value) {
			addCriterion("page <>", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageGreaterThan(String value) {
			addCriterion("page >", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageGreaterThanOrEqualTo(String value) {
			addCriterion("page >=", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageLessThan(String value) {
			addCriterion("page <", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageLessThanOrEqualTo(String value) {
			addCriterion("page <=", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageLike(String value) {
			addCriterion("page like", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageNotLike(String value) {
			addCriterion("page not like", value, "page");
			return (Criteria) this;
		}

		public Criteria andPageIn(List<String> values) {
			addCriterion("page in", values, "page");
			return (Criteria) this;
		}

		public Criteria andPageNotIn(List<String> values) {
			addCriterion("page not in", values, "page");
			return (Criteria) this;
		}

		public Criteria andPageBetween(String value1, String value2) {
			addCriterion("page between", value1, value2, "page");
			return (Criteria) this;
		}

		public Criteria andPageNotBetween(String value1, String value2) {
			addCriterion("page not between", value1, value2, "page");
			return (Criteria) this;
		}

		public Criteria andSceneIsNull() {
			addCriterion("scene is null");
			return (Criteria) this;
		}

		public Criteria andSceneIsNotNull() {
			addCriterion("scene is not null");
			return (Criteria) this;
		}

		public Criteria andSceneEqualTo(String value) {
			addCriterion("scene =", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneNotEqualTo(String value) {
			addCriterion("scene <>", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneGreaterThan(String value) {
			addCriterion("scene >", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneGreaterThanOrEqualTo(String value) {
			addCriterion("scene >=", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneLessThan(String value) {
			addCriterion("scene <", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneLessThanOrEqualTo(String value) {
			addCriterion("scene <=", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneLike(String value) {
			addCriterion("scene like", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneNotLike(String value) {
			addCriterion("scene not like", value, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneIn(List<String> values) {
			addCriterion("scene in", values, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneNotIn(List<String> values) {
			addCriterion("scene not in", values, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneBetween(String value1, String value2) {
			addCriterion("scene between", value1, value2, "scene");
			return (Criteria) this;
		}

		public Criteria andSceneNotBetween(String value1, String value2) {
			addCriterion("scene not between", value1, value2, "scene");
			return (Criteria) this;
		}

		public Criteria andExt1IsNull() {
			addCriterion("ext1 is null");
			return (Criteria) this;
		}

		public Criteria andExt1IsNotNull() {
			addCriterion("ext1 is not null");
			return (Criteria) this;
		}

		public Criteria andExt1EqualTo(String value) {
			addCriterion("ext1 =", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1NotEqualTo(String value) {
			addCriterion("ext1 <>", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1GreaterThan(String value) {
			addCriterion("ext1 >", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1GreaterThanOrEqualTo(String value) {
			addCriterion("ext1 >=", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1LessThan(String value) {
			addCriterion("ext1 <", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1LessThanOrEqualTo(String value) {
			addCriterion("ext1 <=", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1Like(String value) {
			addCriterion("ext1 like", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1NotLike(String value) {
			addCriterion("ext1 not like", value, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1In(List<String> values) {
			addCriterion("ext1 in", values, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1NotIn(List<String> values) {
			addCriterion("ext1 not in", values, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1Between(String value1, String value2) {
			addCriterion("ext1 between", value1, value2, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt1NotBetween(String value1, String value2) {
			addCriterion("ext1 not between", value1, value2, "ext1");
			return (Criteria) this;
		}

		public Criteria andExt2IsNull() {
			addCriterion("ext2 is null");
			return (Criteria) this;
		}

		public Criteria andExt2IsNotNull() {
			addCriterion("ext2 is not null");
			return (Criteria) this;
		}

		public Criteria andExt2EqualTo(String value) {
			addCriterion("ext2 =", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2NotEqualTo(String value) {
			addCriterion("ext2 <>", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2GreaterThan(String value) {
			addCriterion("ext2 >", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2GreaterThanOrEqualTo(String value) {
			addCriterion("ext2 >=", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2LessThan(String value) {
			addCriterion("ext2 <", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2LessThanOrEqualTo(String value) {
			addCriterion("ext2 <=", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2Like(String value) {
			addCriterion("ext2 like", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2NotLike(String value) {
			addCriterion("ext2 not like", value, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2In(List<String> values) {
			addCriterion("ext2 in", values, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2NotIn(List<String> values) {
			addCriterion("ext2 not in", values, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2Between(String value1, String value2) {
			addCriterion("ext2 between", value1, value2, "ext2");
			return (Criteria) this;
		}

		public Criteria andExt2NotBetween(String value1, String value2) {
			addCriterion("ext2 not between", value1, value2, "ext2");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}