package cn.chiship.framework.third.biz.wxpub.service.impl;

import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.third.biz.wxpub.mapper.ThirdWechatTagMapper;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTag;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTagExample;
import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatTagService;
import cn.chiship.sdk.third.service.WxPubService;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 微信用户标签业务接口实现层 2022/7/8
 *
 * @author lijian
 */
@Service
public class ThirdWechatTagServiceImpl extends BaseServiceImpl<ThirdWechatTag, ThirdWechatTagExample>
        implements ThirdWechatTagService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatTagServiceImpl.class);

    private static final int NUMBER_HUNDRED = 100;

    @Resource
    ThirdWechatTagMapper thirdWechatTagMapper;

    @Resource
    private GlobalCacheService globalCacheService;

    @Resource
    private WxPubService wxPubService;

    void wxPubServiceConfig() {
        wxPubService.config(globalCacheService.getWeiXinConfigByAppId(ThirdUtil.getAppId())).token();
    }

    @Override
    public BaseResult insertSelective(ThirdWechatTag thirdWechatTag) {
        ThirdWechatTagExample wechatTagExample = new ThirdWechatTagExample();
        ThirdWechatTagExample.Criteria criteria = wechatTagExample.createCriteria();
        criteria.andAppIdEqualTo(thirdWechatTag.getAppId());
        if (thirdWechatTagMapper.countByExample(wechatTagExample) >= NUMBER_HUNDRED) {
            return BaseResult.error("最多可以创建100个标签!");
        }
        criteria.andTagNameEqualTo(thirdWechatTag.getTagName());
        if (thirdWechatTagMapper.countByExample(wechatTagExample) > 0) {
            return BaseResult.error("标签称已存在,请重新输入!");
        }
        wxPubServiceConfig();
        BaseResult weChatCommonResult = wxPubService.createTag(thirdWechatTag.getTagName());
        if (!weChatCommonResult.isSuccess()) {
            return BaseResult.error(weChatCommonResult.getData());
        }
        JSONObject json = (JSONObject) weChatCommonResult.getData();
        thirdWechatTag.setTagId(json.getJSONObject("tag").getString("id"));
        return super.insertSelective(thirdWechatTag);
    }

    @Override
    public BaseResult updateByPrimaryKeySelective(ThirdWechatTag thirdWechatTag) {
        ThirdWechatTagExample wechatTagExample = new ThirdWechatTagExample();
        ThirdWechatTagExample.Criteria criteria = wechatTagExample.createCriteria();
        criteria.andAppIdEqualTo(thirdWechatTag.getAppId());
        criteria.andTagNameEqualTo(thirdWechatTag.getTagName());
        if (thirdWechatTagMapper.countByExample(wechatTagExample) > 0) {
            return BaseResult.error("标签称已存在,请重新输入!");
        }
        if (StringUtil.isNullOrEmpty(thirdWechatTag.getTagId())) {
            return BaseResult.error("请先同步创建用户标签!");
        }
        wxPubServiceConfig();
        BaseResult baseResult = wxPubService.updateTag(thirdWechatTag.getTagId(), thirdWechatTag.getTagName());
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        return super.updateByPrimaryKeySelective(thirdWechatTag);
    }

    @Override
    public BaseResult deleteByExample(ThirdWechatTagExample thirdWechatTagExample) {
        List<ThirdWechatTag> thirdWechatTags = thirdWechatTagMapper.selectByExample(thirdWechatTagExample);
        wxPubServiceConfig();
        for (ThirdWechatTag thirdWechatTag : thirdWechatTags) {
            wxPubService.deleteTag(thirdWechatTag.getTagId());
        }
        return super.deleteByExample(thirdWechatTagExample);
    }

    @Override
    public BaseResult syncSave(ThirdWechatTag thirdWechatTag) {
        wxPubServiceConfig();
        BaseResult baseResult = wxPubService.createTag(thirdWechatTag.getTagName());
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        JSONObject json = (JSONObject) baseResult.getData();

        thirdWechatTag.setTagId(json.getJSONObject("tag").getString("id"));
        return updateByPrimaryKeySelective(thirdWechatTag);
    }

    @Override
    public BaseResult syncTags() {
        ThirdWechatTagExample thirdWechatTagExample = new ThirdWechatTagExample();
        thirdWechatTagExample.createCriteria().andAppIdEqualTo(ThirdUtil.getAppId());
        thirdWechatTagMapper.deleteByExample(thirdWechatTagExample);
        wxPubServiceConfig();
        BaseResult baseResult = wxPubService.getTags();
        if (!baseResult.isSuccess()) {
            return baseResult;
        }
        JSONObject jsonResult = (JSONObject) baseResult.getData();
        JSONArray array = jsonResult.getJSONArray("tags");
        for (int i = 0; i < array.size(); i++) {
            JSONObject json = array.getJSONObject(i);
            ThirdWechatTag thirdWechatTag = new ThirdWechatTag();
            thirdWechatTag.setTagId(json.getString("id"));
            thirdWechatTag.setTagName(json.getString("name"));
            thirdWechatTag.setCount(json.getInteger("count"));
            thirdWechatTag.setAppId(ThirdUtil.getAppId());
            super.insertSelective(thirdWechatTag);
        }
        return BaseResult.ok();
    }

}
