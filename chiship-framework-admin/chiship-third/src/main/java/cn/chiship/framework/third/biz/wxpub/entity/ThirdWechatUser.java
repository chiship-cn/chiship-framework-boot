package cn.chiship.framework.third.biz.wxpub.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class ThirdWechatUser implements Serializable {

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	/**
	 * 微信用户ID
	 *
	 * @mbg.generated
	 */
	private String openId;

	/**
	 * 公众号ID
	 *
	 * @mbg.generated
	 */
	private String appId;

	/**
	 * 昵称
	 *
	 * @mbg.generated
	 */
	private String nickName;

	/**
	 * 性别
	 *
	 * @mbg.generated
	 */
	private String sex;

	private String language;

	private String province;

	private String city;

	private String country;

	private String headImageUrl;

	private Long subscripeTime;

	private Byte isCancle;

	private Long unSubscripeTime;

	private String location;

	private String remark;

	/**
	 * 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息
	 *
	 * @mbg.generated
	 */
	private Byte subscribe;

	private String unionid;

	private Integer groupId;

	private String tagIdList;

	/**
	 * 用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION
	 * 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_LINK
	 * 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_OTHERS 其他
	 *
	 * @mbg.generated
	 */
	private String subscribeScene;

	private String qrScene;

	private String rSceneStr;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getOpenId() {
		return openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHeadImageUrl() {
		return headImageUrl;
	}

	public void setHeadImageUrl(String headImageUrl) {
		this.headImageUrl = headImageUrl;
	}

	public Long getSubscripeTime() {
		return subscripeTime;
	}

	public void setSubscripeTime(Long subscripeTime) {
		this.subscripeTime = subscripeTime;
	}

	public Byte getIsCancle() {
		return isCancle;
	}

	public void setIsCancle(Byte isCancle) {
		this.isCancle = isCancle;
	}

	public Long getUnSubscripeTime() {
		return unSubscripeTime;
	}

	public void setUnSubscripeTime(Long unSubscripeTime) {
		this.unSubscripeTime = unSubscripeTime;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Byte getSubscribe() {
		return subscribe;
	}

	public void setSubscribe(Byte subscribe) {
		this.subscribe = subscribe;
	}

	public String getUnionid() {
		return unionid;
	}

	public void setUnionid(String unionid) {
		this.unionid = unionid;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getTagIdList() {
		return tagIdList;
	}

	public void setTagIdList(String tagIdList) {
		this.tagIdList = tagIdList;
	}

	public String getSubscribeScene() {
		return subscribeScene;
	}

	public void setSubscribeScene(String subscribeScene) {
		this.subscribeScene = subscribeScene;
	}

	public String getQrScene() {
		return qrScene;
	}

	public void setQrScene(String qrScene) {
		this.qrScene = qrScene;
	}

	public String getrSceneStr() {
		return rSceneStr;
	}

	public void setrSceneStr(String rSceneStr) {
		this.rSceneStr = rSceneStr;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", openId=").append(openId);
		sb.append(", appId=").append(appId);
		sb.append(", nickName=").append(nickName);
		sb.append(", sex=").append(sex);
		sb.append(", language=").append(language);
		sb.append(", province=").append(province);
		sb.append(", city=").append(city);
		sb.append(", country=").append(country);
		sb.append(", headImageUrl=").append(headImageUrl);
		sb.append(", subscripeTime=").append(subscripeTime);
		sb.append(", isCancle=").append(isCancle);
		sb.append(", unSubscripeTime=").append(unSubscripeTime);
		sb.append(", location=").append(location);
		sb.append(", remark=").append(remark);
		sb.append(", subscribe=").append(subscribe);
		sb.append(", unionid=").append(unionid);
		sb.append(", groupId=").append(groupId);
		sb.append(", tagIdList=").append(tagIdList);
		sb.append(", subscribeScene=").append(subscribeScene);
		sb.append(", qrScene=").append(qrScene);
		sb.append(", rSceneStr=").append(rSceneStr);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ThirdWechatUser other = (ThirdWechatUser) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getOpenId() == null ? other.getOpenId() == null : this.getOpenId().equals(other.getOpenId()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getNickName() == null ? other.getNickName() == null
						: this.getNickName().equals(other.getNickName()))
				&& (this.getSex() == null ? other.getSex() == null : this.getSex().equals(other.getSex()))
				&& (this.getLanguage() == null ? other.getLanguage() == null
						: this.getLanguage().equals(other.getLanguage()))
				&& (this.getProvince() == null ? other.getProvince() == null
						: this.getProvince().equals(other.getProvince()))
				&& (this.getCity() == null ? other.getCity() == null : this.getCity().equals(other.getCity()))
				&& (this.getCountry() == null ? other.getCountry() == null
						: this.getCountry().equals(other.getCountry()))
				&& (this.getHeadImageUrl() == null ? other.getHeadImageUrl() == null
						: this.getHeadImageUrl().equals(other.getHeadImageUrl()))
				&& (this.getSubscripeTime() == null ? other.getSubscripeTime() == null
						: this.getSubscripeTime().equals(other.getSubscripeTime()))
				&& (this.getIsCancle() == null ? other.getIsCancle() == null
						: this.getIsCancle().equals(other.getIsCancle()))
				&& (this.getUnSubscripeTime() == null ? other.getUnSubscripeTime() == null
						: this.getUnSubscripeTime().equals(other.getUnSubscripeTime()))
				&& (this.getLocation() == null ? other.getLocation() == null
						: this.getLocation().equals(other.getLocation()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
				&& (this.getSubscribe() == null ? other.getSubscribe() == null
						: this.getSubscribe().equals(other.getSubscribe()))
				&& (this.getUnionid() == null ? other.getUnionid() == null
						: this.getUnionid().equals(other.getUnionid()))
				&& (this.getGroupId() == null ? other.getGroupId() == null
						: this.getGroupId().equals(other.getGroupId()))
				&& (this.getTagIdList() == null ? other.getTagIdList() == null
						: this.getTagIdList().equals(other.getTagIdList()))
				&& (this.getSubscribeScene() == null ? other.getSubscribeScene() == null
						: this.getSubscribeScene().equals(other.getSubscribeScene()))
				&& (this.getQrScene() == null ? other.getQrScene() == null
						: this.getQrScene().equals(other.getQrScene()))
				&& (this.getrSceneStr() == null ? other.getrSceneStr() == null
						: this.getrSceneStr().equals(other.getrSceneStr()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getOpenId() == null) ? 0 : getOpenId().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getNickName() == null) ? 0 : getNickName().hashCode());
		result = prime * result + ((getSex() == null) ? 0 : getSex().hashCode());
		result = prime * result + ((getLanguage() == null) ? 0 : getLanguage().hashCode());
		result = prime * result + ((getProvince() == null) ? 0 : getProvince().hashCode());
		result = prime * result + ((getCity() == null) ? 0 : getCity().hashCode());
		result = prime * result + ((getCountry() == null) ? 0 : getCountry().hashCode());
		result = prime * result + ((getHeadImageUrl() == null) ? 0 : getHeadImageUrl().hashCode());
		result = prime * result + ((getSubscripeTime() == null) ? 0 : getSubscripeTime().hashCode());
		result = prime * result + ((getIsCancle() == null) ? 0 : getIsCancle().hashCode());
		result = prime * result + ((getUnSubscripeTime() == null) ? 0 : getUnSubscripeTime().hashCode());
		result = prime * result + ((getLocation() == null) ? 0 : getLocation().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		result = prime * result + ((getSubscribe() == null) ? 0 : getSubscribe().hashCode());
		result = prime * result + ((getUnionid() == null) ? 0 : getUnionid().hashCode());
		result = prime * result + ((getGroupId() == null) ? 0 : getGroupId().hashCode());
		result = prime * result + ((getTagIdList() == null) ? 0 : getTagIdList().hashCode());
		result = prime * result + ((getSubscribeScene() == null) ? 0 : getSubscribeScene().hashCode());
		result = prime * result + ((getQrScene() == null) ? 0 : getQrScene().hashCode());
		result = prime * result + ((getrSceneStr() == null) ? 0 : getrSceneStr().hashCode());
		return result;
	}

}