package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTag;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTagExample;

/**
 * 微信用户标签业务接口层 2022/7/8
 *
 * @author lijian
 */
public interface ThirdWechatTagService extends BaseService<ThirdWechatTag, ThirdWechatTagExample> {

	/**
	 * 同步创建
	 * @param thirdWechatTag
	 * @return
	 */
	BaseResult syncSave(ThirdWechatTag thirdWechatTag);

	/**
	 * 同步标签
	 * @return
	 */
	BaseResult syncTags();

}
