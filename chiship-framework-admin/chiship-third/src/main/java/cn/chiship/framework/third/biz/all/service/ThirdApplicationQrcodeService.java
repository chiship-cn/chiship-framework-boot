package cn.chiship.framework.third.biz.all.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcode;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcodeExample;

/**
 * 二维码业务接口层 2024/11/25
 *
 * @author lijian
 */
public interface ThirdApplicationQrcodeService
		extends BaseService<ThirdApplicationQrcode, ThirdApplicationQrcodeExample> {

	/**
	 * 生成二维码
	 * @param id
	 * @return
	 */
	BaseResult generateQrCode(String id);

}
