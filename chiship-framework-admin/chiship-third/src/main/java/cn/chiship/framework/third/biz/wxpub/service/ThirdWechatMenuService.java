package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenu;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenuExample;
import com.alibaba.fastjson.JSONObject;

import java.util.List;

/**
 * 微信菜单业务接口层 2022/6/19
 *
 * @author lijian
 */
public interface ThirdWechatMenuService extends BaseService<ThirdWechatMenu, ThirdWechatMenuExample> {

	/**
	 * 菜单树
	 * @param appId
	 * @return
	 */
	List<JSONObject> treeTable(String appId);

	/**
	 * 发布菜单
	 * @param appId
	 * @return
	 */
	BaseResult releaseMenu(String appId);

}
