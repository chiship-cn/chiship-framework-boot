package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTag;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTagExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface ThirdWechatTagMapper extends BaseMapper<ThirdWechatTag, ThirdWechatTagExample> {

}