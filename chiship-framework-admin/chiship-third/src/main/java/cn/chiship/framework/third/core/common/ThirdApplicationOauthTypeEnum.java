package cn.chiship.framework.third.core.common;

/**
 * @author lijian
 */

public enum ThirdApplicationOauthTypeEnum {

	/**
	 * 微信小程序
	 */
	APPLICATION_OAUTH_WX_MINI("wx_mini", "微信小程序"),
	/**
	 * 微信公众号
	 */
	APPLICATION_OAUTH_WX_PUB("wx_pub", "微信公众号"),
	/**
	 * 钉钉
	 */
	APPLICATION_OAUTH_DING_TALK("ding_talk", "钉钉小程序"),

	/**
	 * 支付宝小程序
	 */
	APPLICATION_OAUTH_ZFB_MINI("zfb_mini", "支付宝小程序"),

	/**
	 * 抖音程序
	 */
	APPLICATION_OAUTH_DY_MINI("dy_mini", "抖音小程序"),;

	private String type;

	private String desc;

	ThirdApplicationOauthTypeEnum(String type, String desc) {
		this.type = type;
		this.desc = desc;
	}

	public String getType() {
		return type;
	}

	public String getDesc() {
		return desc;
	}

}
