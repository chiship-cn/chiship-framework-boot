package cn.chiship.framework.third.biz.all.mapper;

import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfig;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfigExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface ThirdApplicationKeyConfigMapper
		extends BaseMapper<ThirdApplicationKeyConfig, ThirdApplicationKeyConfigExample> {

}