package cn.chiship.framework.third.biz.wxpub.service.impl;

import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.third.biz.wxpub.mapper.ThirdWechatResourceCategoryMapper;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategory;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategoryExample;
import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatResourceCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 * 微信资源分类业务接口实现层 2022/7/16
 *
 * @author lijian
 */
@Service
public class ThirdWechatResourceCategoryServiceImpl
		extends BaseServiceImpl<ThirdWechatResourceCategory, ThirdWechatResourceCategoryExample>
		implements ThirdWechatResourceCategoryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatResourceCategoryServiceImpl.class);

	@Resource
	ThirdWechatResourceCategoryMapper thirdWechatResourceCategoryMapper;

}
