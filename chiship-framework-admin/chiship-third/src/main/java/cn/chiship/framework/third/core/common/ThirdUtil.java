package cn.chiship.framework.third.core.common;

import cn.chiship.sdk.core.exception.custom.BusinessException;
import cn.chiship.sdk.core.util.PrintUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.util.http.RequestUtil;
import cn.chiship.sdk.framework.util.ServletUtil;

import java.util.Map;

/**
 * @author lijian
 */
public class ThirdUtil {

	/**
	 * 获取appId
	 */
	public static String getAppId() {
		/**
		 * get请求获取
		 */
		String appId = ServletUtil.getParameter(ThirdConstants.APP_ID);
		if (!StringUtil.isNullOrEmpty(appId)) {
			return appId;
		}
		try {
			/**
			 * Post请求获取
			 */
			Map<String, String> result = RequestUtil.getRequestBodyMap(ServletUtil.getRequest());
			if (!result.containsKey(ThirdConstants.APP_ID)) {
				throw new BusinessException("缺少参数【" + ThirdConstants.APP_ID + "】");
			}
			appId = result.get(ThirdConstants.APP_ID);
			if (!StringUtil.isNullOrEmpty(appId)) {
				return appId;
			}
		}
		catch (Exception e) {
			PrintUtil.console("获取appId参数失败了");
			throw new BusinessException(e.getLocalizedMessage());
		}
		throw new BusinessException("缺少参数【" + ThirdConstants.APP_ID + "】");
	}

}
