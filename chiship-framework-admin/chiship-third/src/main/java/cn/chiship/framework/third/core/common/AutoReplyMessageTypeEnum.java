package cn.chiship.framework.third.core.common;

/**
 * @author lj 自动回复消息类型
 */

public enum AutoReplyMessageTypeEnum {

	/**
	 * 关注时回复
	 */
	AUTO_REPLY_MESSAGE_TYPE_SUB(Byte.valueOf("0"), "关注时回复"),
	/**
	 * 消息回复
	 */
	AUTO_REPLY_MESSAGE_TYPE_NORMAL(Byte.valueOf("1"), "消息回复"),
	/**
	 * 关键词回复
	 */
	AUTO_REPLY_MESSAGE_TYPE_KEYWORD(Byte.valueOf("2"), "关键词回复"),

	/**
	 * 菜单回复
	 */
	AUTO_REPLY_MESSAGE_TYPE_MENU(Byte.valueOf("3"), "菜单"),;

	private Byte type;

	private String message;

	AutoReplyMessageTypeEnum(Byte type, String message) {
		this.type = type;
		this.message = message;
	}

	public Byte getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
