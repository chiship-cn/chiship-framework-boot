package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReply;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReplyExample;

/**
 * 自动回复业务接口层 2024/7/11
 *
 * @author lijian
 */
public interface ThirdWechatAutoReplyService extends BaseService<ThirdWechatAutoReply, ThirdWechatAutoReplyExample> {

}
