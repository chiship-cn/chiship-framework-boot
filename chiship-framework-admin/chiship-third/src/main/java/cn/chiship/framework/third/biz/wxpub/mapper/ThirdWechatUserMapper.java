package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatUser;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatUserExample;
import java.util.List;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface ThirdWechatUserMapper extends BaseMapper<ThirdWechatUser, ThirdWechatUserExample> {

	/**
	 * 批量插入
	 * @param wechatUsers
	 * @return
	 */
	int insertSelectiveBatch(List<ThirdWechatUser> wechatUsers);

}