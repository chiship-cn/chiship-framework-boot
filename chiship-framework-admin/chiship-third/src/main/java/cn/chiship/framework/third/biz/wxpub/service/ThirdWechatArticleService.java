package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.framework.third.biz.wxpub.pojo.dto.BasicMaterialsDto;
import cn.chiship.framework.third.core.enums.ThirdContentStatusEnum;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatArticle;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatArticleExample;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 文章素材业务接口层 2022/7/13
 *
 * @author lijian
 */
public interface ThirdWechatArticleService extends BaseService<ThirdWechatArticle, ThirdWechatArticleExample> {

    /**
     * 保存临时素材
     *
     * @param basicMaterialsDto
     * @param file
     * @param cacheUserVO
     * @return
     */
    BaseResult saveBasic(BasicMaterialsDto basicMaterialsDto, MultipartFile file, CacheUserVO cacheUserVO);

    /**
     * 修改文章状态
     *
     * @param ids                    主键集合
     * @param thirdContentStatusEnum 状态
     * @return BaseResult
     */
    BaseResult changeStatus(List<String> ids, ThirdContentStatusEnum thirdContentStatusEnum);

    /**
     * 移入回收站
     *
     * @param ids 主键集合
     * @return BaseResult
     */
    BaseResult moveRecycleBin(List<String> ids);

    /**
     * 回收站恢复
     *
     * @param ids 主键集合
     * @return BaseResult
     */
    BaseResult recovery(List<String> ids);

    /**
     * 文章预览
     *
     * @param id          主键
     * @param cacheUserVO 用户
     * @return BaseResult
     */
    BaseResult view(HttpServletRequest request, String id, CacheUserVO cacheUserVO);

    /**
     * 上一篇及下一篇
     *
     * @param id         主键
     * @param categoryId 分类频道 all：所有频道
     * @param isAll
     * @return
     */
    BaseResult findPreAndNext(String id, String categoryId, Boolean isAll);

    /**
     * 设置是否允许评论
     *
     * @param id           主键
     * @param allowComment 是否允许
     * @return
     */
    BaseResult setAllowComment(String id, Boolean allowComment);

}
