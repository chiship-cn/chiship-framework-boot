package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 微信用户标签表单 2022/7/8
 *
 * @author LiJian
 */
@ApiModel(value = "微信用户标签表单")
public class ThirdWechatTagDto {

	@ApiModelProperty(value = "公众号AppId")
	@NotEmpty(message = "公众号AppId" + BaseTipConstants.NOT_EMPTY)
	private String appId;

	@ApiModelProperty(value = "标签")
	@NotEmpty(message = "标签" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 30)
	private String tagName;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

}
