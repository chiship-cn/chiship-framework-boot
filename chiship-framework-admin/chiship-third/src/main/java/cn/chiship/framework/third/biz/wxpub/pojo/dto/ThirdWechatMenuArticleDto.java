package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;
import java.util.List;

/**
 * 微信菜单与文章关联表单 2022/7/16
 *
 * @author LiJian
 */
@ApiModel(value = "微信菜单与文章关联表单")
public class ThirdWechatMenuArticleDto {

	@ApiModelProperty(value = "菜单ID", required = true)
	@NotEmpty(message = "菜单ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String menuId;

	@ApiModelProperty(value = "菜单事件ID", required = true)
	@NotEmpty(message = "菜单事件ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String menuEventValue;

	@ApiModelProperty(value = "素材ID集合", required = true)
	private List<String> articleIds;

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getMenuEventValue() {
		return menuEventValue;
	}

	public void setMenuEventValue(String menuEventValue) {
		this.menuEventValue = menuEventValue;
	}

	public List<String> getArticleIds() {
		return articleIds;
	}

	public void setArticleIds(List<String> articleIds) {
		this.articleIds = articleIds;
	}

}
