package cn.chiship.framework.third.biz.wxpub.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-17
 */
public class ThirdWechatAutoReply implements Serializable {

	/**
	 * 创建时间
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 公众号
	 */
	private String appId;

	/**
	 * 回复类型 0：关注时回复 1：消息回复 2：关键词回复 3：微信菜单
	 */
	private Byte replayType;

	/**
	 * 类型 0 图片 1 语音 2 视频 3 文本 4 图文
	 */
	private Byte resourceType;

	/**
	 * 文字内容
	 */
	private String textContent;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 素材ID
	 */
	private String mediaId;

	/**
	 * 素材路径
	 */
	private String fileUrl;

	/**
	 * 文章配置，最多允许8条
	 */
	private String articleContent;

	/**
	 * 匹配类型 0半匹配 1全匹配
	 */
	private Byte matchingType;

	/**
	 * 消息类型
	 */
	private String messageType;

	/**
	 * 关键字
	 */
	private String keyword;

	/**
	 * 关联配置ID 菜单menu_value或消息主键
	 */
	private String relationConfigId;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Byte getReplayType() {
		return replayType;
	}

	public void setReplayType(Byte replayType) {
		this.replayType = replayType;
	}

	public Byte getResourceType() {
		return resourceType;
	}

	public void setResourceType(Byte resourceType) {
		this.resourceType = resourceType;
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getArticleContent() {
		return articleContent;
	}

	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}

	public Byte getMatchingType() {
		return matchingType;
	}

	public void setMatchingType(Byte matchingType) {
		this.matchingType = matchingType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getRelationConfigId() {
		return relationConfigId;
	}

	public void setRelationConfigId(String relationConfigId) {
		this.relationConfigId = relationConfigId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", appId=").append(appId);
		sb.append(", replayType=").append(replayType);
		sb.append(", resourceType=").append(resourceType);
		sb.append(", textContent=").append(textContent);
		sb.append(", title=").append(title);
		sb.append(", mediaId=").append(mediaId);
		sb.append(", fileUrl=").append(fileUrl);
		sb.append(", articleContent=").append(articleContent);
		sb.append(", matchingType=").append(matchingType);
		sb.append(", messageType=").append(messageType);
		sb.append(", keyword=").append(keyword);
		sb.append(", relationConfigId=").append(relationConfigId);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ThirdWechatAutoReply other = (ThirdWechatAutoReply) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getReplayType() == null ? other.getReplayType() == null
						: this.getReplayType().equals(other.getReplayType()))
				&& (this.getResourceType() == null ? other.getResourceType() == null
						: this.getResourceType().equals(other.getResourceType()))
				&& (this.getTextContent() == null ? other.getTextContent() == null
						: this.getTextContent().equals(other.getTextContent()))
				&& (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
				&& (this.getMediaId() == null ? other.getMediaId() == null
						: this.getMediaId().equals(other.getMediaId()))
				&& (this.getFileUrl() == null ? other.getFileUrl() == null
						: this.getFileUrl().equals(other.getFileUrl()))
				&& (this.getArticleContent() == null ? other.getArticleContent() == null
						: this.getArticleContent().equals(other.getArticleContent()))
				&& (this.getMatchingType() == null ? other.getMatchingType() == null
						: this.getMatchingType().equals(other.getMatchingType()))
				&& (this.getMessageType() == null ? other.getMessageType() == null
						: this.getMessageType().equals(other.getMessageType()))
				&& (this.getKeyword() == null ? other.getKeyword() == null
						: this.getKeyword().equals(other.getKeyword()))
				&& (this.getRelationConfigId() == null ? other.getRelationConfigId() == null
						: this.getRelationConfigId().equals(other.getRelationConfigId()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getReplayType() == null) ? 0 : getReplayType().hashCode());
		result = prime * result + ((getResourceType() == null) ? 0 : getResourceType().hashCode());
		result = prime * result + ((getTextContent() == null) ? 0 : getTextContent().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + ((getMediaId() == null) ? 0 : getMediaId().hashCode());
		result = prime * result + ((getFileUrl() == null) ? 0 : getFileUrl().hashCode());
		result = prime * result + ((getArticleContent() == null) ? 0 : getArticleContent().hashCode());
		result = prime * result + ((getMatchingType() == null) ? 0 : getMatchingType().hashCode());
		result = prime * result + ((getMessageType() == null) ? 0 : getMessageType().hashCode());
		result = prime * result + ((getKeyword() == null) ? 0 : getKeyword().hashCode());
		result = prime * result + ((getRelationConfigId() == null) ? 0 : getRelationConfigId().hashCode());
		return result;
	}

}