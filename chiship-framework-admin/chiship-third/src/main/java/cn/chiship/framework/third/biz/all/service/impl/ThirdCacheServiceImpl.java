package cn.chiship.framework.third.biz.all.service.impl;

import cn.chiship.framework.third.biz.all.service.ThirdApplicationKeyConfigService;
import cn.chiship.framework.third.biz.all.service.ThirdCacheService;
import cn.chiship.sdk.cache.service.RedisService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lijian
 */
@Service
public class ThirdCacheServiceImpl implements ThirdCacheService {

    @Resource
    private RedisService redisService;

    @Resource
    private ThirdApplicationKeyConfigService applicationKeyConfigService;

    @Override
    public void cacheConfig() {
        applicationKeyConfigService.cache();
    }

}
