package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategory;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategoryExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface ThirdWechatResourceCategoryMapper
		extends BaseMapper<ThirdWechatResourceCategory, ThirdWechatResourceCategoryExample> {

}