package cn.chiship.framework.third.biz.all.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.all.service.ThirdApplicationKeyConfigService;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfig;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfigExample;
import cn.chiship.framework.third.biz.all.pojo.dto.ThirdApplicationKeyConfigDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 三方应用集成配置控制层 2022/6/12
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/applicationKeyConfig")
@Api(tags = "三方应用集成配置")
public class ThirdApplicationKeyConfigController
        extends BaseController<ThirdApplicationKeyConfig, ThirdApplicationKeyConfigExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdApplicationKeyConfigController.class);

    @Resource
    private ThirdApplicationKeyConfigService thirdApplicationKeyConfigService;

    @Override
    public BaseService getService() {
        return thirdApplicationKeyConfigService;
    }

    @SystemOptionAnnotation(describe = "三方应用集成配置分页")
    @ApiOperation(value = "三方应用集成配置分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
                    paramType = "query"),
            @ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
                    dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

    })
    @GetMapping(value = "/page")
    public ResponseEntity<BaseResult> page(@RequestParam(required = false, value = "keyword") String keyword) {
        ThirdApplicationKeyConfigExample thirdApplicationKeyConfigExample = new ThirdApplicationKeyConfigExample();
        // 创造条件
        ThirdApplicationKeyConfigExample.Criteria thirdApplicationKeyConfigCriteria = thirdApplicationKeyConfigExample
                .createCriteria();
        thirdApplicationKeyConfigCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            thirdApplicationKeyConfigCriteria.andNameLike("%" + keyword + "%");
        }
        return super.responseEntity(BaseResult.ok(super.page(thirdApplicationKeyConfigExample)));
    }

    @SystemOptionAnnotation(describe = "三方应用集成配置列表数据")
    @ApiOperation(value = "三方应用集成配置列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
            @ApiImplicitParam(name = "type", value = "类型", dataTypeClass = Byte.class, paramType = "query"),})
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(@RequestParam(required = false, value = "keyword") String keyword,
                                           @RequestParam(required = false, value = "type") Byte type) {
        ThirdApplicationKeyConfigExample thirdApplicationKeyConfigExample = new ThirdApplicationKeyConfigExample();
        // 创造条件
        ThirdApplicationKeyConfigExample.Criteria thirdApplicationKeyConfigCriteria = thirdApplicationKeyConfigExample
                .createCriteria();
        thirdApplicationKeyConfigCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
            thirdApplicationKeyConfigCriteria.andNameLike("%" + keyword + "%");
        }
        if (!StringUtil.isNull(type)) {
            thirdApplicationKeyConfigCriteria.andTypeEqualTo(type);
        }

        return super.responseEntity(BaseResult.ok(super.list(thirdApplicationKeyConfigExample)));
    }

    @ApiOperation(value = "根据AppId加载数据")
    @ApiImplicitParams({@ApiImplicitParam(name = "appId", value = "AppId", dataTypeClass = String.class,
            paramType = "query", required = true)})
    @GetMapping(value = "getByAppId")
    public ResponseEntity<BaseResult> getByAppId(@RequestParam(value = "appId") String appId) {
        return super.responseEntity(thirdApplicationKeyConfigService.getByAppId(appId));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存三方应用集成配置")
    @ApiOperation(value = "保存三方应用集成配置")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(
            @RequestBody @Valid ThirdApplicationKeyConfigDto thirdApplicationKeyConfigDto) {
        ThirdApplicationKeyConfig thirdApplicationKeyConfig = new ThirdApplicationKeyConfig();
        BeanUtils.copyProperties(thirdApplicationKeyConfigDto, thirdApplicationKeyConfig);
        thirdApplicationKeyConfig.setType(thirdApplicationKeyConfigDto.getApplicationTypeEnum().getType());
        BaseResult baseResult = super.save(thirdApplicationKeyConfig);
        if (baseResult.isSuccess()) {
            thirdApplicationKeyConfigService.cache();
        }
        return super.responseEntity(baseResult);
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新三方应用集成配置")
    @ApiOperation(value = "更新三方应用集成配置")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
            required = true)})
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid ThirdApplicationKeyConfigDto thirdApplicationKeyConfigDto) {
        ThirdApplicationKeyConfig thirdApplicationKeyConfig = new ThirdApplicationKeyConfig();
        BeanUtils.copyProperties(thirdApplicationKeyConfigDto, thirdApplicationKeyConfig);
        BaseResult baseResult = super.update(id, thirdApplicationKeyConfig);
        if (baseResult.isSuccess()) {
            thirdApplicationKeyConfigService.cache();
        }
        return super.responseEntity(baseResult);
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除三方应用集成配置")
    @ApiOperation(value = "删除三方应用集成配置")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody List<String> ids) {
        ThirdApplicationKeyConfigExample thirdApplicationKeyConfigExample = new ThirdApplicationKeyConfigExample();
        thirdApplicationKeyConfigExample.createCriteria().andIdIn(ids);
        BaseResult baseResult = super.remove(thirdApplicationKeyConfigExample);
        if (baseResult.isSuccess()) {
            thirdApplicationKeyConfigService.cache();
        }
        return super.responseEntity(baseResult);
    }

    @ApiOperation(value = "根据类型加载数据")
    @ApiImplicitParams({@ApiImplicitParam(name = "type", value = "类型", dataTypeClass = Byte.class, paramType = "path",
            required = true)})
    @GetMapping(value = "init/{type}")
    public ResponseEntity<BaseResult> init(@PathVariable(value = "type") Byte type) {
        return super.responseEntity(thirdApplicationKeyConfigService.initConfig(type));
    }

    @SystemOptionAnnotation(describe = "三方应用集成配置", option = BusinessTypeEnum.SYSTEM_OPTION_SAVE)
    @ApiOperation(value = "三方应用集成配置")
    @PostMapping(value = "saveOrUpdate")
    public ResponseEntity<BaseResult> saveOrUpdate(
            @RequestBody @Valid ThirdApplicationKeyConfigDto applicationKeyConfigDto) {
        ThirdApplicationKeyConfig applicationKeyConfig = new ThirdApplicationKeyConfig();
        BeanUtils.copyProperties(applicationKeyConfigDto, applicationKeyConfig);
        BaseResult baseResult = thirdApplicationKeyConfigService.config(applicationKeyConfig);
        if (baseResult.isSuccess()) {
            thirdApplicationKeyConfigService.cache();
        }
        return super.responseEntity(baseResult);
    }

    @ApiOperation(value = "获取支付配置信息")
    @GetMapping(value = "getPayConfig")
    public ResponseEntity<BaseResult> getPayConfig() {
        return super.responseEntity(thirdApplicationKeyConfigService.getPayConfig());
    }

}
