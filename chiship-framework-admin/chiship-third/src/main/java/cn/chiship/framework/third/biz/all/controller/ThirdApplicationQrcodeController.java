package cn.chiship.framework.third.biz.all.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.all.service.ThirdApplicationQrcodeService;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcode;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcodeExample;
import cn.chiship.framework.third.biz.all.pojo.dto.ThirdApplicationQrcodeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 二维码控制层 2024/11/25
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/thirdApplicationQrcode")
@Api(tags = "二维码")
public class ThirdApplicationQrcodeController
		extends BaseController<ThirdApplicationQrcode, ThirdApplicationQrcodeExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdApplicationQrcodeController.class);

	@Resource
	private ThirdApplicationQrcodeService thirdApplicationQrcodeService;

	@Override
	public BaseService getService() {
		return thirdApplicationQrcodeService;
	}

	@SystemOptionAnnotation(describe = "二维码分页")
	@ApiOperation(value = "二维码分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "applicationType", value = "应用类型", dataTypeClass = Byte.class,
					paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "applicationType") Byte applicationType) {
		ThirdApplicationQrcodeExample thirdApplicationQrcodeExample = new ThirdApplicationQrcodeExample();
		// 创造条件
		ThirdApplicationQrcodeExample.Criteria thirdApplicationQrcodeCriteria = thirdApplicationQrcodeExample
				.createCriteria();
		thirdApplicationQrcodeCriteria.andIsDeletedEqualTo(BaseConstants.NO).andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			thirdApplicationQrcodeCriteria.andTitleLike(keyword + "%");
		}
		if (!StringUtil.isNull(applicationType)) {
			thirdApplicationQrcodeCriteria.andApplicationTypeEqualTo(applicationType);
		}
		return super.responseEntity(BaseResult.ok(super.page(thirdApplicationQrcodeExample)));
	}

	@SystemOptionAnnotation(describe = "二维码列表数据")
	@ApiOperation(value = "二维码列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "-gmtModified", value = "sort") String sort) {
		ThirdApplicationQrcodeExample thirdApplicationQrcodeExample = new ThirdApplicationQrcodeExample();
		// 创造条件
		ThirdApplicationQrcodeExample.Criteria thirdApplicationQrcodeCriteria = thirdApplicationQrcodeExample
				.createCriteria();
		thirdApplicationQrcodeCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		thirdApplicationQrcodeExample.setOrderByClause(StringUtil.getOrderByValue(sort));
		return super.responseEntity(BaseResult.ok(super.list(thirdApplicationQrcodeExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存二维码")
	@ApiOperation(value = "保存二维码")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid ThirdApplicationQrcodeDto thirdApplicationQrcodeDto) {
		ThirdApplicationQrcode thirdApplicationQrcode = new ThirdApplicationQrcode();
		BeanUtils.copyProperties(thirdApplicationQrcodeDto, thirdApplicationQrcode);
		return super.responseEntity(super.save(thirdApplicationQrcode));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新二维码")
	@ApiOperation(value = "更新二维码")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ThirdApplicationQrcodeDto thirdApplicationQrcodeDto) {
		ThirdApplicationQrcode thirdApplicationQrcode = new ThirdApplicationQrcode();
		BeanUtils.copyProperties(thirdApplicationQrcodeDto, thirdApplicationQrcode);
		return super.responseEntity(super.update(id, thirdApplicationQrcode));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除二维码")
	@ApiOperation(value = "删除二维码")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		ThirdApplicationQrcodeExample thirdApplicationQrcodeExample = new ThirdApplicationQrcodeExample();
		thirdApplicationQrcodeExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(thirdApplicationQrcodeExample));
	}

	@SystemOptionAnnotation(describe = "场景码生成", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
	@ApiOperation(value = "场景码生成")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true), })
	@GetMapping("generateQrCode/{id}")
	public ResponseEntity<BaseResult> generateQrCode(@PathVariable("id") String id) {
		return super.responseEntity(thirdApplicationQrcodeService.generateQrCode(id));
	}

}