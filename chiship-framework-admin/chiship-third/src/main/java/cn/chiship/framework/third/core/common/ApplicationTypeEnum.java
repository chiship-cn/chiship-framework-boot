package cn.chiship.framework.third.core.common;

/**
 * @author lijian
 */

public enum ApplicationTypeEnum {

	/**
	 * 微信公众号
	 */
	APPLICATION_TYPE_WX_PUB(Byte.valueOf("1"), "微信公众号"),
	/**
	 * 小程序
	 */
	APPLICATION_TYPE_WX_MP(Byte.valueOf("2"), "小程序"),
	/**
	 * 企业微信
	 */
	APPLICATION_TYPE_WX_COMPANY(Byte.valueOf("3"), "企业微信"),
	/**
	 * 钉钉
	 */
	APPLICATION_TYPE_DING_TALK(Byte.valueOf("4"), "钉钉"),;

	private Byte type;

	private String message;

	ApplicationTypeEnum(Byte type, String message) {
		this.type = type;
		this.message = message;
	}

	public Byte getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
