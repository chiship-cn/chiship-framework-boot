package cn.chiship.framework.third.biz.wxmini.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author lijian
 */
@ApiModel(value = "小程序待解密数据表单")
public class DecryptingDto {

	@ApiModelProperty(value = "敏感数据在内的完整用户信息的加密数据", required = true)
	@NotEmpty(message = "敏感数据在内的完整用户信息的加密数据" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1)
	String encryptedData;

	@ApiModelProperty(value = "加密初始向量", required = true)
	@NotEmpty(message = "加密初始向量" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1)
	String iv;

	@ApiModelProperty(value = "登陆换取的sessionKey", required = true)
	@NotEmpty(message = "登陆换取的sessionKey" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1)
	String sessionKey;

	public String getEncryptedData() {
		return encryptedData;
	}

	public void setEncryptedData(String encryptedData) {
		this.encryptedData = encryptedData;
	}

	public String getIv() {
		return iv;
	}

	public void setIv(String iv) {
		this.iv = iv;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

}
