package cn.chiship.framework.third.biz.all.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-11
 */
public class ThirdApplicationKeyConfig implements Serializable {

	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	private String photo;

	/**
	 * 三方应用名称
	 */
	private String name;

	/**
	 * 项目标识
	 */
	private String projectId;

	/**
	 * 标识
	 */
	private String identification;

	/**
	 * 三方应用APP_ID
	 */
	private String appId;

	/**
	 * 三方应用Key
	 */
	private String appKey;

	/**
	 * 三方应用密钥
	 */
	private String appSecret;

	/**
	 * 服务器地址
	 */
	private String serverUrl;

	/**
	 * 请求Token
	 */
	private String serverToken;

	/**
	 * 描述
	 */
	private String remark;

	/**
	 * 微信类型 1 公众号 2 小程序 3 企业微信 4 钉钉
	 */
	private Byte type;

	/**
	 * 备用地址1
	 */
	private String url1;

	/**
	 * 备用地址2
	 */
	private String url2;

	/**
	 * 备用地址3
	 */
	private String url3;

	/**
	 * 备用数值
	 */
	private String value1;

	/**
	 * 备用数值
	 */
	private String value2;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getIdentification() {
		return identification;
	}

	public void setIdentification(String identification) {
		this.identification = identification;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getServerToken() {
		return serverToken;
	}

	public void setServerToken(String serverToken) {
		this.serverToken = serverToken;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getUrl1() {
		return url1;
	}

	public void setUrl1(String url1) {
		this.url1 = url1;
	}

	public String getUrl2() {
		return url2;
	}

	public void setUrl2(String url2) {
		this.url2 = url2;
	}

	public String getUrl3() {
		return url3;
	}

	public void setUrl3(String url3) {
		this.url3 = url3;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", photo=").append(photo);
		sb.append(", name=").append(name);
		sb.append(", projectId=").append(projectId);
		sb.append(", identification=").append(identification);
		sb.append(", appId=").append(appId);
		sb.append(", appKey=").append(appKey);
		sb.append(", appSecret=").append(appSecret);
		sb.append(", serverUrl=").append(serverUrl);
		sb.append(", serverToken=").append(serverToken);
		sb.append(", remark=").append(remark);
		sb.append(", type=").append(type);
		sb.append(", url1=").append(url1);
		sb.append(", url2=").append(url2);
		sb.append(", url3=").append(url3);
		sb.append(", value1=").append(value1);
		sb.append(", value2=").append(value2);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ThirdApplicationKeyConfig other = (ThirdApplicationKeyConfig) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getPhoto() == null ? other.getPhoto() == null : this.getPhoto().equals(other.getPhoto()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getProjectId() == null ? other.getProjectId() == null
						: this.getProjectId().equals(other.getProjectId()))
				&& (this.getIdentification() == null ? other.getIdentification() == null
						: this.getIdentification().equals(other.getIdentification()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getAppKey() == null ? other.getAppKey() == null : this.getAppKey().equals(other.getAppKey()))
				&& (this.getAppSecret() == null ? other.getAppSecret() == null
						: this.getAppSecret().equals(other.getAppSecret()))
				&& (this.getServerUrl() == null ? other.getServerUrl() == null
						: this.getServerUrl().equals(other.getServerUrl()))
				&& (this.getServerToken() == null ? other.getServerToken() == null
						: this.getServerToken().equals(other.getServerToken()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getUrl1() == null ? other.getUrl1() == null : this.getUrl1().equals(other.getUrl1()))
				&& (this.getUrl2() == null ? other.getUrl2() == null : this.getUrl2().equals(other.getUrl2()))
				&& (this.getUrl3() == null ? other.getUrl3() == null : this.getUrl3().equals(other.getUrl3()))
				&& (this.getValue1() == null ? other.getValue1() == null : this.getValue1().equals(other.getValue1()))
				&& (this.getValue2() == null ? other.getValue2() == null : this.getValue2().equals(other.getValue2()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getPhoto() == null) ? 0 : getPhoto().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getProjectId() == null) ? 0 : getProjectId().hashCode());
		result = prime * result + ((getIdentification() == null) ? 0 : getIdentification().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getAppKey() == null) ? 0 : getAppKey().hashCode());
		result = prime * result + ((getAppSecret() == null) ? 0 : getAppSecret().hashCode());
		result = prime * result + ((getServerUrl() == null) ? 0 : getServerUrl().hashCode());
		result = prime * result + ((getServerToken() == null) ? 0 : getServerToken().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getUrl1() == null) ? 0 : getUrl1().hashCode());
		result = prime * result + ((getUrl2() == null) ? 0 : getUrl2().hashCode());
		result = prime * result + ((getUrl3() == null) ? 0 : getUrl3().hashCode());
		result = prime * result + ((getValue1() == null) ? 0 : getValue1().hashCode());
		result = prime * result + ((getValue2() == null) ? 0 : getValue2().hashCode());
		return result;
	}

}