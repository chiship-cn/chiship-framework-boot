package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.third.core.common.ThirdOauthLoginDto;

import java.util.Map;

/**
 * @author lijian
 */
public interface WeixinService {

    /**
     * 解析消息
     *
     * @param params 来自微信封装后的消息
     * @return
     */
    String analysisMessage(Map<String, String> params);

    /**
     * 获得授权链接
     *
     * @param oauthLoginDto
     * @return
     */
    BaseResult oauth2Auth(ThirdOauthLoginDto oauthLoginDto);

}
