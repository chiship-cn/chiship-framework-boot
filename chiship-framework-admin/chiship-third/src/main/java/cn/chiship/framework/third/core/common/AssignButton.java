package cn.chiship.framework.third.core.common;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenu;
import cn.chiship.sdk.third.core.wx.enums.WxPubMenuTypeEnum;
import cn.chiship.sdk.third.core.wx.pub.entity.WxPubButton;
import cn.chiship.sdk.third.core.wx.pub.entity.WxPubClickButton;
import cn.chiship.sdk.third.core.wx.pub.entity.WxPubMpButton;
import cn.chiship.sdk.third.core.wx.pub.entity.WxPubViewButton;

/**
 * 根据WechatMenu的type类型为相应的button类赋值
 *
 * @author lijian
 */
public class AssignButton {

	private AssignButton() {
	}

	public static WxPubButton menuToButton(ThirdWechatMenu wechatMenu) {
		if (WxPubMenuTypeEnum.WX_PUB_MENU_CLICK.getType().equals(wechatMenu.getType())) {
			return new WxPubClickButton(wechatMenu.getName(), wechatMenu.getEventValue());
		}
		else if (WxPubMenuTypeEnum.WX_PUB_MENU_VIEW.getType().equals(wechatMenu.getType())) {
			return new WxPubViewButton(wechatMenu.getName(), wechatMenu.getEventValue());
		}
		else if (WxPubMenuTypeEnum.WX_PUB_MENU_MP.getType().equals(wechatMenu.getType())) {
			return new WxPubMpButton(wechatMenu.getName(), wechatMenu.getMpAppId(), wechatMenu.getMpPagePath(),
					wechatMenu.getMpUrl());
		}
		return null;
	}

}
