package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReply;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReplyExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-07-11
 */
public interface ThirdWechatAutoReplyMapper extends BaseMapper<ThirdWechatAutoReply, ThirdWechatAutoReplyExample> {

}
