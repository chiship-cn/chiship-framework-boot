package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;


/**
 * 微信用户表单 2022/6/18
 *
 * @author LiJian
 */
@ApiModel(value = "微信用户表单")
public class ThirdWechatUserDto {

    @ApiModelProperty(value = "备注")
    @Length(min = 0, max = 30)
    private String remark;

    @ApiModelProperty(value = "标签")
    private String tagIdList;

    public @Length(min = 0, max = 30) String getRemark() {
        return remark;
    }

    public void setRemark(@Length(min = 0, max = 30) String remark) {
        this.remark = remark;
    }

    public String getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(String tagIdList) {
        this.tagIdList = tagIdList;
    }
}