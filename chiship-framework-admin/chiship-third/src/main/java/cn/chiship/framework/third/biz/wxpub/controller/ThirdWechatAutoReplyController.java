package cn.chiship.framework.third.biz.wxpub.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.third.biz.wxpub.pojo.dto.ThirdWechatAutoReplyDto;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatAutoReplyService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReply;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReplyExample;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 自动回复控制层 2024/7/11
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/thirdWechatAutoReply")
@Api(tags = "自动回复")
public class ThirdWechatAutoReplyController extends BaseController<ThirdWechatAutoReply, ThirdWechatAutoReplyExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatAutoReplyController.class);

	@Resource
	private ThirdWechatAutoReplyService thirdWechatAutoReplyService;

	@Override
	public BaseService getService() {
		return thirdWechatAutoReplyService;
	}

	@SystemOptionAnnotation(describe = "自动回复分页")
	@ApiOperation(value = "自动回复分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "replayType", value = "回复类型  0：关注时回复  1：消息回复 2：关键词回复 3：菜单",
					dataTypeClass = Byte.class, paramType = "query"),
			@ApiImplicitParam(name = "relationConfigId", value = "关联主键", dataTypeClass = String.class,
					paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "replayType") Byte replayType,
			@RequestParam(required = false, defaultValue = "", value = "relationConfigId") String relationConfigId) {
		ThirdWechatAutoReplyExample thirdWechatAutoReplyExample = new ThirdWechatAutoReplyExample();
		// 创造条件
		ThirdWechatAutoReplyExample.Criteria thirdWechatAutoReplyCriteria = thirdWechatAutoReplyExample
				.createCriteria();
		thirdWechatAutoReplyCriteria.andIsDeletedEqualTo(BaseConstants.NO).andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		if (!StringUtil.isNull(replayType)) {
			thirdWechatAutoReplyCriteria.andReplayTypeEqualTo(replayType);
		}
		if (!StringUtil.isNullOrEmpty(relationConfigId)) {
			thirdWechatAutoReplyCriteria.andRelationConfigIdEqualTo(relationConfigId);
		}
		return super.responseEntity(BaseResult.ok(super.page(thirdWechatAutoReplyExample)));
	}

	@SystemOptionAnnotation(describe = "自动回复列表数据")
	@ApiOperation(value = "自动回复列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "replayType", value = "回复类型  0：关注时回复  1：消息回复 2：关键词回复 3：菜单",
					dataTypeClass = Byte.class, paramType = "query"),
			@ApiImplicitParam(name = "relationConfigId", value = "关联主键", dataTypeClass = String.class,
					paramType = "query"),

	})
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(required = false, defaultValue = "", value = "replayType") Byte replayType,
			@RequestParam(required = false, defaultValue = "", value = "relationConfigId") String relationConfigId) {
		ThirdWechatAutoReplyExample thirdWechatAutoReplyExample = new ThirdWechatAutoReplyExample();
		// 创造条件
		ThirdWechatAutoReplyExample.Criteria thirdWechatAutoReplyCriteria = thirdWechatAutoReplyExample
				.createCriteria();
		thirdWechatAutoReplyCriteria.andIsDeletedEqualTo(BaseConstants.NO).andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
		}
		if (!StringUtil.isNull(replayType)) {
			thirdWechatAutoReplyCriteria.andReplayTypeEqualTo(replayType);
		}
		if (!StringUtil.isNullOrEmpty(relationConfigId)) {
			thirdWechatAutoReplyCriteria.andRelationConfigIdEqualTo(relationConfigId);
		}
		return super.responseEntity(BaseResult.ok(super.list(thirdWechatAutoReplyExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存自动回复")
	@ApiOperation(value = "保存自动回复")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid ThirdWechatAutoReplyDto thirdWechatAutoReplyDto) {
		ThirdWechatAutoReply thirdWechatAutoReply = new ThirdWechatAutoReply();
		BeanUtils.copyProperties(thirdWechatAutoReplyDto, thirdWechatAutoReply);
		return super.responseEntity(super.save(thirdWechatAutoReply));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新自动回复")
	@ApiOperation(value = "更新自动回复")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ThirdWechatAutoReplyDto thirdWechatAutoReplyDto) {
		ThirdWechatAutoReply thirdWechatAutoReply = new ThirdWechatAutoReply();
		BeanUtils.copyProperties(thirdWechatAutoReplyDto, thirdWechatAutoReply);
		return super.responseEntity(super.update(id, thirdWechatAutoReply));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除自动回复")
	@ApiOperation(value = "删除自动回复")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody @Valid List<String> ids) {
		ThirdWechatAutoReplyExample thirdWechatAutoReplyExample = new ThirdWechatAutoReplyExample();
		thirdWechatAutoReplyExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(thirdWechatAutoReplyExample));
	}

}
