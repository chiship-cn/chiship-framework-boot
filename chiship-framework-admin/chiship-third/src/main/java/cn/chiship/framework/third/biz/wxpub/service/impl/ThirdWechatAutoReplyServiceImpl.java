package cn.chiship.framework.third.biz.wxpub.service.impl;

import cn.chiship.framework.third.core.common.AutoReplyMessageTypeEnum;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.third.biz.wxpub.mapper.ThirdWechatAutoReplyMapper;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReply;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatAutoReplyExample;
import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatAutoReplyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 自动回复业务接口实现层 2024/7/11
 *
 * @author lijian
 */
@Service
public class ThirdWechatAutoReplyServiceImpl extends BaseServiceImpl<ThirdWechatAutoReply, ThirdWechatAutoReplyExample>
		implements ThirdWechatAutoReplyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatAutoReplyServiceImpl.class);

	@Resource
	ThirdWechatAutoReplyMapper thirdWechatAutoReplyMapper;

	@Override
	public BaseResult insertSelective(ThirdWechatAutoReply thirdWechatAutoReply) {
		/**
		 * 回复类型 0：关注时回复 1：消息回复 2：关键词回复 3：微信菜单
		 */
		ThirdWechatAutoReplyExample wechatAutoReplyExample = new ThirdWechatAutoReplyExample();

		if (AutoReplyMessageTypeEnum.AUTO_REPLY_MESSAGE_TYPE_SUB.getType()
				.equals(thirdWechatAutoReply.getReplayType())) {
			/**
			 * 关注时回复 根据appID、回复类型查找数据 若有记录则进行修改操作 若没有记录则进行新增操作
			 */
			wechatAutoReplyExample.createCriteria().andAppIdEqualTo(thirdWechatAutoReply.getAppId())
					.andReplayTypeEqualTo(thirdWechatAutoReply.getReplayType());
		}
		else if (AutoReplyMessageTypeEnum.AUTO_REPLY_MESSAGE_TYPE_NORMAL.getType()
				.equals(thirdWechatAutoReply.getReplayType())) {
			/**
			 * 消息 根据appID、回复类型、消息类型查找数据 若有记录则进行修改操作 若没有记录则进行新增操作
			 */
			wechatAutoReplyExample.createCriteria().andAppIdEqualTo(thirdWechatAutoReply.getAppId())
					.andReplayTypeEqualTo(thirdWechatAutoReply.getReplayType())
					.andResourceTypeEqualTo(thirdWechatAutoReply.getResourceType())
					.andMessageTypeEqualTo(thirdWechatAutoReply.getMessageType());

		}
		else if (AutoReplyMessageTypeEnum.AUTO_REPLY_MESSAGE_TYPE_MENU.getType()
				.equals(thirdWechatAutoReply.getReplayType())) {
			/**
			 * 菜单回复 根据appID、EventKey查找数据 若有记录则进行修改操作 若没有记录则进行新增操作
			 */
			wechatAutoReplyExample.createCriteria().andAppIdEqualTo(thirdWechatAutoReply.getAppId())
					.andRelationConfigIdEqualTo(thirdWechatAutoReply.getRelationConfigId());
		}
		else {
			wechatAutoReplyExample.createCriteria().andIdEqualTo("-1");
		}
		List<ThirdWechatAutoReply> thirdWechatAutoReplies = thirdWechatAutoReplyMapper
				.selectByExample(wechatAutoReplyExample);
		if (thirdWechatAutoReplies.isEmpty()) {
			return super.insertSelective(thirdWechatAutoReply);
		}
		else {
			ThirdWechatAutoReply wechatAutoReply = thirdWechatAutoReplies.get(0);
			thirdWechatAutoReply.setId(wechatAutoReply.getId());
			thirdWechatAutoReply.setGmtCreated(wechatAutoReply.getGmtCreated());
			thirdWechatAutoReply.setGmtModified(System.currentTimeMillis());
			thirdWechatAutoReply.setIsDeleted(wechatAutoReply.getIsDeleted());
			thirdWechatAutoReplyMapper.updateByPrimaryKey(thirdWechatAutoReply);
			return BaseResult.ok();
		}
	}

	@Override
	public BaseResult updateByPrimaryKeySelective(ThirdWechatAutoReply thirdWechatAutoReply) {
		ThirdWechatAutoReply wechatAutoReply = selectByPrimaryKey(thirdWechatAutoReply.getId());
		thirdWechatAutoReply.setId(wechatAutoReply.getId());
		thirdWechatAutoReply.setGmtCreated(wechatAutoReply.getGmtCreated());
		thirdWechatAutoReply.setGmtModified(System.currentTimeMillis());
		thirdWechatAutoReply.setIsDeleted(wechatAutoReply.getIsDeleted());
		thirdWechatAutoReplyMapper.updateByPrimaryKey(thirdWechatAutoReply);
		return BaseResult.ok();
	}

}
