package cn.chiship.framework.third.biz.wxpub.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class ThirdWechatUserExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ThirdWechatUserExample() {
		oredCriteria = new ArrayList<>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andOpenIdIsNull() {
			addCriterion("open_id is null");
			return (Criteria) this;
		}

		public Criteria andOpenIdIsNotNull() {
			addCriterion("open_id is not null");
			return (Criteria) this;
		}

		public Criteria andOpenIdEqualTo(String value) {
			addCriterion("open_id =", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdNotEqualTo(String value) {
			addCriterion("open_id <>", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdGreaterThan(String value) {
			addCriterion("open_id >", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdGreaterThanOrEqualTo(String value) {
			addCriterion("open_id >=", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdLessThan(String value) {
			addCriterion("open_id <", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdLessThanOrEqualTo(String value) {
			addCriterion("open_id <=", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdLike(String value) {
			addCriterion("open_id like", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdNotLike(String value) {
			addCriterion("open_id not like", value, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdIn(List<String> values) {
			addCriterion("open_id in", values, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdNotIn(List<String> values) {
			addCriterion("open_id not in", values, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdBetween(String value1, String value2) {
			addCriterion("open_id between", value1, value2, "openId");
			return (Criteria) this;
		}

		public Criteria andOpenIdNotBetween(String value1, String value2) {
			addCriterion("open_id not between", value1, value2, "openId");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNull() {
			addCriterion("app_id is null");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNotNull() {
			addCriterion("app_id is not null");
			return (Criteria) this;
		}

		public Criteria andAppIdEqualTo(String value) {
			addCriterion("app_id =", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotEqualTo(String value) {
			addCriterion("app_id <>", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThan(String value) {
			addCriterion("app_id >", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThanOrEqualTo(String value) {
			addCriterion("app_id >=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThan(String value) {
			addCriterion("app_id <", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThanOrEqualTo(String value) {
			addCriterion("app_id <=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLike(String value) {
			addCriterion("app_id like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotLike(String value) {
			addCriterion("app_id not like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdIn(List<String> values) {
			addCriterion("app_id in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotIn(List<String> values) {
			addCriterion("app_id not in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdBetween(String value1, String value2) {
			addCriterion("app_id between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotBetween(String value1, String value2) {
			addCriterion("app_id not between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andNickNameIsNull() {
			addCriterion("nick_name is null");
			return (Criteria) this;
		}

		public Criteria andNickNameIsNotNull() {
			addCriterion("nick_name is not null");
			return (Criteria) this;
		}

		public Criteria andNickNameEqualTo(String value) {
			addCriterion("nick_name =", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotEqualTo(String value) {
			addCriterion("nick_name <>", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameGreaterThan(String value) {
			addCriterion("nick_name >", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameGreaterThanOrEqualTo(String value) {
			addCriterion("nick_name >=", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLessThan(String value) {
			addCriterion("nick_name <", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLessThanOrEqualTo(String value) {
			addCriterion("nick_name <=", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameLike(String value) {
			addCriterion("nick_name like", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotLike(String value) {
			addCriterion("nick_name not like", value, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameIn(List<String> values) {
			addCriterion("nick_name in", values, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotIn(List<String> values) {
			addCriterion("nick_name not in", values, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameBetween(String value1, String value2) {
			addCriterion("nick_name between", value1, value2, "nickName");
			return (Criteria) this;
		}

		public Criteria andNickNameNotBetween(String value1, String value2) {
			addCriterion("nick_name not between", value1, value2, "nickName");
			return (Criteria) this;
		}

		public Criteria andSexIsNull() {
			addCriterion("sex is null");
			return (Criteria) this;
		}

		public Criteria andSexIsNotNull() {
			addCriterion("sex is not null");
			return (Criteria) this;
		}

		public Criteria andSexEqualTo(String value) {
			addCriterion("sex =", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotEqualTo(String value) {
			addCriterion("sex <>", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThan(String value) {
			addCriterion("sex >", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexGreaterThanOrEqualTo(String value) {
			addCriterion("sex >=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThan(String value) {
			addCriterion("sex <", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLessThanOrEqualTo(String value) {
			addCriterion("sex <=", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexLike(String value) {
			addCriterion("sex like", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotLike(String value) {
			addCriterion("sex not like", value, "sex");
			return (Criteria) this;
		}

		public Criteria andSexIn(List<String> values) {
			addCriterion("sex in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotIn(List<String> values) {
			addCriterion("sex not in", values, "sex");
			return (Criteria) this;
		}

		public Criteria andSexBetween(String value1, String value2) {
			addCriterion("sex between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andSexNotBetween(String value1, String value2) {
			addCriterion("sex not between", value1, value2, "sex");
			return (Criteria) this;
		}

		public Criteria andLanguageIsNull() {
			addCriterion("language is null");
			return (Criteria) this;
		}

		public Criteria andLanguageIsNotNull() {
			addCriterion("language is not null");
			return (Criteria) this;
		}

		public Criteria andLanguageEqualTo(String value) {
			addCriterion("language =", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageNotEqualTo(String value) {
			addCriterion("language <>", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageGreaterThan(String value) {
			addCriterion("language >", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageGreaterThanOrEqualTo(String value) {
			addCriterion("language >=", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageLessThan(String value) {
			addCriterion("language <", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageLessThanOrEqualTo(String value) {
			addCriterion("language <=", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageLike(String value) {
			addCriterion("language like", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageNotLike(String value) {
			addCriterion("language not like", value, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageIn(List<String> values) {
			addCriterion("language in", values, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageNotIn(List<String> values) {
			addCriterion("language not in", values, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageBetween(String value1, String value2) {
			addCriterion("language between", value1, value2, "language");
			return (Criteria) this;
		}

		public Criteria andLanguageNotBetween(String value1, String value2) {
			addCriterion("language not between", value1, value2, "language");
			return (Criteria) this;
		}

		public Criteria andProvinceIsNull() {
			addCriterion("province is null");
			return (Criteria) this;
		}

		public Criteria andProvinceIsNotNull() {
			addCriterion("province is not null");
			return (Criteria) this;
		}

		public Criteria andProvinceEqualTo(String value) {
			addCriterion("province =", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceNotEqualTo(String value) {
			addCriterion("province <>", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceGreaterThan(String value) {
			addCriterion("province >", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceGreaterThanOrEqualTo(String value) {
			addCriterion("province >=", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceLessThan(String value) {
			addCriterion("province <", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceLessThanOrEqualTo(String value) {
			addCriterion("province <=", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceLike(String value) {
			addCriterion("province like", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceNotLike(String value) {
			addCriterion("province not like", value, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceIn(List<String> values) {
			addCriterion("province in", values, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceNotIn(List<String> values) {
			addCriterion("province not in", values, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceBetween(String value1, String value2) {
			addCriterion("province between", value1, value2, "province");
			return (Criteria) this;
		}

		public Criteria andProvinceNotBetween(String value1, String value2) {
			addCriterion("province not between", value1, value2, "province");
			return (Criteria) this;
		}

		public Criteria andCityIsNull() {
			addCriterion("city is null");
			return (Criteria) this;
		}

		public Criteria andCityIsNotNull() {
			addCriterion("city is not null");
			return (Criteria) this;
		}

		public Criteria andCityEqualTo(String value) {
			addCriterion("city =", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotEqualTo(String value) {
			addCriterion("city <>", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThan(String value) {
			addCriterion("city >", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityGreaterThanOrEqualTo(String value) {
			addCriterion("city >=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThan(String value) {
			addCriterion("city <", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLessThanOrEqualTo(String value) {
			addCriterion("city <=", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityLike(String value) {
			addCriterion("city like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotLike(String value) {
			addCriterion("city not like", value, "city");
			return (Criteria) this;
		}

		public Criteria andCityIn(List<String> values) {
			addCriterion("city in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotIn(List<String> values) {
			addCriterion("city not in", values, "city");
			return (Criteria) this;
		}

		public Criteria andCityBetween(String value1, String value2) {
			addCriterion("city between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andCityNotBetween(String value1, String value2) {
			addCriterion("city not between", value1, value2, "city");
			return (Criteria) this;
		}

		public Criteria andCountryIsNull() {
			addCriterion("country is null");
			return (Criteria) this;
		}

		public Criteria andCountryIsNotNull() {
			addCriterion("country is not null");
			return (Criteria) this;
		}

		public Criteria andCountryEqualTo(String value) {
			addCriterion("country =", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotEqualTo(String value) {
			addCriterion("country <>", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryGreaterThan(String value) {
			addCriterion("country >", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryGreaterThanOrEqualTo(String value) {
			addCriterion("country >=", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLessThan(String value) {
			addCriterion("country <", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLessThanOrEqualTo(String value) {
			addCriterion("country <=", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryLike(String value) {
			addCriterion("country like", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotLike(String value) {
			addCriterion("country not like", value, "country");
			return (Criteria) this;
		}

		public Criteria andCountryIn(List<String> values) {
			addCriterion("country in", values, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotIn(List<String> values) {
			addCriterion("country not in", values, "country");
			return (Criteria) this;
		}

		public Criteria andCountryBetween(String value1, String value2) {
			addCriterion("country between", value1, value2, "country");
			return (Criteria) this;
		}

		public Criteria andCountryNotBetween(String value1, String value2) {
			addCriterion("country not between", value1, value2, "country");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlIsNull() {
			addCriterion("head_image_url is null");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlIsNotNull() {
			addCriterion("head_image_url is not null");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlEqualTo(String value) {
			addCriterion("head_image_url =", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlNotEqualTo(String value) {
			addCriterion("head_image_url <>", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlGreaterThan(String value) {
			addCriterion("head_image_url >", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlGreaterThanOrEqualTo(String value) {
			addCriterion("head_image_url >=", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlLessThan(String value) {
			addCriterion("head_image_url <", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlLessThanOrEqualTo(String value) {
			addCriterion("head_image_url <=", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlLike(String value) {
			addCriterion("head_image_url like", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlNotLike(String value) {
			addCriterion("head_image_url not like", value, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlIn(List<String> values) {
			addCriterion("head_image_url in", values, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlNotIn(List<String> values) {
			addCriterion("head_image_url not in", values, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlBetween(String value1, String value2) {
			addCriterion("head_image_url between", value1, value2, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andHeadImageUrlNotBetween(String value1, String value2) {
			addCriterion("head_image_url not between", value1, value2, "headImageUrl");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeIsNull() {
			addCriterion("subscripe_time is null");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeIsNotNull() {
			addCriterion("subscripe_time is not null");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeEqualTo(Long value) {
			addCriterion("subscripe_time =", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeNotEqualTo(Long value) {
			addCriterion("subscripe_time <>", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeGreaterThan(Long value) {
			addCriterion("subscripe_time >", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("subscripe_time >=", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeLessThan(Long value) {
			addCriterion("subscripe_time <", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeLessThanOrEqualTo(Long value) {
			addCriterion("subscripe_time <=", value, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeIn(List<Long> values) {
			addCriterion("subscripe_time in", values, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeNotIn(List<Long> values) {
			addCriterion("subscripe_time not in", values, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeBetween(Long value1, Long value2) {
			addCriterion("subscripe_time between", value1, value2, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andSubscripeTimeNotBetween(Long value1, Long value2) {
			addCriterion("subscripe_time not between", value1, value2, "subscripeTime");
			return (Criteria) this;
		}

		public Criteria andIsCancleIsNull() {
			addCriterion("is_cancle is null");
			return (Criteria) this;
		}

		public Criteria andIsCancleIsNotNull() {
			addCriterion("is_cancle is not null");
			return (Criteria) this;
		}

		public Criteria andIsCancleEqualTo(Byte value) {
			addCriterion("is_cancle =", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleNotEqualTo(Byte value) {
			addCriterion("is_cancle <>", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleGreaterThan(Byte value) {
			addCriterion("is_cancle >", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_cancle >=", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleLessThan(Byte value) {
			addCriterion("is_cancle <", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleLessThanOrEqualTo(Byte value) {
			addCriterion("is_cancle <=", value, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleIn(List<Byte> values) {
			addCriterion("is_cancle in", values, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleNotIn(List<Byte> values) {
			addCriterion("is_cancle not in", values, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleBetween(Byte value1, Byte value2) {
			addCriterion("is_cancle between", value1, value2, "isCancle");
			return (Criteria) this;
		}

		public Criteria andIsCancleNotBetween(Byte value1, Byte value2) {
			addCriterion("is_cancle not between", value1, value2, "isCancle");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeIsNull() {
			addCriterion("un_subscripe_time is null");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeIsNotNull() {
			addCriterion("un_subscripe_time is not null");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeEqualTo(Long value) {
			addCriterion("un_subscripe_time =", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeNotEqualTo(Long value) {
			addCriterion("un_subscripe_time <>", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeGreaterThan(Long value) {
			addCriterion("un_subscripe_time >", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeGreaterThanOrEqualTo(Long value) {
			addCriterion("un_subscripe_time >=", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeLessThan(Long value) {
			addCriterion("un_subscripe_time <", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeLessThanOrEqualTo(Long value) {
			addCriterion("un_subscripe_time <=", value, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeIn(List<Long> values) {
			addCriterion("un_subscripe_time in", values, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeNotIn(List<Long> values) {
			addCriterion("un_subscripe_time not in", values, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeBetween(Long value1, Long value2) {
			addCriterion("un_subscripe_time between", value1, value2, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andUnSubscripeTimeNotBetween(Long value1, Long value2) {
			addCriterion("un_subscripe_time not between", value1, value2, "unSubscripeTime");
			return (Criteria) this;
		}

		public Criteria andLocationIsNull() {
			addCriterion("location is null");
			return (Criteria) this;
		}

		public Criteria andLocationIsNotNull() {
			addCriterion("location is not null");
			return (Criteria) this;
		}

		public Criteria andLocationEqualTo(String value) {
			addCriterion("location =", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationNotEqualTo(String value) {
			addCriterion("location <>", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationGreaterThan(String value) {
			addCriterion("location >", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationGreaterThanOrEqualTo(String value) {
			addCriterion("location >=", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationLessThan(String value) {
			addCriterion("location <", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationLessThanOrEqualTo(String value) {
			addCriterion("location <=", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationLike(String value) {
			addCriterion("location like", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationNotLike(String value) {
			addCriterion("location not like", value, "location");
			return (Criteria) this;
		}

		public Criteria andLocationIn(List<String> values) {
			addCriterion("location in", values, "location");
			return (Criteria) this;
		}

		public Criteria andLocationNotIn(List<String> values) {
			addCriterion("location not in", values, "location");
			return (Criteria) this;
		}

		public Criteria andLocationBetween(String value1, String value2) {
			addCriterion("location between", value1, value2, "location");
			return (Criteria) this;
		}

		public Criteria andLocationNotBetween(String value1, String value2) {
			addCriterion("location not between", value1, value2, "location");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andSubscribeIsNull() {
			addCriterion("subscribe is null");
			return (Criteria) this;
		}

		public Criteria andSubscribeIsNotNull() {
			addCriterion("subscribe is not null");
			return (Criteria) this;
		}

		public Criteria andSubscribeEqualTo(Byte value) {
			addCriterion("subscribe =", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeNotEqualTo(Byte value) {
			addCriterion("subscribe <>", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeGreaterThan(Byte value) {
			addCriterion("subscribe >", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeGreaterThanOrEqualTo(Byte value) {
			addCriterion("subscribe >=", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeLessThan(Byte value) {
			addCriterion("subscribe <", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeLessThanOrEqualTo(Byte value) {
			addCriterion("subscribe <=", value, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeIn(List<Byte> values) {
			addCriterion("subscribe in", values, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeNotIn(List<Byte> values) {
			addCriterion("subscribe not in", values, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeBetween(Byte value1, Byte value2) {
			addCriterion("subscribe between", value1, value2, "subscribe");
			return (Criteria) this;
		}

		public Criteria andSubscribeNotBetween(Byte value1, Byte value2) {
			addCriterion("subscribe not between", value1, value2, "subscribe");
			return (Criteria) this;
		}

		public Criteria andUnionidIsNull() {
			addCriterion("unionid is null");
			return (Criteria) this;
		}

		public Criteria andUnionidIsNotNull() {
			addCriterion("unionid is not null");
			return (Criteria) this;
		}

		public Criteria andUnionidEqualTo(String value) {
			addCriterion("unionid =", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidNotEqualTo(String value) {
			addCriterion("unionid <>", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidGreaterThan(String value) {
			addCriterion("unionid >", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidGreaterThanOrEqualTo(String value) {
			addCriterion("unionid >=", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidLessThan(String value) {
			addCriterion("unionid <", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidLessThanOrEqualTo(String value) {
			addCriterion("unionid <=", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidLike(String value) {
			addCriterion("unionid like", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidNotLike(String value) {
			addCriterion("unionid not like", value, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidIn(List<String> values) {
			addCriterion("unionid in", values, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidNotIn(List<String> values) {
			addCriterion("unionid not in", values, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidBetween(String value1, String value2) {
			addCriterion("unionid between", value1, value2, "unionid");
			return (Criteria) this;
		}

		public Criteria andUnionidNotBetween(String value1, String value2) {
			addCriterion("unionid not between", value1, value2, "unionid");
			return (Criteria) this;
		}

		public Criteria andGroupIdIsNull() {
			addCriterion("group_id is null");
			return (Criteria) this;
		}

		public Criteria andGroupIdIsNotNull() {
			addCriterion("group_id is not null");
			return (Criteria) this;
		}

		public Criteria andGroupIdEqualTo(Integer value) {
			addCriterion("group_id =", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdNotEqualTo(Integer value) {
			addCriterion("group_id <>", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdGreaterThan(Integer value) {
			addCriterion("group_id >", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdGreaterThanOrEqualTo(Integer value) {
			addCriterion("group_id >=", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdLessThan(Integer value) {
			addCriterion("group_id <", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdLessThanOrEqualTo(Integer value) {
			addCriterion("group_id <=", value, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdIn(List<Integer> values) {
			addCriterion("group_id in", values, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdNotIn(List<Integer> values) {
			addCriterion("group_id not in", values, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdBetween(Integer value1, Integer value2) {
			addCriterion("group_id between", value1, value2, "groupId");
			return (Criteria) this;
		}

		public Criteria andGroupIdNotBetween(Integer value1, Integer value2) {
			addCriterion("group_id not between", value1, value2, "groupId");
			return (Criteria) this;
		}

		public Criteria andTagIdListIsNull() {
			addCriterion("tag_id_list is null");
			return (Criteria) this;
		}

		public Criteria andTagIdListIsNotNull() {
			addCriterion("tag_id_list is not null");
			return (Criteria) this;
		}

		public Criteria andTagIdListEqualTo(String value) {
			addCriterion("tag_id_list =", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListNotEqualTo(String value) {
			addCriterion("tag_id_list <>", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListGreaterThan(String value) {
			addCriterion("tag_id_list >", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListGreaterThanOrEqualTo(String value) {
			addCriterion("tag_id_list >=", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListLessThan(String value) {
			addCriterion("tag_id_list <", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListLessThanOrEqualTo(String value) {
			addCriterion("tag_id_list <=", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListLike(String value) {
			addCriterion("tag_id_list like", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListNotLike(String value) {
			addCriterion("tag_id_list not like", value, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListIn(List<String> values) {
			addCriterion("tag_id_list in", values, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListNotIn(List<String> values) {
			addCriterion("tag_id_list not in", values, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListBetween(String value1, String value2) {
			addCriterion("tag_id_list between", value1, value2, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andTagIdListNotBetween(String value1, String value2) {
			addCriterion("tag_id_list not between", value1, value2, "tagIdList");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneIsNull() {
			addCriterion("subscribe_scene is null");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneIsNotNull() {
			addCriterion("subscribe_scene is not null");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneEqualTo(String value) {
			addCriterion("subscribe_scene =", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneNotEqualTo(String value) {
			addCriterion("subscribe_scene <>", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneGreaterThan(String value) {
			addCriterion("subscribe_scene >", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneGreaterThanOrEqualTo(String value) {
			addCriterion("subscribe_scene >=", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneLessThan(String value) {
			addCriterion("subscribe_scene <", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneLessThanOrEqualTo(String value) {
			addCriterion("subscribe_scene <=", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneLike(String value) {
			addCriterion("subscribe_scene like", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneNotLike(String value) {
			addCriterion("subscribe_scene not like", value, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneIn(List<String> values) {
			addCriterion("subscribe_scene in", values, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneNotIn(List<String> values) {
			addCriterion("subscribe_scene not in", values, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneBetween(String value1, String value2) {
			addCriterion("subscribe_scene between", value1, value2, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andSubscribeSceneNotBetween(String value1, String value2) {
			addCriterion("subscribe_scene not between", value1, value2, "subscribeScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneIsNull() {
			addCriterion("qr_scene is null");
			return (Criteria) this;
		}

		public Criteria andQrSceneIsNotNull() {
			addCriterion("qr_scene is not null");
			return (Criteria) this;
		}

		public Criteria andQrSceneEqualTo(String value) {
			addCriterion("qr_scene =", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneNotEqualTo(String value) {
			addCriterion("qr_scene <>", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneGreaterThan(String value) {
			addCriterion("qr_scene >", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneGreaterThanOrEqualTo(String value) {
			addCriterion("qr_scene >=", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneLessThan(String value) {
			addCriterion("qr_scene <", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneLessThanOrEqualTo(String value) {
			addCriterion("qr_scene <=", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneLike(String value) {
			addCriterion("qr_scene like", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneNotLike(String value) {
			addCriterion("qr_scene not like", value, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneIn(List<String> values) {
			addCriterion("qr_scene in", values, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneNotIn(List<String> values) {
			addCriterion("qr_scene not in", values, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneBetween(String value1, String value2) {
			addCriterion("qr_scene between", value1, value2, "qrScene");
			return (Criteria) this;
		}

		public Criteria andQrSceneNotBetween(String value1, String value2) {
			addCriterion("qr_scene not between", value1, value2, "qrScene");
			return (Criteria) this;
		}

		public Criteria andRSceneStrIsNull() {
			addCriterion("r_scene_str is null");
			return (Criteria) this;
		}

		public Criteria andRSceneStrIsNotNull() {
			addCriterion("r_scene_str is not null");
			return (Criteria) this;
		}

		public Criteria andRSceneStrEqualTo(String value) {
			addCriterion("r_scene_str =", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrNotEqualTo(String value) {
			addCriterion("r_scene_str <>", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrGreaterThan(String value) {
			addCriterion("r_scene_str >", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrGreaterThanOrEqualTo(String value) {
			addCriterion("r_scene_str >=", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrLessThan(String value) {
			addCriterion("r_scene_str <", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrLessThanOrEqualTo(String value) {
			addCriterion("r_scene_str <=", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrLike(String value) {
			addCriterion("r_scene_str like", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrNotLike(String value) {
			addCriterion("r_scene_str not like", value, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrIn(List<String> values) {
			addCriterion("r_scene_str in", values, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrNotIn(List<String> values) {
			addCriterion("r_scene_str not in", values, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrBetween(String value1, String value2) {
			addCriterion("r_scene_str between", value1, value2, "rSceneStr");
			return (Criteria) this;
		}

		public Criteria andRSceneStrNotBetween(String value1, String value2) {
			addCriterion("r_scene_str not between", value1, value2, "rSceneStr");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}