package cn.chiship.framework.third.biz.wxpub.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Example
 *
 * @author lijian
 * @date 2024-07-17
 */
public class ThirdWechatAutoReplyExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public ThirdWechatAutoReplyExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNull() {
			addCriterion("app_id is null");
			return (Criteria) this;
		}

		public Criteria andAppIdIsNotNull() {
			addCriterion("app_id is not null");
			return (Criteria) this;
		}

		public Criteria andAppIdEqualTo(String value) {
			addCriterion("app_id =", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotEqualTo(String value) {
			addCriterion("app_id <>", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThan(String value) {
			addCriterion("app_id >", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdGreaterThanOrEqualTo(String value) {
			addCriterion("app_id >=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThan(String value) {
			addCriterion("app_id <", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLessThanOrEqualTo(String value) {
			addCriterion("app_id <=", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdLike(String value) {
			addCriterion("app_id like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotLike(String value) {
			addCriterion("app_id not like", value, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdIn(List<String> values) {
			addCriterion("app_id in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotIn(List<String> values) {
			addCriterion("app_id not in", values, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdBetween(String value1, String value2) {
			addCriterion("app_id between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andAppIdNotBetween(String value1, String value2) {
			addCriterion("app_id not between", value1, value2, "appId");
			return (Criteria) this;
		}

		public Criteria andReplayTypeIsNull() {
			addCriterion("replay_type is null");
			return (Criteria) this;
		}

		public Criteria andReplayTypeIsNotNull() {
			addCriterion("replay_type is not null");
			return (Criteria) this;
		}

		public Criteria andReplayTypeEqualTo(Byte value) {
			addCriterion("replay_type =", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeNotEqualTo(Byte value) {
			addCriterion("replay_type <>", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeGreaterThan(Byte value) {
			addCriterion("replay_type >", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("replay_type >=", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeLessThan(Byte value) {
			addCriterion("replay_type <", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeLessThanOrEqualTo(Byte value) {
			addCriterion("replay_type <=", value, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeIn(List<Byte> values) {
			addCriterion("replay_type in", values, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeNotIn(List<Byte> values) {
			addCriterion("replay_type not in", values, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeBetween(Byte value1, Byte value2) {
			addCriterion("replay_type between", value1, value2, "replayType");
			return (Criteria) this;
		}

		public Criteria andReplayTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("replay_type not between", value1, value2, "replayType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIsNull() {
			addCriterion("resource_type is null");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIsNotNull() {
			addCriterion("resource_type is not null");
			return (Criteria) this;
		}

		public Criteria andResourceTypeEqualTo(Byte value) {
			addCriterion("resource_type =", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotEqualTo(Byte value) {
			addCriterion("resource_type <>", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeGreaterThan(Byte value) {
			addCriterion("resource_type >", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("resource_type >=", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeLessThan(Byte value) {
			addCriterion("resource_type <", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeLessThanOrEqualTo(Byte value) {
			addCriterion("resource_type <=", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIn(List<Byte> values) {
			addCriterion("resource_type in", values, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotIn(List<Byte> values) {
			addCriterion("resource_type not in", values, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeBetween(Byte value1, Byte value2) {
			addCriterion("resource_type between", value1, value2, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("resource_type not between", value1, value2, "resourceType");
			return (Criteria) this;
		}

		public Criteria andTextContentIsNull() {
			addCriterion("text_content is null");
			return (Criteria) this;
		}

		public Criteria andTextContentIsNotNull() {
			addCriterion("text_content is not null");
			return (Criteria) this;
		}

		public Criteria andTextContentEqualTo(String value) {
			addCriterion("text_content =", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentNotEqualTo(String value) {
			addCriterion("text_content <>", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentGreaterThan(String value) {
			addCriterion("text_content >", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentGreaterThanOrEqualTo(String value) {
			addCriterion("text_content >=", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentLessThan(String value) {
			addCriterion("text_content <", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentLessThanOrEqualTo(String value) {
			addCriterion("text_content <=", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentLike(String value) {
			addCriterion("text_content like", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentNotLike(String value) {
			addCriterion("text_content not like", value, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentIn(List<String> values) {
			addCriterion("text_content in", values, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentNotIn(List<String> values) {
			addCriterion("text_content not in", values, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentBetween(String value1, String value2) {
			addCriterion("text_content between", value1, value2, "textContent");
			return (Criteria) this;
		}

		public Criteria andTextContentNotBetween(String value1, String value2) {
			addCriterion("text_content not between", value1, value2, "textContent");
			return (Criteria) this;
		}

		public Criteria andTitleIsNull() {
			addCriterion("title is null");
			return (Criteria) this;
		}

		public Criteria andTitleIsNotNull() {
			addCriterion("title is not null");
			return (Criteria) this;
		}

		public Criteria andTitleEqualTo(String value) {
			addCriterion("title =", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotEqualTo(String value) {
			addCriterion("title <>", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThan(String value) {
			addCriterion("title >", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleGreaterThanOrEqualTo(String value) {
			addCriterion("title >=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThan(String value) {
			addCriterion("title <", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLessThanOrEqualTo(String value) {
			addCriterion("title <=", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleLike(String value) {
			addCriterion("title like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotLike(String value) {
			addCriterion("title not like", value, "title");
			return (Criteria) this;
		}

		public Criteria andTitleIn(List<String> values) {
			addCriterion("title in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotIn(List<String> values) {
			addCriterion("title not in", values, "title");
			return (Criteria) this;
		}

		public Criteria andTitleBetween(String value1, String value2) {
			addCriterion("title between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andTitleNotBetween(String value1, String value2) {
			addCriterion("title not between", value1, value2, "title");
			return (Criteria) this;
		}

		public Criteria andMediaIdIsNull() {
			addCriterion("media_id is null");
			return (Criteria) this;
		}

		public Criteria andMediaIdIsNotNull() {
			addCriterion("media_id is not null");
			return (Criteria) this;
		}

		public Criteria andMediaIdEqualTo(String value) {
			addCriterion("media_id =", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdNotEqualTo(String value) {
			addCriterion("media_id <>", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdGreaterThan(String value) {
			addCriterion("media_id >", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdGreaterThanOrEqualTo(String value) {
			addCriterion("media_id >=", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdLessThan(String value) {
			addCriterion("media_id <", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdLessThanOrEqualTo(String value) {
			addCriterion("media_id <=", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdLike(String value) {
			addCriterion("media_id like", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdNotLike(String value) {
			addCriterion("media_id not like", value, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdIn(List<String> values) {
			addCriterion("media_id in", values, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdNotIn(List<String> values) {
			addCriterion("media_id not in", values, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdBetween(String value1, String value2) {
			addCriterion("media_id between", value1, value2, "mediaId");
			return (Criteria) this;
		}

		public Criteria andMediaIdNotBetween(String value1, String value2) {
			addCriterion("media_id not between", value1, value2, "mediaId");
			return (Criteria) this;
		}

		public Criteria andFileUrlIsNull() {
			addCriterion("file_url is null");
			return (Criteria) this;
		}

		public Criteria andFileUrlIsNotNull() {
			addCriterion("file_url is not null");
			return (Criteria) this;
		}

		public Criteria andFileUrlEqualTo(String value) {
			addCriterion("file_url =", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlNotEqualTo(String value) {
			addCriterion("file_url <>", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlGreaterThan(String value) {
			addCriterion("file_url >", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlGreaterThanOrEqualTo(String value) {
			addCriterion("file_url >=", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlLessThan(String value) {
			addCriterion("file_url <", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlLessThanOrEqualTo(String value) {
			addCriterion("file_url <=", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlLike(String value) {
			addCriterion("file_url like", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlNotLike(String value) {
			addCriterion("file_url not like", value, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlIn(List<String> values) {
			addCriterion("file_url in", values, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlNotIn(List<String> values) {
			addCriterion("file_url not in", values, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlBetween(String value1, String value2) {
			addCriterion("file_url between", value1, value2, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andFileUrlNotBetween(String value1, String value2) {
			addCriterion("file_url not between", value1, value2, "fileUrl");
			return (Criteria) this;
		}

		public Criteria andArticleContentIsNull() {
			addCriterion("article_content is null");
			return (Criteria) this;
		}

		public Criteria andArticleContentIsNotNull() {
			addCriterion("article_content is not null");
			return (Criteria) this;
		}

		public Criteria andArticleContentEqualTo(String value) {
			addCriterion("article_content =", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentNotEqualTo(String value) {
			addCriterion("article_content <>", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentGreaterThan(String value) {
			addCriterion("article_content >", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentGreaterThanOrEqualTo(String value) {
			addCriterion("article_content >=", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentLessThan(String value) {
			addCriterion("article_content <", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentLessThanOrEqualTo(String value) {
			addCriterion("article_content <=", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentLike(String value) {
			addCriterion("article_content like", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentNotLike(String value) {
			addCriterion("article_content not like", value, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentIn(List<String> values) {
			addCriterion("article_content in", values, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentNotIn(List<String> values) {
			addCriterion("article_content not in", values, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentBetween(String value1, String value2) {
			addCriterion("article_content between", value1, value2, "articleContent");
			return (Criteria) this;
		}

		public Criteria andArticleContentNotBetween(String value1, String value2) {
			addCriterion("article_content not between", value1, value2, "articleContent");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeIsNull() {
			addCriterion("matching_type is null");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeIsNotNull() {
			addCriterion("matching_type is not null");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeEqualTo(Byte value) {
			addCriterion("matching_type =", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeNotEqualTo(Byte value) {
			addCriterion("matching_type <>", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeGreaterThan(Byte value) {
			addCriterion("matching_type >", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("matching_type >=", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeLessThan(Byte value) {
			addCriterion("matching_type <", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeLessThanOrEqualTo(Byte value) {
			addCriterion("matching_type <=", value, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeIn(List<Byte> values) {
			addCriterion("matching_type in", values, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeNotIn(List<Byte> values) {
			addCriterion("matching_type not in", values, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeBetween(Byte value1, Byte value2) {
			addCriterion("matching_type between", value1, value2, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMatchingTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("matching_type not between", value1, value2, "matchingType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeIsNull() {
			addCriterion("message_type is null");
			return (Criteria) this;
		}

		public Criteria andMessageTypeIsNotNull() {
			addCriterion("message_type is not null");
			return (Criteria) this;
		}

		public Criteria andMessageTypeEqualTo(String value) {
			addCriterion("message_type =", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeNotEqualTo(String value) {
			addCriterion("message_type <>", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeGreaterThan(String value) {
			addCriterion("message_type >", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeGreaterThanOrEqualTo(String value) {
			addCriterion("message_type >=", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeLessThan(String value) {
			addCriterion("message_type <", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeLessThanOrEqualTo(String value) {
			addCriterion("message_type <=", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeLike(String value) {
			addCriterion("message_type like", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeNotLike(String value) {
			addCriterion("message_type not like", value, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeIn(List<String> values) {
			addCriterion("message_type in", values, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeNotIn(List<String> values) {
			addCriterion("message_type not in", values, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeBetween(String value1, String value2) {
			addCriterion("message_type between", value1, value2, "messageType");
			return (Criteria) this;
		}

		public Criteria andMessageTypeNotBetween(String value1, String value2) {
			addCriterion("message_type not between", value1, value2, "messageType");
			return (Criteria) this;
		}

		public Criteria andKeywordIsNull() {
			addCriterion("keyword is null");
			return (Criteria) this;
		}

		public Criteria andKeywordIsNotNull() {
			addCriterion("keyword is not null");
			return (Criteria) this;
		}

		public Criteria andKeywordEqualTo(String value) {
			addCriterion("keyword =", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordNotEqualTo(String value) {
			addCriterion("keyword <>", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordGreaterThan(String value) {
			addCriterion("keyword >", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordGreaterThanOrEqualTo(String value) {
			addCriterion("keyword >=", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordLessThan(String value) {
			addCriterion("keyword <", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordLessThanOrEqualTo(String value) {
			addCriterion("keyword <=", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordLike(String value) {
			addCriterion("keyword like", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordNotLike(String value) {
			addCriterion("keyword not like", value, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordIn(List<String> values) {
			addCriterion("keyword in", values, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordNotIn(List<String> values) {
			addCriterion("keyword not in", values, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordBetween(String value1, String value2) {
			addCriterion("keyword between", value1, value2, "keyword");
			return (Criteria) this;
		}

		public Criteria andKeywordNotBetween(String value1, String value2) {
			addCriterion("keyword not between", value1, value2, "keyword");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdIsNull() {
			addCriterion("relation_config_id is null");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdIsNotNull() {
			addCriterion("relation_config_id is not null");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdEqualTo(String value) {
			addCriterion("relation_config_id =", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdNotEqualTo(String value) {
			addCriterion("relation_config_id <>", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdGreaterThan(String value) {
			addCriterion("relation_config_id >", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdGreaterThanOrEqualTo(String value) {
			addCriterion("relation_config_id >=", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdLessThan(String value) {
			addCriterion("relation_config_id <", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdLessThanOrEqualTo(String value) {
			addCriterion("relation_config_id <=", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdLike(String value) {
			addCriterion("relation_config_id like", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdNotLike(String value) {
			addCriterion("relation_config_id not like", value, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdIn(List<String> values) {
			addCriterion("relation_config_id in", values, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdNotIn(List<String> values) {
			addCriterion("relation_config_id not in", values, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdBetween(String value1, String value2) {
			addCriterion("relation_config_id between", value1, value2, "relationConfigId");
			return (Criteria) this;
		}

		public Criteria andRelationConfigIdNotBetween(String value1, String value2) {
			addCriterion("relation_config_id not between", value1, value2, "relationConfigId");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}