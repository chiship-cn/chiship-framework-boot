package cn.chiship.framework.third.biz.all.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfig;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationKeyConfigExample;

/**
 * 三方应用集成配置业务接口层 2022/6/12
 *
 * @author lijian
 */
public interface ThirdApplicationKeyConfigService extends BaseService<ThirdApplicationKeyConfig, ThirdApplicationKeyConfigExample> {

    /**
     * 缓存配置
     */
    void cache();

    /**
     * 根据APPID获取配置
     *
     * @param appId
     * @return
     */
    BaseResult getByAppId(String appId);

    /**
     * 加载数据
     *
     * @param type
     * @return
     */
    BaseResult initConfig(Byte type);

    /**
     * 配置
     *
     * @param applicationKeyConfig
     * @return
     */
    BaseResult config(ThirdApplicationKeyConfig applicationKeyConfig);

    /**
     * 获取支付配置信息
     *
     * @return
     */
    BaseResult getPayConfig();

}
