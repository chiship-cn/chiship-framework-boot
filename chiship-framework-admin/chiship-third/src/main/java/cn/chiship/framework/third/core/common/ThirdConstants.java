package cn.chiship.framework.third.core.common;

import cn.chiship.framework.common.constants.CommonConstants;

/**
 * @author lijian
 */
public class ThirdConstants extends CommonConstants {

	public static final String WELCOME = "WELCOME";

	public static final String APP_ID = "appId";

}
