package cn.chiship.framework.third.biz.wxmini.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.framework.third.biz.wxmini.pojo.dto.DecryptingDto;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.third.service.WxMpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

/**
 * @author lijian
 */
@RestController
@RequestMapping("/wxMini")
@Api(tags = "微信小程序控制器")
public class WxMiniController {

	@Resource
	WxMpService wxMpService;

	@Resource
	private GlobalCacheService globalCacheService;

	@SystemOptionAnnotation(describe = "小程序Code换取Session", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
	@ApiOperation(value = "小程序Code换取Session")
	@ApiImplicitParams({ @ApiImplicitParam(name = "code", value = "小程序登录获得的Code", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping("/code2Session")
	public ResponseEntity<BaseResult> code2Session(@RequestParam(value = "code") String code) {
		wxMpService.config(globalCacheService.getWeiXinConfigByAppId(ThirdUtil.getAppId())).token();
		return new ResponseEntity<>(wxMpService.code2Session(code), HttpStatus.OK);

	}

	@ApiOperation(value = "小程序换取手机号")
	@ApiImplicitParams({ @ApiImplicitParam(name = "code", value = "小程序登录获得的Code", required = true,
			dataTypeClass = String.class, paramType = "query"), })
	@GetMapping("/code2Mobile")
	public ResponseEntity<BaseResult> code2Mobile(@RequestParam(value = "code") String code) {
		wxMpService.config(globalCacheService.getWeiXinConfigByAppId(ThirdUtil.getAppId())).token();
		BaseResult baseResult = wxMpService.getPhoneNumber(code);
		if (!baseResult.isSuccess()) {
			return new ResponseEntity<>(baseResult, HttpStatus.OK);
		}
		Map json = (HashMap) baseResult.getData();
		baseResult = BaseResult.ok(StringUtil.getString(json.get("phoneNumber")));
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

	@SystemOptionAnnotation(describe = "解密小程序敏感数据", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
	@ApiOperation(value = "解密小程序敏感数据")
	@PostMapping("/decryptingOpenData")
	@NoParamsSign
	public ResponseEntity<BaseResult> decryptingOpenData(@RequestBody @Valid DecryptingDto decryptingDto) {
		wxMpService.config(globalCacheService.getWeiXinConfigByAppId(ThirdUtil.getAppId())).token();
		BaseResult baseResult = wxMpService.decryptingOpenData(decryptingDto.getEncryptedData(), decryptingDto.getIv(),
				decryptingDto.getSessionKey());
		return new ResponseEntity<>(baseResult, HttpStatus.OK);
	}

}
