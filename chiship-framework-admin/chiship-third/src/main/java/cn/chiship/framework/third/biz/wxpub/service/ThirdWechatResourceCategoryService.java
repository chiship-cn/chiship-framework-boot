package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategory;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategoryExample;

/**
 * 微信资源分类业务接口层 2022/7/16
 *
 * @author lijian
 */
public interface ThirdWechatResourceCategoryService
		extends BaseService<ThirdWechatResourceCategory, ThirdWechatResourceCategoryExample> {

}
