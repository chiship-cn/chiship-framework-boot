package cn.chiship.framework.third.biz.all.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-11-26
 */
public class ThirdApplicationQrcode implements Serializable {

	/**
	 * 创建时间
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	/**
	 * 应用类型 1 公众号 2 小程序 3 企业微信 4 钉钉
	 */
	private Byte applicationType;

	private String appId;

	/**
	 * 标题
	 */
	private String title;

	/**
	 * 0 临时二维码 1 永久二维码
	 */
	private Byte type;

	/**
	 * 超时时间
	 */
	private Integer expireSeconds;

	/**
	 * 内容
	 */
	private String article;

	/**
	 * 内容类型 0 纯文本 1 资源库选择
	 */
	private Byte articleType;

	/**
	 * 跳转页面
	 */
	private String page;

	/**
	 * 场景值
	 */
	private String scene;

	/**
	 * 扩展参数1
	 */
	private String ext1;

	/**
	 * 扩展参数2
	 */
	private String ext2;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public Byte getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(Byte applicationType) {
		this.applicationType = applicationType;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Integer getExpireSeconds() {
		return expireSeconds;
	}

	public void setExpireSeconds(Integer expireSeconds) {
		this.expireSeconds = expireSeconds;
	}

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public Byte getArticleType() {
		return articleType;
	}

	public void setArticleType(Byte articleType) {
		this.articleType = articleType;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getScene() {
		return scene;
	}

	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getExt1() {
		return ext1;
	}

	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}

	public String getExt2() {
		return ext2;
	}

	public void setExt2(String ext2) {
		this.ext2 = ext2;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", applicationType=").append(applicationType);
		sb.append(", appId=").append(appId);
		sb.append(", title=").append(title);
		sb.append(", type=").append(type);
		sb.append(", expireSeconds=").append(expireSeconds);
		sb.append(", article=").append(article);
		sb.append(", articleType=").append(articleType);
		sb.append(", page=").append(page);
		sb.append(", scene=").append(scene);
		sb.append(", ext1=").append(ext1);
		sb.append(", ext2=").append(ext2);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ThirdApplicationQrcode other = (ThirdApplicationQrcode) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getApplicationType() == null ? other.getApplicationType() == null
						: this.getApplicationType().equals(other.getApplicationType()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getTitle() == null ? other.getTitle() == null : this.getTitle().equals(other.getTitle()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getExpireSeconds() == null ? other.getExpireSeconds() == null
						: this.getExpireSeconds().equals(other.getExpireSeconds()))
				&& (this.getArticle() == null ? other.getArticle() == null
						: this.getArticle().equals(other.getArticle()))
				&& (this.getArticleType() == null ? other.getArticleType() == null
						: this.getArticleType().equals(other.getArticleType()))
				&& (this.getPage() == null ? other.getPage() == null : this.getPage().equals(other.getPage()))
				&& (this.getScene() == null ? other.getScene() == null : this.getScene().equals(other.getScene()))
				&& (this.getExt1() == null ? other.getExt1() == null : this.getExt1().equals(other.getExt1()))
				&& (this.getExt2() == null ? other.getExt2() == null : this.getExt2().equals(other.getExt2()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getApplicationType() == null) ? 0 : getApplicationType().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getTitle() == null) ? 0 : getTitle().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getExpireSeconds() == null) ? 0 : getExpireSeconds().hashCode());
		result = prime * result + ((getArticle() == null) ? 0 : getArticle().hashCode());
		result = prime * result + ((getArticleType() == null) ? 0 : getArticleType().hashCode());
		result = prime * result + ((getPage() == null) ? 0 : getPage().hashCode());
		result = prime * result + ((getScene() == null) ? 0 : getScene().hashCode());
		result = prime * result + ((getExt1() == null) ? 0 : getExt1().hashCode());
		result = prime * result + ((getExt2() == null) ? 0 : getExt2().hashCode());
		return result;
	}

}