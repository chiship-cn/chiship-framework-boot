package cn.chiship.framework.third.biz.wxpub.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.third.biz.all.service.ThirdCacheService;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatTagService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTag;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatTagExample;
import cn.chiship.framework.third.biz.wxpub.pojo.dto.ThirdWechatTagDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 微信用户标签控制层 2022/7/8
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/thirdWechatTag")
@Api(tags = "微信用户标签")
public class ThirdWechatTagController extends BaseController<ThirdWechatTag, ThirdWechatTagExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatTagController.class);

	@Resource
	private ThirdWechatTagService thirdWechatTagService;

	@Resource
	private ThirdCacheService cacheService;

	@Override
	public BaseService getService() {
		return thirdWechatTagService;
	}

	@SystemOptionAnnotation(describe = "微信用户标签分页")
	@ApiOperation(value = "微信用户标签分页")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "page", value = "当前页码", defaultValue = "1", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "limit", value = "每页条数", defaultValue = "10", dataTypeClass = Long.class,
					paramType = "query"),
			@ApiImplicitParam(name = "sort", value = "排序方式", defaultValue = "-gmtModified",
					dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),

	})
	@GetMapping(value = "/page")
	public ResponseEntity<BaseResult> page(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		ThirdWechatTagExample thirdWechatTagExample = new ThirdWechatTagExample();
		// 创造条件
		ThirdWechatTagExample.Criteria thirdWechatTagCriteria = thirdWechatTagExample.createCriteria();
		thirdWechatTagCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		thirdWechatTagCriteria.andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			thirdWechatTagCriteria.andTagNameLike("%" + keyword + "%");
		}
		return super.responseEntity(BaseResult.ok(super.page(thirdWechatTagExample)));
	}

	@SystemOptionAnnotation(describe = "微信用户标签列表数据")
	@ApiOperation(value = "微信用户标签列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
		ThirdWechatTagExample thirdWechatTagExample = new ThirdWechatTagExample();
		// 创造条件
		ThirdWechatTagExample.Criteria thirdWechatTagCriteria = thirdWechatTagExample.createCriteria();
		thirdWechatTagCriteria.andIsDeletedEqualTo(BaseConstants.NO);
		thirdWechatTagCriteria.andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			thirdWechatTagCriteria.andTagNameLike("%" + keyword + "%");
		}
		return super.responseEntity(BaseResult.ok(super.list(thirdWechatTagExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存微信用户标签")
	@ApiOperation(value = "保存微信用户标签")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(@RequestBody @Valid ThirdWechatTagDto thirdWechatTagDto) {
		ThirdWechatTag thirdWechatTag = new ThirdWechatTag();
		BeanUtils.copyProperties(thirdWechatTagDto, thirdWechatTag);
		return super.responseEntity(super.save(thirdWechatTag));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新微信用户标签")
	@ApiOperation(value = "更新微信用户标签")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ThirdWechatTagDto thirdWechatTagDto) {
		ThirdWechatTag thirdWechatTag = new ThirdWechatTag();
		BeanUtils.copyProperties(thirdWechatTagDto, thirdWechatTag);
		return super.responseEntity(super.update(id, thirdWechatTag));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除微信用户标签")
	@ApiOperation(value = "删除微信用户标签")
	@PostMapping(value = "remove")
	@NoParamsSign
	public ResponseEntity<BaseResult> remove(@RequestBody(required = true) List<String> ids) {
		ThirdWechatTagExample thirdWechatTagExample = new ThirdWechatTagExample();
		thirdWechatTagExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(thirdWechatTagExample));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "同步创建用户标签")
	@ApiOperation(value = "同步创建用户标签")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "syncSave/{id}")
	public ResponseEntity<BaseResult> syncSave(@PathVariable("id") String id,
			@RequestBody @Valid ThirdWechatTagDto thirdWechatTagDto) {
		ThirdWechatTag thirdWechatTag = new ThirdWechatTag();
		BeanUtils.copyProperties(thirdWechatTagDto, thirdWechatTag);
		thirdWechatTag.setId(id);
		thirdWechatTag.setAppId(ThirdUtil.getAppId());
		return super.responseEntity(thirdWechatTagService.syncSave(thirdWechatTag));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "同步用户标签")
	@ApiOperation(value = "同步用户标签")
	@GetMapping(value = "syncTags")
	public ResponseEntity<BaseResult> syncTags() {
		return super.responseEntity(thirdWechatTagService.syncTags());
	}

}
