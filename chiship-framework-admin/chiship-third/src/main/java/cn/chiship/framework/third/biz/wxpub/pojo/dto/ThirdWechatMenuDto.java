package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 微信菜单表单 2022/6/19
 *
 * @author LiJian
 */
@ApiModel(value = "微信菜单表单")
public class ThirdWechatMenuDto {

	@ApiModelProperty(value = "公众号AppId")
	private String appId;

	@ApiModelProperty(value = "自定义菜单名称")
	@NotEmpty(message = "自定义菜单名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 8)
	private String name;

	@ApiModelProperty(value = "自定义菜单类型")
	@NotEmpty(message = "自定义菜单类型" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1)
	private String type;

	@ApiModelProperty(value = "自定义菜单上级")
	private String pid;

	@ApiModelProperty(value = "自定义菜单值")
	@NotEmpty(message = "自定义菜单值" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1)
	private String eventValue;

	@ApiModelProperty(value = "排序")
	@NotNull(message = "自定义菜单排序" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(99)
	private Long orders;

	@ApiModelProperty(value = "小程序ID")
	private String mpAppId;

	@ApiModelProperty(value = "小程序路径")
	private String mpPagePath;

	@ApiModelProperty(value = "小程序URL")
	private String mpUrl;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getEventValue() {
		return eventValue;
	}

	public void setEventValue(String eventValue) {
		this.eventValue = eventValue;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	public String getMpAppId() {
		return mpAppId;
	}

	public void setMpAppId(String mpAppId) {
		this.mpAppId = mpAppId;
	}

	public String getMpPagePath() {
		return mpPagePath;
	}

	public void setMpPagePath(String mpPagePath) {
		this.mpPagePath = mpPagePath;
	}

	public String getMpUrl() {
		return mpUrl;
	}

	public void setMpUrl(String mpUrl) {
		this.mpUrl = mpUrl;
	}

}
