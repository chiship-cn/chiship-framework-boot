package cn.chiship.framework.third.biz.wxpub.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.constants.CommonConstants;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatResourceCategoryService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategory;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatResourceCategoryExample;
import cn.chiship.framework.third.biz.wxpub.pojo.dto.ThirdWechatResourceCategoryDto;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 微信资源分类控制层 2022/7/16
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/thirdWechatResourceCategory")
@Api(tags = "微信资源分类")
public class ThirdWechatResourceCategoryController
		extends BaseController<ThirdWechatResourceCategory, ThirdWechatResourceCategoryExample> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatResourceCategoryController.class);

	@Resource
	private ThirdWechatResourceCategoryService thirdWechatResourceCategoryService;

	@Override
	public BaseService getService() {
		return thirdWechatResourceCategoryService;
	}

	/**
	 * 加载类型树形表格
	 */
	@SystemOptionAnnotation(describe = "加载类型树形表格", systemName = CommonConstants.SYSTEM_NAME_WECHAT)
	@ApiOperation(value = "加载类型树形表格")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型", required = true, dataTypeClass = Byte.class,
					paramType = "query"), })
	@GetMapping("loadTree")
	public ResponseEntity<BaseResult> loadTree(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(value = "type") Byte type) {
		ThirdWechatResourceCategoryExample thirdWechatResourceCategoryExample = new ThirdWechatResourceCategoryExample();
		// 创造条件
		ThirdWechatResourceCategoryExample.Criteria thirdWechatResourceCategoryCriteria = thirdWechatResourceCategoryExample
				.createCriteria();
		thirdWechatResourceCategoryCriteria.andIsDeletedEqualTo(BaseConstants.NO).andTypeEqualTo(type)
				.andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			thirdWechatResourceCategoryCriteria.andNameLike("%" + keyword + "%");
		}
		List<ThirdWechatResourceCategory> wechatResourceCategoryList = super.list(thirdWechatResourceCategoryExample);
		List<JSONObject> treeVos = assemblyCategory("0", wechatResourceCategoryList);
		return super.responseEntity(BaseResult.ok(treeVos));
	}

	/**
	 * 递归组装类型树
	 */
	public List<JSONObject> assemblyCategory(String pid, List<ThirdWechatResourceCategory> wechatResourceCategoryList) {

		List<JSONObject> treeVos = new ArrayList<>();
		for (ThirdWechatResourceCategory wechatResourceCategory : wechatResourceCategoryList) {
			if (wechatResourceCategory.getPid().equals(pid)) {
				treeVos.add(JSON.parseObject(JSON.toJSONString(wechatResourceCategory)));
			}
		}
		for (int i = 0; i < treeVos.size(); i++) {
			List<JSONObject> children = assemblyCategory(treeVos.get(i).getString("id"), wechatResourceCategoryList);
			treeVos.get(i).put("children", children);
			if (children.isEmpty()) {
				treeVos.get(i).put("hasChildren", false);
			}
		}
		return treeVos;
	}

	@SystemOptionAnnotation(describe = "微信资源分类列表数据")
	@ApiOperation(value = "微信资源分类列表数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),
			@ApiImplicitParam(name = "type", value = "类型", required = true, dataTypeClass = Byte.class,
					paramType = "query"), })
	@GetMapping(value = "/list")
	public ResponseEntity<BaseResult> list(
			@RequestParam(required = false, defaultValue = "", value = "keyword") String keyword,
			@RequestParam(value = "type") Byte type) {
		ThirdWechatResourceCategoryExample thirdWechatResourceCategoryExample = new ThirdWechatResourceCategoryExample();
		// 创造条件
		ThirdWechatResourceCategoryExample.Criteria thirdWechatResourceCategoryCriteria = thirdWechatResourceCategoryExample
				.createCriteria();
		thirdWechatResourceCategoryCriteria.andIsDeletedEqualTo(BaseConstants.NO).andTypeEqualTo(type)
				.andAppIdEqualTo(ThirdUtil.getAppId());
		if (!StringUtil.isNullOrEmpty(keyword)) {
			thirdWechatResourceCategoryCriteria.andNameLike("%" + keyword + "%");
		}
		return super.responseEntity(BaseResult.ok(super.list(thirdWechatResourceCategoryExample)));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存微信资源分类")
	@ApiOperation(value = "保存微信资源分类")
	@PostMapping(value = "save")
	public ResponseEntity<BaseResult> save(
			@RequestBody @Valid ThirdWechatResourceCategoryDto thirdWechatResourceCategoryDto) {
		ThirdWechatResourceCategory thirdWechatResourceCategory = new ThirdWechatResourceCategory();
		BeanUtils.copyProperties(thirdWechatResourceCategoryDto, thirdWechatResourceCategory);
		return super.responseEntity(super.save(thirdWechatResourceCategory));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新微信资源分类")
	@ApiOperation(value = "更新微信资源分类")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
			required = true) })
	@PostMapping(value = "update/{id}")
	public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
			@RequestBody @Valid ThirdWechatResourceCategoryDto thirdWechatResourceCategoryDto) {
		ThirdWechatResourceCategory thirdWechatResourceCategory = new ThirdWechatResourceCategory();
		BeanUtils.copyProperties(thirdWechatResourceCategoryDto, thirdWechatResourceCategory);
		return super.responseEntity(super.update(id, thirdWechatResourceCategory));
	}

	@SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除微信资源分类")
	@ApiOperation(value = "删除微信资源分类")
	@PostMapping(value = "remove")
	public ResponseEntity<BaseResult> remove(@RequestBody(required = true) List<String> ids) {
		ThirdWechatResourceCategoryExample thirdWechatResourceCategoryExample = new ThirdWechatResourceCategoryExample();
		thirdWechatResourceCategoryExample.createCriteria().andIdIn(ids);
		return super.responseEntity(super.remove(thirdWechatResourceCategoryExample));
	}

}
