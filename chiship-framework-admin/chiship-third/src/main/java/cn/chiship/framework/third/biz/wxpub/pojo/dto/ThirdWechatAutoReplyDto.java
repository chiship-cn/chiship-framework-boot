package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 自动回复表单 2024/7/11
 *
 * @author LiJian
 */
@ApiModel(value = "自动回复表单")
public class ThirdWechatAutoReplyDto {

	@ApiModelProperty(value = "公众号AppId")
	private String appId;

	@ApiModelProperty(value = "回复类型   0：关注时回复  1：消息回复 2：关键词回复  3:微信菜单", required = true)
	@NotNull
	@Min(0)
	@Max(3)
	private Byte replayType;

	@ApiModelProperty(value = "类型  0 图片  1 语音  2 视频  3 文本  4 图文", required = true)
	@NotNull
	@Min(0)
	@Max(4)
	private Byte resourceType;

	@ApiModelProperty(value = "文字内容")
	private String textContent;

	@ApiModelProperty(value = "标题")
	private String title;

	@ApiModelProperty(value = "素材ID")
	private String mediaId;

	@ApiModelProperty(value = "素材路径")
	private String fileUrl;

	@ApiModelProperty(value = "文章配置，最多允许8条")
	private String articleContent;

	@ApiModelProperty(value = "关键字")
	private String keyword;

	@ApiModelProperty(value = "匹配类型  0半匹配 1全匹配")
	private Byte matchingType;

	@ApiModelProperty(value = "消息类型")
	private String messageType;

	@ApiModelProperty(value = "关联主键")
	private String relationConfigId;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public Byte getReplayType() {
		return replayType;
	}

	public void setReplayType(Byte replayType) {
		this.replayType = replayType;
	}

	public Byte getResourceType() {
		return resourceType;
	}

	public void setResourceType(Byte resourceType) {
		this.resourceType = resourceType;
	}

	public String getTextContent() {
		return textContent;
	}

	public void setTextContent(String textContent) {
		this.textContent = textContent;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getFileUrl() {
		return fileUrl;
	}

	public void setFileUrl(String fileUrl) {
		this.fileUrl = fileUrl;
	}

	public String getArticleContent() {
		return articleContent;
	}

	public void setArticleContent(String articleContent) {
		this.articleContent = articleContent;
	}

	public Byte getMatchingType() {
		return matchingType;
	}

	public void setMatchingType(Byte matchingType) {
		this.matchingType = matchingType;
	}

	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getRelationConfigId() {
		return relationConfigId;
	}

	public void setRelationConfigId(String relationConfigId) {
		this.relationConfigId = relationConfigId;
	}

}
