package cn.chiship.framework.third.biz.zfb.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.framework.third.biz.zfb.pojo.dto.DecryptingDto;
import cn.chiship.sdk.core.annotation.NoParamsSign;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.third.service.ZfbMpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author lijian
 */
@RestController
@RequestMapping("/zfbMini")
@Api(tags = "支付宝小程序登录控制器")
public class ZfbMiniController {

    @Resource
    ZfbMpService zfbMpService;

    @Resource
    private GlobalCacheService globalCacheService;

    @SystemOptionAnnotation(describe = "Code换取授权访问令牌", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "Code换取授权访问令牌")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "code", value = "小程序登录获得的Code", required = true, dataTypeClass = String.class, paramType = "query")
    })
    @GetMapping("/getOauthToken")
    public ResponseEntity<BaseResult> getOauthToken(@RequestParam(value = "code") String code) {
        zfbMpService.config();
        return new ResponseEntity<>(zfbMpService.getOauthToken(code), HttpStatus.OK);

    }

    @SystemOptionAnnotation(describe = "解密小程序敏感数据", option = BusinessTypeEnum.SYSTEM_OPTION_OTHER)
    @ApiOperation(value = "解密小程序敏感数据")
    @PostMapping("/decryptingOpenData")
    @NoParamsSign
    public ResponseEntity<BaseResult> decryptingOpenData(@RequestBody @Valid DecryptingDto decryptingDto) {
        zfbMpService.config();
        BaseResult baseResult = zfbMpService.decryptingOpenData(decryptingDto.getEncryptedData(), decryptingDto.getSign());
        return new ResponseEntity<>(baseResult, HttpStatus.OK);
    }

}
