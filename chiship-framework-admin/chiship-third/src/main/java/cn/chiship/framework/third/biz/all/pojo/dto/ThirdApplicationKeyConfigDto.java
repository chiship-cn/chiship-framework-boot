package cn.chiship.framework.third.biz.all.pojo.dto;

import cn.chiship.framework.third.core.common.ApplicationTypeEnum;
import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 三方应用集成配置表单 2022/6/12
 *
 * @author LiJian
 */
@ApiModel(value = "三方应用集成配置表单")
public class ThirdApplicationKeyConfigDto {

	@ApiModelProperty(value = "主键id")
	private String id;

	@ApiModelProperty(value = "三方应用名称", required = true)
	@NotEmpty(message = "三方应用名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String name;

	@ApiModelProperty(value = "三方应用APP_ID", required = true)
	@NotEmpty(message = "三方应用APP_ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String appId;

	@ApiModelProperty(value = "三方应用Key")
	@Length(min = 1, max = 100)
	private String appKey;

	@ApiModelProperty(value = "三方应用密钥", required = true)
	@NotEmpty(message = "三方应用密钥" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String appSecret;

	@ApiModelProperty(value = "三方类型")
	@NotNull(message = "三方类型 " + BaseTipConstants.NOT_EMPTY)
	private ApplicationTypeEnum applicationTypeEnum;

	@ApiModelProperty(value = "服务器地址")
	private String serverUrl;

	@ApiModelProperty(value = "请求Token")
	private String serverToken;

	@ApiModelProperty(value = "描述")
	private String remark;

	@ApiModelProperty(value = "封面 ")
	private String photo;

	@ApiModelProperty(value = "备用地址1 ")
	private String url1;

	@ApiModelProperty(value = "备用地址2 ")
	private String url2;

	@ApiModelProperty(value = "备用地址3 ")
	private String url3;

	@ApiModelProperty(value = "备用数值1 ")
	private String value1;

	@ApiModelProperty(value = "备用数值2 ")
	private String value2;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public ApplicationTypeEnum getApplicationTypeEnum() {
		return applicationTypeEnum;
	}

	public void setApplicationTypeEnum(ApplicationTypeEnum applicationTypeEnum) {
		this.applicationTypeEnum = applicationTypeEnum;
	}

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getServerToken() {
		return serverToken;
	}

	public void setServerToken(String serverToken) {
		this.serverToken = serverToken;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getUrl1() {
		return url1;
	}

	public void setUrl1(String url1) {
		this.url1 = url1;
	}

	public String getUrl2() {
		return url2;
	}

	public void setUrl2(String url2) {
		this.url2 = url2;
	}

	public String getUrl3() {
		return url3;
	}

	public void setUrl3(String url3) {
		this.url3 = url3;
	}

	public String getValue1() {
		return value1;
	}

	public void setValue1(String value1) {
		this.value1 = value1;
	}

	public String getValue2() {
		return value2;
	}

	public void setValue2(String value2) {
		this.value2 = value2;
	}

}
