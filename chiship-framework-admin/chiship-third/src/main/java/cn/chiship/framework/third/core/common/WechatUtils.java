package cn.chiship.framework.third.core.common;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatUser;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @author lijian
 */
public class WechatUtils {

	private WechatUtils() {

	}

	/**
	 * json转WechatUser对象
	 */
	public static ThirdWechatUser convertJsonToWechatUser(JSONObject jsonObject) {
		if (null != jsonObject) {
			ThirdWechatUser wechatUser = JSON.parseObject(jsonObject.toJSONString(), ThirdWechatUser.class);
			wechatUser.setHeadImageUrl(jsonObject.get("headimgurl").toString());
			wechatUser.setSubscripeTime(Long.parseLong(jsonObject.get("subscribe_time").toString()));
			wechatUser.setIsCancle(BaseConstants.NO);
			return wechatUser;
		}
		else {
			return null;
		}
	}

	public static PrintWriter getWriter(HttpServletResponse response) {
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		return writer;
	}

	/**
	 * 打印字符串
	 * @param response
	 * @param msg
	 */
	public static void printlnStr(HttpServletResponse response, String msg) {
		PrintWriter writer = getWriter(response);
		writer.println(msg);
	}

}
