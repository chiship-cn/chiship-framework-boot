package cn.chiship.framework.third.biz.wxpub.controller;

import cn.chiship.framework.common.annotation.SystemOptionAnnotation;
import cn.chiship.framework.common.constants.CommonConstants;
import cn.chiship.framework.common.enums.BusinessTypeEnum;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.sdk.framework.base.BaseController;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.annotation.Authorization;

import javax.annotation.Resource;

import cn.chiship.framework.third.biz.wxpub.service.ThirdWechatMenuService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenu;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenuExample;
import cn.chiship.framework.third.biz.wxpub.pojo.dto.ThirdWechatMenuDto;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 微信菜单控制层 2022/6/19
 *
 * @author lijian
 */
@RestController
@Authorization
@RequestMapping("/thirdWechatMenu")
@Api(tags = "微信菜单")
public class ThirdWechatMenuController extends BaseController<ThirdWechatMenu, ThirdWechatMenuExample> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdWechatMenuController.class);

    @Resource
    private ThirdWechatMenuService thirdWechatMenuService;

    @Override
    public BaseService getService() {
        return thirdWechatMenuService;
    }

    @ApiOperation(value = "自定义菜单树形表格(无分页)")
    @GetMapping(value = "/treeTable")
    public ResponseEntity<BaseResult> treeTable() {
        List<JSONObject> weChatMenus = thirdWechatMenuService.treeTable(ThirdUtil.getAppId());
        return super.responseEntity(BaseResult.ok(weChatMenus));
    }

    @ApiOperation(value = "微信菜单列表数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "keyword", value = "关键字", dataTypeClass = String.class, paramType = "query"),})
    @GetMapping(value = "/list")
    public ResponseEntity<BaseResult> list(
            @RequestParam(required = false, defaultValue = "", value = "keyword") String keyword) {
        ThirdWechatMenuExample thirdWechatMenuExample = new ThirdWechatMenuExample();
        // 创造条件
        ThirdWechatMenuExample.Criteria thirdWechatMenuCriteria = thirdWechatMenuExample.createCriteria();
        thirdWechatMenuCriteria.andIsDeletedEqualTo(BaseConstants.NO);
        if (!StringUtil.isNullOrEmpty(keyword)) {
        }
        return super.responseEntity(BaseResult.ok(super.list(thirdWechatMenuExample)));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_SAVE, describe = "保存微信菜单")
    @ApiOperation(value = "保存微信菜单")
    @PostMapping(value = "save")
    public ResponseEntity<BaseResult> save(@RequestBody @Valid ThirdWechatMenuDto thirdWechatMenuDto) {
        ThirdWechatMenu thirdWechatMenu = new ThirdWechatMenu();
        BeanUtils.copyProperties(thirdWechatMenuDto, thirdWechatMenu);
        thirdWechatMenu.setAppId(ThirdUtil.getAppId());
        thirdWechatMenu.setAdminId(getUserId());
        thirdWechatMenu.setAdminName(getUserName());
        return super.responseEntity(super.save(thirdWechatMenu));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_UPDATE, describe = "更新微信菜单")
    @ApiOperation(value = "更新微信菜单")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "主键", dataTypeClass = String.class, paramType = "path",
            required = true)})
    @PostMapping(value = "update/{id}")
    public ResponseEntity<BaseResult> update(@PathVariable("id") String id,
                                             @RequestBody @Valid ThirdWechatMenuDto thirdWechatMenuDto) {
        ThirdWechatMenu thirdWechatMenu = new ThirdWechatMenu();
        BeanUtils.copyProperties(thirdWechatMenuDto, thirdWechatMenu);
        return super.responseEntity(super.update(id, thirdWechatMenu));
    }

    @SystemOptionAnnotation(option = BusinessTypeEnum.SYSTEM_OPTION_REMOVE, describe = "删除微信菜单")
    @ApiOperation(value = "删除微信菜单")
    @PostMapping(value = "remove")
    public ResponseEntity<BaseResult> remove(@RequestBody(required = true) List<String> ids) {
        ThirdWechatMenuExample thirdWechatMenuExample = new ThirdWechatMenuExample();
        thirdWechatMenuExample.createCriteria().andIdIn(ids);
        return super.responseEntity(super.remove(thirdWechatMenuExample));
    }

    @SystemOptionAnnotation(describe = "生成菜单", systemName = CommonConstants.SYSTEM_NAME_WECHAT)
    @ApiOperation(value = "生成菜单")
    @GetMapping(value = "generate")
    public ResponseEntity<BaseResult> generate() {
        return super.responseEntity(thirdWechatMenuService.releaseMenu(ThirdUtil.getAppId()));
    }

}
