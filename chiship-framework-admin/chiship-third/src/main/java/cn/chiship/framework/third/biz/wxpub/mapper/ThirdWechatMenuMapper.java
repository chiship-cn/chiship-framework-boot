package cn.chiship.framework.third.biz.wxpub.mapper;

import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenu;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatMenuExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * @author lijian
 */
public interface ThirdWechatMenuMapper extends BaseMapper<ThirdWechatMenu, ThirdWechatMenuExample> {

}