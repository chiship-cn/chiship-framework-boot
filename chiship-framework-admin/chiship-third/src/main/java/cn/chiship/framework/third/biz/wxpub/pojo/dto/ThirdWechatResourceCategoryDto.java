package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 微信资源分类表单 2022/7/16
 *
 * @author LiJian
 */
@ApiModel(value = "微信资源分类表单")
public class ThirdWechatResourceCategoryDto {

	@ApiModelProperty(value = "公众号AppId")
	private String appId;

	@ApiModelProperty(value = "分类名称", required = true)
	@NotEmpty(message = "分类名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String name;

	@ApiModelProperty(value = "类型(0 图片  1 语音  2 视频  3 文本  4 图文 有上下级)", required = true)
	@NotNull
	private Byte type;

	@ApiModelProperty(value = "所属上级")
	private String pid;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

}
