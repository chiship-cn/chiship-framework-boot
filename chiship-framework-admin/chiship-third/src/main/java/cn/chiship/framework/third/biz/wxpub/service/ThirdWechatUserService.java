package cn.chiship.framework.third.biz.wxpub.service;

import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatUser;
import cn.chiship.framework.third.biz.wxpub.entity.ThirdWechatUserExample;

import java.util.List;

/**
 * 微信用户业务接口层 2022/6/18
 *
 * @author lijian
 */
public interface ThirdWechatUserService extends BaseService<ThirdWechatUser, ThirdWechatUserExample> {

    /**
     * 根据AppId及OpenID获取用户
     *
     * @param appId
     * @param openId
     * @return
     */
    BaseResult getByAppIdAndOpenId(String appId, String openId);

    /**
     * 保存用户
     *
     * @param wechatUser
     */
    void saveWechatUser(ThirdWechatUser wechatUser);

    /**
     * 批量新增
     *
     * @param wechatUsers
     * @return
     */
    BaseResult insertSelectiveBatch(List<ThirdWechatUser> wechatUsers);

    /**
     * 同步粉丝
     *
     * @return
     */
    BaseResult syncUser();

}
