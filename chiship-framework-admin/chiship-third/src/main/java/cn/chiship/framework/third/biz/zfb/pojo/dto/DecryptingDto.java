package cn.chiship.framework.third.biz.zfb.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author lijian
 */
@ApiModel(value = "小程序待解密数据表单")
public class DecryptingDto {

    @ApiModelProperty(value = "敏感数据在内的完整用户信息的加密数据", required = true)
    @NotEmpty(message = "敏感数据在内的完整用户信息的加密数据" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1)
    String encryptedData;

    @ApiModelProperty(value = "签名", required = true)
    @NotEmpty(message = "签名" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1)
    String sign;

    public String getEncryptedData() {
        return encryptedData;
    }

    public void setEncryptedData(String encryptedData) {
        this.encryptedData = encryptedData;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
