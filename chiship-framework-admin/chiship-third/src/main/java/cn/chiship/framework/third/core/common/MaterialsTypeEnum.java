package cn.chiship.framework.third.core.common;

/**
 * @author lj 素材类型
 */

public enum MaterialsTypeEnum {

	/**
	 * 图片
	 */
	BASIC_MATERIALS_TYPE_IMAGE(Byte.valueOf("0"), "image"),
	/**
	 * 语音
	 */
	BASIC_MATERIALS_TYPE_VOICE(Byte.valueOf("1"), "voice"),
	/**
	 * 视频
	 */
	BASIC_MATERIALS_TYPE_VIDEO(Byte.valueOf("2"), "video"),
	/**
	 * 文本
	 */
	BASIC_MATERIALS_TYPE_TEXT(Byte.valueOf("3"), "text"),
	/**
	 * 图文
	 */
	BASIC_MATERIALS_TYPE_ARTICLE(Byte.valueOf("4"), "article"),;

	private Byte type;

	private String message;

	MaterialsTypeEnum(Byte type, String message) {
		this.type = type;
		this.message = message;
	}

	public Byte getType() {
		return type;
	}

	public String getMessage() {
		return message;
	}

}
