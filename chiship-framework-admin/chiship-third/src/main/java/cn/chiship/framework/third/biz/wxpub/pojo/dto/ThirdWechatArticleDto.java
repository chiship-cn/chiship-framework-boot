package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.*;

/**
 * 文章素材表单 2022/7/13
 *
 * @author LiJian
 */
@ApiModel(value = "文章素材表单")
public class ThirdWechatArticleDto {


    @ApiModelProperty(value = "公众号AppId")
    private String appId;

    @ApiModelProperty(value = "标题", required = true)
    @NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
    @Length(min = 1, max = 50)
    private String title;

    @ApiModelProperty(value = "作者")
    private String author;

    @ApiModelProperty(value = "分类")
    private String categoryId;

    @ApiModelProperty(value = "封面")
    private String photo;

    @ApiModelProperty(value = "摘要")
    private String summary;

    @ApiModelProperty(value = "附件")
    private String attachment;

    @ApiModelProperty(value = "是否置顶", required = true)
    @Min(0)
    @Max(1)
    private Byte isTop;

    @ApiModelProperty(value = "是否原创", required = true)
    @Min(0)
    @Max(1)
    private Byte isOriginal;

    @ApiModelProperty(value = "来源")
    private String source;

    @ApiModelProperty(value = "来源url")
    private String sourceUrl;

    @ApiModelProperty(value = "类型   0 图片  1 语音  2 视频  3 文本  4 图文", required = true)
    @NotNull
    private Byte resourceType;

    @ApiModelProperty(value = "媒体ID")
    private String mediaId;

    @ApiModelProperty(value = "留言 0 不开启留言  1 所有人 2 仅粉丝")
    private Byte leavingMessage;

    @ApiModelProperty(value = "状态 0 草稿箱 1 未发布 2 已发布", required = true)
    @Min(0)
    @Max(2)
    private Byte status;

    @ApiModelProperty(value = "浏览数")
    private Long viewCount;

    @ApiModelProperty(value = "点赞数")
    private Long likeCount;

    @ApiModelProperty(value = "收藏数")
    private Long collectCount;


    @ApiModelProperty(value = "评论数")
    private Long commentCount;

    @ApiModelProperty(value = "正文")
    private String contentBody;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public Byte getIsTop() {
        return isTop;
    }

    public void setIsTop(Byte isTop) {
        this.isTop = isTop;
    }

    public Byte getIsOriginal() {
        return isOriginal;
    }

    public void setIsOriginal(Byte isOriginal) {
        this.isOriginal = isOriginal;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    public Byte getResourceType() {
        return resourceType;
    }

    public void setResourceType(Byte resourceType) {
        this.resourceType = resourceType;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public Byte getLeavingMessage() {
        return leavingMessage;
    }

    public void setLeavingMessage(Byte leavingMessage) {
        this.leavingMessage = leavingMessage;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public Long getViewCount() {
        return viewCount;
    }

    public void setViewCount(Long viewCount) {
        this.viewCount = viewCount;
    }

    public Long getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Long likeCount) {
        this.likeCount = likeCount;
    }

    public Long getCollectCount() {
        return collectCount;
    }

    public void setCollectCount(Long collectCount) {
        this.collectCount = collectCount;
    }

    public Long getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Long commentCount) {
        this.commentCount = commentCount;
    }

    public String getContentBody() {
        return contentBody;
    }

    public void setContentBody(String contentBody) {
        this.contentBody = contentBody;
    }
}
