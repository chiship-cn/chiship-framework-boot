package cn.chiship.framework.third.biz.wxpub.entity;

import java.io.Serializable;

/**
 * 实体
 *
 * @author lijian
 * @date 2024-07-18
 */
public class ThirdWechatMenu implements Serializable {

	/**
	 * 创建时间
	 */
	private String id;

	/**
	 * 创建时间
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 */
	private Byte isDeleted;

	private String appId;

	private String name;

	/**
	 * click 单击 view 链接 miniprogram 小程序
	 */
	private String type;

	private String pid;

	private String eventValue;

	private Long orders;

	private String adminId;

	private String adminName;

	/**
	 * 小程序ID
	 */
	private String mpAppId;

	/**
	 * 小程序路径
	 */
	private String mpPagePath;

	/**
	 * 小程序URL
	 */
	private String mpUrl;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getEventValue() {
		return eventValue;
	}

	public void setEventValue(String eventValue) {
		this.eventValue = eventValue;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getMpAppId() {
		return mpAppId;
	}

	public void setMpAppId(String mpAppId) {
		this.mpAppId = mpAppId;
	}

	public String getMpPagePath() {
		return mpPagePath;
	}

	public void setMpPagePath(String mpPagePath) {
		this.mpPagePath = mpPagePath;
	}

	public String getMpUrl() {
		return mpUrl;
	}

	public void setMpUrl(String mpUrl) {
		this.mpUrl = mpUrl;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", appId=").append(appId);
		sb.append(", name=").append(name);
		sb.append(", type=").append(type);
		sb.append(", pid=").append(pid);
		sb.append(", eventValue=").append(eventValue);
		sb.append(", orders=").append(orders);
		sb.append(", adminId=").append(adminId);
		sb.append(", adminName=").append(adminName);
		sb.append(", mpAppId=").append(mpAppId);
		sb.append(", mpPagePath=").append(mpPagePath);
		sb.append(", mpUrl=").append(mpUrl);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		ThirdWechatMenu other = (ThirdWechatMenu) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getAppId() == null ? other.getAppId() == null : this.getAppId().equals(other.getAppId()))
				&& (this.getName() == null ? other.getName() == null : this.getName().equals(other.getName()))
				&& (this.getType() == null ? other.getType() == null : this.getType().equals(other.getType()))
				&& (this.getPid() == null ? other.getPid() == null : this.getPid().equals(other.getPid()))
				&& (this.getEventValue() == null ? other.getEventValue() == null
						: this.getEventValue().equals(other.getEventValue()))
				&& (this.getOrders() == null ? other.getOrders() == null : this.getOrders().equals(other.getOrders()))
				&& (this.getAdminId() == null ? other.getAdminId() == null
						: this.getAdminId().equals(other.getAdminId()))
				&& (this.getAdminName() == null ? other.getAdminName() == null
						: this.getAdminName().equals(other.getAdminName()))
				&& (this.getMpAppId() == null ? other.getMpAppId() == null
						: this.getMpAppId().equals(other.getMpAppId()))
				&& (this.getMpPagePath() == null ? other.getMpPagePath() == null
						: this.getMpPagePath().equals(other.getMpPagePath()))
				&& (this.getMpUrl() == null ? other.getMpUrl() == null : this.getMpUrl().equals(other.getMpUrl()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getAppId() == null) ? 0 : getAppId().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getType() == null) ? 0 : getType().hashCode());
		result = prime * result + ((getPid() == null) ? 0 : getPid().hashCode());
		result = prime * result + ((getEventValue() == null) ? 0 : getEventValue().hashCode());
		result = prime * result + ((getOrders() == null) ? 0 : getOrders().hashCode());
		result = prime * result + ((getAdminId() == null) ? 0 : getAdminId().hashCode());
		result = prime * result + ((getAdminName() == null) ? 0 : getAdminName().hashCode());
		result = prime * result + ((getMpAppId() == null) ? 0 : getMpAppId().hashCode());
		result = prime * result + ((getMpPagePath() == null) ? 0 : getMpPagePath().hashCode());
		result = prime * result + ((getMpUrl() == null) ? 0 : getMpUrl().hashCode());
		return result;
	}

}