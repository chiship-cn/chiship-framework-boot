package cn.chiship.framework.third.biz.all.service.impl;

import cn.chiship.framework.common.pojo.vo.ThirdApplicationKeyConfigVo;
import cn.chiship.framework.common.service.GlobalCacheService;
import cn.chiship.framework.third.core.common.ApplicationTypeEnum;
import cn.chiship.framework.third.core.common.ThirdUtil;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import cn.chiship.sdk.core.util.ObjectUtil;
import cn.chiship.sdk.core.util.StringUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;
import cn.chiship.framework.third.biz.all.mapper.ThirdApplicationQrcodeMapper;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcode;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcodeExample;
import cn.chiship.framework.third.biz.all.service.ThirdApplicationQrcodeService;
import cn.chiship.sdk.third.service.WxMpService;
import cn.chiship.sdk.third.service.WxPubService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

/**
 * 二维码业务接口实现层 2024/11/25
 *
 * @author lijian
 */
@Service
public class ThirdApplicationQrcodeServiceImpl
        extends BaseServiceImpl<ThirdApplicationQrcode, ThirdApplicationQrcodeExample>
        implements ThirdApplicationQrcodeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ThirdApplicationQrcodeServiceImpl.class);

    @Resource
    ThirdApplicationQrcodeMapper thirdApplicationQrcodeMapper;

    @Resource
    WxPubService wxPubService;

    @Resource
    WxMpService wxMpService;

    @Resource
    GlobalCacheService globalCacheService;

    @Override
    public BaseResult insertSelective(ThirdApplicationQrcode applicationQrcode) {
        ThirdApplicationKeyConfigVo applicationKeyConfigVo = globalCacheService.getThirdApplicationConfigByAppId(ThirdUtil.getAppId());
        applicationQrcode.setAppId(applicationKeyConfigVo.getAppId());
        applicationQrcode.setApplicationType(applicationKeyConfigVo.getType());
        return super.insertSelective(applicationQrcode);
    }

    @Override
    public BaseResult generateQrCode(String id) {
        ThirdApplicationQrcode applicationQrcode = selectByPrimaryKey(id);
        if (ObjectUtil.isEmpty(applicationQrcode)) {
            return BaseResult.error("无效的二维码场景");
        }
        if (ApplicationTypeEnum.APPLICATION_TYPE_WX_PUB.getType().equals(applicationQrcode.getApplicationType())) {
            wxPubService.config(globalCacheService.getWeiXinConfigByAppId(applicationQrcode.getAppId())).token();
            if (BaseConstants.NO == (applicationQrcode.getType())) {
                // 临时二维码
                return wxPubService.getQrCodeByTemporary(applicationQrcode.getExpireSeconds(),
                        applicationQrcode.getScene());
            } else {
                // 永久二维码
                return wxPubService.getQrCodeByForever(applicationQrcode.getScene());
            }
        } else if (ApplicationTypeEnum.APPLICATION_TYPE_WX_MP.getType().equals(applicationQrcode.getApplicationType())) {
            wxMpService.config(globalCacheService.getWeiXinConfigByAppId(applicationQrcode.getAppId())).token();
            if (StringUtil.isNullOrEmpty(applicationQrcode.getScene())) {
                return BaseResult.error("场景值不能为空！");
            }
            return wxMpService.getQrCodeUnLimit(applicationQrcode.getScene(), applicationQrcode.getPage());
        }

        return BaseResult.error("还未集成");
    }

}
