package cn.chiship.framework.third.biz.all.mapper;

import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcode;
import cn.chiship.framework.third.biz.all.entity.ThirdApplicationQrcodeExample;

import cn.chiship.sdk.framework.base.BaseMapper;

/**
 * Mapper
 *
 * @author lijian
 * @date 2024-11-25
 */
public interface ThirdApplicationQrcodeMapper
		extends BaseMapper<ThirdApplicationQrcode, ThirdApplicationQrcodeExample> {

}