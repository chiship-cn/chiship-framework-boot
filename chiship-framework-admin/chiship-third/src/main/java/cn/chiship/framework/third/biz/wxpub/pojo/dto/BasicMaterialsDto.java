package cn.chiship.framework.third.biz.wxpub.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lj 基本素材表单
 */
public class BasicMaterialsDto {

	@ApiModelProperty(value = "公众号AppId")
	private String appId;

	@ApiModelProperty(value = "标题", required = true)
	@NotEmpty(message = "标题" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 50)
	private String title;

	@ApiModelProperty(value = "分类")
	private String categoryId;

	@ApiModelProperty(value = "内容")
	private String content;

	@ApiModelProperty(value = "类型   0 图片  1 语音  2 视频  3 文本", required = true)
	@NotNull
	private Byte resourceType;

	@ApiModelProperty(value = "媒体ID")
	private String mediaId;

	@ApiModelProperty(value = "文件夹")
	private String catalogId;


	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Byte getResourceType() {
		return resourceType;
	}

	public void setResourceType(Byte resourceType) {
		this.resourceType = resourceType;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
}
