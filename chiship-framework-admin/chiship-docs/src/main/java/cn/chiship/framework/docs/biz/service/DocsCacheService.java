package cn.chiship.framework.docs.biz.service;

/**
 * 缓存数据
 *
 * @author lijian
 */
public interface DocsCacheService {

	/**
	 * 缓存文件夹
	 */
	void cacheFileCatalog();

}
