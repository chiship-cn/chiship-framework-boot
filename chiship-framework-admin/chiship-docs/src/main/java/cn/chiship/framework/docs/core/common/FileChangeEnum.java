package cn.chiship.framework.docs.core.common;

/**
 * 文件变化枚举
 *
 * @author lijian
 */
public enum FileChangeEnum {

	/**
	 * 新建文件
	 */
	NEW(0, "新建文件"),
	/**
	 * 上传文件
	 */
	UPLOAD(1, "上传文件"),
	/**
	 * 文件重命名
	 */
	RENAME(2, "文件重命名"),
	/**
	 * 移入回收站
	 */
	MOVE_RECYCLE(3, "移入回收站"),

	/**
	 * 添加收藏
	 */
	ADD_COLLECTION(4, "添加收藏"),
	/**
	 * 取消收藏
	 */
	CANCEL_COLLECTION(5, "取消收藏"),
	/**
	 * 删除文件
	 */
	REMOVE_FILE(6, "删除文件"),

	/**
	 * 还原文件
	 */
	REDUCTION_FILE(7, "还原文件"),;

	private int code;

	private String message;

	FileChangeEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
