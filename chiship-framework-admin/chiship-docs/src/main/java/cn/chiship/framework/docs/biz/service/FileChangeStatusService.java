package cn.chiship.framework.docs.biz.service;

import cn.chiship.framework.docs.biz.entity.FileChangeStatusExample;
import cn.chiship.framework.docs.biz.pojo.dto.FileCatalogChangeStatusDto;
import cn.chiship.framework.docs.biz.pojo.dto.FileChangeStatusDto;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.docs.core.common.FileCatalogChangeEnum;
import cn.chiship.framework.docs.core.common.FileChangeEnum;
import cn.chiship.framework.docs.biz.entity.FileChangeStatus;

/**
 * FileChangeStatusService接口
 *
 * @author lijian
 * @date 2020/4/28
 */
public interface FileChangeStatusService extends BaseService<FileChangeStatus, FileChangeStatusExample> {

	/**
	 * 文件变化记录
	 * @param fileChangeStatusDto
	 * @param fileChangeEnum
	 * @return
	 */
	BaseResult saveChange(FileChangeStatusDto fileChangeStatusDto, FileChangeEnum fileChangeEnum);

	/**
	 * 文档变化记录
	 * @param fileCatalogChangeStatusDto
	 * @param fileCatalogChangeEnum
	 * @return
	 */
	BaseResult saveChange(FileCatalogChangeStatusDto fileCatalogChangeStatusDto,
			FileCatalogChangeEnum fileCatalogChangeEnum);

}
