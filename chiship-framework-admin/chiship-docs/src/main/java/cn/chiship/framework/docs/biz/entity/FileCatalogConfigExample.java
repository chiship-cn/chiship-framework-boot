package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class FileCatalogConfigExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public FileCatalogConfigExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidIsNull() {
			addCriterion("catalog_uuid is null");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidIsNotNull() {
			addCriterion("catalog_uuid is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidEqualTo(String value) {
			addCriterion("catalog_uuid =", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidNotEqualTo(String value) {
			addCriterion("catalog_uuid <>", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidGreaterThan(String value) {
			addCriterion("catalog_uuid >", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_uuid >=", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidLessThan(String value) {
			addCriterion("catalog_uuid <", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidLessThanOrEqualTo(String value) {
			addCriterion("catalog_uuid <=", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidLike(String value) {
			addCriterion("catalog_uuid like", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidNotLike(String value) {
			addCriterion("catalog_uuid not like", value, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidIn(List<String> values) {
			addCriterion("catalog_uuid in", values, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidNotIn(List<String> values) {
			addCriterion("catalog_uuid not in", values, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidBetween(String value1, String value2) {
			addCriterion("catalog_uuid between", value1, value2, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogUuidNotBetween(String value1, String value2) {
			addCriterion("catalog_uuid not between", value1, value2, "catalogUuid");
			return (Criteria) this;
		}

		public Criteria andCatalogNameIsNull() {
			addCriterion("catalog_name is null");
			return (Criteria) this;
		}

		public Criteria andCatalogNameIsNotNull() {
			addCriterion("catalog_name is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogNameEqualTo(String value) {
			addCriterion("catalog_name =", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameNotEqualTo(String value) {
			addCriterion("catalog_name <>", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameGreaterThan(String value) {
			addCriterion("catalog_name >", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_name >=", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameLessThan(String value) {
			addCriterion("catalog_name <", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameLessThanOrEqualTo(String value) {
			addCriterion("catalog_name <=", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameLike(String value) {
			addCriterion("catalog_name like", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameNotLike(String value) {
			addCriterion("catalog_name not like", value, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameIn(List<String> values) {
			addCriterion("catalog_name in", values, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameNotIn(List<String> values) {
			addCriterion("catalog_name not in", values, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameBetween(String value1, String value2) {
			addCriterion("catalog_name between", value1, value2, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogNameNotBetween(String value1, String value2) {
			addCriterion("catalog_name not between", value1, value2, "catalogName");
			return (Criteria) this;
		}

		public Criteria andCatalogPathIsNull() {
			addCriterion("catalog_path is null");
			return (Criteria) this;
		}

		public Criteria andCatalogPathIsNotNull() {
			addCriterion("catalog_path is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogPathEqualTo(String value) {
			addCriterion("catalog_path =", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathNotEqualTo(String value) {
			addCriterion("catalog_path <>", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathGreaterThan(String value) {
			addCriterion("catalog_path >", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_path >=", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathLessThan(String value) {
			addCriterion("catalog_path <", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathLessThanOrEqualTo(String value) {
			addCriterion("catalog_path <=", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathLike(String value) {
			addCriterion("catalog_path like", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathNotLike(String value) {
			addCriterion("catalog_path not like", value, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathIn(List<String> values) {
			addCriterion("catalog_path in", values, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathNotIn(List<String> values) {
			addCriterion("catalog_path not in", values, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathBetween(String value1, String value2) {
			addCriterion("catalog_path between", value1, value2, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPathNotBetween(String value1, String value2) {
			addCriterion("catalog_path not between", value1, value2, "catalogPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathIsNull() {
			addCriterion("catalog_full_path is null");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathIsNotNull() {
			addCriterion("catalog_full_path is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathEqualTo(String value) {
			addCriterion("catalog_full_path =", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathNotEqualTo(String value) {
			addCriterion("catalog_full_path <>", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathGreaterThan(String value) {
			addCriterion("catalog_full_path >", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_full_path >=", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathLessThan(String value) {
			addCriterion("catalog_full_path <", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathLessThanOrEqualTo(String value) {
			addCriterion("catalog_full_path <=", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathLike(String value) {
			addCriterion("catalog_full_path like", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathNotLike(String value) {
			addCriterion("catalog_full_path not like", value, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathIn(List<String> values) {
			addCriterion("catalog_full_path in", values, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathNotIn(List<String> values) {
			addCriterion("catalog_full_path not in", values, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathBetween(String value1, String value2) {
			addCriterion("catalog_full_path between", value1, value2, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogFullPathNotBetween(String value1, String value2) {
			addCriterion("catalog_full_path not between", value1, value2, "catalogFullPath");
			return (Criteria) this;
		}

		public Criteria andCatalogPidIsNull() {
			addCriterion("catalog_pid is null");
			return (Criteria) this;
		}

		public Criteria andCatalogPidIsNotNull() {
			addCriterion("catalog_pid is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogPidEqualTo(String value) {
			addCriterion("catalog_pid =", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidNotEqualTo(String value) {
			addCriterion("catalog_pid <>", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidGreaterThan(String value) {
			addCriterion("catalog_pid >", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_pid >=", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidLessThan(String value) {
			addCriterion("catalog_pid <", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidLessThanOrEqualTo(String value) {
			addCriterion("catalog_pid <=", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidLike(String value) {
			addCriterion("catalog_pid like", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidNotLike(String value) {
			addCriterion("catalog_pid not like", value, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidIn(List<String> values) {
			addCriterion("catalog_pid in", values, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidNotIn(List<String> values) {
			addCriterion("catalog_pid not in", values, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidBetween(String value1, String value2) {
			addCriterion("catalog_pid between", value1, value2, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andCatalogPidNotBetween(String value1, String value2) {
			addCriterion("catalog_pid not between", value1, value2, "catalogPid");
			return (Criteria) this;
		}

		public Criteria andIsChildrenIsNull() {
			addCriterion("is_children is null");
			return (Criteria) this;
		}

		public Criteria andIsChildrenIsNotNull() {
			addCriterion("is_children is not null");
			return (Criteria) this;
		}

		public Criteria andIsChildrenEqualTo(Byte value) {
			addCriterion("is_children =", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenNotEqualTo(Byte value) {
			addCriterion("is_children <>", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenGreaterThan(Byte value) {
			addCriterion("is_children >", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_children >=", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenLessThan(Byte value) {
			addCriterion("is_children <", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenLessThanOrEqualTo(Byte value) {
			addCriterion("is_children <=", value, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenIn(List<Byte> values) {
			addCriterion("is_children in", values, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenNotIn(List<Byte> values) {
			addCriterion("is_children not in", values, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenBetween(Byte value1, Byte value2) {
			addCriterion("is_children between", value1, value2, "isChildren");
			return (Criteria) this;
		}

		public Criteria andIsChildrenNotBetween(Byte value1, Byte value2) {
			addCriterion("is_children not between", value1, value2, "isChildren");
			return (Criteria) this;
		}

		public Criteria andProjectsIdIsNull() {
			addCriterion("projects_id is null");
			return (Criteria) this;
		}

		public Criteria andProjectsIdIsNotNull() {
			addCriterion("projects_id is not null");
			return (Criteria) this;
		}

		public Criteria andProjectsIdEqualTo(String value) {
			addCriterion("projects_id =", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdNotEqualTo(String value) {
			addCriterion("projects_id <>", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdGreaterThan(String value) {
			addCriterion("projects_id >", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdGreaterThanOrEqualTo(String value) {
			addCriterion("projects_id >=", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdLessThan(String value) {
			addCriterion("projects_id <", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdLessThanOrEqualTo(String value) {
			addCriterion("projects_id <=", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdLike(String value) {
			addCriterion("projects_id like", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdNotLike(String value) {
			addCriterion("projects_id not like", value, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdIn(List<String> values) {
			addCriterion("projects_id in", values, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdNotIn(List<String> values) {
			addCriterion("projects_id not in", values, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdBetween(String value1, String value2) {
			addCriterion("projects_id between", value1, value2, "projectsId");
			return (Criteria) this;
		}

		public Criteria andProjectsIdNotBetween(String value1, String value2) {
			addCriterion("projects_id not between", value1, value2, "projectsId");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNull() {
			addCriterion("user_name is null");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNotNull() {
			addCriterion("user_name is not null");
			return (Criteria) this;
		}

		public Criteria andUserNameEqualTo(String value) {
			addCriterion("user_name =", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotEqualTo(String value) {
			addCriterion("user_name <>", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThan(String value) {
			addCriterion("user_name >", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("user_name >=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThan(String value) {
			addCriterion("user_name <", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThanOrEqualTo(String value) {
			addCriterion("user_name <=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLike(String value) {
			addCriterion("user_name like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotLike(String value) {
			addCriterion("user_name not like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameIn(List<String> values) {
			addCriterion("user_name in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotIn(List<String> values) {
			addCriterion("user_name not in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameBetween(String value1, String value2) {
			addCriterion("user_name between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotBetween(String value1, String value2) {
			addCriterion("user_name not between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNull() {
			addCriterion("real_name is null");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNotNull() {
			addCriterion("real_name is not null");
			return (Criteria) this;
		}

		public Criteria andRealNameEqualTo(String value) {
			addCriterion("real_name =", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotEqualTo(String value) {
			addCriterion("real_name <>", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThan(String value) {
			addCriterion("real_name >", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("real_name >=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThan(String value) {
			addCriterion("real_name <", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThanOrEqualTo(String value) {
			addCriterion("real_name <=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLike(String value) {
			addCriterion("real_name like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotLike(String value) {
			addCriterion("real_name not like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameIn(List<String> values) {
			addCriterion("real_name in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotIn(List<String> values) {
			addCriterion("real_name not in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameBetween(String value1, String value2) {
			addCriterion("real_name between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotBetween(String value1, String value2) {
			addCriterion("real_name not between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNull() {
			addCriterion("is_built_in is null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNotNull() {
			addCriterion("is_built_in is not null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInEqualTo(Byte value) {
			addCriterion("is_built_in =", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotEqualTo(Byte value) {
			addCriterion("is_built_in <>", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThan(Byte value) {
			addCriterion("is_built_in >", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_built_in >=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThan(Byte value) {
			addCriterion("is_built_in <", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThanOrEqualTo(Byte value) {
			addCriterion("is_built_in <=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIn(List<Byte> values) {
			addCriterion("is_built_in in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotIn(List<Byte> values) {
			addCriterion("is_built_in not in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in not between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIsNull() {
			addCriterion("is_encryption is null");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIsNotNull() {
			addCriterion("is_encryption is not null");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionEqualTo(Byte value) {
			addCriterion("is_encryption =", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotEqualTo(Byte value) {
			addCriterion("is_encryption <>", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionGreaterThan(Byte value) {
			addCriterion("is_encryption >", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_encryption >=", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionLessThan(Byte value) {
			addCriterion("is_encryption <", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionLessThanOrEqualTo(Byte value) {
			addCriterion("is_encryption <=", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIn(List<Byte> values) {
			addCriterion("is_encryption in", values, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotIn(List<Byte> values) {
			addCriterion("is_encryption not in", values, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionBetween(Byte value1, Byte value2) {
			addCriterion("is_encryption between", value1, value2, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotBetween(Byte value1, Byte value2) {
			addCriterion("is_encryption not between", value1, value2, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIsNull() {
			addCriterion("encryption_password is null");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIsNotNull() {
			addCriterion("encryption_password is not null");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordEqualTo(String value) {
			addCriterion("encryption_password =", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotEqualTo(String value) {
			addCriterion("encryption_password <>", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordGreaterThan(String value) {
			addCriterion("encryption_password >", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordGreaterThanOrEqualTo(String value) {
			addCriterion("encryption_password >=", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLessThan(String value) {
			addCriterion("encryption_password <", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLessThanOrEqualTo(String value) {
			addCriterion("encryption_password <=", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLike(String value) {
			addCriterion("encryption_password like", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotLike(String value) {
			addCriterion("encryption_password not like", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIn(List<String> values) {
			addCriterion("encryption_password in", values, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotIn(List<String> values) {
			addCriterion("encryption_password not in", values, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordBetween(String value1, String value2) {
			addCriterion("encryption_password between", value1, value2, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotBetween(String value1, String value2) {
			addCriterion("encryption_password not between", value1, value2, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIsNull() {
			addCriterion("encryption_salt is null");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIsNotNull() {
			addCriterion("encryption_salt is not null");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltEqualTo(String value) {
			addCriterion("encryption_salt =", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotEqualTo(String value) {
			addCriterion("encryption_salt <>", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltGreaterThan(String value) {
			addCriterion("encryption_salt >", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltGreaterThanOrEqualTo(String value) {
			addCriterion("encryption_salt >=", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLessThan(String value) {
			addCriterion("encryption_salt <", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLessThanOrEqualTo(String value) {
			addCriterion("encryption_salt <=", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLike(String value) {
			addCriterion("encryption_salt like", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotLike(String value) {
			addCriterion("encryption_salt not like", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIn(List<String> values) {
			addCriterion("encryption_salt in", values, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotIn(List<String> values) {
			addCriterion("encryption_salt not in", values, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltBetween(String value1, String value2) {
			addCriterion("encryption_salt between", value1, value2, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotBetween(String value1, String value2) {
			addCriterion("encryption_salt not between", value1, value2, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeIsNull() {
			addCriterion("catalog_type is null");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeIsNotNull() {
			addCriterion("catalog_type is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeEqualTo(Byte value) {
			addCriterion("catalog_type =", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeNotEqualTo(Byte value) {
			addCriterion("catalog_type <>", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeGreaterThan(Byte value) {
			addCriterion("catalog_type >", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("catalog_type >=", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeLessThan(Byte value) {
			addCriterion("catalog_type <", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeLessThanOrEqualTo(Byte value) {
			addCriterion("catalog_type <=", value, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeIn(List<Byte> values) {
			addCriterion("catalog_type in", values, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeNotIn(List<Byte> values) {
			addCriterion("catalog_type not in", values, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeBetween(Byte value1, Byte value2) {
			addCriterion("catalog_type between", value1, value2, "catalogType");
			return (Criteria) this;
		}

		public Criteria andCatalogTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("catalog_type not between", value1, value2, "catalogType");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileIsNull() {
			addCriterion("is_allow_new_file is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileIsNotNull() {
			addCriterion("is_allow_new_file is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileEqualTo(Byte value) {
			addCriterion("is_allow_new_file =", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileNotEqualTo(Byte value) {
			addCriterion("is_allow_new_file <>", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileGreaterThan(Byte value) {
			addCriterion("is_allow_new_file >", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_new_file >=", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileLessThan(Byte value) {
			addCriterion("is_allow_new_file <", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_new_file <=", value, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileIn(List<Byte> values) {
			addCriterion("is_allow_new_file in", values, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileNotIn(List<Byte> values) {
			addCriterion("is_allow_new_file not in", values, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_new_file between", value1, value2, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFileNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_new_file not between", value1, value2, "isAllowNewFile");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderIsNull() {
			addCriterion("is_allow_new_folder is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderIsNotNull() {
			addCriterion("is_allow_new_folder is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderEqualTo(Byte value) {
			addCriterion("is_allow_new_folder =", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderNotEqualTo(Byte value) {
			addCriterion("is_allow_new_folder <>", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderGreaterThan(Byte value) {
			addCriterion("is_allow_new_folder >", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_new_folder >=", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderLessThan(Byte value) {
			addCriterion("is_allow_new_folder <", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_new_folder <=", value, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderIn(List<Byte> values) {
			addCriterion("is_allow_new_folder in", values, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderNotIn(List<Byte> values) {
			addCriterion("is_allow_new_folder not in", values, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_new_folder between", value1, value2, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowNewFolderNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_new_folder not between", value1, value2, "isAllowNewFolder");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIsNull() {
			addCriterion("is_allow_read is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIsNotNull() {
			addCriterion("is_allow_read is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadEqualTo(Byte value) {
			addCriterion("is_allow_read =", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotEqualTo(Byte value) {
			addCriterion("is_allow_read <>", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadGreaterThan(Byte value) {
			addCriterion("is_allow_read >", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_read >=", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadLessThan(Byte value) {
			addCriterion("is_allow_read <", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_read <=", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIn(List<Byte> values) {
			addCriterion("is_allow_read in", values, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotIn(List<Byte> values) {
			addCriterion("is_allow_read not in", values, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_read between", value1, value2, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_read not between", value1, value2, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIsNull() {
			addCriterion("is_allow_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIsNotNull() {
			addCriterion("is_allow_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedEqualTo(Byte value) {
			addCriterion("is_allow_deleted =", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotEqualTo(Byte value) {
			addCriterion("is_allow_deleted <>", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedGreaterThan(Byte value) {
			addCriterion("is_allow_deleted >", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_deleted >=", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedLessThan(Byte value) {
			addCriterion("is_allow_deleted <", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_deleted <=", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIn(List<Byte> values) {
			addCriterion("is_allow_deleted in", values, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotIn(List<Byte> values) {
			addCriterion("is_allow_deleted not in", values, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_deleted between", value1, value2, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_deleted not between", value1, value2, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andOrdersIsNull() {
			addCriterion("orders is null");
			return (Criteria) this;
		}

		public Criteria andOrdersIsNotNull() {
			addCriterion("orders is not null");
			return (Criteria) this;
		}

		public Criteria andOrdersEqualTo(Long value) {
			addCriterion("orders =", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotEqualTo(Long value) {
			addCriterion("orders <>", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersGreaterThan(Long value) {
			addCriterion("orders >", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersGreaterThanOrEqualTo(Long value) {
			addCriterion("orders >=", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersLessThan(Long value) {
			addCriterion("orders <", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersLessThanOrEqualTo(Long value) {
			addCriterion("orders <=", value, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersIn(List<Long> values) {
			addCriterion("orders in", values, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotIn(List<Long> values) {
			addCriterion("orders not in", values, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersBetween(Long value1, Long value2) {
			addCriterion("orders between", value1, value2, "orders");
			return (Criteria) this;
		}

		public Criteria andOrdersNotBetween(Long value1, Long value2) {
			addCriterion("orders not between", value1, value2, "orders");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}