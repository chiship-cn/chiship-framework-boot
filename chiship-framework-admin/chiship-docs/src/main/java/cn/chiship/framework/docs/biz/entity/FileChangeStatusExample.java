package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class FileChangeStatusExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public FileChangeStatusExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdIsNull() {
			addCriterion("option_user_id is null");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdIsNotNull() {
			addCriterion("option_user_id is not null");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdEqualTo(String value) {
			addCriterion("option_user_id =", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdNotEqualTo(String value) {
			addCriterion("option_user_id <>", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdGreaterThan(String value) {
			addCriterion("option_user_id >", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("option_user_id >=", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdLessThan(String value) {
			addCriterion("option_user_id <", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdLessThanOrEqualTo(String value) {
			addCriterion("option_user_id <=", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdLike(String value) {
			addCriterion("option_user_id like", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdNotLike(String value) {
			addCriterion("option_user_id not like", value, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdIn(List<String> values) {
			addCriterion("option_user_id in", values, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdNotIn(List<String> values) {
			addCriterion("option_user_id not in", values, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdBetween(String value1, String value2) {
			addCriterion("option_user_id between", value1, value2, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserIdNotBetween(String value1, String value2) {
			addCriterion("option_user_id not between", value1, value2, "optionUserId");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameIsNull() {
			addCriterion("option_user_name is null");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameIsNotNull() {
			addCriterion("option_user_name is not null");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameEqualTo(String value) {
			addCriterion("option_user_name =", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameNotEqualTo(String value) {
			addCriterion("option_user_name <>", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameGreaterThan(String value) {
			addCriterion("option_user_name >", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("option_user_name >=", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameLessThan(String value) {
			addCriterion("option_user_name <", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameLessThanOrEqualTo(String value) {
			addCriterion("option_user_name <=", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameLike(String value) {
			addCriterion("option_user_name like", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameNotLike(String value) {
			addCriterion("option_user_name not like", value, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameIn(List<String> values) {
			addCriterion("option_user_name in", values, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameNotIn(List<String> values) {
			addCriterion("option_user_name not in", values, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameBetween(String value1, String value2) {
			addCriterion("option_user_name between", value1, value2, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionUserNameNotBetween(String value1, String value2) {
			addCriterion("option_user_name not between", value1, value2, "optionUserName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameIsNull() {
			addCriterion("option_real_name is null");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameIsNotNull() {
			addCriterion("option_real_name is not null");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameEqualTo(String value) {
			addCriterion("option_real_name =", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameNotEqualTo(String value) {
			addCriterion("option_real_name <>", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameGreaterThan(String value) {
			addCriterion("option_real_name >", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("option_real_name >=", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameLessThan(String value) {
			addCriterion("option_real_name <", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameLessThanOrEqualTo(String value) {
			addCriterion("option_real_name <=", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameLike(String value) {
			addCriterion("option_real_name like", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameNotLike(String value) {
			addCriterion("option_real_name not like", value, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameIn(List<String> values) {
			addCriterion("option_real_name in", values, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameNotIn(List<String> values) {
			addCriterion("option_real_name not in", values, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameBetween(String value1, String value2) {
			addCriterion("option_real_name between", value1, value2, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andOptionRealNameNotBetween(String value1, String value2) {
			addCriterion("option_real_name not between", value1, value2, "optionRealName");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIsNull() {
			addCriterion("resource_type is null");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIsNotNull() {
			addCriterion("resource_type is not null");
			return (Criteria) this;
		}

		public Criteria andResourceTypeEqualTo(Byte value) {
			addCriterion("resource_type =", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotEqualTo(Byte value) {
			addCriterion("resource_type <>", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeGreaterThan(Byte value) {
			addCriterion("resource_type >", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("resource_type >=", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeLessThan(Byte value) {
			addCriterion("resource_type <", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeLessThanOrEqualTo(Byte value) {
			addCriterion("resource_type <=", value, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeIn(List<Byte> values) {
			addCriterion("resource_type in", values, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotIn(List<Byte> values) {
			addCriterion("resource_type not in", values, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeBetween(Byte value1, Byte value2) {
			addCriterion("resource_type between", value1, value2, "resourceType");
			return (Criteria) this;
		}

		public Criteria andResourceTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("resource_type not between", value1, value2, "resourceType");
			return (Criteria) this;
		}

		public Criteria andOriginalIdIsNull() {
			addCriterion("original_id is null");
			return (Criteria) this;
		}

		public Criteria andOriginalIdIsNotNull() {
			addCriterion("original_id is not null");
			return (Criteria) this;
		}

		public Criteria andOriginalIdEqualTo(String value) {
			addCriterion("original_id =", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdNotEqualTo(String value) {
			addCriterion("original_id <>", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdGreaterThan(String value) {
			addCriterion("original_id >", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdGreaterThanOrEqualTo(String value) {
			addCriterion("original_id >=", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdLessThan(String value) {
			addCriterion("original_id <", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdLessThanOrEqualTo(String value) {
			addCriterion("original_id <=", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdLike(String value) {
			addCriterion("original_id like", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdNotLike(String value) {
			addCriterion("original_id not like", value, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdIn(List<String> values) {
			addCriterion("original_id in", values, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdNotIn(List<String> values) {
			addCriterion("original_id not in", values, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdBetween(String value1, String value2) {
			addCriterion("original_id between", value1, value2, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalIdNotBetween(String value1, String value2) {
			addCriterion("original_id not between", value1, value2, "originalId");
			return (Criteria) this;
		}

		public Criteria andOriginalNameIsNull() {
			addCriterion("original_name is null");
			return (Criteria) this;
		}

		public Criteria andOriginalNameIsNotNull() {
			addCriterion("original_name is not null");
			return (Criteria) this;
		}

		public Criteria andOriginalNameEqualTo(String value) {
			addCriterion("original_name =", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameNotEqualTo(String value) {
			addCriterion("original_name <>", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameGreaterThan(String value) {
			addCriterion("original_name >", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameGreaterThanOrEqualTo(String value) {
			addCriterion("original_name >=", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameLessThan(String value) {
			addCriterion("original_name <", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameLessThanOrEqualTo(String value) {
			addCriterion("original_name <=", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameLike(String value) {
			addCriterion("original_name like", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameNotLike(String value) {
			addCriterion("original_name not like", value, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameIn(List<String> values) {
			addCriterion("original_name in", values, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameNotIn(List<String> values) {
			addCriterion("original_name not in", values, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameBetween(String value1, String value2) {
			addCriterion("original_name between", value1, value2, "originalName");
			return (Criteria) this;
		}

		public Criteria andOriginalNameNotBetween(String value1, String value2) {
			addCriterion("original_name not between", value1, value2, "originalName");
			return (Criteria) this;
		}

		public Criteria andTargetIdIsNull() {
			addCriterion("target_id is null");
			return (Criteria) this;
		}

		public Criteria andTargetIdIsNotNull() {
			addCriterion("target_id is not null");
			return (Criteria) this;
		}

		public Criteria andTargetIdEqualTo(String value) {
			addCriterion("target_id =", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdNotEqualTo(String value) {
			addCriterion("target_id <>", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdGreaterThan(String value) {
			addCriterion("target_id >", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdGreaterThanOrEqualTo(String value) {
			addCriterion("target_id >=", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdLessThan(String value) {
			addCriterion("target_id <", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdLessThanOrEqualTo(String value) {
			addCriterion("target_id <=", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdLike(String value) {
			addCriterion("target_id like", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdNotLike(String value) {
			addCriterion("target_id not like", value, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdIn(List<String> values) {
			addCriterion("target_id in", values, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdNotIn(List<String> values) {
			addCriterion("target_id not in", values, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdBetween(String value1, String value2) {
			addCriterion("target_id between", value1, value2, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetIdNotBetween(String value1, String value2) {
			addCriterion("target_id not between", value1, value2, "targetId");
			return (Criteria) this;
		}

		public Criteria andTargetNameIsNull() {
			addCriterion("target_name is null");
			return (Criteria) this;
		}

		public Criteria andTargetNameIsNotNull() {
			addCriterion("target_name is not null");
			return (Criteria) this;
		}

		public Criteria andTargetNameEqualTo(String value) {
			addCriterion("target_name =", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameNotEqualTo(String value) {
			addCriterion("target_name <>", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameGreaterThan(String value) {
			addCriterion("target_name >", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameGreaterThanOrEqualTo(String value) {
			addCriterion("target_name >=", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameLessThan(String value) {
			addCriterion("target_name <", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameLessThanOrEqualTo(String value) {
			addCriterion("target_name <=", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameLike(String value) {
			addCriterion("target_name like", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameNotLike(String value) {
			addCriterion("target_name not like", value, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameIn(List<String> values) {
			addCriterion("target_name in", values, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameNotIn(List<String> values) {
			addCriterion("target_name not in", values, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameBetween(String value1, String value2) {
			addCriterion("target_name between", value1, value2, "targetName");
			return (Criteria) this;
		}

		public Criteria andTargetNameNotBetween(String value1, String value2) {
			addCriterion("target_name not between", value1, value2, "targetName");
			return (Criteria) this;
		}

		public Criteria andChangeStatusIsNull() {
			addCriterion("change_status is null");
			return (Criteria) this;
		}

		public Criteria andChangeStatusIsNotNull() {
			addCriterion("change_status is not null");
			return (Criteria) this;
		}

		public Criteria andChangeStatusEqualTo(Byte value) {
			addCriterion("change_status =", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusNotEqualTo(Byte value) {
			addCriterion("change_status <>", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusGreaterThan(Byte value) {
			addCriterion("change_status >", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusGreaterThanOrEqualTo(Byte value) {
			addCriterion("change_status >=", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusLessThan(Byte value) {
			addCriterion("change_status <", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusLessThanOrEqualTo(Byte value) {
			addCriterion("change_status <=", value, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusIn(List<Byte> values) {
			addCriterion("change_status in", values, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusNotIn(List<Byte> values) {
			addCriterion("change_status not in", values, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusBetween(Byte value1, Byte value2) {
			addCriterion("change_status between", value1, value2, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeStatusNotBetween(Byte value1, Byte value2) {
			addCriterion("change_status not between", value1, value2, "changeStatus");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkIsNull() {
			addCriterion("change_remark is null");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkIsNotNull() {
			addCriterion("change_remark is not null");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkEqualTo(String value) {
			addCriterion("change_remark =", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkNotEqualTo(String value) {
			addCriterion("change_remark <>", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkGreaterThan(String value) {
			addCriterion("change_remark >", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("change_remark >=", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkLessThan(String value) {
			addCriterion("change_remark <", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkLessThanOrEqualTo(String value) {
			addCriterion("change_remark <=", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkLike(String value) {
			addCriterion("change_remark like", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkNotLike(String value) {
			addCriterion("change_remark not like", value, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkIn(List<String> values) {
			addCriterion("change_remark in", values, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkNotIn(List<String> values) {
			addCriterion("change_remark not in", values, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkBetween(String value1, String value2) {
			addCriterion("change_remark between", value1, value2, "changeRemark");
			return (Criteria) this;
		}

		public Criteria andChangeRemarkNotBetween(String value1, String value2) {
			addCriterion("change_remark not between", value1, value2, "changeRemark");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}