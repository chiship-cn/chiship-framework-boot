package cn.chiship.framework.docs.biz.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author lijian
 */
@ApiModel(value = "文件重命名表单")
public class FileResourcesRenameDto {

	@ApiModelProperty(value = "文件名称", required = true)
	@NotEmpty(message = "文件名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String originalFileName;

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

}
