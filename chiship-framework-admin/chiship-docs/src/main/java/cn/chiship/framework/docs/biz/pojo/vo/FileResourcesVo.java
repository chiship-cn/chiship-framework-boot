package cn.chiship.framework.docs.biz.pojo.vo;

/**
 * @author lijian 文件资源视图
 */
public class FileResourcesVo {

	/**
	 * 主键
	 */
	private String id;

	/**
	 * 新增时间
	 */
	private Long gmtCreated;

	/**
	 * 修改时间
	 */
	private Long gmtModified;

	/**
	 * 唯一标识
	 */
	private String uuid;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 是否是文件
	 */
	private Boolean isFile;

	/**
	 * 创建者
	 */
	private String realName;

	/**
	 * 文件大小
	 */
	private Long fileSize;

	/**
	 * 文件类型
	 */
	private Byte fileType;

	/**
	 * 文件扩展名
	 */
	private String fileExt;

	/**
	 * 所属目录
	 */
	private String catalogId;

	/**
	 * 存储位置
	 */
	private Byte storageLocation;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getFile() {
		return isFile;
	}

	public void setFile(Boolean file) {
		isFile = file;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public Byte getFileType() {
		return fileType;
	}

	public void setFileType(Byte fileType) {
		this.fileType = fileType;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public Byte getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(Byte storageLocation) {
		this.storageLocation = storageLocation;
	}

}
