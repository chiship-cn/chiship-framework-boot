package cn.chiship.framework.docs.biz.mapper;

import cn.chiship.framework.docs.biz.entity.FileCatalogConfigExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.docs.biz.entity.FileCatalogConfig;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @author LiJian
 */
public interface FileCatalogConfigMapper extends BaseMapper<FileCatalogConfig, FileCatalogConfigExample> {

	/**
	 * 根据ID集合修改文件夹所属
	 * @param catalogPid
	 * @param idList
	 * @return
	 */
	int updatePidByIdList(@Param("catalogPid") String catalogPid, @Param("list") List<String> idList);

}