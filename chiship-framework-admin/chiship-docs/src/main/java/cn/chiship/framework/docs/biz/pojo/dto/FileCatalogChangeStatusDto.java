package cn.chiship.framework.docs.biz.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author LiJian
 */
@ApiModel(value = "文件夹变化表单")
public class FileCatalogChangeStatusDto {

	@ApiModelProperty(value = "操作者主键", required = true)
	@NotEmpty(message = "操作者主键" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String optionUserId;

	@ApiModelProperty(value = "操作者用户名", required = true)
	@NotEmpty(message = "操作者用户名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String optionUserName;

	@ApiModelProperty(value = "操作者真实姓名", required = true)
	@NotEmpty(message = "操作者真实姓名" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String optionRealName;

	@ApiModelProperty(value = "原始文件ID", required = true)
	@NotEmpty(message = "原始文件ID" + BaseTipConstants.NOT_EMPTY)
	private String originalId;

	@ApiModelProperty(value = "原始文件名称", required = true)
	@NotEmpty(message = "原始文件名称" + BaseTipConstants.NOT_EMPTY)
	private String originalName;

	@ApiModelProperty(value = "目标文件ID", required = true)
	@NotEmpty(message = "目标文件ID" + BaseTipConstants.NOT_EMPTY)
	private String targetId;

	@ApiModelProperty(value = "目标文件名称", required = true)
	@NotEmpty(message = "目标文件名称" + BaseTipConstants.NOT_EMPTY)
	private String targetName;

	public String getOptionUserId() {
		return optionUserId;
	}

	public void setOptionUserId(String optionUserId) {
		this.optionUserId = optionUserId;
	}

	public String getOptionUserName() {
		return optionUserName;
	}

	public void setOptionUserName(String optionUserName) {
		this.optionUserName = optionUserName;
	}

	public String getOptionRealName() {
		return optionRealName;
	}

	public void setOptionRealName(String optionRealName) {
		this.optionRealName = optionRealName;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

}
