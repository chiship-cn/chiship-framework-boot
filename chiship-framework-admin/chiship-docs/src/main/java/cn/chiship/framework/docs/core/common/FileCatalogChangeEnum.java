package cn.chiship.framework.docs.core.common;

/**
 * 文件变化枚举
 *
 * @author lijian
 */
public enum FileCatalogChangeEnum {

	/**
	 * 新建文件夹
	 */
	NEW(0, "新建文件夹"),
	/**
	 * 文件夹重命名
	 */
	RENAME(1, "文件夹重命名"),
	/**
	 * 移入回收站
	 */
	MOVE_RECYCLE(2, "移入回收站"),
	/**
	 * 删除文件夹
	 */
	REMOVE_RECYCLE(3, "删除文件夹"),
	/**
	 * 改变文件删除状态
	 */
	CHANGE_IS_ALLOW_DELETE(4, "改变文件删除状态"),;

	private int code;

	private String message;

	FileCatalogChangeEnum(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public int getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}

}
