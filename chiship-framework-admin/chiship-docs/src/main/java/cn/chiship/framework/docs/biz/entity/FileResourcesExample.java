package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author lijian
 */
public class FileResourcesExample implements Serializable {

	protected String orderByClause;

	protected boolean distinct;

	protected List<Criteria> oredCriteria;

	private static final long serialVersionUID = 1L;

	public FileResourcesExample() {
		oredCriteria = new ArrayList<Criteria>();
	}

	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}

	public String getOrderByClause() {
		return orderByClause;
	}

	public void setDistinct(boolean distinct) {
		this.distinct = distinct;
	}

	public boolean isDistinct() {
		return distinct;
	}

	public List<Criteria> getOredCriteria() {
		return oredCriteria;
	}

	public void or(Criteria criteria) {
		oredCriteria.add(criteria);
	}

	public Criteria or() {
		Criteria criteria = createCriteriaInternal();
		oredCriteria.add(criteria);
		return criteria;
	}

	public Criteria createCriteria() {
		Criteria criteria = createCriteriaInternal();
		if (oredCriteria.size() == 0) {
			oredCriteria.add(criteria);
		}
		return criteria;
	}

	protected Criteria createCriteriaInternal() {
		Criteria criteria = new Criteria();
		return criteria;
	}

	public void clear() {
		oredCriteria.clear();
		orderByClause = null;
		distinct = false;
	}

	protected abstract static class GeneratedCriteria implements Serializable {

		protected List<Criterion> criteria;

		protected GeneratedCriteria() {
			super();
			criteria = new ArrayList<Criterion>();
		}

		public boolean isValid() {
			return criteria.size() > 0;
		}

		public List<Criterion> getAllCriteria() {
			return criteria;
		}

		public List<Criterion> getCriteria() {
			return criteria;
		}

		protected void addCriterion(String condition) {
			if (condition == null) {
				throw new RuntimeException("Value for condition cannot be null");
			}
			criteria.add(new Criterion(condition));
		}

		protected void addCriterion(String condition, Object value, String property) {
			if (value == null) {
				throw new RuntimeException("Value for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value));
		}

		protected void addCriterion(String condition, Object value1, Object value2, String property) {
			if (value1 == null || value2 == null) {
				throw new RuntimeException("Between values for " + property + " cannot be null");
			}
			criteria.add(new Criterion(condition, value1, value2));
		}

		public Criteria andIdIsNull() {
			addCriterion("id is null");
			return (Criteria) this;
		}

		public Criteria andIdIsNotNull() {
			addCriterion("id is not null");
			return (Criteria) this;
		}

		public Criteria andIdEqualTo(String value) {
			addCriterion("id =", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotEqualTo(String value) {
			addCriterion("id <>", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThan(String value) {
			addCriterion("id >", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdGreaterThanOrEqualTo(String value) {
			addCriterion("id >=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThan(String value) {
			addCriterion("id <", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLessThanOrEqualTo(String value) {
			addCriterion("id <=", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdLike(String value) {
			addCriterion("id like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotLike(String value) {
			addCriterion("id not like", value, "id");
			return (Criteria) this;
		}

		public Criteria andIdIn(List<String> values) {
			addCriterion("id in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotIn(List<String> values) {
			addCriterion("id not in", values, "id");
			return (Criteria) this;
		}

		public Criteria andIdBetween(String value1, String value2) {
			addCriterion("id between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andIdNotBetween(String value1, String value2) {
			addCriterion("id not between", value1, value2, "id");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNull() {
			addCriterion("gmt_created is null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIsNotNull() {
			addCriterion("gmt_created is not null");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedEqualTo(Long value) {
			addCriterion("gmt_created =", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotEqualTo(Long value) {
			addCriterion("gmt_created <>", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThan(Long value) {
			addCriterion("gmt_created >", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_created >=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThan(Long value) {
			addCriterion("gmt_created <", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_created <=", value, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedIn(List<Long> values) {
			addCriterion("gmt_created in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotIn(List<Long> values) {
			addCriterion("gmt_created not in", values, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedBetween(Long value1, Long value2) {
			addCriterion("gmt_created between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtCreatedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_created not between", value1, value2, "gmtCreated");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNull() {
			addCriterion("gmt_modified is null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIsNotNull() {
			addCriterion("gmt_modified is not null");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedEqualTo(Long value) {
			addCriterion("gmt_modified =", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotEqualTo(Long value) {
			addCriterion("gmt_modified <>", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThan(Long value) {
			addCriterion("gmt_modified >", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedGreaterThanOrEqualTo(Long value) {
			addCriterion("gmt_modified >=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThan(Long value) {
			addCriterion("gmt_modified <", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedLessThanOrEqualTo(Long value) {
			addCriterion("gmt_modified <=", value, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedIn(List<Long> values) {
			addCriterion("gmt_modified in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotIn(List<Long> values) {
			addCriterion("gmt_modified not in", values, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedBetween(Long value1, Long value2) {
			addCriterion("gmt_modified between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andGmtModifiedNotBetween(Long value1, Long value2) {
			addCriterion("gmt_modified not between", value1, value2, "gmtModified");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNull() {
			addCriterion("is_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIsNotNull() {
			addCriterion("is_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsDeletedEqualTo(Byte value) {
			addCriterion("is_deleted =", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotEqualTo(Byte value) {
			addCriterion("is_deleted <>", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThan(Byte value) {
			addCriterion("is_deleted >", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_deleted >=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThan(Byte value) {
			addCriterion("is_deleted <", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_deleted <=", value, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedIn(List<Byte> values) {
			addCriterion("is_deleted in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotIn(List<Byte> values) {
			addCriterion("is_deleted not in", values, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andIsDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_deleted not between", value1, value2, "isDeleted");
			return (Criteria) this;
		}

		public Criteria andFileUuidIsNull() {
			addCriterion("file_uuid is null");
			return (Criteria) this;
		}

		public Criteria andFileUuidIsNotNull() {
			addCriterion("file_uuid is not null");
			return (Criteria) this;
		}

		public Criteria andFileUuidEqualTo(String value) {
			addCriterion("file_uuid =", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidNotEqualTo(String value) {
			addCriterion("file_uuid <>", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidGreaterThan(String value) {
			addCriterion("file_uuid >", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidGreaterThanOrEqualTo(String value) {
			addCriterion("file_uuid >=", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidLessThan(String value) {
			addCriterion("file_uuid <", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidLessThanOrEqualTo(String value) {
			addCriterion("file_uuid <=", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidLike(String value) {
			addCriterion("file_uuid like", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidNotLike(String value) {
			addCriterion("file_uuid not like", value, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidIn(List<String> values) {
			addCriterion("file_uuid in", values, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidNotIn(List<String> values) {
			addCriterion("file_uuid not in", values, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidBetween(String value1, String value2) {
			addCriterion("file_uuid between", value1, value2, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFileUuidNotBetween(String value1, String value2) {
			addCriterion("file_uuid not between", value1, value2, "fileUuid");
			return (Criteria) this;
		}

		public Criteria andFilePidIsNull() {
			addCriterion("file_pid is null");
			return (Criteria) this;
		}

		public Criteria andFilePidIsNotNull() {
			addCriterion("file_pid is not null");
			return (Criteria) this;
		}

		public Criteria andFilePidEqualTo(String value) {
			addCriterion("file_pid =", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidNotEqualTo(String value) {
			addCriterion("file_pid <>", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidGreaterThan(String value) {
			addCriterion("file_pid >", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidGreaterThanOrEqualTo(String value) {
			addCriterion("file_pid >=", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidLessThan(String value) {
			addCriterion("file_pid <", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidLessThanOrEqualTo(String value) {
			addCriterion("file_pid <=", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidLike(String value) {
			addCriterion("file_pid like", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidNotLike(String value) {
			addCriterion("file_pid not like", value, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidIn(List<String> values) {
			addCriterion("file_pid in", values, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidNotIn(List<String> values) {
			addCriterion("file_pid not in", values, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidBetween(String value1, String value2) {
			addCriterion("file_pid between", value1, value2, "filePid");
			return (Criteria) this;
		}

		public Criteria andFilePidNotBetween(String value1, String value2) {
			addCriterion("file_pid not between", value1, value2, "filePid");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNull() {
			addCriterion("user_id is null");
			return (Criteria) this;
		}

		public Criteria andUserIdIsNotNull() {
			addCriterion("user_id is not null");
			return (Criteria) this;
		}

		public Criteria andUserIdEqualTo(String value) {
			addCriterion("user_id =", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotEqualTo(String value) {
			addCriterion("user_id <>", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThan(String value) {
			addCriterion("user_id >", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdGreaterThanOrEqualTo(String value) {
			addCriterion("user_id >=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThan(String value) {
			addCriterion("user_id <", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLessThanOrEqualTo(String value) {
			addCriterion("user_id <=", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdLike(String value) {
			addCriterion("user_id like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotLike(String value) {
			addCriterion("user_id not like", value, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdIn(List<String> values) {
			addCriterion("user_id in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotIn(List<String> values) {
			addCriterion("user_id not in", values, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdBetween(String value1, String value2) {
			addCriterion("user_id between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserIdNotBetween(String value1, String value2) {
			addCriterion("user_id not between", value1, value2, "userId");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNull() {
			addCriterion("user_name is null");
			return (Criteria) this;
		}

		public Criteria andUserNameIsNotNull() {
			addCriterion("user_name is not null");
			return (Criteria) this;
		}

		public Criteria andUserNameEqualTo(String value) {
			addCriterion("user_name =", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotEqualTo(String value) {
			addCriterion("user_name <>", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThan(String value) {
			addCriterion("user_name >", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameGreaterThanOrEqualTo(String value) {
			addCriterion("user_name >=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThan(String value) {
			addCriterion("user_name <", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLessThanOrEqualTo(String value) {
			addCriterion("user_name <=", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameLike(String value) {
			addCriterion("user_name like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotLike(String value) {
			addCriterion("user_name not like", value, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameIn(List<String> values) {
			addCriterion("user_name in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotIn(List<String> values) {
			addCriterion("user_name not in", values, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameBetween(String value1, String value2) {
			addCriterion("user_name between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andUserNameNotBetween(String value1, String value2) {
			addCriterion("user_name not between", value1, value2, "userName");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNull() {
			addCriterion("is_built_in is null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIsNotNull() {
			addCriterion("is_built_in is not null");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInEqualTo(Byte value) {
			addCriterion("is_built_in =", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotEqualTo(Byte value) {
			addCriterion("is_built_in <>", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThan(Byte value) {
			addCriterion("is_built_in >", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_built_in >=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThan(Byte value) {
			addCriterion("is_built_in <", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInLessThanOrEqualTo(Byte value) {
			addCriterion("is_built_in <=", value, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInIn(List<Byte> values) {
			addCriterion("is_built_in in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotIn(List<Byte> values) {
			addCriterion("is_built_in not in", values, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsBuiltInNotBetween(Byte value1, Byte value2) {
			addCriterion("is_built_in not between", value1, value2, "isBuiltIn");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIsNull() {
			addCriterion("is_encryption is null");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIsNotNull() {
			addCriterion("is_encryption is not null");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionEqualTo(Byte value) {
			addCriterion("is_encryption =", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotEqualTo(Byte value) {
			addCriterion("is_encryption <>", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionGreaterThan(Byte value) {
			addCriterion("is_encryption >", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_encryption >=", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionLessThan(Byte value) {
			addCriterion("is_encryption <", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionLessThanOrEqualTo(Byte value) {
			addCriterion("is_encryption <=", value, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionIn(List<Byte> values) {
			addCriterion("is_encryption in", values, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotIn(List<Byte> values) {
			addCriterion("is_encryption not in", values, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionBetween(Byte value1, Byte value2) {
			addCriterion("is_encryption between", value1, value2, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andIsEncryptionNotBetween(Byte value1, Byte value2) {
			addCriterion("is_encryption not between", value1, value2, "isEncryption");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIsNull() {
			addCriterion("encryption_password is null");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIsNotNull() {
			addCriterion("encryption_password is not null");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordEqualTo(String value) {
			addCriterion("encryption_password =", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotEqualTo(String value) {
			addCriterion("encryption_password <>", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordGreaterThan(String value) {
			addCriterion("encryption_password >", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordGreaterThanOrEqualTo(String value) {
			addCriterion("encryption_password >=", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLessThan(String value) {
			addCriterion("encryption_password <", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLessThanOrEqualTo(String value) {
			addCriterion("encryption_password <=", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordLike(String value) {
			addCriterion("encryption_password like", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotLike(String value) {
			addCriterion("encryption_password not like", value, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordIn(List<String> values) {
			addCriterion("encryption_password in", values, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotIn(List<String> values) {
			addCriterion("encryption_password not in", values, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordBetween(String value1, String value2) {
			addCriterion("encryption_password between", value1, value2, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionPasswordNotBetween(String value1, String value2) {
			addCriterion("encryption_password not between", value1, value2, "encryptionPassword");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIsNull() {
			addCriterion("encryption_salt is null");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIsNotNull() {
			addCriterion("encryption_salt is not null");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltEqualTo(String value) {
			addCriterion("encryption_salt =", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotEqualTo(String value) {
			addCriterion("encryption_salt <>", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltGreaterThan(String value) {
			addCriterion("encryption_salt >", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltGreaterThanOrEqualTo(String value) {
			addCriterion("encryption_salt >=", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLessThan(String value) {
			addCriterion("encryption_salt <", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLessThanOrEqualTo(String value) {
			addCriterion("encryption_salt <=", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltLike(String value) {
			addCriterion("encryption_salt like", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotLike(String value) {
			addCriterion("encryption_salt not like", value, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltIn(List<String> values) {
			addCriterion("encryption_salt in", values, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotIn(List<String> values) {
			addCriterion("encryption_salt not in", values, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltBetween(String value1, String value2) {
			addCriterion("encryption_salt between", value1, value2, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andEncryptionSaltNotBetween(String value1, String value2) {
			addCriterion("encryption_salt not between", value1, value2, "encryptionSalt");
			return (Criteria) this;
		}

		public Criteria andFileTypeIsNull() {
			addCriterion("file_type is null");
			return (Criteria) this;
		}

		public Criteria andFileTypeIsNotNull() {
			addCriterion("file_type is not null");
			return (Criteria) this;
		}

		public Criteria andFileTypeEqualTo(Byte value) {
			addCriterion("file_type =", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeNotEqualTo(Byte value) {
			addCriterion("file_type <>", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeGreaterThan(Byte value) {
			addCriterion("file_type >", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeGreaterThanOrEqualTo(Byte value) {
			addCriterion("file_type >=", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeLessThan(Byte value) {
			addCriterion("file_type <", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeLessThanOrEqualTo(Byte value) {
			addCriterion("file_type <=", value, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeIn(List<Byte> values) {
			addCriterion("file_type in", values, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeNotIn(List<Byte> values) {
			addCriterion("file_type not in", values, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeBetween(Byte value1, Byte value2) {
			addCriterion("file_type between", value1, value2, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileTypeNotBetween(Byte value1, Byte value2) {
			addCriterion("file_type not between", value1, value2, "fileType");
			return (Criteria) this;
		}

		public Criteria andFileExtIsNull() {
			addCriterion("file_ext is null");
			return (Criteria) this;
		}

		public Criteria andFileExtIsNotNull() {
			addCriterion("file_ext is not null");
			return (Criteria) this;
		}

		public Criteria andFileExtEqualTo(String value) {
			addCriterion("file_ext =", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtNotEqualTo(String value) {
			addCriterion("file_ext <>", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtGreaterThan(String value) {
			addCriterion("file_ext >", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtGreaterThanOrEqualTo(String value) {
			addCriterion("file_ext >=", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtLessThan(String value) {
			addCriterion("file_ext <", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtLessThanOrEqualTo(String value) {
			addCriterion("file_ext <=", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtLike(String value) {
			addCriterion("file_ext like", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtNotLike(String value) {
			addCriterion("file_ext not like", value, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtIn(List<String> values) {
			addCriterion("file_ext in", values, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtNotIn(List<String> values) {
			addCriterion("file_ext not in", values, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtBetween(String value1, String value2) {
			addCriterion("file_ext between", value1, value2, "fileExt");
			return (Criteria) this;
		}

		public Criteria andFileExtNotBetween(String value1, String value2) {
			addCriterion("file_ext not between", value1, value2, "fileExt");
			return (Criteria) this;
		}

		public Criteria andCatalogIdIsNull() {
			addCriterion("catalog_id is null");
			return (Criteria) this;
		}

		public Criteria andCatalogIdIsNotNull() {
			addCriterion("catalog_id is not null");
			return (Criteria) this;
		}

		public Criteria andCatalogIdEqualTo(String value) {
			addCriterion("catalog_id =", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdNotEqualTo(String value) {
			addCriterion("catalog_id <>", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdGreaterThan(String value) {
			addCriterion("catalog_id >", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdGreaterThanOrEqualTo(String value) {
			addCriterion("catalog_id >=", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdLessThan(String value) {
			addCriterion("catalog_id <", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdLessThanOrEqualTo(String value) {
			addCriterion("catalog_id <=", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdLike(String value) {
			addCriterion("catalog_id like", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdNotLike(String value) {
			addCriterion("catalog_id not like", value, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdIn(List<String> values) {
			addCriterion("catalog_id in", values, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdNotIn(List<String> values) {
			addCriterion("catalog_id not in", values, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdBetween(String value1, String value2) {
			addCriterion("catalog_id between", value1, value2, "catalogId");
			return (Criteria) this;
		}

		public Criteria andCatalogIdNotBetween(String value1, String value2) {
			addCriterion("catalog_id not between", value1, value2, "catalogId");
			return (Criteria) this;
		}

		public Criteria andFileNameIsNull() {
			addCriterion("file_name is null");
			return (Criteria) this;
		}

		public Criteria andFileNameIsNotNull() {
			addCriterion("file_name is not null");
			return (Criteria) this;
		}

		public Criteria andFileNameEqualTo(String value) {
			addCriterion("file_name =", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameNotEqualTo(String value) {
			addCriterion("file_name <>", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameGreaterThan(String value) {
			addCriterion("file_name >", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameGreaterThanOrEqualTo(String value) {
			addCriterion("file_name >=", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameLessThan(String value) {
			addCriterion("file_name <", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameLessThanOrEqualTo(String value) {
			addCriterion("file_name <=", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameLike(String value) {
			addCriterion("file_name like", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameNotLike(String value) {
			addCriterion("file_name not like", value, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameIn(List<String> values) {
			addCriterion("file_name in", values, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameNotIn(List<String> values) {
			addCriterion("file_name not in", values, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameBetween(String value1, String value2) {
			addCriterion("file_name between", value1, value2, "fileName");
			return (Criteria) this;
		}

		public Criteria andFileNameNotBetween(String value1, String value2) {
			addCriterion("file_name not between", value1, value2, "fileName");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIsNull() {
			addCriterion("is_allow_read is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIsNotNull() {
			addCriterion("is_allow_read is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadEqualTo(Byte value) {
			addCriterion("is_allow_read =", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotEqualTo(Byte value) {
			addCriterion("is_allow_read <>", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadGreaterThan(Byte value) {
			addCriterion("is_allow_read >", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_read >=", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadLessThan(Byte value) {
			addCriterion("is_allow_read <", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_read <=", value, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadIn(List<Byte> values) {
			addCriterion("is_allow_read in", values, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotIn(List<Byte> values) {
			addCriterion("is_allow_read not in", values, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_read between", value1, value2, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowReadNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_read not between", value1, value2, "isAllowRead");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIsNull() {
			addCriterion("is_allow_deleted is null");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIsNotNull() {
			addCriterion("is_allow_deleted is not null");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedEqualTo(Byte value) {
			addCriterion("is_allow_deleted =", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotEqualTo(Byte value) {
			addCriterion("is_allow_deleted <>", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedGreaterThan(Byte value) {
			addCriterion("is_allow_deleted >", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_allow_deleted >=", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedLessThan(Byte value) {
			addCriterion("is_allow_deleted <", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedLessThanOrEqualTo(Byte value) {
			addCriterion("is_allow_deleted <=", value, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedIn(List<Byte> values) {
			addCriterion("is_allow_deleted in", values, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotIn(List<Byte> values) {
			addCriterion("is_allow_deleted not in", values, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_deleted between", value1, value2, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andIsAllowDeletedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_allow_deleted not between", value1, value2, "isAllowDeleted");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNull() {
			addCriterion("real_name is null");
			return (Criteria) this;
		}

		public Criteria andRealNameIsNotNull() {
			addCriterion("real_name is not null");
			return (Criteria) this;
		}

		public Criteria andRealNameEqualTo(String value) {
			addCriterion("real_name =", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotEqualTo(String value) {
			addCriterion("real_name <>", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThan(String value) {
			addCriterion("real_name >", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameGreaterThanOrEqualTo(String value) {
			addCriterion("real_name >=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThan(String value) {
			addCriterion("real_name <", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLessThanOrEqualTo(String value) {
			addCriterion("real_name <=", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameLike(String value) {
			addCriterion("real_name like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotLike(String value) {
			addCriterion("real_name not like", value, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameIn(List<String> values) {
			addCriterion("real_name in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotIn(List<String> values) {
			addCriterion("real_name not in", values, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameBetween(String value1, String value2) {
			addCriterion("real_name between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andRealNameNotBetween(String value1, String value2) {
			addCriterion("real_name not between", value1, value2, "realName");
			return (Criteria) this;
		}

		public Criteria andIsCollectedIsNull() {
			addCriterion("is_collected is null");
			return (Criteria) this;
		}

		public Criteria andIsCollectedIsNotNull() {
			addCriterion("is_collected is not null");
			return (Criteria) this;
		}

		public Criteria andIsCollectedEqualTo(Byte value) {
			addCriterion("is_collected =", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedNotEqualTo(Byte value) {
			addCriterion("is_collected <>", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedGreaterThan(Byte value) {
			addCriterion("is_collected >", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_collected >=", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedLessThan(Byte value) {
			addCriterion("is_collected <", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedLessThanOrEqualTo(Byte value) {
			addCriterion("is_collected <=", value, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedIn(List<Byte> values) {
			addCriterion("is_collected in", values, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedNotIn(List<Byte> values) {
			addCriterion("is_collected not in", values, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedBetween(Byte value1, Byte value2) {
			addCriterion("is_collected between", value1, value2, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsCollectedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_collected not between", value1, value2, "isCollected");
			return (Criteria) this;
		}

		public Criteria andIsSharedIsNull() {
			addCriterion("is_shared is null");
			return (Criteria) this;
		}

		public Criteria andIsSharedIsNotNull() {
			addCriterion("is_shared is not null");
			return (Criteria) this;
		}

		public Criteria andIsSharedEqualTo(Byte value) {
			addCriterion("is_shared =", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedNotEqualTo(Byte value) {
			addCriterion("is_shared <>", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedGreaterThan(Byte value) {
			addCriterion("is_shared >", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedGreaterThanOrEqualTo(Byte value) {
			addCriterion("is_shared >=", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedLessThan(Byte value) {
			addCriterion("is_shared <", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedLessThanOrEqualTo(Byte value) {
			addCriterion("is_shared <=", value, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedIn(List<Byte> values) {
			addCriterion("is_shared in", values, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedNotIn(List<Byte> values) {
			addCriterion("is_shared not in", values, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedBetween(Byte value1, Byte value2) {
			addCriterion("is_shared between", value1, value2, "isShared");
			return (Criteria) this;
		}

		public Criteria andIsSharedNotBetween(Byte value1, Byte value2) {
			addCriterion("is_shared not between", value1, value2, "isShared");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameIsNull() {
			addCriterion("original_file_name is null");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameIsNotNull() {
			addCriterion("original_file_name is not null");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameEqualTo(String value) {
			addCriterion("original_file_name =", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameNotEqualTo(String value) {
			addCriterion("original_file_name <>", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameGreaterThan(String value) {
			addCriterion("original_file_name >", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameGreaterThanOrEqualTo(String value) {
			addCriterion("original_file_name >=", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameLessThan(String value) {
			addCriterion("original_file_name <", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameLessThanOrEqualTo(String value) {
			addCriterion("original_file_name <=", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameLike(String value) {
			addCriterion("original_file_name like", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameNotLike(String value) {
			addCriterion("original_file_name not like", value, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameIn(List<String> values) {
			addCriterion("original_file_name in", values, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameNotIn(List<String> values) {
			addCriterion("original_file_name not in", values, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameBetween(String value1, String value2) {
			addCriterion("original_file_name between", value1, value2, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andOriginalFileNameNotBetween(String value1, String value2) {
			addCriterion("original_file_name not between", value1, value2, "originalFileName");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceIsNull() {
			addCriterion("file_come_source is null");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceIsNotNull() {
			addCriterion("file_come_source is not null");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceEqualTo(String value) {
			addCriterion("file_come_source =", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceNotEqualTo(String value) {
			addCriterion("file_come_source <>", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceGreaterThan(String value) {
			addCriterion("file_come_source >", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceGreaterThanOrEqualTo(String value) {
			addCriterion("file_come_source >=", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceLessThan(String value) {
			addCriterion("file_come_source <", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceLessThanOrEqualTo(String value) {
			addCriterion("file_come_source <=", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceLike(String value) {
			addCriterion("file_come_source like", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceNotLike(String value) {
			addCriterion("file_come_source not like", value, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceIn(List<String> values) {
			addCriterion("file_come_source in", values, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceNotIn(List<String> values) {
			addCriterion("file_come_source not in", values, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceBetween(String value1, String value2) {
			addCriterion("file_come_source between", value1, value2, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileComeSourceNotBetween(String value1, String value2) {
			addCriterion("file_come_source not between", value1, value2, "fileComeSource");
			return (Criteria) this;
		}

		public Criteria andFileStatusIsNull() {
			addCriterion("file_status is null");
			return (Criteria) this;
		}

		public Criteria andFileStatusIsNotNull() {
			addCriterion("file_status is not null");
			return (Criteria) this;
		}

		public Criteria andFileStatusEqualTo(String value) {
			addCriterion("file_status =", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusNotEqualTo(String value) {
			addCriterion("file_status <>", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusGreaterThan(String value) {
			addCriterion("file_status >", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusGreaterThanOrEqualTo(String value) {
			addCriterion("file_status >=", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusLessThan(String value) {
			addCriterion("file_status <", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusLessThanOrEqualTo(String value) {
			addCriterion("file_status <=", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusLike(String value) {
			addCriterion("file_status like", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusNotLike(String value) {
			addCriterion("file_status not like", value, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusIn(List<String> values) {
			addCriterion("file_status in", values, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusNotIn(List<String> values) {
			addCriterion("file_status not in", values, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusBetween(String value1, String value2) {
			addCriterion("file_status between", value1, value2, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileStatusNotBetween(String value1, String value2) {
			addCriterion("file_status not between", value1, value2, "fileStatus");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeIsNull() {
			addCriterion("file_content_type is null");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeIsNotNull() {
			addCriterion("file_content_type is not null");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeEqualTo(String value) {
			addCriterion("file_content_type =", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeNotEqualTo(String value) {
			addCriterion("file_content_type <>", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeGreaterThan(String value) {
			addCriterion("file_content_type >", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeGreaterThanOrEqualTo(String value) {
			addCriterion("file_content_type >=", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeLessThan(String value) {
			addCriterion("file_content_type <", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeLessThanOrEqualTo(String value) {
			addCriterion("file_content_type <=", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeLike(String value) {
			addCriterion("file_content_type like", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeNotLike(String value) {
			addCriterion("file_content_type not like", value, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeIn(List<String> values) {
			addCriterion("file_content_type in", values, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeNotIn(List<String> values) {
			addCriterion("file_content_type not in", values, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeBetween(String value1, String value2) {
			addCriterion("file_content_type between", value1, value2, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileContentTypeNotBetween(String value1, String value2) {
			addCriterion("file_content_type not between", value1, value2, "fileContentType");
			return (Criteria) this;
		}

		public Criteria andFileInfoIsNull() {
			addCriterion("file_info is null");
			return (Criteria) this;
		}

		public Criteria andFileInfoIsNotNull() {
			addCriterion("file_info is not null");
			return (Criteria) this;
		}

		public Criteria andFileInfoEqualTo(String value) {
			addCriterion("file_info =", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoNotEqualTo(String value) {
			addCriterion("file_info <>", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoGreaterThan(String value) {
			addCriterion("file_info >", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoGreaterThanOrEqualTo(String value) {
			addCriterion("file_info >=", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoLessThan(String value) {
			addCriterion("file_info <", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoLessThanOrEqualTo(String value) {
			addCriterion("file_info <=", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoLike(String value) {
			addCriterion("file_info like", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoNotLike(String value) {
			addCriterion("file_info not like", value, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoIn(List<String> values) {
			addCriterion("file_info in", values, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoNotIn(List<String> values) {
			addCriterion("file_info not in", values, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoBetween(String value1, String value2) {
			addCriterion("file_info between", value1, value2, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileInfoNotBetween(String value1, String value2) {
			addCriterion("file_info not between", value1, value2, "fileInfo");
			return (Criteria) this;
		}

		public Criteria andFileSizeIsNull() {
			addCriterion("file_size is null");
			return (Criteria) this;
		}

		public Criteria andFileSizeIsNotNull() {
			addCriterion("file_size is not null");
			return (Criteria) this;
		}

		public Criteria andFileSizeEqualTo(Long value) {
			addCriterion("file_size =", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeNotEqualTo(Long value) {
			addCriterion("file_size <>", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeGreaterThan(Long value) {
			addCriterion("file_size >", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeGreaterThanOrEqualTo(Long value) {
			addCriterion("file_size >=", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeLessThan(Long value) {
			addCriterion("file_size <", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeLessThanOrEqualTo(Long value) {
			addCriterion("file_size <=", value, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeIn(List<Long> values) {
			addCriterion("file_size in", values, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeNotIn(List<Long> values) {
			addCriterion("file_size not in", values, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeBetween(Long value1, Long value2) {
			addCriterion("file_size between", value1, value2, "fileSize");
			return (Criteria) this;
		}

		public Criteria andFileSizeNotBetween(Long value1, Long value2) {
			addCriterion("file_size not between", value1, value2, "fileSize");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdIsNull() {
			addCriterion("original_resources_id is null");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdIsNotNull() {
			addCriterion("original_resources_id is not null");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdEqualTo(String value) {
			addCriterion("original_resources_id =", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdNotEqualTo(String value) {
			addCriterion("original_resources_id <>", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdGreaterThan(String value) {
			addCriterion("original_resources_id >", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdGreaterThanOrEqualTo(String value) {
			addCriterion("original_resources_id >=", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdLessThan(String value) {
			addCriterion("original_resources_id <", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdLessThanOrEqualTo(String value) {
			addCriterion("original_resources_id <=", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdLike(String value) {
			addCriterion("original_resources_id like", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdNotLike(String value) {
			addCriterion("original_resources_id not like", value, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdIn(List<String> values) {
			addCriterion("original_resources_id in", values, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdNotIn(List<String> values) {
			addCriterion("original_resources_id not in", values, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdBetween(String value1, String value2) {
			addCriterion("original_resources_id between", value1, value2, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andOriginalResourcesIdNotBetween(String value1, String value2) {
			addCriterion("original_resources_id not between", value1, value2, "originalResourcesId");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNull() {
			addCriterion("remark is null");
			return (Criteria) this;
		}

		public Criteria andRemarkIsNotNull() {
			addCriterion("remark is not null");
			return (Criteria) this;
		}

		public Criteria andRemarkEqualTo(String value) {
			addCriterion("remark =", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotEqualTo(String value) {
			addCriterion("remark <>", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThan(String value) {
			addCriterion("remark >", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkGreaterThanOrEqualTo(String value) {
			addCriterion("remark >=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThan(String value) {
			addCriterion("remark <", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLessThanOrEqualTo(String value) {
			addCriterion("remark <=", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkLike(String value) {
			addCriterion("remark like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotLike(String value) {
			addCriterion("remark not like", value, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkIn(List<String> values) {
			addCriterion("remark in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotIn(List<String> values) {
			addCriterion("remark not in", values, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkBetween(String value1, String value2) {
			addCriterion("remark between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andRemarkNotBetween(String value1, String value2) {
			addCriterion("remark not between", value1, value2, "remark");
			return (Criteria) this;
		}

		public Criteria andStorageLocationIsNull() {
			addCriterion("storage_location is null");
			return (Criteria) this;
		}

		public Criteria andStorageLocationIsNotNull() {
			addCriterion("storage_location is not null");
			return (Criteria) this;
		}

		public Criteria andStorageLocationEqualTo(Byte value) {
			addCriterion("storage_location =", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationNotEqualTo(Byte value) {
			addCriterion("storage_location <>", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationGreaterThan(Byte value) {
			addCriterion("storage_location >", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationGreaterThanOrEqualTo(Byte value) {
			addCriterion("storage_location >=", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationLessThan(Byte value) {
			addCriterion("storage_location <", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationLessThanOrEqualTo(Byte value) {
			addCriterion("storage_location <=", value, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationIn(List<Byte> values) {
			addCriterion("storage_location in", values, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationNotIn(List<Byte> values) {
			addCriterion("storage_location not in", values, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationBetween(Byte value1, Byte value2) {
			addCriterion("storage_location between", value1, value2, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStorageLocationNotBetween(Byte value1, Byte value2) {
			addCriterion("storage_location not between", value1, value2, "storageLocation");
			return (Criteria) this;
		}

		public Criteria andStoragePathIsNull() {
			addCriterion("storage_path is null");
			return (Criteria) this;
		}

		public Criteria andStoragePathIsNotNull() {
			addCriterion("storage_path is not null");
			return (Criteria) this;
		}

		public Criteria andStoragePathEqualTo(String value) {
			addCriterion("storage_path =", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathNotEqualTo(String value) {
			addCriterion("storage_path <>", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathGreaterThan(String value) {
			addCriterion("storage_path >", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathGreaterThanOrEqualTo(String value) {
			addCriterion("storage_path >=", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathLessThan(String value) {
			addCriterion("storage_path <", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathLessThanOrEqualTo(String value) {
			addCriterion("storage_path <=", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathLike(String value) {
			addCriterion("storage_path like", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathNotLike(String value) {
			addCriterion("storage_path not like", value, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathIn(List<String> values) {
			addCriterion("storage_path in", values, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathNotIn(List<String> values) {
			addCriterion("storage_path not in", values, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathBetween(String value1, String value2) {
			addCriterion("storage_path between", value1, value2, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStoragePathNotBetween(String value1, String value2) {
			addCriterion("storage_path not between", value1, value2, "storagePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathIsNull() {
			addCriterion("storage_relative_path is null");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathIsNotNull() {
			addCriterion("storage_relative_path is not null");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathEqualTo(String value) {
			addCriterion("storage_relative_path =", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathNotEqualTo(String value) {
			addCriterion("storage_relative_path <>", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathGreaterThan(String value) {
			addCriterion("storage_relative_path >", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathGreaterThanOrEqualTo(String value) {
			addCriterion("storage_relative_path >=", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathLessThan(String value) {
			addCriterion("storage_relative_path <", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathLessThanOrEqualTo(String value) {
			addCriterion("storage_relative_path <=", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathLike(String value) {
			addCriterion("storage_relative_path like", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathNotLike(String value) {
			addCriterion("storage_relative_path not like", value, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathIn(List<String> values) {
			addCriterion("storage_relative_path in", values, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathNotIn(List<String> values) {
			addCriterion("storage_relative_path not in", values, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathBetween(String value1, String value2) {
			addCriterion("storage_relative_path between", value1, value2, "storageRelativePath");
			return (Criteria) this;
		}

		public Criteria andStorageRelativePathNotBetween(String value1, String value2) {
			addCriterion("storage_relative_path not between", value1, value2, "storageRelativePath");
			return (Criteria) this;
		}

	}

	public static class Criteria extends GeneratedCriteria implements Serializable {

		protected Criteria() {
			super();
		}

	}

	public static class Criterion implements Serializable {

		private String condition;

		private Object value;

		private Object secondValue;

		private boolean noValue;

		private boolean singleValue;

		private boolean betweenValue;

		private boolean listValue;

		private String typeHandler;

		public String getCondition() {
			return condition;
		}

		public Object getValue() {
			return value;
		}

		public Object getSecondValue() {
			return secondValue;
		}

		public boolean isNoValue() {
			return noValue;
		}

		public boolean isSingleValue() {
			return singleValue;
		}

		public boolean isBetweenValue() {
			return betweenValue;
		}

		public boolean isListValue() {
			return listValue;
		}

		public String getTypeHandler() {
			return typeHandler;
		}

		protected Criterion(String condition) {
			super();
			this.condition = condition;
			this.typeHandler = null;
			this.noValue = true;
		}

		protected Criterion(String condition, Object value, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.typeHandler = typeHandler;
			if (value instanceof List<?>) {
				this.listValue = true;
			}
			else {
				this.singleValue = true;
			}
		}

		protected Criterion(String condition, Object value) {
			this(condition, value, null);
		}

		protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
			super();
			this.condition = condition;
			this.value = value;
			this.secondValue = secondValue;
			this.typeHandler = typeHandler;
			this.betweenValue = true;
		}

		protected Criterion(String condition, Object value, Object secondValue) {
			this(condition, value, secondValue, null);
		}

	}

}