package cn.chiship.framework.docs.core.common;

import cn.chiship.framework.common.constants.CommonConstants;

/**
 * @author lijian
 */
public class DocsCommonConstants extends CommonConstants {

	/**
	 * 本地存储
	 */
	public static final byte STORAGE_LOCATION_LOCAL = 0;

	/**
	 * 阿里云存储
	 */
	public static final byte STORAGE_LOCATION_ALI_OSS = 1;

	/**
	 * 腾讯云存储
	 */
	public static final byte STORAGE_LOCATION_TENCENT_OSS = 2;

	/**
	 * 我的文档
	 */
	public static final String CATALOG_TYPE_USER = "user";

}
