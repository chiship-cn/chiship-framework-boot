package cn.chiship.framework.docs.biz.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author lijianf
 */
@ApiModel(value = "文档目录表单")
public class FileCatalogConfigDto {

	@ApiModelProperty(value = "目录名称", required = true)
	@NotEmpty(message = "目录名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String catalogName;

	@ApiModelProperty(value = "目录路径", required = true)
	@NotEmpty(message = "目录路径" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 20)
	private String catalogPath;

	@ApiModelProperty(value = "所属父级目录", required = true)
	@NotEmpty(message = "所属父级目录" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 36)
	private String catalogPid;

	@ApiModelProperty(value = "是否加密", required = true)
	@NotNull(message = "是否加密" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isEncryption;

	@ApiModelProperty(value = "密码")
	@Length(min = 8, max = 16)
	private String encryptionPassword;

	@ApiModelProperty(value = "是否允许新建文件", required = true)
	@NotNull(message = "是否允许新建文件" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isAllowNewFile;

	@ApiModelProperty(value = "是否允许新建文件夹", required = true)
	@NotNull(message = "是否允许新建文件夹" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isAllowNewFolder;

	@ApiModelProperty(value = "是否只读", required = true)
	@NotNull(message = "是否只读" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isAllowRead;

	@ApiModelProperty(value = "是否允许删除", required = true)
	@NotNull(message = "是否允许删除" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte isAllowDeleted;

	public String getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	public String getCatalogPath() {
		return catalogPath;
	}

	public void setCatalogPath(String catalogPath) {
		this.catalogPath = catalogPath;
	}

	public String getCatalogPid() {
		return catalogPid;
	}

	public void setCatalogPid(String catalogPid) {
		this.catalogPid = catalogPid;
	}

	public Byte getIsEncryption() {
		return isEncryption;
	}

	public void setIsEncryption(Byte isEncryption) {
		this.isEncryption = isEncryption;
	}

	public String getEncryptionPassword() {
		return encryptionPassword;
	}

	public void setEncryptionPassword(String encryptionPassword) {
		this.encryptionPassword = encryptionPassword;
	}

	public Byte getIsAllowNewFile() {
		return isAllowNewFile;
	}

	public void setIsAllowNewFile(Byte isAllowNewFile) {
		this.isAllowNewFile = isAllowNewFile;
	}

	public Byte getIsAllowNewFolder() {
		return isAllowNewFolder;
	}

	public void setIsAllowNewFolder(Byte isAllowNewFolder) {
		this.isAllowNewFolder = isAllowNewFolder;
	}

	public Byte getIsAllowRead() {
		return isAllowRead;
	}

	public void setIsAllowRead(Byte isAllowRead) {
		this.isAllowRead = isAllowRead;
	}

	public Byte getIsAllowDeleted() {
		return isAllowDeleted;
	}

	public void setIsAllowDeleted(Byte isAllowDeleted) {
		this.isAllowDeleted = isAllowDeleted;
	}

}
