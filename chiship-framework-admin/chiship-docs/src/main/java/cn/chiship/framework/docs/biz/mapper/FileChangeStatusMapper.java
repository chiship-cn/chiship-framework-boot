package cn.chiship.framework.docs.biz.mapper;

import cn.chiship.framework.docs.biz.entity.FileChangeStatusExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.docs.biz.entity.FileChangeStatus;

/**
 * @author LiJian
 */
public interface FileChangeStatusMapper extends BaseMapper<FileChangeStatus, FileChangeStatusExample> {

}