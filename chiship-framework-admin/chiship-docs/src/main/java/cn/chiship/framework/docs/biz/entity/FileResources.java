package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class FileResources implements Serializable {

	private String id;

	private Long gmtCreated;

	private Long gmtModified;

	private Byte isDeleted;

	private String fileUuid;

	private String filePid;

	private String userId;

	private String userName;

	private Byte isBuiltIn;

	private Byte isEncryption;

	private String encryptionPassword;

	private String encryptionSalt;

	private Byte fileType;

	private String fileExt;

	private String catalogId;

	private String fileName;

	private Byte isAllowRead;

	private Byte isAllowDeleted;

	private String realName;

	private Byte isCollected;

	private Byte isShared;

	private String originalFileName;

	private String fileComeSource;

	private String fileStatus;

	private String fileContentType;

	private String fileInfo;

	private Long fileSize;

	private String originalResourcesId;

	private String remark;

	private Byte storageLocation;

	private String storagePath;

	private String storageRelativePath;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getFileUuid() {
		return fileUuid;
	}

	public void setFileUuid(String fileUuid) {
		this.fileUuid = fileUuid;
	}

	public String getFilePid() {
		return filePid;
	}

	public void setFilePid(String filePid) {
		this.filePid = filePid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Byte getIsBuiltIn() {
		return isBuiltIn;
	}

	public void setIsBuiltIn(Byte isBuiltIn) {
		this.isBuiltIn = isBuiltIn;
	}

	public Byte getIsEncryption() {
		return isEncryption;
	}

	public void setIsEncryption(Byte isEncryption) {
		this.isEncryption = isEncryption;
	}

	public String getEncryptionPassword() {
		return encryptionPassword;
	}

	public void setEncryptionPassword(String encryptionPassword) {
		this.encryptionPassword = encryptionPassword;
	}

	public String getEncryptionSalt() {
		return encryptionSalt;
	}

	public void setEncryptionSalt(String encryptionSalt) {
		this.encryptionSalt = encryptionSalt;
	}

	public Byte getFileType() {
		return fileType;
	}

	public void setFileType(Byte fileType) {
		this.fileType = fileType;
	}

	public String getFileExt() {
		return fileExt;
	}

	public void setFileExt(String fileExt) {
		this.fileExt = fileExt;
	}

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Byte getIsAllowRead() {
		return isAllowRead;
	}

	public void setIsAllowRead(Byte isAllowRead) {
		this.isAllowRead = isAllowRead;
	}

	public Byte getIsAllowDeleted() {
		return isAllowDeleted;
	}

	public void setIsAllowDeleted(Byte isAllowDeleted) {
		this.isAllowDeleted = isAllowDeleted;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Byte getIsCollected() {
		return isCollected;
	}

	public void setIsCollected(Byte isCollected) {
		this.isCollected = isCollected;
	}

	public Byte getIsShared() {
		return isShared;
	}

	public void setIsShared(Byte isShared) {
		this.isShared = isShared;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getFileComeSource() {
		return fileComeSource;
	}

	public void setFileComeSource(String fileComeSource) {
		this.fileComeSource = fileComeSource;
	}

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileInfo() {
		return fileInfo;
	}

	public void setFileInfo(String fileInfo) {
		this.fileInfo = fileInfo;
	}

	public Long getFileSize() {
		return fileSize;
	}

	public void setFileSize(Long fileSize) {
		this.fileSize = fileSize;
	}

	public String getOriginalResourcesId() {
		return originalResourcesId;
	}

	public void setOriginalResourcesId(String originalResourcesId) {
		this.originalResourcesId = originalResourcesId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Byte getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(Byte storageLocation) {
		this.storageLocation = storageLocation;
	}

	public String getStoragePath() {
		return storagePath;
	}

	public void setStoragePath(String storagePath) {
		this.storagePath = storagePath;
	}

	public String getStorageRelativePath() {
		return storageRelativePath;
	}

	public void setStorageRelativePath(String storageRelativePath) {
		this.storageRelativePath = storageRelativePath;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", fileUuid=").append(fileUuid);
		sb.append(", filePid=").append(filePid);
		sb.append(", userId=").append(userId);
		sb.append(", userName=").append(userName);
		sb.append(", isBuiltIn=").append(isBuiltIn);
		sb.append(", isEncryption=").append(isEncryption);
		sb.append(", encryptionPassword=").append(encryptionPassword);
		sb.append(", encryptionSalt=").append(encryptionSalt);
		sb.append(", fileType=").append(fileType);
		sb.append(", fileExt=").append(fileExt);
		sb.append(", catalogId=").append(catalogId);
		sb.append(", fileName=").append(fileName);
		sb.append(", isAllowRead=").append(isAllowRead);
		sb.append(", isAllowDeleted=").append(isAllowDeleted);
		sb.append(", realName=").append(realName);
		sb.append(", isCollected=").append(isCollected);
		sb.append(", isShared=").append(isShared);
		sb.append(", originalFileName=").append(originalFileName);
		sb.append(", fileComeSource=").append(fileComeSource);
		sb.append(", fileStatus=").append(fileStatus);
		sb.append(", fileContentType=").append(fileContentType);
		sb.append(", fileInfo=").append(fileInfo);
		sb.append(", fileSize=").append(fileSize);
		sb.append(", originalResourcesId=").append(originalResourcesId);
		sb.append(", remark=").append(remark);
		sb.append(", storageLocation=").append(storageLocation);
		sb.append(", storagePath=").append(storagePath);
		sb.append(", storageRelativePath=").append(storageRelativePath);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		FileResources other = (FileResources) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getFileUuid() == null ? other.getFileUuid() == null
						: this.getFileUuid().equals(other.getFileUuid()))
				&& (this.getFilePid() == null ? other.getFilePid() == null
						: this.getFilePid().equals(other.getFilePid()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserName() == null ? other.getUserName() == null
						: this.getUserName().equals(other.getUserName()))
				&& (this.getIsBuiltIn() == null ? other.getIsBuiltIn() == null
						: this.getIsBuiltIn().equals(other.getIsBuiltIn()))
				&& (this.getIsEncryption() == null ? other.getIsEncryption() == null
						: this.getIsEncryption().equals(other.getIsEncryption()))
				&& (this.getEncryptionPassword() == null ? other.getEncryptionPassword() == null
						: this.getEncryptionPassword().equals(other.getEncryptionPassword()))
				&& (this.getEncryptionSalt() == null ? other.getEncryptionSalt() == null
						: this.getEncryptionSalt().equals(other.getEncryptionSalt()))
				&& (this.getFileType() == null ? other.getFileType() == null
						: this.getFileType().equals(other.getFileType()))
				&& (this.getFileExt() == null ? other.getFileExt() == null
						: this.getFileExt().equals(other.getFileExt()))
				&& (this.getCatalogId() == null ? other.getCatalogId() == null
						: this.getCatalogId().equals(other.getCatalogId()))
				&& (this.getFileName() == null ? other.getFileName() == null
						: this.getFileName().equals(other.getFileName()))
				&& (this.getIsAllowRead() == null ? other.getIsAllowRead() == null
						: this.getIsAllowRead().equals(other.getIsAllowRead()))
				&& (this.getIsAllowDeleted() == null ? other.getIsAllowDeleted() == null
						: this.getIsAllowDeleted().equals(other.getIsAllowDeleted()))
				&& (this.getRealName() == null ? other.getRealName() == null
						: this.getRealName().equals(other.getRealName()))
				&& (this.getIsCollected() == null ? other.getIsCollected() == null
						: this.getIsCollected().equals(other.getIsCollected()))
				&& (this.getIsShared() == null ? other.getIsShared() == null
						: this.getIsShared().equals(other.getIsShared()))
				&& (this.getOriginalFileName() == null ? other.getOriginalFileName() == null
						: this.getOriginalFileName().equals(other.getOriginalFileName()))
				&& (this.getFileComeSource() == null ? other.getFileComeSource() == null
						: this.getFileComeSource().equals(other.getFileComeSource()))
				&& (this.getFileStatus() == null ? other.getFileStatus() == null
						: this.getFileStatus().equals(other.getFileStatus()))
				&& (this.getFileContentType() == null ? other.getFileContentType() == null
						: this.getFileContentType().equals(other.getFileContentType()))
				&& (this.getFileInfo() == null ? other.getFileInfo() == null
						: this.getFileInfo().equals(other.getFileInfo()))
				&& (this.getFileSize() == null ? other.getFileSize() == null
						: this.getFileSize().equals(other.getFileSize()))
				&& (this.getOriginalResourcesId() == null ? other.getOriginalResourcesId() == null
						: this.getOriginalResourcesId().equals(other.getOriginalResourcesId()))
				&& (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
				&& (this.getStorageLocation() == null ? other.getStorageLocation() == null
						: this.getStorageLocation().equals(other.getStorageLocation()))
				&& (this.getStoragePath() == null ? other.getStoragePath() == null
						: this.getStoragePath().equals(other.getStoragePath()))
				&& (this.getStorageRelativePath() == null ? other.getStorageRelativePath() == null
						: this.getStorageRelativePath().equals(other.getStorageRelativePath()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getFileUuid() == null) ? 0 : getFileUuid().hashCode());
		result = prime * result + ((getFilePid() == null) ? 0 : getFilePid().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
		result = prime * result + ((getIsBuiltIn() == null) ? 0 : getIsBuiltIn().hashCode());
		result = prime * result + ((getIsEncryption() == null) ? 0 : getIsEncryption().hashCode());
		result = prime * result + ((getEncryptionPassword() == null) ? 0 : getEncryptionPassword().hashCode());
		result = prime * result + ((getEncryptionSalt() == null) ? 0 : getEncryptionSalt().hashCode());
		result = prime * result + ((getFileType() == null) ? 0 : getFileType().hashCode());
		result = prime * result + ((getFileExt() == null) ? 0 : getFileExt().hashCode());
		result = prime * result + ((getCatalogId() == null) ? 0 : getCatalogId().hashCode());
		result = prime * result + ((getFileName() == null) ? 0 : getFileName().hashCode());
		result = prime * result + ((getIsAllowRead() == null) ? 0 : getIsAllowRead().hashCode());
		result = prime * result + ((getIsAllowDeleted() == null) ? 0 : getIsAllowDeleted().hashCode());
		result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
		result = prime * result + ((getIsCollected() == null) ? 0 : getIsCollected().hashCode());
		result = prime * result + ((getIsShared() == null) ? 0 : getIsShared().hashCode());
		result = prime * result + ((getOriginalFileName() == null) ? 0 : getOriginalFileName().hashCode());
		result = prime * result + ((getFileComeSource() == null) ? 0 : getFileComeSource().hashCode());
		result = prime * result + ((getFileStatus() == null) ? 0 : getFileStatus().hashCode());
		result = prime * result + ((getFileContentType() == null) ? 0 : getFileContentType().hashCode());
		result = prime * result + ((getFileInfo() == null) ? 0 : getFileInfo().hashCode());
		result = prime * result + ((getFileSize() == null) ? 0 : getFileSize().hashCode());
		result = prime * result + ((getOriginalResourcesId() == null) ? 0 : getOriginalResourcesId().hashCode());
		result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
		result = prime * result + ((getStorageLocation() == null) ? 0 : getStorageLocation().hashCode());
		result = prime * result + ((getStoragePath() == null) ? 0 : getStoragePath().hashCode());
		result = prime * result + ((getStorageRelativePath() == null) ? 0 : getStorageRelativePath().hashCode());
		return result;
	}

}