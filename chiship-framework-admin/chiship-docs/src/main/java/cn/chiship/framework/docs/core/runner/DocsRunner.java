package cn.chiship.framework.docs.core.runner;

import cn.chiship.framework.docs.biz.service.DocsCacheService;
import cn.chiship.framework.docs.core.properties.FileUploadProperties;
import cn.chiship.sdk.cache.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 初始化数据
 *
 * @author lijian
 */
@Component
@Order(1)
public class DocsRunner implements CommandLineRunner {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocsRunner.class);

	@Autowired
	DocsCacheService cacheService;

	@Resource
	RedisService redisService;

	@Resource
	FileUploadProperties fileUploadProperties;

	@Override
	public void run(String... args) throws Exception {
		LOGGER.info("----------------DocsRunner---------------------");
		cacheService.cacheFileCatalog();
	}

}
