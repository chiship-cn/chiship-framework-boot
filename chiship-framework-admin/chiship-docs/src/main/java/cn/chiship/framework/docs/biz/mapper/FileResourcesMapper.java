package cn.chiship.framework.docs.biz.mapper;

import cn.chiship.framework.docs.biz.entity.FileResourcesExample;
import cn.chiship.sdk.framework.base.BaseMapper;
import cn.chiship.framework.docs.biz.entity.FileResources;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * @author lijian
 */
public interface FileResourcesMapper extends BaseMapper<FileResources, FileResourcesExample> {

	/**
	 * 根据ID集合修改文件夹所属
	 * @param catalogId
	 * @param idList
	 * @return
	 */
	int updatePidByIdList(@Param("catalogId") String catalogId, @Param("list") List<String> idList);

}