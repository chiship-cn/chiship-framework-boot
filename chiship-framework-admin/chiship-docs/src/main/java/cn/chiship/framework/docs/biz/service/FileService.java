package cn.chiship.framework.docs.biz.service;

import cn.chiship.framework.docs.biz.pojo.dto.ChunkMultipartFileParam;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;

import javax.servlet.http.HttpServletResponse;

/**
 * @author lijian
 */
public interface FileService {

	/**
	 * 处理POST请求（携带文件流）
	 * @param chunk 文件块
	 * @param response 响应
	 * @return 结果
	 */
	BaseResult chunkUploadPost(ChunkMultipartFileParam chunk, HttpServletResponse response);

	/**
	 * 处理GET请求（携带文件流）
	 * @param chunk 文件块
	 * @return
	 */
	BaseResult chunkUploadGet(ChunkMultipartFileParam chunk);

	/**
	 * 文件合并
	 * @param chunk
	 * @param cacheUserVO
	 * @return
	 */
	BaseResult mergeFile(ChunkMultipartFileParam chunk, CacheUserVO cacheUserVO);

}
