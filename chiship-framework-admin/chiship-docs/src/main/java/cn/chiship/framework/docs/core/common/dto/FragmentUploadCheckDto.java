package cn.chiship.framework.docs.core.common.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * 文件分片上传检测表单
 *
 * @author lijian
 */
@ApiModel(value = "文件分片上传检测表单")
public class FragmentUploadCheckDto {

	@ApiModelProperty(value = "文件夹ID", required = true)
	@NotEmpty(message = "文件夹ID" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 10, max = 32)
	private String catalogId;

	@ApiModelProperty(value = "文件MD5信息码", required = true)
	@NotEmpty(message = "文件MD5信息码" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 32, max = 32)
	private String fileMd5;

	@ApiModelProperty(value = "源文件名称", required = true)
	@NotEmpty(message = "源文件名称" + BaseTipConstants.NOT_EMPTY)
	@Length(min = 1, max = 100)
	private String originalFileName;

	public String getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}

	public String getFileMd5() {
		return fileMd5;
	}

	public void setFileMd5(String fileMd5) {
		this.fileMd5 = fileMd5;
	}

	public String getOriginalFileName() {
		return originalFileName;
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

}
