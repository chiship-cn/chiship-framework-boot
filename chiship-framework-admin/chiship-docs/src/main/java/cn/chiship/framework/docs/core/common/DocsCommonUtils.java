package cn.chiship.framework.docs.core.common;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author lijian
 */
public class DocsCommonUtils {

	private DocsCommonUtils() {
	}

	/**
	 * 获取文档分类
	 * @param fileExt
	 * @return
	 */
	public static Byte getFileTypeByExt(String fileExt) {
		fileExt = fileExt.toLowerCase();
		Map<String, Byte> exts = Maps.newHashMapWithExpectedSize(7);
		/**
		 * 图片
		 */
		exts.put("jpg", Byte.valueOf("1"));
		exts.put("png", Byte.valueOf("1"));
		exts.put("jpeg", Byte.valueOf("1"));
		/**
		 * 文档
		 */
		exts.put("doc", Byte.valueOf("2"));
		exts.put("docx", Byte.valueOf("2"));

		/**
		 * 表格
		 */
		exts.put("xls", Byte.valueOf("3"));
		exts.put("xlsx", Byte.valueOf("3"));

		/**
		 * 演示文稿
		 */
		exts.put("ppt", Byte.valueOf("4"));
		exts.put("pptx", Byte.valueOf("4"));

		/**
		 * 文本文档
		 */
		exts.put("txt", Byte.valueOf("5"));

		/**
		 * 压缩档
		 */
		exts.put("zip", Byte.valueOf("6"));
		exts.put("rar", Byte.valueOf("6"));

		/**
		 * PDF
		 */
		exts.put("pdf", Byte.valueOf("7"));

		/**
		 * mp3
		 */
		exts.put("mp3", Byte.valueOf("8"));

		/**
		 * mp4
		 */
		exts.put("mp4", Byte.valueOf("9"));

		if (exts.containsKey(fileExt)) {
			return exts.get(fileExt);
		}
		return 127;

	}

}
