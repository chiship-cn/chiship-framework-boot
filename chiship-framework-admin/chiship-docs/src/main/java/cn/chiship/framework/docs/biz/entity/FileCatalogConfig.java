package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class FileCatalogConfig implements Serializable {

	/**
	 * 主键
	 *
	 * @mbg.generated
	 */
	private String id;

	/**
	 * 创建时间
	 *
	 * @mbg.generated
	 */
	private Long gmtCreated;

	/**
	 * 更新时间
	 *
	 * @mbg.generated
	 */
	private Long gmtModified;

	/**
	 * 逻辑删除（0：否，1：是）
	 *
	 * @mbg.generated
	 */
	private Byte isDeleted;

	private String catalogUuid;

	private String catalogName;

	/**
	 * 发送状态 0 失败 1 成功
	 *
	 * @mbg.generated
	 */
	private String catalogPath;

	private String catalogFullPath;

	private String catalogPid;

	/**
	 * 是否是孩子元素
	 *
	 * @mbg.generated
	 */
	private Byte isChildren;

	private String projectsId;

	/**
	 * 模块
	 *
	 * @mbg.generated
	 */
	private String userId;

	private String userName;

	private String realName;

	/**
	 * 是否内置 0 否 1 是
	 *
	 * @mbg.generated
	 */
	private Byte isBuiltIn;

	/**
	 * 是否加密
	 *
	 * @mbg.generated
	 */
	private Byte isEncryption;

	private String encryptionPassword;

	private String encryptionSalt;

	/**
	 * 0：最近 1 收藏 2 个人文档 3 企业网盘 4 我的共享 5 回收站 6.我的设备 7.加密文件夹 8 普通文件夹
	 *
	 * @mbg.generated
	 */
	private Byte catalogType;

	/**
	 * 是否允许新建文件
	 *
	 * @mbg.generated
	 */
	private Byte isAllowNewFile;

	/**
	 * 是否允许新建文件夹
	 *
	 * @mbg.generated
	 */
	private Byte isAllowNewFolder;

	private Byte isAllowRead;

	private Byte isAllowDeleted;

	private Long orders;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getCatalogUuid() {
		return catalogUuid;
	}

	public void setCatalogUuid(String catalogUuid) {
		this.catalogUuid = catalogUuid;
	}

	public String getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	public String getCatalogPath() {
		return catalogPath;
	}

	public void setCatalogPath(String catalogPath) {
		this.catalogPath = catalogPath;
	}

	public String getCatalogFullPath() {
		return catalogFullPath;
	}

	public void setCatalogFullPath(String catalogFullPath) {
		this.catalogFullPath = catalogFullPath;
	}

	public String getCatalogPid() {
		return catalogPid;
	}

	public void setCatalogPid(String catalogPid) {
		this.catalogPid = catalogPid;
	}

	public Byte getIsChildren() {
		return isChildren;
	}

	public void setIsChildren(Byte isChildren) {
		this.isChildren = isChildren;
	}

	public String getProjectsId() {
		return projectsId;
	}

	public void setProjectsId(String projectsId) {
		this.projectsId = projectsId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public Byte getIsBuiltIn() {
		return isBuiltIn;
	}

	public void setIsBuiltIn(Byte isBuiltIn) {
		this.isBuiltIn = isBuiltIn;
	}

	public Byte getIsEncryption() {
		return isEncryption;
	}

	public void setIsEncryption(Byte isEncryption) {
		this.isEncryption = isEncryption;
	}

	public String getEncryptionPassword() {
		return encryptionPassword;
	}

	public void setEncryptionPassword(String encryptionPassword) {
		this.encryptionPassword = encryptionPassword;
	}

	public String getEncryptionSalt() {
		return encryptionSalt;
	}

	public void setEncryptionSalt(String encryptionSalt) {
		this.encryptionSalt = encryptionSalt;
	}

	public Byte getCatalogType() {
		return catalogType;
	}

	public void setCatalogType(Byte catalogType) {
		this.catalogType = catalogType;
	}

	public Byte getIsAllowNewFile() {
		return isAllowNewFile;
	}

	public void setIsAllowNewFile(Byte isAllowNewFile) {
		this.isAllowNewFile = isAllowNewFile;
	}

	public Byte getIsAllowNewFolder() {
		return isAllowNewFolder;
	}

	public void setIsAllowNewFolder(Byte isAllowNewFolder) {
		this.isAllowNewFolder = isAllowNewFolder;
	}

	public Byte getIsAllowRead() {
		return isAllowRead;
	}

	public void setIsAllowRead(Byte isAllowRead) {
		this.isAllowRead = isAllowRead;
	}

	public Byte getIsAllowDeleted() {
		return isAllowDeleted;
	}

	public void setIsAllowDeleted(Byte isAllowDeleted) {
		this.isAllowDeleted = isAllowDeleted;
	}

	public Long getOrders() {
		return orders;
	}

	public void setOrders(Long orders) {
		this.orders = orders;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", catalogUuid=").append(catalogUuid);
		sb.append(", catalogName=").append(catalogName);
		sb.append(", catalogPath=").append(catalogPath);
		sb.append(", catalogFullPath=").append(catalogFullPath);
		sb.append(", catalogPid=").append(catalogPid);
		sb.append(", isChildren=").append(isChildren);
		sb.append(", projectsId=").append(projectsId);
		sb.append(", userId=").append(userId);
		sb.append(", userName=").append(userName);
		sb.append(", realName=").append(realName);
		sb.append(", isBuiltIn=").append(isBuiltIn);
		sb.append(", isEncryption=").append(isEncryption);
		sb.append(", encryptionPassword=").append(encryptionPassword);
		sb.append(", encryptionSalt=").append(encryptionSalt);
		sb.append(", catalogType=").append(catalogType);
		sb.append(", isAllowNewFile=").append(isAllowNewFile);
		sb.append(", isAllowNewFolder=").append(isAllowNewFolder);
		sb.append(", isAllowRead=").append(isAllowRead);
		sb.append(", isAllowDeleted=").append(isAllowDeleted);
		sb.append(", orders=").append(orders);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		FileCatalogConfig other = (FileCatalogConfig) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getCatalogUuid() == null ? other.getCatalogUuid() == null
						: this.getCatalogUuid().equals(other.getCatalogUuid()))
				&& (this.getCatalogName() == null ? other.getCatalogName() == null
						: this.getCatalogName().equals(other.getCatalogName()))
				&& (this.getCatalogPath() == null ? other.getCatalogPath() == null
						: this.getCatalogPath().equals(other.getCatalogPath()))
				&& (this.getCatalogFullPath() == null ? other.getCatalogFullPath() == null
						: this.getCatalogFullPath().equals(other.getCatalogFullPath()))
				&& (this.getCatalogPid() == null ? other.getCatalogPid() == null
						: this.getCatalogPid().equals(other.getCatalogPid()))
				&& (this.getIsChildren() == null ? other.getIsChildren() == null
						: this.getIsChildren().equals(other.getIsChildren()))
				&& (this.getProjectsId() == null ? other.getProjectsId() == null
						: this.getProjectsId().equals(other.getProjectsId()))
				&& (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(other.getUserId()))
				&& (this.getUserName() == null ? other.getUserName() == null
						: this.getUserName().equals(other.getUserName()))
				&& (this.getRealName() == null ? other.getRealName() == null
						: this.getRealName().equals(other.getRealName()))
				&& (this.getIsBuiltIn() == null ? other.getIsBuiltIn() == null
						: this.getIsBuiltIn().equals(other.getIsBuiltIn()))
				&& (this.getIsEncryption() == null ? other.getIsEncryption() == null
						: this.getIsEncryption().equals(other.getIsEncryption()))
				&& (this.getEncryptionPassword() == null ? other.getEncryptionPassword() == null
						: this.getEncryptionPassword().equals(other.getEncryptionPassword()))
				&& (this.getEncryptionSalt() == null ? other.getEncryptionSalt() == null
						: this.getEncryptionSalt().equals(other.getEncryptionSalt()))
				&& (this.getCatalogType() == null ? other.getCatalogType() == null
						: this.getCatalogType().equals(other.getCatalogType()))
				&& (this.getIsAllowNewFile() == null ? other.getIsAllowNewFile() == null
						: this.getIsAllowNewFile().equals(other.getIsAllowNewFile()))
				&& (this.getIsAllowNewFolder() == null ? other.getIsAllowNewFolder() == null
						: this.getIsAllowNewFolder().equals(other.getIsAllowNewFolder()))
				&& (this.getIsAllowRead() == null ? other.getIsAllowRead() == null
						: this.getIsAllowRead().equals(other.getIsAllowRead()))
				&& (this.getIsAllowDeleted() == null ? other.getIsAllowDeleted() == null
						: this.getIsAllowDeleted().equals(other.getIsAllowDeleted()))
				&& (this.getOrders() == null ? other.getOrders() == null : this.getOrders().equals(other.getOrders()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getCatalogUuid() == null) ? 0 : getCatalogUuid().hashCode());
		result = prime * result + ((getCatalogName() == null) ? 0 : getCatalogName().hashCode());
		result = prime * result + ((getCatalogPath() == null) ? 0 : getCatalogPath().hashCode());
		result = prime * result + ((getCatalogFullPath() == null) ? 0 : getCatalogFullPath().hashCode());
		result = prime * result + ((getCatalogPid() == null) ? 0 : getCatalogPid().hashCode());
		result = prime * result + ((getIsChildren() == null) ? 0 : getIsChildren().hashCode());
		result = prime * result + ((getProjectsId() == null) ? 0 : getProjectsId().hashCode());
		result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
		result = prime * result + ((getUserName() == null) ? 0 : getUserName().hashCode());
		result = prime * result + ((getRealName() == null) ? 0 : getRealName().hashCode());
		result = prime * result + ((getIsBuiltIn() == null) ? 0 : getIsBuiltIn().hashCode());
		result = prime * result + ((getIsEncryption() == null) ? 0 : getIsEncryption().hashCode());
		result = prime * result + ((getEncryptionPassword() == null) ? 0 : getEncryptionPassword().hashCode());
		result = prime * result + ((getEncryptionSalt() == null) ? 0 : getEncryptionSalt().hashCode());
		result = prime * result + ((getCatalogType() == null) ? 0 : getCatalogType().hashCode());
		result = prime * result + ((getIsAllowNewFile() == null) ? 0 : getIsAllowNewFile().hashCode());
		result = prime * result + ((getIsAllowNewFolder() == null) ? 0 : getIsAllowNewFolder().hashCode());
		result = prime * result + ((getIsAllowRead() == null) ? 0 : getIsAllowRead().hashCode());
		result = prime * result + ((getIsAllowDeleted() == null) ? 0 : getIsAllowDeleted().hashCode());
		result = prime * result + ((getOrders() == null) ? 0 : getOrders().hashCode());
		return result;
	}

}