package cn.chiship.framework.docs.biz.pojo.dto;

import cn.chiship.sdk.core.base.constants.BaseTipConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * @author LiJian
 */
@ApiModel(value = "操作回收站表单")
public class OptionRecycleBinDto {

	@ApiModelProperty(value = "资源ID")
	private String fileId;

	@ApiModelProperty(value = "资源分类（0：企业 1：个人）", required = true)
	@NotNull(message = "资源分类" + BaseTipConstants.NOT_EMPTY)
	@Min(0)
	@Max(1)
	private Byte type;

	private Byte isFile;

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public Byte getType() {
		return type;
	}

	public void setType(Byte type) {
		this.type = type;
	}

	public Byte getIsFile() {
		return isFile;
	}

	public void setIsFile(Byte isFile) {
		this.isFile = isFile;
	}

}
