package cn.chiship.framework.docs.biz.entity;

import java.io.Serializable;

/**
 * @author lijian
 */
public class FileChangeStatus implements Serializable {

	private String id;

	private Long gmtCreated;

	private Long gmtModified;

	private Byte isDeleted;

	private String optionUserId;

	private String optionUserName;

	private String optionRealName;

	private Byte resourceType;

	private String originalId;

	private String originalName;

	private String targetId;

	private String targetName;

	private Byte changeStatus;

	private String changeRemark;

	private static final long serialVersionUID = 1L;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getGmtCreated() {
		return gmtCreated;
	}

	public void setGmtCreated(Long gmtCreated) {
		this.gmtCreated = gmtCreated;
	}

	public Long getGmtModified() {
		return gmtModified;
	}

	public void setGmtModified(Long gmtModified) {
		this.gmtModified = gmtModified;
	}

	public Byte getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getOptionUserId() {
		return optionUserId;
	}

	public void setOptionUserId(String optionUserId) {
		this.optionUserId = optionUserId;
	}

	public String getOptionUserName() {
		return optionUserName;
	}

	public void setOptionUserName(String optionUserName) {
		this.optionUserName = optionUserName;
	}

	public String getOptionRealName() {
		return optionRealName;
	}

	public void setOptionRealName(String optionRealName) {
		this.optionRealName = optionRealName;
	}

	public Byte getResourceType() {
		return resourceType;
	}

	public void setResourceType(Byte resourceType) {
		this.resourceType = resourceType;
	}

	public String getOriginalId() {
		return originalId;
	}

	public void setOriginalId(String originalId) {
		this.originalId = originalId;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getTargetId() {
		return targetId;
	}

	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}

	public String getTargetName() {
		return targetName;
	}

	public void setTargetName(String targetName) {
		this.targetName = targetName;
	}

	public Byte getChangeStatus() {
		return changeStatus;
	}

	public void setChangeStatus(Byte changeStatus) {
		this.changeStatus = changeStatus;
	}

	public String getChangeRemark() {
		return changeRemark;
	}

	public void setChangeRemark(String changeRemark) {
		this.changeRemark = changeRemark;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", id=").append(id);
		sb.append(", gmtCreated=").append(gmtCreated);
		sb.append(", gmtModified=").append(gmtModified);
		sb.append(", isDeleted=").append(isDeleted);
		sb.append(", optionUserId=").append(optionUserId);
		sb.append(", optionUserName=").append(optionUserName);
		sb.append(", optionRealName=").append(optionRealName);
		sb.append(", resourceType=").append(resourceType);
		sb.append(", originalId=").append(originalId);
		sb.append(", originalName=").append(originalName);
		sb.append(", targetId=").append(targetId);
		sb.append(", targetName=").append(targetName);
		sb.append(", changeStatus=").append(changeStatus);
		sb.append(", changeRemark=").append(changeRemark);
		sb.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}
		if (that == null) {
			return false;
		}
		if (getClass() != that.getClass()) {
			return false;
		}
		FileChangeStatus other = (FileChangeStatus) that;
		return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
				&& (this.getGmtCreated() == null ? other.getGmtCreated() == null
						: this.getGmtCreated().equals(other.getGmtCreated()))
				&& (this.getGmtModified() == null ? other.getGmtModified() == null
						: this.getGmtModified().equals(other.getGmtModified()))
				&& (this.getIsDeleted() == null ? other.getIsDeleted() == null
						: this.getIsDeleted().equals(other.getIsDeleted()))
				&& (this.getOptionUserId() == null ? other.getOptionUserId() == null
						: this.getOptionUserId().equals(other.getOptionUserId()))
				&& (this.getOptionUserName() == null ? other.getOptionUserName() == null
						: this.getOptionUserName().equals(other.getOptionUserName()))
				&& (this.getOptionRealName() == null ? other.getOptionRealName() == null
						: this.getOptionRealName().equals(other.getOptionRealName()))
				&& (this.getResourceType() == null ? other.getResourceType() == null
						: this.getResourceType().equals(other.getResourceType()))
				&& (this.getOriginalId() == null ? other.getOriginalId() == null
						: this.getOriginalId().equals(other.getOriginalId()))
				&& (this.getOriginalName() == null ? other.getOriginalName() == null
						: this.getOriginalName().equals(other.getOriginalName()))
				&& (this.getTargetId() == null ? other.getTargetId() == null
						: this.getTargetId().equals(other.getTargetId()))
				&& (this.getTargetName() == null ? other.getTargetName() == null
						: this.getTargetName().equals(other.getTargetName()))
				&& (this.getChangeStatus() == null ? other.getChangeStatus() == null
						: this.getChangeStatus().equals(other.getChangeStatus()))
				&& (this.getChangeRemark() == null ? other.getChangeRemark() == null
						: this.getChangeRemark().equals(other.getChangeRemark()));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		result = prime * result + ((getGmtCreated() == null) ? 0 : getGmtCreated().hashCode());
		result = prime * result + ((getGmtModified() == null) ? 0 : getGmtModified().hashCode());
		result = prime * result + ((getIsDeleted() == null) ? 0 : getIsDeleted().hashCode());
		result = prime * result + ((getOptionUserId() == null) ? 0 : getOptionUserId().hashCode());
		result = prime * result + ((getOptionUserName() == null) ? 0 : getOptionUserName().hashCode());
		result = prime * result + ((getOptionRealName() == null) ? 0 : getOptionRealName().hashCode());
		result = prime * result + ((getResourceType() == null) ? 0 : getResourceType().hashCode());
		result = prime * result + ((getOriginalId() == null) ? 0 : getOriginalId().hashCode());
		result = prime * result + ((getOriginalName() == null) ? 0 : getOriginalName().hashCode());
		result = prime * result + ((getTargetId() == null) ? 0 : getTargetId().hashCode());
		result = prime * result + ((getTargetName() == null) ? 0 : getTargetName().hashCode());
		result = prime * result + ((getChangeStatus() == null) ? 0 : getChangeStatus().hashCode());
		result = prime * result + ((getChangeRemark() == null) ? 0 : getChangeRemark().hashCode());
		return result;
	}

}