package cn.chiship.framework.docs.biz.service.impl;

import cn.chiship.framework.common.constants.CommonCacheConstants;
import cn.chiship.framework.docs.biz.entity.FileCatalogConfig;
import cn.chiship.framework.docs.biz.entity.FileCatalogConfigExample;
import cn.chiship.framework.docs.biz.mapper.FileCatalogConfigMapper;
import cn.chiship.framework.docs.biz.service.DocsCacheService;
import cn.chiship.sdk.cache.service.RedisService;
import cn.chiship.sdk.core.base.constants.BaseCacheConstants;
import cn.chiship.sdk.core.base.constants.BaseConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author lijian
 */
@Service
public class DocsCacheServiceImpl implements DocsCacheService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DocsCacheServiceImpl.class);

	@Resource
	RedisService redisService;

	@Resource
	FileCatalogConfigMapper fileCatalogConfigMapper;

	@Override
	public void cacheFileCatalog() {
		String key = CommonCacheConstants.buildKey(BaseCacheConstants.REDIS_FILE_CATALOG_CONFIG_PREFIX);
		redisService.del(key);
		FileCatalogConfigExample fileCatalogConfigExample = new FileCatalogConfigExample();
		fileCatalogConfigExample.createCriteria().andIsDeletedEqualTo(BaseConstants.NO);
		List<FileCatalogConfig> fileCatalogConfigs = fileCatalogConfigMapper.selectByExample(fileCatalogConfigExample);
		for (FileCatalogConfig fileCatalogConfig : fileCatalogConfigs) {
			redisService.hset(key, fileCatalogConfig.getId(), fileCatalogConfig);
		}
	}

}
