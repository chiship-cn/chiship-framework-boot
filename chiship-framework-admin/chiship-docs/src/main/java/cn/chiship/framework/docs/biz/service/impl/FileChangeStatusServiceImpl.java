package cn.chiship.framework.docs.biz.service.impl;

import cn.chiship.framework.docs.biz.entity.FileChangeStatus;
import cn.chiship.framework.docs.biz.entity.FileChangeStatusExample;
import cn.chiship.framework.docs.biz.mapper.FileChangeStatusMapper;
import cn.chiship.framework.docs.biz.pojo.dto.FileCatalogChangeStatusDto;
import cn.chiship.framework.docs.biz.pojo.dto.FileChangeStatusDto;
import cn.chiship.framework.docs.biz.service.FileChangeStatusService;
import cn.chiship.framework.docs.core.common.FileCatalogChangeEnum;
import cn.chiship.framework.docs.core.common.FileChangeEnum;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.core.id.SnowflakeIdUtil;
import cn.chiship.sdk.framework.base.BaseServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * FileChangeStatusService实现
 *
 * @author lijian
 * @date 2020/4/28
 */
@Service
public class FileChangeStatusServiceImpl extends BaseServiceImpl<FileChangeStatus, FileChangeStatusExample>
		implements FileChangeStatusService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileChangeStatusServiceImpl.class);

	@Resource
	FileChangeStatusMapper fileChangeStatusMapper;

	@Override
	public BaseResult saveChange(FileChangeStatusDto fileChangeStatusDto, FileChangeEnum fileChangeEnum) {
		FileChangeStatus fileChangeStatus = new FileChangeStatus();
		BeanUtils.copyProperties(fileChangeStatusDto, fileChangeStatus);
		fileChangeStatus.setId(SnowflakeIdUtil.generateStrId());
		fileChangeStatus.setGmtCreated(System.currentTimeMillis());
		fileChangeStatus.setGmtModified(System.currentTimeMillis());
		fileChangeStatus.setIsDeleted(Byte.valueOf("0"));
		fileChangeStatus.setResourceType(Byte.valueOf("1"));
		fileChangeStatus.setChangeStatus(Byte.valueOf(fileChangeEnum.getCode() + ""));
		fileChangeStatus.setChangeRemark(fileChangeEnum.getMessage());
		fileChangeStatusMapper.insertSelective(fileChangeStatus);
		return BaseResult.ok();
	}

	@Override
	public BaseResult saveChange(FileCatalogChangeStatusDto fileCatalogChangeStatusDto,
			FileCatalogChangeEnum fileCatalogChangeEnum) {
		FileChangeStatus fileChangeStatus = new FileChangeStatus();
		BeanUtils.copyProperties(fileCatalogChangeStatusDto, fileChangeStatus);
		fileChangeStatus.setResourceType(Byte.valueOf("0"));
		fileChangeStatus.setChangeStatus(Byte.valueOf(fileCatalogChangeEnum.getCode() + ""));
		fileChangeStatus.setChangeRemark(fileCatalogChangeEnum.getMessage());
		return super.insertSelective(fileChangeStatus);
	}

}
