package cn.chiship.framework.docs.biz.service;

import cn.chiship.framework.docs.biz.entity.FileResources;
import cn.chiship.framework.docs.biz.entity.FileResourcesExample;
import cn.chiship.sdk.cache.vo.CacheUserVO;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * FileResourcesService接口
 *
 * @author lijian
 * @date 2020/4/24
 */
public interface FileResourcesService extends BaseService<FileResources, FileResourcesExample> {

	/**
	 * 从缓存获取
	 * @param fileUuid
	 * @return
	 */
	FileResources cacheByFileUuid(String fileUuid);

	/**
	 * 缓存获取
	 * @param fileUuid
	 * @return
	 */
	List<FileResources> cacheByFileUuid(List<String> fileUuid);

	/**
	 * 新增文件
	 * @param catalogId
	 * @param fileName
	 * @param originalName
	 * @param fileSize
	 * @param contentTyp
	 * @param fileExt
	 * @param userId
	 * @param userName
	 * @param realName
	 * @param storageLocation
	 * @param storagePath
	 * @param storageRelativePath
	 * @return
	 */
	BaseResult fileResourcesAdd(String catalogId, String fileName, String originalName, Long fileSize,
			String contentTyp, String fileExt, String userId, String userName, String realName, Byte storageLocation,
			String storagePath, String storageRelativePath);

	/**
	 * 添加至收藏
	 * @param projectsId
	 * @param fileId
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult collectionAdd(String projectsId, String fileId, String userId, String userName, String realName);

	/**
	 * 取消收藏
	 * @param fileId
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult collectionCancel(String fileId, String userId, String userName, String realName);

	/**
	 * 移入回收站
	 * @param projectsId
	 * @param fileId
	 * @param type 0:企业 1 个人
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult moveToRecycleBin(String projectsId, String fileId, Byte type, String userId, String userName,
			String realName);

	/**
	 * 删除文件
	 * @param fileUuid
	 * @return
	 */
	BaseResult removeFile(String fileUuid);

	/**
	 * 文件还原
	 * @param fileId
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult reduction(String fileId, String userId, String userName, String realName);

	/**
	 * 文件重命名
	 * @param fileResources
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult rename(FileResources fileResources, String userId, String userName, String realName);

	/**
	 * 修改是否允许删除
	 * @param isAllowDelete
	 * @param id
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult changeIsAllowDelete(Boolean isAllowDelete, String id, String userId, String userName, String realName);

	/**
	 * 文件上传处理
	 * @param inputStream
	 * @param catalogId
	 * @param fileName
	 * @param contentType
	 * @param cacheUserVO
	 * @return
	 * @throws IOException
	 */
	BaseResult uploadFile(InputStream inputStream, String catalogId, String fileName, String contentType,
			CacheUserVO cacheUserVO) throws IOException;

	/**
	 * 文件上传处理
	 * @param multipartFile
	 * @param catalogId
	 * @param cacheUserVO
	 * @return
	 * @throws IOException
	 */
	BaseResult uploadFile(MultipartFile multipartFile, String catalogId, CacheUserVO cacheUserVO);

	/**
	 * 获取同文件夹所有文件
	 * @param uuid
	 * @return
	 */
	BaseResult listSameFolderFileByUuid(String uuid);

}
