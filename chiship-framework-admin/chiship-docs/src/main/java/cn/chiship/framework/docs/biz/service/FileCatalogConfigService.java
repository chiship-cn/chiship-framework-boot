package cn.chiship.framework.docs.biz.service;

import cn.chiship.framework.docs.biz.entity.FileCatalogConfigExample;
import cn.chiship.sdk.core.base.BaseResult;
import cn.chiship.sdk.framework.base.BaseService;
import cn.chiship.framework.docs.biz.entity.FileCatalogConfig;

/**
 * FileCatalogConfigService接口
 *
 * @author lijian
 * @date 2020/4/23
 */
public interface FileCatalogConfigService extends BaseService<FileCatalogConfig, FileCatalogConfigExample> {

	/**
	 * 重命名
	 * @param fileCatalogConfig
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult updateCatalogNameById(FileCatalogConfig fileCatalogConfig, String userId, String userName,
			String realName);

	/**
	 * 根据ID集合修改文件夹所属
	 * @param id
	 * @param userId
	 * @param userName
	 * @param realName
	 * @return
	 */
	BaseResult updatePidByIdList(String id, String userId, String userName, String realName);

	/**
	 * 初始化文件夹
	 * @param projectsId
	 * @return
	 */
	BaseResult initCatalog(String projectsId);

}
