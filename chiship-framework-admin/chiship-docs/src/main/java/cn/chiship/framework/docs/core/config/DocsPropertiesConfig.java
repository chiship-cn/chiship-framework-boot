package cn.chiship.framework.docs.core.config;

import cn.chiship.framework.common.properties.FrameworkProperties;
import cn.chiship.framework.docs.core.properties.FileUploadProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lijian
 */
@Configuration
@EnableConfigurationProperties(value = { FileUploadProperties.class })
public class DocsPropertiesConfig {
    @Bean
    public FileUploadProperties fileUploadProperties() {
        return new FileUploadProperties();
    }
}
