/**
 * 获得文件预览路径
 * @param {*} url   路径或资源标识
 * @param {*} fileType 类型
 * @param {*} type  打开模式 view-预览 share-分享
 * @param {*} title 标题
 */
export function getViewUrl(url, fileType, type, title) {
  var result = null
  switch (fileType) {
    case 1:
      result = `/fileView/images/${type}/${encodeURIComponent(url)}`
      break
    case 3:
      result = `/fileView/excel/${type}/${url}`
      break
    case 7:
      result = `/fileView/pdf/${type}/${url}`
      break
    case 8:
    case 9:
      result = `/fileView/video/${type}/${url}/${title}`
      break
    default:
      result = `/fileView/notSupport/${type}/${url}`
  }
  return result
}
