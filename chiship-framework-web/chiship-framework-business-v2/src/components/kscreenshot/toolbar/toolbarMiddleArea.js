import colorBoard from './colorBoard'
import setLineWidth from './setLineWidth'

export default function toolbarMiddleArea(me) {
  const toolbarMiddleArea = document.createElement('span')
  toolbarMiddleArea.id = 'kssToolbarMiddleArea'

  toolbarMiddleArea.appendChild(colorBoard(me))
  toolbarMiddleArea.appendChild(setLineWidth(me))

  return toolbarMiddleArea
}
