import {
  css,
  typeChecking
} from '../../util'
import copy from '../copy'
import download from '../download'
import endAndClear from '../endAndClear'
import img from '../../assets/imgs/complete.png'

export default function completeBT(me) {
  const completeBT = document.createElement('span')
  completeBT.id = 'kssCompleteBT'
  completeBT.className = 'kssToolbarItemBT'
  // completeBT.innerHTML = '√完成'
  completeBT.title = '完成截图'
  css(completeBT, {
    // width: '40px',
    // 'line-height': '28px'
  })
  const colorImg = document.createElement('img')
  colorImg.className = 'kssToolbarItemImg'
  colorImg.src = img
  me.colorImg = colorImg

  completeBT.appendChild(colorImg)
  completeBT.addEventListener('click', function() {
    me.isEdit = true
    const lastShot = me.snapshootList[me.snapshootList.length - 1]
    copy(me, lastShot)
    me.needDownload === true && download(me)
    typeChecking(me.endCB) === '[object Function]' && me.endCB(lastShot)
    endAndClear(me)
    completeBT.removeEventListener('click', function() {})
  })

  return completeBT
}
