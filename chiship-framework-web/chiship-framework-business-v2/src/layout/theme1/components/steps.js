const steps = [{
  element: '#sidebar-org-container',
  popover: {
    title: '切换组织',
    description: '可以切换已加入组织',
    position: 'bottom'
  },
  padding: 0
}, {
  element: '#sidebar-footer-container',
  popover: {
    title: '展开收缩',
    description: '打开或关闭侧边栏',
    position: 'bottom'
  }
}, {
  element: '#systemInfo-container',
  popover: {
    title: '系统消息',
    description: '展示系统消息或通知公告，也可进入我的消息列表',
    position: 'left'
  }
}, {
  element: '#screenfull',
  popover: {
    title: '全屏',
    description: '设置页面进入全屏模式',
    position: 'left'
  }
}, {
  element: '#person-info-container',
  popover: {
    title: '个人中心',
    description: '进入个人中心、修改密码、手机号、邮箱、锁屏等',
    position: 'left'
  }
}, {
  element: '#tags-view-container',
  popover: {
    title: '标签',
    description: '您浏览的页面历史',
    position: 'bottom'
  },
  padding: 0
}, {
  element: '#chat-container',
  popover: {
    title: '聊天入口',
    description: '进入聊天系统，拖动可改变位置',
    position: 'right'
  }
}]

export default steps
