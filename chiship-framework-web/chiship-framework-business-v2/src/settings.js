var title = 'Chiship●JAVA快速开发平台(单体版)'
var company = '济南智舰网络科技有限公司'
/**
 * 默认设置
 */
module.exports = {

  // 标题
  title,

  // 公司
  company,
  // 未活动时长开始锁屏，单位：分钟
  lockScreen: 60,

  // Token Key
  tokenKey: 'CHISHIP_FRAMEWORK_BOOT_TOKEN',

  // 项目ID
  projectId: '1883442361',
  // APP_ID
  appId: '638638776006557696',
  // APP_KEY
  appKey: '61cada1e12324f79bf404f4b3cc6d9d7',

  // 公钥
  publicKey: `-----BEGIN PUBLIC KEY-----
  MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCmkpXILKab5JZglcgE1NHSCrcxIQwNvFnTPyHsOd9hqXTap24eCVURmRSpAzkOcnRy6SvNjvXMtHMB4VDy34jBtgs1OQIi03eY1BnFcZupDkBxObtetUr5DnDaE2KbvinaSHQH9n1MyM7RVUZLhlZwE/URCLmHttJrPQfmY/DPEQIDAQAB
  -----END PUBLIC KEY-----`,
  // 私钥
  privateKey: `-----BEGIN PRIVATE KEY-----
  MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAKaSlcgsppvklmCVyATU0dIKtzEhDA28WdM/Iew532GpdNqnbh4JVRGZFKkDOQ5ydHLpK82O9cy0cwHhUPLfiMG2CzU5AiLTd5jUGcVxm6kOQHE5u161SvkOcNoTYpu+KdpIdAf2fUzIztFVRkuGVnAT9REIuYe20ms9B+Zj8M8RAgMBAAECgYEApd+nBDVinC8fiILfMeB0KQO+tU/BXxRHJtPhhgmDZw+GbA762zJT4jhcmIm7EaXsTFnh4ssP/o9bT23+XD05QoAKfn25Qj4HFVysdcI+KjBfw0XANS3Pe1LvXR7dRpocJNl7kPVbILsu+wmi9w9K19ieBZkxUEs0EVajX8rGf1kCQQDkpodOMrazIJPkbeyJrDVlkgWSI47b7fb6IkTX3ZFzixkZgMFxijgvZbu8vDy0j2xpjcRjw6IfINYSeKQ3r2mDAkEAun8tv2k4xhcZIYdjc4TtNj37qjcjctKYzTXMpptCm4KWEy8kJz6zcx5jbTmyHErdpkD9bPULMWvB47i6IH6E2wJBAM7OgFsOK4lg0eMuOV9cTv+LT1aaqr/pQBWFoVbNpJ0pFo6mklCrf53/GgrfBtkZUCk4fITvkVcuT/FtBLsJARkCQQCrVcYAsTmQe44CCLEsYvXPPHil84wkpCfvd7qxYbh6yCj6LPgI+gjA/S0ZHsVsSreBUvnAjQugdsAlZwPQcIu7AkAgbuP2B/MMbX3HXYKBDjUj7Frpm+1mF0jA+7ebzrH747+2xG1XFVbDcsbaOkHxTTCVwOyK10i8nVe1Ke7tmLG8
  -----END PRIVATE KEY-----`,
  // 签名串
  signKey: '6b50e13',

  // 开启页面权限
  openPagePer: false,
  // 白名单 可以在路由中设置 meta对象添加 auth=false
  whiteList: new Set(['login', 'password', 'register', 'company', 'noPermission']),
  // 系统配置项key
  configKey: 'systemName,siteVersion,technicalSupport,companyTel,CopyrightInfo,filingInfo,registerUser,loginScan,loginMobile',
  // 默认系统配置
  defaultConfig: {
    systemName: this.title, // 系统名称
    siteVersion: '1.0', // 版本
    technicalSupport: company, // 技术支持
    companyTel: '18363000000', // 联系电话
    CopyrightInfo: `Copyright @ 2020-2024 ${company}版权所有 `, // 版权信息
    filingInfo: ` <a href="https://beian.miit.gov.cn/#/Integrated/index" target="blank">鲁ICP备17045419号</a>`, // 备案信息
    registerUser: '1', // 是否开启注册
    loginScan: '0', // 是否开启扫码登录
    loginMobile: '1'// 是否开启手机登录
  }

}
