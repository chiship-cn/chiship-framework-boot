import Chiship from '@/Chiship'
import store from '@/store'
const defaultSettings = require('@/settings.js')
import { Message } from 'element-ui'

loadDefault()
async function loadDefault() {
  let chiship
  try {
    var softInfo = await store.dispatch('getSoftInfo')
    var systemConfig = await store.dispatch('getSystemConfig', defaultSettings.configKey)
    chiship = new Chiship({
      title: systemConfig.systemName,
      config: {
        ...softInfo,
        ...systemConfig
      }
    })
  } catch (err) {
    Message({
      message: '加载配置项失败，将使用默认配置！',
      type: 'warning',
      duration: 5 * 1000
    })
    chiship = new Chiship({})
  }
  chiship.init()
}
