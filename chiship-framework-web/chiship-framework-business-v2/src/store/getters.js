const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.sso.token,
  userInfo: state => state.sso.userInfo,
  roleInfo: state => {
    const roleInfo = state.sso.userInfo.cacheRoleVo
    if (!roleInfo) {
      return {}
    }
    return roleInfo
  },
  roleInfos: state => {
    const roleInfos = state.sso.userInfo.cacheRoleVos
    if (!roleInfos) {
      return []
    }
    return roleInfos
  },
  orgInfo: (state) => {
    const userInfo = state.sso.userInfo
    var organizationVo = userInfo.cacheOrganizationVo
    if (organizationVo) {
      const org = {
        id: organizationVo.id,
        name: organizationVo.name,
        code: organizationVo.code,
        level: organizationVo.level,
        type: organizationVo.type,
        tag: organizationVo.tag,
        treeNumber: organizationVo.treeNumber,
        regionLevel1: organizationVo.regionLevel1,
        regionLevel2: organizationVo.regionLevel2,
        regionLevel3: organizationVo.regionLevel3,
        regionLevel4: organizationVo.regionLevel4,
        regionAddress: organizationVo.regionAddress
      }
      return org
    } else {
      return {}
    }
  },
  deptInfo: (state) => {
    const userInfo = state.sso.userInfo
    var departmentVo = userInfo.cacheDepartmentVo
    if (departmentVo) {
      const dept = {
        id: departmentVo.id,
        name: departmentVo.name,
        code: departmentVo.code
      }
      return dept
    } else {
      return {}
    }
  },
  orgInfos: state => {
    const orgInfos = state.sso.userInfo.cacheOrganizationVos
    if (!orgInfos) {
      return []
    }
    return orgInfos
  },
  deptInfos: state => {
    const deptInfos = state.sso.userInfo.cacheDepartmentVos
    if (!deptInfos) {
      return []
    }
    return deptInfos
  },
  permission: state => state.sso.permission,
  permission_routers: state => state.permission.routers,
  addRouters: state => state.permission.addRouters
}
export default getters
