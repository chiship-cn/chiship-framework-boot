import request from '@/utils/request'
const MemberUserService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 会员分页
     */
    memberUserPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUser/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员列表
     */
    memberUserList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUser/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得会员
    */
    memberUserGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUser/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得会员详情
    */
    memberUserGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUser/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 会员更新
     */
    memberUserUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUser/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员删除
     */
    memberUserRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUser/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员根据指定字段校验数据是否存在
     */
    memberUserValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUser/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员导出
     */
    memberUserExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.exportData('memberUser/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员实时导出进度
     */
    memberUserGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.exportData('memberUser/getExportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员导出
     */
    memberUserImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUser/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员实时导入进度
     */
    memberUserGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUser/getImportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default MemberUserService
