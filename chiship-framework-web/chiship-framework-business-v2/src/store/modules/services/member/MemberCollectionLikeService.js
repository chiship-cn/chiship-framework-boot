import request from '@/utils/request'
const MemberCollectionLikeService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 收藏点赞分页
     */
    memberCollectionLikePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberCollectionLike/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞列表
     */
    memberCollectionLikeList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberCollectionLike/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得收藏点赞
    */
    memberCollectionLikeGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberCollectionLike/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得收藏点赞详情
    */
    memberCollectionLikeGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberCollectionLike/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 收藏点赞保存
     */
    memberCollectionLikeSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberCollectionLike/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞更新
     */
    memberCollectionLikeUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberCollectionLike/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞删除
     */
    memberCollectionLikeRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberCollectionLike/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞根据指定字段校验数据是否存在
     */
    memberCollectionLikeValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberCollectionLike/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞导出
     */
    memberCollectionLikeExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberCollectionLike/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞实时导出进度
     */
    memberCollectionLikeGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberCollectionLike/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞导入
     */
    memberCollectionLikeImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberCollectionLike/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 收藏点赞实时导入进度
     */
    memberCollectionLikeGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberCollectionLike/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default MemberCollectionLikeService
