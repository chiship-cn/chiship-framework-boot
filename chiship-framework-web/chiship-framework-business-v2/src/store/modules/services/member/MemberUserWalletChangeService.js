import request from '@/utils/request'
const MemberUserWalletChangeService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 会员钱包变化记录分页
     */
    memberUserWalletChangePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWalletChange/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录列表
     */
    memberUserWalletChangeList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWalletChange/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得会员钱包变化记录
    */
    memberUserWalletChangeGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWalletChange/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得会员钱包变化记录详情
    */
    memberUserWalletChangeGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWalletChange/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录删除
     */
    memberUserWalletChangeRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWalletChange/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录根据指定字段校验数据是否存在
     */
    memberUserWalletChangeValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWalletChange/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录导出
     */
    memberUserWalletChangeExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberUserWalletChange/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录实时导出进度
     */
    memberUserWalletChangeGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberUserWalletChange/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录导入
     */
    memberUserWalletChangeImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWalletChange/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员钱包变化记录实时导入进度
     */
    memberUserWalletChangeGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWalletChange/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default MemberUserWalletChangeService
