import request from '@/utils/request'
const MemberCommentService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 评论表分页
     */
    memberCommentPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberComment/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表列表
     */
    memberCommentList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberComment/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得评论表
    */
    memberCommentGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberComment/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得评论表详情
    */
    memberCommentGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberComment/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 评论表保存
     */
    memberCommentSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表更新
     */
    memberCommentUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表删除
     */
    memberCommentRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表根据指定字段校验数据是否存在
     */
    memberCommentValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberComment/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表导出
     */
    memberCommentExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberComment/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表实时导出进度
     */
    memberCommentGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberComment/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表导入
     */
    memberCommentImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论表实时导入进度
     */
    memberCommentGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论审核通过
     */
    memberCommentPass({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/pass/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 评论审核屏蔽
     */
    memberCommentShield({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('memberComment/shield/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default MemberCommentService
