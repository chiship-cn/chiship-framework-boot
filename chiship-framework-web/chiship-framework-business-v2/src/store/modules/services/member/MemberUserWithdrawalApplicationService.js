import request from '@/utils/request'
const MemberUserWithdrawalApplicationService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 提现申请分页
     */
    memberUserWithdrawalApplicationPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWithdrawalApplication/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请列表
     */
    memberUserWithdrawalApplicationList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWithdrawalApplication/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得提现申请
    */
    memberUserWithdrawalApplicationGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWithdrawalApplication/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得提现申请详情
    */
    memberUserWithdrawalApplicationGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWithdrawalApplication/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请删除
     */
    memberUserWithdrawalApplicationRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWithdrawalApplication/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请根据指定字段校验数据是否存在
     */
    memberUserWithdrawalApplicationValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('memberUserWithdrawalApplication/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请导出
     */
    memberUserWithdrawalApplicationExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberUserWithdrawalApplication/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请实时导出进度
     */
    memberUserWithdrawalApplicationGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('memberUserWithdrawalApplication/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请导入
     */
    memberUserWithdrawalApplicationImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWithdrawalApplication/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请实时导入进度
     */
    memberUserWithdrawalApplicationGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWithdrawalApplication/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 提现申请审批
     */
    memberUserWithdrawalApplicationExamine({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUserWithdrawalApplication/examine', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default MemberUserWithdrawalApplicationService
