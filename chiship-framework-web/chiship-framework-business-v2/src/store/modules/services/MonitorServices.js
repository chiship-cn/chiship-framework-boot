import request from '@/utils/request'
const MonitorServices = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 获得软件信息
     */
    getSoftInfo({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('monitor/softInfo', { loading: false }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    apiAllMicroservices({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('allRouter').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 性能监控-服务器信息
     * @returns
     */
    monitorEnv({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('actuator/metrics', params).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 性能监控-服务器信息
     */
    monitorSystemInfo({ commit }) {
      return new Promise((resolve, reject) => {
        Promise.all([
          request.get('actuator/metrics/system.cpu.count'),
          request.get('actuator/metrics/system.cpu.usage'),
          request.get('actuator/metrics/process.start.time'),
          request.get('actuator/metrics/process.uptime'),
          request.get('actuator/metrics/process.cpu.usage')
        ]).then((values) => {
          resolve(values)
        })
      })
    },

    /**
     * 性能监控-Tomcat信息
     * @returns
     */
    monitorTomcatInfo({ commit }) {
      return new Promise((resolve, reject) => {
        Promise.all([
          request.get('actuator/metrics/tomcat.sessions.created'),
          request.get('actuator/metrics/tomcat.sessions.expired'),
          request.get('actuator/metrics/tomcat.sessions.active.current'),
          request.get('actuator/metrics/tomcat.sessions.active.max'),
          request.get('actuator/metrics/tomcat.sessions.rejected')

          // 2.3.5.RELEASE 无此API
          // request.get('actuator/metrics/tomcat.global.sent'),
          // request.get('actuator/metrics/tomcat.global.request.max'),
          // request.get('actuator/metrics/tomcat.global.request'),
          // request.get('actuator/metrics/tomcat.threads.current'),
          // request.get('actuator/metrics/tomcat.threads.config.max'),
          // 2.1.3.RELEASE 无此API
          // request.get('actuator/metrics/tomcat.servlet.request'),
          // request.get('actuator/metrics/tomcat.servlet.request.max')
        ]).then((values) => {
          resolve(values)
        })
      })
    },

    /**
     * 性能监控-JVM信息
     * @returns
     */
    monitorJVMInfo({ commit }, params) {
      return new Promise((resolve, reject) => {
        Promise.all([
          request.get('actuator/metrics/jvm.memory.max', params),
          request.get('actuator/metrics/jvm.memory.committed', params),
          request.get('actuator/metrics/jvm.memory.used', params),
          request.get('actuator/metrics/jvm.buffer.memory.used', params),
          request.get('actuator/metrics/jvm.buffer.count', params),
          request.get('actuator/metrics/jvm.threads.daemon', params),
          request.get('actuator/metrics/jvm.threads.live', params),
          request.get('actuator/metrics/jvm.threads.peak', params),
          request.get('actuator/metrics/jvm.classes.loaded', params),
          request.get('actuator/metrics/jvm.classes.unloaded', params),
          request.get('actuator/metrics/jvm.gc.memory.allocated', params),
          request.get('actuator/metrics/jvm.gc.memory.promoted', params),
          request.get('actuator/metrics/jvm.gc.max.data.size', params),
          request.get('actuator/metrics/jvm.gc.live.data.size', params),
          request.get('actuator/metrics/jvm.gc.pause', params)
        ]).then((values) => {
          resolve(values)
        })
      })
    },

    /**
     * 性能监控-请求追踪
     * @returns
     */
    monitorHttpTrace({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('actuator/httptrace').then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 性能监控-服务监控
     * @returns
     */
    monitorServer({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('monitor/server', params).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 性能监控-Redis
     * @returns
     */
    monitorRedis({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('monitor/redis').then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 性能监控-Redis Key
     * @returns
     */
    redisKeys({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('monitor/redis/keys').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 性能监控-Redis Values
     * @returns
     */
    redisValues({ commit }, key) {
      return new Promise((resolve, reject) => {
        request.post('monitor/redis/value', key).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default MonitorServices
