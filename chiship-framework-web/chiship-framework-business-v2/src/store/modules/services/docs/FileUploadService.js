import request from '@/utils/request'

const FileUploadService = {
  state: {},

  mutations: {},

  actions: {
    // file 上传
    fileUpload({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.uploadFile('file/upload', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // Base64图片上传
    base64Upload({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('file/base64Upload', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 分片合并文件
     */
    chunksMerge({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('file/chunksMerge', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default FileUploadService
