import request from '@/utils/request'
const FileResourcesService = {
  state: {},

  mutations: {},

  actions: {

    fileStorageLocation({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/storageLocation', {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    getFileResourcesById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/getById/' + id, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    currentUserFilePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/user/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesChangeHistory({ commit }, fileId) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/listChangeByFileId', { fileId: fileId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesGetByUUID({ commit }, uuid) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/getByUUID/' + uuid, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取同文件夹同类型数据
    listFileResourcesSameFolderFileByUUID({ commit }, uuid) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/listSameFolderFileByUUID', { uuid }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    fileResourcesMoveToRecycleBin({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/moveToRecycleBin', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesCollectionAdd({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/collectionAdd', params.fileId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesCollectionCancel({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/collectionCancel', params.fileId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    fileResourcesRecycleBinReduction({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/reduction/' + id, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    clearRecycleBin({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/clearRecycleBin', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    checkFileResourcesPassword({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/checkPassword/' + params.id, params.password).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 重命名
    fileResourcesRename({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('fileResources/rename/' + params.id, { originalFileName: params.originalFileName }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    listFileResourcesByUUID({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('fileResources/listByUUID', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 删除文件
    fileResourcesRemove({ commit }, params) {
      console.log(params)
      return new Promise((resolve, reject) => {
        request.post('fileResources/removeFile', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    listViewFileResourcesByUUID({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('fileView/listByUUID', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    viewFileResourcesGetByUUID({ commit }, uuid) {
      return new Promise((resolve, reject) => {
        request.get('fileView/getByUUID/' + uuid, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default FileResourcesService
