import request from '@/utils/request'
const FileCatalogServices = {
  state: {},

  mutations: {},

  actions: {
    initDocsCatalog({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('catalog/initCatalog', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    listFileSystemCatalog({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('catalog/listSystemCatalog').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getFileCatalogById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('catalog/getById/' + id, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    listFileCatalogByPid({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('catalog/listByPid', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    saveFileCatalog({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('catalog/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    removeFileCatalog({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('catalog/remove/' + params.id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    updateCatalogNameById({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('catalog/updateCatalogNameById/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default FileCatalogServices
