import request from '@/utils/request'
const GlobalTreeService = {
  state: {},

  mutations: {},

  actions: {

    /**
     * 加载单位树   已整理
     * @param {*} param0
     * @param {*} params
     */
    loadUnitTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('tree/loadUnitTree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 加载用户树
     */
    loadUserOrganizationTree({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('tree/loadUserOrganizationTree').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default GlobalTreeService
