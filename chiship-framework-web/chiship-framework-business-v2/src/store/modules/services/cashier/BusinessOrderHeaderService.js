import request from '@/utils/request'
const BusinessOrderHeaderService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 订单主表分页
     */
    businessOrderHeaderPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表列表
     */
    businessOrderHeaderList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单主表
    */
    businessOrderHeaderGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单主表详情
    */
    businessOrderHeaderGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 订单主表删除
     */
    businessOrderHeaderRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表导出
     */
    businessOrderHeaderExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderHeader/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表实时导出进度
     */
    businessOrderHeaderGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderHeader/getExportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表导出
     */
    businessOrderHeaderImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表实时导入进度
     */
    businessOrderHeaderGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/getImportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 创建充值订单
     */
    businessCreateRechargeOrder({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/createRechargeOrder', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessOrderHeaderService
