import request from '@/utils/request'
const BusinessOrderPaymentRecordService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 订单支付分页
     */
    businessOrderPaymentRecordPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderPaymentRecord/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付列表
     */
    businessOrderPaymentRecordList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderPaymentRecord/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单支付
    */
    businessOrderPaymentRecordGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderPaymentRecord/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单支付详情
    */
    businessOrderPaymentRecordGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderPaymentRecord/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 订单支付保存
     */
    businessOrderPaymentRecordSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderPaymentRecord/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付更新
     */
    businessOrderPaymentRecordUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderPaymentRecord/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付删除
     */
    businessOrderPaymentRecordRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderPaymentRecord/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付根据指定字段校验数据是否存在
     */
    businessOrderPaymentRecordValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderPaymentRecord/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付导出
     */
    businessOrderPaymentRecordExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderPaymentRecord/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付实时导出进度
     */
    businessOrderPaymentRecordGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderPaymentRecord/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付导入
     */
    businessOrderPaymentRecordImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderPaymentRecord/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单支付实时导入进度
     */
    businessOrderPaymentRecordGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderPaymentRecord/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessOrderPaymentRecordService
