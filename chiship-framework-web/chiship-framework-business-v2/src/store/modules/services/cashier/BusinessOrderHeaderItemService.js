import request from '@/utils/request'
import constants from '@/utils/constants'
const BusinessOrderHeaderItemService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 订单明细分页
     */
    businessOrderHeaderItemPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get(constants.UPMS + 'businessOrderHeaderItem/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细列表
     */
    businessOrderHeaderItemList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get(constants.UPMS + 'businessOrderHeaderItem/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单明细
    */
    businessOrderHeaderItemGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get(constants.UPMS + 'businessOrderHeaderItem/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单明细详情
    */
    businessOrderHeaderItemGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get(constants.UPMS + 'businessOrderHeaderItem/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 订单明细保存
     */
    businessOrderHeaderItemSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(constants.UPMS + 'businessOrderHeaderItem/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细更新
     */
    businessOrderHeaderItemUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(constants.UPMS + 'businessOrderHeaderItem/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细删除
     */
    businessOrderHeaderItemRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(constants.UPMS + 'businessOrderHeaderItem/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细根据指定字段校验数据是否存在
     */
    businessOrderHeaderItemValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get(constants.UPMS + 'businessOrderHeaderItem/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细导出
     */
    businessOrderHeaderItemExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile(constants.UPMS + 'businessOrderHeaderItem/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细实时导出进度
     */
    businessOrderHeaderItemGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile(constants.UPMS + 'businessOrderHeaderItem/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细导入
     */
    businessOrderHeaderItemImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(constants.UPMS + 'businessOrderHeaderItem/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单明细实时导入进度
     */
    businessOrderHeaderItemGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(constants.UPMS + 'businessOrderHeaderItem/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessOrderHeaderItemService
