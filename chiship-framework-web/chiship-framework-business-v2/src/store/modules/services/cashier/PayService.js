import request from '@/utils/request'

var PayService = {

  state: {},

  mutations: {},

  actions: {
  /**
    * 根据订单号查询订单状态
    */
    orderStatusByOrderId({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('pay/orderStatusByOrderId', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
   * 获取支付链接
   */
    generatePayQrCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('pay/generatePayQrCode', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
   * 退款
   */
    refundPay({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('pay/refund', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
   * 订单对账
   */
    orderReconciliation({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('pay/reconciliation/order', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
   * 退款对账
   */
    refundReconciliation({ commit }, refundNo) {
      return new Promise((resolve, reject) => {
        request.get('pay/reconciliation/refund', { refundNo: refundNo }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default PayService
