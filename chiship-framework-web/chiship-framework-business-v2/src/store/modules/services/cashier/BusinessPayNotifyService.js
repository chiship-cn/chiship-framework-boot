import request from '@/utils/request'
const BusinessPayNotifyService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 支付通知分页
     */
    businessPayNotifyPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessPayNotify/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知列表
     */
    businessPayNotifyList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessPayNotify/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得支付通知
    */
    businessPayNotifyGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessPayNotify/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得支付通知详情
    */
    businessPayNotifyGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessPayNotify/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 支付通知保存
     */
    businessPayNotifySave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessPayNotify/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知更新
     */
    businessPayNotifyUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessPayNotify/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知删除
     */
    businessPayNotifyRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessPayNotify/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知根据指定字段校验数据是否存在
     */
    businessPayNotifyValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessPayNotify/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知导出
     */
    businessPayNotifyExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessPayNotify/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知实时导出进度
     */
    businessPayNotifyGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessPayNotify/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知导入
     */
    businessPayNotifyImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessPayNotify/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 支付通知实时导入进度
     */
    businessPayNotifyGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessPayNotify/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessPayNotifyService
