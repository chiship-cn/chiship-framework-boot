import request from '@/utils/request'
const BusinessRefundOrderService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 退款订单分页
     */
    businessRefundOrderPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessRefundOrder/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单列表
     */
    businessRefundOrderList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessRefundOrder/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得退款订单
    */
    businessRefundOrderGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessRefundOrder/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得退款订单详情
    */
    businessRefundOrderGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessRefundOrder/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 退款订单保存
     */
    businessRefundOrderSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessRefundOrder/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单更新
     */
    businessRefundOrderUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessRefundOrder/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单删除
     */
    businessRefundOrderRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessRefundOrder/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单根据指定字段校验数据是否存在
     */
    businessRefundOrderValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessRefundOrder/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单导出
     */
    businessRefundOrderExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessRefundOrder/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单实时导出进度
     */
    businessRefundOrderGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessRefundOrder/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单导入
     */
    businessRefundOrderImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessRefundOrder/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款订单实时导入进度
     */
    businessRefundOrderGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessRefundOrder/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessRefundOrderService
