import request from '@/utils/request'
const ContentArticleService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 文档详细表分页
     */
    contentArticlePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表分页
     */
    contentArticlePageV2({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/pageV2', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表列表
     */
    contentArticleList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得文档详细表
    */
    contentArticleGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得文档详细表详情
    */
    contentArticleGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 文档详细表保存
     */
    contentArticleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表更新
     */
    contentArticleUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表删除
     */
    contentArticleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表根据指定字段校验数据是否存在
     */
    contentArticleValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表导出
     */
    contentArticleExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentArticle/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表实时导出进度
     */
    contentArticleGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentArticle/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表导入
     */
    contentArticleImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文档详细表实时导入进度
     */
    contentArticleGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 发布
     */
    contentArticleRelease({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/release', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 撤销发布
     */
    contentArticleRevokePublication({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/revokePublication', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 回收站恢复
     */
    contentArticleRecovery({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/recovery', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 移入回收站
     */
    contentArticleMoveRecycleBin({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/moveRecycleBin', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 时间维度统计文章数量
     */
    contentArticleAnalysisByTime({ commit }, dateTime) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/analysisByTime', { dateTime: dateTime }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    contentArticleFindPreAndNext({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentArticle/findPreAndNext', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    contentArticleSetAllowComment({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/setAllowComment/' + params.id, params.allow).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    contentArticleSyncWxPub({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/syncWxPub', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    contentArticleRevokeWxPub({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('contentArticle/revokeWxPub', id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ContentArticleService
