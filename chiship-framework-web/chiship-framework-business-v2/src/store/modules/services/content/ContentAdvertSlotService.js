import request from '@/utils/request'
const ContentAdvertSlotService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 广告版位表分页
     */
    contentAdvertSlotPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvertSlot/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表列表
     */
    contentAdvertSlotList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvertSlot/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得广告版位表
    */
    contentAdvertSlotGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvertSlot/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得广告版位表详情
    */
    contentAdvertSlotGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvertSlot/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 广告版位表保存
     */
    contentAdvertSlotSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvertSlot/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表更新
     */
    contentAdvertSlotUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvertSlot/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表删除
     */
    contentAdvertSlotRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvertSlot/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表根据指定字段校验数据是否存在
     */
    contentAdvertSlotValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvertSlot/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表导出
     */
    contentAdvertSlotExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentAdvertSlot/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表实时导出进度
     */
    contentAdvertSlotGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentAdvertSlot/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表导入
     */
    contentAdvertSlotImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvertSlot/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位表实时导入进度
     */
    contentAdvertSlotGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvertSlot/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ContentAdvertSlotService
