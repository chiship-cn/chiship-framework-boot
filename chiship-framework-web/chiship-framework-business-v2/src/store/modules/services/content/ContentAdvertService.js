import request from '@/utils/request'
const ContentAdvertService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 广告版位分页
     */
    contentAdvertPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位列表
     */
    contentAdvertList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得广告版位
    */
    contentAdvertGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得广告版位详情
    */
    contentAdvertGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 广告版位保存
     */
    contentAdvertSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvert/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位更新
     */
    contentAdvertUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvert/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位删除
     */
    contentAdvertRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvert/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位根据指定字段校验数据是否存在
     */
    contentAdvertValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位导出
     */
    contentAdvertExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentAdvert/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位实时导出进度
     */
    contentAdvertGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('contentAdvert/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位导入
     */
    contentAdvertImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvert/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 广告版位实时导入进度
     */
    contentAdvertGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('contentAdvert/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ContentAdvertService
