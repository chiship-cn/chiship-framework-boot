import request from '@/utils/request'
const UpmsUserService = {
  state: {},

  mutations: {},

  actions: {

    // 用户分页
    userPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户分页(用户、机构、角色)
    userPageV2({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/pageV2', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户角色树
    getUserAndRoleTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/role/tree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户岗位树
    getUserAndPostTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/post/tree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户部门树
    getUserAndOrgTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/org/tree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取用户基本信息列表
    listUserBaseInfoByIds({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.get('user/getUserBaseInfoByIds', { ids: ids }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 用户基本信息列表
    userBaseInfoList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/list/baseInfo', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户列表
    userList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 根据主键获取详情
    getUserDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('user/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 根据手机号获取用户信息
    getUserByMobile({ commit }, mobile) {
      return new Promise((resolve, reject) => {
        request.get('user/getUserByMobile', { mobile: mobile }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户保存
    userSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户更新
    userUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户删除
    userRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 初始化密码
    initialPassword({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/initialPassword', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 用户启用&禁用
    changeIsLocked({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/changeIsLocked/' + params.id, params.isLocked).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 根据指定字段校验数据是否存在
    userValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('user/validateExistByField', {
          id: params.id,
          field: params.field,
          value: params.value
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取当前登录用户详情信息
    getCurrentUserDetailedInfo({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('user/getCurrentUserDetailedInfo', {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获得用户权限
    listPermissionByUserId({ commit }, userId) {
      return new Promise((resolve, reject) => {
        request.get('user/listPermissionByUserId', {
          userId: userId
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 短信邀请
    userUserSmsInvite({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/smsInvite', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsUserService
