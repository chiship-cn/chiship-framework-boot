import request from '@/utils/request'
const UpmsOrganizationService = {
  state: {},

  mutations: {},

  actions: {

    // 加载组织
    loadOrganization({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('organization/loadByPid', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 加载组织树
    loadOrganizationTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('organization/loadTree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    loadOrganizationTreeByTreeNumber({ commit }, treeNumber) {
      return new Promise((resolve, reject) => {
        request.get('organization/loadTreeByTreeNumber', { treeNumber: treeNumber }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    loadOrganizationByIds({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.get('organization/listByIds', { ids: ids }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 根据主键获得详情
    upmsOrganizationGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('organization/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 根据主键获得详情
    upmsOrganizationGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('organization/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 单位机构保存
    upmsOrganizationUnitSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/saveUnit', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 单位机构更新
    upmsOrganizationUnitUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/updateUnit/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 生成组织机构编号
    generateOrganizationCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('organization/generateOrgCode', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 生成邀请码
    upmsOrganizationInvitationCode({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('organization/invitationCode/' + id, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 通过邀请码获取机构信息
    getOrganizationByInvitation({ commit }, code) {
      return new Promise((resolve, reject) => {
        request.post('organization/getByInvitation/' + code, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 单位部门保存
    upmsOrganizationDeptSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/saveDept', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 单位部门更新
    upmsOrganizationDeptUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/updateDept/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 单位或部门删除
    upmsOrganizationRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 设置负责人
    organizationSetChargePerson({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/setChargePerson', {
          'organizationId': params.organizationId,
          'realName': params.realName,
          'userId': params.userId
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 组织调整
    organizationAdjustment({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('organization/adjustment', {
          'organizationId': params.id,
          'targetOrganizationId': params.targetOrg
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 解散企业
    upmsOrganizationDissolution({ commit }, orgId) {
      return new Promise((resolve, reject) => {
        request.post('organization/dissolution', orgId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsOrganizationService
