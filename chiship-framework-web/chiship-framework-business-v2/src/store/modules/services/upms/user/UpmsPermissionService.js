import request from '@/utils/request'

const UpmsPermissionService = {
  state: {},

  mutations: {},

  actions: {
    // 菜单表格树
    menuTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('permission/menuTreeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 根据所属父级加载权限
    listPermission({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('permission/listPermission', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 权限表格树(所有)
    permissionTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('permission/permissionTreeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 菜单保存
    menuSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('permission/menuSave', {
          pid: params.pid,
          name: params.name,
          menuCode: params.menuCode,
          icon: params.icon,
          orders: params.orders,
          application: params.application,
          applicationGroup: params.applicationGroup,
          category: params.category
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 菜单修改
    menuUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(`permission/menuUpdate/${params.id}`, {
          pid: params.pid,
          name: params.name,
          menuCode: params.menuCode,
          icon: params.icon,
          orders: params.orders,
          application: params.application,
          applicationGroup: params.applicationGroup,
          category: params.category
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 启用/禁用菜单、权限
    changePermissionState({ commit }, params) {
      return new Promise((resolve, reject) => {
        const isDisabled = params.isDisabled
        request.post(`permission/changeIsState/${params.id}`, isDisabled).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 权限删除
    permissionRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('permission/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 快速创建权限
    fastCreatePermission({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('permission/fastCreatePermission', {
          permissionValue: params.permissionValue,
          pid: params.pid
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 权限保存
    permissionSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('permission/save', {
          permissionValue: params.permissionValue,
          name: params.name,
          pid: params.pid,
          orders: params.orders,
          category: params.category
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 权限修改
    permissionUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(`permission/update/${params.id}`, {
          permissionValue: params.permissionValue,
          name: params.name,
          pid: params.pid,
          orders: params.orders,
          category: params.category
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 根据主键获取权限
    upmsPermissionsGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get(`permission/getById/${id}`).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 下载权限编码
    downloadPermissionCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('permission/downloadPermissionCode', params.category).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsPermissionService
