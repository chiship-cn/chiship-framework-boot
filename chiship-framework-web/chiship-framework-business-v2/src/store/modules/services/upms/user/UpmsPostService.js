import request from '@/utils/request'
const UpmsPostService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 工作岗位分页
     */
    upmsPostPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位列表
     */
    upmsPostList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位树形
     */
    upmsPostTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/tree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得工作岗位
    */
    upmsPostGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得工作岗位详情
    */
    upmsPostGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 工作岗位保存
     */
    upmsPostSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位更新
     */
    upmsPostUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位删除
     */
    upmsPostRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位根据指定字段校验数据是否存在
     */
    upmsPostValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPost/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位导出
     */
    upmsPostExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsPost/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位实时导出进度
     */
    upmsPostGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsPost/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位导入
     */
    upmsPostImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 工作岗位实时导入进度
     */
    upmsPostGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 更新岗位状态
    upmsPostChangeStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPost/changeStatus/' + params.id, params.status).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsPostService
