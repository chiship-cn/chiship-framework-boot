import request from '@/utils/request'
const UpmsUserLoginHistoryService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 用户登陆历史分页
     */
    userLoginHistoryPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('userLoginHistory/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 当前登录用户登陆历史分页
     */
    currentUserLoginHistoryPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('userLoginHistory/current/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除用户登陆历史
     * @param {*} param0
     * @param {*} params
     */
    userLoginHistoryRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('userLoginHistory/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除系统日志
     */
    userLoginHistorClear({ commit }) {
      return new Promise((resolve, reject) => {
        request.post('userLoginHistory/clear').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除当前用户系统日志
     */
    currentUserLoginHistorClear({ commit }) {
      return new Promise((resolve, reject) => {
        request.post('userLoginHistory/current/clear').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsUserLoginHistoryService
