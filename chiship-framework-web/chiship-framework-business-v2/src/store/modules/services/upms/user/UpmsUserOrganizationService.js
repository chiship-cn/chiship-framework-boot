import request from '@/utils/request'
const UpmsUserOrganizationService = {
  state: {},

  mutations: {},

  actions: {
    listOrganizationByUserId({ commit }, userId) {
      return new Promise((resolve, reject) => {
        request.get('userOrganization/listByUserId', {
          userId: userId
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 添加用户
    organizationUserSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('userOrganization/save', {
          'userId': params.userId,
          'organizationId': params.organizationId,
          'job': params.job,
          'orders': params.orders,
          'phone': params.phone
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 修改用户
    organizationUserUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('userOrganization/update/' + params.id, {
          'userId': params.userId,
          'organizationId': params.organizationId,
          'job': params.job,
          'orders': params.orders,
          'phone': params.phone
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 移除用户
    organizationUserRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('userOrganization/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 校验用户是否在组织下添加
    validateOrganizationUserExist({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('userOrganization/validateExist', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsUserOrganizationService
