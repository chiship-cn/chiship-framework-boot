import request from '@/utils/request'

const roleService = {
  state: {},

  mutations: {},

  actions: {
    // 角色树
    roleTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('role/treeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 角色保存
    roleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('role/save', {
          code: params.code,
          name: params.name,
          dataScope: params.dataScope,
          pid: params.pid,
          description: params.description
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 角色更新
    roleUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('role/update/' + params.id, {
          code: params.code,
          name: params.name,
          dataScope: params.dataScope,
          pid: params.pid,
          description: params.description
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 角色删除
    roleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('role/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取指定角色下的用户
    pageUserByRoleId({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('role/pageUserByRoleId', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存分配的用户
    userRoleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(`role/userRoleSave/${params.roleId}`, params.userIdList).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 删除分配的用户
    userRoleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(`role/userRoleRemove/${params.roleId}`, params.userIdList).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取指定角色下的权限
    getPermissionByRoleId({ commit }, roleId) {
      return new Promise((resolve, reject) => {
        request.get('role/getPermissionByRoleId', { roleId: roleId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存分配的权限
    rolePermissionSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('role/rolePermissionSave/' + params.roleId, params.permissionIdList).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 删除分配的权限
    rolePermissionRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('role/rolePermissionCancel/' + params.roleId, params.permissionIdList).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default roleService
