import request from '@/utils/request'
const UpmsFeedbackService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 意见反馈分页
     */
    upmsFeedbackPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('feedback/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 当前用户意见反馈分页
     */
    currentUserFeedbackPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('feedback/current/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得意见反馈
    */
    upmsFeedbackGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('feedback/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得意见反馈详情
    */
    upmsFeedbackGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('feedback/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 意见反馈保存
     */
    upmsFeedbackSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('feedback/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 意见反馈更新
     */
    upmsFeedbackUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('feedback/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 意见反馈删除
     */
    upmsFeedbackRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('feedback/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 回复
     */
    upmsFeedbackReply({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('feedback/reply', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsFeedbackService
