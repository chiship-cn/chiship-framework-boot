import request from '@/utils/request'
const DbService = {
  state: {},

  mutations: {},

  actions: {
    DBListTables({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('db/listTables').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    DBListColumns({ commit }, tableName) {
      return new Promise((resolve, reject) => {
        request.get('db/listColumns', { tableName: tableName }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default DbService
