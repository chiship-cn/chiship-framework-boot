import request from '@/utils/request'
const UpmsQuartzJobLogService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 定时任务日志分页
     */
    upmsQuartzJobLogPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志列表
     */
    upmsQuartzJobLogList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得定时任务日志
    */
    upmsQuartzJobLogGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得定时任务日志详情
    */
    upmsQuartzJobLogGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 定时任务日志删除
     */
    upmsQuartzJobLogRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsQuartzJobLog/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 清空定时任务日志
     */
    upmsQuartzJobLogClear({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/clear', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志根据指定字段校验数据是否存在
     */
    upmsQuartzJobLogValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsQuartzJobLog/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志导出
     */
    upmsQuartzJobLogExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsQuartzJobLog/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志实时导出进度
     */
    upmsQuartzJobLogGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsQuartzJobLog/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志导入
     */
    upmsQuartzJobLogImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsQuartzJobLog/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务日志实时导入进度
     */
    upmsQuartzJobLogGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsQuartzJobLog/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsQuartzJobLogService
