import request from '@/utils/request'

var UpmsSystemExceptionLogService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 系统异常日志分页
     */
    systemExceptionLogPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('systemExceptionLog/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除系统异常日志
     */
    systemExceptionLogRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemExceptionLog/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 处理系统异常日志
     */
    systemExceptionLogDeal({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemExceptionLog/deal', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除系统异常日志
     */
    systemExceptionLogClear({ commit }) {
      return new Promise((resolve, reject) => {
        request.post('systemExceptionLog/clear').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 根据主键获得详情
     */
    systemExceptionLogGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('systemExceptionLog/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsSystemExceptionLogService
