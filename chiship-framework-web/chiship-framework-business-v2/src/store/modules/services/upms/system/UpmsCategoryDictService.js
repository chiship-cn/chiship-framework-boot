import request from '@/utils/request'
const UpmsCategoryDictService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 分类字典树形表格
     */
    upmsCategoryDictTreeByType({ commit }, type) {
      return new Promise((resolve, reject) => {
        request.get('upmsCategoryDict/treeByType', { type: type }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 缓存中加载
     */
    upmsCategoryDictCacheTreeByType({ commit }, type) {
      return new Promise((resolve, reject) => {
        request.get('upmsCategoryDict/cacheTreeByType', { type: type }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得分类字典
    */
    upmsCategoryDictGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsCategoryDict/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得分类字典详情
    */
    upmsCategoryDictGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsCategoryDict/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 分类字典保存
     */
    upmsCategoryDictSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsCategoryDict/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典更新
     */
    upmsCategoryDictUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsCategoryDict/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典删除
     */
    upmsCategoryDictRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsCategoryDict/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典根据指定字段校验数据是否存在
     */
    upmsCategoryDictValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsCategoryDict/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典导出
     */
    upmsCategoryDictExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsCategoryDict/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典实时导出进度
     */
    upmsCategoryDictGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsCategoryDict/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典导入
     */
    upmsCategoryDictImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsCategoryDict/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 分类字典实时导入进度
     */
    upmsCategoryDictGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsCategoryDict/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsCategoryDictService
