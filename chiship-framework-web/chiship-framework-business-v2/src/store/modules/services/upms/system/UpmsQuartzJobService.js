import request from '@/utils/request'
const UpmsquartzJobService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 定时任务分页
     */
    quartzJobPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('quartzJob/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务保存
     */
    quartzJobSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('quartzJob/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务更新
     */
    quartzJobUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('quartzJob/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务启用&禁用
     */
    quartzJobChangeIsState({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('quartzJob/changeIsState/' + params.id, params.status).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 定时任务删除
     */
    quartzJobRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('quartzJob/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    quartzJobListTasks({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('quartzJob/listTasks').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsquartzJobService
