import request from '@/utils/request'
const systemConfigService = {
  state: {},

  mutations: {},

  actions: {
    // 分组列表
    loadConfigGroup({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('systemConfig/group/list').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 分组保存
    systemConfigGroupSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/group/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 分组更新
    systemConfigGroupUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/group/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 根据分组ID获取参数配置项
    listConfigByGroupId({ commit }, groupId) {
      return new Promise((resolve, reject) => {
        request.get('systemConfig/listByGroupId', { groupId: groupId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 根据分组编号获取参数配置项
    listConfigByGroupCode({ commit }, groupCode) {
      return new Promise((resolve, reject) => {
        request.get('systemConfig/listByGroupCode', { groupCode: groupCode }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getConfigByGroupCode({ commit }, groupCode) {
      return new Promise((resolve, reject) => {
        request.get('systemConfig/getByGroupCode', { groupCode: groupCode }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    saveConfig({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    configUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    systemConfigRemove({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/remove', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    updateConfig({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemConfig/updateConfig', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default systemConfigService
