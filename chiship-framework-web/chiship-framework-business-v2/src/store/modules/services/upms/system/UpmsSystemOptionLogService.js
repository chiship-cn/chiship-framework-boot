import request from '@/utils/request'
const UpmsSystemOptionLogService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 系统日志分页
     * @param {*} param0
     * @param {*} params
     */
    systemOptionLogPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('systemOptionLog/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 当前登录用户系统日志分页
     */
    userSystemOptionLogPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('systemOptionLog/current/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除系统日志
     * @param {*} param0
     * @param {*} params
     */
    systemOptionLogRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('systemOptionLog/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 删除系统日志
     */
    systemOptionLogClear({ commit }) {
      return new Promise((resolve, reject) => {
        request.post('systemOptionLog/clear').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 根据主键获得详情
     */
    systemOptionLogGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('systemOptionLog/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsSystemOptionLogService
