import request from '@/utils/request'
const UpmsNoticeService = {
  state: {
    notifyUpdate: false,
    updateComponent: 'admin',
    updateMyMsgType: 'notice'
  },

  mutations: {
    NOTIFY_UPDATE: (state, flag) => {
      state.notifyUpdate = flag
    },
    UPDATE_COMPONENT: (state, name) => {
      state.updateComponent = name
    },
    UPDATE_MY_MSG_TYPE: (state, name) => {
      state.updateMyMsgType = name
    }
  },

  actions: {
    // 通知更新数据
    notifyUpdateNoticeData({ commit }, flag) {
      commit('NOTIFY_UPDATE', flag)
    },
    // 更新通知组件数据
    updateNoticeComponent({ commit }, name) {
      commit('UPDATE_COMPONENT', name)
    },
    // 更新我的消息类型
    updateMyMsgType({ commit }, name) {
      commit('UPDATE_MY_MSG_TYPE', name)
    },
    // 根据主键获取详情
    getNoticeById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('notice/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 通知公告分页
     */
    noticePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('notice/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 通知公告保存
     */
    noticeSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('notice/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 通知公告更新
     */
    noticeUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('notice/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 通知公告删除
     */
    noticeRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('notice/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 撤销发布
     */
    noticeRevoke({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('notice/revoke/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 后台用户发布通知
     */
    adminUserPublishNotice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('user/publishNotice/' + params.id, params.scope).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 会员用户发布通知
     */
    memberUserPublishNotice({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('memberUser/publishNotice/' + params.id, params.scope).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsNoticeService
