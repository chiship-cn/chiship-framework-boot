import request from '@/utils/request'
const UpmsAppVersionService = {
  state: {},

  mutations: {},

  actions: {
    appVersionPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('appVersion/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    appVersionListByCode({ commit }, appCode) {
      return new Promise((resolve, reject) => {
        request.get('appVersion/listByCode', { appCode: appCode }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    appVersionSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('appVersion/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    appVersionUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('appVersion/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    appVersionRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('appVersion/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    appVersionGetNew({ commit }, appCode) {
      return new Promise((resolve, reject) => {
        request.get('appVersion/getNew/' + appCode, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsAppVersionService
