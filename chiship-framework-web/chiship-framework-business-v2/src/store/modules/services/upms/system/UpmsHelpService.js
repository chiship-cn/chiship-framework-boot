import request from '@/utils/request'
const UpmsHelpService = {
  state: {},

  mutations: {},

  actions: {
    // 帮助文档分页
    helpArticlePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('helpContent/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 帮助文档分页
    contentUnionAllCategoryByPid({ commit }, pid) {
      return new Promise((resolve, reject) => {
        request.get('helpContent/contentUnionAllCategoryByPid', { pid: pid }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 保存文档
    saveHelpArticle({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('helpContent/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获得详情
    getHelpContentDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get(`helpContent/getDetailsById/${id}`, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 更新文档
    updateHelpArticle({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post(`helpContent/update/${params.id}`, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 删除文档
    removehelpArticle({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('helpContent/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsHelpService
