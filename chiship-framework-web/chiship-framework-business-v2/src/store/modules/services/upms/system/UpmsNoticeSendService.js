import request from '@/utils/request'
const UpmsNoticeSendService = {
  state: {},

  mutations: {},

  actions: {
    // 根据主键获取详情
    getNoticeSendById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsNoticeSend/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 当前用户通告阅读标记表分页
     */
    currentUserNoticeSendPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsNoticeSend/currentUserPage', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 当前用户通告阅读全部已读
     */
    currentUserNoticeSendSetRead({ commit }, noticType) {
      return new Promise((resolve, reject) => {
        request.post('upmsNoticeSend/currentUserSetRead', noticType).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 当前用户未读消息列表
     */
    currentUserUnReadNotice({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('upmsNoticeSend/currentUserUnRead').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 设为已读
    noticeSetAsRead({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('upmsNoticeSend/setAsRead', id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsNoticeSendService
