import request from '@/utils/request'
const UpmsSystemOnlinegService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 在线用户
     * @param {*} param0
     * @param {*} params
     */
    userOnlinePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('userOnline/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 强退
     * @param {*} param0
     * @param {*} userName
     * @returns
     */
    userForceLogout({ commit }, userName) {
      console.log(2323434, userName)
      return new Promise((resolve, reject) => {
        request.post('userOnline/forceLogout', userName).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsSystemOnlinegService
