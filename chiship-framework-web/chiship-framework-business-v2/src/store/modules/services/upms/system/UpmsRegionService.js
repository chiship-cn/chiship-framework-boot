import request from '@/utils/request'

const UpmsRegionService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 地区分页
     */
    regionPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('region/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 根据ID获取下级数据
     */
    regionLoadByPid({ commit }, pid) {
      return new Promise((resolve, reject) => {
        request.get('region/loadByPid', { pid: pid }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 地区联动
    getCacheRegion({ commit }, pid) {
      return new Promise((resolve, reject) => {
        request.get('region/cacheListByPid', { pid: pid, loading: false }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 同步地区
     */
    syncCacheRegion({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('region/cacheRegion').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导出
    exportRegionData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('region/exportData', params).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getRegionExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('region/getExportProcessStatus', params.taskId).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    loadRegionGeoByPidAndName({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('region/loadGeoByPidAndName', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsRegionService
