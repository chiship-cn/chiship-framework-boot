import request from '@/utils/request'
import { Message, Notification } from 'element-ui'

const UpmsSmsCodeService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 验证码信息分页
     */
    smsCodePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('smsCode/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 验证码信息分页
    */
    smsCodeRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('smsCode/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获得手机验证码
    mobileVerificationCode({ commit }, mobile) {
      return new Promise((resolve, reject) => {
        request.get('smsCode/mobile', {
          mobile: mobile
        }).then(response => {
          if (response.code) {
            Notification({
              type: 'success',
              dangerouslyUseHTMLString: true,
              duration: 5 * 1000,
              message: `测试验证码：${response.code}`
            })
          }
          if (response.msg) {
            Message({
              message: response.msg,
              type: 'success',
              duration: 5 * 1000
            })
          }

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获得邮箱验证码
    emailVerificationCode({ commit }, email) {
      return new Promise((resolve, reject) => {
        request.get('smsCode/email', {
          email: email
        }).then(response => {
          if (response.code) {
            Notification({
              type: 'success',
              dangerouslyUseHTMLString: true,
              duration: 5 * 1000,
              message: `测试验证码：${response.code}`
            })
          }
          if (response.msg) {
            Message({
              message: response.msg,
              type: 'success',
              duration: 5 * 1000
            })
          }

          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 核查验证码
    checkVerificationCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('smsCode/check', {
          device: params.device,
          code: params.code
        }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}
export default UpmsSmsCodeService
