import request from '@/utils/request'
const UpmsSmsLogService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 短信发送日志分页
     */
    upmsSmsLogPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsSmsLog/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志列表
     */
    upmsSmsLogList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsSmsLog/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得短信发送日志
    */
    upmsSmsLogGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsSmsLog/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得短信发送日志详情
    */
    upmsSmsLogGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsSmsLog/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 短信发送日志删除
     */
    upmsSmsLogRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsSmsLog/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志清空
     */
    upmsSmsLogClear({ commit }) {
      return new Promise((resolve, reject) => {
        request.post('upmsSmsLog/clear').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志根据指定字段校验数据是否存在
     */
    upmsSmsLogValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsSmsLog/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志导出
     */
    upmsSmsLogExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsSmsLog/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志实时导出进度
     */
    upmsSmsLogGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsSmsLog/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志导入
     */
    upmsSmsLogImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsSmsLog/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 短信发送日志实时导入进度
     */
    upmsSmsLogGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsSmsLog/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsSmsLogService
