import request from '@/utils/request'
const TestExampleService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 示例（正式环境可删除）分页
     */
    testExamplePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('testExample/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）列表
     */
    testExampleList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('testExample/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得示例（正式环境可删除）
    */
    testExampleGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('testExample/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得示例（正式环境可删除）详情
    */
    testExampleGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('testExample/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 示例（正式环境可删除）保存
     */
    testExampleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('testExample/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）更新
     */
    testExampleUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('testExample/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）删除
     */
    testExampleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('testExample/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）导出
     */
    testExampleExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('testExample/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）实时导出进度
     */
    testExampleGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('testExample/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）导出
     */
    testExampleImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('testExample/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 示例（正式环境可删除）实时导入进度
     */
    testExampleGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('testExample/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    testExampleGeneratePdf({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.downloadFile('testExample/generatePdf/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    testExampleGenerateWord({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.downloadFile('testExample/generateWord/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    testExamplePermissions1({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('testExample/permissions1').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    testExamplePermissions2({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('testExample/permissions2').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default TestExampleService
