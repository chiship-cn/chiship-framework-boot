import request from '@/utils/request'
const BusinessFormService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 自定义表单分页
     */
    businessFormPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单分页
     */
    businessFormTree({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/tree').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    businessFormTreeAll({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/treeAll').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单列表
     */
    businessFormList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自定义表单
    */
    businessFormGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自定义表单详情
    */
    businessFormGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 自定义表单保存
     */
    businessFormSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单更新
     */
    businessFormUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单删除
     */
    businessFormRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单根据指定字段校验数据是否存在
     */
    businessFormValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessForm/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单导出
     */
    businessFormExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessForm/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单实时导出进度
     */
    businessFormGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessForm/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单导入
     */
    businessFormImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自定义表单实时导入进度
     */
    businessFormGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    businessFormSaveContent({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/saveConfig', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    businessFormPublish({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/publish', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    businessFormRevoke({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessForm/revoke', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessFormService
