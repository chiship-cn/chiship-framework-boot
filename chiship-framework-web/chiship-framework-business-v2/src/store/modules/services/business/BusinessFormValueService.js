import request from '@/utils/request'
const BusinessFormValueService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 自封有表单提交数据分页
     */
    businessFormValuePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据分页
     */
    businessFormValuePageV1({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/pageV1', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据分页（当前登录用户专用）
     */
    businessFormValuePageV2({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/pageV2', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据列表
     */
    businessFormValueList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自封有表单提交数据
    */
    businessFormValueGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自封有表单提交数据详情
    */
    businessFormValueGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 游客提交数据
    businessFormValueTouristSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/touristSave', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据保存
     */
    businessFormValueSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据更新
     */
    businessFormValueUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据删除
     */
    businessFormValueRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据根据指定字段校验数据是否存在
     */
    businessFormValueValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessFormValue/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据导出
     */
    businessFormValueExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessFormValue/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据实时导出进度
     */
    businessFormValueGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessFormValue/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据导入
     */
    businessFormValueImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自封有表单提交数据实时导入进度
     */
    businessFormValueGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessFormValue/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default BusinessFormValueService
