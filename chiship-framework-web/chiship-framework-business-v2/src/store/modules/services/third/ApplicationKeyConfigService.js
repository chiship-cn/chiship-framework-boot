import request from '@/utils/request'
const ApplicationKeyConfigService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 三方应用集成配置分页
     */
    applicationKeyConfigPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    applicationKeyConfigList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
    * 根据主键获得三方应用集成配置
    */
    applicationKeyConfigGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得三方应用集成配置详情
    */
    applicationKeyConfigGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据APPID获得三方应用集成配置详情
    */
    applicationKeyConfigGetByAppId({ commit }, appId) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/getByAppId', { appId: appId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 三方应用集成配置保存
     */
    applicationKeyConfigSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('applicationKeyConfig/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 三方应用集成配置更新
     */
    applicationKeyConfigUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('applicationKeyConfig/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 三方应用集成配置删除
     */
    applicationKeyConfigRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('applicationKeyConfig/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    initApplicationKeyConfig({ commit }, type) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/init/' + type).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    saveOrUpdateApplicationKeyConfig({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('applicationKeyConfig/saveOrUpdate', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getThirdPayConfig({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('applicationKeyConfig/getPayConfig').then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ApplicationKeyConfigService
