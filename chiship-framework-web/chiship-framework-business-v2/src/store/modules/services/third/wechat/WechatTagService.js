import request from '@/utils/request'
const ThirdWechatTagService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 微信用户标签分页
     */
    thirdWechatTagPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatTag/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户标签列表
     */
    thirdWechatTagList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatTag/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信用户标签
    */
    thirdWechatTagGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatTag/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信用户标签详情
    */
    thirdWechatTagGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatTag/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 微信用户标签保存
     */
    thirdWechatTagSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatTag/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户标签更新
     */
    thirdWechatTagUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatTag/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户标签删除
     */
    thirdWechatTagRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatTag/remove?appId=' + params.appId, params.ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户标签同步创建
     */
    thirdWechatTagSyncSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatTag/syncSave/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户标签同步
     */
    thirdWechatTagSyncTags({ commit }, appId) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatTag/syncTags', { appId: appId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default ThirdWechatTagService
