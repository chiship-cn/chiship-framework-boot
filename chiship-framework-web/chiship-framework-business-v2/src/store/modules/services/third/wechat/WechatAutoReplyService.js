import request from '@/utils/request'
const ThirdWechatAutoReplyService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 自动回复分页
     */
    thirdWechatAutoReplyPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatAutoReply/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复列表
     */
    thirdWechatAutoReplyList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatAutoReply/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自动回复
    */
    thirdWechatAutoReplyGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatAutoReply/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得自动回复详情
    */
    thirdWechatAutoReplyGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatAutoReply/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 自动回复保存
     */
    thirdWechatAutoReplySave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatAutoReply/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复更新
     */
    thirdWechatAutoReplyUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatAutoReply/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复删除
     */
    thirdWechatAutoReplyRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatAutoReply/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复根据指定字段校验数据是否存在
     */
    thirdWechatAutoReplyValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatAutoReply/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复导出
     */
    thirdWechatAutoReplyExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('thirdWechatAutoReply/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复实时导出进度
     */
    thirdWechatAutoReplyGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('thirdWechatAutoReply/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复导入
     */
    thirdWechatAutoReplyImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatAutoReply/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 自动回复实时导入进度
     */
    thirdWechatAutoReplyGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatAutoReply/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ThirdWechatAutoReplyService
