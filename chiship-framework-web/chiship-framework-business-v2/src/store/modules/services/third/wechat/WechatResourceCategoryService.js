import request from '@/utils/request'
const ThirdWechatResourceCategoryService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 微信资源分类分页
     */
    thirdWechatResourceCategoryTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatResourceCategory/loadTree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信资源分类
    */
    thirdWechatResourceCategoryGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatResourceCategory/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信资源分类详情
    */
    thirdWechatResourceCategoryGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatResourceCategory/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 微信资源分类保存
     */
    thirdWechatResourceCategorySave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatResourceCategory/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信资源分类更新
     */
    thirdWechatResourceCategoryUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatResourceCategory/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信资源分类删除
     */
    thirdWechatResourceCategoryRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatResourceCategory/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ThirdWechatResourceCategoryService
