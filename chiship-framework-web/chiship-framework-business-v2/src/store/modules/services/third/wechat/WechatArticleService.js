import request from '@/utils/request'
const ThirdWechatArticleService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 文章素材分页
     */
    thirdWechatArticlePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatArticle/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得文章素材
    */
    thirdWechatArticleGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatArticle/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得文章素材详情
    */
    thirdWechatArticleGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatArticle/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 文章素材保存
     */
    thirdWechatArticleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 基本素材保存
     */
    thirdWechatArticleSaveBasic({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/saveBasic', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 文章素材更新
     */
    thirdWechatArticleUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 文章素材删除
     */
    thirdWechatArticleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 发布
     */
    thirdWechatArticleRelease({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/release', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 撤销发布
     */
    thirdWechatArticleRevokePublication({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/revokePublication', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 回收站恢复
     */
    thirdWechatArticleRecovery({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/recovery', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 移入回收站
     */
    thirdWechatArticleMoveRecycleBin({ commit }, ids) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/moveRecycleBin', ids).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    thirdWechatArticleFindPreAndNext({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatArticle/findPreAndNext', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    thirdWechatArticleSetAllowComment({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatArticle/setAllowComment/' + params.id, params.allow).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default ThirdWechatArticleService
