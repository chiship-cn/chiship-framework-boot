import request from '@/utils/request'
const WechatUserService = {
  state: {},

  mutations: {},

  actions: {
    wechatUserPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatUser/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户更新
     */
    wechatUserUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatUser/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信用户删除
     */
    wechatUserRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatUser/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    synchronizationWechatUser({ commit }, appId) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatUser/synchronizationUser', { appId: appId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default WechatUserService
