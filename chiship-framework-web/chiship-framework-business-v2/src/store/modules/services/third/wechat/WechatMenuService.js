import request from '@/utils/request'
const WechatMenuService = {
  state: {},

  mutations: {},

  actions: {
    wechatMenuTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatMenu/treeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信菜单
    */
    wechatMenuGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatMenu/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得微信菜单详情
    */
    wechatMenuGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatMenu/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 微信菜单保存
     */
    wechatMenuSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatMenu/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信菜单更新
     */
    wechatMenuUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatMenu/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信菜单删除
     */
    wechatMenuRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdWechatMenu/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    wechatMenuGenerate({ commit }, appId) {
      return new Promise((resolve, reject) => {
        request.get('thirdWechatMenu/generate', { appId: appId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default WechatMenuService
