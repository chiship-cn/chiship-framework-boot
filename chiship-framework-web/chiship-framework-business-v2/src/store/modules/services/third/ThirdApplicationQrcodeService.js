import request from '@/utils/request'
const ThirdApplicationQrcodeService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 二维码 分页
     */
    thirdApplicationQrcodePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdApplicationQrcode/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得二维码
    */
    thirdApplicationQrcodeGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdApplicationQrcode/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得二维码 详情
    */
    thirdApplicationQrcodeGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('thirdApplicationQrcode/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 二维码 保存
     */
    thirdApplicationQrcodeSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdApplicationQrcode/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 二维码 更新
     */
    thirdApplicationQrcodeUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdApplicationQrcode/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 二维码 删除
     */
    thirdApplicationQrcodeRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('thirdApplicationQrcode/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 微信二维码生成
    generateApplicationQrCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('thirdApplicationQrcode/generateQrCode/' + params.id, { appId: params.appId }).then(response => {
          resolve('data:image/png;base64,' + response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ThirdApplicationQrcodeService
