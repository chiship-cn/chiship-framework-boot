import request from '@/utils/request'
import tools from '@/utils/common/tools'
const GlobalUtilServices = {
  state: {},

  mutations: {},

  actions: {
    // 文件下载
    downLoadFile({ commit }, uuid) {
      return new Promise((resolve, reject) => {
        request.downloadFile('fileView/download/' + uuid, { download: true }).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导出
    exportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('export/data/' + params.className, params).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    getExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('export/getProcessStatus/' + params.className, params.taskId).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导入
    importData({ commit }, formData) {
      return new Promise((resolve, reject) => {
        request.post('import/data/' + formData.get('className'), formData).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导入数据实时进度
    getImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('import/getProcessStatus/' + params.className, params.taskId).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 图片验证码
    getCaptchaCode({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.pictureStream('captcha/getCaptchaCode', params).then(response => {
          const data = response
          resolve('data:image/png;base64,' + btoa(new Uint8Array(data).reduce((data, byte) => data + String.fromCharCode(byte), '')))
        }).catch(error => {
          reject(error)
        })
      })
    },

    getSystemConfig({ commit }, keys) {
      return new Promise((resolve, reject) => {
        request.get('util/systemConfig', { keys: keys, loading: false }).then(response => {
          var config = {}
          for (var item of response) {
            config[item.paramCode] = item.paramValue.replace('${TIME}', tools.formatDate(new Date().getTime(), 'yyyy'))
          }
          resolve(config)
        }).catch(error => {
          reject(error)
        })
      })
    },

    idCardOcr({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('util/idCardOcr', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    ocrTool({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('util/ocr', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default GlobalUtilServices
