import request from '@/utils/request'
const ProductService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 商品分页
     */
    productPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('product/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分页
     */
    productPageV2({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('product/pageV2', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品列表
     */
    productList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('product/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得商品
    */
    productGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('product/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得商品详情
    */
    productGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('product/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 商品保存
     */
    productSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('product/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品更新
     */
    productUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('product/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品删除
     */
    productRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('product/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品根据指定字段校验数据是否存在
     */
    productValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('product/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品导出
     */
    productExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('product/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品实时导出进度
     */
    productGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('product/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品导入
     */
    productImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('product/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品实时导入进度
     */
    productGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('product/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品上架
     */
    productPutShelves({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('product/putShelves/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品下架
     */
    productRemoveShelves({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.post('product/removeShelves/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default ProductService
