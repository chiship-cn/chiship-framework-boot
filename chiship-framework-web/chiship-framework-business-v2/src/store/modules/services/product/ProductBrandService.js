import request from '@/utils/request'
const ProductBrandService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 品牌分页
     */
    productBrandPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productBrand/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌列表
     */
    productBrandList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productBrand/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得品牌
    */
    productBrandGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('productBrand/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得品牌详情
    */
    productBrandGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('productBrand/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 品牌保存
     */
    productBrandSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productBrand/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌更新
     */
    productBrandUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productBrand/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌删除
     */
    productBrandRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productBrand/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌根据指定字段校验数据是否存在
     */
    productBrandValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productBrand/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌导出
     */
    productBrandExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('productBrand/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌实时导出进度
     */
    productBrandGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('productBrand/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌导入
     */
    productBrandImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productBrand/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 品牌实时导入进度
     */
    productBrandGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productBrand/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ProductBrandService
