import request from '@/utils/request'
const ProductCategoryService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 商品分类分页
     */
    productCategoryTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/treeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类列表
     */
    productCategoryList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得商品分类
    */
    productCategoryGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得商品分类详情
    */
    productCategoryGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 商品分类保存
     */
    productCategorySave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类更新
     */
    productCategoryUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类删除
     */
    productCategoryRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类根据指定字段校验数据是否存在
     */
    productCategoryValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类导出
     */
    productCategoryExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('productCategory/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类实时导出进度
     */
    productCategoryGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('productCategory/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类导入
     */
    productCategoryImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类实时导入进度
     */
    productCategoryGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 商品分类绑定品牌
     */
    productCategoryBindBrand({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('productCategory/bindBrand/' + params.id, params.brandIds).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获取绑定的品牌
    productCategoryListBindBrand({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('productCategory/listBindBrand', { id: id }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default ProductCategoryService
