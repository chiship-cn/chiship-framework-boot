import Layout from '@/layout'
import cockpitRouter from './modules/Cockpit'
import fileViewRouter from './modules/FileView'
import exampleRoutet from './modules/Example'
import productRouter from './modules/Product'
import cashierRouter from './modules/Cashier'
import businessRouter from './modules/Business'
import contentRouter from './modules/Content'
import memberRouter from './modules/Member'
import docsRouter from './modules/Docs'
import thirdRouter from './modules/third'
import systemRouter from './modules/System'
import userRouter from './modules/User'
import monitoringRourer from './modules/Monitoring'
import logsRouter from './modules/Logs'
/**
 * 动态路由
 */
export const asyncRouters = [
  cockpitRouter,
  fileViewRouter,
  businessRouter,
  contentRouter,
  memberRouter,
  productRouter,
  cashierRouter,
  thirdRouter,
  docsRouter,
  systemRouter,
  userRouter,
  monitoringRourer,
  logsRouter
]
/**
 * 基础路由
 */
export const constantRouters = [
  { path: '*', redirect: '/404', name: '404*', hidden: true },
  {
    path: '/404',
    name: '404',
    component: () => import('@/views/error/404'),
    hidden: true
  }, {
    path: '/500',
    name: '500',
    component: () => import('@/views/error/500'),
    hidden: true
  }, {
    path: '/noPermission',
    name: 'noPermission',
    component: () => import('@/views/error/noPermission'),
    hidden: true
  }, {
    path: '/sso',
    name: 'sso',
    component: () => import('@/views/sso/Layout'),
    redirect: '/login',
    hidden: true,
    children: [{
      path: '/login',
      name: 'login',
      meta: { title: '登录' },
      component: () => import('@/views/sso/login')
    }, {
      path: '/invite/:code',
      name: 'invite',
      component: () => import('@/views/common/profile/invite'),
      hidden: true
    }, {
      path: '/password',
      name: 'password',
      meta: { title: '找回密码' },
      component: () => import('@/views/sso/password')
    }, {
      path: '/register',
      name: 'register',
      meta: { title: '注册' },
      component: () => import('@/views/sso/register')
    }, {
      path: '/company',
      name: 'company',
      meta: { title: '创建企业' },
      component: () => import('@/views/sso/company')
    }]
  }, {
    path: '/legaAgreement/:code',
    name: 'legaAgreement',
    meta: { title: '隐私服务协议内容', auth: false },
    hidden: true,
    component: () => import('@/views/sso/legaAgreement')
  }, {
    path: '/app/h5/download/:code',
    name: 'appH5Download',
    component: () => import('@/views/upms/systemSetting/appVersion/h5Download'),
    meta: { auth: false },
    hidden: true
  }, {
    path: '/notice/h5/preview/:id',
    name: 'noticeH5Preview',
    component: () => import('@/views/upms/systemSetting/notice/h5Preview'),
    meta: { auth: false },
    hidden: true
  }, {
    path: '/article/h5/preview/:id',
    name: 'articleH5Preview',
    component: () => import('@/views/content/article/h5Preview'),
    meta: { auth: false },
    hidden: true
  }, {
    path: '/wxPubArticle/h5/preview/:id',
    name: 'wxPubArticleH5Preview',
    component: () => import('@/views/third/wechat/imageText/h5Preview'),
    meta: { auth: false },
    hidden: true
  }, {
    path: '/designForm/h5/submit/:id',
    name: 'designFormH5Submit',
    component: () => import('@/views/business/designForm/h5Submit'),
    meta: { auth: false },
    hidden: true
  }, {
    path: '/lockScreen',
    name: 'lockScreen',
    component: () => import('@/views/lock'),
    hidden: true
  }, {
    path: '/helpDocs',
    name: 'helpDocs',
    component: () => import('@/views/upms/systemSetting/help/helpDocsLayout'),
    redirect: '/helpDocs/index',
    meta: { auth: false },
    hidden: true,
    children: [{
      path: 'index',
      name: 'helpDocsIndex',
      component: () => import('@/views/upms/systemSetting/help/helpDocs'),
      meta: { auth: false }
    }]
  }, {
    path: '/common',
    name: 'common',
    component: Layout,
    redirect: '/common/profile',
    hidden: true,
    children: [{
      path: 'profile',
      name: 'profile',
      component: () => import('@/views/common/profile/index'),
      meta: { title: '个人资料', icon: 'dashboard' }
    }]
  }, {
    path: '/addressBook',
    component: Layout,
    redirect: '/addressBook',
    name: 'addressBook',
    hidden: true,
    meta: { title: '通讯录', icon: 'menuHome' },
    children: [{
      path: '',
      name: 'addressBookIndex',
      component: () => import('@/views/upms/user/user/addressBook'),
      meta: { title: '通讯录', icon: 'menuHome' }
    }]
  }, {
    path: '/message',
    component: Layout,
    redirect: '/message',
    name: 'message',
    hidden: true,
    meta: { title: '我的消息', icon: 'menuMessage' },
    children: [{
      path: '',
      name: 'messageIndex',
      meta: { title: '我的消息', icon: 'menuMessage' },
      component: () => import('@/views/common/msg/index')
    }, {
      path: 'sendDetails',
      name: 'messageSendDetails',
      component: () => import('@/views/common/msg/sendDetails'),
      meta: { title: '消息详情', icon: 'menuNotice', activeMenu: 'messageIndex' },
      hidden: true
    }]
  }, {
    path: '/',
    component: Layout,
    redirect: '/workbench',
    name: 'homepage',
    meta: { title: '主页', icon: 'iconHome' },
    children: [{
      path: 'workbench',
      name: 'homepageWorkbench',
      component: () => import('@/views/homepage/workbench'),
      meta: { title: '工作台', icon: 'iconWorkbench' }
    }, {
      path: 'dashboard',
      name: 'homepageDashboard',
      component: () => import('@/views/homepage/dashboard'),
      meta: { title: '概览', icon: 'iconDashboard', valid: false }
    }, {
      path: 'cockpitV2',
      name: 'homepageCockpit',
      redirect: '/cockpit/index',
      meta: { title: '驾驶舱', icon: 'iconCockpit' }
    }]
  },
  exampleRoutet
]
export const allRouters = [
  ...constantRouters,
  ...asyncRouters
]
