/**
 * 用户
 */

import Layout from '@/layout'

const userRouter = {
  path: '/user',
  component: Layout,
  redirect: '/user',
  name: 'users',
  meta: { title: '用户', icon: 'menuUser' },
  children: [{
    path: 'index',
    name: 'userManager',
    component: () => import('@/views/upms/user/user/user'),
    meta: { title: '用户管理', icon: 'menuUser' }
  }, {
    path: 'all',
    name: 'userAllManager',
    hidden: true,
    component: () => import('@/views/upms/user/user/userAll'),
    meta: { title: '在库用户', icon: 'menuUser', activeMenu: 'userManager' }
  }, {
    path: 'unit',
    name: 'userUnit',
    component: () => import('@/views/upms/user/unit/index'),
    meta: { title: '机构部门', icon: 'menuUnit' }
  }, {
    path: 'post',
    name: 'userPost',
    component: () => import('@/views/upms/user/post'),
    meta: { title: '工作岗位', icon: 'menuUnit' }
  }, {
    path: 'role',
    name: 'roleManager',
    component: () => import('@/views/upms/user/role/index'),
    meta: { title: '角色管理', icon: 'menuRole' }
  }, {
    path: 'menu',
    name: 'menuManager',
    component: () => import('@/views/upms/user/menu'),
    meta: { title: '菜单管理', icon: 'menuPermission' }
  }]
}

export default userRouter
