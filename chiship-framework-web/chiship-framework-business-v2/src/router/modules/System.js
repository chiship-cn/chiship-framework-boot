/**
 * 系统路由
 */

import Layout from '@/layout'

const systemRouter = {
  path: '/system',
  component: Layout,
  redirect: '/system/systemConfig',
  name: 'system',
  meta: { title: '系统', icon: 'menuSystemSetting' },
  children: [{
    path: 'systemConfig',
    name: 'systemConfig',
    component: () => import('@/views/upms/systemSetting/systemConfig/index'),
    meta: { title: '参数配置', icon: 'menuSystemConfig' }
  }, {
    path: 'dataDict',
    name: 'dataDict',
    component: () => import('@/views/upms/systemSetting/dictionary/upmsDataDict'),
    meta: { title: '字典管理', icon: 'meunDataDict' }
  }, {
    path: 'categoryDict',
    name: 'categoryDict',
    component: () => import('@/views/upms/systemSetting/categoryDict/index'),
    meta: { title: '分类字典', icon: 'meunDataDict' }
  }, {
    path: 'quartzJob',
    name: 'systemQuartzJob',
    component: () => import('@/views/upms/systemSetting/quartzJob/index'),
    meta: { title: '定时任务', icon: 'menuQuartzJob' }
  }, {
    path: 'smsConfig',
    name: 'systemSmsConfig',
    component: () => import('@/views/upms/systemSetting/sms/index'),
    meta: { title: '短信配置', icon: 'menuHelp', code: 'smsConfig' }
  }, {
    path: 'formDesign',
    name: 'systemFormDesign',
    component: () => import('@/views/business/designForm/index'),
    meta: { title: '表单设计', icon: 'menuQuartzJob' }
  }, {
    path: 'region',
    name: 'region',
    component: () => import('@/views/upms/systemSetting/region'),
    meta: { title: '行政区划', icon: 'menuRegion' }
  }, {
    path: 'app',
    name: 'appVersion',
    component: () => import('@/views/upms/systemSetting/appVersion/index'),
    meta: { title: 'APP版本', icon: 'menuApp' }
  }, {
    path: 'db',
    name: 'systemDB',
    component: () => import('@/views/upms/systemSetting/db/index'),
    meta: { title: '数据库', icon: 'menuQuartzJob' }
  }]
}

export default systemRouter
