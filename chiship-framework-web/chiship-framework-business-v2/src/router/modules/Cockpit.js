import EmptyLayout from '@/layout/EmptyLayout'
const cockpitRouter = {
  path: '/cockpit',
  component: EmptyLayout,
  redirect: '/cockpit/index',
  name: 'cockpit',
  hidden: true,
  meta: { title: '驾驶舱' },
  children: [{
    path: 'index',
    name: 'cockpitIndex',
    component: () => import('@/views/cockpit/index'),
    meta: { title: '驾驶舱' }
  }]
}
export default cockpitRouter
