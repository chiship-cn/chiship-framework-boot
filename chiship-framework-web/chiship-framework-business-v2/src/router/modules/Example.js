/**
 * 示例路由
 */
import Layout from '@/layout'
var exampleRoutet = {
  path: '/example',
  name: 'example',
  component: Layout,
  redirect: '/example/map',
  meta: { title: '示例', icon: 'iconExample' },
  children: [{
    path: 'map',
    name: 'exampleMap',
    meta: { title: '地图下钻', icon: 'menuHome' },
    component: () => import('@/views/example/map')
  }, {
    path: 'localCamera',
    name: 'localCamera',
    meta: { title: '本地摄像头', icon: 'menuMessage' },
    component: () => import('@/views/example/localCamera')
  }, {
    path: 'icon',
    name: 'icon',
    meta: { title: '图标库', icon: 'menuMessage' },
    component: () => import('@/views/example/icon')
  }, {
    path: 'upload',
    name: 'exampleUpload',
    meta: { title: '上传示例', icon: 'menuDocument' },
    component: () => import('@/views/example/upload')
  }, {
    path: 'skill',
    name: 'exampleSkill',
    meta: { title: '数据字典', icon: 'menuDocument', dataDict: 'skill' },
    component: () => import('@/views/upms/systemSetting/dictionary/commonDataDict')
  }, {
    path: 'permission',
    name: 'examplePermission',
    meta: { title: '权限控制', icon: 'menuDocument' },
    component: () => import('@/views/example/permission')
  }, {
    path: 'jsonEditor',
    name: 'exampleJsonEditor',
    meta: { title: 'JSON Editor', icon: 'menuDocument' },
    component: () => import('@/views/example/jsonEditor')
  }, {
    path: 'screen',
    name: 'exampleScreen',
    meta: { title: '截屏', icon: 'menuDocument' },
    component: () => import('@/views/example/screen')
  }, {
    path: 'drawingBoard',
    name: 'exampleDrawingBoard',
    meta: { title: '画板', icon: 'menuDocument' },
    component: () => import('@/views/example/drawingBoard')
  }, {
    path: 'dynamicForm',
    name: 'exampleDynamicForm',
    meta: { title: '动态表单', icon: 'menuDocument' },
    component: () => import('@/views/example/dynamicForm')
  }, {
    path: 'seo',
    name: 'exampleSEO',
    meta: { title: 'SEO优化', icon: 'menuDocument' },
    component: () => import('@/views/example/seo')
  }, {
    path: 'fullcalendar',
    name: 'exampleFullcalendar',
    meta: { title: 'Full Calendar', icon: 'menuDocument' },
    component: () => import('@/views/example/fullcalendar')
  }, {
    path: 'orgTree',
    name: 'exampleOrgTree',
    meta: { title: 'Vue Org Tree', icon: 'menuDocument' },
    component: () => import('@/views/example/orgTree')
  }, {
    path: 'chooseUser',
    name: 'exampleChooseUser',
    meta: { title: '人员/组织选择器', icon: 'menuDocument' },
    component: () => import('@/views/example/chooseUser')
  }]
}
export default exampleRoutet
