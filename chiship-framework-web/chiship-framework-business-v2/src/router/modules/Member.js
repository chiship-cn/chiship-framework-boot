/**
 * 会员
 */

import Layout from '@/layout'
const memberRouter = {
  path: '/member',
  component: Layout,
  redirect: '/member',
  name: 'member',
  meta: { title: '会员', icon: 'menuLog' },
  children: [{
    path: '',
    name: 'memberIndex',
    meta: { title: '会员', icon: 'menuMessage' },
    component: () => import('@/views/member/index')
  }, {
    path: 'wallet',
    name: 'memberWallet',
    meta: { title: '钱包变化', icon: 'menuMessage' },
    component: () => import('@/views/member/wallet')
  }, {
    path: 'withdrawalApplication',
    name: 'memberWithdrawalApplication',
    meta: { title: '提现申请', icon: 'menuMessage' },
    component: () => import('@/views/member/withdrawalApplication')
  }]
}

export default memberRouter

