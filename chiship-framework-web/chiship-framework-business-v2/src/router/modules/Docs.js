/**
 * 文档路由
 */

import Layout from '@/layout'
const docsRouter = {
  path: '/document',
  component: Layout,
  redirect: '/document/system',
  name: 'document',
  hidden: true,
  meta: { title: '文档', icon: 'menuDocument' },
  children: [{
    path: 'system',
    name: 'systemDocumentExplorer',
    meta: { title: '文档', icon: '**' },
    component: () => import('@/views/docs/system')
  }]
}

export default docsRouter
