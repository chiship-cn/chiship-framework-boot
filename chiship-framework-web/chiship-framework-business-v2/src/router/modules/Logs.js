/**
 * 日志
 */

import Layout from '@/layout'
const logsRouter = {
  path: '/securityLog',
  component: Layout,
  redirect: '/systemLog',
  name: 'securityLog',
  hidden: true,
  meta: { title: '日志', icon: 'menuLog' },
  children: [{
    path: '',
    name: 'securityLogIndex',
    meta: { title: '日志', icon: 'menuMessage' },
    component: () => import('@/views/upms/systemSetting/log/index')
  }]
}

export default logsRouter
