/**
 * 文件预览路由
 */
import EmptyLayout from '@/layout/EmptyLayout'
var fileViewRouter = {
  path: '/fileView',
  name: 'fileView',
  component: EmptyLayout,
  redirect: '/fileView/excel',
  hidden: true,
  meta: { auth: false },
  children: [{
    path: 'excel/:type/:id',
    name: 'fileViewExcel',
    meta: { auth: false },
    component: () => import('@/components/file/view/excel')
  }, {
    path: 'video/:type/:id/:title',
    name: 'fileViewVideo',
    meta: { auth: false },
    component: () => import('@/components/file/view/video')
  }, {
    path: 'images/:type/:id',
    name: 'fileViewImages',
    meta: { auth: false },
    component: () => import('@/components/file/view/images')
  }, {
    path: 'image/:type/:id',
    name: 'fileViewImage',
    meta: { auth: false },
    component: () => import('@/components/file/view/image')
  }, {
    path: 'pdf/:type/:id',
    name: 'fileViewPdf',
    meta: { auth: false },
    component: () => import('@/components/file/view/pdf')
  }, {
    path: 'notSupport/:type/:id',
    name: 'fileViewNotSupport',
    meta: { auth: false },
    component: () => import('@/components/file/view/notSupport')
  }]
}
export default fileViewRouter
