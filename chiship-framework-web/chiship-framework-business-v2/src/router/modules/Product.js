/**
 * 商品路由
 */

import Layout from '@/layout'
const productRouter = {
  path: '/product',
  component: Layout,
  redirect: '/product/index',
  name: 'product',
  meta: { title: '商品', icon: 'menuDocument' },
  children: [{
    path: 'create',
    name: 'productCreate',
    meta: { title: '商品发布', icon: '**' },
    component: () => import('@/views/product/create')
  }, {
    path: 'index',
    name: 'productIndex',
    meta: { title: '商品列表', icon: '**' },
    component: () => import('@/views/product/index')
  }, {
    path: 'category',
    name: 'productCategory',
    meta: { title: '商品分类', icon: '**' },
    component: () => import('@/views/product/category')
  }, {
    path: 'brand',
    name: 'productBrand',
    meta: { title: '商品品牌', icon: '**' },
    component: () => import('@/views/product/brand')
  }]
}

export default productRouter
