/**
 * 收银台
 */

import Layout from '@/layout'

const cashierRouter = {
  path: '/cashier',
  component: Layout,
  redirect: '/cashier',
  name: 'cashier',
  meta: { title: '收银台', icon: 'menuUser' },
  children: [{
    path: '',
    name: 'cashierIndex',
    component: () => import('@/views/cashier/index'),
    meta: { title: '收银台', icon: 'menuUser' }
  }, {
    path: 'order',
    name: 'cashierOrder',
    component: () => import('@/views/cashier/order'),
    meta: { title: '支付订单', icon: 'userManager' }
  }, {
    path: 'refundOrder',
    name: 'cashierRefundOrder',
    component: () => import('@/views/cashier/refundOrder'),
    meta: { title: '退款申请', icon: 'userManager' }
  }, {
    path: 'payment',
    name: 'cashierPayment',
    component: () => import('@/views/cashier/payment'),
    meta: { title: '支付流水', icon: 'userManager' }
  }, {
    path: 'refund',
    name: 'cashierRefund',
    component: () => import('@/views/cashier/refund'),
    meta: { title: '退款流水', icon: 'userManager' }
  }, {
    path: 'notify',
    name: 'cashierNotify',
    component: () => import('@/views/cashier/payNotify'),
    meta: { title: '通知记录', icon: 'userManager' }
  }]
}

export default cashierRouter

