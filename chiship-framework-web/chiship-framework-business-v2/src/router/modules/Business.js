
import Layout from '@/layout'
const businessRouter = {
  path: '/business',
  component: Layout,
  redirect: '/business/formDesign',
  name: 'business',
  meta: { title: '业务', icon: 'menuAdminManger' },
  children: [{
    path: 'informationSubmit',
    name: 'businessInformation',
    meta: { title: '信息提交', icon: 'menuMessage' },
    component: () => import('@/views/business/informationSubmit')
  }, {
    path: 'formValue',
    name: 'businessFormValue',
    meta: { title: '表单数据', icon: 'menuMessage' },
    component: () => import('@/views/business/formValue')
  }, {
    path: 'testExample',
    name: 'testExample',
    meta: { title: '案例', icon: 'menuMessage' },
    component: () => import('@/views/business/testExample')
  }]
}
export default businessRouter

