/**
 * 微信公众号路由
 */

import EmptyLayout from '@/layout/EmptyLayout'
const WechatRouter = {
  path: 'wechat',
  component: EmptyLayout,
  redirect: '/third/wechat/explorer',
  name: 'wechat',
  meta: { title: '微信公众号', icon: 'menuWechat' },
  children: [{
    path: '',
    name: 'wechatConfig',
    meta: { title: '公众号配置', icon: 'kennels_mm' },
    component: () => import('@/views/third/wechat/config')
  }, {
    path: 'menu',
    name: 'wechatFunctionMenu',
    component: () => import('@/views/third/wechat/wechatMenu'),
    meta: { title: '自定义菜单', icon: 'table' }
  }, {
    path: 'autoreply',
    name: 'wechatAutoReply',
    component: () => import('@/views/third/wechat/wechatAutoReply'),
    meta: { title: '消息自动回复', icon: 'table' }
  }, {
    path: '/third/wechat/materialLibrary',
    component: EmptyLayout,
    redirect: '/third/wechat/materialLibrary/index',
    name: 'wechatMaterialLibrary',
    meta: { title: '素材管理', icon: 'sys_mm' },
    children: [{
      path: 'index',
      name: 'wechatMaterialLibraryIndex',
      component: () => import('@/views/third/wechat/materialLibrary'),
      meta: { title: '素材管理', icon: 'table' }
    }, {
      path: 'imageText',
      name: 'materialLibraryImageText',
      component: () => import('@/views/third/wechat/imageText/index'),
      meta: { title: '图文素材', icon: 'table' }
    }, {
      path: 'imageText/create/:appId',
      name: 'materialLibraryImageTextCreate',
      hidden: true,
      component: () => import('@/views/third/wechat/imageText/create'),
      meta: { title: '图文素材发布', icon: 'table', activeMenu: 'materialLibraryImageText' }
    }, {
      path: 'imageText/details/:id',
      name: 'materialLibraryImageTextDetails',
      component: () => import('@/views/third/wechat/imageText/details'),
      meta: { title: '图文素材详情', icon: 'table', activeMenu: 'materialLibraryImageText' },
      hidden: true
    }, {
      path: 'category',
      name: 'materialLibraryCategory',
      component: () => import('@/views/third/wechat/resourceCategory'),
      meta: { title: '素材分类', icon: 'table' }
    }, {
      path: 'template',
      name: 'materialLibraryTemplate',
      component: () => import('@/views/third/wechat/imageText'),
      hidden: true,
      meta: { title: '模板消息', icon: 'table' }
    }]
  }, {
    path: '/third/wechat/manager',
    component: EmptyLayout,
    redirect: '/third/wechat/manager/user',
    name: 'wechatManager',
    meta: { title: '微信管理', icon: 'sys_mm' },
    children: [{
      path: 'user',
      name: 'wechatManagerUser',
      component: () => import('@/views/third/wechat/wechatUser'),
      meta: { title: '粉丝管理', icon: 'table' }
    }, {
      path: 'tag',
      name: 'wechatTag',
      component: () => import('@/views/third/wechat/tag'),
      meta: { title: '用户标签', icon: 'table' }
    }]
  }, {
    path: 'qrcode',
    name: 'wechatQrcode',
    component: () => import('@/views/third/applicationQrcode'),
    meta: { title: '二维码管理', icon: 'menuQrcode', type: 1 }
  }, {
    path: '/third/wechat/analysis',
    component: EmptyLayout,
    redirect: '/third/wechat/analysis/user',
    name: 'wechatAnalysis',
    meta: { title: '统计分析', icon: 'sys_mm' },
    children: [{
      path: 'user',
      name: 'wechatUserAnalysis',
      component: () => import('@/views/example/uncultivated'),
      meta: { title: '用户分析', icon: 'table' }
    }, {
      path: 'content',
      name: 'wechatContentAnalysis',
      component: () => import('@/views/example/uncultivated'),
      meta: { title: '内容分析', icon: 'table' }
    }, {
      path: 'menu',
      name: 'wechatMenuAnalysis',
      component: () => import('@/views/example/uncultivated'),
      meta: { title: '菜单分析', icon: 'table' }
    }]
  }]
}

export default WechatRouter
