import wechatRouter from './Wechat'

import WechatMiniRouter from './WechatMini'

import Layout from '@/layout'
const thirdRouter = {
  path: '/third',
  component: Layout,
  redirect: '/monitor/performance/systemInfo',
  name: 'third',
  meta: { title: '渠道', icon: 'menuAdminManger' },
  children: [{
    path: 'config',
    name: 'ApplicationKeyConfig',
    hidden: true,
    component: () => import('@/views/third/applicationKeyConfig'),
    meta: { title: '集成配置', icon: 'menuQuartzJob' }
  }, {
    path: 'pay',
    name: 'payConfig',
    component: () => import('@/views/third/payConfig'),
    meta: { title: '支付配置', icon: 'menuQuartzJob' }
  },
  wechatRouter,
  WechatMiniRouter
  ]
}
export default thirdRouter

