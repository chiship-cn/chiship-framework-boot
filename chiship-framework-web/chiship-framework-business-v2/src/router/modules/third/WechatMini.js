/**
 * 微信小程序路由
 */

import EmptyLayout from '@/layout/EmptyLayout'
const WechatMiniRouter = {
  path: 'wechatMini',
  component: EmptyLayout,
  redirect: '/third/wechatMini',
  name: 'wechatMini',
  meta: { title: '微信小程序', icon: 'menuWxMiNi' },
  children: [{
    path: '',
    name: 'wechatMiniConfig',
    meta: { title: '小程序配置', icon: 'menuWxMiNi' },
    component: () => import('@/views/third/wechatMini/config')
  }, {
    path: 'qrcode',
    name: 'wechatMiniQrcode',
    component: () => import('@/views/third/applicationQrcode'),
    meta: { title: '二维码管理', icon: 'menuQrcode', type: 2 }
  }]
}

export default WechatMiniRouter
