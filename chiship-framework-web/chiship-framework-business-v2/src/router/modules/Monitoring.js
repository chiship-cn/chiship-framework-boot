/**
 * 监控
 */

import Layout from '@/layout'
import EmptyLayout from '@/layout/EmptyLayout'
const MonitoringRouter = {
  path: '/monitor',
  component: Layout,
  redirect: '/monitor/systemUserOnline',
  name: 'SystemMonitor',
  meta: { title: '监控', icon: 'menuPermissionFP' },
  children: [{
    path: 'systemUserOnline',
    name: 'systemUserOnline',
    component: () => import('@/views/upms/monitor/userOnline'),
    meta: { title: '在线用户', icon: 'menuSystemUserOnline' }
  }, {
    path: 'druid',
    component: () => import('@/views/iframe'),
    name: 'druid',
    meta: { title: 'SQL监控', icon: 'menuDruid', 'remoteUrl': 'druid/' }
  }, {
    path: 'performance',
    component: EmptyLayout,
    redirect: '/monitor/performance/systemInfo',
    name: 'performanceMonitoring',
    meta: { title: '性能监控', icon: 'menuPermissionFP' },
    children: [{
      path: 'systemInfo',
      name: 'performanceSystemInfo',
      component: () => import('@/views/upms/monitor/systemInfo'),
      meta: { title: '服务监控', icon: 'menuMonitorSystem' }
    }, {
      path: 'redis',
      name: 'performanceRedis',
      component: () => import('@/views/upms/monitor/redis'),
      meta: { title: 'Redis监控', icon: 'menuMonitorRedis' }
    }, {
      path: 'tomcat',
      name: 'performanceTomcat',
      component: () => import('@/views/upms/monitor/tomcat'),
      meta: { title: 'Tomcat信息', icon: 'menuMonitorTomcat' }
    }, {
      path: 'jvm',
      name: 'performanceJVM',
      component: () => import('@/views/upms/monitor/jvm'),
      meta: { title: 'JVM信息', icon: 'menuMonitorJVM' }
    }, {
      path: 'httpTrace',
      name: 'performanceHttpTrace',
      component: () => import('@/views/upms/monitor/httpTrace'),
      meta: { title: '请求追踪', icon: 'menuMonitorTrace' }
    }]
  }, {
    path: 'swagger',
    name: 'swagger',
    component: () => import('@/views/iframe'),
    meta: { title: '接口文档', icon: 'menuDruid', 'remoteUrl': 'doc.html' }
  }]
}

export default MonitoringRouter

