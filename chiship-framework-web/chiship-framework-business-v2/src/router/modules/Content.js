import Layout from '@/layout'
const contentRouter = {
  path: '/content',
  component: Layout,
  redirect: '/content',
  name: 'content',
  meta: { title: '内容', icon: 'menuAdminManger' },
  children: [{
    path: '',
    name: 'contentIndex',
    meta: { title: '新闻管理', icon: '**' },
    component: () => import('@/views/content/article/index')
  }, {
    path: 'create',
    name: 'contentCreate',
    hidden: true,
    meta: { title: '新闻发布', icon: '**', activeMenu: 'contentIndex' },
    component: () => import('@/views/content/article/create')
  }, {
    path: 'details/:id',
    name: 'contentDetails',
    hidden: true,
    meta: { title: '新闻详情', icon: '**', activeMenu: 'contentIndex' },
    component: () => import('@/views/content/article/details')
  }, {
    path: 'category',
    name: 'contentCategory',
    meta: { title: '栏目管理', icon: '**', type: '05' },
    component: () => import('@/views/upms/systemSetting/categoryDict/commonCategoryDict')
  }, {
    path: 'notice',
    name: 'systemNotice',
    component: () => import('@/views/upms/systemSetting/notice/index'),
    meta: { title: '通知公告', icon: 'menuNotice' }
  }, {
    path: 'notice/create',
    name: 'systemNoticeCreate',
    hidden: true,
    component: () => import('@/views/upms/systemSetting/notice/create'),
    meta: { title: '通知公告发布', icon: 'menuNotice', activeMenu: 'systemNotice' }
  }, {
    path: 'notice/details',
    name: 'systemNoticeDetails',
    component: () => import('@/views/upms/systemSetting/notice/details'),
    meta: { title: '通知公告详情', icon: 'menuNotice', activeMenu: 'systemNotice' },
    hidden: true
  }, {
    path: 'feedback',
    name: 'feedback',
    component: () => import('@/views/upms/systemSetting/feedback'),
    meta: { title: '意见反馈', icon: 'menuHelp' }
  }, {
    path: 'help',
    name: 'help',
    component: () => import('@/views/upms/systemSetting/help/index'),
    meta: { title: '帮助文档', icon: 'menuHelp' }
  }, {
    path: 'help/details/:id',
    name: 'helpDetails',
    component: () => import('@/views/upms/systemSetting/help/details'),
    meta: { title: '帮助文档详情', icon: 'menuHelp', activeMenu: 'help' },
    hidden: true
  }, {
    path: 'policyAgre',
    name: 'contentPolicyAgre',
    component: () => import('@/views/upms/systemSetting/systemConfig/commonSystemSetting'),
    meta: { title: '政策协议', icon: 'menuHelp', code: 'policyAgre' }
  }, {
    path: 'advert',
    name: 'contentAdvert',
    meta: { title: '广告轮播', icon: 'menuDocument' },
    component: () => import('@/views/content/advert/index')
  }, {
    path: 'advertSlot',
    name: 'contentAdvertSlot',
    hidden: true,
    meta: { title: '广告板块', icon: 'menuDocument', activeMenu: 'contentAdvert' },
    component: () => import('@/views/content/advert/slot')
  }]
}
export default contentRouter

