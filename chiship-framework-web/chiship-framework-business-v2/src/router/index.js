/**
 * 路由
 */
import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import { constantRouters, asyncRouters } from '@/router/router.config'
const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRouters, ...asyncRouters]
})

const router = createRouter()
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
