/**
 * 公共组件
 */
import Vue from 'vue'

// JsonEditor编辑器
Vue.component('JsonEditor', () => import('@/components/JsonEditor'))
// Fullcalendar日历
Vue.component('Fullcalendar', () => import('@/components/Fullcalendar/index'))
// 画板
Vue.component('DrawingBoard', () => import('@/components/DrawingBoard/index.vue'))
// 分页组件
Vue.component('Pagination', () => import('@/components/layout/Pagination'))
// 上升下降组件
Vue.component('Trend', () => import('@/components/layout/Trend'))
// 卡片布局
Vue.component('Card', () => import('@/components/layout/Card'))
// 列表布局
Vue.component('CList', () => import('@/components/layout/List/index'))
Vue.component('CItem', () => import('@/components//layout/List/item'))

// ------------------------自定义组件开始------------------------
// 拍照上传
Vue.component('CameraUpload', () => import('@/components/custom/camera/CameraUpload'))

// 拍摄身份证
Vue.component('CameraIdCard', () => import('@/components/custom/camera/CameraIdCard'))

// 拍摄身份证OCR识别
Vue.component('CameraOCRIDCard', () => import('@/components/custom/camera/CameraOCRIDCard'))

// OCR识别
Vue.component('CameraOCR', () => import('@/components/custom/camera/CameraOCR'))

// 侧边栏组件
Vue.component('SideBox', () => import('@/components/custom/SideBox'))

// 表格工具栏组件
Vue.component('TableToolbar', () => import('@/components/custom/TableToolbar'))

// 操作按钮组件
Vue.component('ButtonOption', () => import('@/components/custom/ButtonOption'))

// 搜素框
Vue.component('Search', () => import('@/components/custom/Search'))

// 密码强度
Vue.component('PasswordLevel', () => import('@/components/custom/PasswordLevel'))

// 图形验证码
Vue.component('ImageCaptchaCode', () => import('@/components/custom/ImageCaptchaCode'))

// 验证码（手机、邮箱）
Vue.component('VerificationCode', () => import('@/components/custom/VerificationCode'))

// 地区选择器组件
Vue.component('RegionSelector', () => import('@/components/custom/selector/RegionSelector'))

// 数据字典组件
Vue.component('DataDictSelect', () => import('@/components/custom/selector/DataDictSelect'))

// 分类字典组件
Vue.component('CategoryDictSelect', () => import('@/components/custom/selector/CategoryDictSelect'))

// 邮箱填充选择组件
Vue.component('EmailInputSelect', () => import('@/components/custom/selector/EmailInputSelect'))

// 图标选择器
Vue.component('IconSelect', () => import('@/components/custom/selector/IconSelect'))

// 地图选择器组件
Vue.component('MapPointSelector', () => import('@/components/custom/selector/MapPointSelector'))

// 小圆点
Vue.component('Dot', () => import('@/components/custom/Dot'))
// ------------------------自定义组件结束-----------------------

Vue.component('FlipClock', () => import('@/components/FlipClock/index'))
// 模态对话框组件
Vue.component('ModelDialog', () => import('@/components/ModelDialog'))

// 系统字典树组件
Vue.component('CategoryDictTree', () => import('@/components/tree/CategoryDictTree'))

// 下拉菜单树组件组件
Vue.component('ElSelectTree', () => import('@/components/tree/ElSelectTree'))

// 导出组件
Vue.component('ExportDropDown', () => import('@/components/ImportExport/ExportDropDown'))

// 导入数据组件
Vue.component('ImportData', () => import('@/components/ImportExport/ImportData'))

// 文件格式图标组件
Vue.component('FileTypeSvgIcon', () => import('@/components/file/FileTypeSvgIcon'))

// 文件夹格式图标组件
Vue.component('FileCatalogSvgIcon', () => import('@/components/file/FileCatalogSvgIcon'))

// 文件下载组件
Vue.component('FileDownload', () => import('@/components/file/FileDownload'))
// 文件预览组件
Vue.component('FileViewer', () => import('@/components/file/FileViewer'))

// 文件列表项组件
Vue.component('FileItem', () => import('@/components/file/FileItem'))
// 图片列表项组件
Vue.component('FileImageItem', () => import('@/components/file/FileImageItem'))
// 文件选择器
Vue.component('FileChoose', () => import('@/components/file/FileChoose'))

// Uploader组件
Vue.component('FileUploader', () => import('@/components/upload/Uploader'))
// 单张图片上传组件
Vue.component('SingleImageUpload', () => import('@/components/upload/SingleImageUpload'))
// 多张图片上传组件
Vue.component('MultipleImageUpload', () => import('@/components/upload/MultipleImageUpload'))

// 文件上传组件
Vue.component('SimpleUploader', () => import('@/components/upload/SimpleUploader'))

// 富文本编辑器
Vue.component('Editor', () => import('@/components/Editor/Editor'))
// DOC文档编辑器
Vue.component('DocEditor', () => import('@/components/Editor/DocEditor'))

// 复制组件
Vue.component('CopyText', () => import('@/components/CopyText'))

Vue.component('organizationTree', () => import('@/views/components/tree/organizationTree'))

// 用户选择器（列表）
Vue.component('UserListSelector', () => import('@/views/components/selector/UserListSelector'))
// 用户选择器
Vue.component('UserSelector', () => import('@/views/components/selector/UserSelector/index'))
// 用户选择器（带组织）
Vue.component('UserSelector2', () => import('@/views/components/selector/UserSelector2'))
// 用户回显列表
Vue.component('UserItem2', () => import('@/views/components/selector/UserItem2'))
// 组织选择器
Vue.component('OrgSelector', () => import('@/views/components/selector/OrgSelector/index'))

// 用户详情组件
Vue.component('UserDetails', () => import('@/views/components/UserDetails'))

// 字典选项操做
Vue.component('DataDictEditor', () => import('@/views/components/DataDictEditor'))
Vue.component('DataDictEditorDialog', () => import('@/views/components/DataDictEditorDialog'))

// 分类字典选项操做
Vue.component('CategoryDictEditor', () => import('@/views/components/CategoryDictEditor'))
Vue.component('CategoryDictEditorDialog', () => import('@/views/components/CategoryDictEditorDialog'))
Vue.component('CategoryDictTree', () => import('@/views/components/tree/CategoryDictTree'))

// 系统配置操作
Vue.component('SystemSettingEditor', () => import('@/views/components/SystemSettingEditor'))

// 岗位选择器
Vue.component('PostSelect', () => import('@/views/components/PostSelect'))

// 分享二维码
Vue.component('ShareQrCode1', () => import('@/views/components/ShareQrCode1'))
Vue.component('ShareQrCode2', () => import('@/views/components/ShareQrCode2'))
