import Cookies from 'js-cookie'

const defaultSettings = require('@/settings.js')

export function getToken() {
  return Cookies.get(defaultSettings.tokenKey)
}

export function setToken(token) {
  return Cookies.set(defaultSettings.tokenKey, token)
}

export function removeToken() {
  Cookies.remove('lockScreen')
  return Cookies.remove(defaultSettings.tokenKey)
}
