/**
 * 打印插件
 * npm install print-js --save
 */
import print from 'print-js'
import Vue from 'vue'

Vue.prototype.$print = (id) => {
  setTimeout(() => {
    print({
      printable: id,
      type: 'html',
      scanStyles: false,
      targetStyles: ['*']
    })
  }, 500)
}

