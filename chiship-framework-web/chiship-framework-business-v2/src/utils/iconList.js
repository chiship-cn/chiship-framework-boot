import Vue from 'vue'
var iconList = [
  {
    label: '阿里云图标',
    type: 'iconfont',
    list: [
      { label: '主页', value: 'icon-zhuye' },
      { label: '验证 验证码', value: 'icon-yanzhengyanzhengma' },
      { label: '主页数据源', value: 'icon-shujuyuan' },
      { label: '秘钥', value: 'icon-miyao42' },
      { label: 'Tomcat', value: 'icon-tomcat' },
      { label: '通知公告', value: 'icon-tongzhigonggao' },
      { label: '机构', value: 'icon-jigou' },
      { label: '帮助文档', value: 'icon-bangzhuwendang' },
      { label: '意见反馈', value: 'icon-yijianfankui' },
      { label: '分类', value: 'icon-fenlei' },
      { label: '系统监控', value: 'icon-xitongjiankong' },
      { label: 'Redis', value: 'icon-Redis' },
      { label: '角色管理', value: 'icon-jiaoseguanli' },
      { label: '系统管理', value: 'icon-xitongguanli' },
      { label: '用户管理', value: 'icon-yonghuguanli' },
      { label: '平台管理', value: 'icon-pingtaiguanli' },
      { label: '字典管理', value: 'icon-zidianguanli' },
      { label: '在线用户', value: 'icon-zaixianyonghu' },
      { label: '文档管理', value: 'icon-wendangguanli' },
      { label: '用户权限', value: 'icon-yonghuquanxian' },
      { label: '参数配置', value: 'icon-canshupeizhi' },
      { label: '服务监控', value: 'icon-zu1773' },
      { label: '系统设置', value: 'icon-xitongshezhi' },
      { label: '用户设置', value: 'icon-yonghushezhi' },
      { label: '权限分配', value: 'icon-quanxianfenpei' },
      { label: '行政区划管理', value: 'icon-hangzhengquhuaguanli' },
      { label: '虚拟机', value: 'icon-xuniji' },
      { label: '业务性能监控', value: 'icon-yewuxingnengjiankong' },
      { label: 'app管理', value: 'icon-appguanli' },
      { label: '菜单管理', value: 'icon-caidanguanli' },
      { label: '系统日志', value: 'icon-xitongrizhi' },
      { label: '接口文档', value: 'icon-jiekouwendang' },
      { label: '追踪', value: 'icon-thinline107-05' },
      { label: '角色权限', value: 'icon-jiaosequanxian' },
      { label: '岗位管理', value: 'icon-a-huaban2fuben18' },
      { label: 'SQL监控', value: 'icon-SQLjiankong' },
      { label: '表单', value: 'icon-biaodan' },
      { label: '用户分析', value: 'icon-yonghufenxi' },
      { label: '回复', value: 'icon-chakantiezihuifu' },
      { label: '用户标签', value: 'icon-tubiao-' },
      { label: '二维码', value: 'icon-erweimaguanli-' },
      { label: '自定义表单', value: 'icon-zidingyibiaodan' },
      { label: '集成-A', value: 'icon-jicheng-A' },
      { label: '微信菜单管理', value: 'icon-zcpt-weixincaidanguanli' },
      { label: '摄像机', value: 'icon-shexiangji' },
      { label: '粉丝', value: 'icon-fensi' },
      { label: '业务管理', value: 'icon-yewuguanli' },
      { label: '内容分析', value: 'icon-neirong' },
      { label: '统计分析', value: 'icon-tongjifenxi' },
      { label: '小程序', value: 'icon-xiaochengxu' },
      { label: '配置', value: 'icon-peizhi' },
      { label: '代码示例', value: 'icon-daimashili' },
      { label: '地图', value: 'icon-ditu' },
      { label: '我的消息', value: 'icon-wodexiaoxi' },
      { label: '素材管理', value: 'icon-sucaiguanli' },
      { label: '自定义菜单', value: 'icon-zidingyicaidan' },
      { label: '工作流', value: 'icon-gongzuoliu' },
      { label: '服务配置', value: 'icon-fuwupeizhi' },
      { label: '微信公众号', value: 'icon-weixingongzhonghao' },
      { label: '图标库', value: 'icon-tubiaoku' },
      { label: '图文素材', value: 'icon-tuwenguanli' },
      { label: '模板消息', value: 'icon-moban' },
      { label: '企业信息', value: 'icon-qiyexinxi' },
      { label: '用户资料', value: 'icon-yonghuziliao' },
      { label: '修改头像', value: 'icon-xiugaitouxiang' },
      { label: '修改密码', value: 'icon-modify-password' },
      { label: '更换手机号', value: 'icon-shouji_huaban1' },
      { label: '更换邮箱', value: 'icon-youxiang' },
      { label: '下属员工', value: 'icon-xiashugendan' },
      { label: '登录记录', value: 'icon-denglujilu' },
      { label: '系统日志', value: 'icon-xitongrizhi1' },
      { label: '错误日志', value: 'icon-cuowurizhi' },
      { label: '登录日志', value: 'icon-denglurizhi' },
      { label: '定时任务日志', value: 'icon-dingshirenwu' }
    ]
  }]

const svgFiles = require.context('@/icons/svg', false, /\.svg$/)
const svgIcons = svgFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  modules.push({ label: moduleName, value: moduleName })
  return modules
}, [])
iconList.push({
  label: 'SVG图标',
  type: 'svg',
  list: svgIcons
})

Vue.prototype.iconList = iconList
export default iconList
