import Vue from 'vue'
import { mapGetters } from 'vuex'
import { Message } from 'element-ui'

const mixin = {
  data() {
    return {
    }
  },
  computed: {
    ...mapGetters([
      'userInfo',
      'roleInfo',
      'orgInfo',
      'deptInfo',
      'roleInfos',
      'orgInfos',
      'deptInfos'
    ])
  },
  methods: {
    handleToBeDeveloped() {
      Message({
        message: '工程师正在努力制作...',
        type: 'warning',
        duration: 5 * 1000
      })
    }
  }
}
Vue.mixin(mixin)
export default mixin
