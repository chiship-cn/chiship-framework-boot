/**
 * 公共正则   此文件不允许改动，若有改动，联系李剑|18363003321
 * lijian
 * 每个前端，直接复制使用
 */
import Vue from 'vue'

var regular = {
  // 不能特殊字符
  specialCharacters: new RegExp("[.`~!@$^&*=|{}';',\\[\\]《》/~！@#￥……&*{}【】‘；：”“'。，？' ']"),

  // 姓名
  realNameRegular: /^[\u4E00-\u9FA5\uf900-\ufa2d·s]{2,20}$/,

  // 手机号
  mobile: /^1[0-9]{10}$/,

  // 电话号码
  phone: /^((0\d{2,3})-)(\d{7,8})(-(\d{3,}))?$/,

  // 邮箱
  email: /^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/,

  // URL
  url: /^http[s]?:\/\/.*/,

  // ip
  ip: /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/,

  /**
   * 正则验证
   * @param {*} val 值
   * @param {*} regular   正则
   * @returns true:通过 false:失败
   */
  test: function(val = '', regular = null) {
    if (!regular) {
      console.error('请传递正确得正则表达式')
      return false
    }
    return regular.test(val)
  }
}
Vue.prototype.$regular = regular
export default regular

