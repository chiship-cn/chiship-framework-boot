
/**
 * 自定义指令
 */
import Vue from 'vue'
Vue.directive('drag', {
  bind: function(el, binding, vnode) {
    const left = document.getElementsByClassName('container-left')
    const box = document.getElementsByClassName('main__container')
    el.onmousedown = function(e) {
      const startX = e.clientX
      el.left = el.offsetLeft
      document.onmousemove = function(e) {
        const endX = e.clientX
        let moveLen = el.left + (endX - startX)
        const maxT = box[0].clientWidth - el.offsetWidth
        if (moveLen < 150) moveLen = 360
        if (moveLen > maxT - 800) moveLen = maxT - 800
        el.style.left = moveLen
        left[0].style.width = moveLen + 'px'
      }
      document.onmouseup = function() {
        document.onmousemove = document.onmouseup = null
      }
    }
  }
})

Vue.directive('tableInfiniteScroll', {
  bind(el, binding) {
    const selectWarp = el.querySelector('.el-table__body-wrapper')
    selectWarp.addEventListener('scroll', function() {
      const sign = 0
      const scrollDistance = this.scrollHeight - this.scrollTop - this.clientHeight
      if (scrollDistance <= sign) {
        binding.value()
      }
    })
  }
})
