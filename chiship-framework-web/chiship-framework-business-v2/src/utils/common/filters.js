import Vue from 'vue'
import common from './index'
import tools from './tools'
/**
 * 公共过滤器   此文件不允许改动，若有改动，联系李剑|18363003321
 * lijian
 * 每个前端，直接复制使用
 */

/**
 * 隐藏手机号码
 * @param val {Number, String} 转换的字符串对象
 * @param retain {Number} 保留位数
 * @return {String}
 */
Vue.filter('privatePhone', (val, retain = 4) => {
  if (String(val).length !== 11 || retain === 0) return val
  const phone = String(val)
  const digit = 11 - 3 - retain
  const reg = new RegExp(`^(\\d{3})\\d{${digit}}(\\d{${retain}})$`)
  return phone.replace(reg, `$1${'*'.repeat(digit)}$2`)
})

/**
 * 隐藏姓名
 * @param val {String} 转换的字符串对象
 * @return {String}
 */
Vue.filter('privateRealName', (val) => {
  if (!val) return ''
  if (val.length === 1) return val
  const firstChar = val.charAt(0)
  if (val.length === 2) return firstChar + '*'
  const lastChar = val.charAt(val.length - 1)
  const middleStars = '*'.repeat(val.length - 2)
  return firstChar + middleStars + lastChar
})

/**
 * 格式化时间
 * @param {*} val
 * @param {*} fmt
 */
Vue.filter('formatDate', (val, fmt) => {
  return tools.formatDate(val, fmt)
})

/**
 * 格式化运行时间
 */
Vue.filter('formatRunTime', (ms) => {
  if (ms) {
    if (ms < 1000) { return '<1秒' }
    const sec_a = Math.round(ms / 1000)
    const min_a = Math.floor(sec_a / 60)
    const sec = sec_a - min_a * 60
    const hour_a = Math.floor(min_a / 60)
    const min = min_a - hour_a * 60
    const d = Math.floor(hour_a / 24)
    const hour = hour_a - d * 24

    let str = ''
    if (sec) { str = sec + '秒' + str }
    if (!min_a) { return str }

    if (min) { str = min + '分钟' + str }
    if (!hour_a) { return str }

    if (hour) { str = hour + '小时' + str }
    if (!d) { return str }

    return d + '天' + str
  }
  return '0秒'
})

/**
 * 计算年龄
 */
Vue.filter('calcAge', (val) => {
  if (val) {
    return tools.calcAge(tools.formatDate(val, 'yyyy-MM-dd'))
  } else {
    return null
  }
})

/**
 * 格式化地区
 * @param {*} val
 */
Vue.filter('regionFormat', (val) => {
  return common.regionFormat(val)
})

/**
 * 格式化数据字典
 * @param {*} val
 */
Vue.filter('dictFormat', (val, mode = 'name') => {
  const result = common.dataDictFormat(val)
  if (typeof (result) === 'object') {
    if (mode === 'name') {
      return result.typeName
    } else if (mode === 'code') {
      return result.typeCode
    } else {
      return result.typeName + '/' + result.typeCode
    }
  }
  return result
})

/**
 * 格式化操作者
 * @param {*} val
 */
Vue.filter('optionUserFormat', (val) => {
  if (val) {
    try {
      var optionUser = JSON.parse(val)
      return optionUser.realName + (optionUser.orgName ? (' | ' + optionUser.orgName) : '')
    } catch (err) {
      return '-'
    }
  }
  return '-'
})

/**
 * 格式化用户选择器选中的数值
 * @param {*} val
 */
Vue.filter('userSelectorFormat', (val) => {
  if (val) {
    try {
      if (val.split(':').length === 1) {
        return val
      } else {
        return val.split(':')[1]
      }
    } catch (err) {
      return '暂无信息'
    }
  }
  return '-'
})

/**
 * 格式化价格
 */
Vue.filter('formatPrice', (val) => {
  if (val === undefined) return ''
  const Num = val.toString().split('.')
  let price = ''
  if (Num[1]) {
    if (Num[1].length > 2) {
      // 小数点后位数大于2
      const dot = Num[1].slice(0, 2)
      price = `${Num[0]}.${dot}`
    } else if (Num[1].length === 1) {
      // 小数点后位数等于1
      price = `${val}0`
    } else {
      // 小数点后位数等于2
      price = val
    }
  } else {
    price = `${val}.00`
  }
  return '￥' + price
})

/**
 * 格式化文件大小
 */
Vue.filter('formatFileSize', (val) => {
  if (!val) {
    return '-'
  } else if (typeof (val) !== 'number') {
    return '-'
  } else if (val < 1024) {
    return val + 'B'
  } else if (val < 1024 * 1024) {
    return (val / 1024).toFixed(2) + 'KB'
  } else if (val < 1024 * 1024 * 1024) {
    return (val / 1024 / 1024).toFixed(2) + 'M'
  } else {
    return (val / 1024 / 1024 / 1024).toFixed(2) + 'G'
  }
})

/**
 * 监测文件类型
 * @param {*} val filename
 */
Vue.filter('fileType', (val) => {
  var key = 99
  try {
    const temp = val.split('.')
    const typeName = temp[temp.length - 1].toLowerCase()
    if (typeName) {
      if (typeName === 'png' || typeName === 'jpg' || typeName === 'gif' || typeName === 'jpeg') {
        key = 1
      } else if (typeName === 'docx' || typeName === 'doc') {
        key = 2
      } else if (typeName === 'xlsx' || typeName === 'xls') {
        key = 3
      } else if (typeName === 'pptx' || typeName === 'ppt') {
        key = 4
      } else if (typeName === 'txt') {
        key = 5
      } else if (typeName === 'zip' || typeName === '7z' || typeName === 'iso' || typeName === 'rar') {
        key = 6
      } else if (typeName === 'pdf') {
        key = 7
      } else {
        key = 99
      }
    }
    return key
  } catch (err) {
    return key
  }
})
