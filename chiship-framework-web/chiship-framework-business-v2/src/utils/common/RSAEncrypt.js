/**
 * RSA参数签名及加密
 */
import JsEncrypt from 'jsencrypt'
import { KJUR, KEYUTIL, hex2b64 } from 'jsrsasign'
import Vue from 'vue'
import { Message } from 'element-ui'
const defaultSettings = require('@/settings.js')
var RSAEncrypt = {
  // 签名算法
  ALGORITHM: 'SHA256withRSA',
  encrypt: function(data) {
    const jse = new JsEncrypt()
    jse.setPublicKey(defaultSettings.publicKey)
    if (typeof (data) === 'object') {
      data = JSON.stringify(data)
    }
    const result = jse.encrypt(data)
    if (typeof (result) === 'boolean') {
      if (!result) {
        Message({
          message: '公钥不正确,请核对！',
          type: 'warning'
        })
        return null
      }
    }
    return result
  },
  decrypt: function(data) {
    if (typeof (data) !== 'string') {
      Message({
        message: '加密数据不正确,请核对！',
        type: 'warning'
      })
      return null
    }
    const jse = new JsEncrypt()
    jse.setPrivateKey(defaultSettings.privateKey)
    if (typeof (data) === 'object') {
      data = JSON.stringify(data)
    }
    const result = jse.decrypt(data)
    return result
  },
  // 私钥签名
  rsaSign: function(plaintext) {
    const signature = new KJUR.crypto.Signature({ 'alg': this.ALGORITHM })
    // 来解析密钥
    const priKey = KEYUTIL.getKey(defaultSettings.privateKey)
    signature.init(priKey)
    // 传入待签明文
    signature.updateString(plaintext)
    const a = signature.sign()
    // 转换成base64，返回
    return hex2b64(a)
  },
  paramSign(config) {
    var encryptParams = config.params
    var method = config.method
    if (method === 'get') {
      if (config.params != null) {
        encryptParams = config.params
      } else {
        encryptParams = {}
      }
    }
    if (method === 'post') {
      if (config.data != null) {
        encryptParams = config.data
      } else {
        encryptParams = {}
      }
    }

    const values = []
    if (typeof (encryptParams) === 'object') {
      if (Object.prototype.toString.call(encryptParams) === '[object Array]') {
        encryptParams.forEach((item) => {
          if (typeof (item) === 'object') {
            for (const encryptParam in item) {
              if (item[encryptParam] !== undefined) {
                values.push(item[encryptParam])
              }
            }
          } else {
            values.push(item)
          }
        })
      } else {
        for (const encryptParam in encryptParams) {
          if (encryptParams[encryptParam] !== undefined) {
            values.push(encryptParams[encryptParam])
          }
        }
      }
    } else {
      if (encryptParams !== undefined) {
        values.push(encryptParams)
      }
    }
    values.sort()
    values.push(defaultSettings.signKey)

    let s = ''
    values.forEach(function(v, i) {
      if (v !== null) {
        s = s + (v) + ''
      }
    })
    // console.log('参与加密参数:' + s)
    return this.rsaSign(s)
  }
}

Vue.prototype.RSAEncrypt = RSAEncrypt
export default RSAEncrypt
