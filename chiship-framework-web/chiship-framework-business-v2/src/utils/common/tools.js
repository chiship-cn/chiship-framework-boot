/**
 * 工具类
 */
import Vue from 'vue'
import CryptoJS from 'crypto-js'
import { Message } from 'element-ui'
import regular from './regular'
import { v4 as uuidv4 } from 'uuid'
import md5 from 'js-md5'
import moment from 'moment'

var tools = {
  /**
   * 邮箱
   * @param {*} s
   * @returns
   */
  isEmail(s) {
    return regular.email.test(s)
  },

  /**
   * 手机
   * @param {*} s
   * @returns
   */
  isMobile(s) {
    return regular.mobile.test(s)
  },

  /**
   * 电话号码
   * @param {*} s
   * @returns
   */
  isPhone(s) {
    return regular.phone.test(s)
  },

  /**
   * URL
   * @param {*} s
   * @returns
   */
  isURL(s) {
    return regular.url.test(s)
  },

  /**
   * 是否字符串
   * @param {*} o
   * @returns
   */
  isString(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'String'
  },

  /**
   * 是否数字
   * @param {*} o
   * @returns
   */
  isNumber(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Number'
  },

  /**
   * 是否boolean
   * @param {*} o
   * @returns
   */
  isBoolean(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Boolean'
  },

  /**
   * 是否函数
   * @param {*} o
   * @returns
   */
  isFunction(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Function'
  },

  /**
   * 是否为null
   * @param {*} o
   * @returns
   */
  isNull(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Null'
  },

  /**
   * 是否undefined
   * @param {*} o
   * @returns
   */
  isUndefined(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Undefined'
  },

  /**
   * 是否对象
   * @param {*} o
   * @returns
   */
  isObj(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Object'
  },

  /**
   * 是否数组
   * @param {*} o
   * @returns
   */
  isArray(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Array'
  },

  /**
   * 是否时间
   * @param {*} o
   * @returns
   */
  isDate(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Date'
  },

  /**
   * 是否正则
   * @param {*} o
   * @returns
   */
  isRegExp(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'RegExp'
  },

  /**
   * 是否错误对象
   * @param {*} o
   * @returns
   */
  isError(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Error'
  },

  /**
   * 是否Symbol函数
   * @param {*} o
   * @returns
   */
  isSymbol(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Symbol'
  },

  /**
   * 是否Promise对象
   * @param {*} o
   * @returns
   */
  isPromise(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Promise'
  },

  /**
   * 是否Set对象
   * @param {*} o
   * @returns
   */
  isSet(o) {
    return Object.prototype.toString.call(o).slice(8, -1) === 'Set'
  },

  /**
   * 用户代理
   * @returns
   */
  ua() {
    return navigator.userAgent.toLowerCase()
  },

  /**
   * 是否是微信浏览器
   * @returns
   */
  isWeiXin() {
    return /(micromessenger)/i.test(this.ua())
  },

  /**
   * 是否是移动端
   * @returns
   */
  isDeviceMobile() {
    return /android|webos|iphone|ipod|balckberry/i.test(this.ua())
  },

  /**
   * 是否是QQ浏览器
   * @returns
   */
  isQQBrowser() {
    return !!this.ua().match(/mqqbrowser|qzone|qqbrowser|qbwebviewtype/i)
  },

  /**
   * 是否是爬虫
   * @returns
   */
  isSpider() {
    return /adsbot|googlebot|bingbot|msnbot|yandexbot|baidubot|robot|careerbot|seznambot|bot|baiduspider|jikespider|symantecspider|scannerlwebcrawler|crawler|360spider|sosospider|sogou web sprider|sogou orion spider/.test(this.ua())
  },

  /**
   * 是否ios
   * @returns
   */
  isIos() {
    var u = navigator.userAgent
    if (u.indexOf('Android') > -1 || u.indexOf('Linux') > -1) { // 安卓手机
      return false
    } else if (u.indexOf('iPhone') > -1) { // 苹果手机
      return true
    } else if (u.indexOf('iPad') > -1) { // iPad
      return false
    } else if (u.indexOf('Windows Phone') > -1) { // winphone手机
      return false
    } else {
      return false
    }
  },

  /**
   * 是否为PC端
   * @returns
   */
  isPC() {
    var userAgentInfo = navigator.userAgent
    var Agents = ['Android', 'iPhone',
      'SymbianOS', 'Windows Phone',
      'iPad', 'iPod']
    var flag = true
    for (var v = 0; v < Agents.length; v++) {
      if (userAgentInfo.indexOf(Agents[v]) > 0) {
        flag = false
        break
      }
    }
    return flag
  },

  /**
   * 去除html标签
   * @param {*} str
   * @returns
   */
  removeHtmltag(str) {
    if (!str) {
      return ''
    }
    return str.replace(/<[^>]+>/g, '')
  },

  /**
   * 获取url参数
   * @param {*} name
   * @returns
   */
  getQueryString(name) {
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i')
    var r = window.location.search.substr(1).match(reg) // 获取url中"?"符后的字符串并正则匹配
    var context = ''
    if (r != null) { context = r[2] }
    reg = null
    r = null
    return context == null || context === '' || context === 'undefined' ? '' : context
  },

  /**
   * 动态引入js
   * @param {*} src
   */
  injectScript(src) {
    const s = document.createElement('script')
    s.type = 'text/javascript'
    s.async = true
    s.src = src
    const t = document.getElementsByTagName('script')[0]
    t.parentNode.insertBefore(s, t)
  },

  /**
   * 根据url地址下载
   * @param {*} url
   * @returns
   */
  downloadUrl(url, fileName = null) {
    var isChrome = navigator.userAgent.toLowerCase().indexOf('chrome') > -1
    var isSafari = navigator.userAgent.toLowerCase().indexOf('safari') > -1
    if (isChrome || isSafari) {
      var link = document.createElement('a')
      link.href = url
      if (link.download !== undefined) {
        if (!fileName) {
          fileName = url.substring(url.lastIndexOf('/') + 1, url.length)
        }

        link.download = fileName
      }
      if (document.createEvent) {
        var e = document.createEvent('MouseEvents')
        e.initEvent('click', true, true)
        link.dispatchEvent(e)
        return true
      }
    }
    if (url.indexOf('?') === -1) {
      url += '?download'
    }
    window.open(url, '_self')
    return true
  },
  /**
   * 下载文件
   */
  downloadFile(response, fileName) {
    if (!fileName) {
      fileName = response.fileName
    }
    if (!fileName) {
      Message({
        message: '下载文件流中没有返回文件名称，请配置文件名或接口返回【Download-FileName】消息头',
        type: 'error',
        duration: 5 * 1000
      })
      return
    }
    if ('msSaveOrOpenBlob' in navigator) { // 兼容ie
      const blob = new Blob([response])
      window.navigator.msSaveOrOpenBlob(blob, fileName)
      return
    } else {
      const link = document.createElement('a')
      const blob = new Blob([response])
      link.style.display = 'none'
      link.href = URL.createObjectURL(blob)
      link.setAttribute('download', decodeURI(fileName))
      document.body.appendChild(link)
      link.click()
      document.body.removeChild(link)
    }
  },
  /**
   * el是否包含某个class
   * @param {*} el
   * @param {*} className
   * @returns
   */
  hasClass(el, className) {
    const reg = new RegExp('(^|\s)' + className + '(\s|$)')
    return reg.test(el.className)
  },

  /**
   * el添加某个class
   * @param {*} el
   * @param {*} className
   * @returns
   */
  addClass(el, className) {
    const reg = new RegExp('(^|\s)' + className + '(\s|$)')
    if (reg.test(el, className)) {
      return
    }
    const newClass = el.className.split(' ')
    newClass.push(className)
    el.className = newClass.join(' ')
  },

  /**
   * el去除某个class
   * @param {*} el
   * @param {*} className
   * @returns
   */
  removeClass(el, className) {
    var reg = new RegExp('(^|\s)' + className + '(\s|$)')
    if (reg.test(el, className)) {
      return
    }
    reg = new RegExp('(^|\\s)' + className + '(\\s|$)', 'g')
    el.className = el.className.replace(reg, ' ')
  },

  /**
   * 判断类型集合
   * @param {*} str
   * @param {*} type
   * @returns
   */
  checkStr(str, type) {
    switch (type) {
      case 'phone': // 手机号码
        return /^1[3|4|5|6|7|8|9][0-9]{9}$/.test(str)
      case 'tel': // 座机
        return /^(0\d{2,3}-\d{7,8})(-\d{1,4})?$/.test(str)
      case 'card': // 身份证
        return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(str)
      case 'pwd': // 密码以字母开头，长度在6~18之间，只能包含字母、数字和下划线
        return /^[a-zA-Z]\w{5,17}$/.test(str)
      case 'postal': // 邮政编码
        return /[1-9]\d{5}(?!\d)/.test(str)
      case 'QQ': // QQ号
        return /^[1-9][0-9]{4,9}$/.test(str)
      case 'email': // 邮箱
        return /^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)+$/.test(str)
      case 'money': // 金额(小数点2位)
        return /^\d*(?:\.\d{0,2})?$/.test(str)
      case 'URL': // 网址
        return /(http|ftp|https):\/\/[\w\-_]+(\.[\w\-_]+)+([\w\-\.,@?^=%&:/~\+#]*[\w\-\@?^=%&/~\+#])?/.test(str)
      case 'IP': // IP
        return /((?:(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d)\\.){3}(?:25[0-5]|2[0-4]\\d|[01]?\\d?\\d))/.test(str)
      case 'date': // 日期时间
        return /^(\d{4})\-(\d{2})\-(\d{2}) (\d{2})(?:\:\d{2}|:(\d{2}):(\d{2}))$/.test(str) || /^(\d{4})\-(\d{2})\-(\d{2})$/.test(str)
      case 'number': // 数字
        return /^[0-9]$/.test(str)
      case 'english': // 英文
        return /^[a-zA-Z]+$/.test(str)
      case 'chinese': // 中文
        return /^[\\u4E00-\\u9FA5]+$/.test(str)
      case 'lower': // 小写
        return /^[a-z]+$/.test(str)
      case 'upper': // 大写
        return /^[A-Z]+$/.test(str)
      case 'HTML': // HTML标记
        return /<("[^"]*"|'[^']*'|[^'">])*>/.test(str)
      default:
        return true
    }
  },

  /**
   * 严格的身份证校验
   * @param {*} sId
   * @returns
   */
  isCardID(sId) {
    if (!/(^\d{15}$)|(^\d{17}(\d|X|x)$)/.test(sId)) {
      console.error('你输入的身份证长度或格式错误')
      return false
    }
    // 身份证城市
    var aCity = { 11: '北京', 12: '天津', 13: '河北', 14: '山西', 15: '内蒙古', 21: '辽宁', 22: '吉林', 23: '黑龙江', 31: '上海', 32: '江苏', 33: '浙江', 34: '安徽', 35: '福建', 36: '江西', 37: '山东', 41: '河南', 42: '湖北', 43: '湖南', 44: '广东', 45: '广西', 46: '海南', 50: '重庆', 51: '四川', 52: '贵州', 53: '云南', 54: '西藏', 61: '陕西', 62: '甘肃', 63: '青海', 64: '宁夏', 65: '新疆', 71: '台湾', 81: '香港', 82: '澳门', 91: '国外' }
    if (!aCity[parseInt(sId.substr(0, 2))]) {
      console.error('你的身份证地区非法')
      return false
    }

    // 出生日期验证
    var sBirthday = (sId.substr(6, 4) + '-' + Number(sId.substr(10, 2)) + '-' + Number(sId.substr(12, 2))).replace(/-/g, '/')
    var d = new Date(sBirthday)
    if (sBirthday !== (d.getFullYear() + '/' + (d.getMonth() + 1) + '/' + d.getDate())) {
      console.error('身份证上的出生日期非法')
      return false
    }

    // 身份证号码校验
    var sum = 0
    var weights = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2]
    var codes = '10X98765432'
    for (var i = 0; i < sId.length - 1; i++) {
      sum += sId[i] * weights[i]
    }
    var last = codes[sum % 11] // 计算出来的最后一位身份证号码
    if (sId[sId.length - 1] !== last) {
      console.error('你输入的身份证号非法')
      return false
    }

    return true
  },
  /**
   * 根据身份证号返回性别、年龄、出生日期
   * @param {*} userCard
  */
  analyzeIDCard(userCard) {
    // 先验证身份证号是否合法
    if (this.isCardID(userCard)) {
      const obj = {}
      // 获取性别
      if (parseInt(userCard.substr(16, 1)) % 2 === 1) {
        obj.sex = 1
      } else {
        obj.sex = 2
      }
      // 获取出生年月日
      var yearBirth = userCard.substring(6, 10)
      var monthBirth = userCard.substring(10, 12)
      var dayBirth = userCard.substring(12, 14)
      // 获得时间戳
      var date = yearBirth + '/' + monthBirth + '/' + dayBirth
      var timestamp = new Date(date).getTime()
      obj.birthday = timestamp
      // 获取当前年月日并计算年龄
      var myDate = new Date()
      var monthNow = myDate.getMonth() + 1
      var dayNow = myDate.getDay()
      var age = myDate.getFullYear() - yearBirth
      if (monthNow < monthBirth || (monthNow === monthBirth && dayNow < dayBirth)) {
        age--
      }
      // 得到年龄
      obj.age = age
      return obj
    }
    return null
  },

  /**
   * 随机数范围
   * @param {*} min
   * @param {*} max
   * @returns
   */
  random(min, max) {
    if (arguments.length === 2) {
      return Math.floor(min + Math.random() * ((max + 1) - min))
    } else {
      return null
    }
  },
  /**
   * 生成UUID
   * @returns
   */
  uuid() {
    return uuidv4().split('-').join('')
  },
  /**
   * MD5加密
   * @param {*} val
   * @returns
   */
  md5(val) {
    return md5('中文')
  },

  /**
   * 将阿拉伯数字翻译成中文的大写数字
   * @param {*} num
   * @returns
   */
  numberToChinese(num) {
    var AA = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九', '十']
    var BB = ['', '十', '百', '仟', '萬', '億', '点', '']
    var a = ('' + num).replace(/(^0*)/g, '').split('.')
    var k = 0
    var re = ''
    for (var i = a[0].length - 1; i >= 0; i--) {
      switch (k) {
        case 0:
          re = BB[7] + re
          break
        case 4:
          if (!new RegExp('0{4}//d{' + (a[0].length - i - 1) + '}$')
            .test(a[0])) { re = BB[4] + re }
          break
        case 8:
          re = BB[5] + re
          BB[7] = BB[5]
          k = 0
          break
      }
      if (k % 4 === 2 && a[0].charAt(i + 2) !== 0 && a[0].charAt(i + 1) === 0) { re = AA[0] + re }
      if (a[0].charAt(i) !== 0) { re = AA[a[0].charAt(i)] + BB[k % 4] + re }
      k++
    }

    if (a.length > 1) {
      re += BB[6]
      for (var j = 0; j < a[1].length; j++) {
        re += AA[a[1].charAt(j)]
      }
    }
    if (re === '一十') {
      re = '十'
    }
    if (re.match(/^一/) && re.length === 3) {
      re = re.replace('一', '')
    }
    return re
  },

  /**
   * 将数字转换为大写金额
   * @param {*} Num
   * @returns
   */
  changeToChinese(Num) {
    // 判断如果传递进来的不是字符的话转换为字符
    if (typeof Num === 'number') {
      Num = Num + ''
    }
    Num = Num.replace(/,/g, '') // 替换tomoney()中的“,”
    Num = Num.replace(/ /g, '') // 替换tomoney()中的空格
    Num = Num.replace(/￥/g, '') // 替换掉可能出现的￥字符
    if (isNaN(Num)) { // 验证输入的字符是否为数字
      // alert("请检查小写金额是否正确");
      return ''
    }
    // 字符处理完毕后开始转换，采用前后两部分分别转换
    var part = String(Num).split('.')
    var newchar = ''
    // 小数点前进行转化
    for (var i = part[0].length - 1; i >= 0; i--) {
      if (part[0].length > 10) {
        return ''
        // 若数量超过拾亿单位，提示
      }
      var tmpnewchar = ''
      var perchar = part[0].charAt(i)
      switch (perchar) {
        case '0':
          tmpnewchar = '零' + tmpnewchar
          break
        case '1':
          tmpnewchar = '壹' + tmpnewchar
          break
        case '2':
          tmpnewchar = '贰' + tmpnewchar
          break
        case '3':
          tmpnewchar = '叁' + tmpnewchar
          break
        case '4':
          tmpnewchar = '肆' + tmpnewchar
          break
        case '5':
          tmpnewchar = '伍' + tmpnewchar
          break
        case '6':
          tmpnewchar = '陆' + tmpnewchar
          break
        case '7':
          tmpnewchar = '柒' + tmpnewchar
          break
        case '8':
          tmpnewchar = '捌' + tmpnewchar
          break
        case '9':
          tmpnewchar = '玖' + tmpnewchar
          break
      }
      switch (part[0].length - i - 1) {
        case 0:
          tmpnewchar = tmpnewchar + '元'
          break
        case 1:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '拾'
          break
        case 2:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '佰'
          break
        case 3:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '仟'
          break
        case 4:
          tmpnewchar = tmpnewchar + '万'
          break
        case 5:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '拾'
          break
        case 6:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '佰'
          break
        case 7:
          if (perchar !== 0) tmpnewchar = tmpnewchar + '仟'
          break
        case 8:
          tmpnewchar = tmpnewchar + '亿'
          break
        case 9:
          tmpnewchar = tmpnewchar + '拾'
          break
      }
      newchar = tmpnewchar + newchar
    }
    // 小数点之后进行转化
    if (Num.indexOf('.') !== -1) {
      if (part[1].length > 2) {
        // alert("小数点之后只能保留两位,系统将自动截断");
        part[1] = part[1].substr(0, 2)
      }
      for (i = 0; i < part[1].length; i++) {
        tmpnewchar = ''
        perchar = part[1].charAt(i)
        switch (perchar) {
          case '0':
            tmpnewchar = '零' + tmpnewchar
            break
          case '1':
            tmpnewchar = '壹' + tmpnewchar
            break
          case '2':
            tmpnewchar = '贰' + tmpnewchar
            break
          case '3':
            tmpnewchar = '叁' + tmpnewchar
            break
          case '4':
            tmpnewchar = '肆' + tmpnewchar
            break
          case '5':
            tmpnewchar = '伍' + tmpnewchar
            break
          case '6':
            tmpnewchar = '陆' + tmpnewchar
            break
          case '7':
            tmpnewchar = '柒' + tmpnewchar
            break
          case '8':
            tmpnewchar = '捌' + tmpnewchar
            break
          case '9':
            tmpnewchar = '玖' + tmpnewchar
            break
        }
        if (i === 0) tmpnewchar = tmpnewchar + '角'
        if (i === 1) tmpnewchar = tmpnewchar + '分'
        newchar = newchar + tmpnewchar
      }
    }
    // 替换所有无用汉字
    while (newchar.search('零零') !== -1) { newchar = newchar.replace('零零', '零') }
    newchar = newchar.replace('零亿', '亿')
    newchar = newchar.replace('亿万', '亿')
    newchar = newchar.replace('零万', '万')
    newchar = newchar.replace('零元', '元')
    newchar = newchar.replace('零角', '')
    newchar = newchar.replace('零分', '')
    if (newchar.charAt(newchar.length - 1) === '元') {
      newchar = newchar + '整'
    }
    return newchar
  },

  /**
   * 保留指定位数小数点
   * @param {*} num
   * @param {*} decimal
   * @returns
   */
  numberRetain(num, decimal = 2) {
    num = num.toString()
    const index = num.indexOf('.')
    if (index !== -1) {
      num = num.substring(0, decimal + index + 1)
    } else {
      num = num.substring(0)
    }
    return parseFloat(num).toFixed(decimal)
  },

  /**
   * 数组扁平化
   * @param {*} data
   * @param {*} children
   * @returns
   */
  flatArray(data, children = 'children') {
    const list = []
    const funChild = (childData) => {
      childData.forEach((element) => {
        element[children] && element[children].length > 0
          ? funChild(element[children])
          : '' // 子级递归
        delete element[children]// 删除children 根据需求看是否需要删除
        list.push(element)
      })
    }
    funChild(data)
    return list
  },

  /**
   * 获得所有父级
   * @param {*} arr     扁平化后的数组
   * @param {*} rootId 顶层节点
   * @param {*} goalId 底层节点
   * @param {*} pid 父级属性
   * @param {*} id 主键属性
   * @returns
   */
  getParentPid(arr = [], rootId = 0, goalId, pid = 'pid', id = 'id') {
    var datas = []
    arr.forEach(item => {
      if (item[id] === goalId) {
        if (item[pid] !== rootId) {
          datas.push(item[pid])
          datas.push(...this.getParentPid(arr, rootId, item[pid], pid, id))
        }
      }
    })
    return datas
  },
  /**
   * 数组对象去重
   * @param {*} datas   数组对象
   * @param {*} field   去重字段
   * @returns
   */
  uniqueArray(datas = [], field) {
    const uniqueArray = datas.filter((value, index, self) =>
      index === self.findIndex((t) => (
        t[field] === value[field]
      ))
    )
    return uniqueArray
  },

  /**
   * 生成指定长度的随机数
   * @param {*} len
   * @returns
   */
  randomString(len) {
    len = len || 5
    var t = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
    var a = t.length
    var n = ''
    for (var i = 0; i < len; i++) n += t.charAt(Math.floor(Math.random() * a))
    return n
  },
  /**
     * 生成指定长度的数字
     * @param {*} len   长度
     * @param {*} radix 基数
     * @returns
     */
  randomNumber(len, radix = 10) {
    var chars = '0123456789'.split('')
    var uuid = []
    radix = radix | chars.length
    if (len) {
      for (var i = 0; i < len; i++) {
        uuid[i] = chars[0 | Math.random() * radix]
      }
    }
    return uuid.join('')
  },

  DATE_FMT: 'yyyy-MM-dd',
  DATE_FMT1: 'yyyy年MM月dd日',
  DATE_TIME_FMT: 'yyyy-MM-dd hh:mm:ss',
  DATE_TIME_FMT1: 'yyyy年MM月dd日 hh时mm分ss秒',
  /**
   * 格式化日期
   * @param {*} d 时间戳
   * @param {*} fmt 默认格式 yyyy-MM-dd hh:mm:ss
   */
  formatDate(d, fmt) {
    if (!d) {
      return ''
    }
    const date = new Date(d)
    if (!fmt) {
      const nowDate = new Date()
      var getTime = (nowDate.getTime() - date.getTime()) / 1000
      if (getTime < 60 * 1) {
        return '刚刚'
      } else if (getTime >= 60 * 1 && getTime < 60 * 60) {
        return parseInt(getTime / 60) + '分钟前'
      } else if (getTime >= 3600 && getTime < 3600 * 24) {
        return parseInt(getTime / 3600) + '小时前'
      } else if (getTime >= 3600 * 24 && getTime < 3600 * 24 * 7) {
        return parseInt(getTime / 3600 / 24) + '天前'
      } else {
        fmt = 'yyyy-MM-dd hh:mm:ss'
      }
    }
    const o = {
      'M+': date.getMonth() + 1, // 月份
      'd+': date.getDate(), // 日
      'h+': date.getHours(), // 小时
      'm+': date.getMinutes(), // 分
      's+': date.getSeconds(), // 秒
      'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
      'S': date.getMilliseconds() // 毫秒
    }
    if (/(y+)/.test(fmt)) { fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length)) }
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) { fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length))) }
    }
    return fmt
  },

  /**
   * 获得星期
   * @param {*} day
   * @returns
   */
  getWeek() {
    var date = new Date()
    var week = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天']
    return week[date.getDay()]
  },
  /**
   * 获取当前时间状态
   * @returns
   */
  getTimeState() {
    // 获取当前小时
    const hours = new Date().getHours()
    // 设置默认文字
    let state = ``
    // 判断当前时间段
    if (hours >= 0 && hours <= 11) {
      state = `上午`
    } else if (hours > 11 && hours <= 13) {
      state = `中午`
    } else if (hours > 13 && hours <= 18) {
      state = `下午`
    } else if (hours > 18 && hours <= 24) {
      state = `晚上`
    }
    return state
  },
  /**
   * 获取区间数据
   * @param {*} flag 1 -今天、2 -昨天、3 -本周、4 -本月、5 -本季、6 -本年
   * @returns
   */
  getTimeInterval(flag = 3) {
    let params = []
    const date = new Date()
    if (flag === 1) {
      params = [
        new Date(moment(date).format('YYYY-MM-DD 00:00:00')).getTime(),
        date.getTime()
      ]
    } else if (flag === 2) {
      params = [
        new Date(moment().subtract(1, 'day').format('YYYY-MM-DD 00:00:00')).getTime(),
        new Date(moment().subtract(1, 'day').format('YYYY-MM-DD 23:59:59')).getTime()
      ]
    } else if (flag === 3) {
      const cWeek = moment(date).format('E')
      params = [
        new Date(moment(date).subtract(Number(cWeek) - 1, 'days').format('YYYY-MM-DD 00:00:00')).getTime(),
        date.getTime()
      ]
    } else if (flag === 4) {
      const sDate = moment().add(0, 'month').format('YYYY-MM') + '-01 00:00:00'
      params = [new Date(sDate).getTime(), date.getTime()]
    } else if (flag === 5) {
      const quarter = moment(Number(moment(date).valueOf())).quarter()
      const current = moment(moment(date).quarter(quarter)['startOf']('quarter').valueOf()).format('YYYY-MM-DD 00:00:00')
      params = [new Date(current).getTime(), date.getTime()]
    } else if (flag === 6) {
      const sD = moment(date, 'YYYYMMDD').format('Y') + '-01-01 00:00:00'
      params = [new Date(sD).getTime(), date.getTime()]
    }
    return params
  },
  /**
   * 计算年龄
   * @param {*} birthday 生日 Example：2020-12-09
   * @returns
   */
  calcAge(birthday) {
    if (birthday) {
      birthday = birthday.split('-')
      // 新建日期对象
      const date = new Date()
      // 今天日期，数组，同 birthday
      const today = [date.getFullYear(), date.getMonth() + 1, date.getDate()]
      // 分别计算年月日差值
      const age = []
      for (var index = 0; index < today.length; index++) {
        age[index] = today[index] - birthday[index]
      }
      // 当天数为负数时，月减 1，天数加上月总天数
      if (age[2] < 0) {
        // 简单获取上个月总天数的方法，不会错
        const lastMonth = new Date(today[0], today[1], 0)
        age[1]--
        age[2] += lastMonth.getDate()
      }
      // 当月数为负数时，年减 1，月数加上 12
      if (age[1] < 0) {
        age[0]--
        age[1] += 12
      }
      var result = ''
      if (age[0] > 0) {
        result += age[0] + '岁'
      }
      if (age[1] > 0) {
        result += age[1] + '月'
      }
      if (age[2] > 0) {
        result += age[2] + '天'
      }
      return result
    } else {
      return '-'
    }
  },
  /**
   * 计算百分比
   * @param {*} val1
   * @param {*} val2
   * @returns
   */
  calPercentage(val1, val2) {
    if (val1 === 0 || val2 === 0) {
      return 0
    }
    return (Math.round(val1 / val2 * 10000) / 100.00)
  },
  /**
   * AES加密
   * @param {*} word
   * @param {*} keyStr
   */
  aesEncrypt(word, keyStr) {
    keyStr = keyStr || 'abcdefghijklmn12'
    var key = CryptoJS.enc.Utf8.parse(keyStr)
    var srcs = CryptoJS.enc.Utf8.parse(word)
    var encrypted = CryptoJS.AES.encrypt(srcs, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    return encrypted.toString()
  },
  /**
   * AES解密
   * @param {*} word
   * @param {*} keyStr
   */
  aesDecrypt(word, keyStr) {
    keyStr = keyStr || 'abcdefghijklmn12'
    var key = CryptoJS.enc.Utf8.parse(keyStr)// Latin1 w8m31+Yy/Nw6thPsMpO5fg==
    var decrypt = CryptoJS.AES.decrypt(word, key, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 })
    return CryptoJS.enc.Utf8.stringify(decrypt).toString()
  },
  /**
   * 检测是否全屏
   */
  checkFullScreen() {
    var flag = false
    const clientHeight = document.documentElement.clientHeight || document.body.clientHeight
    flag = screen.height === clientHeight
    return flag
  },
  /**
   * 检查浏览器
   */
  checkBrower() {
    const userAgent = navigator.userAgent
    var isLessIE11 = userAgent.indexOf('compatible') > -1 && userAgent.indexOf('MSIE') > -1
    if (isLessIE11) {
      var str = '你的浏览器版本太low了,已经和时代脱轨了 :('
      var str2 =
                "推荐使用:<a href='http://dcbrowser.sjkj777.cn/' target='_blank' style='color:blue;'>谷歌</a>," +
                "<a href='http://www.firefox.com.cn/' target='_blank' style='color:blue;'>火狐</a>," +
                "<a href='https://browser.360.cn/' target='_blank' style='color:blue;'>360浏览器</a>," +
                '其他双核极速模式'
      document.writeln(
        "<pre style='text-align:center;color:#fff;background-color:#0cc; height:100%;border:0;position:fixed;top:0;left:0;width:100%;z-index:1234'>" +
                    "<h2 style='padding-top:200px;margin:0'><strong>" +
                    str +
                    '<br/></strong></h2><h2>' +
                    str2 +
                    "</h2><h2 style='margin:0'><strong>如果你的使用的是双核浏览器,请切换到极速模式访问<br/></strong></h2></pre>"
      )
      document.execCommand('Stop')
    }
  },
  /**
   * 简单实现防抖方法
   *
   * 防抖(debounce)函数在第一次触发给定的函数时，不立即执行函数，而是给出一个期限值(delay)，比如100ms。
   * 如果100ms内再次执行函数，就重新开始计时，直到计时结束后再真正执行函数。
   * 这样做的好处是如果短时间内大量触发同一事件，只会执行一次函数。
   *
   * @param fn 要防抖的函数
   * @param delay 防抖的毫秒数
   * @returns {Function}
 */
  simpleDebounce: function(fn, delay = 100) {
    let timer = null
    return function() {
      const args = arguments
      if (timer) {
        clearTimeout(timer)
      }
      timer = setTimeout(() => {
        fn.apply(this, args)
      }, delay)
    }
  },
  /**
   * 二维码加密
   * @param {*} type
   * @param {*} code
   * @param {*} name
   * @param {*} expiryTime
   */
  qrEncrypt: function(type, code, name, expiryTime = -1) {
    var qrcodeJSON = {
      type: type,
      code: code,
      name: name,
      expiryTime: expiryTime
    }
    var result = this.aesEncrypt(JSON.stringify(qrcodeJSON))
    return result
  },
  /**
   * 二维码解密
   * @param {*} data
   */
  qrDecrypt: function(data) {
    try {
      var result = JSON.parse(this.aesDecrypt(data))
      if (result.expiryTime === -1) {
        return result
      } else if (result.expiryTime > new Date().getTime()) {
        return result
      } else {
        Message({
          message: '二维码已失效',
          type: 'warning',
          duration: 2 * 1000
        })
        return null
      }
    } catch (err) {
      Message({
        message: '二维码解析错误!',
        type: 'warning',
        duration: 2 * 1000
      })
      return null
    }
  },
  /**
   * 获取当前路径
   * @returns
   */
  getUrl() {
    let url = window.location.protocol + '//' + window.location.host
    var pathName = window.location.pathname
    if (pathName && pathName !== '/') {
      url += pathName
    }
    if (url.endsWith('/')) {
      url = url.substring(0, url.length - 1)
    }
    return url
  },
  isExternal: function(path) {
    return /^(https?:|mailto:|tel:)/.test(path)
  },

  /**
   * @param {HTMLElement} element
   * @param {string} className
  */
  toggleClass(element, className) {
    if (!element || !className) {
      return
    }
    let classString = element.className
    const nameIndex = classString.indexOf(className)
    if (nameIndex === -1) {
      classString += '' + className
    } else {
      classString =
        classString.substr(0, nameIndex) +
        classString.substr(nameIndex + className.length)
    }
    element.className = classString
  }

}

Vue.prototype.T = tools
export default tools
