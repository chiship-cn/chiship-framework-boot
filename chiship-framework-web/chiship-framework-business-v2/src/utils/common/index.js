/**
 * 公共JS   此文件不允许改动，若有改动，联系李剑|18363003321
 * lijian
 * 每个前端，直接复制使用
 */
import Vue from 'vue'
const defaultSettings = require('@/settings.js')
var common = {
  /**
   * 每页默认显示20条数据
   */
  PAGE_LIMIT_DEFAULT: 20,
  /**
   * 默认排序
   */
  SORT_DEFAULT: '-gmtModified',

  /**
   * 格式化地区
   * @param {*} d 地区格式为：ID:省;ID:市;ID:县/区;ID:街道  或  ID:省;ID:市;ID:县/区
   */
  regionFormat(d) {
    if (d) {
      if (d.split(';').length === 5) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '') +
                   regions[3].split(':')[1].replace('-1', '') +
                   regions[4].split(':')[1].replace('-1', '')
      } else if (d.split(';').length === 4) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '') +
                   regions[3].split(':')[1].replace('-1', '')
      } else if (d.split(';').length === 3) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '')
      } else {
        return '地址格式错误'
      }
    } else {
      return '暂无地址'
    }
  },
  /**
   * 获取地区key值
   * @param {*} d 地区格式为：ID:省;ID:市;ID:县/区;ID:街道  或  ID:省;ID:市;ID:县/区
   */
  regionIdFormat(d) {
    if (d) {
      if (d.split(';').length === 5) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0'),
          street: regions[3].split(':')[0].replace('-1', '0'),
          address: regions[4].split(':')[1].replace('-1', '0')
        }
      } else if (d.split(';').length === 4) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0'),
          street: regions[3].split(':')[0].replace('-1', '0')
        }
      } else if (d.split(';').length === 3) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0')
        }
      } else {
        return '地址格式错误'
      }
    } else {
      return '-'
    }
  },
  /**
   * 格式化数据字典组
   * @param {*} d
   */
  dataDictFormat(d) {
    if (d) {
      const datas = d.split(':')
      if (d.split(':').length === 3) {
        return {
          typeCode: datas[0],
          typeName: datas[1],
          colorValue: datas[2]
        }
      } else if (d.split(':').length === 2) {
        return {
          typeCode: datas[0],
          typeName: datas[1],
          colorValue: ''
        }
      } else {
        return d
      }
    } else {
      return '-'
    }
  },

  getFileView(uuid) {
    if (uuid) {
      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
        return uuid
      } else {
        return `${process.env.NODE_ENV === 'development' ? process.env.VUE_APP_BASE_API : window.g.baseURL}fileView/${uuid}?AppId=${defaultSettings.appId}&AppKey=${defaultSettings.appKey}&ProjectsId=${defaultSettings.projectId}`
      }
    } else {
      return '-1'
    }
  },
  handleFilePreview(uuid) {
    window.open(this.getFileView(uuid))
  }
}

Vue.prototype.common = common
export default common
