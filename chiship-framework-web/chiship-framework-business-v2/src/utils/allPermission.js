/**
 * 权限相关控制
 * @author lijian
 * 使用方法如下：
 *    按钮级别控制
 *      1、v-hasPermission="$P.******"
 *      2、:disabled="!$P.hasPermission($P.******)"
 *    菜单级别控制
 *      1、$P.hasMenu('***********')
 *      2、$P.getMenu('***********')  {code: "******",icon: "********",orders: ***,title: "********"}
 */
import store from '@/store'
import Vue from 'vue'
import { constantRouters } from '@/router/router.config'

Vue.directive('hasPermission', {
  inserted: function(el, binding, vnode) {
    if (binding.value) {
      const permissions = store.getters.permission.permissionButtons
      if (!permissions.has(binding.value)) {
        el.parentNode.removeChild(el)
      }
    } else {
      throw new Error('未定义的权限值')
    }
  }
})
function getRouterNames(routers) {
  var routerNames = []
  routers.forEach(route => {
    const tmp = { ...route }
    if (tmp.children) {
      routerNames.push(...getRouterNames(tmp.children))
    }
    if (tmp.name) {
      routerNames.push(tmp.name)
    }
  })
  return routerNames
}
var allPermission = {
  getRouterNames: getRouterNames,
  hasPermission: function(perms) {
    const permissions = store.getters.permission.permissionButtons
    return permissions.has(perms)
  },
  hasMenu: function(menu, valid = false) {
    /**
     * 1.检测常量菜单是否需要验证
     * 2.检测常量菜单
     * 3.检测用户分配菜单
     */
    const menus = store.getters.permission.permissionMenus
    var constantRouterNames = getRouterNames(constantRouters)
    if (constantRouterNames.includes(menu)) {
      if (valid === true) {
        return menus.has(menu)
      }
      return true
    }
    return menus.has(menu)
  },
  getMenu: function(menu) {
    return store.getters.permission.permissionMenus.get(menu)
  },
  // 编辑参数配置
  SYSTEMCONFIG_UPDATE: 'systemConfig:update',
  // 新增参数配置
  SYSTEMCONFIG_SAVE: 'systemConfig:save',
  // 删除参数配置
  SYSTEMCONFIG_REMOVE: 'systemConfig:remove',
  // 编辑字典项
  DATADICTITEM_UPDATE: 'dataDictItem:update',
  // 新增字典项
  DATADICTITEM_SAVE: 'dataDictItem:save',
  // 批量删除典项
  DATADICTITEM_REMOVEALL: 'dataDictItem:removeAll',
  // 删除字典项
  DATADICTITEM_REMOVE: 'dataDictItem:remove',
  // 编辑字典组
  DATADICT_UPDATE: 'dataDict:update',
  // 新增字典组
  DATADICT_SAVE: 'dataDict:save',
  // 删除字典组
  DATADICT_REMOVE: 'dataDict:remove',
  // 导出字典管理
  DATADICT_EXPORT: 'dataDict:export',
  // 新增组织机构
  USER_UNIT_SAVE: 'userUnit:save'

}

Vue.prototype.$P = allPermission
export default allPermission
