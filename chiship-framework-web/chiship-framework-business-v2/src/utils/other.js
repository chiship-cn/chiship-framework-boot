import Vue from 'vue'
import ElementUI from 'element-ui'

/**
 * 系统错误捕获
 */
Vue.config.errorHandler = (err, vm) => {
  console.error(err)
  ElementUI.Notification({
    type: 'error',
    title: 'JS异常',
    dangerouslyUseHTMLString: true,
    message: '<m style="color:red">按F12查看错误信息</m><br>请将错误信息发送给管理员进行处理'
  })
}
/**
 * 图片错误
 */
Vue.prototype.handleImageError = function(e) {
  let defaultImg = require('@/assets/images/default/image.png')
  var dataset = e.target.dataset
  if (dataset.default) {
    defaultImg = require(`@/assets/images/default/${dataset.default}.png`)
  }
  e.target.src = defaultImg
}

Vue.prototype.$FILE_SERVER = (process.env.NODE_ENV === 'development' ? process.env.VUE_APP_BASE_API : window.g.baseURL)
Vue.prototype.formCreaterUploadSuccess = function(e, t) {
  if (e.code === 200) {
    t.url = e.data.uuid
    this.$message({ showClose: true, message: '上传成功!', type: 'success' })
  } else {
    this.$message({ showClose: true, message: e.data == null ? e.message : e.data, type: 'warning' })
  }
}
