/**
 * 三方插件全局注册
 */
import Vue from 'vue'
import { Message } from 'element-ui'

// Echarts图表
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

// 图片预览组件
// import 'viewerjs/dist/viewer.css'
import VueViewer from 'v-viewer'
Vue.use(VueViewer, {
  defaultOptions: {
    zIndex: 1002,
    keyboard: true
  }
})
// 图片懒加载
import VueLazyload from 'vue-lazyload'
Vue.use(VueLazyload, {
  // error: require('../public/images/img_default.png'),
  // loading: require('../public/images/image_loading.png')
})

// 网页出现动画
import lottie from 'lottie-web'
Vue.prototype.$lottie = lottie

// 大文件分片上传、断点续传
import uploader from 'vue-simple-uploader'
Vue.use(uploader)

// 复制  使用this.$copyText()
import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)

// 代码块
import hljs from 'highlight.js'
import 'highlight.js/styles/atelier-cave-light.css'
Vue.directive('highlight', function(el) {
  const blocks = el.querySelectorAll('pre code')
  blocks.forEach((block) => {
    hljs.highlightBlock(block)
  })
})

// 动态form表单
import formCreate from '@form-create/element-ui'
import FcDesigner from '@form-create/designer'
Vue.use(FcDesigner)
Vue.use(formCreate)
Vue.prototype.formCreaterUploadSuccess = (e, t) => {
  if (e.code === 200) {
    t.url = e.data.uuid
  } else {
    Message({
      message: e.data == null ? e.message : e.data,
      type: 'warning',
      duration: 5 * 1000
    })
  }
}
Vue.prototype.formCreaterFormatError = (e, t) => {
  console.log(e, t)
}

// SEO优化META
import VueMeta from 'vue-meta'
Vue.use(VueMeta, {
  // optional pluginOptions
  refreshOnceOnNavigation: true
})

// 组织树
import Vue2OrgTree from 'vue2-org-tree'
import 'vue2-org-tree/dist/style.css'
Vue.use(Vue2OrgTree)

// json编辑器
import vueJsonEditor from 'vue-json-editor'
Vue.component('vueJsonEditor', vueJsonEditor)

// dataV
import dataV from '@jiaminghi/data-view'
Vue.use(dataV)

// 分割面板
import { Splitpanes, Pane } from 'splitpanes'
import 'splitpanes/dist/splitpanes.css'
Vue.component('Splitpanes', Splitpanes)
Vue.component('Pane', Pane)
