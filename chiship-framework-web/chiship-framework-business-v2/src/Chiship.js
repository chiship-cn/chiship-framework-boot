import Vue from 'vue'
import store from '@/store'
import router from '@/router'
import App from '@/App'
import 'normalize.css/normalize.css' // A modern alternative to CSS resets

// ElementUI相关
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n
Vue.use(ElementUI, {
  locale,
  size: 'small'
})

// 全局样式
import '@/styles/index.scss'

// SVG图标
import '@/icons'

// 路由安全守卫
import '@/router/router.security'

// 工具类
import '@/utils'

// 阿里图标
import '@/assets/iconfont/iconfont.css'

const defaultSettings = require('@/settings.js')

console.info(`-------版权信息-------\n\t${defaultSettings.title}，由${defaultSettings.defaultConfig.technicalSupport}提供技术支持！ \n\t当前时间：${Vue.prototype.T.formatDate(new Date().getTime(), 'yyyy-MM-dd hh:mm:ss')}\n`)
Vue.config.productionTip = false
export default class Chiship {
  constructor(options) {
    if (!options) {
      options = {}
    }
    Vue.prototype.$title = options.title || defaultSettings.title
    document.title = options.title || Vue.prototype.$title
    Vue.prototype.$config = { ...defaultSettings.defaultConfig, ...options.config }
  }

  init() {
    // 这里一定要用 require 不能用 import. 因为需要支持自定义配置
    new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
  }
}
