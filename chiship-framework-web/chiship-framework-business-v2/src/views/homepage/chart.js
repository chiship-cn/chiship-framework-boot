import * as echarts from 'echarts'
import tools from '@/utils/common/tools'

/**
 * 统计数据
 */
export const loadTjsjEchart = (datas = [], fn, params = {}) => {
  const myEchart = echarts.init(document.getElementById('tjsjEchart'))
  myEchart.showLoading()

  var xLabel = []
  var yData1 = []
  var yData2 = []
  datas.forEach(item => {
    xLabel.push(item.time)
    yData1.push(item.today)
    yData2.push(item.yesterday)
  })
  var option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        lineStyle: {
          color: '#ddd'
        }
      },
      backgroundColor: 'rgba(255,255,255,1)',
      padding: [5, 10],
      textStyle: {
        color: '#7588E4'
      },
      extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
    },
    grid: {
      left: '0',
      right: '20px',
      top: '15px',
      bottom: '36px',
      containLabel: true
    },
    legend: {
      bottom: '0',
      itemGap: 48,
      data: ['今日', '昨日']
    },
    xAxis: {
      type: 'category',
      data: xLabel,
      boundaryGap: false,
      splitLine: {
        show: false
      },
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#dcdcdc'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 12,
          color: '#333333'
        }
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        lineStyle: {
          color: ['#dcdcdc']
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#dcdcdc'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 12,
          color: '#333333'
        }
      }
    },
    series: [{
      name: '今日',
      type: 'line',
      smooth: true,
      showSymbol: true,
      symbol: 'circle',
      symbolSize: 6,
      data: yData1,
      areaStyle: {
        normal: {
          color: echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(199, 237, 250,0.5)'
          }, {
            offset: 1,
            color: 'rgba(199, 237, 250,0.2)'
          }], false)
        }
      },
      itemStyle: {
        normal: {
          color: '#f7b851'
        }
      },
      lineStyle: {
        normal: {
          width: 2
        }
      }
    }, {
      name: '昨日',
      type: 'line',
      smooth: true,
      showSymbol: true,
      symbol: 'circle',
      symbolSize: 6,
      data: yData2,
      areaStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(216, 244, 247,1)'
          }, {
            offset: 1,
            color: 'rgba(216, 244, 247,1)'
          }], false)
        }
      },
      itemStyle: {
        normal: {
          color: '#58c8da'
        }
      },
      lineStyle: {
        normal: {
          width: 2
        }
      }
    }]
  }
  myEchart.hideLoading()
  myEchart.setOption(option)
  myEchart.on('click', (param) => {
    console.log(param)
    fn(param.name, params)
  })
}

/**
 * 销售渠道
 */
export const loadXsqdEchart = (datas, fn, params) => {
  const myEchart = echarts.init(document.getElementById('xstdEchart'))
  myEchart.showLoading()
  var value1 = datas.value1
  var value2 = datas.value2
  var option = {
    title: {
      text: tools.calPercentage(value1, value1 + value2) + '%',
      x: 'center',
      y: 'center',
      textStyle: {
        fontWeight: 'normal',
        color: 'rgba(0, 0, 0, 0.9)',
        fontSize: '25'
      },
      subtext: '线上渠道占比'
    },
    tooltip: {
      formatter: function(params) {
        return params.name + '：' + params.percent + ' %'
      }
    },
    grid: {
      top: '0',
      right: '0'
    },
    legend: {
      show: true,
      itemGap: 12,
      bottom: 0,
      data: ['线上', '门店']
    },

    series: [
      {
        name: 'circle',
        type: 'pie',
        clockWise: true,
        radius: ['60%', '76%'],
        itemStyle: {
          normal: {
            label: {
              show: false
            },
            labelLine: {
              show: false
            }
          }
        },
        hoverAnimation: false,
        data: [
          {
            value: value1,
            name: '线上',
            itemStyle: {
              normal: {
                color: {
                  // 颜色渐变
                  colorStops: [
                    {
                      offset: 0,
                      color: '#4FADFD' // 0% 处的颜色
                    },
                    {
                      offset: 1,
                      color: '#28E8FA' // 100% 处的颜色1
                    }
                  ]
                },
                label: {
                  show: false
                },
                labelLine: {
                  show: false
                }
              }
            }
          },
          {
            name: '门店',
            value: value2,
            itemStyle: {
              normal: {
                color: '#E1E8EE'
              }
            }
          }
        ]
      }
    ]
  }

  myEchart.hideLoading()
  myEchart.setOption(option)
  myEchart.on('click', (param) => {
    console.log(param)
    fn(param.name, params)
  })
}

/**
 * 出入库概览
 */
export const loadCrkglEchart = (datas = [], fn, params = {}) => {
  const myEchart = echarts.init(document.getElementById('crkglEchart'))
  myEchart.showLoading()

  var xLabel = []
  var yData1 = []
  var yData2 = []
  datas.forEach(item => {
    xLabel.push(item.time)
    yData1.push(item.today)
    yData2.push(item.yesterday)
  })
  var option = {
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        lineStyle: {
          color: '#ddd'
        }
      },
      backgroundColor: 'rgba(255,255,255,1)',
      padding: [5, 10],
      textStyle: {
        color: '#7588E4'
      },
      extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
    },
    grid: {
      left: '0',
      right: '20px',
      top: '15px',
      bottom: '36px',
      containLabel: true
    },
    legend: {
      bottom: '0',
      itemGap: 48,
      data: ['今日', '昨日']
    },
    xAxis: {
      type: 'category',
      data: xLabel,
      axisTick: {
        alignWithLabel: true, // 刻度线和标签对齐
        interval: 0 // 间隔
      },
      splitLine: {
        show: false // 网格线开关
      },
      axisLine: {
        lineStyle: {
          color: '#dcdcdc'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 12,
          color: '#333333'
        }
      }
    },
    yAxis: {
      type: 'value',
      splitLine: {
        lineStyle: {
          color: ['#dcdcdc']
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#dcdcdc'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          fontSize: 12,
          color: '#333333'
        }
      }
    },
    series: [{
      name: '今日',
      type: 'bar',
      smooth: true,
      showSymbol: true,
      symbol: 'circle',
      symbolSize: 6,
      data: yData1,
      areaStyle: {
        normal: {
          color: echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(199, 237, 250,0.5)'
          }, {
            offset: 1,
            color: 'rgba(199, 237, 250,0.2)'
          }], false)
        }
      },
      itemStyle: {
        normal: {
          color: '#0052D9'
        }
      },
      lineStyle: {
        normal: {
          width: 2
        }
      }
    }, {
      name: '昨日',
      type: 'bar',
      smooth: true,
      showSymbol: true,
      symbol: 'circle',
      symbolSize: 6,
      data: yData2,
      areaStyle: {
        normal: {
          color: echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(216, 244, 247,1)'
          }, {
            offset: 1,
            color: 'rgba(216, 244, 247,1)'
          }], false)
        }
      },
      itemStyle: {
        normal: {
          color: '#63E29C'
        }
      },
      lineStyle: {
        normal: {
          width: 2
        }
      }
    }]
  }
  myEchart.hideLoading()
  myEchart.setOption(option)
  myEchart.on('click', (param) => {
    console.log(param)
    fn(param.name, params)
  })
}

