export default {
  props: {
    // 子部门或组织架构传递
    treeNumber: {
      type: String,
      default: null
    },
    // 本部门传递
    orgId: {
      type: String,
      default: null
    }
  },
  data() {
    return {
      searchValue: null,
      treeLoading: false,
      treeDatas: [],
      defaultTreeProps: {
        children: 'children',
        label: 'name'
      }
    }
  },
  watch: {
    searchValue(val) {
      this.$refs.tree.filter(val)
    }
  },
  methods: {
    /* 节点过滤函数 */
    onTreeFilter(value, data) {
      if (!value) return true
      var label = data.name + '(' + data.mobile + ')'
      return data[this.defaultTreeProps.filter] ? label.indexOf(value) !== -1 || data[this.defaultTreeProps.filter].indexOf(value.toUpperCase()) !== -1 : label.indexOf(value) !== -1
    },
    setSelectNode(datas) {
      var ids = []
      datas.forEach(item => {
        ids.push(item.id)
      })
      if (this.$refs.tree) {
        this.$refs.tree.setCheckedKeys(ids)
      }
    },
    handleCurrentChange() {
      var checkNodes = this.$refs.tree.getCheckedNodes(false, true)
      var selectUsers = []
      checkNodes.filter(item => {
        return item.type === 'user'
      }).forEach(item => {
        selectUsers.push({
          id: item.id,
          name: item.name,
          mobile: item.mobile,
          avatar: item.avatar
        })
      })
      this.$emit('selectUser', selectUsers)
    }
  }
}
