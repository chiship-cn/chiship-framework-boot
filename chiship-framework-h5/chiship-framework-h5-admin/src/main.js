import Vue from 'vue'

import 'normalize.css/normalize.css'

import Vant from 'vant'
import { Notify } from 'vant'
import 'vant/lib/index.css'
Vue.use(Vant)

import '@/styles/index.scss'

import App from './App'
import store from './store'
import router from './router'

import '@/icons'
import '@/permission'
import '@/utils'

import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts

/**
 * 公共弹框
 */
Vue.prototype.msgSuccess = function(msg) {
  this.$toast(msg)
}
Vue.prototype.msgError = function(msg) {
  this.$toast(msg)
}
Vue.prototype.msgWarning = function(msg) {
  this.$toast(msg)
}
/**
 * 图片错误
 */
Vue.prototype.handleImageError = function(e) {
  let defaultImg = require('@/assets/images/default/image.png')
  var dataset = e.target.dataset
  if (dataset.default) {
    defaultImg = require(`@/assets/images/default/${dataset.default}.png`)
  }
  e.target.src = defaultImg
}
/**
 * 系统错误捕获
 */
Vue.config.errorHandler = (err, vm) => {
  console.error(err)
  Notify({
    type: 'danger',
    message: '按F12查看错误信息,请将错误信息发送给管理员进行处理'
  })
}

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
