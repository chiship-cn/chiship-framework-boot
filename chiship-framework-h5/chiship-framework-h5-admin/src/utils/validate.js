import Vue from 'vue'
import common from './index'
import regular from './regular'
import tools from './tools'

var validate = {
  // 校验姓名
  validateRealName(rule, value, callback) {
    const reg = regular.realNameRegular
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('请输入真实姓名'))
    } else {
      callback()
    }
  },
  // 不能包含特殊字符
  checkInput(rule, value, callback) {
    var pattern = regular.specialCharacters
    if (!value) {
      callback()
    } else if (pattern.test(value)) {
      callback(new Error('禁止输入特殊字符及空格'))
    } else if (value.indexOf(' ') !== -1) {
      callback(new Error('禁止输入特殊字符及空格'))
    } else {
      callback()
    }
  },
  // 只接受字母或数字
  validateEngOrNum(rule, value, callback) {
    const reg = /^([a-zA-Z0-9]+)$/
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('只能输入字母或数字'))
    } else {
      callback()
    }
  },
  // 只接受数字
  validateNumber(rule, value, callback) {
    const reg = /^([0-9]+)$/
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('只能输入数字'))
    } else {
      callback()
    }
  },
  // 只接受字母
  validateEnglish(rule, value, callback) {
    const reg = /^([a-zA-Z]+)$/
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('只能输入字母'))
    } else {
      callback()
    }
  },

  // 校验手机号
  validateMobile(rule, value, callback) {
    const reg = regular.mobile
    console.log(!reg.test(value))
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('请输入正确的11位手机号码'))
    } else {
      callback()
    }
  },
  // 校验邮箱
  validateEmail(rule, value, callback) {
    const reg = regular.email
    if (!value) {
      callback()
    } else if (!reg.test(value)) {
      callback(new Error('请输入有效的邮箱'))
    } else {
      callback()
    }
  },
  // 校验身份证号
  validateIDCard(rule, value, callback) {
    if (!value) {
      callback()
    } else if (!tools.isCardID(value)) {
      callback('身份证不合法,请重写输入')
    } else {
      callback()
    }
  },
  // 校验座机号
  validatePhone(rule, value, callback) {
    if (!value) {
      callback()
    } else if (!regular.phone.test(value)) {
      callback(new Error('请输入有效的号码,如:0451-82803983'))
    } else {
      callback()
    }
  },
  // 校验IP地址
  validateIP(rule, value, callback) {
    if (!value) {
      callback()
    } else {
      var ips = value.split(';')
      for (let i = 0; i < ips.length; i++) {
        if (!regular.ip.test(ips[i])) {
          callback(new Error('请输入正确的IP地址,若输入多个IP地址用英文分号隔开'))
        } else {
          callback()
        }
      }
    }
  },
  // 校验密码规则
  validatePassword(rule, value, callback) {
    const reg = /^(?=.*[a-zA-Z])(?=.*\d).{8,16}$/
    if (!reg.test(value)) {
      callback(new Error('请输入8-16位密码,建议至少包含数字、大小写字母两种类型!'))
    }
    var level = 0
    // 判断这个字符串中有没有数字
    if (common._hasNumber(value)) {
      level++
    }
    // 判断字符串中有没有字母
    if (common._hasBigChar(value) && common._hasSmallChar(value)) {
      level++
    }
    if (level >= 1) {
      callback()
    } else {
      callback(new Error('密码强度不够'))
    }
  }
}
Vue.prototype.commonValidate = validate
export default validate
