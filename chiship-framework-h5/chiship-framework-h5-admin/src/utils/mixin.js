import Vue from 'vue'
import { mapGetters } from 'vuex'

const mixin = {
  data() {
    return {
    }
  },
  computed: {
    ...mapGetters([
      'userInfo',
      'roles'
    ])
  },
  methods: {
  }
}
Vue.mixin(mixin)
export default mixin
