import Vue from 'vue'
import tools from './tools'
/**
 * 公共过滤器   此文件不允许改动，若有改动，联系李剑|18363003321
 * lijian
 * 每个前端，直接复制使用
 */

/**
 * 隐藏手机号码
 * @param val {Number, String} 转换的字符串对象
 * @param retain {Number} 保留位数
 * @return {String}
 */
Vue.filter('privatePhone', (val, retain = 4) => {
  if (String(val).length !== 11 || retain === 0) return val
  const phone = String(val)
  const digit = 11 - 3 - retain
  const reg = new RegExp(`^(\\d{3})\\d{${digit}}(\\d{${retain}})$`)
  return phone.replace(reg, `$1${'*'.repeat(digit)}$2`)
})

/**
 * 隐藏姓名
 * @param val {String} 转换的字符串对象
 * @return {String}
 */
Vue.filter('privateRealName', (val) => {
  if (!val) {
    return ''
  } else if (val.length === 1) {
    return val
  } else {
    return val.replace(/(?<=.)./g, '*')
  }
})

/**
 * 格式化时间
 * @param {*} val
 * @param {*} fmt
 */
Vue.filter('formatDate', (val, fmt) => {
  return tools.formatDate(val, fmt)
})

/**
 * 格式化运行时间
 */
Vue.filter('formatRunTime', (ms) => {
  if (ms) {
    if (ms < 1000) { return '<1秒' }
    const sec_a = Math.round(ms / 1000)
    const min_a = Math.floor(sec_a / 60)
    const sec = sec_a - min_a * 60
    const hour_a = Math.floor(min_a / 60)
    const min = min_a - hour_a * 60
    const d = Math.floor(hour_a / 24)
    const hour = hour_a - d * 24

    let str = ''
    if (sec) { str = sec + '秒' + str }
    if (!min_a) { return str }

    if (min) { str = min + '分钟' + str }
    if (!hour_a) { return str }

    if (hour) { str = hour + '小时' + str }
    if (!d) { return str }

    return d + '天' + str
  }
  return '0秒'
})

/**
 * 格式化文件大小
 */
Vue.filter('formatFileSize', (val) => {
  if (!val) {
    return '-'
  } else if (typeof (val) !== 'number') {
    return '-'
  } else if (val < 1024) {
    return val + 'B'
  } else if (val < 1024 * 1024) {
    return (val / 1024).toFixed(2) + 'KB'
  } else if (val < 1024 * 1024 * 1024) {
    return (val / 1024 / 1024).toFixed(2) + 'M'
  } else {
    return (val / 1024 / 1024 / 1024).toFixed(2) + 'G'
  }
})
