/**
 * 公共组件
 */
import Vue from 'vue'

// 顶部导航
Vue.component('NavBar', () => import('@/components/custom/NavBar'))

// 数据字典选择器
Vue.component('DataDictSelect', () => import('@/components/custom/DataDictSelect'))
