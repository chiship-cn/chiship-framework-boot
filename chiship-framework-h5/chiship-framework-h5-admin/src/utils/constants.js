import Vue from 'vue'
var constants = {
  PROJECTS_ID: '1883442361',
  APP_ID: '638638776006557696',
  APP_KEY: '61cada1e12324f79bf404f4b3cc6d9d7',
  getFileView(uuid) {
    if (uuid) {
      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
        return uuid
      } else {
        return `${process.env.NODE_ENV === 'development' ? process.env.VUE_APP_BASE_API : window.g.baseURL}fileView/${uuid}?AppId=${this.APP_ID}&AppKey=${this.APP_KEY}&ProjectsId=${this.PROJECTS_ID}`
      }
    } else {
      return '-1'
    }
  }
}

Vue.prototype.constants = constants
export default constants
