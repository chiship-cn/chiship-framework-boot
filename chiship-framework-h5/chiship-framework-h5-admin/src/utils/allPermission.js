import Vue from 'vue'
import store from '@/store'
const allPermission = {
  hasMenu(code) {
    const menus = store.getters.permission.buttons
    return menus.has(code)
  },
  hasButton(code) {
    const buttons = store.getters.permission.buttons
    return buttons.has(code)
  },
  USER_ADD: 'platformUser.add'
}
Vue.prototype.$P = allPermission
export default allPermission
