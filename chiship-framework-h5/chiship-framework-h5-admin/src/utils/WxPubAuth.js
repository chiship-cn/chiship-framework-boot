import request from '@/utils/request'
import tools from '@/utils/tools'

import { Toast } from 'vant'

export default {
  doWxPubAuth(query) {
    if (!tools.isWeiXin()) {
      Toast('请使用微信打开！')
      return
    }
    if (!query) {
      query = ''
    }
    request.post('wxPub/authorize', {
      appId: window.g.wxPubAppId,
      oauthChannel: 'OAUTH_CHANNEL_WX_PUB',
      callback: encodeURIComponent(window.location.href) + query
    }).then(response => {
      window.location.href = response
    }).catch(_ => {
      Toast('微信授权失败！')
    })
  }
}
