import request from '@/utils/request'
const UpmsPermissionService = {
  state: {},

  mutations: {},

  actions: {
    // 菜单树
    upmsMenuTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPermission/menuTree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 权限树
    upmsPermissionTree({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPermission/permissionTree', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 菜单保存
     */
    upmsMenuSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/saveMenu', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 菜单编辑
     */
    upmsMenuUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/updateMenu/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
    * 根据主键获得权限
    */
    upmsPermissionGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsPermission/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得权限详情
    */
    upmsPermissionGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsPermission/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 权限保存
     */
    upmsPermissionSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限更新
     */
    upmsPermissionUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限删除
     */
    upmsPermissionRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限根据指定字段校验数据是否存在
     */
    upmsPermissionValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsPermission/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限导出
     */
    upmsPermissionExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.exportData('upmsPermission/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限实时导出进度
     */
    upmsPermissionGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.exportData('upmsPermission/getExportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限导出
     */
    upmsPermissionImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 权限实时导入进度
     */
    upmsPermissionGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsPermission/getImportProcessStatus', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsPermissionService
