import request from '@/utils/request'
const UpmsRoleService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 角色分页
     */
    upmsRolePage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsRole/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色列表
     */
    upmsRoleList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsRole/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得角色
    */
    upmsRoleGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsRole/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 角色保存
     */
    upmsRoleSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsRole/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色更新
     */
    upmsRoleUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsRole/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色删除
     */
    upmsRoleRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsRole/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色根据指定字段校验数据是否存在
     */
    upmsRoleValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsRole/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 根据角色获取权限
     */
    getPermissionByRoleId({ commit }, roleId) {
      return new Promise((resolve, reject) => {
        request.get('upmsRole/getPermissionByRoleId', { roleId: roleId }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色权限分配保存
     */
    rolePermissionSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsRole/rolePermissionSave/' + params.roleId, params.permissionIds).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 角色权限分配取消
     */
    rolePermissionRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsRole/rolePermissionRemove/' + params.roleId, params.permissionIds).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default UpmsRoleService
