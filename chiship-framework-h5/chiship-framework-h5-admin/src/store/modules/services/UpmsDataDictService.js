import request from '@/utils/request'
const UpmsDataDictService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 字典组树
     */
    listDataDict({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('dataDict/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 字典组保存
     */
    dataDictSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典组更新
     */
    dataDictUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典组删除
     */
    dataDictRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典组详情
     */
    dataDictGet({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('dataDict/getById/' + id, {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 缓存数据字典组
    cacheDataDict({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('dataDict/cacheDataDict', {}).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典分页
     */
    dataDictItemPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('dataDict/item/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典保存
     */
    dataDictItemSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/item/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 字典更新
     */
    dataDictItemUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/item/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 字典删除
     */
    dataDictItemRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('dataDict/item/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 根据字典项加载对应的字典值
     */
    findDataDictItemByCode({ commit }, code) {
      return new Promise((resolve, reject) => {
        request.get('dataDict/findByCodeFromCache', { dataDictCode: code }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsDataDictService
