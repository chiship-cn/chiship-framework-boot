import request from '@/utils/request'
const UtilServices = {
  actions: {
    // 获取文本验证码
    getCaptchaCodeText({ commit }) {
      return new Promise((resolve, reject) => {
        request.get('captcha/getCaptchaCodeText').then((res) => {
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    // 获取图形验证码
    getCaptchaCode({ commit }) {
      return new Promise((resolve, reject) => {
        request.pictureStream('captcha/getCaptchaCode').then((res) => {
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    // Base64图片上传
    fileBase64Upload({ commit }, base64) {
      return new Promise((resolve, reject) => {
        request.post('file/base64Upload', base64).then((res) => {
          resolve(res)
        }).catch((err) => {
          reject(err)
        })
      })
    },
    // 文件下载
    downLoadFile({ commit }, uuid) {
      return new Promise((resolve, reject) => {
        request.downloadFile('fileView/download/' + uuid, { download: true }).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导出
    exportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('export/data/' + params.className, params).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 获得实时异步导出进度
    getExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('export/getProcessStatus/' + params.className, params.taskId).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导入
    importData({ commit }, formData) {
      console.log(formData)
      return new Promise((resolve, reject) => {
        request.post('import/data/' + formData.get('className'), formData).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    },
    // 导入数据实时进度
    getImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('import/getProcessStatus/' + params.className, params.taskId).then(response => {
          const data = response
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}
export default UtilServices

