import request from '@/utils/request'
const BusinessOrderHeaderService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 订单主表分页
     */
    businessOrderHeaderPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表列表
     */
    businessOrderHeaderList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单主表
    */
    businessOrderHeaderGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得订单主表详情
    */
    businessOrderHeaderGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 订单主表保存
     */
    businessOrderHeaderSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表更新
     */
    businessOrderHeaderUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表删除
     */
    businessOrderHeaderRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表根据指定字段校验数据是否存在
     */
    businessOrderHeaderValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('businessOrderHeader/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表导出
     */
    businessOrderHeaderExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderHeader/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表实时导出进度
     */
    businessOrderHeaderGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('businessOrderHeader/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表导入
     */
    businessOrderHeaderImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单主表实时导入进度
     */
    businessOrderHeaderGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 创建充值订单
    */
    businessCreateRechargeOrde({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('businessOrderHeader/createRechargeOrder', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 根据订单号查询订单状态
     */
    orderStatusByOrderId({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('pay/orderStatusByOrderId', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * H5支付
     */
    h5Pay({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('pay/h5Pay', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 微信公众号预支付
     */
    doPrepay({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('pay/prepay', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款
     */
    refundPay({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('pay/refund', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 订单对账
     */
    orderReconciliation({ commit }, orderNo) {
      return new Promise((resolve, reject) => {
        request.get('pay/reconciliation/order', { orderNo: orderNo }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 退款对账
     */
    refundReconciliation({ commit }, refundNo) {
      return new Promise((resolve, reject) => {
        request.get('pay/reconciliation/refund', { refundNo: refundNo }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default BusinessOrderHeaderService
