import request from '@/utils/request'
const UpmsOrganizationService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 组织树状表格
     */
    upmsOrganizationTreeTable({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/treeTable', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织分页
     */
    upmsOrganizationPage({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/page', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织列表
     */
    upmsOrganizationList({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/list', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得组织
    */
    upmsOrganizationGetById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/getById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
    * 根据主键获得组织详情
    */
    upmsOrganizationGetDetailsById({ commit }, id) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/getDetailsById/' + id).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },

    /**
     * 组织保存
     */
    upmsOrganizationSave({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsOrganization/save', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织更新
     */
    upmsOrganizationUpdate({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsOrganization/update/' + params.id, params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织删除
     */
    upmsOrganizationRemove({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsOrganization/remove', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织根据指定字段校验数据是否存在
     */
    upmsOrganizationValidateExistByField({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.get('upmsOrganization/validateExistByField', { id: params.id, field: params.field, value: params.value }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织导出
     */
    upmsOrganizationExportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsOrganization/exportData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织实时导出进度
     */
    upmsOrganizationGetExportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.downloadFile('upmsOrganization/getExportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织导入
     */
    upmsOrganizationImportData({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsOrganization/importData', params).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    },
    /**
     * 组织实时导入进度
     */
    upmsOrganizationGetImportProcessStatus({ commit }, params) {
      return new Promise((resolve, reject) => {
        request.post('upmsOrganization/getImportProcessStatus', params.taskId).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default UpmsOrganizationService
