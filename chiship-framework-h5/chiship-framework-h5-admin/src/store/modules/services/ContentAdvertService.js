import request from '@/utils/request'
const ContentAdvertService = {
  state: {},

  mutations: {},

  actions: {
    /**
     * 根据编码获取广告
     */
    contentAdvertGetByCode({ commit }, code) {
      return new Promise((resolve, reject) => {
        request.get('contentAdvert/getByCode', { code: code }).then(response => {
          resolve(response)
        }).catch(error => {
          reject(error)
        })
      })
    }

  }
}

export default ContentAdvertService
