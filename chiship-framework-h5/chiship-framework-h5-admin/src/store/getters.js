const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.sso.token,
  userInfo: state => state.sso.userInfo,
  roles: state => state.sso.roles,
  permission_routes: state => state.permission.routes,
  permission: state => state.sso.permission
}
export default getters
