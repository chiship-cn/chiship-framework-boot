import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'
import EmptyLayout from '@/layout/EmptyLayout'

/**
 * 动态路由
 */
export const asyncRoutes = [{
  path: '/login',
  name: 'login',
  component: () => import('@/views/login'),
  hidden: true
}, {
  path: '/404',
  name: '404',
  component: () => import('@/views/404'),
  hidden: true
}, {
  path: '/',
  name: 'message',
  component: Layout,
  redirect: '/message',
  meta: { title: '消息', icon: 'chat-o' },
  children: [{
    path: 'message',
    name: 'messageIndex',
    component: () => import('@/views/message/index'),
    meta: { title: '消息', activeMenu: 'message' }
  }]
}, {
  path: '/application',
  component: Layout,
  redirect: '/application',
  name: 'application',
  meta: { title: '应用', icon: 'apps-o' },
  children: [{
    path: '',
    name: 'applicationIndex',
    component: () => import('@/views/application/index'),
    meta: { title: '应用', activeMenu: 'application' }
  }]
}, {
  path: '/addressBook',
  component: Layout,
  redirect: '/addressBook',
  name: 'addressBook',
  meta: { title: '通讯录', icon: 'friends-o' },
  children: [{
    path: '',
    name: 'addressBookIndex',
    component: () => import('@/views/mailList/index'),
    meta: { title: '通讯录', activeMenu: 'addressBook' }
  }, {
    path: 'details/:id',
    name: 'addressBookDetails',
    component: () => import('@/views/mailList/details'),
    meta: { title: '通讯录详情', activeMenu: 'addressBook' }
  }]
}, {
  path: '/my',
  component: Layout,
  redirect: '/my',
  name: 'my',
  meta: { title: '个人中心', icon: 'manager-o' },
  children: [{
    path: '',
    name: 'myIndex',
    component: () => import('@/views/my/index'),
    meta: { title: '个人中心', activeMenu: 'my' }
  }, {
    path: '/pay',
    component: EmptyLayout,
    redirect: '/pay/cashier',
    name: 'pay',
    hidden: true,
    meta: { title: '支付系统', icon: 'bill-o' },
    children: [{
      path: 'cashier',
      name: 'payCashier',
      component: () => import('@/views/pay/cashier'),
      meta: { title: '收银台', activeMenu: 'pay' }
    }, {
      path: 'result',
      name: 'payResult',
      hidden: true,
      component: () => import('@/views/pay/result'),
      meta: { title: '支付结果', activeMenu: 'pay' }
    }]
  }]
}/*, {
  path: '*',
  redirect: '/404',
  hidden: true
}*/]

const createRouter = () => new Router({
  // mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: asyncRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
