import http from '@/common/service/service.js'
import constants from '@/common/util/constants'
import RSAEncrypt from '@/common/util/RSAEncrypt'

const UserServices = {
	state: {},

	mutations: {},

	actions: {
		userPageV2({commit },params) {
			console.log(params)
			return new Promise((resolve, reject) => {
				http.g('user/pageV2',params).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		userList({commit }) {
			return new Promise((resolve, reject) => {
				http.g('user/list').then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		getUserDetailsById({commit },id) {
			return new Promise((resolve, reject) => {
				http.g('user/getDetailsById/'+id).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
	}

}

export default UserServices