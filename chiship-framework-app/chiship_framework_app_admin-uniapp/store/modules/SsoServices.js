import http from '@/common/service/service.js'
import constants from '@/common/util/constants'
import RSAEncrypt from '@/common/util/RSAEncrypt'

const UserServices = {
	state: {},

	mutations: {},

	actions: {
		// 登陆
		login({
			commit
		}, params) {
			console.log(http)
			var userInfo = Object.assign({}, params)
			userInfo.password = RSAEncrypt.encrypt(userInfo.password)
			userInfo.username = RSAEncrypt.encrypt(userInfo.username)
			return new Promise((resolve, reject) => {
				http.p('sso/basicLogin', {
					username: userInfo.username,
					password: userInfo.password,
				}).then(response => {
					uni.setStorageSync(constants.ACCESS_TOKEN, response);
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		getUserInfo({
			commit
		}) {
			return new Promise((resolve, reject) => {
				http.g('sso/getInfo').then(response => {
					uni.setStorageSync(constants.USER_INFO, response);
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		scanLogin({
			commit
		}, qrContent) {
			return new Promise((resolve, reject) => {
				http.p('sso/doQRCodeScan', qrContent).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},
		getPermission({
			commit
		}) {
			return new Promise((resolve, reject) => {
				http.g('sso/getPermission').then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		},

		scanLogin({
			commit
		}, qrContent) {
			return new Promise((resolve, reject) => {
				http.p('sso/doQRCodeScan', qrContent).then(response => {
					resolve(response)
				}).catch(error => {
					reject(error)
				})
			})
		}, // 退出
		logout({
			commit,
			state
		}) {
			return new Promise((resolve, reject) => {
				http.p('sso/logout').then(() => {
					uni.removeStorageSync(constants.ACCESS_TOKEN);
					uni.removeStorageSync(constants.USER_INFO);
					uni.navigateTo({
						url: "/pages/login/index"
					})
					resolve()
				}).catch(error => {
					reject(error)
				})
			})
		},
	}

}

export default UserServices