import App from './App.vue'
import store from './store'

// #ifndef VUE3
import Vue from 'vue'


Vue.prototype.$store = store;

import '@/common/util'

import uView from '@/uni_modules/uview-ui'
Vue.use(uView)


Vue.config.productionTip = false
App.mpType = 'app'
console.log("uViewUI:https://uviewui.com/")
console.log("uni-UI:https://zh.uniapp.dcloud.io/component/uniui/uni-ui.html")
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		store,
		app
	}
}
// #endif
