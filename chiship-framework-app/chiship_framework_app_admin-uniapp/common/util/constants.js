
import configService from '@/common/service/config.service.js'

var constants = {
	PROJECTS_ID: '1883442361',
	APP_ID: '638638776006557696',
	APP_KEY: '61cada1e12324f79bf404f4b3cc6d9d7',
	APPLICATION_NAME: 'chiship-upms',

	ACCESS_TOKEN: 'Access-Token',
	USER_INFO: 'USER-INFO',
	

	ORGANIZATION_CATEGORY1: '01:总站',
	/**
	 * 乡镇（街道）级
	 */
	ORGANIZATION_LEVEL5: 'organizationType:05',
	/**
	 * 县（市、区、旗）级
	 */
	ORGANIZATION_LEVEL4: 'organizationType:04',
	/**
	 * 市（地、州、盟）级
	 */
	ORGANIZATION_LEVEL3: 'organizationType:03',

	// 帮助文档封面
	FILE_CATALOG_HELP_LOGO: '608451054881280000',
	// 帮助文档附件
	FILE_CATALOG_HELP_ATTACHMENT: '608451197659582464',
	// 帮助文档正文
	FILE_CATALOG_HELP_CONTENT: '608451096228728832', // true
	// FILE_CATALOG_HELP_CONTENT: '608166049571475456', // tmp
	// 通知公告
	FILE_CATALOG_NOTICE_CONTENT: '608451197659582464',

	// 单位logo
	FILE_CATALOG_UNIT_LOGO: '608174563840823296',

	// 用户头像
	FILE_CATALOG_USER_AVATAR: '608176065380683776',

	// 微信配置
	FILE_CATALOG_WECHAT_CONFIG: '587872197573046272',

	// 工作流
	FILE_CATALOG_WORK_FLOW: '630621892690272256',
	getFileView(uuid) {
	    if (uuid) {
	      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
	        return uuid
	      } else {
	        return `${configService.BASE_URL}fileView/${uuid}?AppId=${constants.APP_ID}&AppKey=${constants.APP_KEY}&ProjectsId=${constants.PROJECTS_ID}`
	      }
	    } else {
	      return '-1'
	    }
	  },

}
export default constants
