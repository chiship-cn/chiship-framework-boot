import JsEncrypt from '../../node_modules/jsencrypt/bin/jsencrypt.js'
import tip from './tip.js'
import defaultSettings from './settings.js'
var RSAEncrypt = {
	// 签名算法
	ALGORITHM: 'SHA256withRSA',
	encrypt: function(data) {
		const jse = new JsEncrypt()
		jse.setPublicKey(defaultSettings.publicKey)
		if (typeof(data) === 'object') {
			data = JSON.stringify(data)
		}
		const result = jse.encrypt(data)
		if (typeof(result) === 'boolean') {
			if (!result) {
				tip.toast('公钥不正确,请核对!')
				return null
			}
		}
		return result
	},
	decrypt: function(data) {
		if (typeof(data) !== 'string') {
			tip.toast('加密数据不正确,请核对!')
			return null
		}
		const jse = new JsEncrypt()
		jse.setPrivateKey(defaultSettings.privateKey)
		if (typeof(data) === 'object') {
			data = JSON.stringify(data)
		}
		const result = jse.decrypt(data)
		return result
	},
}
export default RSAEncrypt