import Vue from 'vue'
const modulesFiles = require.context('.', true, /\.js$/)
modulesFiles.keys().reduce((modules, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/(.*)\.\w+$/, '$1')
  const value = modulesFiles(modulePath)
  modules[moduleName] = value.default
  if(moduleName.indexOf('/')===-1){
	  console.log('注入名称：$'+moduleName)
	  Vue.prototype['$'+moduleName]=value.default
  }
  return modules
}, {})
