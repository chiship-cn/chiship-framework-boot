/**
 * 公共JS   此文件不允许改动，若有改动，联系李剑|18363003321
 * lijian
 * 每个前端，直接复制使用
 */
import Vue from 'vue'
var common = {
  /**
   * 每页默认显示20条数据
   */
  PAGE_LIMIT_DEFAULT: 20,
  /**
   * 默认排序
   */
  SORT_DEFAULT: '-gmtModified',
  DATE_FMT: 'yyyy-MM-dd',
  DATE_FMT1: 'yyyy年MM月dd日',
  DATE_TIME_FMT: 'yyyy-MM-dd hh:mm:ss',
  DATE_TIME_FMT1: 'yyyy年MM月dd日 hh时mm分ss秒',
  /**
   * 格式化日期
   * @param {*} d 时间戳
   * @param {*} fmt 默认格式 yyyy-MM-dd hh:mm:ss
   */
  formatDate(d, fmt) {
    if (!d) {
      return ''
    }
    const date = new Date(d)
    if (!fmt) {
      const nowDate = new Date()
      var getTime = (nowDate.getTime() - date.getTime()) / 1000
      if (getTime < 60 * 1) {
        return '刚刚'
      } else if (getTime >= 60 * 1 && getTime < 60 * 60) {
        return parseInt(getTime / 60) + '分钟前'
      } else if (getTime >= 3600 && getTime < 3600 * 24) {
        return parseInt(getTime / 3600) + '小时前'
      } else if (getTime >= 3600 * 24 && getTime < 3600 * 24 * 7) {
        return parseInt(getTime / 3600 / 24) + '天前'
      } else {
        fmt = 'yyyy-MM-dd hh:mm:ss'
      }
    }
    const o = {
      'M+': date.getMonth() + 1, // 月份
      'd+': date.getDate(), // 日
      'h+': date.getHours(), // 小时
      'm+': date.getMinutes(), // 分
      's+': date.getSeconds(), // 秒
      'q+': Math.floor((date.getMonth() + 3) / 3), // 季度
      'S': date.getMilliseconds() // 毫秒
    }
    if (/(y+)/.test(fmt)) { fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length)) }
    for (var k in o) {
      if (new RegExp('(' + k + ')').test(fmt)) { fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length))) }
    }
    return fmt
  },
  getWeek(day) {
    var week = ['星期一', '星期二', '星期三', '星期四', '星期五', '星期六', '星期天']
    return week[day]
  },
  /**
   * 生成指定长度的随机数
   * @param {*} len
   * @returns
   */
  randomString(len) {
    len = len || 5
    var t = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678'
    var a = t.length
    var n = ''
    for (var i = 0; i < len; i++) n += t.charAt(Math.floor(Math.random() * a))
    return n
  },
  /**
   * 生成指定长度的数字
   * @param {*} len   长度
   * @param {*} radix 基数
   * @returns
   */
  randomNumber(len, radix = 10) {
    var chars = '0123456789'.split('')
    var uuid = []
    radix = radix | chars.length
    if (len) {
      for (var i = 0; i < len; i++) {
        uuid[i] = chars[0 | Math.random() * radix]
      }
    }
    return uuid.join('')
  },
 
 
  /**
   * 格式化地区
   * @param {*} d 地区格式为：ID:省;ID:市;ID:县/区;ID:街道  或  ID:省;ID:市;ID:县/区
   */
  regionFormat(d) {
    if (d) {
      if (d.split(';').length === 5) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '') +
                   regions[3].split(':')[1].replace('-1', '') +
                   regions[4].split(':')[1].replace('-1', '')
      } else if (d.split(';').length === 4) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '') +
                   regions[3].split(':')[1].replace('-1', '')
      } else if (d.split(';').length === 3) {
        const regions = d.split(';')
        return regions[0].split(':')[1].replace('-1', '') +
                   regions[1].split(':')[1].replace('-1', '') +
                   regions[2].split(':')[1].replace('-1', '')
      } else {
        return '地址格式错误'
      }
    } else {
      return '暂无地址'
    }
  },
  /**
   * 获取地区key值
   * @param {*} d 地区格式为：ID:省;ID:市;ID:县/区;ID:街道  或  ID:省;ID:市;ID:县/区
   */
  regionIdFormat(d) {
    if (d) {
      if (d.split(';').length === 5) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0'),
          street: regions[3].split(':')[0].replace('-1', '0'),
          address: regions[4].split(':')[1].replace('-1', '0')
        }
      } else if (d.split(';').length === 4) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0'),
          street: regions[3].split(':')[0].replace('-1', '0')
        }
      } else if (d.split(';').length === 3) {
        const regions = d.split(';')
        return {
          province: regions[0].split(':')[0].replace('-1', '0'),
          city: regions[1].split(':')[0].replace('-1', '0'),
          area: regions[2].split(':')[0].replace('-1', '0')
        }
      } else {
        return '地址格式错误'
      }
    } else {
      return '-'
    }
  },
  /**
   * 格式化数据字典组
   * @param {*} d
   */
  dataDictFormat(d) {
    if (d) {
      const datas = d.split(':')
      if (d.split(':').length === 3) {
        return {
          typeCode: datas[0],
          typeName: datas[1],
          colorValue: datas[2]
        }
      } else if (d.split(':').length === 2) {
        return {
          typeCode: datas[0],
          typeName: datas[1],
          colorValue: ''
        }
      } else {
        return d
      }
    } else {
      return '-'
    }
  },
  /**
   * 简单实现防抖方法
   *
   * 防抖(debounce)函数在第一次触发给定的函数时，不立即执行函数，而是给出一个期限值(delay)，比如100ms。
   * 如果100ms内再次执行函数，就重新开始计时，直到计时结束后再真正执行函数。
   * 这样做的好处是如果短时间内大量触发同一事件，只会执行一次函数。
   *
   * @param fn 要防抖的函数
   * @param delay 防抖的毫秒数
   * @returns {Function}
 */
  simpleDebounce: function(fn, delay = 100) {
    let timer = null
    return function() {
      const args = arguments
      if (timer) {
        clearTimeout(timer)
      }
      timer = setTimeout(() => {
        fn.apply(this, args)
      }, delay)
    }
  },
  
  /**
   * 验证是否含有数字
   * @param {*} $value
  */
  _hasNumber($value) {
    let $bool = false
    const $Letters = '1234567890'
    for (let $i = 0; $i < $value.length; $i++) {
      const $c = $value.charAt($i)
      if ($Letters.indexOf($c) !== -1) {
        $bool = true
        break
      }
    }
    return $bool
  },
  /**
   * 验证是否含有小写字母
   * @param {*} $value
  */
  _hasSmallChar($value) {
    let $bool = false
    const $Letters = 'abcdefghijklnmopqrstuvwxyz'
    for (let $i = 0; $i < $value.length; $i++) {
      const $c = $value.charAt($i)
      if ($Letters.indexOf($c) !== -1) {
        $bool = true
        break
      }
    }
    return $bool
  },
  /**
   * 验证码是否含有大写字母
   * @param {*} $value
  */
  _hasBigChar($value) {
    let $bool = false
    const $Letters = 'ABCDEFGHIJKLNMOPQRSTUVWXYZ'
    for (let $i = 0; $i < $value.length; $i++) {
      const $c = $value.charAt($i)
      if ($Letters.indexOf($c) !== -1) {
        $bool = true
        break
      }
    }
    return $bool
  },
  /**
   * 根据身份证号返回性别、年龄、出生日期
   * @param {*} $value
  */
  analyzeIDCard($value) {
    let obj = {}
    const userCard = $value
    // 先验证身份证号是否合法
    validate.validateIDCard('', userCard, function(res) {
      // undefined为验证通过
      if (res === undefined) {
        // 获取性别
        if (parseInt(userCard.substr(16, 1)) % 2 === 1) {
          obj.sex = 1
        } else {
          obj.sex = 2
        }
        // 获取出生年月日
        var yearBirth = userCard.substring(6, 10)
        var monthBirth = userCard.substring(10, 12)
        var dayBirth = userCard.substring(12, 14)
        // 获得时间戳
        var date = yearBirth + '/' + monthBirth + '/' + dayBirth
        var timestamp = new Date(date).getTime()
        obj.birthday = timestamp
        // 获取当前年月日并计算年龄
        var myDate = new Date()
        var monthNow = myDate.getMonth() + 1
        var dayNow = myDate.getDay()
        var age = myDate.getFullYear() - yearBirth
        if (monthNow < monthBirth || (monthNow === monthBirth && dayNow < dayBirth)) {
          age--
        }
        // 得到年龄
        obj.age = age
        // 返回性别和年龄
      } else {
        obj = null
      }
    })
    return obj
  },
}

Vue.prototype.common = common
export default common
