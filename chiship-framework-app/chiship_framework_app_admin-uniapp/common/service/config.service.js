let BASE_URL = ''


if (process.env.NODE_ENV == 'development') {
    BASE_URL = 'http://chiship.site/chiship/framework/boot/' // 开发环境
} else {
	BASE_URL = 'http://chiship.site/chiship/framework/boot/' // 生产环境
}
let staticDomainURL = BASE_URL+ '/sys/common/static';

const configService = {
	BASE_URL: BASE_URL,
	staticDomainURL: staticDomainURL
};

export default configService
