import util from './utils/util'
import constant from './utils/constant'
import api from './utils/api'
import network from './utils/network'

App({
  onLaunch(options) {
    // 第一次打开
    // options.query == {number:1}
    console.info('App onLaunch');
  },
  onShow(options) {
    // 从后台被 scheme 重新打开
    // options.query == {number:1}
  },
  verifyLogin() {
    return new Promise((resolve, reject) => {
      var token = util.getStorage(constant.TOKEN)
      console.log(token)
      if (!token) {
        util.showToast('您还未登录，请登录')
        setTimeout(function () {
          my.navigateTo({
            url: '/pages/user/login/userName/index'
          })
        }, 1000)
        reject('您还未登录，请登录')
      } else {
        network.get(api.getUserInfo).then(res => {
          console.log(res)
          //util.setStorage(constant.USER_INFO,res)
          resolve(res)
        }).catch((e) => {
          console.log(e)
        })
      }
    })

  },
  getOpenId() {
    return new Promise((reslove, reject) => {
      my.getAuthCode({
        scopes: 'auth_user',
        success: ({ authCode }) => {
          network.get(api.getOauthToken, {
            code: authCode
          }).then((res) => {
            reslove(res.openId)
          }).catch((_) => {
            reject('error')
          })
        },
        fail: (err) => {
          util.showToast('授权失败')
          reject('授权失败')
        }
      })
    })
  },
  getUserInfo() {
    return util.getStorage(constant.USER_INFO)
  },
  util,
  constant,
  network,
  api
});
