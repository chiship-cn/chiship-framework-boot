import constant from './constant'
import api from './api'
var util = {
  getAppId(){
    var {miniProgram:{appId}}=my.getAccountInfoSync()
    if(!appId){
      console.error('获取appId失败，配置默认appId或忽略，自行选择')
    }
    return appId
  },
  getFileView(uuid) {
    if (uuid) {
      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
        return uuid
      } else {
        console.log("getFileView", constant)
        return api.BUSINESS + "/fileView/" + uuid + "?AppId=" + constant.APP_ID + "&AppKey=" + constant.APP_KEY + "&ProjectsId=" + constant.PROJECTS_ID
      }
    } else {
      return null
    }
  },
  formatTime(dateTime, fmt) {
    let date = new Date(dateTime);
    if (!fmt) {
      const nowDate = new Date()
      var getTime = (nowDate.getTime() - date.getTime()) / 1000
      if (getTime < 60 * 1) {
        return '刚刚'
      } else if (getTime >= 60 * 1 && getTime < 60 * 60) {
        return parseInt(getTime / 60) + '分钟前'
      } else if (getTime >= 3600 && getTime < 3600 * 24) {
        return parseInt(getTime / 3600) + '小时前'
      } else if (getTime >= 3600 * 24 && getTime < 3600 * 24 * 7) {
        return parseInt(getTime / 3600 / 24) + '天前'
      } else {
        fmt = 'yyyy-MM-dd hh:mm:ss'
      }
    }
    let o = {
      "M+": date.getMonth() + 1, //月份
      "d+": date.getDate(), //日
      "h+": date.getHours(), //小时
      "m+": date.getMinutes(), //分
      "s+": date.getSeconds(), //秒
      "q+": Math.floor((date.getMonth() + 3) / 3), //季度
      "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o)
      if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;

  },
  fmtFileSize(val = 0) {
    if (!val) {
      return '-'
    } else if (typeof (val) !== 'number') {
      return '-'
    } else if (val < 1024) {
      return (val).toFixed(2) + 'KB'
    } else if (val < 1024 * 1024) {
      return (val / 1024).toFixed(2) + 'M'
    } else {
      return (val / 1024 / 1024).toFixed(2) + 'G'
    }
  },
  showToast(message = "提示消息") {
    my.showToast({ content: message })
  },
  getStorage(key) {
    const { data } = my.getStorageSync({
      key: key,
    });
    return data;
  },
  setStorage(key, value) {
    my.setStorageSync({
      key: key,
      data: value,
    });
  },
  removeStorage(key) {
    my.removeStorageSync({
      key: key,
    });
  },
  fmtOptionUser(val) {
    if (val) {
      try {
        var optionUser = JSON.parse(val)
        return optionUser.realName + (optionUser.orgName ? (' | ' + optionUser.orgName) : '')
      } catch (err) {
        return '-'
      }
    }
    return '-'
  },
}

export default util