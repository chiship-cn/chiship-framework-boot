import constant from './constant'
import util from './util'
var headers = {
  'content-type': 'application/json;charset=UTF-8',
  "ProjectsId": constant.PROJECTS_ID,
  "App-Id": constant.APP_ID,
  "App-Key": constant.APP_KEY,
  "Sign": "MD5="
}

var network = {
  get(url, params, message = '加载中...', errorShow = true) {
    my.showLoading({
      content: message
    });
    return new Promise((resolve, reject) => {
      my.request({
        url: url + "?appId=" + util.getAppId(),
        data: params,
        headers: {
          ...headers,
          "Access-Token": util.getStorage(constant.TOKEN)
        },
        method: 'GET',
        dataType: 'json',
        timeout: 30000,
        success: (res) => {
          my.hideLoading()
          var { data, statusCode } = res
          if (statusCode == 200 && data.code == 200) {
            resolve(data.data)
          } else if (data.code == 502 || data.code == 506003) {
            util.showToast('令牌失效，重新登录')
            setTimeout(function () {
              util.removeStorage(constant.USER_INFO)
              util.removeStorage(constant.TOKEN)
              my.navigateTo({
                url: '/pages/user/login/userName/index'
              })
            }, 1000)
          } else {
            var msg = data.data == null ? data.message : typeof (data.data) == 'object' ? JSON.stringify(data.data) : data.data
            util.showToast(msg)
            reject(msg)
          }
        },
        fail: function (res) {
          my.hideLoading()
          util.showToast(res.errMsg || '网络故障')
          reject(res)
        }
      })
    })
  },
  post(url, params, message = '加载中...', errorShow = true) {
    my.showLoading({
      content: message
    });
    return new Promise((resolve, reject) => {
      my.request({
        url: url + "?appId=" + util.getAppId(),
        method: 'POST',
        data: params,
        headers: {
          ...headers,
          'content-type': 'application/json',
          "Access-Token": util.getStorage(constant.TOKEN)
        },
        dataType: 'json',
        success: function (res) {
          my.hideLoading()
          var { data, statusCode } = res
          if (statusCode == 200 && data.code == 200) {
            resolve(data.data)
          } else if (data.code == 502 || data.code == 506003) {
            util.showToast('令牌失效，重新登录')
            setTimeout(function () {
              util.removeStorage(constant.USER_INFO)
              util.removeStorage(constant.TOKEN)
              my.navigateTo({
                url: '/pages/user/login/userName/index'
              })
            }, 1000)
          } else {
            var msg = data.data == null ? data.message : typeof (data.data) == 'object' ? JSON.stringify(data.data) : data.data
            util.showToast(msg)
            reject(msg)
          }
        },
        fail: function (error) {
          my.hideLoading()
          util.showToast(res.errMsg || '网络故障')
          reject(res)
        }
      });
    })
  }
}

export default network