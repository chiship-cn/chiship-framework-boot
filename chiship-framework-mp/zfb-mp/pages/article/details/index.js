Page({
  data: {
    toView: '',
    id: null,
    isCollect: false,
    isLike: false,
    categoryId: null,
    articleRecord: {},
    commentContent: '',
    commentIsLoading: false,
    commentsQuery: {
      page: 1,
      limit: 20,
      sort: '-gmtCreated'
    },
    comments: [],
    commentPages: 1,
    commentCreateShow: false
  },
  onLoad(options) {
    // options = { id: '1021197745640693760', category: '' }
    if (!options) {
      this.handleError()
      return
    }
    this.setData({
      categoryId: options.category
    })
    var id = options.id
    if (!id) {
      this.handleError()
      return
    }
    this.setData({
      id: id
    })
    this.initDetails()
  },
  handleError() {
    getApp().util.showToast('缺少参数')
    my.navigateTo({
      url: '/pages/article/index/index',
    })
  },
  onShareAppMessage() {
    return {
      title: this.data.articleRecord.title,
      path: '/pages/article/details/index?id=' + this.data.articleRecord.id,
    };
  },
  initDetails() {
    getApp().network.get(getApp().api.contentView.replace('$ID', this.data.id)).then(res => {
      this.setData({
        articleRecord: {
          ...res,
          _publishDate: getApp().util.formatTime(res.publishDate)
        }
      })
      this.contenPreAndNext()
      this.fetchComments()
      var token = getApp().util.getStorage(getApp().constant.TOKEN)
      if (token) {
        this.checkIsAddCollectLike()
      }
    })
  },
  contenPreAndNext() {
    getApp().network.get(getApp().api.contenPreAndNext, {
      id: this.data.articleRecord.id,
      categoryId: this.data.categoryId ? this.data.articleRecord.categoryId : '',
      isAll: false
    }).then(res => {
      this.setData({
        next: res.next,
        pre: res.pre
      })
    })
  },
  checkIsAddCollectLike() {
    getApp().network.post(getApp().api.isCollectionLike, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE'
    }).then(res => {
      this.setData({
        isCollect: res.collect,
        isLike: res.like
      })
    })
  },
  goArticleDetails(event) {
    my.redirectTo({
      url: '/pages/article/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  doFavoriteLike(event) {
    getApp().verifyLogin()
    var type = event.currentTarget.dataset.type
    console.log(type)
    var typeEnum = ''
    if (type === 'collect') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_SC'
    }
    if (type === 'like') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_DZ'
    }
    getApp().network.post(getApp().api.collectionLikeAdd, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE',
      typeEnum: typeEnum
    }).then(res => {
      this.initDetails()
    })
  },
  doFavoriteLikeCancel(event) {
    getApp().verifyLogin()
    var type = event.currentTarget.dataset.type
    console.log(type)
    var typeEnum = ''
    if (type === 'collect') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_SC'
    }
    if (type === 'like') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_DZ'
    }
    getApp().network.post(getApp().api.collectionLikeCancel, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE',
      typeEnum: typeEnum
    }).then(res => {
      this.initDetails()
    })
  },
  goCmment() {
    getApp().verifyLogin().then(() => {
      this.setData({
        commentContent: '',
        commentCreateShow: true
      })
    })

  },
  onCommentCreateClose() {
    this.setData({
      commentCreateShow: false
    })
  },
  goCmmentList() {
    this.setData({
      toView: "comment-list"
    })
  },
  fetchCommentNew() {
    this.setData({
      comments: [],
      commentPages: 1,
      commentsQuery: {
        ...this.data.commentsQuery,
        commentsQuery: 1
      }
    })
    this.fetchComments()
  },
  fetchCommentNext() {
    var page = this.data.commentsQuery.page
    if (page < this.data.commentPages) {
      page += 1
    } else {
      getApp().util.showToast('没有更多了')
      return
    }
    this.setData({
      commentsQuery: {
        ...this.data.commentsQuery,
        page: page
      }
    })
    this.fetchComments()
  },
  fetchComments() {
    if (this.data.articleRecord.isAllowComment === 0) {
      return
    }
    this.setData({
      commentsQuery: {
        ...this.data.commentsQuery,
        module: 'MEMBER_COMMENT_MODULE_ARTICLE',
        moduleId: this.data.articleRecord.id
      }
    })
    getApp().network.get(getApp().api.memberCommentPage, this.data.commentsQuery).then(res => {
      var comments = []
      for (var item of res.records) {
        item.createdUserAvatar = getApp().util.getFileView(item.createdUserAvatar)
        item._gmtCreated = getApp().util.formatTime(item.gmtCreated)

        comments.push(item)
      }
      this.setData({
        commentIsLoading: false,
        comments: this.data.comments.concat(...comments),
        commentPages: res.pages
      })
    })
  },
  handleCommentContentChange(value) {
    this.setData({
      commentContent: value
    });
  },
  handleCommentSave() {

    if (!this.data.commentContent) {
      getApp().util.showToast('请输入评论内容！')
      return
    }
    getApp().network.post(getApp().api.memberCommentCreate, {
      parentId: '0',
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COMMENT_MODULE_ARTICLE',
      text: this.data.commentContent
    }).then(res => {
      getApp().util.showToast('评论成功！')
      var comments = this.data.comments
      comments.unshift({
        ...res,
        createdUserAvatar: getApp().util.getFileView(res.createdUserAvatar),
        _gmtCreated: getApp().util.formatTime(res.gmtCreated)
      })
      this.setData({
        commentCreateShow: false,
        comments: comments,
        commentContent: '',
        articleRecord: {
          ...this.data.articleRecord,
          comments: this.data.articleRecord.comments + 1
        }
      })
    })
  }
});
