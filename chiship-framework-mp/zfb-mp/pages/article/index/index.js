Page({
  data: {
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    scrollHeight: 700,
    activeCategory: 0,
    categories: [],
    listQuery: {
      page: 1,
      limit: 20,
      title: '',
      treeNumber: ''
    },
    newsPages: 1,
    news: []
  },
  onLoad() {
    this.fetchCategory()
  },
  onPullDownRefresh() {
    this.pullDownRefresh()
    my.stopPullDownRefresh();
  },
  handleSearchChange(value) {
    this.setData({
      keyword:value
    });
  },
  fetchCategory() {
    //
    getApp().network.get(getApp().api.categoryDictTree, { type: '05' }).then(res => {
      console.log(res)
      var categories = [{
        id: '-1',
        name: '全部',
        treeNumber: ''
      }]
      res.forEach(item => {
        categories.push({
          id: item.id,
          name: item.name,
          treeNumber: item.treeNumber
        })
      });
      this.setData({
        categories: categories,
        newsPages: 1,
        news: [],
        listQuery: {
          ...this.data.listQuery,
          page: 1,
          treeNumber: ""
        }
      })
      this.initNews()
    })
  },
  onChange(event) {
    this.setData({
      activeCategory: event,
      news: [],
      newsPages: 1,
      listQuery: {
        ...this.data.listQuery,
        page: 1,
        treeNumber: this.data.categories[event].treeNumber
      }
    })
    this.initNews()
  },
  onSearch() {
    this.setData({
      news: [],
      listQuery: {
        ...this.data.listQuery,
        title: this.data.keyword,
        page: 1
      }
    })
    this.initNews()
  },
  pullDownRefresh() {
    console.log('下拉刷新')
    this.setData({
      keyword: null,
      news: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.initNews()
  },
  loadMore() {
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if (page < this.data.newsPages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.initNews()
  },
  initNews() {
    getApp().network.get(getApp().api.contentPage, this.data.listQuery).then(res => {
      var news = []
      for (var item of res.records) {
        var images = []
        if (item.image1) {
          images.push(getApp().util.getFileView(item.image1))
        }
        if (item.image2) {
          images.push(getApp().util.getFileView(item.image2))
        }
        if (item.image3) {
          images.push(getApp().util.getFileView(item.image3))
        }
        item.images = images
        item.imageCount = images.length
        news.push(item)
      }

      this.setData({
        isLoading: false,
        news: this.data.news.concat(...news),
        newsPages: res.pages
      })
    })
  },
  goArticleDetails(event) {
    my.navigateTo({
      url: '/pages/article/details/index?category=' + this.data.listQuery.treeNumber + '&id=' + event.currentTarget.dataset.id,
    })
  }
});
