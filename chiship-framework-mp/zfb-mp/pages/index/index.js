var app=getApp();
Page({
  data:{
    funs: [],
    scrollHeight: 700,
    adverts: [],
    noticeContent: null,
    contentArticles: []
  },
  onLoad(query) {
    my.getSystemInfo({
      success: (res) => {
        let windowHeight = (res.windowHeight * (750 / res.windowWidth));
        if (res.model.search('iPhone X') != -1) {}
        this.setData({
          windowWidth: res.windowWidth,
          scrollHeight: ((windowHeight * 0.6) - (200) * 750 / res.windowWidth)
        })
      }
    })
    this.initFuns()
    this.fetchAdvert()
    this.fetchNotice()
    this.fetchContentArticles()
  },
  onReady() {
    // 页面加载完成
  },
  onShow() {
    // 页面显示
  },
  onHide() {
    // 页面隐藏
  },
  onUnload() {
    // 页面被关闭
  },
  onTitleClick() {
    // 标题被点击
  },
  onPullDownRefresh() {
    // 页面被下拉
  },
  onReachBottom() {
    // 页面被拉到底部
  },
  onShareAppMessage() {
    // 返回自定义分享信息
    return {
      title: 'My App',
      desc: 'My App description',
      path: 'pages/index/index',
    };
  },
  initFuns() {
    this.setData({
      funs: [{
          code: 'zfcs',
          icon:'https://chiship.oss-cn-shanghai.aliyuncs.com/chiship/temp/dog/mini_program/fun_zjbl.png',
          description: '收银台',
        },
        {
          code: 'grsz',
          description: '个人设置',
          icon:'https://chiship.oss-cn-shanghai.aliyuncs.com/chiship/temp/dog/mini_program/fun_zjbl.png'
        },
        {
          code: 'zxzx',
          description: '资讯中心',
          icon:'https://chiship.oss-cn-shanghai.aliyuncs.com/chiship/temp/dog/mini_program/fun_zjbl.png'
        },
        {
          code: 'gywm',
          description: '关于我们',
          icon:'https://chiship.oss-cn-shanghai.aliyuncs.com/chiship/temp/dog/mini_program/fun_zjbl.png'
        },
      ]
    })
  },
  goFunction(e) {
    var code = e.currentTarget.dataset.item.code
    var url = null
    switch (code) {
      case 'grsz': //个人设置
        url = "/pages/my/personalData/index"
        my.navigateTo({
          url: url,
        })
        break;
      case 'zxzx': //资讯中心
        this.handleLookMore()
        break;
      case 'gywm': //关于我们
        url = "/pages/my/about/index"
        my.navigateTo({
          url: url,
        })
        break;
      case 'zfcs':
        url = "/pages/cashier/pay/index"
        my.navigateTo({
          url: url,
        })
        break
    }

  },
  fetchAdvert() {
    app.network.get(app.api.contentAdvertByCode, {
      code: 'member_index'
    }).then(res => {
      res.children.forEach(item => {
        item.imageUrl = app.util.getFileView(item.imageUrl)
      })
      console.log(res.children)
      this.setData({
        adverts: res.children
      })
    })
  },
  fetchNotice() {
    app.network.get(app.api.noticePage, {
      page: 1,
      limit: 5,
      sort: '-sendTime',
      noticeType: 1,
      noticeUserType: 'member'
    }).then(res => {
      var content = ''
      res.records.forEach((item, index) => {
        content += (index + 1) + '.' + item.noticeTitle + '。'
      })
      this.setData({
        noticeContent: content
      })
    })
  },
  goArticleDetails(event) {
    my.navigateTo({
      url: '/pages/article/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  fetchContentArticles() {
    app.network.get(app.api.contentPage, {
      page: 1,
      limit: 50
    }).then(res => {
      res.records.forEach(item => {
        item.image1 = app.util.getFileView(item.image1) || '/image/noNews.png'
      })
      this.setData({
        contentArticles: res.records
      })
    })
  },
  handleLookMore() {
    my.navigateTo({
      url: "/pages/article/index/index",
    })
  },
  async createOrder() {
    var order = await app.network.post(app.api.createOrder, {
      orderName: '账号充值',
      price: '0.01'
    })
    wx.login({
      success: ({
        code
      }) => {
        app.network.get(app.api.code2Session, {
          code: code
        }).then(res => {
          console.log(res)
          this.doPay(order, res.openid)
        })

      }
    })

  },
  doPay(orderNo, openId) {
    app.network.post(app.api.wxPrepay, {
      openId: openId,
      tradeType: 'MINI_PROGRAM',
      orderNo: orderNo
    }).then(res => {
      wx.requestPayment({
        timeStamp: res.timeStamp,
        nonceStr: res.nonceStr,
        package: res.packageVal,
        signType: res.signType,
        paySign: res.paySign,
        success: function (res) {
          wx.showToast({
            title: '支付成功！',
          })
        },
        fail: function (res) {}

      })
    })
  },
  handleGoNotice() {
    my.navigateTo({
      url: '/pages/notice/index/index',
    })
  }
});
