Page({
  data: {
    reachBottomText: null,
    keyword: null,
    listQuery: {
      page: 1,
      limit: 20,
      noticeTitle: '',
      sort: '-sendTime',
      noticeType: 1,
      noticeUserType: 'member'
    },
    noticePages: 1,
    notices: []
  },
  onLoad() {
    this.initNotices()
  },
  onPullDownRefresh() {
    this.pullDownRefresh()
    my.stopPullDownRefresh();
  },
  handleSearchChange(value) {
    this.setData({
      keyword:value
    });
  },
  onSearch() {
    this.setData({
      notices: [],
      listQuery: {
        ...this.data.listQuery,
        noticeTitle: this.data.keyword,
        page: 1
      }
    })
    this.initNotices()
  },
  pullDownRefresh() {
    this.setData({
      keyword: null,
      notices: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.initNotices()
  },
  loadMore() {
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if (page < this.data.noticePages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.initNotices()
  },
  initNotices() {
    getApp().network.get(getApp().api.noticePage, this.data.listQuery).then(res => {
      var notices = []
      for (var item of res.records) {
        item.createdBy = getApp().util.fmtOptionUser(item.createdBy)
        item._sendTime = getApp().util.formatTime(item.sendTime)
        notices.push(item)
      }

      this.setData({
        notices: this.data.notices.concat(...notices),
        noticePages: res.pages
      })
    })
  },
  goArticleDetails(event) {
    my.navigateTo({
      url: '/pages/notice/details/index?id=' + event.currentTarget.dataset.id,
    })
  }

})