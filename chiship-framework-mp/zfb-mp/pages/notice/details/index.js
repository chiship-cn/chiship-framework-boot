Page({
  data: {
    id:null,
    noticeRecord:{}
  },
  onLoad(options) {
    if(!options){
      this.handleError()
      return
    }
    var id = options.id
    if(!id){
      this.handleError()
      return
    }
    this.setData({
      id:id
    })
    this.initDetails()
  },
  handleError(){
    getApp().util.showToast('缺少参数')
    my.navigateTo({
      url: '/pages/notice/index/index',
    })
  },
  initDetails(){
    getApp().network.get(getApp().api.noticeView.replace('$ID',this.data.id)).then(res=>{
      this.setData({
        noticeRecord:{
          ...res,
          createdBy:getApp().util.fmtOptionUser(res.createdBy),
          _sendTime:getApp().util.formatTime(res.sendTime)
        }
      })
    })
  }
});
