Page({
  data: {
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    scrollHeight: 700,
    activeCategory: 0,
    categories: [],
    secondCategories: [],
    listQuery: {
      page: 1,
      limit: 20,
      name: '',
      treeNumber: ''
    },
    productsPages: 1,
    products: []
  },
  onLoad() {
    this.fetchCategory()
  },
  onPullDownRefresh() {
    this.pullDownRefresh()
    my.stopPullDownRefresh();
  },
  handleSearchChange(value) {
    this.setData({
      keyword: value
    });
  },
  fetchCategory() {
    getApp().network.get(getApp().api.productCategory).then(res => {
      console.log(res)
      var categories = [{
        id: '-1',
        name: '全部',
        treeNumber: ''
      }]
      res.forEach(item => {
        categories.push({
          id: item.id,
          name: item.categoryName,
          treeNumber: item.treeNumber
        })
      });
      this.setData({
        categories: categories,
        productsPages: 1,
        products: [],
        listQuery: {
          ...this.data.listQuery,
          page: 1,
          treeNumber: ""
        }
      })
      this.fetchProducts()
    })
  },
  onChange(event) {
    this.setData({
      activeCategory: event,
      products: [],
      productsPages: 1,
      listQuery: {
        ...this.data.listQuery,
        page: 1,
        treeNumber: this.data.categories[event].treeNumber
      }
    })
    getApp().network.get(getApp().api.productCategory, {
      pid: this.data.categories[event].id
    }).then(res => {
      var categories = []
      res.forEach(item => {
        categories.push({
          ...item,
          description: item.categoryName,
          icon: getApp().util.getFileView(item.categoryIcon)
        })
      });
      this.setData({
        secondCategories: categories
      })
    })
    this.fetchProducts()
  },
  onSearch() {
    this.setData({
      products: [],
      listQuery: {
        ...this.data.listQuery,
        name: this.data.keyword,
        page: 1
      }
    })
    this.fetchProducts()
  },
  pullDownRefresh() {
    console.log('下拉刷新')
    this.setData({
      keyword: null,
      products: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.fetchProducts()
  },
  loadMore() {
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if (page < this.data.productsPages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.fetchProducts()
  },
  fetchProducts() {
    getApp().network.get(getApp().api.productPage, this.data.listQuery).then(res => {
      var products = []
      for (var item of res.records) {
        item.smallImage = getApp().util.getFileView(item.smallImage)
        products.push(item)
      }

      this.setData({
        isLoading: false,
        products: this.data.products.concat(...products),
        productsPages: res.pages
      })
      console.log(this.data)
    })
  },
  goProductDetails(event) {
    my.navigateTo({
      url: '/pages/product/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  goNextCategory(event) {
    console.log(event)
    var {
      id,
      categoryName,
      treeNumber
    } = event
    my.navigateTo({
      url: `/pages/product/list/index?id=${id}&tree=${treeNumber}&name=${categoryName}`,
    })
  },
});
