import { Form } from 'antd-mini/es/Form/form';
const app =getApp()
Page({
  data: {
    radioGroupOptions: [
      { value: '0', label: '' },
    ],
    passwordShow: false
  },
  onLoad() {
    this.form = new Form({
      /*rules: {
        username: [
          { required: true, message: '请输入用户名' },
        ],
        password: [
          { required: true, message: '需要输入密码', },
        ]
      },*/
    });
  },
  handleRef(ref) {
    this.form.addItem(ref);
  },

  showPassword() {
    console.log(this.data.passwordShow)
    this.setData({
      passwordShow: !this.data.passwordShow
    })
  },
  async doLogin() {
    const values = await this.form.submit();
    var {username,password}=values
    if(!username){
      app.util.showToast('请输入用户名')
      return
    }
    if(!password){
      app.util.showToast('请输入密码')
      return
    }
    app.network.post(app.api.basicLogin, {
      username: username,
      password: password
    }).then(res => {
      app.util.setStorage(app.constant.TOKEN, res)
      app.util.showToast("登录成功！")
      app.verifyLogin().then(res=>{
        console.log(111)
        my.reLaunch({
          url: '/pages/index/index'
        });
      })
    }).catch(() => {})

  },
  
});
