// pages/article/details/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    toView: '',
    id: null,
    isCollect: false,
    isLike: false,
    categoryId: null,
    articleRecord: {},
    commentContent: '',
    commentIsLoading: false,
    commentsQuery: {
      page: 1,
      limit: 20,
      sort: '-gmtCreated'
    },
    comments: [],
    commentPages: 1,
    commentCreateShow: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    // options={id:'1019103284915859456',category:''}
    if (!options) {
      this.handleError()
      return
    }
    this.setData({
      categoryId: options.category
    })
    var id = options.id
    if (!id) {
      this.handleError()
      return
    }
    this.setData({
      id: id
    })
    this.initDetails()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {
    return {
      title: this.articleRecord.title,
    }
  },
  handleError() {
    getApp().util.showToast('缺少参数')
    wx.navigateTo({
      url: '/pages/article/index/index',
    })
  },
  initDetails() {
    getApp().network.get(getApp().api.contentView.replace('$ID', this.data.id)).then(res => {
      this.setData({
        articleRecord: {
          ...res,
          _publishDate: getApp().util.formatTime(res.publishDate)
        }
      })
      this.contenPreAndNext()
      this.fetchComments()
      var token = wx.getStorageSync(getApp().constant.TOKEN)
      if (token) {
        this.checkIsAddCollectLike()
      }
    })
  },
  contenPreAndNext() {
    getApp().network.get(getApp().api.contenPreAndNext, {
      id: this.data.articleRecord.id,
      categoryId: this.data.categoryId ? this.data.articleRecord.categoryId : '',
      isAll: false
    }).then(res => {
      this.setData({
        next: res.next,
        pre: res.pre
      })
    })
  },
  checkIsAddCollectLike() {
    getApp().network.post(getApp().api.isCollectionLike, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE'
    }).then(res => {
      this.setData({
        isCollect: res.collect,
        isLike: res.like
      })
    })
  },
  goArticleDetails(event) {
    wx.redirectTo({
      url: '/pages/article/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  doFavoriteLike(event) {
    getApp().verifyLogin()
    var type = event.currentTarget.dataset.type
    console.log(type)
    var typeEnum = ''
    if (type === 'collect') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_SC'
    }
    if (type === 'like') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_DZ'
    }
    getApp().network.post(getApp().api.collectionLikeAdd, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE',
      typeEnum: typeEnum
    }).then(res => {
      this.initDetails()
    })
  },
  doFavoriteLikeCancel(event) {
    getApp().verifyLogin()
    var type = event.currentTarget.dataset.type
    console.log(type)
    var typeEnum = ''
    if (type === 'collect') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_SC'
    }
    if (type === 'like') {
      typeEnum = 'MEMBER_COLLECTION_LIKE_DZ'
    }
    getApp().network.post(getApp().api.collectionLikeCancel, {
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COLLECTION_LIKE_MODULE_ARTICLE',
      typeEnum: typeEnum
    }).then(res => {
      this.initDetails()
    })
  },
  goCmment() {
    this.setData({
      commentContent: '',
      commentCreateShow: true
    })
  },
  onCommentCreateClose() {
    this.setData({
      commentCreateShow: false
    })
  },
  goCmmentList() {
    this.setData({
      toView: "comment-list"
    })
  },
  fetchCommentNew() {
    this.setData({
      comments: [],
      commentPages: 1,
      commentsQuery: {
        ...this.data.commentsQuery,
        commentsQuery: 1
      }
    })
    this.fetchComments()
  },
  fetchCommentNext() {
    var page = this.data.commentsQuery.page
    if (page < this.data.commentPages) {
      page += 1
    } else {
      getApp().util.showToast('没有更多了')
      return
    }
    this.setData({
      commentsQuery: {
        ...this.data.commentsQuery,
        page: page
      }
    })
    this.fetchComments()
  },
  fetchComments() {
    if (this.data.articleRecord.isAllowComment === 0) {
      return
    }
    this.setData({
      commentsQuery: {
        ...this.data.commentsQuery,
        module: 'MEMBER_COMMENT_MODULE_ARTICLE',
        moduleId: this.data.articleRecord.id
      }
    })
    getApp().network.get(getApp().api.memberCommentPage, this.data.commentsQuery).then(res => {
      var comments = []
      for (var item of res.records) {
        item.createdUserAvatar = getApp().util.getFileView(item.createdUserAvatar)
        item._gmtCreated = getApp().util.formatTime(item.gmtCreated)

        comments.push(item)
      }
      this.setData({
        commentIsLoading: false,
        comments: this.data.comments.concat(...comments),
        commentPages: res.pages
      })
    })
  },
  handleCommentSave() {
    getApp().verifyLogin()
    if (!this.data.commentContent) {
      getApp().util.showToast('请输入评论内容！')
      return
    }
    getApp().network.post(getApp().api.memberCommentCreate, {
      parentId: '0',
      moduleId: this.data.articleRecord.id,
      moduleEnum: 'MEMBER_COMMENT_MODULE_ARTICLE',
      text: this.data.commentContent
    }).then(res => {
      getApp().util.showToast('评论成功！')
      var comments = this.data.comments
      comments.unshift({
        ...res,
        createdUserAvatar: getApp().util.getFileView(res.createdUserAvatar),
        _gmtCreated: getApp().util.formatTime(res.gmtCreated)
      })
      this.setData({
        commentCreateShow: false,
        comments: comments,
        commentContent: '',
        articleRecord: {
          ...this.data.articleRecord,
          comments: this.data.articleRecord.comments + 1
        }
      })
    })
  }
})