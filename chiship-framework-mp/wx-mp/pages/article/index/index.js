Page({

  /**
   * 页面的初始数据
   */
  data: {
    reachBottomText:null,
    keyword:null,
    isLoading:false,
    scrollHeight:700,
    activeCategory:'top',
    categories:[],
    listQuery:{
      page:1,
      limit:20,
      title:'',
      treeNumber:''
    },
    newsPages:1,
    news:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchCategory()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  fetchCategory(){
    //
    getApp().network.get(getApp().api.categoryDictTree, {type:'05'}).then(res=>{
      console.log(res)
      var categories=[{
        id:'-1',
        name:'全部',
        treeNumber:''
      }]
      res.forEach(item => {
        categories.push({
          id:item.id,
          name:item.name,
          treeNumber:item.treeNumber
        })
      });
      this.setData({
        categories:categories,
        newsPages:1,
        news:[],
        listQuery:{
          ...this.data.listQuery,
          page:1,
          treeNumber:""
        }
      })
      this.initNews()
    })
  },
  onChange(event){
    this.setData({
      activeCategory:event.detail.name,
      news:[],
      newsPages:1,
      listQuery:{
        ...this.data.listQuery,
        page:1,
        treeNumber:this.data.categories[event.detail.index].treeNumber
      }
    })
    this.initNews()
  },
  onSearch(){
    this.setData({
      news:[],
      listQuery:{
        ...this.data.listQuery,
        title:this.data.keyword,
        page:1
      }
    })
    this.initNews()
  },
  pullDownRefresh(){
    console.log('下拉刷新')
    this.setData({
      keyword:null,
      news:[],
      reachBottomText:null,
      listQuery:{
        ...this.data.listQuery,
        page:1
      }
    })
    this.initNews()
  },
  loadMore(){
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if(page<this.data.newsPages){
      page+=1
      this.setData({
        reachBottomText: null
      })
    }else{
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword:null,
      listQuery:{
        ...this.data.listQuery,
        page:page
      }
    })
    this.initNews()
  },
  initNews(){
    getApp().network.get(getApp().api.contentPage, this.data.listQuery).then(res=>{
      var news=[]
      for(var item of res.records){
        var images=[]
        if(item.image1){
          images.push(getApp().util.getFileView(item.image1))
        }
        if(item.image2){
          images.push(getApp().util.getFileView(item.image2))
        }
        if(item.image3){
          images.push(getApp().util.getFileView(item.image3))
        }
        item.images=images
        item.imageCount=images.length
        news.push(item)
      }
      
      this.setData({
        isLoading:false,
        news:this.data.news.concat(...news),
        newsPages:res.pages
      })
    }) 
  },
  goArticleDetails(event){
    wx.navigateTo({
      url: '/pages/article/details/index?category='+this.data.listQuery.treeNumber+'&id='+event.currentTarget.dataset.id,
    })
  }
   
})