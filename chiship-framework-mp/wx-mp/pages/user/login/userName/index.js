// pages/login/userName/index.js
import WxValidate from '../../../../utils/WxValidate'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    passwordShow: false,
    radio: '0',
    username: '',
    password: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initValidate();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  onChange(event) {
    this.setData({
      radio: event.detail,
    });
  },
  showPassword() {
    this.setData({
      passwordShow: !this.data.passwordShow,
    });
  },
  initValidate() {
    const rules = {
      username: {
        required: true,
      },
      password: {
        required: true,
        rangelength: [5, 20]
      }
    }
    const messages = {
      username: {
        required: '请输入用户名',
      },
      password: {
        required: '请输入密码',
        rangelength: '密码长度在5到20之间'
      }
    }
    this.WxValidate = new WxValidate(rules, messages)
  },

  doLogin() {
    var {
      username,
      password,
      radio
    } = this.data
    if (radio === '0') {
      getApp().util.showToast("请先同意用户协议")
      return
    }
    if (!this.WxValidate.checkForm(this.data)) {
      const error = this.WxValidate.errorList[0];
      getApp().util.showToast(error.msg)
      return;
    }

    getApp().network.post(getApp().api.basicLogin, {
      username: username,
      password: password
    }).then(res => {
      wx.setStorageSync(getApp().constant.TOKEN, res)
      getApp().util.showToast("登录成功！")
      getApp().verifyLogin()
      setTimeout(() => {
        wx.switchTab({
          url: '/pages/index/index',
        })
      }, 1000);
    }).catch(() => {})

  },
  goMobile() {
    wx.navigateTo({
      url: '/pages/user/login/mobile/index',
    })
  },
  goRegister() {
    wx.navigateTo({
      url: '/pages/user/register/index',
    })
  },
  goForgetPwd() {
    wx.navigateTo({
      url: '/pages/user/forget/index',
    })
  },

})