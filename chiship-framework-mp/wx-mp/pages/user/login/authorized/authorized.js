// pages/user/login/authorized/authorized.js
const constant = require('../../../../utils/constant');
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    agree: '',
  },

  /**
   * 组件的初始数据
   */
  data: {
    actionSheet: false,
    avatarUrl: '../../../../image/user.jpg',
    avatar: '',
    nickName: '',
    mobile: '',
    loginUserInfo: {}
  },
  created() {
    let token = wx.getStorageSync(constant.TOKEN)
    if (token) {
      wx.switchTab({
        url: '/pages/index/index'
      })
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    getChooseAvatar(e) {
      const {
        avatarUrl
      } = e.detail
      var base64 = wx.getFileSystemManager().readFileSync(avatarUrl, 'base64')
      this.uploadAvatar(base64)
    },
    uploadAvatar(base64) {
      getApp().network.post(getApp().api.base64UploadNoLogin, {
        base64: 'data:image/png;base64,' + base64,
        catalogId: getApp().constant.CATALOG_MEMBER,
        fileName: 'member-avatar'
      }).then((res) => {
        console.log(res)
        this.setData({
          avatar: res.uuid,
          avatarUrl: getApp().util.getFileView(res.uuid),
        })
      }).catch((_) => {})
    },
    noLogin() {
      this.setData({
        actionSheet: false
      })
    },
    doAuthorized() {
      if (this.data.agree !== '1') {
        getApp().util.showToast("请先同意用户协议")
        return
      }
      getApp().getOpenId().then(openId => {
        this.setData({
          openId:openId
        })
        this.doLogin()
      })
    },
    doLogin() {
      getApp().network.post(getApp().api.appletLogin, this.data.openId).then((res) => {
        if (false === res.success) {
          this.setData({
            actionSheet: true
          })
        } else {
          wx.setStorageSync(getApp().constant.TOKEN, res.data)
          getApp().util.showToast("登录成功！")
          getApp().verifyLogin()
          setTimeout(() => {
            wx.switchTab({
              url: '/pages/index/index',
            })
          }, 1000);
        }
      }).catch((err) => {
        console.log(err)
      })
    },
    /**
     * 获得手机号-激发函数
     */
    getMobile: function ({
      detail
    }) {
      if (detail.errMsg.indexOf('getPhoneNumber:ok') > -1) {
        this.getPhoneNumber(detail.code)
      }
    },

    getPhoneNumber(code) {
      getApp().network.get(getApp().api.getPhoneNumber, {
        code: code
      }).then((res) => {
        this.setData({
          mobile: res,
        })
      })
    },

    doRegiser() {
      console.log(this.data)
      var {
        openId,
        nickName,
        avatar,
        mobile
      } = this.data
      if (!avatar) {
        getApp().util.showToast('请设置用户头像')
        return
      }
      if (!nickName) {
        getApp().util.showToast('请设置用户昵称！')
        return
      }
      if (!mobile) {
        getApp().util.showToast('请设置手机号！')
        return
      }
      getApp().network.post(getApp().api.appletRegister, {
        openId,
        nickName,
        mobile,
        avatar
      }).then((res) => {
        wx.setStorageSync(getApp().constant.TOKEN, res)
        getApp().util.showToast("注册成功,登录中！")
        getApp().verifyLogin()
        setTimeout(() => {
          wx.switchTab({
            url: '/pages/index/index',
          })
        }, 1000);
      }).catch((err) => {
        console.log(err)
      })
    },
  }
})