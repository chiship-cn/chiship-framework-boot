Page({

  /**
   * 页面的初始数据
   */
  data: {
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    listQuery: {
      page: 1,
      limit: 20,
      noticeTitle: '',
      sort: '-sendTime',
      noticeType: 1,
      noticeUserType: 'member'
    },
    noticePages: 1,
    notices: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initNotices()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  onSearch() {
    this.setData({
      notices: [],
      listQuery: {
        ...this.data.listQuery,
        noticeTitle: this.data.keyword,
        page: 1
      }
    })
    this.initNotices()
  },
  pullDownRefresh() {
    console.log('下拉刷新')
    this.setData({
      keyword: null,
      notices: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.initNotices()
  },
  loadMore() {
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if (page < this.data.noticePages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.initNotices()
  },
  initNotices() {
    getApp().network.get(getApp().api.noticePage, this.data.listQuery).then(res => {
      var notices = []
      for (var item of res.records) {
        item.createdBy = getApp().util.fmtOptionUser(item.createdBy)
        notices.push(item)
      }

      this.setData({
        isLoading: false,
        notices: this.data.notices.concat(...notices),
        noticePages: res.pages
      })
    })
  },
  goArticleDetails(event) {
    wx.navigateTo({
      url: '/pages/notice/details/index?id=' + event.currentTarget.dataset.id,
    })
  }

})