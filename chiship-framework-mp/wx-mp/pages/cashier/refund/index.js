Page({
  /**
   * 页面的初始数据
   */
  data: {
    defaultStatus: '0',
    defaultSort: '-gmtCreated',
    orderStatus: [{
        text: '全部',
        value: ''
      },
      {
        text: '申请中',
        value: '0'
      },
      {
        text: '退款成功',
        value: '1'
      },
      {
        text: '处理中',
        value: '2'
      },
      {
        text: '退款关闭',
        value: '3'
      },
      {
        text: '退款异常',
        value: '4'
      },
      {
        text: '退款失败',
        value: '5'
      }
    ],
    sortOrders: [{
      text: '默认排序',
      value: '-gmtCreated'
    }],
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    categories: [],
    listQuery: {
      page: 1,
      limit: 20,
      title: '',
      refundStatus: '0',
      sort: '-gmtCreated'
    },
    orderPages: 1,
    orderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchOrder()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  handleDropdownChange(event) {
    this.setData({
      orderList: [],
      orderPages: 1,
      listQuery: {
        ...this.data.listQuery,
        refundStatus: this.data.defaultStatus,
        sort: this.data.defaultSort,
        page: 1
      }
    })
    this.fetchOrder()
  },
  onSearch() {
    this.setData({
      orderList: [],
      listQuery: {
        ...this.data.listQuery,
        refundNo: this.data.keyword,
        page: 1
      }
    })
    this.fetchOrder()
  },
  pullDownRefresh() {
    this.setData({
      keyword: null,
      orderList: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.fetchOrder()
  },
  loadMore() {
    var page = this.data.listQuery.page
    if (page < this.data.orderPages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.fetchOrder()
  },
  fetchOrder() {
    getApp().network.get(getApp().api.refundOrderList, this.data.listQuery).then(res => {
      var order = []
      for (var item of res.records) {
        item._orderDate = getApp().util.formatTime(item.orderDate, 'yyyy-MM-dd hh:mm:ss')
        order.push(item)
      }
      this.setData({
        isLoading: false,
        orderList: this.data.orderList.concat(...order),
        orderPages: res.pages
      })
    })
  }
})