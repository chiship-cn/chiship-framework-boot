Page({
  /**
   * 页面的初始数据
   */
  data: {
    defaultOrderType: '',
    defaultStatus: '0',
    defaultSort: '-gmtCreated',
    orderTypes: [{
      text: '全部订单',
      value: ''
    }, {
      text: '充值订单',
      value: '0'
    }, {
      text: '商品订单',
      value: '1'
    }],
    orderStatus: [{
        text: '全部订单',
        value: ''
      },
      {
        text: '已付款',
        value: '2'
      },
      {
        text: '待付款',
        value: '0'
      },
      {
        text: '付款失败',
        value: '1'
      },
      {
        text: '已关闭',
        value: '3'
      }
    ],
    sortOrders: [{
        text: '默认排序',
        value: '-gmtCreated'
      },
      {
        text: '支付排序',
        value: '-payTime'
      }
    ],
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    categories: [],
    listQuery: {
      page: 1,
      limit: 20,
      title: '',
      orderStatus: '0',
      sort: '-gmtCreated'
    },
    orderPages: 1,
    orderList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchOrder()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  handleDropdownChange(event) {
    this.setData({
      orderList: [],
      orderPages: 1,
      listQuery: {
        ...this.data.listQuery,
        orderStatus: this.data.defaultStatus,
        orderType:this.data.defaultOrderType,
        sort: this.data.defaultSort,
        page: 1
      }
    })
    this.fetchOrder()
  },
  onSearch() {
    this.setData({
      orderList: [],
      listQuery: {
        ...this.data.listQuery,
        orderNo: this.data.keyword,
        page: 1
      }
    })
    this.fetchOrder()
  },
  pullDownRefresh() {
    this.setData({
      keyword: null,
      orderList: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.fetchOrder()
  },
  loadMore() {
    var page = this.data.listQuery.page
    if (page < this.data.orderPages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.fetchOrder()
  },
  fetchOrder() {
    getApp().network.get(getApp().api.orderList, this.data.listQuery).then(res => {
      var order = []
      for (var item of res.records) {
        item._orderDate = getApp().util.formatTime(item.orderDate, 'yyyy-MM-dd hh:mm:ss')
        order.push(item)
      }
      this.setData({
        isLoading: false,
        orderList: this.data.orderList.concat(...order),
        orderPages: res.pages
      })
    })
  },
  handleDoRefund({
    currentTarget
  }) {
    var orderId = currentTarget.dataset.id
    getApp().network.post(getApp().api.refundPay, {
      orderNo: orderId,
      refundAmount: 0.01,
      refundReason: '我就想退款'
    }).then((res) => {
      getApp().util.showToast('退款申请已发起')
      this.pullDownRefresh()
    }).catch((_) => {})
  },
  handleDoPay({
    currentTarget
  }) {
    var orderId = currentTarget.dataset.id
    getApp().getOpenId().then(openId => {
      this.doPay(orderId, openId)
    })

  },
  doPay(orderNo, openId) {
    getApp().network.post(getApp().api.doPrepay, {
      openId: openId,
      tradeType: 'MINI_PROGRAM',
      payMethod: 'wx_v3',
      orderNo: orderNo
    }).then(res => {
      wx.requestPayment({
        timeStamp: res.timeStamp,
        nonceStr: res.nonceStr,
        package: res.packageVal,
        signType: res.signType,
        paySign: res.paySign,
        success: (res) => {
          wx.showToast({
            title: '支付成功！',
          })
          this.pullDownRefresh()
        },
        fail: function (res) {}

      })
    })
  },
})