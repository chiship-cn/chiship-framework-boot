// pages/cashier/pay/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    payWay: '1',
    moneys: [0.01, 0.1, 0.5, 1, 2],
    moneySelect: 0.01,
    payMoney: 0.01
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getTabBar().init();
    getApp().verifyLogin()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  handleFillMoney({
    currentTarget
  }) {
    var {
      dataset: {
        value
      }
    } = currentTarget
    console.log(currentTarget)
    if (value) {
      this.setData({
        moneySelect: value,
        payMoney: value
      })
    }
  },
  async doCreateOrder() {
    if (this.data.payMoney == 0) {
      getApp().util.showToast('充值金额不能为0')
      return
    }
    var openId =await getApp().getOpenId()
    var orderNo = await getApp().network.post(getApp().api.createRechargeOrder, {
      orderName: '小程序账号充值',
      price: this.data.payMoney,
      openId: openId
    })
    this.doPay(orderNo, openId)
  },
  doPay(orderNo, openId) {
    getApp().network.post(getApp().api.doPrepay, {
      openId: openId,
      tradeType: 'MINI_PROGRAM',
      payMethod: 'wx_v3',
      orderNo: orderNo
    }).then(res => {
      wx.requestPayment({
        timeStamp: res.timeStamp,
        nonceStr: res.nonceStr,
        package: res.packageVal,
        signType: res.signType,
        paySign: res.paySign,
        success: function (res) {
          wx.showToast({
            title: '支付成功！',
          })
        },
        fail: function (res) {}

      })
    })
  },
  goWallet(){
    wx.navigateTo({
      url: '/pages/my/wallet/index',
    })
  },
  goOrderList() {
    wx.navigateTo({
      url: '/pages/cashier/order/index',
    })
  },
  goRefundOrderList() {
    wx.navigateTo({
      url: '/pages/cashier/refund/index',
    })
  },
})