// pages/my/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wallet: getApp().util.fmtPrice(0),
    funs: [],
    userInfo: {},
    avatarUrl: '',
    version: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    getApp().verifyLogin().then(res => {
      this.fetchWallet()
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      userInfo: getApp().getUserInfo(),
      avatarUrl: getApp().util.getFileView(getApp().getUserInfo().avatar) || '/image/user.jpg',
      version: getApp().constant.VERSION
    })
    this.getTabBar().init();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  fetchWallet() {
    getApp().network.get(getApp().api.getUserWallet, ).then(res => {
      this.setData({
        ...res,
        wallet: getApp().util.fmtPrice(res.wallet)
      })
    })
  },
  goSetting() {
    //跳转页面
    wx.navigateTo({
      url: '/pages/my/setting/index',
    })
  },
  goFeedback() {
    wx.navigateTo({
      url: '/pages/feedback/index/index',
    })
  },
  goWallet() {
    wx.navigateTo({
      url: '/pages/my/wallet/index',
    })
  },
  handleModifyNickName() {
    let $this = this
    wx.showModal({
      title: '设置昵称',
      editable: true,
      placeholderText: '请输入2-10长度昵称',
      success(res) {
        if (res.confirm) {
          var nickName = res.content
          if (!nickName) {
            getApp().util.showToast("请输入昵称")
            $this.handleModifyNickName()
            return
          }
          if (nickName.length > 10 || nickName.length < 2) {
            getApp().util.showToast("请正确输入昵称")
            $this.handleModifyNickName()
            return
          }

          getApp().network.post(getApp().api.modifyNickName, nickName).then((res) => {
            getApp().util.showToast('修改成功！')
            wx.setStorageSync(getApp().constant.TOKEN, res)
            getApp().verifyLogin()
            $this.setData({
              userInfo: {
                ...$this.data.userInfo,
                nickName: nickName
              }
            })
          }).catch((err) => {})
        }
      }
    })
  },

})