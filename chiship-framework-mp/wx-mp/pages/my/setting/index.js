// pages/my/setting/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cacheSize: '0Kb',
    userInfo: {},
    avatarUrl: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var cacheSize = getApp().util.fmtFileSize(wx.getStorageInfoSync().currentSize)
    this.setData({
      cacheSize: cacheSize
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.setData({
      userInfo: getApp().getUserInfo(),
      avatarUrl: getApp().util.getFileView(getApp().getUserInfo().avatar) || '/image/user.jpg'
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  clearStorage() {
    wx.showModal({
      title: '注意',
      content: '是否清楚当前缓存，并跳转到授权登录页？',
      success(res) {
        if (res.confirm) {
          wx.clearStorageSync()
          wx.redirectTo({
            url: '/pages/user/login/userName/index'
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  goAbout() {
    wx.navigateTo({
      url: '/pages/my/about/index',
    })
  },
  goPersonalData() {
    wx.navigateTo({
      url: '/pages/my/personalData/index',
    })
  },
  goModifyPassword() {
    wx.navigateTo({
      url: '/pages/my/modifyPassword/index',
    })
  },
  doLogout() {
    wx.showModal({
      title: '退出提示',
      content: '您是否真的退出当前账号，是否继续？',
      success(res) {
        if (res.confirm) {
          getApp().network.post(getApp().api.logout).then(res => {
            wx.clearStorageSync()
            wx.redirectTo({
              url: '/pages/user/login/userName/index'
            })
          })
        } else if (res.cancel) {}
      }
    })
  },
})