Page({

  /**
   * 页面的初始数据
   */
  data: {
    binds: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  fetchInfo() {
    //getCurrentUserInfo
    getApp().network.get(getApp().api.getCurrentUserInfo).then(res => {
      console.log(res)
      var _gender = '保密'
      console.log(res.memberUser.gender)
      if (res.memberUser.gender) {
        if (res.memberUser.gender === 1) {
          _gender = '男'
        }
        if (res.memberUser.gender === 2) {
          _gender = '女'
        }
      }
      this.setData({
        ...res.memberUser,
        _avatar: getApp().util.getFileView(res.memberUser.avatar),
        _gender,
        regionInfo: res.memberUser._regionLevel1 + res.memberUser._regionLevel2 + res.memberUser._regionLevel3 + res.memberUser._regionLevel4,
        binds: res.binds
      })
    })
  },
  handleUpload(event) {
    const {
      file
    } = event.detail;
    getApp().network.upload(getApp().api.fileUpload, {
      file: file.url,
      catalogId: getApp().constant.CATALOG_MEMBER
    }).then((res) => {
      this.setData({
        avatar: res.uuid,
        _avatar: getApp().util.getFileView(res.uuid)
      });
    }).catch((_) => {

    })
  },
  handleOpenRegion() {
    this.selectComponent("#regionSelect").open()
  },
  handleSelectRegion({
    detail
  }) {
    console.log(detail)
    var {
      regionIds,
      reginName
    } = detail
    this.setData({
      regionInfo: reginName,
      regionLevel1: regionIds[0],
      regionLevel2: regionIds[1],
      regionLevel3: regionIds[2],
      regionLevel4: regionIds[3]
    })
  },
  doSave() {
    var {
      avatar,
      realName,
      userName,
      gender,
      nickName,
      email,
      regionLevel1,
      regionLevel2,
      regionLevel3,
      regionLevel4,
      address,
      signature,
      introduction
    } = this.data
    getApp().network.post(getApp().api.modifyUser, {
      avatar: avatar,
      realName,
      userName,
      gender,
      regionLevel1,
      regionLevel2,
      regionLevel3,
      regionLevel4,
      address,
      introduction: introduction,
      email: email,
      signature: signature,
      nickName: nickName
    }).then((res) => {
      wx.setStorageSync(getApp().constant.TOKEN, res)
      getApp().util.showToast('修改成功！')
      setTimeout(() => {
        this.fetchInfo()
      }, 1000);
    }).catch((err) => {})
  },
  handleChooseGender() {
    const gender = this.selectComponent('#gender');
    gender.open()
  },
  onGenderSelect({
    detail
  }) {
    this.setData({
      _gender: detail.name,
      gender: detail.code
    })
  }
})