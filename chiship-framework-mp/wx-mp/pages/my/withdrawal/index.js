// pages/my/withdrawal/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    wallet: getApp().util.fmtPrice(0),
    withdrawalAmount: '',
    withdrawalBtnDisable: true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.fetchWallet()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  fetchWallet() {
    getApp().verifyLogin().then(res => {
      getApp().network.get(getApp().api.getUserWallet, ).then(res => {
        this.setData({
          ...res,
          wallet: getApp().util.fmtPrice(res.wallet)
        })
      })
    })
  },
  handleFillAll() {
    this.setData({
      withdrawalAmount: this.data.wallet,
      withdrawalBtnDisable: false
    })
  },
  handleWithdrawalChange(e) {
    var money = this.data.withdrawalAmount
    const regExp = /^[0-9]\d*(\.\d{1,2})?$/
    var disabled = true
    disabled = !regExp.test(money)
    console.log(money)
    if (money*1 >= 0.1 && money*1 <= this.data.wallet * 1) {
      console.log(1111)
      disabled = false
    }else{
      disabled=true
    }
    console.log(disabled)
    this.setData({
      withdrawalBtnDisable: disabled
    })
  },
  doWithdrawal() {
    if (this.data.withdrawalBtnDisable === true) {
      return
    }
    getApp().getOpenId().then(openId => {
      getApp().network.post(getApp().api.memberUserWithdrawalApplication, {
        openId: openId,
        money: this.data.withdrawalAmount,
        channelWay: 'wx_v3',
        reason: '申请提现'
      }).then(res => {
        getApp().util.showToast('提现申请成功,等待审核')
        this.setData({
          withdrawalBtnDisable:true
        })
        setTimeout(() => {
          wx.redirectTo({
            url: '/pages/my/wallet/index',
          })
        }, 2000);
      })
    })

  }
})