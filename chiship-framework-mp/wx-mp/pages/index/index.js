// pages/login/index.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    funs: [],
    scrollHeight: 700,
    adverts: [],
    noticeContent: null,
    contentArticles: []
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.initFuns()
    var $this = this
    wx.getSystemInfo({
      success: function (res) {
        let windowHeight = (res.windowHeight * (750 / res.windowWidth));
        console.log(windowHeight)
        if (res.model.search('iPhone X') != -1) {}
        $this.setData({
          windowWidth: res.windowWidth,
          scrollHeight: ((windowHeight * 0.6) - (250) * 750 / res.windowWidth)
        })
      }
    })
    this.fetchAdvert()
    this.fetchNotice()
    this.fetchContentArticles()

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getTabBar().init();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  initFuns() {
    this.setData({
      funs: [{
          code: 'grsz',
          label: '个人设置'
        },
        {
          code: 'zxzx',
          label: '资讯中心'
        },
        {
          code: 'gywm',
          label: '关于我们'
        },
      ]
    })
  },
  goFunction(e) {
    var code = e.currentTarget.dataset.item.code
    var url = null
    switch (code) {
      case 'grsz': //个人设置
        url = "/pages/my/personalData/index"
        wx.navigateTo({
          url: url,
        })
        break;
      case 'zxzx': //资讯中心
        this.handleLookMore()
        break;
      case 'gywm': //关于我们
        url = "/pages/my/about/index"
        wx.navigateTo({
          url: url,
        })
        break;
    }

  },
  fetchAdvert() {
    getApp().network.get(getApp().api.contentAdvertByCode, {
      code: 'member_index'
    }).then(res => {
      console.log(res)
      res.children.forEach(item => {
        item.imageUrl = getApp().util.getFileView(item.imageUrl)
      })
      this.setData({
        adverts: res.children
      })
    })
  },
  fetchNotice() {
    getApp().network.get(getApp().api.noticePage, {
      page: 1,
      limit: 5,
      sort: '-sendTime',
      noticeType: 1,
      noticeUserType: 'member'
    }).then(res => {
      var content = ''
      res.records.forEach((item, index) => {
        content += (index + 1) + '.' + item.noticeTitle + '。'
      })
      this.setData({
        noticeContent: content
      })
    })
  },
  goArticleDetails(event) {
    wx.navigateTo({
      url: '/pages/article/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  fetchContentArticles() {
    getApp().network.get(getApp().api.contentPage, {
      page: 1,
      limit: 50
    }).then(res => {
      res.records.forEach(item => {
        item.image1 = getApp().util.getFileView(item.image1) || '/image/noNews.png'
      })
      this.setData({
        contentArticles: res.records
      })
    })
  },
  handleLookMore() {
    wx.navigateTo({
      url: "/pages/article/index/index",
    })
  },
  handleGoNotice() {
    wx.navigateTo({
      url: '/pages/notice/index/index',
    })
  }
})