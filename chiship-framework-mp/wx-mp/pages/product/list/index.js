// pages/product/list/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    scrollHeight: 700,
    activeCategory: 'top',
    categories: [],
    listQuery: {
      page: 1,
      limit: 20,
      title: '',
      treeNumber: ''
    },
    productPages: 1,
    products: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var categoryId='0'
    var treeNumber=null
    var categoryName=null
    if (options) {
      categoryId=options.id
      treeNumber=options.tree
      categoryName=options.name
    }
    this.setData({
      categoryId: categoryId?categoryId:'0',
      treeNumber:treeNumber?treeNumber:null
    })
    console.log(1111,this.data.listQuery)
    if(categoryName){
      wx.setNavigationBarTitle({
        title: categoryName
      })
    }
   
    this.fetchCategory()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  fetchCategory() {
    getApp().network.get(getApp().api.productCategory,{pid:this.data.categoryId}).then(res => {
      var categories = [{
        id:this.data.categoryId,
        name: '全部',
        treeNumber: this.data.treeNumber
      }]
      res.forEach(item => {
        categories.push({
          id: item.id,
          name: item.categoryName,
          treeNumber: item.treeNumber
        })
      });
      this.setData({
        categories: categories,
        productPages: 1,
        products: [],
        listQuery: {
          ...this.data.listQuery,
          page: 1,
          treeNumber: this.data.treeNumber
        }
      })
      this.fetchProducts()
    })
  },
  onChange(event){
    this.setData({
      activeCategory:event.detail.name,
      products:[],
      productPages:1,
      listQuery:{
        ...this.data.listQuery,
        page:1,
        treeNumber:this.data.categories[event.detail.index].treeNumber
      }
    })
    this.fetchProducts()
  },
  pullDownRefresh(){
    this.setData({
      keyword:null,
      products:[],
      reachBottomText:null,
      listQuery:{
        ...this.data.listQuery,
        page:1
      }
    })
    this.fetchProducts()
  },
  loadMore(){
    var page = this.data.listQuery.page
    if(page<this.data.productPages){
      page+=1
      this.setData({
        reachBottomText: null
      })
    }else{
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword:null,
      listQuery:{
        ...this.data.listQuery,
        page:page
      }
    })
    this.fetchProducts()
  },
  fetchProducts(){
    getApp().network.get(getApp().api.productPage, this.data.listQuery).then(res=>{
      var products=[]
      for(var item of res.records){
        item.smallImage=getApp().util.getFileView(item.smallImage)
        products.push(item)
      }
      this.setData({
        isLoading:false,
        products:this.data.products.concat(...products),
        productPages:res.pages
      })
    }) 
  },
  goProductDetails(event) {
    wx.navigateTo({
      url: '/pages/product/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
})