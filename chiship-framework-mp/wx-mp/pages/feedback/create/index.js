// pages/user/forget/index.js
import WxValidate from '../../../utils/WxValidate'

Page({

  /**
   * 页面的初始数据
   */
  data: {
    catalogId: getApp().constant.CATALOG_FEEDBACK,
    title: '[会员小程序]意见反馈',
    content: '',
    categoryId: '',
    categoryName: '',
    images: '',
    videos: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initValidate()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  initValidate() {
    const rules = {
      title: {
        required: true,
        rangelength: [1, 50]
      },
      categoryId: {
        required: true
      },
      content: {
        required: true,
        rangelength: [1, 1000]
      }
    }
    const messages = {
      title: {
        required: '请输入标题',
        rangelength: '请输入50字以内的标题'
      },
      categoryId: {
        required: '请选择分类',
      },
      content: {
        required: '请输入详细描述',
        rangelength: '请输入1000字以内的详细描述'
      }
    }
    this.WxValidate = new WxValidate(rules, messages)
  },
  doSave() {
    var {
      title,
      content,
      categoryId,
      images,
      videos
    } = this.data
    if (!this.WxValidate.checkForm(this.data)) {
      const error = this.WxValidate.errorList[0];
      getApp().util.showToast(error.msg)
      return;
    }
    getApp().network.post(getApp().api.feedbackSave, {
      title: title,
      category: categoryId,
      content: content,
      images: images,
      videos: videos
    }).then(res => {
      getApp().util.showToast('提交成功')
      setTimeout(() => {
        wx.navigateTo({
          url: '/pages/feedback/index/index',
        })
      }, 500);
    }).catch(() => {})
  },
  handleOpenCategoryDict() {
    this.selectComponent("#categoryDictSelect").open()
  },
  handleSelectCategoryDict({
    detail
  }) {
    console.log(detail)
    this.setData({
      categoryId: detail.id,
      categoryName: detail.name
    })
  },
  getImage({
    detail
  }) {
    this.setData({
      images: detail
    })
  },
  getVideo({
    detail
  }) {
    console.log(1111, detail)
    this.setData({
      videos: detail
    })
  },
})