// pages/article/details/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id: null,
    feedbackRecord: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    //options={id:'1011780143340318720'}
    if (!options) {
      this.handleError()
      return
    }
    var id = options.id
    if (!id) {
      this.handleError()
      return
    }
    this.setData({
      id: id
    })
    this.initDetails()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  handleError() {
    getApp().util.showToast('缺少参数')
    wx.navigateTo({
      url: '/pages/feedback/index/index',
    })
  },
  initDetails() {
    getApp().network.get(getApp().api.feedbackView.replace('$ID', this.data.id)).then(res => {
      this.setData({
        feedbackRecord: {
          ...res,
          _gmtCreated: getApp().util.formatTime(res.gmtCreated, 'yyyy-MM-dd hh:mm'),
          _replyTime: getApp().util.formatTime(res.replyTime, 'yyyy-MM-dd hh:mm'),
          _replyPeople: getApp().util.fmtOptionUser(res.replyPeople)
        }
      })
    })
  }
})