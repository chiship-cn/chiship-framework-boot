Page({

  /**
   * 页面的初始数据
   */
  data: {
    reachBottomText: null,
    keyword: null,
    isLoading: false,
    listQuery: {
      page: 1,
      limit: 20,
      keyword: '',
      sort: '-gmtCreated'
    },
    feedbackPages: 1,
    feedbacks: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initfeedbacks()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  },
  onSearch() {
    this.setData({
      feedbacks: [],
      listQuery: {
        ...this.data.listQuery,
        keyword: this.data.keyword,
        page: 1
      }
    })
    this.initfeedbacks()
  },
  pullDownRefresh() {
    console.log('下拉刷新')
    this.setData({
      keyword: null,
      feedbacks: [],
      reachBottomText: null,
      listQuery: {
        ...this.data.listQuery,
        page: 1
      }
    })
    this.initfeedbacks()
  },
  loadMore() {
    console.log('上拉加载')
    var page = this.data.listQuery.page
    if (page < this.data.feedbackPages) {
      page += 1
      this.setData({
        reachBottomText: null
      })
    } else {
      this.setData({
        reachBottomText: '已经到达最后一页'
      })
      return
    }
    this.setData({
      keyword: null,
      listQuery: {
        ...this.data.listQuery,
        page: page
      }
    })
    this.initfeedbacks()
  },
  initfeedbacks() {
    getApp().network.get(getApp().api.feedbackPage, this.data.listQuery).then(res => {
      var feedbacks = []
      for (var item of res.records) {
        item.createdBy = getApp().util.fmtOptionUser(item.createdBy)
        feedbacks.push(item)
      }

      this.setData({
        isLoading: false,
        feedbacks: this.data.feedbacks.concat(...feedbacks),
        feedbackPages: res.pages
      })
    })
  },
  goArticleDetails(event) {
    wx.navigateTo({
      url: '/pages/feedback/details/index?id=' + event.currentTarget.dataset.id,
    })
  },
  goFeedbackCreate() {
    wx.navigateTo({
      url: '/pages/feedback/create/index',
    })
  },
  onSwipeClose(event) {
    const {
      position,
      instance
    } = event.detail;
    const {
      dataset: {
        id
      }
    } = event.currentTarget
    console.log(id)
    switch (position) {
      case 'right':
        wx.showModal({
          title: '提示',
          content: '确定删除吗？',
          success: (res) => {
            if (res.confirm) {
              getApp().network.post(getApp().api.feedbackRemove, [id]).then(result => {
                getApp().util.showToast('删除成功!')
                this.pullDownRefresh()
              }).catch((_) => {})
            }
          }
        })
        break;
    }
  }

})