var api = require('./api')
var constant = require('./constant')

var util = {
  getAppId(){
    var {miniProgram:{appId}}=wx.getAccountInfoSync()
    if(!appId){
      console.error('获取appId失败，配置默认appId或忽略，自行选择')
    }
    return appId
  },
  formatNumber(n) {
    n = n.toString()
    return n[1] ? n : `0${n}`
  },

  formatDateMonth(date) {
    var date = new Date(date)
    var year = date.getFullYear()
    var month = date.getMonth() + 1
    console.log(year + "-" + month, date, 12321)

    return [year, month].map(formatNumber).join('-')
  },
  formatTime(dateTime, fmt) {
    let date = new Date(dateTime);
    if (!fmt) {
      const nowDate = new Date()
      var getTime = (nowDate.getTime() - date.getTime()) / 1000
      if (getTime < 60 * 1) {
        return '刚刚'
      } else if (getTime >= 60 * 1 && getTime < 60 * 60) {
        return parseInt(getTime / 60) + '分钟前'
      } else if (getTime >= 3600 && getTime < 3600 * 24) {
        return parseInt(getTime / 3600) + '小时前'
      } else if (getTime >= 3600 * 24 && getTime < 3600 * 24 * 7) {
        return parseInt(getTime / 3600 / 24) + '天前'
      } else {
        fmt = 'yyyy-MM-dd hh:mm:ss'
      }
    }
    let o = {
      "M+": date.getMonth() + 1, //月份
      "d+": date.getDate(), //日
      "h+": date.getHours(), //小时
      "m+": date.getMinutes(), //分
      "s+": date.getSeconds(), //秒
      "q+": Math.floor((date.getMonth() + 3) / 3), //季度
      "S": date.getMilliseconds() //毫秒
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (let k in o)
      if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;

  },
  getFirstDayOfMonth(year, month) {
    return new Date(year, month, 1).getTime()
  },
  getLastDayOfMonth(year, month) {
    return new Date(year, month + 1, 0).getTime()
  },
  showToast(message = "提示消息") {
    wx.showToast({
      title: message,
      icon: 'none'
    })
  },
  uuid() {
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
      s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
    // s[8] = s[13] = s[18] = s[23] = "-";

    var uuid = s.join("");
    return uuid;
  },
  isMobile(val) {
    return /^1[0-9]{10}$/.test(val)
  },
  formateIphoneTime(date) {
    if (typeof date === 'string') {
      return date.replace(/-/g, '/');
    } else {
      return date
    }
  },
  getFileView(uuid) {
    if (uuid) {
      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
        return uuid
      } else {
        return api.BUSINESS + "/fileView/" + uuid + "?AppId=" + constant.APP_ID + "&AppKey=" + constant.APP_KEY + "&ProjectsId=" + constant.PROJECTS_ID
      }
    } else {
      return null
    }
  },
  fmtOptionUser(val) {
    if (val) {
      try {
        var optionUser = JSON.parse(val)
        return optionUser.realName + (optionUser.orgName ? (' | ' + optionUser.orgName) : '')
      } catch (err) {
        return '-'
      }
    }
    return '-'
  },
  fmtFileSize(val = 0) {
    if (!val) {
      return '-'
    } else if (typeof (val) !== 'number') {
      return '-'
    } else if (val < 1024) {
      return (val).toFixed(2) + 'KB'
    } else if (val < 1024 * 1024) {
      return (val / 1024).toFixed(2) + 'M'
    } else {
      return (val / 1024 / 1024).toFixed(2) + 'G'
    }
  },
  fmtPrice(val){
    if (val === undefined) return ''
    const Num = val.toString().split('.')
    let price = ''
    if (Num[1]) {
      if (Num[1].length > 2) {
        const dot = Num[1].slice(0, 2)
        price = `${Num[0]}.${dot}`
      } else if (Num[1].length === 1) {
        price = `${val}0`
      } else {
        // 小数点后位数等于2
        price = val
      }
    } else {
      price = `${val}.00`
    }
    return price
  }
}
module.exports = util