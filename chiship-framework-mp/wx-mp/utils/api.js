// config.js

var host = "https://chiship.site/"
var host = 'http://localhost:10020/'
var BUSINESS = host + 'chiship/framework/boot'
var apiConfig = {
  BUSINESS,
  host,
  //地区
  region: `${BUSINESS}/region/cacheListByPid`,
  //数据字典
  listDataDictByCode: `${BUSINESS}/dataDict/findByCode?dataDictCode=$CODE`,
  //分类字典
  listCategoryDictByCode: `${BUSINESS}/upmsCategoryDict/cacheTreeByType?type=$TYPE`,
  //获取手机验证码
  mobileVerificationCode: `${BUSINESS}/smsCode/mobile`,
  //获取手机号
  getPhoneNumber: `${BUSINESS}/wxMini/code2Mobile`,
  //解析code-拿到openid、session_key
  code2Session: `${BUSINESS}/wxMini/code2Session`,
  //授权登录
  appletLogin: `${BUSINESS}/memberSso/appletLogin`,
  //普通登录
  basicLogin: `${BUSINESS}/memberSso/basicLogin`,
  //手机号+验证码登录
  mobileLogin: `${BUSINESS}/memberSso/mobileLogin`,
  //获取钱包
  getUserWallet: `${BUSINESS}/memberSso/getWallet`,
  //获取用户信息
  getUserInfo: `${BUSINESS}/memberSso/getInfo`,
  getCurrentUserInfo: `${BUSINESS}/memberSso/getCurrentUserInfo`,
  //找回密码
  forgetPassword: `${BUSINESS}/memberSso/forgotPassword`,
  //修改密码
  modifyPassword: `${BUSINESS}/memberSso/modifyPassword`,
  //注册用户
  userRegister: `${BUSINESS}/memberSso/register`,
  //注册
  appletRegister: `${BUSINESS}/memberSso/wxMpRegister`,
  //退出
  logout: `${BUSINESS}/memberSso/logout`,
  //修改用户
  modifyUser: `${BUSINESS}/memberSso/modifyUser`,
  //修改用户名
  modifyUserName: `${BUSINESS}/memberSso/modifyUserName`,
  //修改昵称
  modifyNickName: `${BUSINESS}/memberSso/modifyNickName`,
  //余额变动明细
  memberUserWalletChange: `${BUSINESS}/memberUserWalletChange/page/current`,
  //申请提现
  memberUserWithdrawalApplication: `${BUSINESS}/pay/withdrawal`,
  //文件上传
  fileUpload: `${BUSINESS}/file/upload`,
  //Base64文件上传
  base64Upload: `${BUSINESS}/file/base64Upload`,
  base64UploadNoLogin: `${BUSINESS}/file/base64UploadNoLogin`,
  //文件上传
  fileUpload: `${BUSINESS}/file/upload`,
  //分类字典
  categoryDictTree: `${BUSINESS}/upmsCategoryDict/cacheTreeByType`,
  //新闻列表
  contentPage: `${BUSINESS}/contentArticle/pageV3`,
  //新闻浏览
  contentView: `${BUSINESS}/contentArticle/view/$ID`,
  //新闻上一条下一条
  contenPreAndNext: `${BUSINESS}/contentArticle/findPreAndNext`,
  //通知公告
  noticePage: `${BUSINESS}/notice/pageV1`,
  //通知公告浏览
  noticeView: `${BUSINESS}/notice/getById/$ID`,
  //广告
  contentAdvertByCode: `${BUSINESS}/contentAdvert/getByCode`,
  //意见反馈
  feedbackPage: `${BUSINESS}/feedback/current/page`,
  //意见反馈浏览
  feedbackView: `${BUSINESS}/feedback/getById/$ID`,
  //意见反馈提交
  feedbackSave: `${BUSINESS}/feedback/save`,
  //意见反馈删除
  feedbackRemove: `${BUSINESS}/feedback/remove`,

  //添加收藏点赞
  collectionLikeAdd: `${BUSINESS}/memberCollectionLike/save`,
  //取消收藏点赞
  collectionLikeCancel: `${BUSINESS}/memberCollectionLike/cancel`,
  //是否收藏点赞
  isCollectionLike: `${BUSINESS}/memberCollectionLike/isAdd`,

  //发布评论
  memberCommentCreate: `${BUSINESS}/memberComment/save`,
  //文章评论列表
  memberCommentPage: `${BUSINESS}/memberComment/pageV1`,

  //预支付
  doPrepay: `${BUSINESS}/pay/prepay`,
  //创建充值订单
  createRechargeOrder: `${BUSINESS}/businessOrderHeader/createRechargeOrder`,
  //创建商品订单
  createProductOrder: `${BUSINESS}/businessOrderHeader/createProductOrder`,
  //订单列表
  orderList: `${BUSINESS}/businessOrderHeader/current/page`,

  //退款列表
  refundOrderList: `${BUSINESS}/businessRefundOrder/current/page`,
  //退款
  refundPay: `${BUSINESS}/pay/refund`,
  //商品分类
  productCategory: `${BUSINESS}/productCategory/cacheTreeTable`,
  //商品列表
  productPage: `${BUSINESS}/product/pageV3`,
  //商品详情
  productDetails: `${BUSINESS}/product/getDetailsById/$ID`
};
module.exports = apiConfig