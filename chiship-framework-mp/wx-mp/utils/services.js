 var services = {
   get(url, params = {}, message = '请求中...') {
     console.log(url, params)
     wx.showLoading({
       title: message,
     })
     return new Promise((reslove, reject) => {
       wx.request({
         url: url,
         data: params,
         method: 'GET',
         success: function (res) {
           wx.hideLoading()
           console.log(11, res)
           if (res.statusCode == 200) {
             reslove(res.data)
           } else {
             reject(res)
           }
         },
         fail: function (res) {
           wx.hideLoading()
           wx.showToast({
             title: res.errMsg || '网络故障',
             icon: 'none',
             duration: 2000,
             mask: true
           })
           reject(res)
         }
       })
     })
   },
   post(url, params = {}, message = '请求中...') {
     wx.showLoading({
       title: message,
     })
     return new Promise((reslove, reject) => {
       wx.request({
         url: url,
         data: params,
         method: 'POST',
         success: function (res) {
           wx.hideLoading()
           if (res.statusCode == 200) {
             reslove(res.data);
           } else {
             reject(res)
           }
         },
         fail: function (res) {
           wx.hideLoading()
           wx.showToast({
             title: res.errMsg || '网络故障',
             icon: 'none',
             duration: 2000,
             mask: true
           })
           reject(res)
         }
       })
     })
   }
 }
 //根据判断message 是否显示loading
 function postRequestLoading(url, params, message, success, fail) {
   if (message != "") {
     wx.showLoading({
       title: message,
     })
   }
   const postRequestTask = wx.request({
     url: url,
     data: params,
     method: 'POST',
     success: function (res) {
       console.log(res)
       if (message != "") {
         wx.hideLoading()
       }
       if (res.statusCode == 200) {
         success(res.data);
       } else {
         wx.showToast({
           title: res.data.data == null ? res.data.message : typeof (res.data.data) == 'object' ? JSON.stringify(res.data.data) : res.data.data,
           icon: 'none',
           duration: 2000,
           mask: true
         })
         fail(res)
       }
     },
     fail: function (res) {
       if (message != "") {
         wx.hideLoading()
       }
       wx.showToast({
         title: res.errMsg || '网络故障',
         icon: 'none',
         duration: 2000,
         mask: true
       })
       fail(res)
     }
   })
 }

 module.exports = services