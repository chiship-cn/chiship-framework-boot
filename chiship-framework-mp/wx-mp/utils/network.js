var constant = require('./constant')
var util = require('./util')
var headers = {
  'content-type': 'application/json;charset=UTF-8',
  "ProjectsId": constant.PROJECTS_ID,
  "App-Id": constant.APP_ID,
  "App-Key": constant.APP_KEY,
  "Sign": "MD5="
}
var network = {
  get(url, params, message = '加载中...', errorShow = true) {
    wx.showLoading({
      title: message,
    })
    params = {
      ...params,
      appId: util.getAppId()
    }
    return new Promise((resolve, reject) => {
      wx.request({
        url: url,
        data: params,
        header: {
          ...headers,
          "Access-Token": wx.getStorageSync(constant.TOKEN)
        },
        method: 'GET',
        success: (res) => {
          wx.hideLoading()
          this.dealResponse(res, resolve, reject)
        },
        fail: function (res) {
          wx.hideLoading()
          util.showToast(res.errMsg || '网络故障')
          reject(res)
        }
      })
    })
  },
  post(url, params, message = '加载中...', errorShow = true) {
    wx.showLoading({
      title: message,
    })
    return new Promise((resolve, reject) => {
      wx.request({
        url: url + "?appId=" + util.getAppId(),
        data: params,
        header: {
          ...headers,
          "Access-Token": wx.getStorageSync(constant.TOKEN)
        },
        method: 'POST',
        success: (res) => {
          wx.hideLoading()
          this.dealResponse(res, resolve, reject)
        },
        fail: function (res) {
          wx.hideLoading()
          util.showToast(res.errMsg || '网络故障')
          reject(res)
        }
      })
    })
  },
  upload(url, params, message = '上传中...', errorShow = true) {
    if (!params.file) {
      wx.showToast({
        title: '请选择文件',
      })
      return
    }
    if (!params.catalogId) {
      wx.showToast({
        title: '请配置catalogId参数',
      })
      return
    }
    wx.showLoading({
      title: message,
    })
    return new Promise((reslove, reject) => {
      delete headers['content-type']
      wx.uploadFile({
        url: url,
        filePath: params.file,
        name: "file", //后台要绑定的名称
        header: {
          ...headers,
          "content-type": "multipart/form-data",
          "Access-Token": wx.getStorageSync(constant.TOKEN)
        },
        //参数绑定
        formData: {
          ...params
        },
        success: function (res) {
          var {
            data,
            statusCode
          } = res
          wx.hideLoading()
          data = JSON.parse(data)
          if (statusCode == 200 && data.code == 200) {
            reslove(data.data)
          } else if (data.code == 502 || data.code == 506003) {
            wx.showToast({
              title: '令牌失效，重新登录',
              icon: 'none',
              duration: 2000,
              mask: true
            })
            setTimeout(function () {
              wx.clearStorageSync(constant.USER_INFO)
              wx.clearStorageSync(constant.TOKEN)
              wx.redirectTo({
                url: '/pages/user/login/userName/index'
              })
            }, 1000)
          } else {
            var msg = data.data == null ? res.data.message : typeof (res.data) == 'object' ? res.data.data : res.data
            wx.showToast({
              title: msg,
              icon: 'none',
              duration: 3000,
              mask: true
            })
            reject(data)
          }
        },
        fail: function (ress) {
          wx.hideLoading()
          wx.showToast({
            title: '网络故障',
            icon: 'none',
            duration: 2000,
            mask: true
          })
          reject(ress)
        }
      })
    })
  },
  dealResponse(res, resolve, reject) {
    var {
      data,
      statusCode
    } = res
    if (statusCode == 200 && data.code == 200) {
      resolve(data.data)
    } else if (data.code == 502 || data.code == 506003) {
      util.showToast('令牌失效，重新登录')
      setTimeout(function () {
        wx.removeStorageSync(constant.USER_INFO)
        wx.removeStorageSync(constant.TOKEN)
        wx.redirectTo({
          url: '/pages/user/login/userName/index'
        })
      }, 1000)
    } else {
      var msg = data.data == null ? data.message : typeof (data.data) == 'object' ? JSON.stringify(data.data) : data.data
      util.showToast(msg)
      reject(msg)
    }
  }
}

module.exports = network