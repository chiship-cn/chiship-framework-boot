Component({
  data: {
    active: 0,
    list: [{
      text: '首页',
      icon: {
        normal: '../image/tab_home.png',
        active: '../image//tab_home_active.png',
      },
      url: "/pages/index/index"
    },{
      text: '收银台',
      icon: {
        normal: '../image/tab_home.png',
        active: '../image//tab_home_active.png',
      },
      url: "/pages/cashier/pay/index"
    }, {
      text: '快速操作',
    }, {
      icon: {
        normal: '../image/tab_my.png',
        active: '../image/tab_my_active.png',
      },
      text: '商品',
      url: "/pages/product/index/index"
    },{
      icon: {
        normal: '../image/tab_my.png',
        active: '../image/tab_my_active.png',
      },
      text: '我的',
      url: "/pages/my/index"
    }]
  },
  methods: {
    onChange(event) {
      wx.switchTab({
        url: this.data.list[event.detail].url
      });
    },
    init() {
      const page = getCurrentPages().pop();
      console.log(page)
      this.setData({
        active: this.data.list.findIndex(item => item.url === `/${page.route}`)
      });
    }
  }
});