Component({
  /**
   * 组件的属性列表
   */
  properties: {
    video: '',
    count: {
      type: Number,
      value: 1
    },
    duration: {
      type: Number,
      value: 60
    },
    catalogId: {
      type: String,
      value: getApp().constant.CATALOG_UNKNOW
    },
    remove: {
      type: Boolean,
      value: true
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    uploadFile: [],
    videoList: []
  },
  created() {
    this.recombinationvideoUrls()
  },
  observers: {
    'video': function (val) {
      if (val) {
        this.setData({
          uploadFile: val.split(',')
        })
        var videoList = []
        this.data.uploadFile.forEach(item => {
          videoList.push({
            url: getApp().util.getFileView(item),
            isvideo: true,
            uuid: item
          })
        })
        this.setData({
          videoList
        })
      }
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    recombinationvideoUrls() {
      var videoList = []
      var uploadFile = this.data.uploadFile
      this.data.uploadFile.forEach(item => {
        videoList.push({
          url: getApp().util.getFileView(item),
          uuid: item
        })
      })
      this.setData({
        videoList
      })
      this.triggerEvent('getvideo', uploadFile.join(','))
    },
    removevideo(e) {
      var uuid = e.target.dataset.uuid
      var uploadFile = this.data.uploadFile
      var temp = uploadFile.filter(item => {
        return item !== uuid
      })
      this.setData({
        uploadFile: temp
      })
      this.recombinationvideoUrls()
    },
    handleUpload(event) {
      const {
        file: {
          url,
          thumb,
          size,
          duration,
          width,
          height
        }
      } = event.detail;
      if (duration > this.data.duration) {
        getApp().util.showToast('上传的小视频大于' + this.data.duration + '秒')
        return
      }
      getApp().network.upload(getApp().api.fileUpload, {
        file: url,
        catalogId: this.properties.catalogId
      }).then((res) => {
        var uploadFile = this.data.uploadFile
        uploadFile.push(res.uuid)
        this.setData({
          uploadFile: uploadFile
        })
        this.recombinationvideoUrls()

      }).catch((_) => {})
    },
  }
})