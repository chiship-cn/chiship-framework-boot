// components/RegionSelect/RegionSelect.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false,
    selectRegion: {},
    regions: []
  },
  /**
   * 组件的方法列表
   */
  methods: {
    open(region) {
      this.setData({
        show: true,
        selectRegion: {}
      })
      this.fetchRegion(100000)
    },
    onClose() {
      this.setData({
        show: false,
        selectRegion: {}
      })
    },
    fetchRegion(id) {
      getApp().network.get(getApp().api.region, {
        pid: id
      }).then((res) => {
        if (res.length === 0) {
          this.chooseRegion({
            target: {
              dataset: {
                obj: null
              }
            }
          })
          return
        }
        this.setData({
          regions: res
        })
      })
    },
    chooseRegion({
      target
    }) {
      var obj = target.dataset.obj
      if (!obj) {
        this.setData({
          show: false
        })
        obj = {
          isLeaf: true
        }
      } else {
        var selectRegion = this.data.selectRegion
        selectRegion[obj.level] = obj
        this.setData({
          selectRegion
        })
      }

      if (obj.isLeaf === false) {
        this.fetchRegion(obj.id)
      } else {
        this.setData({
          show: false
        })
        var obj = {}
        var regionNames = []
        regionNames.push(this.data.selectRegion.province.name)
        regionNames.push(this.data.selectRegion.city.name)
        regionNames.push(this.data.selectRegion.district.name)
        regionNames.push(this.data.selectRegion.street ? this.data.selectRegion.street.name : '')
        regionNames.push(this.data.selectRegion.village ? this.data.selectRegion.village.name : '')

        obj.reginName = regionNames.join('')

        var regionIds = []
        regionIds.push(this.data.selectRegion.province.id)
        regionIds.push(this.data.selectRegion.city.id)
        regionIds.push(this.data.selectRegion.district.id)
        regionIds.push(this.data.selectRegion.street ? this.data.selectRegion.street.id : '')
        regionIds.push(this.data.selectRegion.village ? this.data.selectRegion.village.id : '')

        obj.regionIds = regionIds
        var regionInfos = []
        regionInfos.push(this.data.selectRegion.province.id + ":" + this.data.selectRegion.province.name)
        regionInfos.push(this.data.selectRegion.city.id + ":" + this.data.selectRegion.city.name)
        regionInfos.push(this.data.selectRegion.district.id + ":" + this.data.selectRegion.district.name)
        regionInfos.push((this.data.selectRegion.street ? this.data.selectRegion.street.id : '') + ":" + (this.data.selectRegion.street ? this.data.selectRegion.street.name : ''))
        regionInfos.push((this.data.selectRegion.village ? this.data.selectRegion.village.id : '') + ":" + (this.data.selectRegion.village ? this.data.selectRegion.village.name : ''))

        obj.regionInfos = regionInfos.join(';')
        this.triggerEvent('selected', obj)

      }
    },
    chooseParentRegion({
      target
    }) {
      var obj = target.dataset.obj
      var selectRegion = this.data.selectRegion
      selectRegion[obj.level] = obj
      this.setData({
        selectRegion
      })
      if (obj.isLeaf === false) {
        this.fetchRegion(obj.pid)
      }
    },
  }
})