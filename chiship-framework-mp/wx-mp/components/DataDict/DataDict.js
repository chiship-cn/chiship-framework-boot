// components/DataDict/DataDict.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    code: '',
    title: '标题'
  },

  /**
   * 组件的初始数据
   */
  data: {
    popupShow: false,
    columnDatas: [],
    dataDicts: []
  },
  observers: {
    'code': function (val) {
      if (val) {
        this.fetchDataDict()
      }
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    open() {
      this.setData({
        popupShow: true
      })
    },
    popupClose() {
      this.setData({
        popupShow: false,
      })
    },
    onPopupDataSelect(event) {
      this.setData({
        popupShow: false,
      })
      var selected = this.data.dataDicts[event.detail.index]
      this.triggerEvent('selected', {
        name: selected.dataDictItemName,
        code: selected.dataDictItemCode,
        color: selected.colorValue,
      })
    },
    fetchDataDict() {
      getApp().network.get(getApp().api.listDataDictByCode.replace('$CODE', this.properties.code)).then((res) => {
        var columnDatas = []
        res.forEach(item => {
          columnDatas.push(item.dataDictItemName)
        })
        this.setData({
          columnDatas: columnDatas,
          dataDicts: res
        })
      })

    }
  }
})