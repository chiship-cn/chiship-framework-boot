Component({

  /**
   * 组件的属性列表
   */
  properties: {
    code: ''
  },

  /**
   * 组件的初始数据
   */
  data: {
    show: false,
    categoryDicts: []
  },
  observers: {
    'code': function (val) {
      if (val) {}
    }
  },
  /**
   * 组件的方法列表
   */
  methods: {
    open() {
      this.setData({
        show: true,
        categoryDicts: []
      })
      this.fetchCategoryDict()
    },
    onClose() {
      this.setData({
        show: false,
        categoryDicts: []
      })
    },
    fetchCategoryDict() {
      getApp().network.get(getApp().api.listCategoryDictByCode.replace('$TYPE', this.properties.code)).then((res) => {
        if (res.length === 0) {
          this.chooseRegion({
            target: {
              dataset: {
                obj: null
              }
            }
          })
          return
        }
        this.setData({
          categoryDicts: res
        })
      })
    },
    chooseCategoryDict({
      target
    }) {
      var obj = target.dataset.obj
      var children = obj.children
      if (children) {
        this.setData({
          categoryDicts: children
        })
      } else {
        wx.showModal({
          title: '确认',
          content: '确定要选中的数据？',
          cancelText: '重新',
          success: (res) => {
            if (res.confirm) {
              this.triggerEvent('selected', {
                name: obj.name,
                id: obj.id,
                tree: obj.treeNumber,
              })
              this.setData({
                categoryDicts: [],
                show: false
              })
            } else if (res.cancel) {
              this.fetchCategoryDict()
            }
          }
        })

      }
    },
    chooseParentRegion({
      target
    }) {},
  }
})