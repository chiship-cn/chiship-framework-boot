let app=getApp()
Page({
  data: {
    avatarUrl:'../../images/user.jpg',
    cacheSize:'0Kb',
    userInfo:{}
  },
  onLoad() {},
  onShow() {
    app.verifyLogin()
    var cacheSize=app.util.fmtFileSize(dd.getStorageInfoSync().currentSize)
    this.setData({ 
      userInfo:app.getUserInfo(),
      avatarUrl:app.getUserInfo().avatar?app.util.getFileView(app.getUserInfo().avatar):'../../images/user.jpg',
      cacheSize:cacheSize
    })

    console.log(this.data.userInfo)
  },
  handleClearCache(){
    dd.confirm({
      title: '注意',
      content: '是否清除当前所有缓存，并跳转到授权登录页面？',
      cancelButtonText: '取消',
      confirmButtonText: '确定',
      success: ({confirm}) => {
        console.log(res)
        if(confirm){
          dd.clearStorageSync();
          app.verifyLogin()
        }
      },
      fail: () => {},
      complete: () => {}
    })
  },
  handleLogout(){
    dd.confirm({
      title: '退出提示',
      content: '您是否真的退出当前账号，是否继续',
      cancelButtonText: '取消',
      confirmButtonText: '确定',
      success: ({confirm}) => {
        if(confirm){
          this.doLogout()
        }
      },
      fail: () => {},
      complete: () => {}
    })
  },
  doLogout(){
    app.network.post(app.api.logout).then(res=>{
      dd.clearStorageSync();
      app.verifyLogin()
    })
  }
});
