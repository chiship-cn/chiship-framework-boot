const app=getApp()
Page({
  data: {},
  onLoad() {

  },
  handleLogin(){
    dd.getAuthCode({
      success:({authCode})=>{
        this.doLogin(authCode)
      },
      fail:(err)=>{
        app.util.showToast('授权失败')
      }
    });
  },
  doLogin(authCode){
    app.network.post(app.api.authLogin,authCode).then(res=>{
      dd.setStorageSync({
        key: app.constant.TOKEN,
        data: res
      });
      app.verifyLogin().then(res=>{
        dd.reLaunch({
          url: '/pages/index/index'
        });
      })
     
    })
  }
});
