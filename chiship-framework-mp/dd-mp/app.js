import util from './utils/util'
import constant from './utils/constant'
import api from './utils/api'
import network from './utils/network'

App({
  onLaunch(options) {
    // 第一次打开
    // options.query == {number:1}
    console.info('App onLaunch');
  },
  onShow(options) {
    // 从后台被 scheme 重新打开
    // options.query == {number:1}
  },
  verifyLogin(){
    return new Promise((resolve,reject)=>{
      var token=util.getStorage(constant.TOKEN)
      if(!token){
        util.showToast('您还未登录，请登录')
        setTimeout(function(){
          dd.navigateTo({
            url: '/pages/login/index'
          })
        },1000)
        reject('您还未登录，请登录')
      }else{
        network.get(api.getUserInfo).then(res=>{
          util.setStorage(constant.USER_INFO,res)
          resolve(res)
        }).catch((_)=>{

        })
      }
    })
    
  },
  getUserInfo(){
    return util.getStorage(constant.USER_INFO)
  },
  util,
  constant,
  network,
  api
});
