import constant from './constant'
import api from './api'
var util={
  getFileView(uuid) {
    if (uuid) {
      if (uuid && (uuid.indexOf('http://') === 0 || uuid.indexOf('https://') === 0)) {
        return uuid
      } else {
        return api.BUSINESS+"/fileView/"+uuid+"?AppId="+constant.APP_ID+"&AppKey="+constant.APP_KEY+"&ProjectsId="+constant.PROJECTS_ID
      }
    } else {
      return null
    }
  },
  fmtFileSize(val=0) {
    if (!val) {
      return '-'
    } else if (typeof (val) !== 'number') {
      return '-'
    } else if (val < 1024) {
      return (val).toFixed(2) + 'KB'
    } else if (val < 1024 * 1024 ) {
      return (val / 1024 ).toFixed(2) + 'M'
    } else {
      return (val / 1024 / 1024 ).toFixed(2) + 'G'
    }
  },
  showToast(message="提示消息"){
    dd.showToast({content: message})
  },
  getStorage(key){
    const {data} = dd.getStorageSync({
      key: key,
    });
    return data;
  },
  setStorage(key,value){
    dd.setStorageSync({
      key: key,
      data: value,
    });
  },
  removeStorage(key){
    dd.removeStorageSync({
      key: key,
    });
  }
}

export default util