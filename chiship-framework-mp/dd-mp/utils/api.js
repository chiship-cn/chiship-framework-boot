// config.js

var host = "https://chiship.site/"
//var host = 'http://localhost:10020/'
var BUSINESS = host + 'chiship/framework/boot'
var api = {
  BUSINESS,
  host,
  //地区
  region: `${BUSINESS}/region/cacheListByPid`,
  //授权码登录
  authLogin: `${BUSINESS}/sso/dingTalk/authCodeLogin`,
  //获取用户信息
  getUserInfo: `${BUSINESS}/sso/getInfo`,
  getCurrentUserInfo: `${BUSINESS}/sso/getCurrentUserInfo`,
  //退出登录
  logout: `${BUSINESS}/sso/logout`,
  
};
module.exports = api