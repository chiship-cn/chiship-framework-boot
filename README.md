
<p align="center">
	<img alt="logo" src="./wiki/images/logo.png" style="width:32px;height:32px"/>
</p>
<h2 align="center">Chiship.JAVA快速开发平台(单体版develop)</h2>
<p align="center">基于SpringBoot+Vue开发的轻量JAVA快速开发框架</p>


项目介绍
-----------------------------------
从事全栈开发已有10年有余，企业中做过大大小小项目10来个。于是就自己一直想做一款后台管理系统，将自己常用到的功能统一做一个集成，看了很多优秀的开源项目但是发现没有合适自己用的。于是决定利用空闲休息时间与老婆着手开始自己写一套适合自己并适用于自己的后台系统。

Chiship快速开发平台是一款企业快速开发定制开发平台！前后端分离架构 SpringBoot2.7.x、ElementUI Vue2，Mybatis-plus，JWT，支持微服务。集成了微信小程序、微信公众号、钉钉小程序、H5、抖音小程序、鸿蒙APP等。同时也集成了最基本的系统权限模块、微信运营模块、内容模块、文档模块、收银台模块， 帮助解决Java项目40%的重复工作，让开发更多关注业务。既能快速提高效率，节省研发成本，同时又不失灵活性！

该框架可以用于所有的Web应用程序，如网站管理后台，网站会员中心，CMS，CRM，OA。所有前端后台代码封装过后十分精简易上手，出错概率低。同时支持移动客户端访问。系统会持续更新一些实用功能。

项目说明
-----------------------------------

### 项目说明


|   项目名  |   说明    |   备注    |
|--------------------|--------------------|--------------------|
|   `chiship-framework-admin`   |   后台管理服务    |   已实现  |
|   `chiship-framework-app/chiship_framework_app_admin_echo`   |   鸿蒙NextApp    |   已实现  |
|   `chiship-framework-h5/chiship-framework-h5-admin`   |   H5后台管理    |   已实现  |
|   `chiship-framework-h5/chiship-framework-h5-member`   |   H5会员系统    |   待规划  |
|   `chiship-framework-h5/chiship-framework-uniapp`   |   UniApp    |  实现部分功能  |
|   `chiship-framework-mp/dd-mp`   |   钉钉小程序(适用于后台管理)    |   实现部分功能  |
|   `chiship-framework-mp/dy-mp`   |   抖音小程序(适用于会员)    |   实现部分功能  |
|   `chiship-framework-mp/wx-mp`   |   微信小程序(适用于会员)    |   已实现  |
|   `chiship-framework-mp/dy-mp`   |   支付宝小程序(适用于会员)    |   实现部分功能  |
|   `chiship-framework-web/chiship-framework-business-v2`   |   Vue2后台管理    |   已实现  |
|   `chiship-framework-web/chiship-framework-business-v3`   |   Vue3后台管理    |   待规划  |

### 在线体验

- 官方网站：[码云](https://gitee.com/chiship-cn/chiship-framework-simple)
- 在线演示：
    - 管理后台：[预览](http://chiship.site/simple)    （用户名：admin   密码：Abcde123456    手机：18300000000    企业邀请码：ch6xe7eprn2l）
    - H5管理后台：[预览](http://chiship.site/simple/h5-admin)    （用户名：admin   密码：Abcde123456）
    - 微信公众号：
        <p align="center">
            <img alt="logo" src="./wiki/images/wxPub.png"/>
        </p>

    - 微信小程序：
        <p align="center">
	        <img alt="logo" src="./wiki/images/wxMp.png"/>
        </p>
     - 支付宝小程序：
        <p align="center">
	        <img alt="logo" src="./wiki/images/zfb.png"/>
        </p>

- 反馈问题：提交[issue](https://gitee.com/chiship-cn/chiship-framework-simple/issues)
- QQ交流群：1002858707
        <p align="center">
	        <img alt="logo" src="./wiki/images/qqChat.png"/>
        </p>

- 数据库脚本：加入QQ群，说明哪个版本，@群主获得！

技术架构
-----------------------------------
### 后端
- IDE建议： IDEA
- 语言：Java 8+
- 依赖管理：Maven
- 基础框架：Spring Boot 2.7.18
- 持久层框架：MybatisPlus 3.5.7
- 安全框架：Jwt 0.9.1
- 数据库连接池：阿里巴巴Druid 1.1.9
- 日志打印：logback
- 缓存：Redis
- 其他：fastjson，poi、Swagger-ui、quartz、chiship-sdk等。
- 默认数据库脚本：MySQL5.7+
- 其他数据库，需要自己转
  
### 前端

- 前端IDE建议：HbuilderX、Vscode
- 采用 Vue2.0+Vuex+Axios+ElementUI等技术方案，包括二次封装组件、utils、、权限校验、按钮级别权限控制等功能
- 依赖管理：node、npm、pnpm、yarn
  
### 环境要求
- 本地环境安装 `Node.js 、npm 、pnpm、JDK1.8、redis-server、MySQL5.7、各类小程序开发工具、鸿蒙Next开发工具`
- Node.js 版本建议`v16.13.0`，要求`Node 16+` 版本以上

内置功能
-----------------------------------
### 管理后台

#### 工作台模块
1.  工作台
2.  概览
3.  驾驶舱

#### 业务模块
1.  信息提交
2.  表单数据

#### 内容模块
1.  新闻管理
2.  栏目管理
3.  通知公告
4.  意见反馈
5.  政策协议
6.  广告轮播
   
#### 商品模块
1.  商品发布
2.  商品管理
3.  分类管理
4.  品牌管理

#### 会员模块
1.  会员管理

#### 收银台模块
1.  收银台
2.  支付订单
3.  退款申请
4.  支付流水
5.  退款流水
6.  通知记录
    
#### 渠道模块
1.  支付配置
2.  微信公众号
       -    公众号配置
       -    自定义菜单
       -    消息自动回复
       -    素材管理(素材管理、图文管理、素材分类)
       -    微信管理(粉丝管理、用户标签)
       -    二维码管理
       -    统计分析(用户分析、菜单分析、内容分析)
3.  微信小程序
       -    小程序配置
       -    二维码管理
     
#### 用户模块
1.  用户管理
2.  组织管理
3.  菜单管理
4.  角色管理

#### 系统模块
1.  参数配置
2.  字典管理
3.  分类管理
4.  定时任务
5.  行政区划
6.  APP版本
7.  验证码
8.  表单设计
9.  数据库
 
#### 监控模块
1.  在线用户
2.  SQL监控
3.  性能监控(服务监控、Redis监控、Tomcat系统、JVM信息、请求追踪)
4.  接口文档

#### 安全日志
1.  登录日志
2.  操作日志
3.  错误日志

### 微信小程序
1.  登录
2.  注册、
3.  忘记密码
4.  授权
5.  广告轮播
6.  通知公告
7.  新闻内容(点赞、收藏、评论)
8.  支付
9.  个人信息维护
10. 修改密码
11. 一些常用功能

### H5后台管理
1.  登录
2.  公众号授权
3.  微信支付、支付宝H5支付
4.  订单
5.  退款

### 系统效果

#### 后台
|     |       |
|--------------------|--------------------|
| ![输入图片说明](wiki/images/%E5%B7%A5%E4%BD%9C%E5%8F%B0.jpg)  |  ![输入图片说明](wiki/images/%E6%A6%82%E8%A7%88.jpg) |
| ![输入图片说明](wiki/images/%E7%94%A8%E6%88%B7%E7%AE%A1%E7%90%86.jpg)  |  ![输入图片说明](wiki/images/%E8%A7%92%E8%89%B2%E7%AE%A1%E7%90%86.jpg) |
| ![输入图片说明](wiki/images/%E7%BB%84%E7%BB%87%E7%AE%A1%E7%90%86.jpg)  | ![输入图片说明](wiki/images/%E8%8F%9C%E5%8D%95%E7%AE%A1%E7%90%86.jpg)  |
|  ![输入图片说明](wiki/images/%E5%8F%82%E6%95%B0%E9%85%8D%E7%BD%AE.jpg) |  ![输入图片说明](wiki/images/%E5%AD%97%E5%85%B8%E7%AE%A1%E7%90%86.jpg) |
|  ![输入图片说明](wiki/images/%E5%88%86%E7%B1%BB%E5%AD%97%E5%85%B8.jpg) | ![输入图片说明](wiki/images/APP%E7%89%88%E6%9C%AC.jpg)  |
|  ![输入图片说明](wiki/images/%E4%B8%AA%E4%BA%BA%E4%B8%AD%E5%BF%83.jpg) | ![输入图片说明](wiki/images/%E7%B3%BB%E7%BB%9F%E6%96%87%E6%A1%A3.jpg)  |
| ![输入图片说明](wiki/images/%E5%85%85%E5%80%BC%E7%BC%B4%E8%B4%B9.jpg)  |  ![输入图片说明](wiki/images/%E9%80%80%E6%AC%BE%E7%94%B3%E8%AF%B7.jpg) |
|![输入图片说明](wiki/images/%E6%94%AF%E4%BB%98%E8%AE%A2%E5%8D%95.jpg)|![输入图片说明](wiki/images/%E6%94%AF%E4%BB%98%E9%85%8D%E7%BD%AE.jpg)|
|![输入图片说明](wiki/images/%E6%96%B0%E9%97%BB%E7%AE%A1%E7%90%86.jpg)|![输入图片说明](wiki/images/%E6%96%B0%E9%97%BB%E5%8F%91%E5%B8%83.jpg)|
|![输入图片说明](wiki/images/%E6%96%B0%E9%97%BB%E8%AF%A6%E6%83%85.jpg)|![输入图片说明](wiki/images/%E9%80%9A%E7%9F%A5%E5%85%AC%E5%91%8A.jpg)|
|![输入图片说明](wiki/images/%E5%85%AC%E4%BC%97%E5%8F%B7%E9%85%8D%E7%BD%AE.jpg)|![输入图片说明](wiki/images/%E5%9B%BE%E6%96%87%E7%B4%A0%E6%9D%90.jpg)|
|![输入图片说明](wiki/images/%E7%B2%89%E4%B8%9D%E7%AE%A1%E7%90%86.jpg)|![输入图片说明](wiki/images/%E6%B6%88%E6%81%AF%E8%87%AA%E5%8A%A8%E5%9B%9E%E5%A4%8D.jpg)|
|![输入图片说明](wiki/images/%E8%87%AA%E5%AE%9A%E4%B9%89%E8%8F%9C%E5%8D%95.jpg)|![输入图片说明](wiki/images/%E4%BA%8C%E7%BB%B4%E7%A0%81%E6%8E%A8%E5%B9%BF.jpg)|
|![输入图片说明](wiki/images/%E5%95%86%E5%93%81%E5%8F%91%E5%B8%83.jpg)|![输入图片说明](wiki/images/%E5%95%86%E5%93%81%E5%88%97%E8%A1%A8.jpg)|
|![输入图片说明](wiki/images/%E5%95%86%E5%93%81%E5%88%86%E7%B1%BB.jpg)|![输入图片说明](wiki/images/%E8%A1%A8%E5%8D%95%E8%AE%BE%E8%AE%A1.jpg)|
|![输入图片说明](wiki/images/%E4%BF%A1%E6%81%AF%E6%8F%90%E4%BA%A4.jpg)|![输入图片说明](wiki/images/%E8%A1%A8%E5%8D%95%E6%95%B0%E6%8D%AE.jpg)|

#### 微信小程序(会员版)
#### H5(管理版)
#### 鸿蒙Next(管理版)

传送门
-----------------------------------

- [Chiship SDK](https://gitee.com/chiship-cn/chiship-sdk)
- [单体版](https://gitee.com/chiship-cn/chiship-framework-boot.git)

- [简单版](https://gitee.com/chiship-cn/chiship-framework-simple.git)

- [分布式版](https://gitee.com/chiship-cn/chiship-framework-cloud.git)


开源共建
-----------------------------------

### 开源协议

Chiship.JAVA快速开发平台 开源软件遵循 [Apache 2.0 协议](https://www.apache.org/licenses/LICENSE-2.0.html)。
允许商业使用，但务必保留类作者、Copyright 信息。

<p align="center">
	<img alt="logo" src="./wiki/images/apache.jpg" style="height:92px"/>
</p>


### 其他说明

- 欢迎提交 [issue](https://gitee.com/chiship-cn/chiship-framework-simple/issues)，请写清楚遇到问题的原因、开发环境、复显步骤。
注意对应提交对应 `develop` 分支
- 代码规范 [spring-javaformat](https://github.com/spring-io/spring-javaformat)

   <details>
    <summary>代码规范说明</summary>

    1. 由于 <a href="https://github.com/spring-io/spring-javaformat" target="_blank">spring-javaformat</a>
       强制所有代码按照指定格式排版，未按此要求提交的代码将不能通过合并（打包）
    2. 如果使用 IntelliJ IDEA
       开发，请安装自动格式化软件 <a href="https://repo1.maven.org/maven2/io/spring/javaformat/spring-javaformat-intellij-idea-plugin/" target="_blank">
       spring-javaformat-intellij-idea-plugin</a>
    3. 其他开发工具，请参考 <a href="https://github.com/spring-io/spring-javaformat" target="_blank">
       spring-javaformat</a>
       说明，或`提交代码前`在项目根目录运行下列命令（需要开发者电脑支持`mvn`命令）进行代码格式化
       ```
       mvn spring-javaformat:apply
       ```
   </details>
- 鼓励支持 如果觉得平台不错，不要吝啬您的赞许和鼓励，请给我们⭐ STAR ⭐吧！


捐赠
-----------------------------------

如果觉得还不错，请作者喝杯咖啡吧 ☺

<p align="center">
	<img alt="logo" src="./wiki/images/paymentCode.png"/>
</p>

